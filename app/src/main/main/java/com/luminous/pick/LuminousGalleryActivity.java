package com.luminous.pick;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.luminous.pick.LuminousGallery.LuminousAction;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.schollyme.R;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.utility.FontLoader;

public class LuminousGalleryActivity extends Activity {
	public static final int REQ_CODE_LUMINOUS_GALLERY_ACTIVITY = 1099;

	private GridView mGalleryGv;
	private TextView mSelectedCountTv;

	private Handler mHandler;
	private LuminousGalleryAdapter mLuminousGalleryAdapter;

	private ProgressBar mNoMediaIv;

	private LuminousAction mLuminousAction;
	private ImageLoader mImageLoader;
	private HeaderLayout mHeaderLayout;
	private static boolean sDispImage = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.luminous_gallery_activity);

		mLuminousAction = LuminousAction.fromString(getIntent().getAction());
		// mLuminousAction = LuminousAction.ACTION_MULTIPLE_PICK;
		if (mLuminousAction == null) {
			finish();
			return;
		}

		mHeaderLayout = new HeaderLayout(findViewById(R.id.container_ll));
		mHeaderLayout.setHeaderValues(R.drawable.ic_close_header, getString(R.string.Camera_Roll), 0);
		mHeaderLayout.setListenerItI(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
			}
		}, null);

		mImageLoader = initImageLoader(getBaseContext());
		init();
	}

	public static synchronized boolean isDisplayImage() {
		return sDispImage;
	}

	public static Intent getIntent(LuminousAction action) {
		sDispImage = true;
		return new Intent(LuminousAction.getString(action));
	}

	public static Intent getIntentVideo(LuminousAction action) {
		sDispImage = false;
		return new Intent(LuminousAction.getString(action));
	}

	public static ImageLoader initImageLoader(Context context) {
		ImageLoader imageLoader = ImageLoader.getInstance();
		try {
			String CACHE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.temp_tmp";
			new File(CACHE_DIR).mkdirs();

			File cacheDir = StorageUtils.getOwnCacheDirectory(context, CACHE_DIR);

			DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(context)
					.defaultDisplayImageOptions(defaultOptions).discCache(new UnlimitedDiscCache(cacheDir))
					.memoryCache(new WeakMemoryCache());

			ImageLoaderConfiguration config = builder.build();
			imageLoader.init(config);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return imageLoader;
	}

	private View mSelectedCountIv;

	private void init() {
		mHandler = new Handler();
		mSelectedCountTv = (TextView) findViewById(R.id.selected_count_tv);
		mSelectedCountTv.getBackground().setColorFilter(getResources().getColor(R.color.red_normal_color), Mode.MULTIPLY);
		ImageView cameraIv = (ImageView) findViewById(R.id.camera_iv);
		cameraIv.setOnClickListener(mCameraClickListener);
		
		// cameraIv.setVisibility(isDisplayImage() ? View.VISIBLE : View.GONE);
		cameraIv.setVisibility(View.GONE); 
		
		mSelectedCountIv = findViewById(R.id.selected_count_iv);
		mSelectedCountIv.setOnClickListener(mOkClickListener);
		TextView chooseOptionsTv = (TextView) findViewById(R.id.choose_options_tv);
		chooseOptionsTv.setText(isDisplayImage() ? R.string.CHOOSE_PHOTOS : R.string.CHOOSE_VIDEO);
		FontLoader.setRobotoRegularTypeface(chooseOptionsTv, mSelectedCountTv);

		mGalleryGv = (GridView) findViewById(R.id.gallery_gv);
		mGalleryGv.setFastScrollEnabled(true);
		mLuminousGalleryAdapter = new LuminousGalleryAdapter(this, mImageLoader);
		PauseOnScrollListener listener = new PauseOnScrollListener(mImageLoader, true, true);
		mGalleryGv.setOnScrollListener(listener);

		if (mLuminousAction == LuminousAction.ACTION_MULTIPLE_PICK) {
			mGalleryGv.setOnItemClickListener(mItemMulClickListener);
			mLuminousGalleryAdapter.setMultiplePick(true);

		} else if (mLuminousAction == LuminousAction.ACTION_PICK) {
			mGalleryGv.setOnItemClickListener(mItemSingleClickListener);
			mLuminousGalleryAdapter.setMultiplePick(false);

		}

		// LayoutInflater layoutInflater = LayoutInflater.from(this);
		// View headerView =
		// layoutInflater.inflate(R.layout.empty_view_grid_footer, null);
		// mGalleryGv.addFooterView(headerView);

		mGalleryGv.setAdapter(mLuminousGalleryAdapter);
		mNoMediaIv = (ProgressBar) findViewById(R.id.no_media_iv);

		new Thread() {

			@Override
			public void run() {
				Looper.prepare();
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						startMediaListing();
						checkImageStatus();
					}
				});
				Looper.loop();
			};

		}.start();

		updateSelCount(0);
	}

	private void checkImageStatus() {
		if (mLuminousGalleryAdapter.isEmpty()) {
			mNoMediaIv.setVisibility(View.VISIBLE);
		} else {
			mNoMediaIv.setVisibility(View.GONE);
		}
	}

	private static final int REQUEST_IMAGE_CAPTURE = 1019;
	private static final int REQUEST_VIDEO_CAPTURE = 1020;
	private File mCaptureFile = null;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_IMAGE_CAPTURE:
			if (RESULT_OK == resultCode && null != mCaptureFile) {
				Intent intent = new Intent();
				intent.putExtra("data", new String[] { mCaptureFile.getPath() });
				intent.putExtra("sel_type_img", isDisplayImage());
				setResult(resultCode, intent);
			}
			finish();
			break;
		case REQUEST_VIDEO_CAPTURE:
			if (RESULT_OK == resultCode) {
				Intent intent = new Intent();
				// intent.putExtra("data", data.getData().getPath());
				intent.putExtra("data", getPath(data.getData()));
				intent.putExtra("sel_type_img", isDisplayImage());
				setResult(resultCode, intent);
			}
			finish();
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private String getPath(Uri uri) {
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String filePath = cursor.getString(columnIndex);
		cursor.close();

		return filePath;
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(imageFileName, /* prefix */
				".jpg", /* suffix */
				storageDir /* directory */
		);

		return image;
	}

	OnClickListener mCameraClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (isDisplayImage()) {
				Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
					try {
						mCaptureFile = createImageFile();
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (null == mCaptureFile) {
						Toast.makeText(v.getContext(), "Unable to launch camera!", Toast.LENGTH_SHORT).show();
						return;
					}
					takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mCaptureFile));
					startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
				} else {
					Toast.makeText(v.getContext(), "Unable to launch camera!", Toast.LENGTH_SHORT).show();
				}
			} else {
				Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
				if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
					startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
				} else {
					Toast.makeText(v.getContext(), "Unable to launch video capture!", Toast.LENGTH_SHORT).show();
				}
			}
		}
	};

	OnClickListener mOkClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			List<LuminousGallery> selected = mLuminousGalleryAdapter.getSelected();

			String[] allPath = new String[selected.size()];
			for (int i = 0; i < allPath.length; i++) {
				allPath[i] = selected.get(i).sdcardPath;
			}

			Intent data = new Intent().putExtra("data", mLuminousAction == LuminousAction.ACTION_PICK ? allPath[0] : allPath);
			data.putExtra("sel_type_img", isDisplayImage());
			setResult(allPath.length > 0 ? RESULT_OK : RESULT_CANCELED, data);
			finish();
		}
	};

	AdapterView.OnItemClickListener mItemMulClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> l, View v, int position, long id) {
			mLuminousGalleryAdapter.changeSelection(v, position);
			int count = mLuminousGalleryAdapter.getSelected().size();
			updateSelCount(count);
		}
	};

	private View lastView;
	private int lastPos;
	AdapterView.OnItemClickListener mItemSingleClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> l, View v, int position, long id) {
			if (lastPos != position && null != lastView && mLuminousGalleryAdapter.isSelected(lastPos)) {
				mLuminousGalleryAdapter.changeSelection(lastView, lastPos);
			}
			lastView = v;
			lastPos = position;
			mLuminousGalleryAdapter.changeSelection(v, position);
			int count = mLuminousGalleryAdapter.getSelected().size();
			updateSelCount(count);
		}
	};

	AdapterView.OnItemClickListener mItemSingleClickListenerOld = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> l, View v, int position, long id) {
			LuminousGallery item = mLuminousGalleryAdapter.getItem(position);
			Intent data = new Intent().putExtra("data", item.sdcardPath);
			data.putExtra("sel_type_img", isDisplayImage());
			setResult(RESULT_OK, data);
			finish();
		}
	};

	Drawable mRightDrawable = null;

	private void updateSelCount(int count) {
		if (null == mRightDrawable) {
			mRightDrawable = mSelectedCountTv.getCompoundDrawables()[2];
		}
		if (count <= 0) {
			mSelectedCountTv.setCompoundDrawables(null, null, null, null);
			mSelectedCountTv.setText("");
		} else {
			mSelectedCountTv.setCompoundDrawables(null, null, mRightDrawable, null);
			mSelectedCountTv.setText(Integer.toString(count));
		}
		mSelectedCountIv.setEnabled(count > 0);
		mSelectedCountTv.setVisibility(count > 0 ? View.VISIBLE : View.INVISIBLE);
	}

	@Override
	protected void onStart() {
		super.onStart();
		mActivityLiveAb.set(true);
	}

	@Override
	protected void onStop() {
		super.onStop();
		mActivityLiveAb.set(false);
	}

	private AtomicBoolean mActivityLiveAb = new AtomicBoolean();

	private void startMediaListing() {

		new Thread(new Runnable() {

			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				Cursor cursor = null;
				if (isDisplayImage()) {
					String[] columns = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID };
					String orderBy = MediaStore.Images.Media._ID+ " desc";;

					cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
				} else {
					String[] columns = { MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID };
					String orderBy = MediaStore.Video.Media._ID+ " desc";;

					cursor = getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
				}

				final boolean mediaAvailable = null != cursor && cursor.getCount() > 0;
				runOnUiThread(new Runnable() {
					public void run() {
						mNoMediaIv.setVisibility(mediaAvailable ? View.GONE : View.VISIBLE);
					}
				});
				if (mediaAvailable) {
					while (cursor.moveToNext()) {
						final LuminousGallery luminousGallery;
						if (isDisplayImage()) {
							final String sdcardPath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
							File file = new File(sdcardPath);
							System.out.println("PATH : "+sdcardPath);
							if (!file.exists() || file.length() <= 0) {
								continue;// do not add corrupted images
							}
							luminousGallery = new LuminousGallery(sdcardPath);
						} else {
							luminousGallery = new LuminousGallery(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA)));
						}
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								mLuminousGalleryAdapter.add(luminousGallery);
							}
						});
					}
				}
			}
		}).start();
	}

}
