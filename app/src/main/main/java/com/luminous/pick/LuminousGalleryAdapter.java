package com.luminous.pick;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.schollyme.Config;
import com.schollyme.R;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.ImageLoaderUniversal;

public class LuminousGalleryAdapter extends BaseAdapter {
	public static final int MAX_MEDIA_SEL_COUNT = 5;
	private static final String TAG = LuminousGalleryAdapter.class.getSimpleName();
	private LayoutInflater mLayoutInflater;
	private ArrayList<LuminousGallery> mLuminousGalleries = new ArrayList<LuminousGallery>();

	private boolean mAllowMultiplePick;
	private int mImgWidth = 0;
	private VideoThumbLoader mVideoThumbLoader;

	public LuminousGalleryAdapter(Context context, ImageLoader imageLoader) {

		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		int[] screenDimen = new int[2];
		DisplayUtil.probeScreenSize(context, screenDimen);
		float spaceBwItems = context.getResources().getDimension(R.dimen.space_separator);

		mImgWidth = (int) ((screenDimen[0] - spaceBwItems * 3) / 4f);
		if (Config.DEBUG) {
			Log.d(TAG, "screenDimen[0]=" + screenDimen[0] + ", screenDimen[1]=" + screenDimen[1] + ", spaceBwItems=" + spaceBwItems
					+ ", mImgWidth=" + mImgWidth);
		}
		mVideoThumbLoader = new VideoThumbLoader();
	}

	@Override
	public int getCount() {
		return mLuminousGalleries.size();
	}

	@Override
	public LuminousGallery getItem(int position) {
		return mLuminousGalleries.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setMultiplePick(boolean isMultiplePick) {
		this.mAllowMultiplePick = isMultiplePick;
	}

	public void selectAll(boolean selection) {
		/*
		 * if(mLuminousGalleries.size()>10) { for (int i = 0; i < 9; i++) {
		 * mLuminousGalleries.get(i).isSeleted = selection;
		 * 
		 * } }else{
		 */
		for (int i = 0; i < mLuminousGalleries.size(); i++) {
			mLuminousGalleries.get(i).isSeleted = selection;

		}

		notifyDataSetChanged();
	}

	public boolean isAllSelected() {
		boolean isAllSelected = true;

		for (int i = 0; i < mLuminousGalleries.size(); i++) {
			if (!mLuminousGalleries.get(i).isSeleted) {
				isAllSelected = false;
				break;
			}
		}

		return isAllSelected;
	}

	public boolean isAnySelected() {
		boolean isAnySelected = false;

		for (int i = 0; i < mLuminousGalleries.size(); i++) {
			if (mLuminousGalleries.get(i).isSeleted) {
				isAnySelected = true;
				break;
			}
		}

		return isAnySelected;
	}

	public List<LuminousGallery> getSelected() {
		List<LuminousGallery> galleries = new ArrayList<LuminousGallery>();

		for (int i = 0; i < mLuminousGalleries.size(); i++) {
			if (mLuminousGalleries.get(i).isSeleted) {
				galleries.add(mLuminousGalleries.get(i));
			}
		}

		return galleries;
	}

	public void addAll(ArrayList<LuminousGallery> files) {

		try {
			this.mLuminousGalleries.clear();
			this.mLuminousGalleries.addAll(files);

		} catch (Exception e) {
			e.printStackTrace();
		}

		notifyDataSetChanged();
	}

	public void add(LuminousGallery luminousGallery) {
		mLuminousGalleries.add(luminousGallery);
		notifyDataSetChanged();
	}

	public void changeSelection(View v, int position) {
		int selCnt = 0;
		for (int i = 0; i < mLuminousGalleries.size(); i++) {
			if (mLuminousGalleries.get(i).isSeleted) {
				selCnt++;
			}
		}
		if (selCnt >= MAX_MEDIA_SEL_COUNT && !mLuminousGalleries.get(position).isSeleted) {
			DialogUtil.showOkDialog(v.getContext(), "You can select max " + MAX_MEDIA_SEL_COUNT + " items.", "");
			return;
		}
		if (mLuminousGalleries.get(position).isSeleted) {
			mLuminousGalleries.get(position).isSeleted = false;
		} else {
			mLuminousGalleries.get(position).isSeleted = true;
		}

		ViewHolder ViewHolder = (ViewHolder) v.getTag();
		ViewHolder.imgQueueMultiSelected.setSelected(mLuminousGalleries.get(position).isSeleted);
		ViewHolder.imgQueueMultiSelected.setVisibility(/*mAllowMultiplePick &&*/ mLuminousGalleries.get(position).isSeleted ? View.VISIBLE
				: View.INVISIBLE);
	}

	public boolean isSelected(int position) {
		if (null != mLuminousGalleries && position < mLuminousGalleries.size() && position >= 0) {
			return mLuminousGalleries.get(position).isSeleted;
		}
		return false;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;
		if (convertView == null) {

			convertView = mLayoutInflater.inflate(R.layout.luminous_gallery_item, null);
			convertView.setLayoutParams(new AbsListView.LayoutParams(mImgWidth, mImgWidth));
			holder = new ViewHolder();
			holder.imgQueue = (ImageView) convertView.findViewById(R.id.imgQueue);
			holder.imgQueueMultiSelected = (ImageView) convertView.findViewById(R.id.imgQueueMultiSelected);
			holder.play_iv = (ImageView) convertView.findViewById(R.id.play_iv);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.imgQueue.setTag(position);
		final boolean selected = mAllowMultiplePick && mLuminousGalleries.get(position).isSeleted;
		holder.imgQueueMultiSelected.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
		holder.imgQueue.setAlpha(selected ? 0.6f : 1.0f);
		holder.play_iv.setVisibility(View.GONE);
		try {
			if (LuminousGalleryActivity.isDisplayImage()) {
				ImageLoaderUniversal.ImageLoadSquare(holder.imgQueue.getContext(), "file://" + mLuminousGalleries.get(position).sdcardPath,
						holder.imgQueue, ImageLoaderUniversal.option_normal_Image_Thumbnail);
				holder.play_iv.setVisibility(View.GONE);
			} else {
				// loadVideoThumb(mLuminousGalleries.get(position).sdcardPath,
				// holder.imgQueue);

				// File f=new File(mLuminousGalleries.get(position).sdcardPath);

				holder.play_iv.setVisibility(View.VISIBLE);
				mVideoThumbLoader.loadVideoThumb(holder.imgQueue, mLuminousGalleries.get(position).sdcardPath);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	public class ViewHolder {
		ImageView imgQueue;
		ImageView imgQueueMultiSelected;
		ImageView play_iv;

	}

	public void clear() {
		mLuminousGalleries.clear();
		notifyDataSetChanged();
	}
}
