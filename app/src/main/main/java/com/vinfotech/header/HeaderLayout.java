package com.vinfotech.header;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.schollyme.R;
import com.vinfotech.utility.FontLoader;

public class HeaderLayout {
	public ImageButton mLeftIb, mRightIb1;
	public TextView mTitleTv, mRightTv;

	public HeaderLayout(View view) {
		mLeftIb = (ImageButton) view.findViewById(R.id.back_button);
		mRightIb1 = (ImageButton) view.findViewById(R.id.right_button);
		mTitleTv = (TextView) view.findViewById(R.id.header_tv);
		mRightTv = (TextView) view.findViewById(R.id.right_tv);
		FontLoader.setRobotoBoldTypeface(mTitleTv);
		FontLoader.setRobotoRegularTypeface(mRightTv);
	}

	public void setHeaderValues(int leftId, String title, int rightId) {
		if (leftId != 0) {
			mLeftIb.setImageResource(leftId);
			mLeftIb.setVisibility(View.VISIBLE);
		} else {
			mLeftIb.setVisibility(View.INVISIBLE);
		}

		if (title != null) {
			mTitleTv.setText(title);
			mTitleTv.setVisibility(View.VISIBLE);
		}

		if (rightId != 0) {
			mRightIb1.setImageResource(rightId);
			mRightIb1.setVisibility(View.VISIBLE);
		} else {
			mRightIb1.setVisibility(View.INVISIBLE);
		}
	}

	public void enableRightButton(boolean status) {
		if (status) {
			mRightIb1.setImageResource(R.drawable.icon_arrw_right_active_white_xhdpi);
			mRightIb1.setEnabled(true);
		} else {
			mRightIb1.setImageResource(R.drawable.icon_arrw_right_inactive_white_xhdpi);
			mRightIb1.setEnabled(false);
		}
	}

	public void setListenerItI(OnClickListener left, OnClickListener right) {
		mLeftIb.setOnClickListener(left);
		mRightIb1.setOnClickListener(right);
	}

	public TextView getRightTv() {
		return mRightTv;
	}
}
