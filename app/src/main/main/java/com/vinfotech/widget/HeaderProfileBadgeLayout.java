package com.vinfotech.widget;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.schollyme.R;

public class HeaderProfileBadgeLayout {

	public static final int RES_NONE = 0;
	public View containerView;
	private TextView mTitleTv;
	public LinearLayout mBackroundLl;
	public View mDropShowView;

	public HeaderProfileBadgeLayout(View view) {
		containerView = view;
		mTitleTv = (TextView) view.findViewById(R.id.title_tv);
		mTitleTv.setText("");
		mBackroundLl = (LinearLayout) view.findViewById(R.id.background_ll);
		mDropShowView = view.findViewById(R.id.drop_show_view);
	}

	public void setShadowBackGround(int shadow) {
		mDropShowView.setBackgroundResource(shadow);
	}

	public void setShadowVisibility(boolean isShowing) {
		mDropShowView.setVisibility((isShowing) ? View.VISIBLE : View.GONE);
	}

	public TextView gettitleView(String tittle) {
		mTitleTv.setText(tittle);
		return mTitleTv;
	}

	public void setBackGroundColor(int color) {
		containerView.setBackgroundColor(color);
	}

	public void setBackGroundDrawable(Drawable color) {
		containerView.setBackgroundDrawable(color);
	}
}