package com.vinfotech.server.imageupload;

/**
 * Upload Listener
 * 
 */
public interface UploadFinishedListener {
	/**
	 * This method updated how much data size uploaded to server
	 * 
	 * @param num
	 */
	void uploadListener(String value);
}
