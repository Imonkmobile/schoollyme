package com.vinfotech.server.imageupload;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Handler;
import android.util.Log;
import android.view.View;

public class MultiPartUpload {
	private String imagePath;
	private Dialog mDialog = null;
	final static int REQUEST_CODE = 1;
	// CHANGE THIS TO YOUR URL
	private String UPLOAD_SERVER_URI = "";
	ProgressDialog progressDialog;
	private Handler mHandler = null;
	UploadFinishedListener uploadFinishedListener;
	private static final String CONTENT_TYPE = "binary/octet-stream";
	private String mResponse;
	@SuppressWarnings("deprecation")
	List<NameValuePair> mNameValuePair;
	private View mLoaderView = null;

	public enum UPLOADTYPE {
		profilepic, coverpic,room,ideabook;
	}
	public MultiPartUpload(){}

	public MultiPartUpload(Dialog dialog,String uploadURL,String imagePath, String imageName,String accessToken, UploadFinishedListener uploadFinishedListener,@SuppressWarnings("deprecation") List<NameValuePair> basicNVPair) {
		super();
		this.mHandler = new Handler();
		this.imagePath = imagePath;
		this.UPLOAD_SERVER_URI = uploadURL;
		this.uploadFinishedListener = uploadFinishedListener;
		this.mNameValuePair = basicNVPair;
		mDialog = dialog;
		execute();
	}

	public MultiPartUpload(String imagePath, String imageName, UploadFinishedListener uploadFinishedListener, String mFileType) {
		super();
		this.mHandler = new Handler();
		this.imagePath = imagePath;
		this.uploadFinishedListener = uploadFinishedListener;
		execute();
	}

	public void setDialog(Dialog dialog) {
		this.mDialog = dialog;
	}

	public void execute() {
		Runnable runnable = new Runnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {

				try {
					showLoader(true);
					HttpClient httpClient = new DefaultHttpClient();
					HttpPost httpPost = new HttpPost(
							UPLOAD_SERVER_URI.toString());
					if (null != mNameValuePair && mNameValuePair.size() > 0) {
						for (NameValuePair pair : mNameValuePair) {
							httpPost.setHeader(pair.getName(), pair.getValue());
							// Log.i("FileUploaderConnector", pair.getName() +
									// ": " + pair.getValue());
						}
					}

					InputStreamEntity reqEntity = new InputStreamEntity(
							new FileInputStream((new File(imagePath))), -1);
					reqEntity.setContentType(CONTENT_TYPE);
					reqEntity.setChunked(true);
					httpPost.setEntity(reqEntity);

					HttpResponse response = httpClient.execute(httpPost);
					mResponse = EntityUtils.toString(response
							.getEntity());
					Log.i("MIS", "" + mResponse);

					showLoader(false);
					mHandler.post(new Runnable() {

						@Override
						public void run() {
							uploadFinishedListener.uploadListener(mResponse);
						}
					});
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		new Thread(runnable).start();
	}

	private void showLoader(final boolean show) {
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if (show) {
					if (null != mLoaderView) {
						mLoaderView.setVisibility(View.VISIBLE);
					} else if (null != mDialog) {
						mDialog.show();
					}
				} else {
					if (null != mLoaderView) {
						mLoaderView.setVisibility(View.GONE);
					} else if (null != mDialog) {
						if (mDialog.isShowing()) {
							mDialog.dismiss();
						}
					}
				}
			}
		});
	}
}