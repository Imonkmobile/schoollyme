package com.vinfotech.server;

import org.json.JSONObject;

import android.content.Context;

import com.vinfotech.utility.JSONUtil;

/**
 * Utility class to generate HTTP request strings
 * 
 * @author gourav.jain
 * 
 */

// http://medlinx-dev.elasticbeanstalk.com/api/SignIn/
// https://postaplan-staging.s3.amazonaws.com/uploads/profile/2ba9217964d5a71d7ccc126bb2ed2aa5.jpg
public class HTTPRequest {
	
	
	public static final int CategoryList/*		*/=1;
	
	// public static final String SERVICE_URL =
	// "http://postaplan.vinfotechprojects.com/api/";
	// public static final String UPLOAD_URL =
	// "https://postaplan-staging.s3.amazonaws.com/uploads/";
	// public static final String IMAGE_URL =
	// "https://postaplan-staging.s3.amazonaws.com/uploads/";
	// public static final String IMAGE_PATH =
	// "https://postaplan-staging.s3.amazonaws.com/uploads/";
	 public static final String IMAGE_PATH_PROFILE ="";
	// "https://postaplan-staging.s3.amazonaws.com/uploads/profile/";
	// public static final String EVENT_BANNER_PATH =
	// "https://postaplan-staging.s3-us-west-1.amazonaws.com/uploads/eventbanner/1080x720/";
	// public static final String EVENT_BANNER_PATH_SMALL =
	// "https://postaplan-staging.s3-us-west-1.amazonaws.com/uploads/eventbanner/97x97/";
	 public static final String IMAGE_PATH_EVENT ="";
	// "https://postaplan-staging.s3.amazonaws.com/uploads/profilebanner/";

//	public static final String SERVICE_URL = "http://postaplan.vinfotechprojects.com/api/";
//	public static final String UPLOAD_URL = "http://postaplan.vinfotechprojects.com/uploads/";
//	public static final String IMAGE_URL = "http://postaplan.vinfotechprojects.com/uploads/";
//	public static final String IMAGE_PATH = "http://postaplan.vinfotechprojects.com/uploads/";
//	public static final String IMAGE_PATH_PROFILE = "http://postaplan.vinfotechprojects.com/uploads/profile/";
//	public static final String EVENT_BANNER_PATH = "http://postaplan.vinfotechprojects.com/uploads/eventbanner/1080x720/";
//	public static final String EVENT_BANNER_PATH_SMALL = "http://postaplan.vinfotechprojects.com/uploads/eventbanner/97x97/";
//	public static final String IMAGE_PATH_EVENT = "http://postaplan.vinfotechprojects.com/uploads/profilebanner/";
//
//	public static final String USER_TYPE_ID = "3";
//	public static final String DEVICE_TYPE = "AndroidPhone";
	public static final int URLTYPE_SERVICE = 1;
	public static final int URLTYPE_UPLOAD = 2;
	public static final int URLTYPE_IMAGE = 3;
	public static final int URLTYPE_EXTERNAL = 4;

	public static final int REQ_CODE_GET_FB_PROFILE /*					*/= 0;
	public static final int REQ_CODE_SIGNIN_SOCIAL /*					*/= 1;
	public static final int REQ_CODE_SIGNUP /*							*/= 2;
	


	/**
	 * the constructor
	 */
	public HTTPRequest(Context context) {
//		HTTPRequest.LoginSessionKey = new PostaPlanPreferences(context).getLoginSessionKey();
	}
	
	public static String getSocialSignUpJSON(String GPlusId,String FacebookId,String DeviceToken,String Password,String FirstName,String RenovateUrl,String LastName,String Email,String Zipcode) {
		JSONObject requestObject = null;
		requestObject = JSONUtil.getJSONObject("GPlusId", GPlusId, "FacebookId", FacebookId,"DeviceToken",DeviceToken,"Password",Password,"FirstName",FirstName,"RenovateUrl",RenovateUrl,"LastName",LastName,"Email",Email,"Zipcode",Zipcode);
		return null == requestObject ? "" : requestObject.toString();
	}
	
	public static String getSocialSignINJSON(String GPlusId,String FacebookId,String DeviceToken) {
		JSONObject requestObject = null;
		requestObject = JSONUtil.getJSONObject("GPlusId", GPlusId, "FacebookId", FacebookId,"DeviceToken",DeviceToken);
		return null == requestObject ? "" : requestObject.toString();
	}


	public static String getRecoverLoginJSON(String email) {

		JSONObject requestObject = null;
		requestObject = JSONUtil.getJSONObject("Value", email, "Type", "Url");

		return null == requestObject ? "" : requestObject.toString();
	}
}
