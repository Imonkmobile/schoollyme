package com.vinfotech.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.http.conn.util.InetAddressUtils;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.AssetFileDescriptor;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.R;

public class Utility {

	/**
	 * Hides the soft input keyboard
	 * 
	 * @param editText
	 *            the container edit text for which soft keyboard is shown
	 */
	public static void hideSoftKeyboard(EditText editText) {
		if (null != editText) {
			InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
		}
	}

	public static void hideSoftKeyboard(View view) {
		if (null != view) {
			InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	// Encripted Password
	public static final String md5(final String plainString) {
		try {
			// Create MD5 Hash
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(plainString.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < messageDigest.length; i++) {

				String h = Integer.toHexString(0xFF & messageDigest[i]);

				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}

			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return "";
	}

	// Declare the length of Mobile edit text
	public static void ShowLog(String text, Context context) {
		Log.i(context.toString(), text);
	}

	public static void ShowToast(String text, Context context) {
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	public static void showLog(String string) {
		Log.i("Utility", "VALUE:" + string);
	}

	public static String FromResouarceToString(Context mContext, int ID) {

		return mContext.getResources().getString(ID);
	}

	public static String generateKeyHash(Context mContext) {
		String key = "";
		try {

			PackageInfo info = mContext.getPackageManager().getPackageInfo("com.schollyme", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				key = Base64.encodeToString(md.digest(), Base64.DEFAULT);
				Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
		return key;
	}

	public static String getDeviceId(Context mContext) {
		return Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID);
	}

	public static String getDeviceMAC_ADDRESS(Context mContext) {
		WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wInfo = wifiManager.getConnectionInfo();
		String macAddress = wInfo.getMacAddress();
		return macAddress;
	}

	public static void textSpann(Context mContext, TextView textView, String normalText) {
		/*
		 * SpannableString text = new SpannableString(normalText);
		 * text.setSpan(new ForegroundColorSpan(R.color.blue), 46, 65,
		 * Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); textView.setText(text);
		 */
		SpannableString spannableText = new SpannableString(normalText);
		ForegroundColorSpan fcs = new ForegroundColorSpan(mContext.getResources().getColor(R.color.link_color));
		StyleSpan bss = new StyleSpan(Typeface.BOLD);
		spannableText.setSpan(fcs, 56, 69, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		spannableText.setSpan(bss, 56, 69, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		textView.setText(spannableText);
	}

	public static int getDateDifference(String selectedDate) {

		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		StringBuilder sb = (new StringBuilder()
		// Month is 0 based, just add 1
				.append(year).append("-").append(month + 1).append("-").append(day));
		String currentDate = sb.toString();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd");
		try {
			Date startDate = simpleDateFormat.parse(selectedDate);
			Date endDate = simpleDateFormat.parse(currentDate);

			// milliseconds
			long different = endDate.getTime() - startDate.getTime();

			long secondsInMilli = 1000;
			long minutesInMilli = secondsInMilli * 60;
			long hoursInMilli = minutesInMilli * 60;
			long daysInMilli = hoursInMilli * 24;

			long elapsedDays = different / daysInMilli;
			int diff = (int) (elapsedDays / 365);
			return diff;
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return 0;

	}

	public static String getIPAddress(boolean useIPv4) {
		try {
			List<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface networkInterface : networkInterfaces) {
				List<InetAddress> inetAddresses = Collections.list(networkInterface.getInetAddresses());
				for (InetAddress inetAddress : inetAddresses) {
					if (!inetAddress.isLoopbackAddress()) {
						String sAddr = inetAddress.getHostAddress().toUpperCase();
						boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
						if (useIPv4) {
							if (isIPv4)
								return sAddr;
						} else {
							if (!isIPv4) {
								// drop ip6 port suffix
								int delim = sAddr.indexOf('%');
								return delim < 0 ? sAddr : sAddr.substring(0, delim);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "";
	}

	public String getScreenSize(Context mContext) {
		String mDeviceDensity;
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int dens = displaymetrics.densityDpi;
		int mDeviceHeight = displaymetrics.heightPixels;
		int mDeviceWidth = displaymetrics.widthPixels;

		Log.v("Height - Width", "Height :" + mDeviceHeight + "  Width : " + mDeviceWidth + "Density : " + dens);

		if (dens <= 120) {
			mDeviceDensity = "Low";
		} else if (dens > 120 && dens <= 160) {
			mDeviceDensity = "Medium";
		} else {
			mDeviceDensity = "High";
		}
		return mDeviceDensity;
	}

	public static void setAnim(View view) {
		int height = view.getHeight();
		Animation animation = new TranslateAnimation(0, 0, -200, 0);
		animation.setDuration(300);
		view.startAnimation(animation);
	}
	
	public static String formateDateForScores(String date){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date mChatDate = sdf.parse(date);
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy");
			String nDate = sdf1.format(mChatDate);
			return nDate;
				/*String val = new DecimalFormat("00").format(mChatDate.getMonth()+1);
				int mMonth = mChatDate.getMonth()+1;
				StringBuilder time = new StringBuilder();
				time.append(mChatDate.getDate())
				.append("-")
				.append(""+val)
				.append("-")
				.append(mChatDate.getYear());
				return time.toString();*/


		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String parseDateForChatHeader(String chatDate) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date mChatDate = sdf.parse(chatDate);
			String todayDate = getToday("yyyy-MM-dd hh:mm:ss");
			Date today = sdf.parse(todayDate);

			String yesterday = getYesterdayDateString();
			Date yesterdayDate = sdf.parse(yesterday);
			int isToday = today.compareTo(mChatDate);
			int isYesterday = yesterdayDate.compareTo(mChatDate);
			if (isToday < 0) {
				return "Today";
			} else if (isYesterday < 1) {

				return "Yesterday";
			} else {
				String val = new DecimalFormat("00").format(mChatDate.getMonth() + 1);
				int mMonth = mChatDate.getMonth() + 1;
				StringBuilder time = new StringBuilder();
				time.append(mChatDate.getDate()).append("-").append("" + val).append("-").append(mChatDate.getYear());
				return time.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	private static boolean isDateIsYesterday(Date date) {
		Calendar mCalender = Calendar.getInstance();
		mCalender.add(Calendar.DATE, -1);
		Date yesterday = new Date(mCalender.getTimeInMillis());
		yesterday.setHours(0);
		yesterday.setMinutes(0);
		yesterday.setSeconds(0);
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		int result = yesterday.compareTo(date);
		if (result == 0) {
			return true;
		}
		return false;
	}

	public static String getToday(String format) {
		Date date = new Date();
		return new SimpleDateFormat(format).format(date);
	}

	private static String getYesterdayDateString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

	public static String getFormattedDate(String chatTimedate,Context mContext) {

		try {
			final SimpleDateFormat mSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date chatDate = null;
			chatDate = mSDF.parse(chatTimedate);

			long smsTimeInMilis = chatDate.getTime();
			Calendar smsTime = Calendar.getInstance();
			smsTime.setTimeInMillis(smsTimeInMilis);

			Calendar now = Calendar.getInstance();

			if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
				return  mContext.getString(R.string.today)/*"Today"
								 * + DateFormat.format(timeFormatString,
								 * smsTime)
								 */;
			} else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
				return mContext.getString(R.string.yesterday) /*"Yesterday"
									 * + DateFormat.format(timeFormatString,
									 * smsTime)
									 */;
			} else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
				return android.text.format.DateFormat.format("MMM dd, yyyy", smsTime) + " at "
						+ android.text.format.DateFormat.format("hh:mma", smsTime);
			} else
				return android.text.format.DateFormat.format("MMM dd, yyyy", smsTime) + " at "
						+ android.text.format.DateFormat.format("hh:mma", smsTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String convertUTCTOIST(String date) {
		try {
			String patternString = "yyyy-MM-dd hh:mm:ss";
			DateFormat utcFormat = new SimpleDateFormat(patternString);
			utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			DateFormat indianFormat = new SimpleDateFormat(patternString);
			TimeZone tz = TimeZone.getDefault();
			indianFormat.setTimeZone(TimeZone.getTimeZone(tz.getID())); // "Asia/Kolkata"
			Date timestamp = utcFormat.parse(date);
			String output = indianFormat.format(timestamp);
			return output;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	public static String getTimeFromDate(String chatDate) {
		String msgTime = "";
		try {
			SimpleDateFormat mSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date mDate = mSDF.parse(convertUTCTOIST(chatDate));

			String hours = new DecimalFormat("00").format(mDate.getHours());
			String minutes = new DecimalFormat("00").format(mDate.getMinutes());

			StringBuilder mBuilder = new StringBuilder();
			mBuilder.append(String.valueOf(hours));
			mBuilder.append(":");
			mBuilder.append(String.valueOf(minutes));

			Calendar now = Calendar.getInstance();
			int a = now.get(Calendar.AM_PM);
			if (a == Calendar.AM) {
				mBuilder.append(" am");
			} else {
				mBuilder.append(" pm");
			}
			msgTime = mBuilder.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msgTime;
	}

	public static String readFromClipboard(Context mContext) {
		int sdk = android.os.Build.VERSION.SDK_INT;
		if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
			android.text.ClipboardManager clipboard = (android.text.ClipboardManager) mContext.getSystemService(mContext.CLIPBOARD_SERVICE);
			return clipboard.getText().toString();
		} else {
			ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);

			// Gets the clipboard data from the clipboard
			ClipData clip = clipboard.getPrimaryClip();
			if (clip != null) {
				String text = null;
				String title = null;
				// Gets the first item from the clipboard data
				ClipData.Item item = clip.getItemAt(0);
				if (text == null) {
					text = coerceToText(mContext, item).toString();
				}

				return text;
			}
		}
		return "";
	}

	private static CharSequence coerceToText(Context context, ClipData.Item item) {
		// If this Item has an explicit textual value, simply return that.
		CharSequence text = item.getText();
		if (text != null) {
			return text;
		}

		// If this Item has a URI value, try using that.
		Uri uri = item.getUri();
		if (uri != null) {

			// First see if the URI can be opened as a plain text stream
			// (of any sub-type). If so, this is the best textual
			// representation for it.
			FileInputStream stream = null;
			try {
				// Ask for a stream of the desired type.
				AssetFileDescriptor descr = context.getContentResolver().openTypedAssetFileDescriptor(uri, "text/*", null);
				stream = descr.createInputStream();
				InputStreamReader reader = new InputStreamReader(stream, "UTF-8");

				// Got it... copy the stream into a local string and return it.
				StringBuilder builder = new StringBuilder(128);
				char[] buffer = new char[8192];
				int len;
				while ((len = reader.read(buffer)) > 0) {
					builder.append(buffer, 0, len);
				}
				return builder.toString();

			} catch (FileNotFoundException e) {
				// Unable to open content URI as text... not really an
				// error, just something to ignore.

			} catch (IOException e) {
				// Something bad has happened.
				Log.w("ClippedData", "Failure loading text", e);
				return e.toString();

			} finally {
				if (stream != null) {
					try {
						stream.close();
					} catch (IOException e) {
					}
				}
			}

			// If we couldn't open the URI as a stream, then the URI itself
			// probably serves fairly well as a textual representation.
			return uri.toString();
		}

		// Finally, if all we have is an Intent, then we can just turn that
		// into text. Not the most user-friendly thing, but it's something.
		Intent intent = item.getIntent();
		if (intent != null) {
			return intent.toUri(Intent.URI_INTENT_SCHEME);
		}

		// Shouldn't get here, but just in case...
		return "";
	}

	public static void copyToClipBoard(String text, Context mContext) {
		try {
			int sdk = android.os.Build.VERSION.SDK_INT;
			if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
				android.text.ClipboardManager clipboard = (android.text.ClipboardManager) mContext
						.getSystemService(mContext.CLIPBOARD_SERVICE);
				clipboard.setText(text);
			} else {
				ClipboardManager clipboard = (ClipboardManager) mContext
						.getSystemService(mContext.CLIPBOARD_SERVICE);
				ClipData clip = ClipData.newPlainText(mContext.getResources().getString(R.string.message),
						text);
				clipboard.setPrimaryClip(clip);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static SpannableString clickableTextSpan(Context mContext, String message1, String message2, String spannable1, String spannable2) {
		try {
			StringBuilder mBuilder = new StringBuilder();
			if (!TextUtils.isEmpty(spannable1)) {
				mBuilder.append(spannable1);
				mBuilder.append(" ");
			}
			if (!TextUtils.isEmpty(message1)) {
				mBuilder.append(message1);
				mBuilder.append(" ");
			}
			if (!TextUtils.isEmpty(spannable2)) {
				mBuilder.append(spannable2);
				mBuilder.append(" ");
			}
			if (!TextUtils.isEmpty(message2)) {
				mBuilder.append(message2);
			}
			ClickableSpan mClickableSpan = new ClickableSpan() {

				@Override
				public void onClick(View widget) {

				}
			};
			SpannableString mSpanString = new SpannableString(mBuilder.toString());
			ForegroundColorSpan fcs = new ForegroundColorSpan(mContext.getResources().getColor(R.color.lt_grey_text));
			StyleSpan bss = new StyleSpan(Typeface.BOLD);
			if (null != message1) {
				int startIndex = mSpanString.toString().indexOf(message1);
				int endIndex = message1.length();
				mSpanString.setSpan(fcs, startIndex, (startIndex + endIndex), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}
			if (null != message2) {
				int startIndex = mSpanString.toString().indexOf(message2);
				int endIndex = message2.length();
				mSpanString.setSpan(fcs, startIndex, (startIndex + endIndex), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}
			if (spannable1 != null) {
				int startIndex = mSpanString.toString().indexOf(spannable1);
				int endIndex = spannable1.length();
				mSpanString.setSpan(bss, startIndex, (startIndex + endIndex), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}
			if (spannable2 != null) {
				int startIndex = mSpanString.toString().indexOf(spannable2);
				int endIndex = spannable2.length();
				mSpanString.setSpan(bss, mSpanString.toString().indexOf(spannable2), (startIndex + endIndex),
						Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}

			return mSpanString;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public static SpannableString clickableTextSpanBlack(Context mContext, String message1, String message2, String spannable1, String spannable2) {
		try {
			StringBuilder mBuilder = new StringBuilder();
			if (!TextUtils.isEmpty(spannable1)) {
				mBuilder.append(spannable1);
				mBuilder.append(" ");
			}
			if (!TextUtils.isEmpty(message1)) {
				mBuilder.append(message1);
				mBuilder.append(" ");
			}
			if (!TextUtils.isEmpty(spannable2)) {
				mBuilder.append(spannable2);
				mBuilder.append(" ");
			}
			if (!TextUtils.isEmpty(message2)) {
				mBuilder.append(message2);
			}
			ClickableSpan mClickableSpan = new ClickableSpan() {

				@Override
				public void onClick(View widget) {

				}
			};
			SpannableString mSpanString = new SpannableString(mBuilder.toString());
			ForegroundColorSpan fcs = new ForegroundColorSpan(mContext.getResources().getColor(R.color.black));
			StyleSpan bss = new StyleSpan(Typeface.BOLD);
			if (null != message1) {
				int startIndex = mSpanString.toString().indexOf(message1);
				int endIndex = message1.length();
				mSpanString.setSpan(fcs, startIndex, (startIndex + endIndex), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}
			if (null != message2) {
				int startIndex = mSpanString.toString().indexOf(message2);
				int endIndex = message2.length();
				mSpanString.setSpan(fcs, startIndex, (startIndex + endIndex), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}
			if (spannable1 != null) {
				int startIndex = mSpanString.toString().indexOf(spannable1);
				int endIndex = spannable1.length();
				mSpanString.setSpan(bss, startIndex, (startIndex + endIndex), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}
			if (spannable2 != null) {
				int startIndex = mSpanString.toString().indexOf(spannable2);
				int endIndex = spannable2.length();
				mSpanString.setSpan(bss, mSpanString.toString().indexOf(spannable2), (startIndex + endIndex),
						Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}

			return mSpanString;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void setSpan(final TextView tv, final Context mContext) {
		if (tv.getText().toString().length() > 0) {
			String textData = tv.getText().toString();
			Spannable spantext = new SpannableString(textData);

			spantext.setSpan(new StyleSpan(Typeface.BOLD), 0, spantext.length(), 0);
			Typeface HelveticaNeueBoldItalic = Typeface.createFromAsset(mContext.getAssets(), "fonts/wearecolt - QuickDeath.ttf");

			spantext.setSpan(HelveticaNeueBoldItalic, 0, spantext.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

			tv.setText(spantext);
		}
	}

	public static String convertInchesToFeetAndInch(int inch, Context mContext) {
		String feetAndInch = "";
		if (inch == 0) {
			return feetAndInch;
		}
		try {
			int mFeet = inch / 12;
			int mInch = inch % 12;
			StringBuilder mBuilder = new StringBuilder();
			mBuilder.append(String.valueOf(mFeet));
			mBuilder.append(" ");
			mBuilder.append(mContext.getResources().getString(R.string.feet));
			mBuilder.append(" ");
			mBuilder.append(String.valueOf(mInch));
			mBuilder.append(" ");
			mBuilder.append(mContext.getResources().getString(R.string.inch));
			feetAndInch = mBuilder.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feetAndInch;
	}

	public static int convertFeetAndInchToInches(String selectedFeet, String selectedInch) {
		try {
			int mFeet = 0;
			int mInch = 0;
			if (!TextUtils.isEmpty(selectedFeet)) {
				String mformatedFeet = selectedFeet.replaceAll("[a-zA-Z]", "");
				mFeet = Integer.parseInt(mformatedFeet.trim());

			}
			if (!TextUtils.isEmpty(selectedInch)) {
				String mformatedInch = selectedInch.replaceAll("[a-zA-Z]", "");
				mInch = Integer.parseInt(mformatedInch.trim());
			}
			int mTotalInches = (mFeet * 12) + mInch;
			return mTotalInches;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static final String[] MONTHS = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	public static String getMOnthNameFromNumber(int month) {
		return MONTHS[month];
	}

 

	public static String formateDate(String date) {
		String formatedDate = "";
		try {
			SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
			Date mDate = formate.parse(date);
			SimpleDateFormat appFormate = new SimpleDateFormat("dd MMM, yyyy");
			formatedDate = appFormate.format(mDate);
			Log.i("Result", "mDate " + formatedDate);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return formatedDate;
	}

}
