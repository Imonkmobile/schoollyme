package com.vinfotech.utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;

public class ImageUtil {

	public static String encodeTobase64(Bitmap bitmap) {
		if (null == bitmap) {
			return null;
		}

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, 90, byteArrayOutputStream);
		byte[] bytes = byteArrayOutputStream.toByteArray();

		String bitmapString = Base64.encodeToString(bytes, Base64.DEFAULT);

		return bitmapString;
	}

	public static Bitmap decodeBase64(String bitmapString) {
		if (null == bitmapString) {
			return null;
		}

		byte[] bytes = Base64.decode(bitmapString, Base64.DEFAULT);

		return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
	}

	public static Bitmap merge(Bitmap bitmap, Bitmap overlay, int left, int top, int alpha) {
		if (null == bitmap || bitmap.getWidth() < 1 || bitmap.getHeight() < 1) {
			return bitmap;
		} else if (null == overlay || overlay.getWidth() < 1 || overlay.getHeight() < 1) {
			return bitmap;
		}

		Paint paint = new Paint();
		paint.setAntiAlias(true);

		Bitmap merged = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);

		Canvas canvas = new Canvas(merged);
		canvas.drawBitmap(bitmap, 0, 0, paint);
		paint.setAlpha(alpha);
		// overlay = getCircledBitmap(overlay, overlay.getWidth());
		canvas.drawBitmap(overlay, left, top, paint);

		return merged;
	}

	/**
	 * Convert Square to Circle Image
	 * 
	 * @param bitmap
	 *            the bitmap to be circled
	 * @return the bitmap
	 */
	public static Bitmap getCircledBitmap(Bitmap bitmap) {
		int minDim = bitmap.getWidth() < bitmap.getHeight() ? bitmap.getWidth() : bitmap.getHeight();
		Bitmap targetBitmap = Bitmap.createBitmap(minDim, minDim, Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(minDim / 2, minDim / 2, minDim / 2, Path.Direction.CCW);

		canvas.clipPath(path);
		canvas.drawBitmap(bitmap, new Rect(0, 0, minDim, minDim), new Rect(0, 0, minDim, minDim), null);
		return targetBitmap;
	}

	public static Bitmap getBitmap(String path) {
		if (null != path) {
			ImageDecoder imageDecoder = new ImageUtil().new ImageDecoder();
			return imageDecoder.getBitmap(path);
		}
		return null;
	}

	public static Bitmap getBitmap(String path, int maxWidth, int maxHeight) {
		if (null != path) {
			ImageDecoder imageDecoder = new ImageUtil().new ImageDecoder();
			return imageDecoder.getBitmap(path, maxWidth, maxHeight);
		}
		return null;
	}

	private class ImageDecoder {
		private BitmapFactory.Options options;
		private int inSampleSize;

		public ImageDecoder() {
			options = new BitmapFactory.Options();
			inSampleSize = 1;
		}

		public Bitmap getBitmap(final String path) {
			options.inSampleSize = inSampleSize;
			try {
				Bitmap bitmap = BitmapFactory.decodeFile(path, options);
				System.out.println("getBitmap inSampleSize=" + inSampleSize + ", width=" + bitmap.getWidth() + ", height="
						+ bitmap.getHeight());
				return bitmap;
			} catch (OutOfMemoryError e) {
				inSampleSize *= 2;
				return getBitmap(path);
			} catch (Exception e) {
				return null;
			}
		}

		public Bitmap getBitmap(final String path, int maxWidth, int maxHeight) {
			options.inSampleSize = inSampleSize;
			try {
				Bitmap bitmap = BitmapFactory.decodeFile(path, options);
				if (bitmap.getWidth() > maxWidth || bitmap.getHeight() > maxHeight) {
					inSampleSize *= 2;
					return getBitmap(path, maxWidth, maxHeight);
				}
				System.out.println("getBitmap inSampleSize=" + inSampleSize + ", width=" + bitmap.getWidth() + ", height="
						+ bitmap.getHeight());
				return bitmap;
			} catch (OutOfMemoryError e) {
				inSampleSize *= 2;
				return getBitmap(path);
			} catch (Exception e) {
				return null;
			}
		}
	}

	public static int getRotationAngle(String imagePath) {
		int rotate = 0;
		try {
			ExifInterface ei = new ExifInterface(imagePath);
			switch (ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0)) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;

			default:
				break;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return rotate;
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}

	public static File saveBitmap(Bitmap bitmap, File directory, String name) {
		if (null == bitmap) {
			System.err.println("saveBitmap-> bitmap is null");
			return null;
		} else if (null == directory) {
			System.err.println("saveBitmap-> directory is null");
			return null;
		} else if (null == name) {
			System.err.println("saveBitmap-> name is null");
			return null;
		}
		File file = new File(directory, name);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			bitmap.compress(CompressFormat.JPEG, 90, fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return file;
	}

	public static Bitmap createThumbnail(Bitmap bitmap, int maxDim) {
		if (null != bitmap) {
			float max = bitmap.getWidth() > bitmap.getHeight() ? bitmap.getWidth() : bitmap.getHeight();
			if (max > maxDim) {
				float scale = max / maxDim;
				float newWidth = bitmap.getWidth() / scale;
				float newHeight = bitmap.getHeight() / scale;
				return ThumbnailUtils.extractThumbnail(bitmap, (int) newWidth, (int) newHeight);
			}
		}
		return bitmap;
	}

	/**
	 * Use for image rotated 
	 * @param path
	 * @return
	 */
	public static boolean normalizeRotation(String path) {
		long ts = System.currentTimeMillis();
		if (null != path) {
			try {
				ExifInterface exif = new ExifInterface(path);
				int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

				int angle = 0;
				if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
					angle = 90;
				} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
					angle = 180;
				} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
					angle = 270;
				}

				if (0 != angle) {
					Matrix matrix = new Matrix();
					matrix.postRotate(angle);
					Bitmap bitmap = getBitmap(path, 2048, 2048);
					bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
					FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
					bitmap.compress(CompressFormat.JPEG, 85, fileOutputStream);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Log.d(ImageUtil.class.getSimpleName(), "normalizeRotation done in " + (System.currentTimeMillis() - ts) + "ms");
		return false;
	}

	public static Bitmap addBorderToBitmap(Bitmap bmp, int borderSize,int color) {
		Bitmap bmpWithBorder = Bitmap.createBitmap(bmp.getWidth() + borderSize * 2, bmp.getHeight() + borderSize * 2, bmp.getConfig());
		Canvas canvas = new Canvas(bmpWithBorder);
		canvas.drawColor(color);
		canvas.drawBitmap(bmp, borderSize, borderSize, null);
		return bmpWithBorder;
	}

	public static String getFileNameFromURI(Uri uri,Context mContext) {
		String result = null;
		if (uri.getScheme().equals("content")) {
			Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);
			try {
				if (cursor != null && cursor.moveToFirst()) {
					result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
				}
			} finally {
				cursor.close();
			}
		}
		if (result == null) {
			result = uri.getPath();
			int cut = result.lastIndexOf('/');
			if (cut != -1) {
				result = result.substring(cut + 1);
			}
		}
		return result;
	}
}