package com.vinfotech.utility;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

import com.schollyme.R;

public class CustomTextDrawable extends Drawable {

	private final String text;
	private final Paint paint;

	public CustomTextDrawable(String text,Context mContext) {
		this.text = text;
		this.paint = new Paint();
		paint.setColor(mContext.getResources().getColor(R.color.blue_fb));
		paint.setTextSize(28f);
		paint.setAntiAlias(true);
		
		paint.setTextAlign(Paint.Align.LEFT);
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawText(text, 0, 10, paint);
	}

	@Override
	public void setAlpha(int alpha) {
		paint.setAlpha(alpha);
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
		paint.setColorFilter(cf);
	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSLUCENT;
	}

}
