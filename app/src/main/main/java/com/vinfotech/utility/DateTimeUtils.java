package com.vinfotech.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;

public class DateTimeUtils {
	public static final String FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
	public static final String FORMAT_MIN = "mm";
	public static final String FORMAT_FULL_EVENT_DATE = "EEE, MMM dd,h:mm aa";
	public static final String FORMAT_FULL_EVENT_LIST_DATE = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_HALF_EVENT_LIST_DATE = "EEEE, hh:mm aa";

	@SuppressLint("SimpleDateFormat") 
	public static String getLongFromStringDate(String inDateString, String Format) {
		SimpleDateFormat inSdf = new SimpleDateFormat(FORMAT_HALF_EVENT_LIST_DATE);
		Date inDate;
		try {
			inDate = inSdf.parse(inDateString);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(inDate);
			return getStringFromLongDate(calendar.getTimeInMillis(), FORMAT_FULL_EVENT_DATE);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return "";
	}

	public static String getStringFromLongDate(long millisecond, String Format) {
		String dateString = DateFormat.format(Format, new Date(millisecond)).toString();
		return dateString;
	}

	/**
	 * 2015-01-15 11:06:00
	 */

	@SuppressLint("SimpleDateFormat")
	public static String getUTCDateFromLongDate(long millisecond) {
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(FORMAT_FULL_EVENT_LIST_DATE);
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		String utcDate = dateFormatGmt.format(new Date(millisecond));
		// Utility.showLog("UTC " + utcDate);
		return utcDate;
	}

	@SuppressLint("SimpleDateFormat") 
	public static String getDateFromUTC(String inDateString) {
		if (inDateString != null) {
			TimeZone timeZone = TimeZone.getDefault();
			SimpleDateFormat inSdf = new SimpleDateFormat(FORMAT_FULL_EVENT_LIST_DATE);
			inSdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date inDate;
			try {
				inDate = inSdf.parse(inDateString);
				SimpleDateFormat dateFormatGmt = new SimpleDateFormat(FORMAT_HALF_EVENT_LIST_DATE);
				dateFormatGmt.setTimeZone(timeZone);
				String utcConvertDate = dateFormatGmt.format(inDate);
				// Utility.showLog(inDateString + " UTCCnvertedDate " +
				// utcConvertDate);
				return utcConvertDate;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return "";
		}
		return "";
	}

}
