package com.vinfotech.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.Config;
import com.schollyme.R;

public class DialogUtil {
	private static final String TAG = DialogUtil.class.getSimpleName();

	public static void showOkDialog(Context context, String msg, String titleRes) {
		if (null == context || null == context || null == msg) {
			return;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (!titleRes.equals("")) {
			builder.setTitle(titleRes);
		}

		builder.setMessage(msg);
		builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.dismiss();
			}
		});
		builder.setCancelable(true);

		builder.show();
	}

	public static AlertDialog showOkDialogButtonLisnter(final Context context, String msg, String titleResId,
			final OnOkButtonListner mListener) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (!titleResId.equals("")) {
			builder.setTitle(titleResId);
		}

		builder.setMessage(msg);

		builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mListener.onOkBUtton();

			}
		});
		builder.setCancelable(true);

		return builder.show();
	}

	public static void showOkDialogNonCancelable(final Context context, String msg, String titleResId, final OnOkButtonListner mListener) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (!titleResId.equals("")) {
			builder.setTitle(titleResId);
		}

		builder.setMessage(msg);

		builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mListener.onOkBUtton();

			}
		});
		builder.setCancelable(false);

		builder.show();
	}

	public static void showOkDialogNonCancelableTwoButtons(final Context context, String msg, String titleResId,
			final OnOkButtonListner mListener) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (!titleResId.equals("")) {
			builder.setTitle(titleResId);
		}

		builder.setMessage(msg);

		builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mListener.onOkBUtton();

			}
		});

		builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});

		builder.setCancelable(false);

		builder.show();
	}

	public static Dialog showOkDialogUncancelableButtonLisnter(final Context context, String msg, String titleResId,
			final OnOkButtonListner mListener) {

		final AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (!titleResId.equals("")) {
			builder.setTitle(titleResId);
		}

		builder.setMessage(msg);

		builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mListener.onOkBUtton();

			}
		});
		builder.setCancelable(false);

		final Dialog mDialog = builder.create();
		mDialog.show();
		return mDialog;
	}

	public interface OnOkCancelButtonListner {

		public void onCancelBUtton();

		public void onOkBUtton();

	}

	public static Dialog showOkConcelDialogButtonLisnter(final Context context, String msg, String titleResId,
			final OnOkCancelButtonListner mListener) {

		final AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (!titleResId.equals("")) {
			builder.setTitle(titleResId);
		}

		builder.setMessage(msg);

		builder.setPositiveButton(R.string.Cancel, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mListener.onOkBUtton();

			}
		});

		builder.setNegativeButton(R.string.Continue, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mListener.onCancelBUtton();

			}
		});

		final Dialog mDialog = builder.create();
		mDialog.show();
		return mDialog;
	}

	public static void showOkCancelDialogButtonLisnter(final Context context, String msg, String titleResId,
			final OnOkButtonListner mListener, DialogInterface.OnCancelListener mCancelListener) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (!titleResId.equals("")) {
			builder.setTitle(titleResId);
		}
		builder.setMessage(msg);
		builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mListener.onOkBUtton();
			}
		});
		builder.setCancelable(true);
		builder.setOnCancelListener(mCancelListener);
		builder.show();
	}

	public static Dialog showYesNoDialogWithTittle(final Context context, String titleResId, String msg, OnClickListener mListener) {
		if (null == context || null == context || null == msg) {
			return null;
		}
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.setContentView(R.layout.yes_no_dialog);

		TextView titleTv = (TextView) dialog.findViewById(R.id.title_tv);
		if (titleResId.equals("")) {
			titleTv.setVisibility(View.GONE);
		} else
			titleTv.setText(titleResId);

		TextView messageTv = (TextView) dialog.findViewById(R.id.message_tv);
		messageTv.setText(msg);

		Button okBtn = (Button) dialog.findViewById(R.id.ok_btn);
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel_btn);
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		okBtn.setTag(dialog);
		okBtn.setOnClickListener(mListener);
		dialog.show();
		return dialog;
	}

	@SuppressLint("NewApi")
	public static void showListDialog(final Context context, int titleResId, final OnItemClickListener listener, String... items) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (0 != titleResId) {
			builder.setTitle(titleResId);
		}

		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
		arrayAdapter.addAll(items);

		builder.setCancelable(true);

		builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (null != listener) {
					listener.onCancel();
				}
				if (Config.DEBUG) {
					Log.d(TAG, "showListDialog.onClick which=" + which + ", canceled=true");
				}
				dialog.dismiss();
			}
		});

		builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				String item = arrayAdapter.getItem(which);
				if (null != listener) {
					listener.onItemClick(which, item);
				}
				if (Config.DEBUG) {
					Log.d(TAG, "showListDialog.onClick which=" + which + ", item=" + item);
				}
			}
		});
		builder.show();
	}

	@SuppressLint("NewApi")
	public static void showMediaDialog(final Context context, int titleResId, final OnItemClickListener listener, String[] items) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		if (0 != titleResId) {
			builder.setTitle(titleResId);
		}

		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
		arrayAdapter.addAll(items);

		builder.setCancelable(false);

		builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (null != listener) {
					listener.onCancel();
				}
				if (Config.DEBUG) {
					Log.d(TAG, "showListDialog.onClick which=" + which + ", canceled=true");
				}
				dialog.dismiss();
			}
		});

		builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				String item = arrayAdapter.getItem(which);
				if (null != listener) {
					listener.onItemClick(which, item);
				}
				if (Config.DEBUG) {
					Log.d(TAG, "showListDialog.onClick which=" + which + ", item=" + item);
				}
			}
		});
		builder.show();
	}

	public static ProgressDialog createDiloag(Context context, String message) {
		ProgressDialog progressDialog = new ProgressDialog(context);
		if (!TextUtils.isEmpty(message)) {
			progressDialog.setMessage(message);
		}
		progressDialog.setCanceledOnTouchOutside(false);
		return progressDialog;
	}

	public static void showOkCancelDialog(final Context context, String title, String message, final OnClickListener okListener,
			final OnClickListener cancelListener) {
		showOkCancelDialog(context, 0, title, message, okListener, cancelListener);
	}

	public static void showOkCancelDialog(final Context context, int okResId, String title, String message,
			final OnClickListener okListener, final OnClickListener cancelListener) {
		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		if (null != title) {
			adb.setTitle(title);
		}
		adb.setMessage(message);
		adb.setPositiveButton(okResId > 0 ? okResId : android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (null != okListener) {
					okListener.onClick(new View(context));
				}
			}
		});

		adb.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (null != cancelListener) {
					cancelListener.onClick(new View(context));
				}
			}
		});
		adb.show();
	}

	public static void showSingleEditDialog(final Context context, int hintResId, String existText, final SingleEditListener listener) {
		final int margin = (int) context.getResources().getDimension(R.dimen.space_large15);

		final AlertDialog.Builder alert = new AlertDialog.Builder(context);

		final EditText editText = new EditText(context);
		editText.setHeight(margin * 10);
		editText.setPadding(margin, margin, margin, margin);
		editText.setBackgroundColor(Color.TRANSPARENT);
		editText.setGravity(Gravity.TOP);
		editText.setHint(hintResId);
		editText.setText(existText);

		final boolean forWriteCaption = (hintResId == R.string.Write_caption);
		if (forWriteCaption) {
			editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
			editText.setHeight((int) context.getResources().getDimension(R.dimen.space_mid10) * 5);
			editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(30) });
		}
		alert.setView(editText);

		alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String text = editText.getText().toString().trim();
				if (TextUtils.isEmpty(text)) {
					Toast.makeText(context, "Please enter text", Toast.LENGTH_SHORT).show();
					if (null != listener) {
						listener.onEdit(true, editText, "");
					}
					return;
				}
				if (null != listener) {
					listener.onEdit(false, editText, text);
				}
				dialog.dismiss();
			}
		});

		alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				if (null != listener) {
					listener.onEdit(true, editText, "");
				}
				dialog.dismiss();
			}
		});

		alert.show();
	}

	public interface SingleEditListener {
		void onEdit(boolean canceled, EditText editText, String text);
	}

	public interface ShareItemClickListener {
		public void shareListener(String socialSiteName);

		public void onCancel();

	}

	public interface OnOkButtonListner {

		public void onOkBUtton();

	}

	public interface OnItemClickListener {
		void onItemClick(int position, String item);

		void onCancel();
	}

	public static Dialog createProgressDialog(Context context, int msgResId) {
		if (msgResId == 0) {
			WhiteProgressDialog dialog = new WhiteProgressDialog(context, msgResId);
			dialog.setCanceledOnTouchOutside(false);
			return dialog;
		} else {
			ProgressDialog mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage(context.getResources().getString(msgResId));
			return mProgressDialog;
		}
	}

	public static Dialog createProgressDialogDefaultWithMessage(Context context, int msgResId) {

		ProgressDialog dialog = new ProgressDialog(context);
		if (msgResId != 0) {

			dialog.setMessage(context.getResources().getString(msgResId));
		}
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		return dialog;
	}

	public static void HintViewer(final Context mContext, String msg, int imgId) {
		final Dialog mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
		LayoutInflater mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = mLayoutInflater.inflate(R.layout.hint_viewer, null);
		mDialog.setContentView(layout);
		ImageView hint_img = (ImageView) mDialog.findViewById(R.id.hint_img);
		TextView msg_tv = (TextView) mDialog.findViewById(R.id.msg_tv);

		hint_img.setImageResource(imgId);

		msg_tv.setText(msg);
		layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		FontLoader.setHandleeRegularTypeface(msg_tv);

		mDialog.show();
	}

	public static void showHintDialog(final Context context, int layoutId) {
		final Dialog mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
		LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = mLayoutInflater.inflate(layoutId, null);
		mDialog.setContentView(layout);
		layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});

		mDialog.show();
	}

	public static void showSODHintDialog(final Context context, boolean hint1Visible, boolean hint2Visible, boolean hint3Visible) {
		final Dialog mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
		LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = mLayoutInflater.inflate(R.layout.sod_coach_mark_dialog, null);

		layout.findViewById(R.id.hint_1_iv).setVisibility(hint1Visible ? View.VISIBLE : View.INVISIBLE);
		layout.findViewById(R.id.hint_2_iv).setVisibility(hint2Visible ? View.VISIBLE : View.INVISIBLE);
		layout.findViewById(R.id.hint_3_iv).setVisibility(hint3Visible ? View.VISIBLE : View.INVISIBLE);

		mDialog.setContentView(layout);
		layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});

		mDialog.show();
	}
}
