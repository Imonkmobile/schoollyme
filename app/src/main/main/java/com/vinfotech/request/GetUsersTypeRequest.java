package com.vinfotech.request;

import java.util.ArrayList;

import org.json.JSONArray;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.model.UserTypeModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;

public class GetUsersTypeRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = GetUsersTypeRequest.class.getSimpleName();

	private static final int REQ_CODE_GET_USERS_TYPE = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public GetUsersTypeRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getUsersTypeServerRequest() {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already Login....");
			}
			return;
		}

		mRequesting = true;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("signup/GetUserTypes/", REQ_CODE_GET_USERS_TYPE, "post", true, null, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_GET_USERS_TYPE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					JSONArray jArray = getJsonArray("Data");
					ArrayList<UserTypeModel> mModelAL = new ArrayList<UserTypeModel>();
					for(int i=0;i<jArray.length();i++){
						UserTypeModel model = new UserTypeModel(jArray.optJSONObject(i));
						mModelAL.add(model);
					}
					mRequestListener.onComplete(true, mModelAL, 0);
				}
			} 
			else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed  Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("GetUserTypes",json) && isSuccess()) {
			
			return true;
		}
		return false;
	}
}