package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.model.SignUpModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class SocialSignInRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = SocialSignInRequest.class.getSimpleName();
	private static final int REQ_CODE_SIGNIN = 1;
	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	

	private SignUpModel mSignUpModule;

	public SocialSignInRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void socialSignInSever(String GPlusId,String FacebookId,String DeviceToken) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already Login....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSocialSignINJSON(GPlusId, FacebookId, DeviceToken);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("UsersApi/CheckLoginStatus/", REQ_CODE_SIGNIN, "post", true, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	
	public static String getSocialSignINJSON(String GPlusId,String FacebookId,String DeviceToken) {
		JSONObject requestObject = null;
		requestObject = JSONUtil.getJSONObject("GPlusId", GPlusId, "FacebookId", FacebookId,"DeviceToken",DeviceToken);
		return null == requestObject ? "" : requestObject.toString();
	}


	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_SIGNIN:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "SignIn Details=" + mSignUpModule);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mSignUpModule, 0);
				}
			} else {
				if (Config.DEBUG) {
					Log.e("", "Failed to SignUp. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {
	}

	@Override
	public void onProgressChange(int progress) {
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("CheckLoginStatus", json) && isSuccess()) {
			mSignUpModule = new SignUpModel(getDataObject());
			mSignUpModule.persist(mContext);
			return true;
		}
		return false;
	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
	}

}
