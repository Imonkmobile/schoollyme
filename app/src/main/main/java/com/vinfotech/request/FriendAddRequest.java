package com.vinfotech.request;

import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.schollyme.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class FriendAddRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = FriendAddRequest.class.getSimpleName();

	private static final int REQ_CODE_ADD_FRIEND = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public FriendAddRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void AddFriendServerRequest(String FriendGUID,String TeamMate) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}
		mRequesting = true;
		String jsonData = getAddFriendJson(FriendGUID,TeamMate);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("friends/acceptFriend/", REQ_CODE_ADD_FRIEND, "post", true, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getAddFriendJson(String FriendGUID,String TeamMate) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey",getLoginSessionKey(),"FriendGUID",FriendGUID,"TeamMate",TeamMate);
		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ADD_FRIEND:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} 
			else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to response. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("friends/acceptFriend/",json) && isSuccess()) {
			
			return true;
		}
		return false;
	}
}