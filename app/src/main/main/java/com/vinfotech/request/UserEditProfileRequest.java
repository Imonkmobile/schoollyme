package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class UserEditProfileRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = UserEditProfileRequest.class.getSimpleName();

	private static final int REQ_CODE_USER_UPDATE_PROFILE = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public UserEditProfileRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getProfileServerRequest(String Email,String AboutUs,String LastName,String FirstName,String ZipCode,String AccessToken) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already requesting....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getProfileJson(Email,AboutUs,LastName,FirstName,ZipCode,AccessToken);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("UsersApi/UpdateUserInfo/", REQ_CODE_USER_UPDATE_PROFILE, "post", true, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getProfileJson(String Email,String AboutUs,String LastName,String FirstName,String ZipCode,String AccessToken) {
		JSONObject jsonObj = JSONUtil.getJSONObject("Email", Email,"AboutUs",AboutUs,"LastName",LastName,"FirstName",FirstName,"ZipCode",ZipCode,"AccessToken",AccessToken);
		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_USER_UPDATE_PROFILE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to SignIn. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("UpdateUserInfo", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}