package com.vinfotech.request;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.Song;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class SongOfTheDayRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = SongOfTheDayRequest.class.getSimpleName();

	private static final int REQ_CODE_SONG_OF_DAY = 112;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private Song mSong;
	private String mAccessToken;

	public SongOfTheDayRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_SONG_OF_DAY, 0, mMessage);
			}
		});
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	public void getSongOfDayFromServer(String UserGUID) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting song of the day...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSongOfDayJSON(UserGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("song/get_song_of_day", REQ_CODE_SONG_OF_DAY, "post", false, jsonData, null, UrlType.SERVICE);
	}

	private String getSongOfDayJSON(String UserGUID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey());
		if (!TextUtils.isEmpty(UserGUID)) {
			try {
				jsonObj.put("UserGUID", UserGUID);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_SONG_OF_DAY:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Song of the day get successful: " + mSong);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mSong, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get song of the day. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("get_song_of_day", json) && isSuccess()) {
			JSONArray jsonArray = getDataArray();
			if(null != jsonArray && jsonArray.length() == 0){
				mSong = new Song(getDataObject());
				return true;
			}
			JSONObject jsonObject = getDataObject();
			if (null != jsonObject) {
				mSong = new Song(getDataObject());
				return true;
			}
		}
		return false;
	}

	public String getAccessToken() {
		return mAccessToken;
	}
}
