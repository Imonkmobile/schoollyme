package com.vinfotech.request;

import java.util.List;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.Teams;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class TeamListRequest extends BaseRequest implements HttpResponseListener {

	private static final String TAG = TeamListRequest.class.getSimpleName();
	private static final int REQ_CODE_TEAM_LIST = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private List<Teams> mTeamList;

	public TeamListRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_TEAM_LIST, 0, mMessage);
			}
		});
	}

	public void TeamListServerRequest(int PageNo,String SearchText,String CategoryID) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}
		mRequesting = true;
		String jsonData = getTeamListJson(PageNo,SearchText,CategoryID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("group/list/", REQ_CODE_TEAM_LIST, "post", true, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getTeamListJson(int PageNo,String SearchText,String CategoryID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(),"PageNo",String.valueOf(PageNo),"PageSize",String.valueOf(Config.PAGE_SIZE),
				"SearchText",SearchText,"CategoryID",CategoryID,"OrderBy","GroupName");
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_TEAM_LIST:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + mTeamList);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mTeamList, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to response. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("group/list/", json) && isSuccess()) {
			mTeamList = Teams.getTeams(getDataArray());
			return true;
		}
		return false;
	}
}