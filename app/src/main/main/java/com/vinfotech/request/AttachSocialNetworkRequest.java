package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class AttachSocialNetworkRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = AttachSocialNetworkRequest.class.getSimpleName();
	private static final int REQ_CODE_ATTACH_NETWORK = 1;
	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	

	public AttachSocialNetworkRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void socialSignInSever(String GPlusId,String FacebookId,String accessToken,String Action) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already added....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAttachDetachJSON(GPlusId, FacebookId, accessToken,Action);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("UsersApi/AttachDetach/", REQ_CODE_ATTACH_NETWORK, "post", true, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	
	public static String getAttachDetachJSON(String GPlusId,String FacebookId,String AccessToken,String Action) {
		JSONObject requestObject = null;
		requestObject = JSONUtil.getJSONObject("GplusId", GPlusId, "FacebookId", FacebookId,"AccessToken",AccessToken,"Action",Action);
		return null == requestObject ? "" : requestObject.toString();
	}


	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ATTACH_NETWORK:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Response Details=" + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				if (Config.DEBUG) {
					Log.e("", "Failed to SignUp. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {
	}

	@Override
	public void onProgressChange(int progress) {
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("AttachDetach", json) && isSuccess()) {
			
			return true;
		}
		return false;
	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
	}

}
