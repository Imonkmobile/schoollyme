package com.vinfotech.request;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.NewsFeed;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class SharePostRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = SharePostRequest.class.getSimpleName();

	public static final String ENTITYTYPE_ACTIVITY = "Activity";
	public static final String ENTITYTYPE_MEDIA = "Media";

	public static final int MODULE_ID_GROUPS = 1;
	public static final int MODULE_ID_USERS = 3;
	public static final int MODULE_ID_EVENT = 14;

	public static final int VISIBILITY_EVERYONE = 1;
	public static final int VISIBILITY_TEAMMATES = 3;

	public static final int COMMENT_OFF = 0;
	public static final int COMMENT_ON = 1;

	private static final int REQ_CODE_COMMENT_LIST = 10;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private List<NewsFeed> mNewsFeeds;

	public SharePostRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_COMMENT_LIST, 0, mMessage);
			}
		});
	}

	public void sharePostInServer(String EntityGUID, String EntityType, String PostContent, int ModuleID, String ModuleEntityGUID,
			int Visiblity, int Commentable) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already sharing post...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSharePostJSON(EntityGUID, EntityType, PostContent, ModuleID, ModuleEntityGUID, Visiblity, Commentable);
		mHttpConnector.setDialog(DialogUtil.createProgressDialogDefaultWithMessage(mContext, R.string.loading));
		mHttpConnector.executeAsync("activity/sharePost", REQ_CODE_COMMENT_LIST, "post", false, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getSharePostJSON(String EntityGUID, String EntityType, String PostContent, int ModuleID, String ModuleEntityGUID,
			int Visiblity, int Commentable) {
		JSONObject jsonObject = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "EntityGUID", EntityGUID, "EntityType",
				EntityType, "ModuleID", Integer.toString(ModuleID), "ModuleEntityGUID", ModuleEntityGUID, "Visibility",
				Integer.toString(Visiblity), "Commentable", Integer.toString(Commentable));
		if (!TextUtils.isEmpty(PostContent)) {
			try {
				jsonObject.put("PostContent", PostContent);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return null == jsonObject ? "" : jsonObject.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_COMMENT_LIST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "Share post successful totalRecords=" + totalRecords + ", mNewsFeeds=" + mNewsFeeds);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mNewsFeeds, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to share post. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("sharePost", json) && isSuccess()) {
			mNewsFeeds = NewsFeed.getNewsFeeds(getDataArray());
			return true;
		}
		return false;
	}
}
