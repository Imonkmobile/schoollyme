package com.vinfotech.request;

import java.util.List;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.schollyme.Config;
import com.schollyme.model.PageMemberUser;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class SearchFriendsForGroupRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = SearchFriendsForGroupRequest.class.getSimpleName();

	private static final int REQ_CODE_SEARCH_USER_FOR_PAGE = 12;

	private HttpConnector mHttpConnector;
	private Context mContext;

	private List<PageMemberUser> mPageInviteMemberUsers;

	public SearchFriendsForGroupRequest(Context context) {
		this.mContext = context;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getUserListInServer(String GroupGUID, String SearchKey) {
		Log.v(TAG, "GroupGUID=" + GroupGUID + ", SearchKey =" + SearchKey );
		if (!mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting user list...");
			}
			return;
		}

		String jsonData = getUserListJSON(GroupGUID, SearchKey);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("group/friend_suggestion/", REQ_CODE_SEARCH_USER_FOR_PAGE, "post", false, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getUserListJSON(String GroupGUID, String SearchKey) {
		JSONObject jsonObject = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "GroupGUID", GroupGUID,"SearchKey",SearchKey);
		
		return null == jsonObject ? "" : jsonObject.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_SEARCH_USER_FOR_PAGE:
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "User list successful totalRecords=" + totalRecords + ", mFriendModels=" + mPageInviteMemberUsers);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mPageInviteMemberUsers, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get user list. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		
		if (super.parse("group/friend_suggestion/", json) && isSuccess()) {

			mPageInviteMemberUsers = PageMemberUser.getPageInviteMembers(getDataArray());
			return true;
		}
		return false;
	}
}