package com.vinfotech.request;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.model.FilterBy;
import com.schollyme.model.FriendModel;
import com.schollyme.model.SportsModel;
import com.schollyme.model.StateModel;
import com.schollyme.model.UserModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class SearchUserRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = SearchUserRequest.class.getSimpleName();

	private static final int REQ_CODE_SEARCH_USER = 11;

	private HttpConnector mHttpConnector;
	private Context mContext;

	private List<FriendModel> mFriendModels;

	public SearchUserRequest(Context context) {
		this.mContext = context;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getUserListInServer(FilterBy filterBy, int PageNo) {
		Log.v(TAG, "getCommentListInServer PageNo=" + PageNo + ", " + filterBy + ", mActivityLive=" + mActivityLive);
		if (!mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting user list...");
			}
			return;
		}

		String jsonData = getUserListJSON(filterBy, PageNo);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("search/user", REQ_CODE_SEARCH_USER, "post", false, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getUserListJSON(FilterBy filterBy, int PageNo) {
		JSONObject jsonObject = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "Offset", Integer.toString(PageNo),
				"Limit", Integer.toString(Config.PAGE_SIZE));
		if (!TextUtils.isEmpty(filterBy.getSearchKeyword())) {
			try {
				jsonObject.put("SearchKeyword", filterBy.getSearchKeyword());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (!TextUtils.isEmpty(filterBy.getType())) {
			try {
				jsonObject.put("Type", filterBy.getType());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (filterBy.getSportsModels().size() > 0) {
			try {
				jsonObject.put("SportsID", SportsModel.toJSONArray(filterBy.getSportsModels()));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (filterBy.getStateModels().size() > 0) {
			try {
				jsonObject.put("StateID", StateModel.toJSONArray(filterBy.getStateModels()));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (!TextUtils.isEmpty(filterBy.getGender())) {
			try {
				jsonObject.put("Gender", filterBy.getGender());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (!TextUtils.isEmpty(filterBy.getUserTypeId())) {
			try {
				jsonObject.put("UserTypeID", filterBy.getUserTypeId());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (!TextUtils.isEmpty(filterBy.getGPA())) {
			try {
				jsonObject.put("GPA", filterBy.getGPA());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (!TextUtils.isEmpty(filterBy.getSATScore())) {
			try {
				jsonObject.put("SATScore", filterBy.getSATScore());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if (!TextUtils.isEmpty(filterBy.getACTScore())) {
			try {
				jsonObject.put("ACTScore", filterBy.getACTScore());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return null == jsonObject ? "" : jsonObject.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_SEARCH_USER:
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "User list successful totalRecords=" + totalRecords + ", mFriendModels=" + mFriendModels);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mFriendModels, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get user list. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		
		if (super.parse("search/user", json) && isSuccess()) {

			mFriendModels = FriendModel.getFriendModels(getDataArray());
			return true;
		}
		return false;
	}
}
