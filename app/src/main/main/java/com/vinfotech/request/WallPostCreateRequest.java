package com.vinfotech.request;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.model.NewMedia;
import com.schollyme.model.NewYoutubeUrl;
import com.schollyme.model.NewsFeed;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class WallPostCreateRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = WallPostCreateRequest.class.getSimpleName();

	public static final int VISIBILITY_PUBLIC = 1;
	public static final int VISIBILITY_TEAMMATES = 3;

	public static final int MODULE_ID_GROUPS = 1;
	public static final int MODULE_ID_USERS = 3;
	public static final int MODULE_ID_EVENT = 14;

	public static final int COMMENT_OFF = 0;
	public static final int COMMENT_ON = 1;

	private static final int REQ_CODE_MEDIA_UPDATE = 3;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private NewsFeed mNewsFeed;

	public WallPostCreateRequest(Context mContext) {
		this.mContext = mContext;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void createWallPostInServer(String PostContent, int ModuleID, String ModuleEntityGUID, int Visibility, int Commentable,
			boolean MyBuzz, List<NewMedia> newMedias, List<NewYoutubeUrl> newYoutubeUrls) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already creating wall post...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getWallCreateJSON(PostContent, ModuleID, ModuleEntityGUID, Visibility, Commentable, MyBuzz, newMedias,
				newYoutubeUrls);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("wallpost/createWallPost", REQ_CODE_MEDIA_UPDATE, "post", true, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getWallCreateJSON(String PostContent, int ModuleID, String ModuleEntityGUID, int Visibility, int Commentable,
			boolean MyBuzz, List<NewMedia> newMedias, List<NewYoutubeUrl> newYoutubeUrls) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "PostContent", PostContent, "ModuleID",
				Integer.toString(ModuleID), "ModuleEntityGUID", ModuleEntityGUID, "Visibility", Integer.toString(Visibility),
				"Commentable", Integer.toString(Commentable), "MyBuzz", (MyBuzz ? "1" : "0"));

		if (null != newMedias) {
			try {
				jsonObj.put("Media", NewMedia.getJSONArray(newMedias));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (null != newYoutubeUrls) {
			try {
				jsonObj.put("Youtube", NewYoutubeUrl.getJSONArray(newYoutubeUrls));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_MEDIA_UPDATE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Wall post create successful: " + mNewsFeed);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mNewsFeed, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to create wall post. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("createWallPost", json) && isSuccess()) {
			JSONArray jsonArray = getDataArray();
			if (null != jsonArray && jsonArray.length() > 0) {
				mNewsFeed = new NewsFeed(jsonArray.optJSONObject(0));
				return true;
			}
		}
		return false;
	}
}
