package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class NotificationChangeSeenStatusRequest extends BaseRequest implements
		HttpResponseListener {
	public static final String TAG = NotificationChangeSeenStatusRequest.class.getSimpleName();

	private static final int REQ_CODE_NOTIFICATION_CHANGE_SEEN_STATUS_REQUEST = 1;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	public NotificationChangeSeenStatusRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_NOTIFICATION_CHANGE_SEEN_STATUS_REQUEST, 0, mMessage);
			}
		});
	}

	public void getNotificationSeenStatusServerRequest() {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already requested...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getNotificationSeenStatusJSON();
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext,
				mMsgResId));
		mHttpConnector.executeAsync("notifications/mark_as_seen/",REQ_CODE_NOTIFICATION_CHANGE_SEEN_STATUS_REQUEST, "post", true, jsonData, null,
				UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getNotificationSeenStatusJSON() {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey",getLoginSessionKey());
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_NOTIFICATION_CHANGE_SEEN_STATUS_REQUEST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "notifications/mark_as_seen");
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(),
							totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "notifications/mark_as_seen"+ getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("notifications/mark_as_seen/", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}