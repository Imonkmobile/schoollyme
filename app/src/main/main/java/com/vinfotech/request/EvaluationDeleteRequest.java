package com.vinfotech.request;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class EvaluationDeleteRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = EvaluationDeleteRequest.class.getSimpleName();

	private static final int REQ_CODE_EVALUATION_DELETE = 23;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	public EvaluationDeleteRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_EVALUATION_DELETE, 0, mMessage);
			}
		});
	}

	public void deleteEvaluationServer(String AlbumGUID) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already deleting album...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getEvaluationDeleteJSON(AlbumGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("evaluation/delete/", REQ_CODE_EVALUATION_DELETE, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getEvaluationDeleteJSON(String EvaluationGUID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey());
		try {
			JSONArray jArray = new JSONArray();
			
			jArray.put(new JSONObject().put("EvaluationGUID", EvaluationGUID));
			jsonObj.put("Evaluation", jArray);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_EVALUATION_DELETE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Evaluation delete successful: " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to delete Evaluation. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("evaluation/delete/", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}
