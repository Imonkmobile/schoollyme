package com.vinfotech.request;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.utility.JSONUtil;

public class UploadImageRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = UploadImageRequest.class.getSimpleName();
	public static final String MODULE_NAME_PROFILE_PICTURE = "ProfilePicture";
	public static final String MODULE_NAME_BLOG_IMAGE = "Media";

	private static final int REQ_CODE_UPLOAD_IMAGE = 5;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private String mPictureUrl;

	public UploadImageRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void uploadSingleImageInServer(Bitmap bitmap, String imageName, String moduleName) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already uploading IMAGE...");
			}
			return;
		}
		if (null == bitmap || bitmap.getWidth() < 10 || bitmap.getHeight() < 10) {
			if (Config.DEBUG) {
				Log.v(TAG, "Invalid bitmap...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getUploadImageJSON(ImageUtil.encodeTobase64(bitmap), imageName, moduleName);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("UploadSingleImage/", REQ_CODE_UPLOAD_IMAGE, "post", true, jsonData, null, UrlType.SERVICE);
	}

	public void uploadSingleImageInServer(File file, String imageName, String moduleName) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already uploading file...");
			}
			return;
		}
		if (null == file || !file.exists()) {
			if (Config.DEBUG) {
				Log.v(TAG, "Invalid file...");
			}
			return;
		}

		mRequesting = true;
		ArrayList<BasicNameValuePair> extraFormData = getUploadImageParams(file.getPath(), imageName, moduleName);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("UploadSingleImage/", REQ_CODE_UPLOAD_IMAGE, "post", true, null, extraFormData, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getUploadImageJSON(String imageString, String imageName, String moduleName) {
		JSONObject jsonObj = JSONUtil.getJSONObject("loginSessionKey", getLoginSessionKey(), "imageString", imageString, "imageName",
				imageName, "moduleName", moduleName);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	private ArrayList<BasicNameValuePair> getUploadImageParams(String imageString, String imageName, String moduleName) {
		ArrayList<BasicNameValuePair> extraFormData = new ArrayList<BasicNameValuePair>();
		extraFormData.add(new BasicNameValuePair("loginSessionKey", getLoginSessionKey()));
		extraFormData.add(new BasicNameValuePair("imageString", imageString));
		extraFormData.add(new BasicNameValuePair("imageName", imageName));
		extraFormData.add(new BasicNameValuePair("moduleName", moduleName));
		return extraFormData;

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {
		switch (reqCode) {
		case REQ_CODE_UPLOAD_IMAGE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Image upload successful: " + mPictureUrl);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mPictureUrl, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to upload Image. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {
		// Handling Error Messages
		if (Config.DEBUG) {
			Log.e(TAG, "Upload image cancled. Error: " + getMessage());
		}
		if (null != mRequestListener) {
			mRequestListener.onComplete(false, "Error uploading media.", 0);
		}
	}

	@Override
	public void onProgressChange(int progress) {

		System.out.println("ZZZZZZZZ== " + progress);

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("UploadSingleImage", json) && isSuccess()) {
			JSONObject jsonObject = getDataObject();
			if (null != jsonObject) {
				mPictureUrl = jsonObject.optString("pictureURLLarge", "");
				return true;
			}
		}
		return false;
	}
}
