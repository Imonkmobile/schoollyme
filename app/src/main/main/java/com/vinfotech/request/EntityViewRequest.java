package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class EntityViewRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = EntityViewRequest.class.getSimpleName();

	private static final int REQ_CODE_ENTITY_VIEW = 15;
	public static final String ENTITY_TYPE_USER = "USER";
	public static final String ENTITY_TYPE_MEDIA = "Media";
	public static final String ENTITY_TYPE_GROUP = "GROUP";
	public static final String ENTITY_TYPE_PAGE = "PAGE";
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	public EntityViewRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_ENTITY_VIEW, 0, mMessage);
			}
		});
	}

	public void setViewAtServer(String EntityGUID, String EntityType) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already incrementing count...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getEntityViewJSON(EntityGUID, EntityType);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("log", REQ_CODE_ENTITY_VIEW, "post", true, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getEntityViewJSON(String EntityGUID, String EntityType) {

		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "EntityGUID", EntityGUID, "EntityType",
				EntityType);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ENTITY_VIEW:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Setting View Successfull: " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, json, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Setting View Successfull. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("log", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}
