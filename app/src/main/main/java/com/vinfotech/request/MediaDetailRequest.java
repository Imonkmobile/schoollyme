package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.model.AlbumMedia;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class MediaDetailRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = MediaDetailRequest.class.getSimpleName();

	private static final int REQ_CODE_MEDIA_DETAIL = 12;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private AlbumMedia mAlbumMedia;

	public MediaDetailRequest(Context mContext) {
		this.mContext = mContext;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getMediaDetailInServer(String AlbumGUID, String MediaGUID) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting media detail...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getMediaDetailJSON(AlbumGUID, MediaGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/media_details", REQ_CODE_MEDIA_DETAIL, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getMediaDetailJSON(String AlbumGUID, String MediaGUID) {
		JSONObject jsonObj = JSONUtil
				.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumGUID", AlbumGUID, "MediaGUID", MediaGUID);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_MEDIA_DETAIL:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Media detail successful: " + mAlbumMedia);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAlbumMedia, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get media detail. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("media_details", json) && isSuccess()) {
			mAlbumMedia = new AlbumMedia(getDataObject());
			return true;
		}
		return false;
	}
}
