package com.vinfotech.request;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.CommentModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class CommentListRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = CommentListRequest.class.getSimpleName();

	public static final String ENTITYTYPE_ACTIVITY = "Activity";
	public static final String ENTITYTYPE_MEDIA = "Media";
	public static final String ENTITYTYPE_BLOG = "Blog";
	private static final int REQ_CODE_COMMENT_LIST = 10;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private boolean mRunInBg = false;

	private List<CommentModel> mCommentsList;

	public CommentListRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_COMMENT_LIST, 0, mMessage);
			}
		});
	}

	public void getCommentListInServer(String ActivityGUID, String EntityType, int PageNo) {
		Log.v(TAG, "getCommentListInServer ActivityGUID=" + ActivityGUID + ", EntityType=" + EntityType + ", PageNo=" + PageNo
				+ ", mRequesting=" + mRequesting + ", mActivityLive=" + mActivityLive);
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting comment list...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getCommentListJSON(ActivityGUID, EntityType, PageNo);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("activity/getAllComments", REQ_CODE_COMMENT_LIST, "post", mRunInBg, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
		mRunInBg = (null == view);
	}

	private String getCommentListJSON(String ActivityGUID, String EntityType, int PageNo) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "ActivityGUID", ActivityGUID, "EntityType",
				EntityType, "PageNo", Integer.toString(PageNo), "PageSize", Integer.toString(Config.PAGE_SIZE));

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_COMMENT_LIST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "Comment list successful totalRecords=" + totalRecords + ", mComments=" + mCommentsList);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mCommentsList, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get comment list. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("getAllComments", json) && isSuccess()) {

			mCommentsList = CommentModel.getComments(getDataArray());
			return true;
		}
		return false;
	}
}
