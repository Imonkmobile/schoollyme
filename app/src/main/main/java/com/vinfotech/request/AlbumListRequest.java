package com.vinfotech.request;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.Album;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class AlbumListRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = AlbumListRequest.class.getSimpleName();

	public static final String SORT_BY_ALBUMNAME = "AlbumName";
	public static final String SORT_BY_CREATEDDATE = "CreatedDate";
	public static final String ORDER_BY_ASC = "ASC";
	public static final String ORDER_BY_DESC = "DESC";
	public static final String ALBUM_TYPE_PHOTO = "PHOTO";
	public static final String ALBUM_TYPE_VIDEO = "VIDEO";

	private static final int REQ_CODE_ALBUM_LIST = 1;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private boolean mRunInBg = false;

	private List<Album> mAlbums;

	public AlbumListRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_ALBUM_LIST, 0, mMessage);
			}
		});
	}

	public void getAlbumListInServer(String UserGUID, String AlbumType, int PageNo, String SortBy, String OrderBy) {
		Log.v(TAG, "getAlbumListInServer UserGUID=" + UserGUID + ", AlbumType=" + AlbumType + ", PageNo=" + PageNo + ", mRequesting="
				+ mRequesting + ", mActivityLive=" + mActivityLive + ", SortBy=" + SortBy + ", OrderBy=" + OrderBy);
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting album list...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAlbumListJSON(UserGUID, AlbumType, PageNo, SortBy, OrderBy);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/list", REQ_CODE_ALBUM_LIST, "post", mRunInBg, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
		mRunInBg = (null == view);
	}

	private String getAlbumListJSON(String UserGUID, String AlbumType, int PageNo, String SortBy, String OrderBy) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "UserGUID", UserGUID, "AlbumType", AlbumType,
				"PageNo", Integer.toString(PageNo), "PageSize", Integer.toString(Config.PAGE_SIZE), "SortBy", SortBy, "OrderBy", OrderBy);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ALBUM_LIST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "Album list successful totalRecords=" + totalRecords + ", mAlbums=" + mAlbums);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAlbums, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get album list. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("album", json) && isSuccess()) {
			mAlbums = Album.getAlbums(getDataArray());
			return true;
		}
		return false;
	}
}
