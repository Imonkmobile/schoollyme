package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class ChangePasswordRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = ChangePasswordRequest.class.getSimpleName();

	private static final int REQ_CODE_CHANGE_PASSWORD = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public ChangePasswordRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new HttpConnector.INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_CHANGE_PASSWORD, 0, mMessage);
			}
		});
	}

	public void changePasswordServerRequest(String OldPassword,String NewPassword,String AccessToken) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already Login....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getForgotPasswordJson(OldPassword,NewPassword);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("changepassword/", REQ_CODE_CHANGE_PASSWORD, "post", false, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getForgotPasswordJson(String OldPassword,String NewPassword) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(),"Password", OldPassword,"PasswordNew",NewPassword);
		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_CHANGE_PASSWORD:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to SignIn. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("changepassword", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}