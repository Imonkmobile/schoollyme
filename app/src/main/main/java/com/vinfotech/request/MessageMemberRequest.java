package com.vinfotech.request;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.MessageMember;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class MessageMemberRequest extends BaseRequest implements HttpResponseListener {
	private static final String TAG = MessageMemberRequest.class.getSimpleName();
	private static final int REQ_CODE_MESSAGE_MEMBERS = 2011;

	public static final String FILTER_ALL = "All";
	public static final String FILTER_ATHLETE = "Athlete";
	public static final String FILTER_PARENT = "Parent";

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private List<MessageMember> mMessageMembers;
	private boolean mRunInBg = false;

	public MessageMemberRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_MESSAGE_MEMBERS, 0, mMessage);
			}
		});
	}

	public void getMessageMemberInServer(String SearchText, String GroupGUID, String Filter, int PageNo) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already requesting message member....");
			}
			return;
		}
		mRequesting = true;
		String jsonData = getMessageMembersJson(SearchText, GroupGUID, Filter, PageNo);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("group/message_members", REQ_CODE_MESSAGE_MEMBERS, "post", mRunInBg, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
		mRunInBg = (null == view);
	}

	private String getMessageMembersJson(String SearchText, String GroupGUID, String Filter, int PageNo) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "SearchText", SearchText, "GroupGUID",
				GroupGUID, "Filter", Filter, "PageNo", Integer.toString(PageNo), "PageSize", Integer.toString(Config.PAGE_SIZE));
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_MESSAGE_MEMBERS:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + mMessageMembers);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mMessageMembers, getTotalRecord());
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to response. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = !live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("message_members", json) && isSuccess()) {
			mMessageMembers = MessageMember.getMessageMembers(getDataArray());
			return true;
		}
		return false;
	}
}