package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class MediaUpdateCaptionRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = MediaUpdateCaptionRequest.class.getSimpleName();

	private static final int REQ_CODE_MEDIA_UPDATE_CAPTION = 3;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private String mCaption;

	public MediaUpdateCaptionRequest(Context mContext) {
		this.mContext = mContext;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void updateMediaFromServer(String AlbumGUID, String MediaGUID, String Caption) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already updating media...");
			}
			return;
		}
		this.mCaption = Caption;

		mRequesting = true;
		String jsonData = getMediaUpdateJSON(AlbumGUID, MediaGUID, Caption);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/update_media_caption", REQ_CODE_MEDIA_UPDATE_CAPTION, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getMediaUpdateJSON(String AlbumGUID, String MediaGUID, String Caption) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumGUID", AlbumGUID, "MediaGUID", MediaGUID, "Caption", Caption);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_MEDIA_UPDATE_CAPTION:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Media update successful: " + mCaption);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mCaption, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to update media. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("SetMediaTitle", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}
