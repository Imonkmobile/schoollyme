package com.vinfotech.request;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.FriendModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class FriendsListRequest extends BaseRequest implements HttpResponseListener {

	private static final String TAG = FriendsListRequest.class.getSimpleName();
	private static final int REQ_CODE_FRIEND_LIST = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private ArrayList<FriendModel> mSuggestedFriendAL;

	public FriendsListRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_FRIEND_LIST, 0, mMessage);
			}
		});
	}

	public void FriendsListServerRequest(String userGuid,String Type,String SortBy,String OrderBy,String PageNo,String PageSize) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}
		mRequesting = true;
		String jsonData = getFriendsListJson(userGuid,Type,SortBy,OrderBy,PageNo,PageSize);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("users/getConnections/", REQ_CODE_FRIEND_LIST, "post", true, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getFriendsListJson(String userGuid,String Type,String SortBy,String OrderBy,String PageNo,String PageSize) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(),"UserGUID",userGuid,"Type",Type,"SortBy",SortBy,"OrderBy",OrderBy,"PageNo",PageNo,"PageSize",PageSize);
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_FRIEND_LIST:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mSuggestedFriendAL, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to response. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("users/getConnections/", json) && isSuccess()) {
			mSuggestedFriendAL = new ArrayList<FriendModel>();
			try {
				JSONObject mJsonObject = getDataObject();
				JSONArray jArray = mJsonObject.getJSONArray("Members");
				if(jArray!=null)
					for(int i=0;i<jArray.length();i++){
						mSuggestedFriendAL.add(new FriendModel(jArray.getJSONObject(i)));
					}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
}