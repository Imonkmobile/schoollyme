package com.vinfotech.request;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.AlbumMedia;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class MediaUploadRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = MediaUploadRequest.class.getSimpleName();
	public static final String MODULE_ID_GROUPS = "1";
	public static final String MODULE_ID_USERS = "3";
	public static final String MODULE_ID_ALBUM = "13";
	public static final String MODULE_NAME_GROUPS = "Groups";
	public static final String MODULE_NAME_USERS = "Users";
	public static final String MODULE_NAME_ALBUM = "Album";
	public static final String TYPE_PROFILE = "profile";
	public static final String TYPE_ALBUM = "album";
	public static final String TYPE_GROUP = "group";
	public static final String TYPE_WALL = "wall";
	public static final String TYPE_GROUP_COVER = "profilebanner";

	private static final int REQ_CODE_MEDIA_UPLOAD_YOUTUBE = 4;
	private static final int REQ_CODE_MEDIA_UPLOAD = 5;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private boolean mRunInBg = false;

	private AlbumMedia mAlbumMedia;

	public MediaUploadRequest(Context context, boolean runInBg) {
		this.mContext = context;
		this.mRunInBg = runInBg;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_MEDIA_UPLOAD, 0, mMessage);
			}
		});
	}

	public void uploadYoutubeMediaInServer(String youtubeURL) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already uploading media...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getUploadMediaYoutubeJSON(youtubeURL);
		mHttpConnector.setDialog(DialogUtil.createProgressDialogDefaultWithMessage(mContext, R.string.loading));
		mHttpConnector.executeAsync("UploadMedia/", REQ_CODE_MEDIA_UPLOAD_YOUTUBE, "post", mRunInBg, jsonData, null, UrlType.SERVICE);
	}

	private String getUploadMediaYoutubeJSON(String youtubeURL) {
		JSONObject jsonObj = JSONUtil.getJSONObject("loginSessionKey", getLoginSessionKey(), "youtubeURL", youtubeURL);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	public void uploadMediaInServer(boolean isImage, File mediaString, String mediaName, String ModuleID, String ModuleEntityGUID,
			String Type) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already uploading media...");
			}
			return;
		}
		if (null == mediaString || !mediaString.exists()) {
			if (Config.DEBUG) {
				Log.v(TAG, "Nothing to upload...");
			}
			return;
		}

		mRequesting = true;
		ArrayList<BasicNameValuePair> extraFormData = getUploadMediaParams(mediaString, mediaName, ModuleID, ModuleEntityGUID, Type);
		mHttpConnector.setDialog(DialogUtil.createProgressDialogDefaultWithMessage(mContext, R.string.loading));
		mHttpConnector.executeAsync((isImage ? "uploadimage/" : "uploadvideo/"), REQ_CODE_MEDIA_UPLOAD, "post", mRunInBg, null, extraFormData,
				UrlType.SERVICE);
	}

	@SuppressWarnings("deprecation")
	private ArrayList<BasicNameValuePair> getUploadMediaParams(File mediaString, String mediaName, String ModuleID,
			String ModuleEntityGUID, String Type) {
		ArrayList<BasicNameValuePair> extraFormData = new ArrayList<BasicNameValuePair>();
		extraFormData.add(new BasicNameValuePair("LoginSessionKey", getLoginSessionKey()));
		extraFormData.add(new BasicNameValuePair("qqfile", mediaString.getPath()));
		extraFormData.add(new BasicNameValuePair("ModuleID", ModuleID));
		extraFormData.add(new BasicNameValuePair("ModuleEntityGUID", ModuleEntityGUID));
		extraFormData.add(new BasicNameValuePair("DeviceType", Config.DEVICE_TYPE));
		extraFormData.add(new BasicNameValuePair("Type", Type));

		return extraFormData;

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_MEDIA_UPLOAD:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "media upload successful: " + mAlbumMedia);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAlbumMedia, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to upload media. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;
		case REQ_CODE_MEDIA_UPLOAD_YOUTUBE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Youtube media upload successful: " + mAlbumMedia);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAlbumMedia, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to upload youtube media. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("UploadMedia", json) && isSuccess()) {
			mAlbumMedia = new AlbumMedia(getDataObject());
			return true;
		}
		return false;
	}
}
