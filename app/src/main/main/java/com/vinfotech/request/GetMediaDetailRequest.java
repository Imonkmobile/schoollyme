package com.vinfotech.request;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.model.AlbumMedia;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class GetMediaDetailRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = GetMediaDetailRequest.class.getSimpleName();

	public static final String ORDER_BY_ALBUMNAME = "ASC";
	public static final String ORDER_BY_CREATEDDATE = "DESC";
	public static final String ALBUM_TYPE_PHOTO = "PHOTO";
	public static final String ALBUM_TYPE_VIDEO = "VIDEO";

	private static final int REQ_CODE_ALBUM_LIST_MEDIA = 2;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private List<AlbumMedia> mAlbumMedias = new ArrayList<AlbumMedia>();

	public GetMediaDetailRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getMediaDetailFromServer(String AlbumGUID, String MediaGUID) {

		// {
		// "LoginSessionKey": "341e30da-560e-0f86-5c3e-36cc8ebd1d4a",
		// "AlbumGUID": "341e30da-560e-0f86-5c3e-36cc8ebd1d4a",
		// "MediaGUID": "341e30da-560e-0f86-5c3e-36cc8ebd1d4a"
		// }

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting album media list...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getMediaDetailJSON(AlbumGUID, MediaGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/media_details", REQ_CODE_ALBUM_LIST_MEDIA, "post", false, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getMediaDetailJSON(String AlbumGUID, String MediaGUID) {
		JSONObject jsonObj = JSONUtil
				.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumGUID", AlbumGUID, "MediaGUID", MediaGUID);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ALBUM_LIST_MEDIA:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "Album media list successful totalRecords=" + totalRecords + ", mAlbumMedias=" + mAlbumMedias);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAlbumMedias, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get album media list. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {

		if (super.parse("album", json) && isSuccess()) {

			JSONObject mJSONObject = getDataObject();
			if (null != mJSONObject) {

				AlbumMedia albumMedia = new AlbumMedia(mJSONObject);
				mAlbumMedias.add(albumMedia);
				return true;
			}
		}
		return false;
	}
}
