package com.vinfotech.request;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.Album;
import com.schollyme.model.NewMedia;
import com.schollyme.model.NewYoutubeUrl;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class AlbumCreateRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = AlbumCreateRequest.class.getSimpleName();

	public static final int VISIBILITY_PUBLIC = 1;
	public static final int VISIBILITY_TEAMMATES = 2;
	public static final String ALBUMTYPE_PHOTO = "PHOTO";
	public static final String ALBUMTYPE_IMAGE = "IMAGE";
	public static final String ALBUMTYPE_VIDEO = "VIDEO";
	public static final String ALBUMTYPE_YOUTUBE = "YOUTUBE";

	private static final int REQ_CODE_MEDIA_UPDATE = 3;
	private static final int REQ_CODE_MEDIA_ADD = 4;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private Album mAlbum;

	public AlbumCreateRequest(final Context mContext) {
		this.mContext = mContext;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_MEDIA_ADD, 0, mMessage);
			}
		});
	}

	public void createAlbumInServer(String AlbumGUID, String AlbumName, String Description, int Visibility, String AlbumType,
			List<NewMedia> newMedias, List<NewYoutubeUrl> newYoutubeUrls) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already creating album...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAlbumCreateJSON(AlbumGUID, AlbumName, Description, Visibility, AlbumType, newMedias, newYoutubeUrls);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/" + (TextUtils.isEmpty(AlbumGUID) ? "add" : "edit"), REQ_CODE_MEDIA_UPDATE, "post", false,
				jsonData, null, UrlType.SERVICE);
	}

	public void addMediaInServer(String AlbumGUID, String AlbumName, String Description, int Visibility, String AlbumType,
			List<NewMedia> newMedias, List<NewYoutubeUrl> newYoutubeUrls) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already adding in album...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAddMediaJSON(AlbumGUID, newMedias, newYoutubeUrls);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/add_media", REQ_CODE_MEDIA_ADD, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getAlbumCreateJSON(String AlbumGUID, String AlbumName, String Description, int Visibility, String AlbumType,
			List<NewMedia> newMedias, List<NewYoutubeUrl> newYoutubeUrls) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumGUID", AlbumGUID, "AlbumName",
				AlbumName, "Description", Description, "Visibility", Integer.toString(Visibility), "AlbumType", AlbumType);

		if (null != newMedias) {
			try {
				jsonObj.put("Media", NewMedia.getJSONArray(newMedias));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (null != newYoutubeUrls) {
			try {
				jsonObj.put("Youtube", NewYoutubeUrl.getJSONArray(newYoutubeUrls));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return null == jsonObj ? "" : jsonObj.toString();

	}

	private String getAddMediaJSON(String AlbumGUID, List<NewMedia> newMedias, List<NewYoutubeUrl> newYoutubeUrls) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumGUID", AlbumGUID);

		if (null != newMedias) {
			try {
				jsonObj.put("Media", NewMedia.getJSONArray(newMedias));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (null != newYoutubeUrls) {
			try {
				jsonObj.put("Youtube", NewYoutubeUrl.getJSONArray(newYoutubeUrls));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_MEDIA_ADD:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Album add successful: " + mAlbum);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to add album. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;
		case REQ_CODE_MEDIA_UPDATE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Album update successful: " + mAlbum);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAlbum, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to update album. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("album", json) && isSuccess()) {
			mAlbum = new Album(getDataObject());
			return true;
		}
		return false;
	}
}
