package com.schollyme;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.luminous.pick.LuminousGallery.LuminousAction;
import com.luminous.pick.LuminousGalleryActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.ErrorLayoutListener;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.media.MediaViewerActivity;
import com.schollyme.media.UploadYoutubeActivity;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewMedia;
import com.schollyme.model.NewYoutubeUrl;
import com.schollyme.model.UploadingMedia;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class CreateWallPostActivity extends BaseActivity implements OnClickListener {
	private static final String TAG = CreateWallPostActivity.class.getSimpleName();
	public static final int REQ_CODE_CREATE_WALL_POST_ACTIVITY = 1031;
	public static final int REQ_CODE_CREATE_WALL_POST_ACTIVITY_USER_PROFILE = 1032;
	private TextView mPrivacyTv, mMarkAsBuzzTv;
	private EditText mPostEt;

	private MediaUploadRequest mMediaUploadRequest;
	private WallPostCreateRequest mWallPostCreateRequest;
	private HeaderLayout mHeaderLayout;
	private ErrorLayout mErrorLayout;
	private LogedInUserModel mLogedInUserModel;

	private int mModuleID;
	private int mVisibility = WallPostCreateRequest.VISIBILITY_PUBLIC;
	private int mCommentable = WallPostCreateRequest.COMMENT_ON;
	private String mPostContent;
	private ArrayList<NewYoutubeUrl> mNewYoutubeUrls;
	private boolean mIsImage = false;
	private String mUserGUID;
	private ImageView mPrivacyIV;

	private boolean isTeamPage = false, mMarkAsBuzz = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_wall_post_activity);

		mModuleID = getIntent().getIntExtra("moduleID", -1);
		mUserGUID = getIntent().getStringExtra("userGUID");
		isTeamPage = getIntent().getBooleanExtra("isTeamPage", false);
		boolean enableMyBuzz = getIntent().getBooleanExtra("enableMyBuzz", false);
		if (Config.DEBUG) {
			Log.d("CreateWallPostActivity", "onCreate mModuleID=" + mModuleID + ", enableMyBuzz=" + enableMyBuzz);
		}
		if (mModuleID < 1) {
			Toast.makeText(this, "Invalid module id.", Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		mPrivacyIV = (ImageView) findViewById(R.id.privacy_iv);
		mHeaderLayout = new HeaderLayout(findViewById(R.id.container_ll));
		mHeaderLayout.setHeaderValues(R.drawable.icon_close, getResources().getString(R.string.Post),
				R.drawable.icon_arrw_right_active_white_xhdpi);
		mHeaderLayout.setListenerItI(this, this);
		mHeaderLayout.enableRightButton(false);
		mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));
		mLogedInUserModel = new LogedInUserModel(this);
		mWallPostCreateRequest = new WallPostCreateRequest(this);
		mWallPostCreateRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, final Object data, int totalRecords) {
				dismissProgressDialog();
				if (success) {
					mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {

						@Override
						public void onErrorShown() {
						}

						@Override
						public void onErrorHidden() {
							setResultAndFinish(data);
						}
					});
					mErrorLayout.showError(getString(R.string.Posted_successfully), true, MsgType.Success);
				} else {
					mHeaderLayout.mRightIb1.setEnabled(true);
					mErrorLayout.showError((String) data, true, MsgType.Error);
				}
			}
		});

		mMediaUploadRequest = new MediaUploadRequest(this, true);
		mMediaUploadRequest.setRequestListener(mRequestListener);

		mPrivacyTv = (TextView) findViewById(R.id.privacy_tv);
		findViewById(R.id.visibility_ll).setOnClickListener(this);
		mMarkAsBuzzTv = (TextView) findViewById(R.id.mark_as_buzz_tv);
		mMarkAsBuzzTv.setVisibility(enableMyBuzz && mLogedInUserModel.isAthlete() ? View.VISIBLE : View.GONE);
		mMarkAsBuzzTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				updateMarkAsBuzz(!mMarkAsBuzz);
			}
		});

		ImageLoaderUniversal.ImageLoadRound(this, Config.IMAGE_URL_PROFILE + mLogedInUserModel.mUserPicURL,
				(ImageView) findViewById(R.id.user_iv), ImageLoaderUniversal.option_Round_Image);
		mPostEt = (EditText) findViewById(R.id.post_et);
		findViewById(R.id.camera_iv).setOnClickListener(this);
		mPostEt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (mPostEt.getText().toString().trim().equals("")) {
					mHeaderLayout.enableRightButton(false);
				} else {
					mHeaderLayout.enableRightButton(true);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		FontLoader.setRobotoRegularTypeface(mPrivacyTv, mPostEt, mMarkAsBuzzTv);

		updateData();
		String mLoggedinUserId = new LogedInUserModel(this).mUserGUID;
		if (!mLoggedinUserId.equals(mUserGUID)) {
			findViewById(R.id.visibility_ll).setClickable(false);
			mPrivacyTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}

		if (isTeamPage) {
			mPrivacyTv.setText(R.string.Teammates);
			mPrivacyIV.setImageResource(R.drawable.ic_lock_grey);
		} else {
			// mPrivacyTv.setText(R.string.Public);
			// mPrivacyTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_globe,
			// 0, 0, 0);
		}
		updateMarkAsBuzz(mMarkAsBuzz);
	}

	public static Intent getIntent(Context context, int moduleID, String userGUID) {
		Intent intent = new Intent(context, CreateWallPostActivity.class);
		intent.putExtra("moduleID", moduleID);
		intent.putExtra("userGUID", userGUID);
		return intent;
	}

	public static Intent getIntent(Context context, int moduleID, String userGUID, boolean enableMyBuzz) {
		Intent intent = new Intent(context, CreateWallPostActivity.class);
		intent.putExtra("moduleID", moduleID);
		intent.putExtra("userGUID", userGUID);
		intent.putExtra("enableMyBuzz", enableMyBuzz);
		return intent;
	}

	public static Intent getIntent(Context context, int moduleID, boolean isTeamPage, String userGUID) {
		Intent intent = new Intent(context, CreateWallPostActivity.class);
		intent.putExtra("moduleID", moduleID);
		intent.putExtra("userGUID", userGUID);
		intent.putExtra("isTeamPage", isTeamPage);
		return intent;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_button:
			try {
				InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(mPostEt.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

			} catch (Exception e) {
				e.printStackTrace();
			}
			setResultAndFinish(null);
			break;
		case R.id.right_button:
			if (isValid()) {
				try {
					InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(mPostEt.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

				} catch (Exception e) {
					e.printStackTrace();
				}
				mHeaderLayout.mRightIb1.setEnabled(false);
				if (mUploadingMedias.size() > 0 && (null == mNewYoutubeUrls || mNewYoutubeUrls.size() == 0)) {
					if (mIsImage) {
						uploadMedia();
					} else if (validateVidSizeWithDialogs(CreateWallPostActivity.this, mUploadingMedias.get(0).path, mErrorLayout)) {
						uploadMedia();
					}
				} else {
					if (null == mProgressDialog) {
						mProgressDialog = DialogUtil.createDiloag(CreateWallPostActivity.this, getString(R.string.Posting));
					} else {
						mProgressDialog.setMessage(getString(R.string.Posting));
					}
					if (!mProgressDialog.isShowing()) {
						mProgressDialog.show();
					}
					mWallPostCreateRequest.createWallPostInServer(mPostContent, mModuleID, mUserGUID, mVisibility, mCommentable,
							mMarkAsBuzz, null, mNewYoutubeUrls);
				}

			}
			break;
		case R.id.visibility_ll:
			DialogUtil.showListDialog(v.getContext(), R.string.Album_Visibility, new OnItemClickListener() {

				@Override
				public void onItemClick(int position, String item) {

					switch (position) {
					case 0:
						mPrivacyIV.setImageResource(R.drawable.ic_globe);
						break;
					case 1:
						mPrivacyIV.setImageResource(R.drawable.ic_lock_grey);
						break;

					default:
						break;
					}
					mPrivacyTv.setText(item);
				}

				@Override
				public void onCancel() {

				}
			}, getString(R.string.Public), getString(R.string.Teammates));
			break;
		case R.id.camera_iv:
			showMediaUploadDialog();
			break;

		default:
			break;
		}
	}

	private final int REQ_CODE_LUMINOUS_GALLERY_MULTIPICK_ACTIVITY = 1098;
	private LinearLayout mThumbContainerLl;
	private int mCurrIndex = 0;
	private List<UploadingMedia> mUploadingMedias = new ArrayList<UploadingMedia>();
	private Map<Integer, View> mViews = new HashMap<Integer, View>();

	private void initMultiPhotoUploadUI() {
		mThumbContainerLl = (LinearLayout) findViewById(R.id.thumb_container_ll);
		addMediaInLayout();
	}

	private void addMediaInLayout() {
		mThumbContainerLl.removeAllViews();
		mViews.clear();
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		final int imgWidth = (int) getResources().getDimension(R.dimen.space_mid10) * 17;
		if (null != mNewYoutubeUrls && mNewYoutubeUrls.size() > 0) {
			for (int i = 0; i < mNewYoutubeUrls.size(); i++) {
				final View view = layoutInflater.inflate(R.layout.upload_media_item, null);
				final View containerRl = view.findViewById(R.id.container_rl);
				containerRl.setTag(i);

				final NewYoutubeUrl newYoutubeUrl = mNewYoutubeUrls.get(i);

				ImageView mediaIv = (ImageView) view.findViewById(R.id.media_iv);
				mediaIv.getLayoutParams().width = imgWidth;
				mediaIv.getLayoutParams().height = imgWidth;
				ImageView typeIv = (ImageView) view.findViewById(R.id.type_iv);

				String thumbUrl = getYoutubeVideoThumb(newYoutubeUrl.Url);
				ImageLoaderUniversal.ImageLoadSquare(this, thumbUrl, mediaIv, ImageLoaderUniversal.option_normal_Image_Thumbnail);
				typeIv.setVisibility(View.VISIBLE);

				mThumbContainerLl.addView(view);
				mViews.put(i, containerRl);

				view.findViewById(R.id.close_iv).setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						mThumbContainerLl.removeView(view);
						mNewYoutubeUrls.remove(newYoutubeUrl);
					}
				});
			}
		} else if (null != mUploadingMedias && mUploadingMedias.size() > 0) {
			for (int i = 0; i < mUploadingMedias.size(); i++) {
				final View view = layoutInflater.inflate(R.layout.upload_media_item, null);
				final View containerRl = view.findViewById(R.id.container_rl);
				containerRl.setTag(i);

				final UploadingMedia uploadingMedia = mUploadingMedias.get(i);

				ImageView mediaIv = (ImageView) view.findViewById(R.id.media_iv);
				mediaIv.getLayoutParams().width = imgWidth;
				mediaIv.getLayoutParams().height = imgWidth;
				ImageView typeIv = (ImageView) view.findViewById(R.id.type_iv);
				if (mIsImage) {
					ImageLoaderUniversal.ImageLoadSquare(this, "file://" + uploadingMedia.path, mediaIv,
							ImageLoaderUniversal.option_normal_Image_Thumbnail);
					typeIv.setVisibility(View.INVISIBLE);
				} else {
					loadVideoThumb(uploadingMedia.path, mediaIv);
					typeIv.setVisibility(View.VISIBLE);
				}

				mThumbContainerLl.addView(view);
				mViews.put(i, containerRl);

				view.findViewById(R.id.close_iv).setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						mThumbContainerLl.removeView(view);
						mUploadingMedias.remove(uploadingMedia);
					}
				});
			}
		}
	}

	public boolean validateVidSizeWithDialogs(Context context, String videoPath, ErrorLayout errorLayout) {
		File file = new File(videoPath);
		if (!file.exists()) {
			if (null != errorLayout) {
				errorLayout.showError(context.getString(R.string.Invalid_file), true, MsgType.Error);
			} else {
				DialogUtil.showOkDialog(context, context.getString(R.string.Invalid_file), "");
			}
			return false;
		} else if (file.length() > Config.MAX_VIDEO_LEN_BYTES) {
			if (null != errorLayout) {
				errorLayout.showError(context.getString(R.string.Video_is_larger_than_40MB), true, MsgType.Error);
			} else {
				DialogUtil.showOkDialog(context, context.getString(R.string.Video_is_larger_than_40MB),
						context.getString(R.string.Video_Limt_Crossed));
			}
			return false;
		} else if (getVideoDuration(videoPath) > Config.MAX_VIDEO_LEN_MILLIS) {
			if (null != errorLayout) {
				errorLayout.showError(context.getString(R.string.Video_is_longer_than_6), true, MsgType.Error);
			} else {
				DialogUtil.showOkDialog(context, context.getString(R.string.Video_is_longer_than_6),
						context.getString(R.string.Video_Limt_Crossed));
			}
			return false;
		}

		return true;
	}

	private void updateMarkAsBuzz(boolean checked) {
		mMarkAsBuzz = checked;
		mMarkAsBuzzTv.setTextColor(mMarkAsBuzz ? Color.WHITE : getResources().getColor(R.color.signup_normal_text_color));
		mMarkAsBuzzTv.setBackgroundResource(mMarkAsBuzz ? R.drawable.circle_red_rect : R.drawable.circle_gray_rect);
	}

	public long getVideoDuration(String videoPath) {
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();
		retriever.setDataSource(videoPath);
		String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
		try {
			return Long.parseLong(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	private void updateData() {
		mPrivacyTv.setText((mVisibility == WallPostCreateRequest.VISIBILITY_PUBLIC ? R.string.Public : R.string.Teammates));
		mPostEt.setText(mPostContent);
	}

	private boolean isValid() {
		mVisibility = (getString(R.string.Public).equalsIgnoreCase(mPrivacyTv.getText().toString().trim()) ? WallPostCreateRequest.VISIBILITY_PUBLIC
				: WallPostCreateRequest.VISIBILITY_TEAMMATES);
		mPostContent = mPostEt.getText().toString().trim();

		if (null != mNewYoutubeUrls && mNewYoutubeUrls.size() > 0) {
			return true;
		} else if (mUploadingMedias.size() > 0) {
			return true;
		} else if (mPostContent.length() == 0) {
			mErrorLayout.showError(getString(R.string.Please_enter_post_content), true, MsgType.Error);
			return false;
		}
		return true;
	}

	private void handleRightBtnEnable() {
		if (mPostEt.getText().toString().trim().length() == 0 && mUploadingMedias.size() == 0) {
			mHeaderLayout.enableRightButton(false);
		} else {
			mHeaderLayout.enableRightButton(true);
		}
	}

	private ArrayList<NewMedia> mNewMeidas = new ArrayList<NewMedia>();
	private RequestListener mRequestListener = new RequestListener() {

		@Override
		public void onComplete(boolean success, Object data, int totalRecords) {
			if (success) {
				AlbumMedia mAlbumMedia = (AlbumMedia) data;
				mNewMedia.MediaGUID = mAlbumMedia.MediaGUID;
				mNewMeidas.add(mNewMedia);

				mCurrIndex++;
				uploadMedia();
			} else if (null != data) {
				dismissProgressDialog();
				DialogUtil.showOkCancelDialog(CreateWallPostActivity.this, null, (String) data, null, null);
			}
		}
	};

	private ProgressDialog mProgressDialog = null;
	private NewMedia mNewMedia = null;

	private void uploadMedia() {
		if (mCurrIndex >= mUploadingMedias.size()) {
			mProgressDialog.setMessage(getString(R.string.Posting));
			mWallPostCreateRequest.createWallPostInServer(mPostContent, mModuleID, mUserGUID, mVisibility, mCommentable, mMarkAsBuzz,
					mNewMeidas, mNewYoutubeUrls);
			return;
		}
		if (null == mProgressDialog) {
			mProgressDialog = DialogUtil.createDiloag(CreateWallPostActivity.this, composeProgressMessage());
		}
		if (!mProgressDialog.isShowing()) {
			mProgressDialog.show();
		}
		mProgressDialog.setMessage(composeProgressMessage());

		File file = new File(mUploadingMedias.get(mCurrIndex).path);
		mNewMedia = new NewMedia("", file.getName(), mPostContent, "", "");
		mMediaUploadRequest.uploadMediaInServer(mIsImage, file, mNewMedia.Caption, MediaUploadRequest.MODULE_ID_ALBUM, mUserGUID,
				MediaUploadRequest.TYPE_WALL);
	}

	private String composeProgressMessage() {
		if (mIsImage && mUploadingMedias.size() > 1) {
			return String.format(getString(R.string.Uploading_d_of_d), (mCurrIndex + 1), mUploadingMedias.size());
		}
		return getString(mIsImage ? R.string.Uploading_photo : R.string.Uploading_video);
	}

	private void dismissProgressDialog() {
		if (null != mProgressDialog && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
	}

	private void setResultAndFinish(Object object) {
		Intent data = new Intent();
		if (null != object) {
			setResult(RESULT_OK, data);
		} else {
			setResult(RESULT_CANCELED);
		}
		finish();
	}

	public void loadVideoThumb(final String path, final ImageView imageView) {
		imageView.setImageResource(R.drawable.ic_deault_media);
		new Thread(new Runnable() {

			@Override
			public void run() {
				File file = new File(path);
				final Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(file.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (null != thumbnail && null != imageView) {
							imageView.setImageBitmap(thumbnail);
						}
					}
				});
			}
		}).start();
	}

	private void showMediaUploadDialog() {
		DialogUtil.showListDialog(this, R.string.Add_Media, new OnItemClickListener() {

			@Override
			public void onItemClick(int position, String item) {
				switch (position) {
				case 0:
					// startActivityForResult(LuminousGalleryActivity.getIntent(LuminousAction.ACTION_PICK),
					// LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY);
					startActivityForResult(LuminousGalleryActivity.getIntent(LuminousAction.ACTION_MULTIPLE_PICK),
							REQ_CODE_LUMINOUS_GALLERY_MULTIPICK_ACTIVITY);
					break;
				case 1:
					startActivityForResult(LuminousGalleryActivity.getIntentVideo(LuminousAction.ACTION_PICK),
							LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY);
					break;
				case 2:
					startActivityForResult(UploadYoutubeActivity.getIntent(CreateWallPostActivity.this),
							UploadYoutubeActivity.REQ_CODE_UPLOAD_YOUTUBE_ACTIVITY);
					break;

				default:
					break;
				}
			}

			@Override
			public void onCancel() {
			}
		}, getString(R.string.Add_Photo), getString(R.string.Add_Video), getString(R.string.Add_Youtube_Video));
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (Config.DEBUG) {
			Log.d(TAG, "onActivityResult requestCode =" + requestCode + ", resultCode=" + resultCode);
		}
		switch (requestCode) {
		case LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY:
			if (Activity.RESULT_OK == resultCode) {
				mIsImage = data.getBooleanExtra("sel_type_img", true);
				final String mediaPath = data.getStringExtra("data");
				mUploadingMedias.clear();
				mUploadingMedias.add(new UploadingMedia("", mediaPath));
				if (!mIsImage) {
					validateVidSizeWithDialogs(CreateWallPostActivity.this, mediaPath, mErrorLayout);
				}
				mNewYoutubeUrls = null;
				handleRightBtnEnable();
				initMultiPhotoUploadUI();
			}
			break;
		case REQ_CODE_LUMINOUS_GALLERY_MULTIPICK_ACTIVITY:
			if (Activity.RESULT_OK == resultCode) {
				mNewYoutubeUrls = null;
				mIsImage = data.getBooleanExtra("sel_type_img", true);
				final String[] imagePaths = data.getStringArrayExtra("data");
				if (Config.DEBUG) {
					Log.d(TAG, "onActivityResult imagePaths=" + imagePaths + ", mIsImage=" + mIsImage);
				}
				if ((null != imagePaths && imagePaths.length > 0)) {
					mUploadingMedias.clear();
					for (int i = 0; i < imagePaths.length; i++) {
						mUploadingMedias.add(new UploadingMedia("", imagePaths[i]));
					}
					initMultiPhotoUploadUI();
				}
			}
			break;
		case UploadYoutubeActivity.REQ_CODE_UPLOAD_YOUTUBE_ACTIVITY:
			if (Activity.RESULT_OK == resultCode) {
				mNewYoutubeUrls = data.getParcelableArrayListExtra("newYoutubeUrls");

				mNewMeidas.clear();
				mUploadingMedias.clear();
				mIsImage = false;
				handleRightBtnEnable();
				initMultiPhotoUploadUI();
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public String getYoutubeVideoThumb(String url) {
		String vidId = MediaViewerActivity.getYouTubeVideoId(url);
		String thumb = "http://img.youtube.com/vi/" + vidId + "/default.jpg";
		if (Config.DEBUG) {
			Log.d(TAG, "getYoutubeVideoThumb thumb=" + thumb);
		}
		return thumb;
	}

}
