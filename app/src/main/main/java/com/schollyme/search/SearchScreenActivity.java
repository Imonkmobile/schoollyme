package com.schollyme.search;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.adapter.SearchScreenAdapter;
import com.schollyme.model.FilterBy;
import com.schollyme.model.FriendModel;
import com.vinfotech.request.SearchUserRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

public class SearchScreenActivity extends BaseActivity implements OnClickListener {

	private VHolder mVHolder;
	private SearchUserRequest mSearchUserRequest;
	private FilterBy mFilterBy;
	private int mPageNumber = Config.DEFAULT_PAGE_INDEX;
	private SearchScreenAdapter mSearchScreenAdapter;
	ArrayList<FriendModel> mFriendModel;
	private boolean isLoading = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_screen_activity);
		initialization();

	}

	private Handler mHandler = new Handler();

	private void initialization() {
		mFilterBy = new FilterBy();
		mSearchScreenAdapter = new SearchScreenAdapter(this);
		mFriendModel = new ArrayList<FriendModel>();
		mSearchUserRequest = new SearchUserRequest(SearchScreenActivity.this);
		mVHolder = new VHolder(findViewById(R.id.container_rl), this);
		mVHolder.mGenricLV.setAdapter(mSearchScreenAdapter);
		mVHolder.mSearchBox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(final CharSequence s, int start, int before, int count) {
				mHandler.removeCallbacksAndMessages(null);
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						mFilterBy.setSearchKeyword(mVHolder.mSearchBox.getText().toString().trim());

						if (s.toString().trim().equals("") || s.toString().trim().length() < 2) {
							// mSearchUserRequest.setActivityStatus(false);
							mVHolder.mEmptyView.setVisibility(View.GONE);
							mFriendModel.clear();
							mSearchScreenAdapter.setList(mFriendModel);
						} else {

							mVHolder.mEmptyView.setVisibility(View.GONE);
							mFriendModel.clear();
							mSearchScreenAdapter.setList(mFriendModel);
							searchUserService(mPageNumber);
						}

					}
				}, 500);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// mSearchUserRequest.setActivityStatus(true);
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (null != mVHolder.mClearViewIB) {
					mVHolder.mClearViewIB.setVisibility(s.length() > 0 ? View.VISIBLE : View.INVISIBLE);
				}

			}
		});

		mVHolder.mSearchBox.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE && mVHolder.mSearchBox.getText().toString().trim().length() == 1) {
					mFriendModel.clear();
					mSearchScreenAdapter.setList(mFriendModel);
					searchUserService(1);
				}
				return false;
			}
		});

		mVHolder.mGenricLV.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// mFriendModel
				startActivity(FriendProfileActivity.getIntent(SearchScreenActivity.this, mFriendModel.get(position).mUserGUID,
						mFriendModel.get(position).mProfileLink, "SearchScreenActivity"));
			}
		});

	}
	
	public void searchUserService(int pagenumber) {
		mSearchUserRequest.getUserListInServer(mFilterBy, pagenumber);
		mSearchUserRequest.setLoader(mVHolder.mLoadingLay);
		if (pagenumber == Config.DEFAULT_PAGE_INDEX) {

			mFriendModel.clear();
			mSearchScreenAdapter.setList(mFriendModel);
		}
		isLoading = false;
		mSearchUserRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {

					if (totalRecords == 0) {
						isLoading = false;
						mVHolder.mEmptyView.setVisibility(View.VISIBLE);
					} else if (mFriendModel.size() < totalRecords) {
						isLoading = true;
						mVHolder.mEmptyView.setVisibility(View.GONE);
						List<FriendModel> mUserModels = (List<FriendModel>) data;
						mFriendModel.addAll(mUserModels);
						mSearchScreenAdapter.setList(mUserModels);

					} else {

						isLoading = true;
					}

				} else {
					if (totalRecords == 0) {
						mVHolder.mEmptyView.setVisibility(View.VISIBLE);
					}
				}
			}
		});

	}

	class VHolder {

		private EditText mSearchBox;
		private ImageButton mClearViewIB, mFilterIB;
		private ListView mGenricLV;
		private RelativeLayout mEmptyView;
		private ImageButton mBackbuttonIB;
		private LinearLayout mLoadingLay;
		private ImageView mIsDefault;
		private RelativeLayout mContainerRL;

		//
		//
		// ListView
		public VHolder(View view, OnClickListener listener) {

			mSearchBox = (EditText) view.findViewById(R.id.search_et);
			mEmptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
			mLoadingLay = (LinearLayout) view.findViewById(R.id.loading_lay_LL);
			mClearViewIB = (ImageButton) view.findViewById(R.id.clear_iv);
			mBackbuttonIB = (ImageButton) view.findViewById(R.id.back_ib);
			mFilterIB = (ImageButton) view.findViewById(R.id.filter_ib);
			mClearViewIB.setVisibility(View.GONE);
			mGenricLV = (ListView) view.findViewById(R.id.genric_lv);
			mIsDefault = (ImageView) view.findViewById(R.id.is_default_view);
			mContainerRL = (RelativeLayout) view.findViewById(R.id.main_container_rl);
			// mIsDefault.setBackgroundColo
			// mIsDefault.setColorFilter(getResources().getColor(R.color.accept_bg_color),
			// Mode.MULTIPLY);
			mClearViewIB.setOnClickListener(listener);
			mFilterIB.setOnClickListener(listener);
			mBackbuttonIB.setOnClickListener(listener);
			mContainerRL.setOnClickListener(listener);
			FontLoader.SetFontToWholeView(SearchScreenActivity.this, view, FontLoader.getRobotoRegular(SearchScreenActivity.this));
		}
	}

	class MyCustomScroll implements OnScrollListener {

		// expInterface.onScrollList();

		private int mLastFirstVisibleItem;

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {

		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

			int k = visibleItemCount + firstVisibleItem;

			if (k >= totalItemCount && isLoading && totalItemCount != 0) {
				mPageNumber++;
				searchUserService(mPageNumber);
			}
			if (mLastFirstVisibleItem < firstVisibleItem) {

				Log.i("SCROLLING DOWN", "TRUE");
			}
			if (mLastFirstVisibleItem > firstVisibleItem) {

				Log.i("SCROLLING UP", "TRUE");
			}

			mLastFirstVisibleItem = firstVisibleItem;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		boolean Res = mFilterBy.isDefault();
		if (Res) {
			mVHolder.mIsDefault.setVisibility(View.GONE);
		} else {
			mVHolder.mIsDefault.setVisibility(View.VISIBLE);
		}

	}

	final int REQ_CODE_FILTER_SCREEN = 789;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.clear_iv:
			mVHolder.mSearchBox.setText("");
			break;
		case R.id.filter_ib:
			startActivityForResult(FilterScreenActivity.getIntent(SearchScreenActivity.this, mFilterBy), REQ_CODE_FILTER_SCREEN);
			this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
			break;

		case R.id.back_ib:
			// mSearchUserRequest.setActivityStatus(false);
			this.finish();
			this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;

		case R.id.main_container_rl:

			if (null != mVHolder.mSearchBox) {
				try {
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
				} catch (Exception e) {
				}
			}
			break;

		default:
			break;

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// mSearchUserRequest.setActivityStatus(false);
		switch (requestCode) {
		case REQ_CODE_FILTER_SCREEN:
			if (resultCode == RESULT_OK) {

				mFilterBy = data.getParcelableExtra("filterBy");
				mFilterBy.setSearchKeyword(mVHolder.mSearchBox.getText().toString());
				if (!mFilterBy.getSearchKeyword().equals("")) {
					searchUserService(1);
				}

			}
			break;

		default:
			break;
		}

		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	public void onBackPressed() {
		// mSearchUserRequest.setActivityStatus(false);
		this.finish();
		this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, SearchScreenActivity.class);
		return intent;
	}
}
