package com.schollyme.inbox;

import java.util.ArrayList;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.schollyme.BaseActivity;
import com.schollyme.R;
import com.schollyme.adapter.MessageToUserListAdapter;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.ChatModel;
import com.schollyme.model.FilterBy;
import com.schollyme.model.FriendModel;
import com.vinfotech.request.InboxSendMessageRequest;
import com.vinfotech.request.SearchUserRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;

public class ForwardMessageActivity extends BaseActivity{

	private Context mContext;
	private ArrayList<FriendModel> mMyFriendsAL;
	private MessageToUserListAdapter mAdapter;
	private ListView mFriendsLV;
	private TextView mNoRecordTV;
	private ArrayList<FriendModel> mSuggestionsAL;
	private EditText mSearchET;
	private ImageButton mClearIB;
	private int PageNo = 1;
	private String messageToBeForward;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_message_activity);
		mContext = this;
		mFriendsLV = (ListView) findViewById(R.id.friends_lv);
		mNoRecordTV = (TextView) findViewById(R.id.no_record_message_tv);
		setHeader(findViewById(R.id.header_layout),R.drawable.selector_cancel, 0, getResources().getString(R.string.forward_message), new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(0, R.anim.slide_down_dialog);
			}
		}, null);
		mSearchET = (EditText) findViewById(R.id.search_et);
		mClearIB = (ImageButton) findViewById(R.id.clear_text_ib);
		PageNo = 1;
		messageToBeForward = getIntent().getStringExtra("messageToBeForward");
		FontLoader.setRobotoRegularTypeface(mSearchET);
		mSearchET.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence searchStr, int arg1, int arg2, int arg3) {
				if(searchStr.length()==0){
					mFriendsLV.setVisibility(View.GONE);
					mNoRecordTV.setVisibility(View.VISIBLE);
					mSuggestionsAL = new ArrayList<FriendModel>();
					mMyFriendsAL = new ArrayList<FriendModel>();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});
		new SearchHandler(mSearchET).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				getSearchUserServerRequest();
			}
		}).setClearView(mClearIB);
		mSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					Utility.hideSoftKeyboard(mSearchET);
					return true;
				}
				return false;
			}
		});
	}

	public static Intent getIntent(Context context,String messageToBeForward) {
		Intent intent = new Intent(context, ForwardMessageActivity.class);
		intent.putExtra("messageToBeForward",messageToBeForward);
		return intent;
	}

	private void setSearchResultFriendsAdapter(ArrayList<FriendModel> mAL){
		mSuggestionsAL = mAL;
		mAdapter = new MessageToUserListAdapter(mContext);
		if(mSuggestionsAL.size()==0){
			mFriendsLV.setVisibility(View.GONE);
			mNoRecordTV.setVisibility(View.VISIBLE);
		}
		else{
			mNoRecordTV.setVisibility(View.GONE);
			mFriendsLV.setVisibility(View.VISIBLE);
			mAdapter.setList(mSuggestionsAL);
			mFriendsLV.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();
			mFriendsLV.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						final int pos, long arg3) {
					final String userName = mSuggestionsAL.get(pos).mFirstName+" "+mSuggestionsAL.get(pos).mLastName;
					final String message = getResources().getString(R.string.forward_to_dialog_message)+" "+userName;
					DialogUtil.showOkDialogButtonLisnter(mContext, message, "",new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {
							messageSendRequest(messageToBeForward, userName, pos);
						}
					} );
					//	startActivity(ChatHistoryActivity.getIntent(mContext, userName,mSuggestionsAL.get(pos),"0"));
				}
			});
		}
		mMyFriendsAL = mSuggestionsAL;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		overridePendingTransition(0, R.anim.slide_down_dialog);
	}

	private void messageSendRequest(String message,final String userName,final int pos){

		InboxSendMessageRequest mRequest = new InboxSendMessageRequest(mContext);
		mRequest.InboxSendMessageServerRequest("No Subject", message, mSuggestionsAL.get(pos).mUserGUID);
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					ChatModel model = (ChatModel) data;
					startActivity(ChatHistoryActivity.getIntent(mContext, userName,mSuggestionsAL.get(pos),model.mMessageGUID));
					finish();
				}
			}
		});
	}

	private void getSearchUserServerRequest(){
		String searchQuery = mSearchET.getText().toString().trim();
		if(!TextUtils.isEmpty(searchQuery)){
			SearchUserRequest mRequest = new SearchUserRequest(mContext);
			FilterBy mFilter = new FilterBy();
			mFilter.setSearchKeyword(searchQuery);
			mRequest.getUserListInServer(mFilter, PageNo);
			mRequest.setRequestListener(new RequestListener() {

				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					if(success){
					   setSearchResultFriendsAdapter((ArrayList<FriendModel>) data);
					}
					else{
						mFriendsLV.setVisibility(View.GONE);
						mNoRecordTV.setVisibility(View.VISIBLE);
					}
				}
			});
		}
	}
}