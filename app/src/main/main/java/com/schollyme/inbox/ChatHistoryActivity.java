package com.schollyme.inbox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.nolanlawson.supersaiyan.SectionedListAdapter;
import com.nolanlawson.supersaiyan.Sectionizer;
import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.adapter.ChatHistoryAdapter;
import com.schollyme.model.ChatModel;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.InboxChangeMessageReadStatusRequest;
import com.vinfotech.request.InboxChangeMessageStatusRequest;
import com.vinfotech.request.InboxMesageDetailRequest;
import com.vinfotech.request.InboxSendMessageRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;

public class ChatHistoryActivity extends BaseActivity{

	private Context mContext;
	private ListView mChatHistoryLV;
	private String mUserGUID;
	private List<ChatModel> mChatHistoryAL;
	private SectionedListAdapter<ChatHistoryAdapter> sectionedAdapter;
	private FriendModel mFriendModel;
	private EditText mWriteMessageET;
	private ImageButton mSendMessageIB;
	private int pageNumber = 1;
	private String mMsgGUID;
	private ChatHistoryAdapter mAdapter;
	private boolean isProcessing = false;
	private Timer timer;
	private boolean loadingFlag = false;
	private int mFirstVisibleItemPos = 0;
	private ProgressBar mLoadMorePB;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_list_activity);
		mContext = this;
		mChatHistoryLV = (ListView) findViewById(R.id.chat_history_lv);
		mWriteMessageET = (EditText) findViewById(R.id.write_message_et);
		mSendMessageIB = (ImageButton) findViewById(R.id.send_message_ib);
		mLoadMorePB = (ProgressBar) findViewById(R.id.load_more_pb);
		String mFriendUserName = getIntent().getStringExtra("friendName");
		mChatHistoryAL = new ArrayList<ChatModel>();
		mMsgGUID = getIntent().getStringExtra("msgGUID");
		mFriendModel = (FriendModel) getIntent().getSerializableExtra("friendModel");

		mUserGUID = mFriendModel.mUserGUID;
		FontLoader.setRobotoRegularTypeface(mWriteMessageET);
		setHeader(findViewById(R.id.header_layout),R.drawable.icon_back, R.drawable.icon_dot_menu_header, mFriendUserName, new OnClickListener() {
			@Override
			public void onClick(View v) {
				Utility.hideSoftKeyboard(mWriteMessageET);
				finish();
			}
		}, MoreOptionsLIstener);

		messageDetailRequest();

		mSendMessageIB.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if(!TextUtils.isEmpty(mWriteMessageET.getText().toString())){
				//	Utility.hideSoftKeyboard(mWriteMessageET);
					messageSendRequest();
					mWriteMessageET.setText("");
				}
			}
		});
		updateMessageReadStatus(mMsgGUID);
		syncMessage();
		mWriteMessageET.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence message, int arg1, int arg2, int arg3) {
				String searchText = message.toString().trim();
				if(searchText.length()>0){
					mSendMessageIB.setEnabled(true);
					mSendMessageIB.setImageResource(R.drawable.ic_send);
				}
				else{
					mSendMessageIB.setEnabled(false);
					mSendMessageIB.setImageResource(R.drawable.ic_send_disable);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});
	}

	OnClickListener MoreOptionsLIstener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
				@Override
				public void onItemClick(int position, String item) {
					if(position==0){
						DialogUtil.showOkDialogButtonLisnter(mContext, mContext.getResources().getString(R.string.deleteall_confirmation),mContext.getResources().getString(R.string.deleteall_dialog_header) , new OnOkButtonListner() {
							@Override
							public void onOkBUtton() {
								if(mChatHistoryAL.size()>0){
									updateMessageStatus(mChatHistoryAL.get(mChatHistoryAL.size()-1).mMessageGUID,mFriendModel.mUserGUID,true,0);
								}
							}
						});
						/*if(mChatHistoryAL.size()>0){
						   updateMessageStatus(mChatHistoryAL.get(mChatHistoryAL.size()-1).mMessageGUID,true,0);
						}*/
					}
					else if(position==1){
						if(new LogedInUserModel(mContext).mUserGUID.equals(mUserGUID)){
							mContext.startActivity(DashboardActivity.getIntent(mContext,6, ""));
						}
						else{
							mContext.startActivity(FriendProfileActivity.getIntent(mContext,mFriendModel.mUserGUID,mFriendModel.mProfileLink, "ChatHistoryActivity"));
						}
					}
				}

				@Override
				public void onCancel() {

				}
			}, mContext.getString(R.string.clear_all_message), mContext.getString(R.string.view_profile));
		}
	};

	public static Intent getIntent(Context context,String userName,FriendModel mModel,String messageGUID) {
		Intent intent = new Intent(context, ChatHistoryActivity.class);
		intent.putExtra("friendName", userName);
		intent.putExtra("friendModel", mModel);
		intent.putExtra("msgGUID", messageGUID);
		return intent;
	}

	int i =0;

	private void setSectionAdapter(){
		sectionedAdapter = SectionedListAdapter.Builder.create(mContext, mAdapter)
				.setSectionizer(new Sectionizer<ChatModel>() {

					@Override
					public CharSequence toSection(ChatModel input) {
						return input.mChatDay;
					}
				})
				.sortKeys()
				.sortValues(new Comparator<ChatModel>() {

					@Override
					public int compare(ChatModel lhs, ChatModel rhs) {
						int result = lhs.mChatDay.compareTo(rhs.mChatDay);
						return result;
					}
				})
				.build();
	}

	private void setChatHistoryAdapter(){

		if(null == mAdapter){
			mAdapter = new ChatHistoryAdapter(mContext,mChatHistoryLV);
		}
		mAdapter.setList(mChatHistoryAL);
		setSectionAdapter();

		mChatHistoryLV.setAdapter(sectionedAdapter);
		/*if(i==0){
			mChatHistoryLV.setAdapter(sectionedAdapter);
			i++;
		}	else
			sectionedAdapter.notifyDataSetChanged();*/

		mChatHistoryLV.setOnScrollListener(new OnScrollListener() {
			boolean bReachedListEnd = false;
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
					Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
					if (null != mAdapter&& !loadingFlag) {
						mFirstVisibleItemPos =  view.getFirstVisiblePosition();
						pageNumber++;
						loadingFlag = true;
						//		mLoadMorePB.setVisibility(View.VISIBLE);
						messageLoadMoreRequest();

					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
				bReachedListEnd = (firstVisibleItem==0)/*((firstVisibleItem + visibleItemCount) >= (totalItemCount))*/;
				mFirstVisibleItemPos = firstVisibleItem;
			}
		});
	}

	private void syncMessage(){
		timer = new Timer();
		TimerTask doAsynchronousTask = new TimerTask() {

			@Override
			public void run() {
				if(!isProcessing){
					messageDetailRequest();
				}
			}
		}; 
		timer.schedule(doAsynchronousTask, Config.MESSAGE_WINDOW_SYNC_DELAY, Config.MESSAGE_WINDOW_SYNC_DELAY);
	}

	private void messageLoadMoreRequest(){
		isProcessing = true;
		InboxMesageDetailRequest mRequest = new InboxMesageDetailRequest(mContext);
		mRequest.getInboxMesageDetailServerRequest(mMsgGUID, String.valueOf(pageNumber), String.valueOf(Config.PAGE_SIZE/2));
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				loadingFlag = false;
				if(success){
					if(null==mChatHistoryAL){
						ArrayList<ChatModel> mLoadMoreAL = (ArrayList<ChatModel>) data;
						Collections.reverse(mLoadMoreAL);
						mLoadMoreAL.addAll(mChatHistoryAL);
						mChatHistoryAL = mLoadMoreAL;
						setChatHistoryAdapter();
					}
					else{

						ArrayList<ChatModel> mLoadMoreAL = (ArrayList<ChatModel>) data;
						if(mLoadMoreAL.size()>0){
							mFirstVisibleItemPos +=mLoadMoreAL.size();
							Collections.reverse(mLoadMoreAL);
							mLoadMoreAL.addAll(mChatHistoryAL);
							mChatHistoryAL = mLoadMoreAL;

							mAdapter.setList(mChatHistoryAL);
							setSectionAdapter();
							mChatHistoryLV.setAdapter(sectionedAdapter);
							mChatHistoryLV.setSelection(mFirstVisibleItemPos);
						}
					}
				}
			}
		});
	}

	private void messageDetailRequest(){
		isProcessing = true;
		InboxMesageDetailRequest mRequest = new InboxMesageDetailRequest(mContext);
		mRequest.getInboxMesageDetailServerRequest(mMsgGUID, String.valueOf(1), String.valueOf(Config.PAGE_SIZE/2));
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				isProcessing = false;
				if(success){
					if(null==mChatHistoryAL){
						mChatHistoryAL =  (ArrayList<ChatModel>) data;
						Collections.reverse(mChatHistoryAL);
						mFirstVisibleItemPos = mChatHistoryLV.getSelectedItemPosition();
						setChatHistoryAdapter();
					}
					else{
						mChatHistoryAL =  (ArrayList<ChatModel>) data;
						Collections.reverse(mChatHistoryAL);
						if(mFirstVisibleItemPos==0)
							setChatHistoryAdapter();
					}
				}
			}
		});
	}

	private void messageSendRequest(){
		isProcessing = true;
		InboxSendMessageRequest mRequest = new InboxSendMessageRequest(mContext);
		mRequest.InboxSendMessageServerRequest("No Subject", mWriteMessageET.getText().toString(), mUserGUID);
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				isProcessing = false;
				if(success){
					ChatModel model = (ChatModel) data;
					if(mChatHistoryAL==null){
						mChatHistoryAL = new ArrayList<ChatModel>();
					}
					mChatHistoryAL.add(model);
					mMsgGUID = model.mMessageGUID;
					if(mAdapter==null){
						setChatHistoryAdapter();
					}
					else{
						mAdapter.setList(mChatHistoryAL);
						setSectionAdapter();
						mChatHistoryLV.setAdapter(sectionedAdapter);
					}
				}
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(null!=timer){
			timer.cancel();
		}
	}

	public void updateMessageStatus(String MessageGUID,String MessageReceiverGUID,final boolean deleteThread,final int pos){
		isProcessing = true;
		InboxChangeMessageStatusRequest mRequest = new InboxChangeMessageStatusRequest(ChatHistoryActivity.this);
		mRequest.getInboxChangeStatusServerRequest(MessageGUID, MessageReceiverGUID, String.valueOf(Config.MOVE_MESSGAE_TO_TRASH),deleteThread);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				isProcessing = false;
				if(deleteThread){
					mChatHistoryAL = new ArrayList<ChatModel>();
				}
				else{
					mChatHistoryAL.remove(pos);
				}

				mAdapter.setList(mChatHistoryAL);
				setSectionAdapter();
				mChatHistoryLV.setAdapter(sectionedAdapter);
				mChatHistoryLV.setSelection(mFirstVisibleItemPos);
				//	setChatHistoryAdapter();
			}
		});
	}

	public void updateMessageReadStatus(String MessageGUID){
		isProcessing = true;
		InboxChangeMessageReadStatusRequest mRequest = new InboxChangeMessageReadStatusRequest(ChatHistoryActivity.this);
		mRequest.getInboxChangeMessageReadStatusServerRequest(MessageGUID, "1");
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				isProcessing = false;
			}
		});
	}
}