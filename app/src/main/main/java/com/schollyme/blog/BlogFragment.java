package com.schollyme.blog;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.adapter.BlogListAdapter;
import com.schollyme.adapter.BlogListAdapter.OnItemClickBlogList;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.BlogModel;
import com.vinfotech.request.BlogListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

public class BlogFragment extends BaseFragment implements OnClickListener, OnRefreshListener {

	private TextView mNoRecordMessageTV;
	private ListView mBlogLV;
	private Context mContext;
	private int mPageNumber = 1;
	private BlogListAdapter mBlogListAdapter;
	private ProgressBar mLoadingCenter;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private ProgressBar mLoaderBottomPB;
	private BlogListRequest mBlogListRequest;
	private EditText mSearchBox;
	private View mMainView;
	private View mFooterRL;
	private TextView mNoMoreTV;
	private ArrayList<BlogModel> mBlogList;

	public static BaseFragment getInstance(Context context) {
		BlogFragment mFragment = new BlogFragment();
		return mFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainView = inflater.inflate(R.layout.blogs_fragment, container, false);
		setHeader();
		initViews(mMainView);
		mLoadingCenter.post(new Runnable() {

			@Override
			public void run() {
				getBlogList(mPageNumber, mSearchBox.getText().toString());
			}
		});
		return mMainView;
	}

	private void getBlogList(int pageIndex2, String string) {

		isLoading = false;
		mNoMoreTV.setVisibility(View.INVISIBLE);

		if (mSwipeRefreshWidget.isRefreshing()) {
			mBlogListRequest.setLoader(null);
		} else {
			mBlogListRequest.setLoader(pageIndex2 <= 1 ? mLoadingCenter : mLoaderBottomPB);
		}

		mBlogListRequest.getBlogListInServer(string, "" + pageIndex2);
		mBlogListRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					if (mPageNumber == 1) {
						resetList();
					}
					List<BlogModel> bloglistcurrent = (List<BlogModel>) data;
					mBlogList.addAll(bloglistcurrent);
					mBlogListAdapter.setList(mBlogList);

					if (mPageNumber == 1) {
						mNoMoreTV.setVisibility(View.INVISIBLE);
					} else {
						mNoMoreTV.setVisibility(View.VISIBLE);
					}

					if (totalRecords == 0) {
						isLoading = false;
						mNoRecordMessageTV.setVisibility(View.VISIBLE);

					} else if (mBlogList.size() < totalRecords) {
						mNoRecordMessageTV.setVisibility(View.GONE);
						isLoading = true;
						mPageNumber++;

					} else {
						isLoading = false;
						mNoRecordMessageTV.setVisibility(View.GONE);
					}

				} else {
					isLoading = false;
					mNoRecordMessageTV.setVisibility(View.GONE);
					mNoMoreTV.setVisibility(View.INVISIBLE);
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
					// mErrorLayout.showError((null == data ?
					// getString(R.string.Something_went_wrong) : (String)
					// data), true, MsgType.Error);

				}
				mSwipeRefreshWidget.setRefreshing(false);
			}

		});
	}

	private void resetList() {
		mPageNumber = 1;
		mBlogList.clear();
		mBlogListAdapter.setList(mBlogList);
	}

	private void setHeader() {
		BaseFragmentActivity.setHeader(R.drawable.icon_menu, 0, getResources().getString(R.string.Blog), new OnClickListener() {
			@Override
			public void onClick(View v) {
				((DashboardActivity) v.getContext()).sliderListener();
			}
		}, null);
		DashboardActivity.hideShowActionBar(true);
	}

	private ErrorLayout mErrorLayout;

	private void initViews(View view) {
		mContext = getActivity();
		mErrorLayout = new ErrorLayout(view.findViewById(R.id.bg_container_rl));
		mBlogLV = (ListView) view.findViewById(R.id.blog_lv);
		mSearchBox = (EditText) view.findViewById(R.id.search_et);
		mLoadingCenter = (ProgressBar) view.findViewById(R.id.loading_center_pb);
		mNoRecordMessageTV = (TextView) view.findViewById(R.id.no_record_message_tv);
		mLoaderBottomPB = (ProgressBar) view.findViewById(R.id.loading_bottom_pb);
		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);

		mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);
		mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
		mNoMoreTV.setVisibility(View.INVISIBLE);
		mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
		mLoaderBottomPB.setVisibility(View.INVISIBLE);
		mLoaderBottomPB.getIndeterminateDrawable()
				.setColorFilter(getResources().getColor(R.color.red_normal_color), PorterDuff.Mode.SRC_IN);
		mBlogLV.addFooterView(mFooterRL);

		new SearchHandler(mSearchBox).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				mPageNumber = 1;
				getBlogList(mPageNumber, mSearchBox.getText().toString());
			}
		}).setClearView(view.findViewById(R.id.clear_text_ib));

		// mBlogListAdapter = new BlogListAdapter(mContext);
		mBlogListAdapter = new BlogListAdapter(mContext, new OnItemClickBlogList() {

			@Override
			public void onClickItems(int ID, int position, BlogModel blogmodel) {
				switch (ID) {
				case 5555: {
					startActivityForResult(BlogDetailActivity.getIntent(mContext, blogmodel.BlogGUID),
							BlogDetailActivity.REQ_CODE_BLOGDETAIL_SCREEN);
				}
					break;
				case R.id.comments_tv: {
					/*
					 * startActivityForResult(CommentListActivity.getIntentBlog(
					 * mContext, blogmodel.BlogGUID, blogmodel.Title),
					 * CommentListActivity.REQ_CODE_COMMENT_LIST);
					 * getActivity().
					 * overridePendingTransition(R.anim.slide_up_in,
					 * R.anim.slide_down_in);
					 */
				}
					break;

				case R.id.likes_tv: {
					/*
					 * startActivityForResult(LikeListActivity.getIntent(mContext
					 * , blogmodel.BlogGUID, CommentListRequest.ENTITYTYPE_BLOG,
					 * "BLOG", blogmodel.IsLike),
					 * LikeListActivity.REQ_FROM_HOME_LIKELIST);
					 */}
					break;

				default:
					break;
				}
			}
		});
		mBlogListRequest = new BlogListRequest(mContext);
		mBlogList = new ArrayList<BlogModel>();
		mBlogLV.setAdapter(mBlogListAdapter);
		mSwipeRefreshWidget.setColorScheme(R.color.red_normal_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		FontLoader.setRobotoRegularTypeface(mNoMoreTV, mNoRecordMessageTV);
		mBlogLV.setOnScrollListener(new MyCustomonScroll());
		// mBlogLV.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view, int
		// position, long id) {
		// startAc
		// }
		// });
	}

	@Override
	public void onClick(View arg0) {

	}

	@Override
	public void onRefresh() {
		mPageNumber = 1;
		getBlogList(mPageNumber, mSearchBox.getText().toString());
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case BlogDetailActivity.REQ_CODE_BLOGDETAIL_SCREEN:
			if (resultCode == Activity.RESULT_OK && null != data) {
				int updatePosition = 0;
				String blogGUID = data.getStringExtra("BLOG_GUID");
				for (int i = 0; i < mBlogList.size(); i++) {
					if (blogGUID.equals(mBlogList.get(i).BlogGUID)) {
						updatePosition = i;
						break;
					}
				}
				mBlogList.get(updatePosition).NoOfLikes = data.getIntExtra("LIKE_COUNT_BLOG", 0);
				mBlogList.get(updatePosition).NoOfComments = data.getIntExtra("COMMENT_COUNT_BLOG", 0);
				mBlogListAdapter.setList(mBlogList);
			}
			break;

		case CommentListActivity.REQ_CODE_COMMENT_LIST:
			if (resultCode == Activity.RESULT_OK && null != data) {

				int updatePosition = 0;
				String blogGUID = data.getStringExtra("BLOG_GUID");
				for (int i = 0; i < mBlogList.size(); i++) {
					if (blogGUID.equals(mBlogList.get(i).BlogGUID)) {
						updatePosition = i;
						break;
					}
				}
				mBlogList.get(updatePosition).NoOfComments = data.getIntExtra("COMMENT_COUNT_BLOG", 0);
				mBlogListAdapter.setList(mBlogList);

			}
			break;

		case LikeListActivity.REQ_FROM_HOME_LIKELIST:
			if (resultCode == Activity.RESULT_OK && null != data) {
				int updatePosition = 0;
				String blogGUID = data.getStringExtra("BLOG_GUID");
				for (int i = 0; i < mBlogList.size(); i++) {
					if (blogGUID.equals(mBlogList.get(i).BlogGUID)) {
						updatePosition = i;
						break;
					}
				}
				mBlogList.get(updatePosition).NoOfLikes = data.getIntExtra("LIKE_COUNT_BLOG", 0);
				mBlogListAdapter.setList(mBlogList);
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public int getConnectivityStatus(Context context) {
		if (null == context) {
			return 0;
		}
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
		if (null != activeNetwork && activeNetwork.isConnected()) {
			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
				return 1;
			}
			if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
				return 2;
			}
		}
		return 0;
	}

	boolean isLoading;

	class MyCustomonScroll implements OnScrollListener {
		private boolean bReachedListEnd;

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			if (bReachedListEnd && isLoading) {

				getBlogList(mPageNumber, mSearchBox.getText().toString());
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);

		}
	}

}