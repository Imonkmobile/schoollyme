package com.schollyme;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.activity.TranscriptsRequestFromCoachActivity;
import com.schollyme.adapter.LeftNavigatorAdapter;
import com.schollyme.blog.BlogFragment;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.fragments.HomeFragment;
import com.schollyme.fragments.SMTop10Fragment;
import com.schollyme.fragments.SettingsFragment;
import com.schollyme.fragments.UserProfileFragment;
import com.schollyme.friends.FriendsTopFragment;
import com.schollyme.inbox.InboxFragment;
import com.schollyme.model.LeftMenuModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.notifications.NotificationFragment;
import com.schollyme.search.SearchScreenActivity;
import com.schollyme.songofday.SongOfDayFragment;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.LogoutRequest;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.NotificationChangeSeenStatusRequest;
import com.vinfotech.request.NotificationCountRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class DashboardActivity extends BaseFragmentActivity implements OnClickListener {

	private Context mContext;
	private ListView mSlideLV;
	public static DrawerLayout mDrawerLayout;
	private FragmentManager mFragmentManager;
	private FragmentTransaction mFragmentTransaction;
	private NotificationCountModel mModel;
	private BaseFragment mBaseFragment = new BaseFragment();
	private LeftNavigatorAdapter mAdapter;
	private static View mHeaderLayout;
	private TextView mUserNameSliderTV, mUserStatusSliderTV;
	private ImageView mUserProfileSliderIV;
	public static int lastFrPosition = 0;
	private ArrayList<LeftMenuModel> mMenuAL;
	private int[] SLIDER_NORMAL_RESOURCES = { R.drawable.selector_music, R.drawable.selector_blogs, R.drawable.selector_blogs, R.drawable.selector_feeds,
			R.drawable.selector_inbox, R.drawable.selector_notification, R.drawable.selector_profile, R.drawable.selector_home_team,
			R.drawable.selector_settings,R.drawable.selector_logout };
	private LogedInUserModel mLogedInUserModel;
	private int fragmentToOpen = 4;
	public String mPostNotiLink = "";
	public String mFromActivity = "";

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		String mOtherProfileLink = intent.getStringExtra("UserProfileLink");
		if (TextUtils.isEmpty(mOtherProfileLink)) {
			Uri uri = intent.getData();
			if (uri != null) {
				mOtherProfileLink = uri.getQueryParameter("UserProfileLink");
			}

			if (lastFrPosition != 7) {
				switchFragment(7, "", "", 0);
			}

		} else {
			fragmentToOpen = getIntent().getIntExtra("FragmentToOpen", 4);
			if (getPreference("isLastBack") && fragmentToOpen != 4) {
				switchFragment(4, "", "", 0);
			} else
				switchFragment(fragmentToOpen, "", "", 0);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.navigation_drawer_layout);




		mContext = this;
		mFragmentManager = getSupportFragmentManager();
		mLogedInUserModel = new LogedInUserModel(this);
		controllInitialization();

		mPostNotiLink = getIntent().getStringExtra("UserProfileLink");
		mFromActivity = getIntent().getStringExtra("fromActivity");

		if (!TextUtils.isEmpty(mPostNotiLink)) {
			Uri uri = getIntent().getData();
			if (uri != null)
				mPostNotiLink = uri.getQueryParameter("UserProfileLink");

			switchFragment(4, mPostNotiLink, "", 0);
		} else {
			fragmentToOpen = getIntent().getIntExtra("FragmentToOpen", 4);
			switchFragment(fragmentToOpen, "", "", 0);
		}
		ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.icon_menu, R.string.app_name,
				R.string.app_name) {
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				// switchFragment(5,"");
			}

			// ** Called when a drawer has settled in a completely open state.
			// *//*
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getInAppNotificationCountRequest();
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	private void controllInitialization() {
		mSlideLV = (ListView) findViewById(R.id.left_drawer);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mHeaderLayout = findViewById(R.id.header_layout);
		setLeftNavigationSlider();
		setHeader(R.drawable.icon_menu, 0, getResources().getString(R.string.app_name), SliderListener, null);
	}

	/**
	 * Handel back press, when left slider is open and user presses back button
	 * then close drawer else super class method will be called
	 * */

	@Override
	public void onBackPressed() {
		if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
			mDrawerLayout.closeDrawers();
		} else {

			if (lastFrPosition == 4) {

				savePreferences(this, "isLastBack", true);
				super.onBackPressed();
			} else {
				lastFrPosition = 4;
				setSliderItem(4);
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUserData();
	}

	private ImageButton mSearchIB,mTranscriptsRequestIB;

	private void setLeftNavigationSlider() {

		int width = (int) (getResources().getDimension(R.dimen.space_mid10) * 16);
		ListView.LayoutParams rlp = new ListView.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, width);

		View header = (View) getLayoutInflater().inflate(R.layout.user_info_view, null);
		mSearchIB = (ImageButton) header.findViewById(R.id.search_ib);
		mSearchIB.setOnClickListener(this);
		mTranscriptsRequestIB = (ImageButton) header.findViewById(R.id.transcripts_ib);
		mTranscriptsRequestIB.setOnClickListener(this);
		
		header.setLayoutParams(rlp);
		String[] sliderOptions = getResources().getStringArray(R.array.slide_tab_options);
		ArrayList<String> sliderOptionsAL = new ArrayList<String>(Arrays.asList(sliderOptions));
		mMenuAL = LeftMenuModel.getLeftMenus(sliderOptionsAL);
		mAdapter = new LeftNavigatorAdapter(SLIDER_NORMAL_RESOURCES, mContext);
		mAdapter.setList(mMenuAL);
		if(mSlideLV.getHeaderViewsCount()==0)
		mSlideLV.addHeaderView(header, null, false);
		mSlideLV.setAdapter(mAdapter);
		mSlideLV.setOnItemClickListener(NavigationDrawerItemClickListener);
		mUserNameSliderTV = (TextView) header.findViewById(R.id.user_name_tv);
		mUserStatusSliderTV = (TextView) header.findViewById(R.id.user_type_tv);
		mUserProfileSliderIV = (ImageView) header.findViewById(R.id.profile_image_iv);
		if(new LogedInUserModel(mContext).mUserType==Config.USER_TYPE_ATHLETE){
			mTranscriptsRequestIB.setVisibility(View.VISIBLE);
		}
		else{
			mTranscriptsRequestIB.setVisibility(View.GONE);
		}
	}

	private void setUserData() {
		LogedInUserModel mModel = new LogedInUserModel(mContext);

		mUserNameSliderTV.setText(mModel.mFirstName + " " + mModel.mLastName);
		int mUserType = mModel.mUserType;
		String mUserTypeName = "";
		if (mUserType == 1) {
			mUserTypeName = String.valueOf(Config.UserType.Coach);
		} else if (mUserType == 2) {
			mUserTypeName = String.valueOf(Config.UserType.Athlete);
		} else if (mUserType == 3) {
			mUserTypeName = String.valueOf(Config.UserType.Fan);
		}
		FontLoader.setRobotoMediumTypeface(mUserNameSliderTV, mUserStatusSliderTV);
		mUserStatusSliderTV.setText(mUserTypeName);
		String imageUrl = Config.IMAGE_URL_PROFILE + mModel.mUserPicURL;
		ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mUserProfileSliderIV, ImageLoaderUniversal.option_Round_Image);
	}

	public static void hideShowActionBar(boolean flag) {
		if (flag) {
			mHeaderLayout.setVisibility(View.VISIBLE);
		} else {
			mHeaderLayout.setVisibility(View.GONE);
		}
	}

	public static Intent getIntent(Context context, int fragmentToOpen, String fromActivity) {
		Intent intent = new Intent(context, DashboardActivity.class);
		intent.putExtra("FragmentToOpen", fragmentToOpen);
		intent.putExtra("fromActivity", fromActivity);

		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		return intent;
	}

	private HomeFragment mHomeFragment;

	public void switchFragment(int position, String mActivityGUID, String mTeamGUID, int mType) {
		lastFrPosition = position;
		switch (position) {
		case 0:
			mBaseFragment = null;
			break;
		case 1:

			mBaseFragment = null;
			mBaseFragment = SongOfDayFragment.newInstance();
			break;
		case 2:
			mBaseFragment = BlogFragment.getInstance(mContext);

			break;
		case 3:
			mBaseFragment = SMTop10Fragment.getInstance();
			break;

		case 4:
			// if (null == mHomeFragment) {
			mHomeFragment = HomeFragment.getInstance(NewsFeedRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID);
			Bundle bundle1 = new Bundle();
			bundle1.putString("fromActivity", mFromActivity);
			bundle1.putString("mActivityGUID", mActivityGUID);
			bundle1.putString("mTeamGUID", mTeamGUID);
			bundle1.putInt("mNotificationType", mType);
			mHomeFragment.setArguments(bundle1);
			mHeaderLayout.setBackgroundColor(getResources().getColor(R.color.header_bg_color));
			// }

			mBaseFragment = mHomeFragment;
			break;
		case 5:
			mBaseFragment = InboxFragment.getInstance(mContext);
			mHeaderLayout.setBackgroundColor(getResources().getColor(R.color.header_bg_color));
			break;

		case 6:
			mBaseFragment = null;
			mBaseFragment = NotificationFragment.getInstance(mContext);
			markNotificationSeenRequest();
			break;

		case 7:
			mBaseFragment = UserProfileFragment.getInstance();
			Bundle bundle = new Bundle();
			bundle.putString("fromActivity", mFromActivity);
			bundle.putString("mActivityGUID", mActivityGUID);
			mBaseFragment.setArguments(bundle);
			mHeaderLayout.setBackgroundResource(R.drawable.bg_login_img);
			break;

		case 8:
			// startActivity(FriendsActivity.getIntent(mContext, 0));
			mBaseFragment = FriendsTopFragment.newInstance(1, new HeaderLayout(mHeaderLayout), false);
			mHeaderLayout.setBackgroundColor(getResources().getColor(R.color.header_bg_color));
			break;
		case 9:
			mBaseFragment = SettingsFragment.getInstance();
		  	break;

		case 10:
			mBaseFragment = null;
			logoutUserRequest();
			break;
		default:
			break;
		}

		if (mBaseFragment != null) {
			savePreferences(this, "isLastBack", false);
			mFragmentManager = getSupportFragmentManager();
			mFragmentTransaction = mFragmentManager.beginTransaction();
			mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
			mFragmentTransaction.commit();
		}
	}

	private void logoutUserRequest() {

		DialogUtil.showOkCancelDialog(mContext, getResources().getString(R.string.app_name),
				getResources().getString(R.string.logout_confirm), new OnClickListener() {

					@Override
					public void onClick(View v) {
						LogoutRequest mRequest = new LogoutRequest(mContext);
						mRequest.LogoutServerRequest();
						mRequest.setRequestListener(new RequestListener() {

							@Override
							public void onComplete(boolean success, Object data, int totalRecords) {
								if (success) {
									AuthenticationClient.logout(mContext);
									LogedInUserModel mModel = new LogedInUserModel(mContext);
									mModel.removeUserData(mContext);
									Intent intent = LoginActivity.getIntent(mContext);
									intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
											| Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(intent);
									((SchollyMeApplication)getApplication()).cancelNotifications(false);
									finish();
								} else {
									DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
											getResources().getString(R.string.app_name), null);
								}
							}
						});
					}
				}, new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

		mAdapter.notifyDataSetChanged();
		mSlideLV.clearChoices();
		mSlideLV.setOnItemClickListener(NavigationDrawerItemClickListener);
	}

	/** Listeners */

	OnClickListener SliderListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			v.setBackgroundColor(getResources().getColor(R.color.selector_menu_bgcolor));
			sliderListener();
		}
	};

	public void sliderListener() {

		getInAppNotificationCountRequest();
		if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
			mDrawerLayout.closeDrawers();
		} else {
			mDrawerLayout.openDrawer(GravityCompat.START);
		}
		/*
		 * if(lastFrPosition==5 && mModel.mTotalNotificationRecords>0 ){
		 * switchFragment(5, ""); }
		 */
	}

	OnItemClickListener NavigationDrawerItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			setSliderItem(position);
		}
	};

	private void setSliderItem(int position) {
		if (position >= 0) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
						mDrawerLayout.closeDrawers();
					}
				}
			}, 300);
			switchFragment(position, "", "", 0);
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.search_ib: {
			startActivity(SearchScreenActivity.getIntent(DashboardActivity.this));

			this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		}
			break;

			
		case R.id.transcripts_ib:
			startActivity(TranscriptsRequestFromCoachActivity.getIntent(mContext,""));
			break;
		default:
			break;
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		startNotiDataRefresh();
	}

	@Override
	protected void onStop() {
		mHandler.removeCallbacksAndMessages(null);
		getIntent().removeExtra(null);
		super.onStop();
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
	}

	private Handler mHandler = new Handler();

	private void startNotiDataRefresh() {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				startNotiDataRefresh();
			}
		}, Config.NOTIFICATION_UPDATE_DELAY);
		getInAppNotificationCountRequest();
	}

	private NotificationCountRequest mNotificationCountRequest;

	public void getInAppNotificationCountRequest() {
		if (null == mNotificationCountRequest) {
			mNotificationCountRequest = new NotificationCountRequest(mContext);
		}

		mNotificationCountRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (null != mMenuAL) {
					try {
						mModel = (NotificationCountModel) data;
						LeftMenuModel inbox = mMenuAL.get(4);
						inbox.mOptionMenuCount = mModel.mTotalMessageRecords;
						mMenuAL.set(4, inbox);

						LeftMenuModel notification = mMenuAL.get(5);
						notification.mOptionMenuCount = mModel.mTotalNotificationRecords;
						mMenuAL.set(5, notification);

						LeftMenuModel friendRequest = mMenuAL.get(7);
						friendRequest.mOptionMenuCount = mModel.mTotalPendingFriendRecords;
						mMenuAL.set(7, friendRequest);
						if (null != mAdapter) {
							mAdapter.setList(mMenuAL);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
		mNotificationCountRequest.getNotificationCountServerRequest();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (null != mHomeFragment) {
			mHomeFragment.onActivityResult(requestCode, resultCode, data);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void savePreferences(Context context, String key, boolean value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("BACK_PREF", context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public boolean getPreference(String key) {
		SharedPreferences shared = getSharedPreferences("BACK_PREF", MODE_PRIVATE);
		return shared.getBoolean(key, false);
	}

	/**
	 * This api will mark notification as seen
	 * */
	private void markNotificationSeenRequest() {
		NotificationChangeSeenStatusRequest mRequest = new NotificationChangeSeenStatusRequest(mContext);
		
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				Log.i("Notification Mark Seen Status", "Users All Notification are marked as seen ::" + data.toString());
				// Do nothing
			}
		});
		mRequest.getNotificationSeenStatusServerRequest();
	}

	@Override
	public void onLocaleUpdate() {
		super.onLocaleUpdate();
		setLeftNavigationSlider();
		if(null!=mBaseFragment){
			mBaseFragment.onLocaleUpdate();
		}
	}
}