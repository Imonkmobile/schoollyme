package com.schollyme;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.schollyme.model.LogedInUserModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.FontLoader;

/**
 * Base activity class. This may be useful in<br/>
 * Implementing google analytics or <br/>
 * Any app wise implementation
 * 
 * @author ravi
 * 
 */
public abstract class BaseFragmentActivity extends BaseLocaleActivity {
	
	private static TextView actionBarTitle;
	private static ImageView rightButton ;
	private static ImageView leftButton;
	private static ProgressBar loadingPb;
	private static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		if(TextUtils.isEmpty(BaseRequest.getLoginSessionKey())){
			BaseRequest.setLoginSessionKey(new LogedInUserModel(this).mLoginSessionKey);
		}
	}

	public static void setHeader(int leftButtonId,int rightButtonId,String title,OnClickListener leftButtonListener,OnClickListener rightButtonListener){
		
		actionBarTitle = (TextView) ((Activity) context).findViewById(R.id.header_tv);
		rightButton = (ImageView) ((Activity) context).findViewById(R.id.right_button);
		leftButton = (ImageView)  ((Activity) context).findViewById(R.id.back_button);
		loadingPb = (ProgressBar) ((Activity) context).findViewById(R.id.loading_h_pb);
		actionBarTitle.setText(title);
		leftButton.setImageResource(leftButtonId);
		rightButton.setImageResource(rightButtonId);
		rightButton.setOnClickListener(rightButtonListener);
		leftButton.setOnClickListener(leftButtonListener);
		FontLoader.setRobotoBoldTypeface(actionBarTitle);
		rightButton.setVisibility(rightButtonId > 0 ? View.VISIBLE : View.GONE);
	}
	
	public static void setHeaderTitle(String title){
		actionBarTitle.setText(title);
	}
	
	public static void hideRightButton(){
		rightButton.setVisibility(View.INVISIBLE);
	}

	public static void showLoader(boolean show){
		loadingPb.setVisibility(show ? View.VISIBLE : View.GONE);
		rightButton.setVisibility(show ? View.GONE : View.VISIBLE);
	}

	@Override
	public void onLocaleUpdate() {

	}
}