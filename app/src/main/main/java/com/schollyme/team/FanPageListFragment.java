package com.schollyme.team;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.friends.BaseFriendFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.Teams;
import com.vinfotech.request.TeamListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.FontLoader;

public class FanPageListFragment extends BaseFriendFragment implements OnClickListener, OnRefreshListener{

	private static Context mContext;
	private ListView mTeamLV;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private LinearLayout mLinearLayout;
	private EditText mTeamSearchET;
	private ImageButton mClearIB;
	private TextView mNoRecordTV;
	private OfficialTeamAdapter mFanPageAdapter;
	private ArrayList<Teams> mFansPageAL;
	private int pageIndex = 1;
	private boolean loadingFlag = false;
	private static ErrorLayout mErrorLayout;
	private View view;

	public static FanPageListFragment newInstance(ErrorLayout mLayout) {
		mErrorLayout = mLayout;
		return new FanPageListFragment();
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.team_fragment, null);
		mContext = getActivity();
		mTeamLV = (ListView) view.findViewById(R.id.team_lv);
		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		mTeamSearchET = (EditText) view.findViewById(R.id.search_et);
		mClearIB = (ImageButton) view.findViewById(R.id.clear_text_ib);
		mLinearLayout = (LinearLayout) view.findViewById(R.id.search_view_ll);
		mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
		mLinearLayout.setVisibility(View.VISIBLE);
		mClearIB.setOnClickListener(this);
		FontLoader.setRobotoRegularTypeface(mTeamSearchET,mNoRecordTV);
		pageIndex = 1;
		mFansPageAL = new ArrayList<Teams>();

		mTeamSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					return true;
				}
				return false;
			}
		});

		new SearchHandler(mTeamSearchET).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				if(mFanPageAdapter!=null){
					if(TextUtils.isEmpty(text)){
						getFansRequest("");
					}
					else{
						getSearchFansRequest(text);
					}
				}
			}
		}).setClearView(mClearIB);

		return view;
	}

	private void setTeamAdapter(){

		mFanPageAdapter = new OfficialTeamAdapter(mContext,mErrorLayout,new AdapterRefreshListener() {

			@Override
			public void onRefresh(int listSize) {
				if(listSize==0){
					mNoRecordTV.setVisibility(View.VISIBLE);
				}
				else{
					mNoRecordTV.setVisibility(View.GONE);
				}
			}
		});
		if(mFansPageAL.size()>0){
			mNoRecordTV.setVisibility(View.GONE);

			mFanPageAdapter.setList(mFansPageAL);
			mTeamLV.setAdapter(mFanPageAdapter);
			mTeamLV.setVisibility(View.VISIBLE);
			mTeamLV.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					if(mFansPageAL.size()>0){
						String mTeamGUID = mFansPageAL.get(position).mTeamGUID;
						startActivity(TeamPageActivity.getIntent(mContext,mTeamGUID,"",position));
					}
				}
			});
			mTeamLV.setOnScrollListener(new OnScrollListener() {
				boolean bReachedListEnd = false;
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
						Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
						if (null != mFanPageAdapter&& !loadingFlag) {
							pageIndex++;
							loadingFlag = true;
							getFansRequest("");
						}
					}
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
					bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount/2));
				}
			});
		}
		else{
			mTeamLV.setAdapter(mFanPageAdapter);
			mFanPageAdapter.setList(mFansPageAL);
			mNoRecordTV.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		pageIndex = 1;
		mFansPageAL = new ArrayList<Teams>();
		getFansRequest(mTeamSearchET.getText().toString().trim());
	}

	@Override
	public void onRefresh() {
			pageIndex = 1;
			mFansPageAL = new ArrayList<Teams>();
			getFansRequest(mTeamSearchET.getText().toString().trim());
	}

	@Override
	public void onReload() {
		if(null != mTeamLV){
			mTeamLV.post(new Runnable() {

				@Override
				public void run() {
					onRefresh();
				}
			});
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.clear_text_ib:
			mTeamSearchET.setText("");
			break;

		default:
			break;
		}
	}

	private void getFansRequest(String mSearchText){

		TeamListRequest mRequest = new TeamListRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				loadingFlag = false;
				mSwipeRefreshWidget.setRefreshing(false);

				if(success){
					mFansPageAL.addAll((ArrayList<Teams>) data);
				}
				else{
					mFansPageAL = new ArrayList<Teams>();
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
				if(mFansPageAL.size()==0){
					mTeamLV.setAdapter(mFanPageAdapter);
					if(mFanPageAdapter!=null)
					mFanPageAdapter.setList(mFansPageAL);
					mNoRecordTV.setVisibility(View.VISIBLE);
				}
				else{
					if(mFanPageAdapter==null){
						setTeamAdapter();
					}
					else{
						mFanPageAdapter.setList(mFansPageAL);
						mNoRecordTV.setVisibility(View.GONE);
					}
				}
			}
		});
		mRequest.TeamListServerRequest(pageIndex,mSearchText,"2");
	}

	private void getSearchFansRequest(String mSearchText){

		TeamListRequest mRequest = new TeamListRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				loadingFlag = false;
				mSwipeRefreshWidget.setRefreshing(false);
				if(success){
					mFansPageAL = new ArrayList<Teams>();
					mFansPageAL.addAll((ArrayList<Teams>) data);
				}
				else{
					mFansPageAL = new ArrayList<Teams>();
				}
				if(mFansPageAL.size()==0){
					mTeamLV.setAdapter(mFanPageAdapter);
					mFanPageAdapter.setList(mFansPageAL);
					mNoRecordTV.setVisibility(View.VISIBLE);
				}
				else{
					if(mFanPageAdapter==null){
						setTeamAdapter();
					}
					else{
						mFanPageAdapter.setList(mFansPageAL);
						mNoRecordTV.setVisibility(View.GONE);
					}
				}
			}
		});
		mRequest.TeamListServerRequest(pageIndex,mSearchText,"2");
	}

	public interface AdapterRefreshListener{
		public void onRefresh(int listSize);
	}
}