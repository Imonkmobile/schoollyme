package com.schollyme.team;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;

import com.schollyme.BaseActivity;
import com.schollyme.R;
import com.schollyme.friends.BaseFriendFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.UserModel;
import com.vinfotech.request.AboutRequest;
import com.vinfotech.request.GetPageCategoryRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.widget.PagerSlidingTabStrip;

public class TeamActivity extends BaseActivity{

	private static final int PAGE_COUNT = 3;
	private PagerSlidingTabStrip mPagerSlidingTabStrip;
	private ViewPager mGenericVp;
	private GridPagerAdapter mGridPagerAdapter;
	private ErrorLayout mErrorLayout;
	private Context mContext;
	private int IsCreateOfficialPage = 1;
	private UserModel mUserModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.team_activity);
		mContext = this;
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_add_page_header, getResources().getString(R.string.team_header),
				new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if(new LogedInUserModel(mContext).mUserType==1){
					final String[] options = (IsCreateOfficialPage == 1 ? new String[]{getString(R.string.official_team_page), getString(R.string.fan_page)} : new String[]{getString(R.string.fan_page)});
					if(options.length==2){
						DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {

							@Override
							public void onItemClick(int position, String item) {
								String mPageTypeSelected = (options.length == 2 ? Integer.toString(position + 1) : "2");
								startActivity(CreatePageActivity.getIntent(mContext, mPageTypeSelected));
							}

							@Override
							public void onCancel() {

							}

						}, options);
					}
					else{
						String mPageTypeSelected = "2";
						startActivity(CreatePageActivity.getIntent(mContext, mPageTypeSelected));
					}

				} else {
					String mPageTypeSelected  = "2";
					startActivity(CreatePageActivity.getIntent(mContext,mPageTypeSelected));
				}
			}
		});

		mGridPagerAdapter = new GridPagerAdapter(getSupportFragmentManager());
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_container_ll));

		mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.sliding_tabs_psts);
		mGenericVp = (ViewPager) findViewById(R.id.generic_vp);
		mGenericVp.setOffscreenPageLimit(1);
		mGenericVp.setAdapter(mGridPagerAdapter);
		mPagerSlidingTabStrip.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				BaseFriendFragment baseFriendFragment = mGridPagerAdapter.getBaseFriendFragment(position);
				if(baseFriendFragment!=null)
					baseFriendFragment.onReload();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int position) {

			}
		});

		mPagerSlidingTabStrip.setViewPager(mGenericVp);
		int currentPageToShow = getIntent().getIntExtra("currentPageToShow", 0);
		mGenericVp.setCurrentItem(currentPageToShow);
//		getPageCategoryServerRequest();
	}

	@Override
	protected void onResume() {
		super.onResume();
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				AboutServerRequest(new LogedInUserModel(mContext).mUserProfileURL);
			}
		}, 500);
	}


	public static Intent getIntent(Context context, int currentPageToShow) {
		Intent intent = new Intent(context, TeamActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("currentPageToShow", currentPageToShow);
		return intent;
	}

	public class GridPagerAdapter extends FragmentPagerAdapter {
		private Map<Integer, BaseFriendFragment> mBaseFriendFragments;

		@SuppressLint("UseSparseArrays") public GridPagerAdapter(FragmentManager fm) {
			super(fm);
			mBaseFriendFragments = new HashMap<Integer, BaseFriendFragment>();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			String tabTitle = "";
			switch (position) {
			case 0:
				tabTitle = getString(R.string.official_tab);
				break;
			case 1:
				tabTitle = getString(R.string.fan_tab);
				break;
			case 2:
				tabTitle = getString(R.string.suggested_tab);
				break;

			}
			return tabTitle;
		}

		@Override
		public int getCount() {
			return PAGE_COUNT;
		}

		@Override
		public Fragment getItem(int position) {
			BaseFriendFragment mFragment = null;
			switch (position) {
			case 0:
				mFragment = OfficialPageListFragment.newInstance(mErrorLayout);
				break;
			case 1:
				mFragment = FanPageListFragment.newInstance(mErrorLayout);
				break;
			case 2:
				mFragment = SuggestedPageListFragment.newInstance(mErrorLayout);
				break;
			}
			mBaseFriendFragments.put(position, mFragment);
			return mFragment;
		}

		public BaseFriendFragment getBaseFriendFragment(int position) {
			return mBaseFriendFragments.get(position);
		}
	}

	private void getPageCategoryServerRequest(){
		GetPageCategoryRequest mRequest = new GetPageCategoryRequest(mContext);

		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

			}
		});
		mRequest.GetPageCategoryServerRequest("1");
	}

	private void AboutServerRequest(String mUserGUID) {
		AboutRequest mRequest = new AboutRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {
					mUserModel = (UserModel) data;
					IsCreateOfficialPage = mUserModel.IsCreateOfficialPage;
				} else {
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.AboutRequestServer(new LogedInUserModel(mContext).mLoginSessionKey, mUserGUID);
	}
}