package com.schollyme.team;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.PageMemberUser;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class InviteToUserAdapter extends BaseAdapter{

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private ArrayList<PageMemberUser> mSuggestedFriendsAL;
	private FriendsSuggestionViewHolder mFriendsSuggestionViewHolder;
	String teammateOptionString;

	public InviteToUserAdapter(Context context){
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(ArrayList<PageMemberUser> mList){
		this.mSuggestedFriendsAL = mList;
		notifyDataSetChanged();
	}
	
	public ArrayList<PageMemberUser> getSelections() {
		ArrayList<PageMemberUser> selections = new ArrayList<PageMemberUser>();
		if (null != mSuggestedFriendsAL) {
			for (PageMemberUser pageMemberModel : mSuggestedFriendsAL) {
				if (pageMemberModel.isSelected) {
					selections.add(pageMemberModel);
				}
			}
		}
		return selections;
	}

	public void select(boolean all) {
		for (PageMemberUser pageMemberModel : mSuggestedFriendsAL) {
			pageMemberModel.isSelected = all;
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null==mSuggestedFriendsAL?0:mSuggestedFriendsAL.size();
	}

	@Override
	public PageMemberUser getItem(int arg0) {
		return mSuggestedFriendsAL.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup parent) {
		mFriendsSuggestionViewHolder = new FriendsSuggestionViewHolder();
		
		if(contentView==null){
			contentView = mLayoutInflater.inflate(R.layout.message_touserlist_adapter, parent,false);
			mFriendsSuggestionViewHolder.mFriendNameTV = (TextView) contentView.findViewById(R.id.user_name_tv);
			mFriendsSuggestionViewHolder.mFriendIV = (ImageView) contentView.findViewById(R.id.profile_image_iv);
			mFriendsSuggestionViewHolder.mSelectIB = (ImageButton) contentView.findViewById(R.id.select_ib);
			FontLoader.setRobotoRegularTypeface(mFriendsSuggestionViewHolder.mFriendNameTV);
			contentView.setTag(mFriendsSuggestionViewHolder);
		}
		else{
			mFriendsSuggestionViewHolder = (FriendsSuggestionViewHolder) contentView.getTag();
		}
		
		mFriendsSuggestionViewHolder.mFriendNameTV.setTag(position);
		mFriendsSuggestionViewHolder.mFriendNameTV.setText(mSuggestedFriendsAL.get(position).mUserFName+" "+mSuggestedFriendsAL.get(position).mUserLName);

		if (mSuggestedFriendsAL.get(position).isSelected) {
			mFriendsSuggestionViewHolder.mSelectIB.setVisibility(View.VISIBLE);
		} else {
			mFriendsSuggestionViewHolder.mSelectIB.setVisibility(View.GONE);
		}
		
		String imageUrl  = Config.IMAGE_URL_PROFILE+mSuggestedFriendsAL.get(position).mUserProflePic;
		
		ImageLoaderUniversal.ImageLoadRound(mContext,imageUrl, mFriendsSuggestionViewHolder.mFriendIV, ImageLoaderUniversal.option_Round_Image);
		
		contentView.setClickable(false);
		return contentView;
	}

	private class FriendsSuggestionViewHolder{
		private TextView mFriendNameTV;
		private ImageView mFriendIV;
		public ImageButton mSelectIB;
	}
}