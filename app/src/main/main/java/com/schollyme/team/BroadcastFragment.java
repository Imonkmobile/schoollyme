package com.schollyme.team;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.adapter.BroadcastMemberAdapter;
import com.schollyme.adapter.BroadcastMemberAdapter.SelectionListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.MessageMember;
import com.vinfotech.request.MessageMemberRequest;
import com.vinfotech.request.SendMessageRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

public class BroadcastFragment extends Fragment implements OnRefreshListener {
	private static final String TAG = BroadcastFragment.class.getSimpleName();

	private TextView mNoRecordTV;
	private EditText mWriteMessageEt;
	private ImageButton mSendMessageIb;
	private ImageView mSelectionIv;
	private TextView mAllTitleTv;

	private View mLoaderPb;
	private View mEmptyView;
	private View mHeaderView, mFooterView;

	private MessageMemberRequest mMessageMemberRequest;
	private SendMessageRequest mSendMessageRequest;
	private BroadcastMemberAdapter mBroadcastMemberAdapter;

	private ErrorLayout mErrorLayout;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private SearchHandler mSearchHandler;
	private OnTextChangeListener mOnTextChangeListener;

	private String mGroupGUID, mGroupName, mFilter, mSearchText = "";
	private String mMsgText = null;
	private boolean mSelAll = false;
	private List<String> mSelectedMembers;

	private List<MessageMember> mMessageMembers = new ArrayList<MessageMember>();
	private int mPageNo = Config.DEFAULT_PAGE_INDEX;
	private boolean mFetchedAll = true;

	public static BroadcastFragment newInstance(ErrorLayout errorLayout, String groupGUID, String groupName, String filter,
			OnTextChangeListener listener) {
		BroadcastFragment broadcastFragment = new BroadcastFragment();
		broadcastFragment.mErrorLayout = errorLayout;
		broadcastFragment.mGroupGUID = groupGUID;
		broadcastFragment.mGroupName = groupName;
		broadcastFragment.mFilter = filter;
		broadcastFragment.mOnTextChangeListener = listener;
		return broadcastFragment;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.broadcast_fragment, null);

		mFooterView = inflater.inflate(R.layout.footer_loader_layout, null);
		mHeaderView = inflater.inflate(R.layout.broadcast_member_header_layout, null);
		mSelectionIv = (ImageView) mHeaderView.findViewById(R.id.selection_iv);
		mHeaderView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSelAll = !mSelAll;
				mBroadcastMemberAdapter.select(mSelAll ? -2 : -1);
				if (!mSelAll && null != mSelectedMembers) {
					mSelectedMembers.clear();
				}
				updateSelectAllBtnState();
				updateSendBtnState();
			}
		});
		mAllTitleTv = (TextView) mHeaderView.findViewById(R.id.title_tv);
		if (MessageMemberRequest.FILTER_ALL == mFilter) {
			mAllTitleTv.setText(R.string.All);
		} else if (MessageMemberRequest.FILTER_ATHLETE == mFilter) {
			mAllTitleTv.setText(R.string.All);
		} else if (MessageMemberRequest.FILTER_PARENT == mFilter) {
			mAllTitleTv.setText(R.string.All);
		}
		mMessageMembers.clear();
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mBroadcastMemberAdapter = new BroadcastMemberAdapter(view.getContext(), new SelectionListener() {

			@Override
			public void onSelectionChange(List<String> selections) {
				mSelectedMembers = selections;
				updateSendBtnState();
				mSelAll = (mSelectedMembers.size() == mMessageMembers.size());
				if (mSelAll) {
					mBroadcastMemberAdapter.select(-2);
				}
				updateSelectAllBtnState();
			}
		});

		mFooterView.setVisibility(View.GONE);
		mLoaderPb = view.findViewById(R.id.loading_pb);
		mEmptyView = view.findViewById(R.id.empty_tv);

		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);

		EditText mSearchEt = (EditText) view.findViewById(R.id.search_et);
		mSearchHandler = new SearchHandler(mSearchEt);
		mSearchHandler.setClearView(view.findViewById(R.id.clear_text_ib));
		mSearchHandler.setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				mSearchText = text;
				if (!TextUtils.isEmpty(text)) {
					mSelectedMembers = mBroadcastMemberAdapter.getSelections();
					//mHeaderView.setVisibility(View.GONE);
					updateHeaderVisivility(false);
				} else {
					//mHeaderView.setVisibility(View.VISIBLE);
					updateHeaderVisivility(true);
				}
				initRequest();
			}
		});

		mWriteMessageEt = (EditText) view.findViewById(R.id.write_message_et);
		mWriteMessageEt.setText(mMsgText);
		mWriteMessageEt.addTextChangedListener(mTextWatcher);
		mSendMessageIb = (ImageButton) view.findViewById(R.id.send_message_ib);
		mSendMessageIb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String formattedMsg = getString(R.string.For) + " " + mGroupName + ": " + mMsgText;
				if (mSelAll) {
					mSendMessageRequest.sendMessageInServer(mGroupGUID, formattedMsg, mFilter, null);
				} else {
					mSendMessageRequest.sendMessageInServer(mGroupGUID, formattedMsg, "", mSelectedMembers);
				}
			}
		});

		ListView genericLv = (ListView) view.findViewById(R.id.users_lv);
		genericLv.addHeaderView(mHeaderView);
		genericLv.addFooterView(mFooterView);
		genericLv.setAdapter(mBroadcastMemberAdapter);
		genericLv.setOnScrollListener(new OnScrollListener() {
			private boolean bReachedListEnd;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (Config.DEBUG) {
					Log.d(TAG, "onScrollStateChanged bReachedListEnd=" + bReachedListEnd + ", mFetchedAll=" + mFetchedAll
							+ ", scrollState=" + scrollState);
				}
				if (bReachedListEnd && !mFetchedAll) {
					mFetchedAll = true;
					mPageNo++;

					mMessageMemberRequest.setLoader(mFooterView);
					mMessageMemberRequest.getMessageMemberInServer(mSearchText, mGroupGUID, mFilter, mPageNo);
				}
			}

			@Override
			public void onScroll(AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
				bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);
				if (Config.DEBUG) {
					Log.d(TAG, "onScroll bReachedListEnd=" + bReachedListEnd + ", firstVisibleItem=" + firstVisibleItem
							+ ", visibleItemCount=" + visibleItemCount + ", totalItemCount=" + totalItemCount);
				}
			}
		});
		genericLv.post(new Runnable() {

			@Override
			public void run() {
				initRequest();
			}
		});

		FontLoader.setRobotoRegularTypeface(mNoRecordTV, mEmptyView, mAllTitleTv, mWriteMessageEt, mSearchEt);
	}

	private TextWatcher mTextWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			mMsgText = mWriteMessageEt.getText().toString().trim();

			if (null != mOnTextChangeListener) {
				mOnTextChangeListener.onTextChange(mMsgText);
			}
			updateSendBtnState();
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};
	
	private void updateHeaderVisivility(boolean visible){
		mHeaderView.setVisibility(visible ? View.VISIBLE : View.GONE);
		mHeaderView.findViewById(R.id.selection_iv).setVisibility(visible ? View.VISIBLE : View.GONE);
		mHeaderView.findViewById(R.id.title_tv).setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onRefresh() {
		initRequest();
	}

	private void initRequest() {
		if (null != mMessageMemberRequest) {
			mMessageMemberRequest.setActivityStatus(false);
		}
		mPageNo = Config.DEFAULT_PAGE_INDEX;
		mMessageMemberRequest = new MessageMemberRequest(getActivity());
		mMessageMemberRequest.setLoader(mSwipeRefreshWidget.isRefreshing() ? null : mLoaderPb);
		mMessageMemberRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mSwipeRefreshWidget.setRefreshing(false);
				if (success) {
					if (mPageNo <= 1) {
						mMessageMembers.clear();
					}
					List<MessageMember> list = (List<MessageMember>) data;
					mMessageMembers.addAll(list);
					if (null != mSelectedMembers && mSelectedMembers.size() > 0) {
						for (String UserGUID : mSelectedMembers) {
							for (MessageMember messageMember : mMessageMembers) {
								if (UserGUID.equalsIgnoreCase(messageMember.UserGUID)) {
									messageMember.selected = true;
								}
							}
						}
						mSelAll = false;
						updateSelectAllBtnState();
						updateSendBtnState();
					} else if (mSelAll) {
						for (MessageMember messageMember : list) {
							messageMember.selected = true;
						}
					}
					mBroadcastMemberAdapter.setList(mMessageMembers);
					mEmptyView.setVisibility(mMessageMembers.size() < 1 ? View.VISIBLE : View.GONE);
					//mHeaderView.setVisibility(mMessageMembers.size() >= 1 && TextUtils.isEmpty(mSearchText) ? View.VISIBLE : View.GONE);
					updateHeaderVisivility(mMessageMembers.size() >= 1 && TextUtils.isEmpty(mSearchText));
					mFetchedAll = (list.size() < Config.PAGE_SIZE);
					mFooterView.setVisibility(mFetchedAll ? View.GONE : View.VISIBLE);
				} else {
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
				}
			}
		});
		mMessageMemberRequest.getMessageMemberInServer(mSearchText, mGroupGUID, mFilter, mPageNo);

		if (null == mSendMessageRequest) {
			mSendMessageRequest = new SendMessageRequest(getActivity());
			mSendMessageRequest.setRequestListener(new RequestListener() {

				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					if (success) {
						mErrorLayout.showError(getString(R.string.Message_sent_successfully), true, MsgType.Success);
						mWriteMessageEt.setText(null);
						mBroadcastMemberAdapter.select(-1);
						mSelAll = false;
						updateSelectAllBtnState();
					} else {
						mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true,
								MsgType.Error);
					}
				}
			});
		}
	}

	private void updateSelectAllBtnState() {
		mSelectionIv.setImageResource(mSelAll ? R.drawable.icon_red_check : R.drawable.icon_grey_check);
	}

	private void updateSendBtnState() {
		boolean valid = !TextUtils.isEmpty(mMsgText);
		if (valid) {
			valid = (mSelAll || (null != mSelectedMembers && mSelectedMembers.size() > 0));
		}
		mSendMessageIb.setEnabled(valid);
		mSendMessageIb.setImageResource(valid ? R.drawable.ic_send : R.drawable.ic_send_disable);
	}

	public void setSearchText(String text) {
		mMsgText = text;
		mWriteMessageEt.post(new Runnable() {

			@Override
			public void run() {
				Log.d("BroadcastFragment", "setSearchText mFilter=" + mFilter + ", mMsgText=" + mMsgText);
				mWriteMessageEt.removeTextChangedListener(mTextWatcher);
				mWriteMessageEt.setText(mMsgText);
				mWriteMessageEt.addTextChangedListener(mTextWatcher);
			}
		});
	}

	public void clearSelection() {
		if (null != mWriteMessageEt) {
			mWriteMessageEt.postDelayed(new Runnable() {

				@Override
				public void run() {
					mSelAll = false;
					if (null != mSelectedMembers) {
						mSelectedMembers.clear();
					}
					mBroadcastMemberAdapter.select(-1);
					updateSelectAllBtnState();
					updateSendBtnState();
				}
			}, 250);
		}
	}

	public interface OnTextChangeListener {
		void onTextChange(String text);
	}
}