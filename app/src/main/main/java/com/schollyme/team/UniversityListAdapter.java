package com.schollyme.team;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.schollyme.R;
import com.schollyme.model.UniversityModel;
import com.vinfotech.utility.FontLoader;

public class UniversityListAdapter extends BaseAdapter {

	private UniversityViewHolder mUniversityViewHolder;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<UniversityModel> mUniversityAL;
	private List<UniversityModel> mFilteredUniversity = new ArrayList<UniversityModel>();

	public UniversityListAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(List<UniversityModel> mAList) {
		this.mUniversityAL = mAList;
		setFilter(null);
	}

	public ArrayList<UniversityModel> getSelections() {
		ArrayList<UniversityModel> selections = new ArrayList<UniversityModel>();
		if (null != mUniversityAL) {
			for (UniversityModel universityModel : mUniversityAL) {
				if (universityModel.isSelected) {
					selections.add(universityModel);
				}
			}
		}
		return selections;
	}

	@Override
	public int getCount() {
		return null == mFilteredUniversity ? 0 : mFilteredUniversity.size();
	}

	@Override
	public UniversityModel getItem(int position) {
		return mFilteredUniversity.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		mUniversityViewHolder = new UniversityViewHolder();
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_item_view, parent, false);
			mUniversityViewHolder.mTitleTV = (TextView) convertView.findViewById(R.id.title_tv);
			mUniversityViewHolder.mSelectIB = (ImageButton) convertView.findViewById(R.id.select_ib);
			convertView.setTag(mUniversityViewHolder);
		} else {
			mUniversityViewHolder = (UniversityViewHolder) convertView.getTag();
		}
		mUniversityViewHolder.mTitleTV.setText(mFilteredUniversity.get(position).mUniversityName);
		mUniversityViewHolder.mTitleTV.setTag(position);
		if (mFilteredUniversity.get(position).isSelected) {
			mUniversityViewHolder.mSelectIB.setVisibility(View.VISIBLE);
		} else {
			mUniversityViewHolder.mSelectIB.setVisibility(View.GONE);
		}
		FontLoader.setRobotoRegularTypeface(mUniversityViewHolder.mTitleTV);
		convertView.setClickable(false);

		return convertView;
	}

	private class UniversityViewHolder {
		public TextView mTitleTV;
		public ImageButton mSelectIB;
	}
	
	public void setFilter(String text) {
		mFilteredUniversity.clear();

		if (null != mUniversityAL) {
			if (TextUtils.isEmpty(text)) {
				mFilteredUniversity.addAll(mUniversityAL);
			} else {
				for (UniversityModel universityModel : mUniversityAL) {
					if (universityModel.mUniversityName.toLowerCase().startsWith(text.toLowerCase()) || universityModel.mUniversityName.toLowerCase().contains((" "+text.toLowerCase()))) {
						mFilteredUniversity.add(universityModel);
					}
				}
			}
		}
		notifyDataSetChanged();
	}
}