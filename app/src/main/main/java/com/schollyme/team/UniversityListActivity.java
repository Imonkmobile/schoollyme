package com.schollyme.team;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.schollyme.BaseActivity;
import com.schollyme.R;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.UniversityModel;
import com.vinfotech.request.GetUniversityRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class UniversityListActivity extends BaseActivity implements OnClickListener,OnRefreshListener {

	private ListView mOptionsLV;
	private AutoCompleteTextView mSearchACTV;
	private LinearLayout mContainerLl;
	private LinearLayout mContainerRl;
	private Context mContext;
	private UniversityListAdapter mUniversityAdapter;
	private UniversityModel mSelections = null;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private int mPageIndex = 1;
	private boolean loadingFlag = false;
	private String mConferenceId;
	private ArrayList<UniversityModel> mUniversitiesAL;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sports_list_activity);
		mContext = this;
		mOptionsLV = (ListView) findViewById(R.id.items_lv);
		mSearchACTV = (AutoCompleteTextView) findViewById(R.id.search_actv);
		mSearchACTV.setHint(getResources().getString(R.string.search_state));
		mContainerRl = (LinearLayout) findViewById(R.id.confirmation_parent_rl);
		mContainerLl = (LinearLayout) findViewById(R.id.view_contaniner_layout_confirm_rl);
		mSearchACTV.setText("");
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_close, R.drawable.selector_confirm,
				getResources().getString(R.string.select_university_label), new OnClickListener() {

			@Override
			public void onClick(View v) {
				Utility.hideSoftKeyboard(mSearchACTV);
				reverseAnimation(null);
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				Utility.hideSoftKeyboard(mSearchACTV);
				if (mUniversityAdapter != null) {

					if (mSelections!=null) {
						Intent intent = new Intent();
						intent.putExtra("data", mSelections);
						setResult(RESULT_OK, intent);
						reverseAnimation(intent);
					}
					else {
						Toast.makeText(UniversityListActivity.this, getResources().getString(R.string.select_one_university), Toast.LENGTH_SHORT).show();
					}
				} else {
					reverseAnimation(null);
				}
			}
		});
		mSelections = getIntent().getParcelableExtra("selections");
		mConferenceId = getIntent().getStringExtra("ConferenceId");
		mSearchACTV.setHint(getResources().getString(R.string.search_university));
		mPageIndex = 1;
		mUniversitiesAL = new ArrayList<UniversityModel>();
		getUniversityRequest();
		startAnimation();
		FontLoader.setRobotoRegularTypeface(mSearchACTV);
		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);

	}

	private void setAdapter(final List<UniversityModel> universityModels) {
		mUniversityAdapter = new UniversityListAdapter(UniversityListActivity.this);
		mUniversityAdapter.setList(universityModels);
		mOptionsLV.setAdapter(mUniversityAdapter);
		mUniversityAdapter.notifyDataSetChanged();
		new SearchHandler(mSearchACTV).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				mUniversityAdapter.setFilter(text);
			}
		});
		mOptionsLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mSelections = mUniversityAdapter.getItem(position);

				for(int i=0;i<universityModels.size();i++){
					UniversityModel mCModel  = universityModels.get(i);
					if(mCModel.mConferenceId.equals(mSelections.mConferenceId)){
						mSelections.isSelected = !mSelections.isSelected;
						universityModels.set(i, mSelections);
					}
					else{
						mCModel.isSelected = false;
						universityModels.set(i, mCModel);
					}
				}
				mUniversityAdapter.notifyDataSetChanged();
			}
		});
		
		/*mOptionsLV.setOnScrollListener(new OnScrollListener() {
			boolean bReachedListEnd = false;
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
					Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
					if (null != mUniversityAdapter&& !loadingFlag) {
						mPageIndex++;
						loadingFlag = true;
						getUniversityRequest();
					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
				bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount/2));
			}
		});*/
	}

	public static Intent getIntent(Context context, UniversityModel selections,String mConferenceId) {
		Intent intent = new Intent(context, UniversityListActivity.class);
		intent.putExtra("selections", selections);
		intent.putExtra("ConferenceId", mConferenceId);
		return intent;
	}

	/**
	 * Animation from bottom to top
	 * */
	private void startAnimation() {
		// Cancels any animations for this container.
		mContainerLl.clearAnimation();
		mContainerRl.clearAnimation();

		Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up_dialog);
		mContainerLl.startAnimation(animation);

		AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
	}

	/**
	 * Animation from top to bottom
	 * */
	private void reverseAnimation(final Intent intent) {
		mContainerLl.clearAnimation();
		Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_down_dialog);
		mContainerLl.startAnimation(animation);
		mContainerLl.setVisibility(View.GONE);

		AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
		mContainerRl.setVisibility(View.GONE);

		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				finish();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}
	}

	private void getUniversityRequest() {
		if(HttpConnector.getConnectivityStatus(mContext)==0){
			DialogUtil.showOkDialog(mContext, Utility.FromResouarceToString(mContext, R.string.No_internet_connection),
					Utility.FromResouarceToString(mContext, R.string.Network_erro));
			if(null!=mSwipeRefreshWidget)
			mSwipeRefreshWidget.setRefreshing(false);
		}
		else{
			GetUniversityRequest mRequest = new GetUniversityRequest(mContext);
			mRequest.GetUniversityServerRequest(mPageIndex, mConferenceId);
			mRequest.setRequestListener(new RequestListener() {

				@SuppressWarnings("unchecked")
				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					loadingFlag = false;
					mSwipeRefreshWidget.setRefreshing(false);
					if (success) {
						@SuppressWarnings("unchecked")
						List<UniversityModel> mUniversityModels = (List<UniversityModel>) data;
						mUniversitiesAL.addAll(mUniversityModels);
						if (null != mUniversitiesAL && null != mSelections) {
							for (UniversityModel universityModel : mUniversitiesAL) {
								if (mSelections.equals(universityModel)) {
									universityModel.isSelected = true;
								}
							}
						}
						setAdapter(mUniversitiesAL);
					}
				}
			});
		}
	}

	@Override
	public void onRefresh() {
		mPageIndex = 1;
		mUniversitiesAL = new ArrayList<UniversityModel>();
		getUniversityRequest();
	}
}