package com.schollyme.team;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.friends.BaseFriendFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.Teams;
import com.schollyme.team.FanPageListFragment.AdapterRefreshListener;
import com.vinfotech.request.TeamListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

public class OfficialPageListFragment extends BaseFriendFragment implements OnClickListener, OnRefreshListener{

	private static Context mContext;
	private ListView mTeamLV;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private LinearLayout mLinearLayout;
	private EditText mTeamSearchET;
	private ImageButton mClearIB;
	private TextView mNoRecordTV;
	private ArrayList<Teams> mTeamsAL;
	private int pageIndex = 1;
	private boolean loadingFlag = false;
	private static ErrorLayout mErrorLayout;
	private View view;
	private OfficialTeamAdapter mTeamAdapter;

	public static OfficialPageListFragment newInstance(ErrorLayout mLayout) {
		mErrorLayout = mLayout;
		return new OfficialPageListFragment();
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.team_fragment, null);
		mContext = getActivity();
		mTeamLV = (ListView) view.findViewById(R.id.team_lv);
		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		mTeamSearchET = (EditText) view.findViewById(R.id.search_et);
		mClearIB = (ImageButton) view.findViewById(R.id.clear_text_ib);
		mLinearLayout = (LinearLayout) view.findViewById(R.id.search_view_ll);
		mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
		mLinearLayout.setVisibility(View.VISIBLE);
		new SearchHandler(mTeamSearchET).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				
				if(mTeamAdapter!=null){
					getSearchTeamsRequest(text);
				}
			}
		}).setClearView(mClearIB);
		FontLoader.setRobotoRegularTypeface(mTeamSearchET,mNoRecordTV);
		pageIndex = 1;
		mTeamsAL = new ArrayList<Teams>();

		mTeamSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					return true;
				}
				return false;
			}
		});
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		pageIndex = 1;
		mTeamsAL = new ArrayList<Teams>();
		loadingFlag = true;
		getTeamsRequest(mTeamSearchET.getText().toString().trim());
	}

	private void setTeamAdapter(){
		
		mTeamAdapter = new OfficialTeamAdapter(mContext,mErrorLayout,new AdapterRefreshListener() {

			@Override
			public void onRefresh(int listSize) {
				if(listSize==0){
					mNoRecordTV.setVisibility(View.VISIBLE);
				}
				else{
					mNoRecordTV.setVisibility(View.GONE);
				}
			}
		});
		if(mTeamsAL.size()>0){
			mNoRecordTV.setVisibility(View.GONE);
			
			mTeamAdapter.setList(mTeamsAL);
			mTeamLV.setAdapter(mTeamAdapter);
			mTeamLV.setVisibility(View.VISIBLE);
			mNoRecordTV.setVisibility(View.GONE);
			mTeamLV.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					if(null!= mTeamsAL && mTeamsAL.size()>0){
						String mTeamGUID = mTeamsAL.get(position).mTeamGUID;
						startActivity(TeamPageActivity.getIntent(mContext,mTeamGUID,"",position));
					}
				}
			});
		}
		else{
			mTeamLV.setAdapter(mTeamAdapter);
			mTeamAdapter.setList(mTeamsAL);
			mNoRecordTV.setVisibility(View.VISIBLE);
		}

	}

	@Override
	public void onRefresh() {
		if(!loadingFlag){
			pageIndex = 1;
			mTeamsAL = new ArrayList<Teams>();
			getTeamsRequest(mTeamSearchET.getText().toString().trim());
		}
	}

	@Override
	public void onReload() {
		if(null != mTeamLV){
			mTeamLV.post(new Runnable() {

				@Override
				public void run() {
					onRefresh();
				}
			});
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.clear_text_ib:
			mTeamSearchET.setText("");
			break;

		default:
			break;
		}
	}

	private void getTeamsRequest(String mSearchText){

		TeamListRequest mRequest = new TeamListRequest(mContext);
		
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				loadingFlag = false;
				mSwipeRefreshWidget.setRefreshing(false);
				if(success){
					mTeamsAL.addAll((ArrayList<Teams>) data);
				}
				else{
					mTeamsAL = new ArrayList<Teams>();
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
				setTeamAdapter();
			}
		});
		mRequest.TeamListServerRequest(pageIndex,mSearchText,"1");
	}
	
	
	private void getSearchTeamsRequest(String mSearchText){

		TeamListRequest mRequest = new TeamListRequest(mContext);
		mRequest.TeamListServerRequest(pageIndex,mSearchText,"1");
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				loadingFlag = false;
				mSwipeRefreshWidget.setRefreshing(false);
				if(success){
					mTeamsAL = ((ArrayList<Teams>) data);
				}
				else{
					mTeamsAL = new ArrayList<Teams>();
				}
				setTeamAdapter();
			}
		});
	}
}