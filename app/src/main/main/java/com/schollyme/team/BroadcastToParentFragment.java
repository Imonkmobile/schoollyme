package com.schollyme.team;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.schollyme.R;
import com.schollyme.friends.BaseFriendFragment;
import com.schollyme.friends.FriendRequestFragment;
import com.schollyme.handler.ErrorLayout;
import com.vinfotech.utility.FontLoader;

public class BroadcastToParentFragment extends BaseFriendFragment implements OnRefreshListener{
	
	private static ErrorLayout mErrorLayout;
	private static Context mContext;
	private ListView mUsersAL;
	private TextView mNoRecordTV;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private int pageIndex = 1;
	private boolean loadingFlag = false;
	private boolean isRequesting = false;
	
	public static BroadcastToParentFragment newInstance(ErrorLayout mLayout) {
		mErrorLayout = mLayout;
		return new BroadcastToParentFragment();
	}

	@Override
	public void onStart() {
		super.onStart();
	
    }

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.broadcast_fragment, null);
		mContext = getActivity();
		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		FontLoader.setRobotoRegularTypeface(mNoRecordTV);
		pageIndex = 1;
		
		return view;
	}

	@Override
	public void onReload() {
		
	}

	@Override
	public void onRefresh() {
		
	}
}