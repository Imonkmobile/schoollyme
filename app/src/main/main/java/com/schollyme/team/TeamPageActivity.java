package com.schollyme.team;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.Config;
import com.schollyme.CreateWallPostActivity;
import com.schollyme.MediaPickerActivity;
import com.schollyme.R;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.adapter.PostFeedAdapter.OnItemClickListenerPost;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.schollyme.model.Teams;
import com.vinfotech.request.FanPageJoinRequest;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.TeamPageDeactivateRequest;
import com.vinfotech.request.TeamPageDetailRequest;
import com.vinfotech.request.TeamPageLeaveRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.ImageUtil;

public class TeamPageActivity extends MediaPickerActivity implements OnClickListener{

	private Context mContext;
	private ErrorLayout mErrorLayout;
	//	private HeaderProfileBadgeLayout mHeaderLayout;
	private TeamPageActivityViewHolder mTeamPageActivityViewHolder;
	private TextView titleTv;
	private int mScreenHeight;
	private String mTeamGUID,mActvityGUID;
	private Teams mTeamPageDetails;
	private int position;
	private String actionType = "";
	private Bitmap mPhotoBitmap;
	
	private boolean mFromQuery = false;

	private void getScreenWidthandHeight() {
		int[] dimens = new int[2];
		DisplayUtil.probeScreenSize((Activity) mContext, dimens);
		mScreenHeight = dimens[1];
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.team_page_activity);
		mContext = this;
		mErrorLayout = new ErrorLayout(findViewById(R.id.profile_image_name_rl));
		//	mHeaderLayout = new HeaderProfileBadgeLayout(findViewById(R.id.header_layout));
		//	mHeaderLayout.setBackGroundColor(getResources().getColor(R.color.transparent_color));

		mTeamPageActivityViewHolder = new TeamPageActivityViewHolder(findViewById(R.id.main_rl), this);
		//		mHeaderLayout.mDropShowView.setVisibility(View.VISIBLE);
		//		titleTv = mHeaderLayout.gettitleView(mTeamPageActivityViewHolder.mPageNameTV.getText().toString());
		//	titleTv.setAlpha(0);
		getScreenWidthandHeight();
		setScrollView();
		FontLoader.setRobotoMediumTypeface(mTeamPageActivityViewHolder.mPageNameTV,mTeamPageActivityViewHolder.mAboutPageTabTV,
				mTeamPageActivityViewHolder.mPageMembersTabTV,mTeamPageActivityViewHolder.mPageMoreOptionsTabTV);
		FontLoader.setRobotoRegularTypeface(mTeamPageActivityViewHolder.mPageSportsTV,mTeamPageActivityViewHolder.mPageAdminTV,mTeamPageActivityViewHolder.mDirectMessageTV);
		mTeamGUID = getIntent().getStringExtra("TeamGUID");
		mActvityGUID  = getIntent().getStringExtra("ActvityGUID");
		position = getIntent().getIntExtra("Position", 0);
		
		Uri uri = getIntent().getData();
		mFromQuery = !(null == uri || TextUtils.isEmpty(uri.getQueryParameter("FromQuery")));
		if (mTeamGUID==null && uri!=null) {
			mTeamGUID = uri.getQueryParameter("TeamGUID");
		}
		initFeedRequest();
	}

	@Override
	protected void onStart() {

		super.onStart();
		getTeamPageDetailsServerRequest(mTeamGUID);

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	public static Intent getIntent(Context mContext,String mTeamGUID,String ActvityGUID,int position){
		Intent intent = new Intent(mContext,TeamPageActivity.class);
		intent.putExtra("TeamGUID", mTeamGUID);
		intent.putExtra("ActvityGUID", ActvityGUID);
		intent.putExtra("Position", position);
		return intent;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.about_tab_tv:
			if(mTeamPageDetails!=null){
				String mAdminGUID = mTeamPageDetails.mAdminUserGUID;
				startActivity(TeamAboutActivity.getIntent(mContext,mTeamGUID,mAdminGUID,position));
			}
			break;

		case R.id.members_tab_tv:
			if(mTeamPageDetails!=null){
				startActivity(TeamMembersActivity.getIntent(mContext,mTeamGUID,mTeamPageDetails.mAdminUserGUID,mTeamPageDetails.mPageCategoryID));
			}
			break;

		case R.id.more_tab_tv:
			if(mTeamPageDetails!=null){
				int optionText;

				if(mTeamPageDetails.isGroupAdmin){
					if(mTeamPageDetails.mStatusID.equals("2")){
						optionText = R.string.deactivate_label;
						actionType = Config.IN_ACTIVATE_PAGE;
					}
					else{
						optionText = R.string.activate_label;
						actionType = Config.ACTIVATE_PAGE;
					}

				}
				else if(mTeamPageDetails.isGroupMember){
					optionText = R.string.leave_header;
				}
				else{
					optionText = R.string.join_label;
				}

				if(mTeamPageDetails.isGroupAdmin){
					DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {

						@Override
						public void onItemClick(int position, String item) {
							if(position==0){
								if(mTeamPageDetails.isGroupAdmin){
									DialogUtil.showOkCancelDialog(mContext,mContext.getResources().getString(R.string.app_name), mContext.getString(R.string.deactivate_page_confirmation), new OnClickListener() {

										@Override
										public void onClick(View arg0) {
											deactivatePageRequest(mTeamPageDetails.mTeamGUID,actionType);
										}
									}, new OnClickListener() {

										@Override
										public void onClick(View v) {

										}
									});
								}
							}
							else if(position==1){
								startActivity(AddMembersToPageActivity.getIntent(mContext, mTeamPageDetails.mTeamGUID));
							}
							else if(position==2){
								startActivity(TeamPageEditActivity.getIntent(mContext, mTeamPageDetails.mTeamGUID, mTeamPageDetails.mAdminUserGUID));
							}
						}

						@Override
						public void onCancel() {

						}
					}, mContext.getString(optionText)/*, mContext.getString(R.string.add_members), mContext.getString(R.string.edit_page_opt)*/);
				}
				else{
					DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {

						@Override
						public void onItemClick(int position, String item) {
							if(mTeamPageDetails.isGroupAdmin){
								deactivatePageRequest(mTeamPageDetails.mTeamGUID,actionType);
							}
							else if(mTeamPageDetails.isGroupMember){
								DialogUtil.showOkCancelDialog(mContext,mContext.getResources().getString(R.string.app_name), mContext.getString(R.string.leave_page_confirmation), new OnClickListener() {

									@Override
									public void onClick(View arg0) {
										leavePageRequest(mTeamPageDetails.mTeamGUID, new LogedInUserModel(mContext).mUserGUID);
									}
								}, new OnClickListener() {

									@Override
									public void onClick(View v) {

									}
								});

							}
							else{
								DialogUtil.showOkCancelDialog(mContext,mContext.getResources().getString(R.string.app_name), mContext.getString(R.string.join_page_confirmation), new OnClickListener() {

									@Override
									public void onClick(View arg0) {
										joinPageRequest(mTeamPageDetails.mTeamGUID);
									}
								}, new OnClickListener() {

									@Override
									public void onClick(View v) {

									}
								});

							}
						}

						@Override
						public void onCancel() {

						}
					}, mContext.getString(optionText));
				}
			}
			break;

		case R.id.back_ib:
			finish();
			break;

		case R.id.message_tv:
			if(mTeamPageDetails.isGroupMember){
				if(mTeamPageDetails!=null){
					startActivity(NewBroadcastActivity.getIntent(mContext, 0, mTeamGUID, mTeamPageDetails.mTeamName));
				}
			}
			else{
				DialogUtil.showOkCancelDialog(mContext,mContext.getResources().getString(R.string.app_name), mContext.getString(R.string.join_page_confirmation), new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						joinPageRequest(mTeamPageDetails.mTeamGUID);
					}
				}, new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});
				//	DialogUtil.showOkDialog(mContext, getResources().getString(R.string.not_member),  getResources().getString(R.string.app_name));
			}
			break;


		case R.id.edit_ib:
			if(mTeamPageDetails.isGroupAdmin){
				displayImagePickerPopup();
			}
			break;
		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	private void setData(){
		mTeamPageActivityViewHolder.mPageNameTV.setText(mTeamPageDetails.mTeamName);
		mTeamPageActivityViewHolder.mPageSportsTV.setText(mTeamPageDetails.mTeamSportsName);
		String mAdminName = getResources().getString(R.string.created_by)+" "+ mTeamPageDetails.mAdminUserFName;
		mTeamPageActivityViewHolder.mPageAdminTV.setText(mAdminName);

		if(!mTeamPageDetails.mTeamCoverImageURL.equals("thumb.png")){
			String imageUrl = Config.COVER_URL + mTeamPageDetails.mTeamCoverImageURL;
			ImageLoaderUniversal.ImageLoadSquare(mContext, imageUrl, mTeamPageActivityViewHolder.mCoverPicIV ,
					ImageLoaderUniversal.option_normal_Image_Thumbnail_cover);
		}


		if(mTeamPageDetails.mPageCategoryID.equals("1")){
			String imageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mTeamImageURL;
			ImageLoaderUniversal.ImageLoadRoundForTeamPage(mContext, imageUrl, mTeamPageActivityViewHolder.mPageLogoIV ,
					ImageLoaderUniversal.option_Round_Image_for_teampage);	
		}
		else{
			String mTeamName = mTeamPageDetails.mTeamName.trim();
			if(!TextUtils.isEmpty(mTeamName)){
				mTeamPageActivityViewHolder.mTeamFirstCharTV.setText(String.valueOf(mTeamName.charAt(0)).toUpperCase());
			}
			if(position%4==0){
				mTeamPageActivityViewHolder.mPageLogoIV.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.circle_blank_logo1));
			}
			else if(position%4==1){
				mTeamPageActivityViewHolder.mPageLogoIV.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.circle_blank_logo2));
			}
			else if(position%4==2){
				mTeamPageActivityViewHolder.mPageLogoIV.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.circle_blank_logo3));
			}
			else if(position%4==3){
				mTeamPageActivityViewHolder.mPageLogoIV.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.circle_blank_logo4));
			}
		}

		if(mTeamPageDetails.isGroupAdmin){
			mTeamPageActivityViewHolder.mEditCoverIB.setVisibility(View.VISIBLE);
			mTeamPageActivityViewHolder.mDirectMessageTV.setVisibility(View.VISIBLE);
			mTeamPageActivityViewHolder.mDirectMessageTV.setCompoundDrawablesWithIntrinsicBounds( R.drawable.selector_team_message,0, 0, 0);
		}
		else{
			if(mTeamPageDetails.isGroupMember || mTeamPageDetails.isGroupAdmin ){
				mTeamPageActivityViewHolder.mDirectMessageTV.setVisibility(View.GONE);
			}
			else{
				mTeamPageActivityViewHolder.mDirectMessageTV.setVisibility(View.VISIBLE);
				//	mTeamPageActivityViewHolder.mDirectMessageIB.setImageResource(R.drawable.selector_add_page_header);
				mTeamPageActivityViewHolder.mDirectMessageTV.setText(getResources().getString(R.string.join_label));
				mTeamPageActivityViewHolder.mDirectMessageTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			}

			mTeamPageActivityViewHolder.mEditCoverIB.setVisibility(View.GONE);
		}
	}

	private void getTeamPageDetailsServerRequest(String mTeamGUID){

		TeamPageDetailRequest mRequest = new TeamPageDetailRequest(mContext);
		mRequest.TeamDetailsServerRequest(mTeamGUID);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mTeamPageDetails = (Teams) data;
					setData();
				}
				else{
					final AlertDialog builder =  DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
							new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {
							finish();
						}
					});
					builder.setCancelable(false);
					builder.setOnKeyListener(new Dialog.OnKeyListener() {

						@Override
						public boolean onKey(DialogInterface arg0, int keyCode,
								KeyEvent event) {
							if (keyCode == KeyEvent.KEYCODE_BACK) {
								finish();
								builder.dismiss();
							}
							return true;
						}
					});
				}

			}
		});
	}

	public class TeamPageActivityViewHolder {
		public TextView mPageNameTV;
		public TextView mPageSportsTV;
		public TextView mPageAdminTV;
		public TextView mTeamFirstCharTV;
		private ImageView mPageLogoIV,mCoverPicIV;
		public RelativeLayout mUserInfoRL;
		public TextView mAboutPageTabTV, mPageMembersTabTV, mPageMoreOptionsTabTV,mDirectMessageTV;
		private ImageButton mBackIB,/*mDirectMessageIB,*/mEditCoverIB;
		private LinearLayout mLinearLayout;

		public TeamPageActivityViewHolder(View view, OnClickListener listener) {
			mPageLogoIV = (ImageView) view.findViewById(R.id.profile_image_iv);
			mCoverPicIV = (ImageView) view.findViewById(R.id.profile_cover_iv);
			mPageNameTV = (TextView) view.findViewById(R.id.user_name_tv);
			mPageSportsTV = (TextView) view.findViewById(R.id.user_status_tv);
			mPageAdminTV = (TextView) view.findViewById(R.id.friend_action_tv);
			mUserInfoRL = (RelativeLayout) view.findViewById(R.id.profile_image_name_rl);
			mTeamFirstCharTV = (TextView) view.findViewById(R.id.team_name_fchar_tv);
			mEditCoverIB = (ImageButton) view.findViewById(R.id.edit_ib);
			//	mBraodcastTV = (TextView) view.findViewById(R.id.broadcast_tv);

			mLinearLayout = (LinearLayout) view.findViewById(R.id.profile_rl);
			mLinearLayout.setOnClickListener(listener);
			mAboutPageTabTV = (TextView) view.findViewById(R.id.about_tab_tv);
			mPageMembersTabTV = (TextView) view.findViewById(R.id.members_tab_tv);
			mPageMoreOptionsTabTV = (TextView) view.findViewById(R.id.more_tab_tv);

			mBackIB = (ImageButton)view.findViewById(R.id.back_ib);
			mDirectMessageTV = (TextView)view.findViewById(R.id.message_tv);

			mEditCoverIB.setOnClickListener(listener);
			mAboutPageTabTV.setOnClickListener(listener);
			mPageMoreOptionsTabTV.setOnClickListener(listener);
			mPageMembersTabTV.setOnClickListener(listener);
			mBackIB.setOnClickListener(listener);
			mDirectMessageTV.setOnClickListener(listener);
			//	mBraodcastTV.setOnClickListener(listener);
		}
	}

	private void setScrollView() {
		//	mHeaderLayout.mBackroundLl.setAlpha(0);
		//	mHeaderLayout.mDropShowView.setAlpha(0);
		//	titleTv.setAlpha(0);
		//	mHeaderLayout.mDropShowView.setVisibility(View.GONE);
		//	mHeaderLayout.mBackroundLl.setVisibility(View.GONE);
	}

	/**
	 * Call API to Deactivate a fan/team page
	 * only page admin/owner have permision to Deactivate a page. 
	 * */
	private void deactivatePageRequest(String mTeamPageGUID,String actionType){
		TeamPageDeactivateRequest mRequest = new TeamPageDeactivateRequest(mContext);
		mRequest.TeamDeactivateRequest(mTeamPageGUID,actionType);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mErrorLayout.showError(data.toString(), true,MsgType.Success);
					getTeamPageDetailsServerRequest(mTeamGUID);
					/*new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							Intent intent = TeamActivity.getIntent(mContext, 0);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							finish();
						}
					}, 2000);*/
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});

	}

	/**Call  api to leave current group,
	 * This action can be perform by page members only */
	private void leavePageRequest(String mTeamGUID,String UserGUID){
		TeamPageLeaveRequest mRequest = new TeamPageLeaveRequest(mContext);
		mRequest.TeamPagesLeaveRequest(mTeamGUID, UserGUID);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mErrorLayout.showError(data.toString(), true,MsgType.Success);
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							Intent intent;
							if(mTeamPageDetails.mPageCategoryID.equals("1")){
								intent = TeamActivity.getIntent(mContext, 0);
							}
							else{
								intent = TeamActivity.getIntent(mContext, 1);
							}

							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							finish();
						}
					}, 2000);
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
	}


	/**
	 * Call API to join a suggested fan page
	 * */
	private void joinPageRequest(String mTeamPageGUID){
		FanPageJoinRequest mRequest = new FanPageJoinRequest(mContext);
		mRequest.getFanPageJoinRequest(mTeamPageGUID,new LogedInUserModel(mContext).mUserGUID);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mErrorLayout.showError(data.toString(), true,MsgType.Success);
					getTeamPageDetailsServerRequest(mTeamGUID);
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		mNewsFeedRequest.setActivityStatus(true);

		switch (requestCode) {
		case LikeListActivity.REQ_FROM_HOME_LIKELIST:
			if (resultCode == RESULT_OK) {
				if (data != null) {
					mNewsFeeds.get(ClickLocation).NoOfLikes = data.getIntExtra("COUNT", 0);
					mNewsFeeds.get(ClickLocation).IsLike = data.getIntExtra("BUTTONSTATUS", 0);
					mPostFeedAdapter.setList(mNewsFeeds, false);
				}
			}
			break;
		case CommentListActivity.REQ_CODE_COMMENT_LIST:
			if (resultCode == RESULT_OK) {
				if (data != null) {
					mNewsFeeds.get(ClickLocation).NoOfComments = data.getIntExtra("COUNT", 0);
					mPostFeedAdapter.setList(mNewsFeeds, false);
				}
			}
			break;

		case CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY:
			if (resultCode == RESULT_OK) {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						mNFFilter.PageNo = 1;
						getFeedRequest(mNFFilter);
					}
				}, 500);
			}
			break;

		case WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY:
			if (resultCode == RESULT_OK) {
				mNewsFeeds.get(ClickLocation).NoOfComments++;
				mPostFeedAdapter.setList(mNewsFeeds, false);
			}

			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void resetList() {
		mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
		mNewsFeeds.clear();
		mNewsFeeds.add(new NewsFeed(null));
		mPostFeedAdapter.setList(mNewsFeeds, false);
	}

	private View mFooterRL;
	private TextView mNoMoreTV;
	private TextView mNoFeedTV;
	private ProgressBar mLoaderBottomPB, mLoadingCenter;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private ListView mGnericLv;

	private PostFeedAdapter mPostFeedAdapter;
	private NFFilter mNFFilter;
	private List<NewsFeed> mNewsFeeds = new ArrayList<NewsFeed>();
	boolean isLoading;

	private NewsFeedRequest mNewsFeedRequest;

	private void initFeedRequest() {
		mNFFilter = new NFFilter(NewsFeedRequest.MODULE_ID_GROUPS, mTeamGUID);

		if(!TextUtils.isEmpty(mActvityGUID)){
			mNFFilter.ActivityGUID = mActvityGUID;	
			mNFFilter.AllActivity = 0;
		}
		mNFFilter.AllActivity = 0;
		mPostFeedAdapter = new PostFeedAdapter(mContext, new OnItemClickListenerPost() {

			@Override
			public void onClickItems(int ID, int position, NewsFeed newsFeed) {
				onItemClick(ID, position, newsFeed);
			}
		}, mTeamGUID);

		mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);

		mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
		mNoMoreTV.setVisibility(View.INVISIBLE);

		mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
		mLoaderBottomPB.setVisibility(View.INVISIBLE);
		mLoaderBottomPB.getIndeterminateDrawable()
		.setColorFilter(getResources().getColor(R.color.red_normal_color), PorterDuff.Mode.SRC_IN);

		mNoFeedTV = (TextView) findViewById(R.id.no_feed_tv);

		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.pull_to_refresh_srl);
		mSwipeRefreshWidget.setColorScheme(R.color.red_normal_color);
		mSwipeRefreshWidget.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				mSwipeRefreshWidget.setRefreshing(true);
				getFeedRequest(mNFFilter);
			}
		});

		mGnericLv = (ListView) findViewById(R.id.genric_lv);
		mGnericLv.addFooterView(mFooterRL);
		mGnericLv.setAdapter(mPostFeedAdapter);
		mGnericLv.setOnScrollListener(new ScrollEndListener());
		mNFFilter.AllActivity = 0;
		getFeedRequest(mNFFilter);
		mNFFilter.ActivityGUID = "";	
	
	}

	int ClickLocation = 0;

	public void onItemClick(int ID, int position, NewsFeed newsFeed) {
		ClickLocation = position;
		switch (ID) {
		case R.id.share_tv:
			if (newsFeed.ShareAllowed == 1) {
				sharePost(newsFeed);
			} else {
				Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
			}
			break;

		case R.id.like_tv:
			likeMediaToggleService(newsFeed, position);
			break;
		case R.id.likes_tv:
			if (newsFeed.NoOfLikes != 0) {
				startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID, "ACTIVITY", "WALL"),
						LikeListActivity.REQ_FROM_HOME_LIKELIST);
				overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
			}
			break;

		case R.id.comment_tv: {
			AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
			String captionForWriteCommentScreen = "";
			if (!newsFeed.PostContent.equals("")) {
				captionForWriteCommentScreen = newsFeed.PostContent;
			} else {

				if (null != newsFeed.Album.AlbumMedias && !newsFeed.Album.AlbumMedias.get(0).ImageName.equals("")) {

					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).Caption;
				} else if(null != newsFeed.Album.AlbumMedias){
					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
				}
				else{
					captionForWriteCommentScreen = "";
				}

			}

			startActivityForResult(WriteCommentActivity.getIntent(this, albumMedia, "ACTIVITY", captionForWriteCommentScreen),
					WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY);
			overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
		}
		break;
		case R.id.comments_tv:

			AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
			String captionForWriteCommentScreen = "";
			if (!newsFeed.PostContent.equals("")) {
				captionForWriteCommentScreen = newsFeed.PostContent;
			} else {

				if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
				}

			}

			startActivityForResult(CommentListActivity.getIntent(mContext, albumMedia, "WALLPOST", captionForWriteCommentScreen),
					CommentListActivity.REQ_CODE_COMMENT_LIST);
			overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

			break;

		case PostFeedAdapter.ConvertViewID:
			if(mTeamPageDetails.isGroupMember){
				startActivityForResult(CreateWallPostActivity.getIntent(mContext, WallPostCreateRequest.MODULE_ID_GROUPS,true, mTeamGUID),
						CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
			}
			else{
				DialogUtil.showOkDialog(mContext, getResources().getString(R.string.not_member),  getResources().getString(R.string.app_name));
			}
			break;
		default:
			break;
		}
	}

	public void sharePost(NewsFeed newsFeedModel) {
		SharePostRequest mSharePostRequest = new SharePostRequest(mContext);

		mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
				NewsFeedRequest.MODULE_ID_GROUPS, mTeamGUID, 1, 1);
		mSharePostRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {
					mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, MsgType.Success);
				} else {
					mErrorLayout.showError((String) data, true, MsgType.Error);
				}
			}
		});
	}

	public void likeMediaToggleService(final NewsFeed newsFeed, final int position) {
		ToggleLikeRequest mToggleLikeRequest = new ToggleLikeRequest(mContext);
		mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");

		mToggleLikeRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {

					if (newsFeed.IsLike == 0) {

						newsFeed.IsLike = 1;
						newsFeed.NoOfLikes++;
					} else {

						newsFeed.IsLike = 0;
						newsFeed.NoOfLikes--;
					}
					mPostFeedAdapter.notifyDataSetChanged();

				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), "");
				}

			}
		});
	}

	private void getFeedRequest(final NFFilter nffilter) {
		if (null == mNewsFeedRequest) {
			mNewsFeedRequest = new NewsFeedRequest(mContext);
		}
		mNewsFeedRequest.getNewsFeedListInServer(nffilter);
		mNoMoreTV.setVisibility(View.INVISIBLE);
		if (nffilter.PageNo == 1) {
			if (mPostFeedAdapter.getCount() <= 1 || mPostFeedAdapter == null) {
				mNewsFeedRequest.setLoader(mLoadingCenter);
			} else {
				mNewsFeedRequest.setLoader(null);
			}

			mLoaderBottomPB.setVisibility(View.INVISIBLE);
		} else {
			mNewsFeedRequest.setLoader(mLoaderBottomPB);
			mLoaderBottomPB.setVisibility(View.VISIBLE);
		}

		mNewsFeedRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mSwipeRefreshWidget.setRefreshing(false);
				if (success) {
					if (null != data) {

						if (mNFFilter.PageNo == 1) {
							resetList();
						}
						List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
						mNewsFeeds.addAll(newsFeeds);
						mPostFeedAdapter.setList(mNewsFeeds, false);

						if (totalRecords == 0) {
							isLoading = false;
							mNoMoreTV.setVisibility(View.INVISIBLE);
							mNoFeedTV.setVisibility(View.VISIBLE);

						} else if (mNewsFeeds.size() <= totalRecords) {

							isLoading = true;
							int newPage = mNFFilter.PageNo + 1;
							mNFFilter.PageNo = newPage;
							mNoFeedTV.setVisibility(View.GONE);
							mNoMoreTV.setVisibility(View.VISIBLE);
						} else {
							mNoMoreTV.setVisibility(View.VISIBLE);
							isLoading = false;
							mNoFeedTV.setVisibility(View.GONE);
							mSwipeRefreshWidget.setRefreshing(false);
						}
						if (mNFFilter.PageNo == 1) {
							mNoMoreTV.setVisibility(View.INVISIBLE);
						} else {
							mNoMoreTV.setVisibility(View.VISIBLE);
						}
					}
				} else {
					isLoading = false;
					DialogUtil.showOkDialog(mContext, (String) data, "");
				}
			}
		});
	}

	private class ScrollEndListener implements OnScrollListener {
		private int mLastFirstVisibleItem;

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			int k = visibleItemCount + firstVisibleItem;
			if (k >= totalItemCount && isLoading && totalItemCount != 0) {
				getFeedRequest(mNFFilter);
			}
			if (mLastFirstVisibleItem < firstVisibleItem) {
				Log.i("SCROLLING DOWN", "TRUE");
			}
			if (mLastFirstVisibleItem > firstVisibleItem) {
				Log.i("SCROLLING UP", "TRUE");
			}
			mLastFirstVisibleItem = firstVisibleItem;
		}
	}


	/** Page cover image block */

	private void displayImagePickerPopup() {

		DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
			@Override
			public void onItemClick(int position, String item) {
				if(position==0){
					openCamera();
				}
				else{
					openGallery();
				}
			}

			@Override
			public void onCancel() {

			}
		}, mContext.getString(R.string.take_picture), mContext.getString(R.string.load_from_library));
	}

	@Override
	protected void onGalleryImageSelected(String fileUri, Bitmap bitmap) {

	}

	@Override
	protected void onCameraImageSelected(String fileUri, Bitmap bitmap) {
		mPhotoBitmap = bitmap;
		try {
			Uri path = Uri.parse(fileUri);

			/*long id = ContentUris.parseId();
			Cursor cursor = getContentResolver()
			            .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
			                    new String[]{ MediaStore.Images.Media.DATA },
			                    MediaStore.Images.Media._ID + " = ?", new String[]{ Long.toString(id) }, 

			                    null);

			 String  path = "";
			if (cursor.moveToNext()) {
			 path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
			}
			cursor.close();*/
			/*mTeamPageActivityViewHolder.mCoverPicIV.setImageBitmap(ImageUtil.getBitmap(path.getPath()));
			DialogUtil.showOkCancelDialog(mContext, getResources().getString(R.string.app_name), getResources().getString(R.string.do_you_want_to_set_cover_image), new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					uploadMediaServerRequest();
				}
			}, new OnClickListener() {

				@Override
				public void onClick(View v) {
					mTeamPageActivityViewHolder.mCoverPicIV.setImageBitmap(null);
				}
			});*/
			uploadMediaServerRequest();


		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onVideoCaptured(String videoPath) {

	}

	@Override
	protected void onMediaPickCanceled(int reqCode) {

	}

	private void uploadMediaServerRequest() {
		MediaUploadRequest mRequest = new MediaUploadRequest(mContext, false);
		String fileName = ImageUtil.getFileNameFromURI(cropImageUri, mContext);
		File mFile = new File(cropImageUri.getPath());
		final LogedInUserModel mLoggedInUserModel = new LogedInUserModel(mContext);
		BaseRequest.setLoginSessionKey(mLoggedInUserModel.mLoginSessionKey);
		mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_GROUPS, mTeamGUID,
				MediaUploadRequest.TYPE_GROUP_COVER);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					getTeamPageDetailsServerRequest(mTeamGUID);
				}
				else{
					mTeamPageActivityViewHolder.mCoverPicIV.setImageBitmap(null);
				}
			}
		});
	}
}