package com.schollyme.team;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.MediaPickerActivity;
import com.schollyme.R;
import com.schollyme.activity.SportsListActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.ConferenceModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SportsModel;
import com.schollyme.model.UniversityModel;
import com.vinfotech.request.CreatePageRequest;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.utility.Utility;

public class CreatePageActivity extends MediaPickerActivity implements OnClickListener {

	private Context mContext;
	private CreatePageViewHolder mCreatePageViewHolder;

	private static final int REQ_CODE_SELECT_SPORT = 100;
	private static final int REQ_CODE_SELECT_CONFERENCE = 200;
	private static final int REQ_CODE_SELECT_UNIVERSITY = 300;
	// private static final int REQ_CODE_SELECT_IMAGE = 400;

	private ArrayList<SportsModel> mSelectedSportModel;
	private ConferenceModel mConferenceModel;
	private UniversityModel mUniversityModel;
	private Bitmap mPhotoBitmap;
	private ErrorLayout mErrorLayout;

	private String mGroupGUID, mGroupName, mGroupDescription, mLogoName, mCategoryIds, mGroupCoverImage = "";
	private int mIsPublic = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.team_page_create);
		mContext = this;

		mCategoryIds = getIntent().getStringExtra("PageTypeSelected");
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_ll));
		mCreatePageViewHolder = new CreatePageViewHolder(findViewById(R.id.main_ll), this);
		String headerTittle = mCategoryIds.equals(Config.PAGE_TYPE_OFFICIAL) ? getResources().getString(R.string.create_official_team)
				: getResources().getString(R.string.create_fan_team);
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm, headerTittle, new OnClickListener() {

			@Override
			public void onClick(View v) {
				Utility.hideSoftKeyboard(mCreatePageViewHolder.mPageNameET);
				finish();
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isValidated()) {
					if (null != cropImageUri/*
											 * && !TextUtils.isEmpty(
											 * cropImageUri)
											 */) {
						uploadMediaServerRequest();
					} else {
						createPageServerRequest("");
					}
				}
			}
		});
	}

	public static Intent getIntent(Context context, String mPageType) {
		Intent intent = new Intent(context, CreatePageActivity.class);
		intent.putExtra("PageTypeSelected", mPageType);
		return intent;
	}

	public class CreatePageViewHolder {

		private EditText mPageNameET, mPageDescripionET;
		private TextView mPageSportsTV, mPageConferenceTV, mPageCollegeTV;
		private TextView mPagePrivacyTV;
		private ImageView mPageLogoIV;
		// private ToggleButton mPrivacyTB;
		private Switch mPrivacySwitch;
		private View mDivider1, mDivider2;

		public CreatePageViewHolder(View view, OnClickListener listener) {
			mPageLogoIV = (ImageView) view.findViewById(R.id.page_logo_iv);
			mPageNameET = (EditText) view.findViewById(R.id.team_name_et);
			mPageSportsTV = (TextView) view.findViewById(R.id.team_sport_tv);
			mPageConferenceTV = (TextView) view.findViewById(R.id.team_conference_tv);
			mPageCollegeTV = (TextView) view.findViewById(R.id.team_college_tv);
			mPageDescripionET = (EditText) view.findViewById(R.id.team_description_et);
			mPagePrivacyTV = (TextView) view.findViewById(R.id.team_privacy_tv);
			// mPrivacyTB = (ToggleButton)
			// view.findViewById(R.id.team_privacy_tb);
			mPrivacySwitch = (Switch) view.findViewById(R.id.team_privacy_switch);
			mDivider1 = view.findViewById(R.id.devider3);
			mDivider2 = view.findViewById(R.id.devider4);
			mPrivacySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
					if (isChecked) {
						mIsPublic = 2;
					} else {
						mIsPublic = 1;
					}
					// mIsPublic =
					// mCreatePageViewHolder.mPrivacySwitch.isChecked()?2:1;
				}
			});
			mPageLogoIV.setOnClickListener(listener);
			mPageSportsTV.setOnClickListener(listener);
			mPageConferenceTV.setOnClickListener(listener);
			mPageCollegeTV.setOnClickListener(listener);
			FontLoader.setRobotoRegularTypeface(mPageSportsTV, mPageNameET, mPageConferenceTV, mPageCollegeTV, mPageDescripionET,
					mPagePrivacyTV);
			mPrivacySwitch.setChecked(true);
			if (mCategoryIds.equals("2")) {
				mPageConferenceTV.setVisibility(View.GONE);
				mPageCollegeTV.setVisibility(View.GONE);
				mDivider1.setVisibility(View.GONE);
				mDivider2.setVisibility(View.GONE);
				mPageLogoIV.setVisibility(View.GONE);
				mPrivacySwitch.setVisibility(View.VISIBLE);
				mPagePrivacyTV.setText(getResources().getString(R.string.team_privacy));
				mPagePrivacyTV.setTextColor(getResources().getColor(R.color.black));
			} else {
				mPageConferenceTV.setVisibility(View.VISIBLE);
				mPageCollegeTV.setVisibility(View.VISIBLE);
				mDivider1.setVisibility(View.VISIBLE);
				mDivider2.setVisibility(View.VISIBLE);
				mPageLogoIV.setVisibility(View.VISIBLE);
				mPrivacySwitch.setVisibility(View.GONE);
				mPagePrivacyTV.setText(getResources().getString(R.string.Private));
				mPagePrivacyTV.setTextColor(getResources().getColor(R.color.text_hint_color));
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.page_logo_iv:
			displayImagePickerPopup();
			break;

		case R.id.team_sport_tv:
			startActivityForResult(SportsListActivity.getIntent(mContext, false, mSelectedSportModel), REQ_CODE_SELECT_SPORT);
			break;

		case R.id.team_conference_tv:
			startActivityForResult(ConferencesListActivity.getIntent(mContext, mConferenceModel), REQ_CODE_SELECT_CONFERENCE);
			break;

		case R.id.team_college_tv:
			if (null != mConferenceModel)
				startActivityForResult(UniversityListActivity.getIntent(mContext, mUniversityModel, mConferenceModel.mConferenceId),
						REQ_CODE_SELECT_UNIVERSITY);
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case REQ_CODE_SELECT_SPORT:
				if (data != null) {
					mSelectedSportModel = data.getParcelableArrayListExtra("data");
					if (mSelectedSportModel != null && mSelectedSportModel.size() > 0)
						mCreatePageViewHolder.mPageSportsTV.setText(mSelectedSportModel.get(0).mSportsName);
				}
				break;

			case REQ_CODE_SELECT_CONFERENCE:
				if (data != null) {
					mConferenceModel = data.getParcelableExtra("data");
					if (mConferenceModel != null)
						mCreatePageViewHolder.mPageConferenceTV.setText(mConferenceModel.mConferenceName);
				}
				break;

			case REQ_CODE_SELECT_UNIVERSITY:
				if (data != null) {
					mUniversityModel = data.getParcelableExtra("data");
					if (mUniversityModel != null)
						mCreatePageViewHolder.mPageCollegeTV.setText(mUniversityModel.mUniversityName);
				}
				break;
			}
		}
	}

	private void displayImagePickerPopup() {

		DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
			@Override
			public void onItemClick(int position, String item) {
				if (position == 0) {
					openCamera();
				} else {
					openGallery();
				}
			}

			@Override
			public void onCancel() {

			}
		}, mContext.getString(R.string.take_picture), mContext.getString(R.string.load_from_library));
	}

	@Override
	protected void onGalleryImageSelected(String fileUri, Bitmap bitmap) {

	}

	@Override
	protected void onCameraImageSelected(String fileUri, Bitmap bitmap) {
		mPhotoBitmap = bitmap;
		mCreatePageViewHolder.mPageLogoIV.setImageBitmap(ImageUtil.getCircledBitmap(mPhotoBitmap));
	}

	@Override
	protected void onVideoCaptured(String videoPath) {

	}

	@Override
	protected void onMediaPickCanceled(int reqCode) {

	}

	private boolean isValidated() {

		mGroupName = mCreatePageViewHolder.mPageNameET.getText().toString();

		mGroupDescription = mCreatePageViewHolder.mPageDescripionET.getText().toString();

		// mIsPublic = mCreatePageViewHolder.mPrivacySwitch.isChecked()?2:1;

		if (TextUtils.isEmpty(mGroupName.trim())) {
			mErrorLayout.showError(getResources().getString(R.string.page_name_require), true, MsgType.Error);
			return false;
		} else if (mSelectedSportModel == null || TextUtils.isEmpty(mSelectedSportModel.get(0).mSportsID)) {
			mErrorLayout.showError(getResources().getString(R.string.page_sports_require), true, MsgType.Error);
			return false;
		}

		else if (mCategoryIds.equals("1")) {
			if (mConferenceModel == null || TextUtils.isEmpty(mConferenceModel.mConferenceId)) {
				mErrorLayout.showError(getResources().getString(R.string.page_conference_require), true, MsgType.Error);
				return false;
			} else if (mUniversityModel == null || TextUtils.isEmpty(mUniversityModel.mUniversityId)) {
				mErrorLayout.showError(getResources().getString(R.string.page_university_require), true, MsgType.Error);
				return false;
			} else if (TextUtils.isEmpty(mGroupDescription.trim())) {
				mErrorLayout.showError(getResources().getString(R.string.page_description_require), true, MsgType.Error);
				return false;
			} else {
				return true;
			}
		} else if (TextUtils.isEmpty(mGroupDescription.trim())) {
			mErrorLayout.showError(getResources().getString(R.string.page_description_require), true, MsgType.Error);
			return false;
		} else {
			return true;
		}
	}

	private void uploadMediaServerRequest() {
		MediaUploadRequest mRequest = new MediaUploadRequest(mContext, false);
		String fileName = ImageUtil.getFileNameFromURI(cropImageUri, mContext);
		File mFile = new File(cropImageUri.getPath());
		final LogedInUserModel mLoggedInUserModel = new LogedInUserModel(mContext);
		BaseRequest.setLoginSessionKey(mLoggedInUserModel.mLoginSessionKey);
		mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_GROUPS, "", MediaUploadRequest.TYPE_PROFILE);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					AlbumMedia mAlbumMedia = (AlbumMedia) data;
					createPageServerRequest(mAlbumMedia.ImageName);
				}
			}
		});
	}

	private void createPageServerRequest(String mLogoName) {
		CreatePageRequest mRequest = new CreatePageRequest(mContext);
		String mConference = "", mCollege = "";
		if (null != mConferenceModel) {
			mConference = mConferenceModel.mConferenceId;
		}

		if (null != mUniversityModel) {
			mCollege = mUniversityModel.mUniversityId;
		}

		mRequest.CreatePageServerRequest(mGroupName, mGroupDescription, mLogoName, mGroupCoverImage, mCategoryIds, mIsPublic,
				mSelectedSportModel.get(0).mSportsID, mCollege, mConference, mGroupGUID);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {
					mErrorLayout.showError(data.toString(), false, MsgType.Success);
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							finish();
						}
					}, 2000);
				} else {
					mErrorLayout.showError(data.toString(), false, MsgType.Error);
				}
			}
		});
	}
}