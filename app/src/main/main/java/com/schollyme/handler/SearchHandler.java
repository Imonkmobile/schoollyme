package com.schollyme.handler;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class SearchHandler {
	private EditText mSearchEt;
	private View mClearView;
	private Handler mHandler = new Handler();
	private boolean mTextChangeEnable = true;

	public SearchHandler(EditText editText) {
		this.mSearchEt = editText;
	}

	private SearchListener mSearchListener;

	public SearchHandler setSearchListener(SearchListener listener) {
		this.mSearchListener = listener;

		if (null != mSearchEt) {
			mSearchEt.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (mTextChangeEnable) {
						mHandler.removeCallbacksAndMessages(null);
						mHandler.postDelayed(new Runnable() {

							@Override
							public void run() {
								if (null != mSearchListener) {
									mSearchListener.onSearch(mSearchEt.getText().toString().trim());
								}
							}
						}, 500);
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					if (null != mClearView) {
						mClearView.setVisibility(s.length() > 0 ? View.VISIBLE : View.INVISIBLE);
					}
				}
			});
		}
		return this;
	}

	public SearchHandler setTextChangeEnable(boolean enable) {
		this.mTextChangeEnable = enable;
		return this;
	}

	public SearchHandler setClearView(View view) {
		mClearView = view;
		if (null != mClearView) {
			mClearView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mSearchEt.setText(null);
				}
			});
		}
		return this;
	}

	public interface SearchListener {
		void onSearch(String text);
	}
}
