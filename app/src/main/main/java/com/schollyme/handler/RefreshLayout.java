package com.schollyme.handler;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.vinfotech.utility.FontLoader;

public class RefreshLayout {
	private static final String TAG = RefreshLayout.class.getSimpleName();

	private int mBtnResId;

	public enum RefreshCause {
		NetworkError, NoRecordFound;
	}

	private LinearLayout mContentLl;
	private RefreshViewHolder mRefreshViewHolder;

	public RefreshLayout(Context context) {
		mContentLl = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.refresh_layout, null);
		mRefreshViewHolder = new RefreshViewHolder(mContentLl);
	}

	public void show(ViewGroup container, final OnClickListener listener, final int imgResId, final int msgResId, final int btnResId,
			final int btnColorId) {
		if (Config.DEBUG) {
			Log.v(TAG, "show imgResId=" + imgResId + ", msgResId=" + msgResId + ", btnResId=" + btnResId + ", btnColorId=" + btnColorId);
		}
		container.removeAllViews();
		container.setVisibility(View.VISIBLE);
		LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		container.addView(mContentLl, params);
		mContentLl.setVisibility(View.VISIBLE);

		if (0 != imgResId) {
			mRefreshViewHolder.mRefreshIv.setImageResource(imgResId);
		}
		if (0 != msgResId) {
			mRefreshViewHolder.mMessageTv.setText(msgResId);
		}
		if (0 != btnResId) {
			mBtnResId = btnResId;
			mRefreshViewHolder.mRefreshTv.setText(mBtnResId);
			mRefreshViewHolder.mRefreshTv.setVisibility(View.VISIBLE);
		} else {
			mRefreshViewHolder.mRefreshTv.setVisibility(View.GONE);
		}
		if (0 != btnColorId) {
			mRefreshViewHolder.mRefreshTv.setTextColor(container.getContext().getResources().getColor(btnColorId));
		}
		mRefreshViewHolder.mRefreshTv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (Config.DEBUG) {
					Log.v(TAG, "show.onClick listener=" + listener);
				}

				if (null != listener) {
					v.setTag(mBtnResId);
					listener.onClick(v);
					// mContentLl.setVisibility(View.INVISIBLE);
				}
			}
		});
	}

	public void hide(ViewGroup container) {
		container.setVisibility(View.GONE);
		container.removeAllViews();
	}

	public class RefreshViewHolder {
		public ImageView mRefreshIv;
		public TextView mMessageTv;
		public TextView mRefreshTv;

		public RefreshViewHolder(View view) {
			mRefreshIv = (ImageView) view.findViewById(R.id.refresh_icon_iv);
			mMessageTv = (TextView) view.findViewById(R.id.refresh_message_tv);
			mRefreshTv = (TextView) view.findViewById(R.id.refresh_title_tv);
			FontLoader.setRobotoRegularTypeface(mMessageTv);
			FontLoader.setRobotoBoldTypeface(mRefreshTv);
		}
	}

}