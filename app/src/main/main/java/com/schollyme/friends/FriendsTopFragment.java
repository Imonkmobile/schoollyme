package com.schollyme.friends;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.friends.FriendRequestFragment.FriendCountListener;
import com.schollyme.handler.ErrorLayout;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.widget.PagerSlidingTabStrip;
import com.vinfotech.widget.PagerSlidingTabStrip.NotificationTabProvider;

public class FriendsTopFragment extends BaseFragment {
	private static final int PAGE_COUNT = 5;
	private PagerSlidingTabStrip mPagerSlidingTabStrip;
	private ViewPager mGenericVp;
	private GridPagerAdapter mGridPagerAdapter;

	private ErrorLayout mErrorLayout;
	private int mCurrentPageToShow;
	private boolean mShowBackBtn = true;;
	private HeaderLayout mHeaderLayout;
	private Context mContext;

	public static FriendsTopFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
		FriendsTopFragment friendsTopFragment = new FriendsTopFragment();
		friendsTopFragment.mCurrentPageToShow = currentPageToShow;
		friendsTopFragment.mHeaderLayout = headerLayout;
		friendsTopFragment.mShowBackBtn = showBack;
		return friendsTopFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.friends_top_fragment, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mContext = getActivity();
		mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
				getResources().getString(R.string.friend_label), 0);
		mHeaderLayout.setListenerItI(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mShowBackBtn) {
					if(((FriendsActivity)getActivity()).mFromActivity.equalsIgnoreCase("GCMIntentService")){
						Intent intent = DashboardActivity.getIntent(getActivity(), 5, "FriendsTopFragment");
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						getActivity().finish();
					}else
						getActivity().finish();
				} else {
					((DashboardActivity)mContext).sliderListener();
				}
			}
		}, null);

		mGridPagerAdapter = new GridPagerAdapter(getChildFragmentManager());
		mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_container_ll));

		mPagerSlidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs_psts);
		mGenericVp = (ViewPager) view.findViewById(R.id.generic_vp);
		mGenericVp.setOffscreenPageLimit(1);
		mGenericVp.setAdapter(mGridPagerAdapter);
		mPagerSlidingTabStrip.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				BaseFriendFragment baseFriendFragment = mGridPagerAdapter.getBaseFriendFragment(position);
				baseFriendFragment.onReload();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int position) {
				// BaseFriendFragment baseFriendFragment =
				// mGridPagerAdapter.getBaseFriendFragment(position);
				// baseFriendFragment.onReload();
			}
		});

		mPagerSlidingTabStrip.setViewPager(mGenericVp);
		mGenericVp.postDelayed(new Runnable() {

			@Override
			public void run() {
				mGenericVp.setCurrentItem(mCurrentPageToShow);
			}
		}, 150);
	}

	public class GridPagerAdapter extends FragmentStatePagerAdapter implements NotificationTabProvider {
		private Map<Integer, BaseFriendFragment> mBaseFriendFragments;
		private String friendCount = "";

		public GridPagerAdapter(FragmentManager fm) {
			super(fm);
			mBaseFriendFragments = new HashMap<Integer, BaseFriendFragment>();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			String tabTitle = "";
			switch (position) {
			case 0:
				tabTitle = getString(R.string.request_tab);
				break;
			case 1:
				tabTitle = getString(R.string.friends_tab);
				break;
			case 2:
				tabTitle = getString(R.string.teammates_tab);
				break;
			case 3:
				tabTitle = getString(R.string.outgoing_tab);
				break;
			case 4:
				tabTitle = getString(R.string.suggestions_tab);
				break;
			}
			return tabTitle;
		}

		@Override
		public int getCount() {
			return PAGE_COUNT;
		}

		@Override
		public Fragment getItem(int position) {
			BaseFriendFragment mFragment = null;
			switch (position) {
			case 0:
				mFragment = FriendRequestFragment.newInstance(mErrorLayout, new FriendCountListener() {

					@Override
					public void onCountChange(int count) {
						friendCount = (count > 0 ? Integer.toString(count) : "");
						mPagerSlidingTabStrip.setViewPager(mGenericVp);
					}
				});
				break;
			case 1:
				mFragment = FriendsFragment.newInstance(mErrorLayout);
				break;
			case 2:
				mFragment = TeammatesFragment.newInstance(mErrorLayout);
				break;
			case 3:
				mFragment = FriendsOutgoingRequestFragment.newInstance(mErrorLayout);
				break;
			case 4:
				mFragment = FriendsSuggestionsFragment.newInstance(mErrorLayout);
				break;
			}
			mBaseFriendFragments.put(position, mFragment);
			return mFragment;
		}

		public BaseFriendFragment getBaseFriendFragment(int position) {
			return mBaseFriendFragments.get(position);
		}

		@Override
		public String getNotificationText(int position) {
			if (position == 0) {
				return friendCount;
			} else {
				return "";
			}
		}
	}
}