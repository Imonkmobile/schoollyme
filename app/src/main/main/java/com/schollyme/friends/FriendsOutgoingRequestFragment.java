package com.schollyme.friends;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.adapter.FriendSuggestionsAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.FriendsListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

public class FriendsOutgoingRequestFragment extends BaseFriendFragment implements OnRefreshListener{

	private static Context mContext;
	private ListView mFriendsSuggestionsAL;
	private TextView mNoRecordTV;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private FriendSuggestionsAdapter mAdapter;
	private ArrayList<FriendModel> mFriendsDataAL;
	private int pageIndex = 1;
	private boolean loadingFlag = false;
	private static ErrorLayout mErrorLayout;
	
	public static FriendsOutgoingRequestFragment newInstance(ErrorLayout mLayout) {
		mErrorLayout = mLayout;
		return new FriendsOutgoingRequestFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.friends_fragment, null);
		mContext = getActivity();
		mFriendsSuggestionsAL = (ListView) view.findViewById(R.id.friends_lv);
		mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
		FontLoader.setRobotoRegularTypeface(mNoRecordTV);
//		mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_ll));
		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		pageIndex = 1;
		mFriendsDataAL = new ArrayList<FriendModel>();
//		getOutgoingRequest(new LogedInUserModel(mContext).mUserGUID);
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
	}
	
	private void setSuggestedFriendsAdapter(final ArrayList<FriendModel> mSuggestionsAL){
		mAdapter = new FriendSuggestionsAdapter(mContext);
		if(mSuggestionsAL.size()==0){
			mNoRecordTV.setVisibility(View.VISIBLE);
			mNoRecordTV.bringToFront();
			mFriendsSuggestionsAL.setAdapter(mAdapter);
			mAdapter.setList(new ArrayList<FriendModel>());
		}
		else{
			mNoRecordTV.setVisibility(View.GONE);
			mSwipeRefreshWidget.setVisibility(View.VISIBLE);
			mFriendsSuggestionsAL.setVisibility(View.VISIBLE);
			mAdapter.setList(mSuggestionsAL);
			mFriendsSuggestionsAL.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();
			mFriendsSuggestionsAL.setOnScrollListener(new OnScrollListener() {
				boolean bReachedListEnd = false;
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
						Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
						if (null != mAdapter&& !loadingFlag) {
							pageIndex++;
							loadingFlag = true;
							getOutgoingRequest(new LogedInUserModel(mContext).mUserGUID);
						}
					}
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
					bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount/2));
				}
			});
			mFriendsSuggestionsAL.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
						long arg3) {
					FriendModel mModel = mSuggestionsAL.get(pos);

					if (new LogedInUserModel(mContext).mUserGUID.equals(mModel.mUserGUID)) {
						mContext.startActivity(DashboardActivity.getIntent(mContext,6, ""));
					} else {
						mContext.startActivity(FriendProfileActivity.getIntent(mContext,mModel.mUserGUID, mModel.mProfileLink, "FriendsFragment"));
					}
					
				}
			});
		}
	}

	
	private void getOutgoingRequest(String userGUID){
		FriendsListRequest mRequest = new FriendsListRequest(mContext);
	
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mFriendsDataAL.addAll((ArrayList<FriendModel>) data);
					setSuggestedFriendsAdapter(mFriendsDataAL);
				}
				else{
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
				}
				mSwipeRefreshWidget.setRefreshing(false);
			}
		});
		mRequest.FriendsListServerRequest(userGUID, "OutgoingRequest", "FirstName", "ASC", String.valueOf(pageIndex), String.valueOf(Config.PAGINATION_PAGE_SIZE));
	}

	@Override
	public void onRefresh() {
		pageIndex = 1;
		mFriendsDataAL = new ArrayList<FriendModel>();
		getOutgoingRequest(new LogedInUserModel(mContext).mUserGUID);
	}

	@Override
	public void onReload() {
		if(null != mFriendsSuggestionsAL){
			mFriendsSuggestionsAL.post(new Runnable() {
				
				@Override
				public void run() {
					onRefresh();
				}
			});
		}
	}
}