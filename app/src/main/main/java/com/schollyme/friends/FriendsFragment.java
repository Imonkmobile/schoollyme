package com.schollyme.friends;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.adapter.FriendRequestAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.FriendsListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class FriendsFragment extends BaseFriendFragment implements OnClickListener, OnRefreshListener{

	private static Context mContext;
	private ListView mFriendsSuggestionsAL;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private LinearLayout mLinearLayout;
	private EditText mFriendSearchET;
	private ImageButton mClearIB;
	private TextView mNoRecordTV;
	private FriendRequestAdapter mAdapter;
	private ArrayList<FriendModel> mFriendsDataAL;
	private int pageIndex = 1;
	private boolean loadingFlag = false;
	private static ErrorLayout mErrorLayout;
	private View view;

	public static FriendsFragment newInstance(ErrorLayout mLayout) {
		mErrorLayout = mLayout;
		return new FriendsFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.friends_fragment, null);
		mContext = getActivity();
		mFriendsSuggestionsAL = (ListView) view.findViewById(R.id.friends_lv);
		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		mFriendSearchET = (EditText) view.findViewById(R.id.search_et);
		mClearIB = (ImageButton) view.findViewById(R.id.clear_text_ib);
		mLinearLayout = (LinearLayout) view.findViewById(R.id.search_view_ll);
		mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
		//		mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_ll));
		mLinearLayout.setVisibility(View.VISIBLE);
		//		mFriendSearchET.addTextChangedListener(new CustomTextWatcher());
		new SearchHandler(mFriendSearchET).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				if(mAdapter!=null)
					mAdapter.setFilter(text);
			}
		}).setClearView(mClearIB);
		mClearIB.setOnClickListener(this);
		FontLoader.setRobotoRegularTypeface(mFriendSearchET,mNoRecordTV);
		pageIndex = 1;
		mFriendsDataAL = new ArrayList<FriendModel>();
		//	getFriendsRequest(mUserId);

		mFriendSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					Utility.hideSoftKeyboard(mFriendSearchET);
					return true;
				}
				return false;
			}
		});

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		/*	String mUserId = new LogedInUserModel(mContext).mUserGUID;
		mFriendsDataAL = new ArrayList<FriendModel>();
		getFriendsRequest(mUserId);*/
	}

	private void setSuggestedFriendsAdapter(final ArrayList<FriendModel> mSuggestionsAL){
		mAdapter = new FriendRequestAdapter(mContext, mErrorLayout, null);
		if(mSuggestionsAL.size()==0){
			mNoRecordTV.setVisibility(View.VISIBLE);
			mNoRecordTV.bringToFront();
			mFriendsSuggestionsAL.setAdapter(mAdapter);
			mAdapter.setList(new ArrayList<FriendModel>());
		}
		else{
			mNoRecordTV.setVisibility(View.GONE);
			mSwipeRefreshWidget.setVisibility(View.VISIBLE);
			mFriendsSuggestionsAL.setVisibility(View.VISIBLE);
			mAdapter.setList(mSuggestionsAL);
			mFriendsSuggestionsAL.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();
			mFriendsSuggestionsAL.setOnScrollListener(new OnScrollListener() {
				boolean bReachedListEnd = false;
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
						Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
						if (null != mAdapter&& !loadingFlag) {
							pageIndex++;
							loadingFlag = true;
							getFriendsRequest(new LogedInUserModel(mContext).mUserGUID);
						}
					}
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
					bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount/2));
				}
			});
		}
		mFriendsSuggestionsAL.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				FriendModel mModel = mSuggestionsAL.get(pos);

				if (new LogedInUserModel(mContext).mUserGUID.equals(mModel.mUserGUID)) {
					mContext.startActivity(DashboardActivity.getIntent(mContext,6, ""));
				} else {
					mContext.startActivity(FriendProfileActivity.getIntent(mContext,mModel.mUserGUID, mModel.mProfileLink, "FriendsFragment"));
				}

			}
		});
		mFriendsDataAL = mSuggestionsAL;
	}

	private void getFriendsRequest(String userGUID){
		if(mContext==null){
			mContext = getActivity();
		}
		FriendsListRequest mRequest = new FriendsListRequest(mContext);
		
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				loadingFlag = false;
				if(success){
					if(mFriendsDataAL.size()==0){
						mFriendsDataAL.addAll((ArrayList<FriendModel>) data);
						setSuggestedFriendsAdapter(mFriendsDataAL);
					}
					else{
						mFriendsDataAL.addAll((ArrayList<FriendModel>) data);
						if(null!=mAdapter){
							mAdapter.notifyDataSetChanged();
						}
						else{
							setSuggestedFriendsAdapter(mFriendsDataAL);
						}
					}
				}
				else{
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
				}
				mSwipeRefreshWidget.setRefreshing(false);
			}
		});
		mRequest.FriendsListServerRequest(userGUID, "Friends", "FirstName", "ASC", String.valueOf(pageIndex), String.valueOf(Config.PAGINATION_PAGE_SIZE*2));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.clear_text_ib:
			mFriendSearchET.setText("");
			if(mAdapter!=null){
				mAdapter.setList(mFriendsDataAL);
				mAdapter.notifyDataSetChanged();
			}
			break;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mFriendSearchET.setText("");
		Utility.hideSoftKeyboard(mFriendSearchET);
	}

	@Override
	public void onPause() {

		super.onPause();
		mFriendSearchET.setText("");
		Utility.hideSoftKeyboard(mFriendSearchET);
	}

	@Override
	public void onRefresh() {
		pageIndex = 1;
		mFriendsDataAL = new ArrayList<FriendModel>();
		getFriendsRequest(new LogedInUserModel(mContext).mUserGUID);
	}

	@Override
	public void onReload() {
		mFriendSearchET.setText("");
		Utility.hideSoftKeyboard(mFriendSearchET);
		if(null != mFriendsSuggestionsAL){
			mFriendsSuggestionsAL.post(new Runnable() {

				@Override
				public void run() {
					onRefresh();
				}
			});
		}
	}
}