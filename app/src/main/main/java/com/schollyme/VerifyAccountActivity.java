package com.schollyme;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.vinfotech.request.VerifyAccountRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;

public class VerifyAccountActivity extends BaseActivity {

	private Context mContext;
	private VerifyActivityViewHolder mVerifyActivityViewHolder;

	private String verificationCode;
	private ErrorLayout mErrorLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.verify_account_activity);
		mContext = this;
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
				getResources().getString(R.string.verify_label), new OnClickListener() {

					@Override
					public void onClick(View v) {
						startActivity(LoginActivity.getIntent(mContext));
						finish();
					}
				}, VerifyAccoutListener);
		mVerifyActivityViewHolder = new VerifyActivityViewHolder(findViewById(R.id.main_rl), null);
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
		FontLoader.setRobotoRegularTypeface(mVerifyActivityViewHolder.mVerificationMessageTV,mVerifyActivityViewHolder.mVerificationCodeET);
		
		mVerifyActivityViewHolder.mVerificationCodeET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					verificationCode = mVerifyActivityViewHolder.mVerificationCodeET.getText().toString().trim();
					if (TextUtils.isEmpty(verificationCode)) {
						mErrorLayout.showError(getResources().getString(R.string.verification_error), true, MsgType.Error);
					} else {
						activateAccountServerRequest();
					}
					return true;
				}
				return false;
			}
		});
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, VerifyAccountActivity.class);
		return intent;
	}

	OnClickListener VerifyAccoutListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			verificationCode = mVerifyActivityViewHolder.mVerificationCodeET.getText().toString().trim();
			if (TextUtils.isEmpty(verificationCode)) {
				// displayErrorMessage(getResources().getString(R.string.verification_error));
				mErrorLayout.showError(getResources().getString(R.string.verification_error), true, MsgType.Error);
			} else {
				activateAccountServerRequest();
			}
		}
	};

	public class VerifyActivityViewHolder {
		private EditText mVerificationCodeET;
		private TextView mVerificationMessageTV;

		public VerifyActivityViewHolder(View view, OnClickListener listener) {
			mVerificationCodeET = (EditText) view.findViewById(R.id.verification_et);
			mVerificationMessageTV = (TextView) view.findViewById(R.id.login_header_tv);
			mVerificationCodeET.requestFocus();
		}
	}

	private void activateAccountServerRequest() {
		VerifyAccountRequest mRequest = new VerifyAccountRequest(mContext);
		mRequest.ActivateAccountServerRequest(verificationCode);
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {

					DialogUtil.showOkCancelDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
							new OnOkButtonListner() {

								@Override
								public void onOkBUtton() {
									Intent intent = LoginActivity.getIntent(mContext);
									intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
											| Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(intent);
									finish();

								}
							},new OnCancelListener() {
								
								@Override
								public void onCancel(DialogInterface dialog) {
									Intent intent = LoginActivity.getIntent(mContext);
									intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
											| Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(intent);
									finish();
								}
							});

				} 
				else{
					mErrorLayout.showError(data.toString(), true, MsgType.Error);
				}
			}
		});
	}
}