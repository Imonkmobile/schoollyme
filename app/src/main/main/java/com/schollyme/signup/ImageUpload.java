package com.schollyme.signup;

import org.json.JSONObject;

public class ImageUpload {

	public String MediaGUID;
	public String ImageName;
	public String ImageServerPath;

	public ImageUpload(String mediaGUID, String imageName, String imageServerPath) {
		super();
		MediaGUID = mediaGUID;
		ImageName = imageName;
		ImageServerPath = imageServerPath;
	}

	public ImageUpload(JSONObject jsonObject) {
		MediaGUID = jsonObject.optString("MediaGUID");
		ImageName = jsonObject.optString("ImageName");
		ImageServerPath = jsonObject.optString("ImageServerPath");
	}

	public String getMediaGUID() {
		return MediaGUID;
	}

	public String getImageName() {
		return ImageName;
	}

	public String getImageServerPath() {
		return ImageServerPath;
	}
}
