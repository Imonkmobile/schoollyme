package com.schollyme.signup;

import android.os.Parcel;
import android.os.Parcelable;

public class LatLong implements Parcelable {

	private double latitude, logitude;

	public LatLong() {
		// TODO Auto-generated constructor stub
	}

	public LatLong(double latitude, double logitude) {
		super();
		this.latitude = latitude;
		this.logitude = logitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLogitude() {
		return logitude;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeDouble(latitude);
		dest.writeDouble(logitude);

	}

	public static final Creator<LatLong> CREATOR = new Creator<LatLong>() {

		@Override
		public LatLong createFromParcel(Parcel source) {
			return new LatLong(source);
		}

		@Override
		public LatLong[] newArray(int size) {
			return new LatLong[size];
		}
	};

	private LatLong(Parcel in) {
		this.latitude = in.readDouble();
		this.logitude = in.readDouble();

	}

}
