package com.schollyme.media;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.ErrorLayoutListener;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Album;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AlbumCreateRequest;
import com.vinfotech.request.AlbumExistsRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class CreateAlbumActivity extends BaseActivity implements OnClickListener {
	public static final int REQ_CODE_CREATE_ALBUM_ACTIVITY = 1014;
	private TextView mPrivacyTv;
	private EditText mTitleEt;
	private EditText mDescEt;

	private AlbumExistsRequest mAlbumExistsRequest;
	private AlbumCreateRequest mAlbumCreateRequest;
	private HeaderLayout mHeaderLayout;
	private ErrorLayout mErrorLayout;
	private ImageView mPrivacyIv;
	private Album mAlbum;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_album_activity);

		mAlbum = getIntent().getParcelableExtra("album");
		if (Config.DEBUG) {
			Log.d("CreateAlbumActivity", "onCreate mAlbum=" + mAlbum);
		}
		if (null == mAlbum) {
			Toast.makeText(this, "Invalid album type.", Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		mHeaderLayout = new HeaderLayout(findViewById(R.id.container_ll));
		mHeaderLayout.setHeaderValues(R.drawable.icon_back,
				getResources().getString(TextUtils.isEmpty(mAlbum.AlbumGUID) ? R.string.Create_Album : R.string.Edit_Album),
				R.drawable.icon_arrw_right_active_white_xhdpi);
		mHeaderLayout.setListenerItI(this, this);
		mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));

		mAlbumExistsRequest = new AlbumExistsRequest(this);
		mAlbumExistsRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					Utility.hideSoftKeyboard(mDescEt);
					startActivity(MediaGridActivity.getIntent(CreateAlbumActivity.this,
							new LogedInUserModel(CreateAlbumActivity.this).mUserGUID, mAlbum));
					finish();
				} else {
					mErrorLayout.showError((String) data, true, MsgType.Error);
				}
			}
		});

		mAlbumCreateRequest = new AlbumCreateRequest(this);
		mAlbumCreateRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, final Object data, int totalRecords) {
				if (success) {
					setResult(Activity.RESULT_OK);
					mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {

						@Override
						public void onErrorShown() {
						}

						@Override
						public void onErrorHidden() {
							finish();
						}
					});
					mErrorLayout.showError(getString(R.string.Album_updated_successfully), true, MsgType.Success);
				} else if (null != data) {
					DialogUtil.showOkDialog(CreateAlbumActivity.this, (String) data, "");
				}
			}
		});

		mPrivacyTv = (TextView) findViewById(R.id.privacy_tv);
		mPrivacyIv = (ImageView) findViewById(R.id.privacy_iv);

		findViewById(R.id.visibility_ll).setOnClickListener(this);

		mTitleEt = (EditText) findViewById(R.id.title_et);
		mDescEt = (EditText) findViewById(R.id.desc_et);

		FontLoader.setRobotoRegularTypeface(mPrivacyTv, mTitleEt, mDescEt);
		updateData();
	}

	public static Intent getIntent(Context context, Album album) {
		Intent intent = new Intent(context, CreateAlbumActivity.class);
		intent.putExtra("album", album);
		return intent;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_button:
			Utility.hideSoftKeyboard(mDescEt);
			finish();
			break;
		case R.id.right_button:
			if (isValid()) {
				if (TextUtils.isEmpty(mAlbum.AlbumGUID)) {
					mAlbumExistsRequest.checkAlbumExistsInServer(mAlbum.AlbumName, mAlbum.AlbumType);
				} else {
					mAlbumCreateRequest.createAlbumInServer(mAlbum.AlbumGUID, mAlbum.AlbumName, mAlbum.Description, mAlbum.Visibility,
							mAlbum.AlbumType, null, null);
				}
			}
			break;
		case R.id.visibility_ll:
			DialogUtil.showListDialog(v.getContext(), R.string.Album_Visibility, new OnItemClickListener() {

				@Override
				public void onItemClick(int position, String item) {
					switch (position) {
					case 0:
						mPrivacyIv.setImageResource(R.drawable.ic_globe);
						break;
					case 1:
						mPrivacyIv.setImageResource(R.drawable.ic_lock_grey);
						break;

					default:
						break;
					}
					mPrivacyTv.setText(item);
				}

				@Override
				public void onCancel() {

				}
			}, getString(R.string.Public), getString(R.string.Teammates));
			break;

		default:
			break;
		}
	}

	private void updateData() {
		mPrivacyTv.setText((mAlbum.Visibility == AlbumCreateRequest.VISIBILITY_PUBLIC ? R.string.Public : R.string.Teammates));
		mPrivacyTv.setText((mAlbum.Visibility == AlbumCreateRequest.VISIBILITY_PUBLIC ? R.string.Public : R.string.Teammates));
		mPrivacyIv.setImageResource(mAlbum.Visibility == AlbumCreateRequest.VISIBILITY_PUBLIC ? R.drawable.ic_globe
				: R.drawable.ic_lock_grey);
		mTitleEt.setText(mAlbum.AlbumName);
		mDescEt.setText(mAlbum.Description);
	}

	private boolean isValid() {
		mAlbum.Visibility = (getString(R.string.Public).equalsIgnoreCase(mPrivacyTv.getText().toString().trim()) ? AlbumCreateRequest.VISIBILITY_PUBLIC
				: AlbumCreateRequest.VISIBILITY_TEAMMATES);
		mAlbum.AlbumName = mTitleEt.getText().toString().trim();
		if (TextUtils.isEmpty(mAlbum.AlbumName)) {
			mErrorLayout.showError(getString(R.string.Please_enter_Album_title), true, MsgType.Error);
			return false;
		}
		mAlbum.Description = mDescEt.getText().toString().trim();
		if (TextUtils.isEmpty(mAlbum.Description)) {
			mErrorLayout.showError(getString(R.string.Please_enter_Album_description), true, MsgType.Error);
			return false;
		}
		return true;
	}

	private void setResultAndFinish(Object album) {
		Utility.hideSoftKeyboard(mDescEt);
		Intent data = new Intent();
		if (null != album) {
			data.putExtra("album", mAlbum);
			setResult(RESULT_OK, data);
		} else {
			setResult(RESULT_CANCELED);
		}
		finish();
	}
}
