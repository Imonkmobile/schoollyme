package com.schollyme.media;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.schollyme.BaseActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.NewYoutubeUrl;
import com.schollyme.model.Youtube;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.ValidateYutubeRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class UploadYoutubeActivity extends BaseActivity implements OnClickListener {
	public static final int REQ_CODE_UPLOAD_YOUTUBE_ACTIVITY = 1020;
	private EditText mUrlEt;

	private ValidateYutubeRequest mValidateYutubeRequest;
	private HeaderLayout mHeaderLayout;
	private ErrorLayout mErrorLayout;
	private String mYoutubeUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upload_youtube_activity);

		mHeaderLayout = new HeaderLayout(findViewById(R.id.container_ll));
		mHeaderLayout.setHeaderValues(R.drawable.ic_close_header, getResources().getString(R.string.Add_Video),
				R.drawable.icon_arrw_right_active_white_xhdpi);
		mHeaderLayout.setListenerItI(this, this);
		mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));

		mUrlEt = (EditText) findViewById(R.id.url_et);
		mUrlEt.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (EditorInfo.IME_ACTION_DONE == actionId) {
					validateURL();
					return true;
				}
				return false;
			}
		});
		FontLoader.setRobotoRegularTypeface(mUrlEt);

		mValidateYutubeRequest = new ValidateYutubeRequest(this);
		mValidateYutubeRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success && null != data && data instanceof Youtube) {
					Youtube youtube = (Youtube) data;
					setResultAndFinish(new NewYoutubeUrl(mYoutubeUrl, youtube.title, "", "", "", youtube.video_length), youtube);
				} else {
					mErrorLayout.showError(getString(R.string.Youtube_video_does_not_exists), true, MsgType.Error);
				}
			}
		});
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, UploadYoutubeActivity.class);
		return intent;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_button:
			setResultAndFinish(null, null);
			break;
		case R.id.right_button:
			validateURL();
			break;

		default:
			break;
		}
	}

	private void validateURL() {
		mYoutubeUrl = mUrlEt.getText().toString().trim();
		int index = -1;
		if (mYoutubeUrl.toLowerCase().startsWith("http://youtu.be") || mYoutubeUrl.toLowerCase().startsWith("https://youtu.be")) {
			index = mYoutubeUrl.lastIndexOf("/");
		} else {
			index = mYoutubeUrl.indexOf("=");
		}
		if (mYoutubeUrl.length() > 0 && android.util.Patterns.WEB_URL.matcher(mYoutubeUrl).matches() && -1 != index
				&& index < mYoutubeUrl.length() - 1) {
			// mValidateYutubeRequest.getVideoDetailsInServer(mYoutubeUrl.substring(index
			// + 1));
			mValidateYutubeRequest.getVideoInfoInServer(mYoutubeUrl.substring(index + 1));
		} else {
			mErrorLayout.showError(getString(R.string.invalid_url), true, MsgType.Error);
		}
	}

	private void setResultAndFinish(Object newYoutubeUrl, Youtube youtube) {
		Utility.hideSoftKeyboard(mUrlEt);
		Intent data = new Intent();
		if (null != newYoutubeUrl) {
			ArrayList<NewYoutubeUrl> newYoutubeUrls = new ArrayList<NewYoutubeUrl>();
			newYoutubeUrls.add((NewYoutubeUrl) newYoutubeUrl);
			data.putParcelableArrayListExtra("newYoutubeUrls", newYoutubeUrls);
			data.putExtra("youtube", youtube);
			setResult(RESULT_OK, data);
		} else {
			setResult(RESULT_CANCELED);
		}
		finish();
	}
}
