package com.schollyme;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.schollyme.Config.UserType;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.UserTypeModel;
import com.vinfotech.request.GetUsersTypeRequest;
import com.vinfotech.request.SignUpRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.ValidationUtil;

/**
 * Activity class. This may be useful in Signup implementation
 * 
 * @author Ravi Bhandari
 * 
 */
public class SignupActivity extends GCMBaseActivity implements OnClickListener, GoogleApiClient.ConnectionCallbacks {

	private SignupViewHolder mSignupViewHolder;
	private Context mContext;
	static final int DATE_PICKER_ID = 1111;
	protected int mMsgResId = 0;
	private int year, month, day;
	private Calendar mCalender;

	private String emailString, passwordString, cPasswordString, userNameString, profileType, dob;

	private double mCurLat = 0.0, mCurLog = 0.0;
	private static final String TAG = "Signup";
	private LocationRequest locationRequest;
	private GoogleApiClient googleApiClient;
	private FusedLocationProviderApi fusedLocationProviderApi;
	public static ArrayList<UserTypeModel> mUserTypeAL;
	private String mUserType = "3";
	private ErrorLayout mErrorLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup_activity);
		mContext = this;
		setHeader(findViewById(R.id.header_layout), R.drawable.selector_cancel, R.drawable.selector_confirm,
				getResources().getString(R.string.signup_label), new OnClickListener() {
			@Override
			public void onClick(View v) {
				Utility.hideSoftKeyboard(mSignupViewHolder.mEmailET);
				finish();
				overridePendingTransition(0, R.anim.slide_down_dialog);
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (validateSignUp()) {
					signUpServerRequest();
				}
			}
		});
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
		mSignupViewHolder = new SignupViewHolder(findViewById(R.id.main_rl), this);
		profileType = String.valueOf(UserType.Fan);
		Utility.textSpann(mContext, (TextView) findViewById(R.id.tnc_tv), getResources().getString(R.string.tnc_text));
		initDatePicker();
		getLocation();
		getUserTypeServerRequest();
	}

	public static Intent getIntent(Context context) {
		Intent intent1 = new Intent(context, SignupActivity.class);
		return intent1;
	}

	/** validate signup parameters */
	private boolean validateSignUp() {
		boolean isValidated = false;

		emailString = mSignupViewHolder.mEmailET.getText().toString().trim();
		passwordString = mSignupViewHolder.mPasswordET.getText().toString().trim();
		cPasswordString = mSignupViewHolder.mCPasswordET.getText().toString().trim();
		userNameString = mSignupViewHolder.mUserNameET.getText().toString().trim();
		profileType = mSignupViewHolder.mProfileTypeET.getText().toString().trim();
		
		DecimalFormat mFormat = new DecimalFormat("00");
		int cMonth = month + 1;

		String mMonth = mFormat.format(Double.valueOf(cMonth));
		String mDay = mFormat.format(Double.valueOf(day));
		
		dob = new StringBuilder().append(year).append("-").append(mMonth).append("-").append(mDay).toString();//mSignupViewHolder.mDobTV.getText().toString();
		if (TextUtils.isEmpty(userNameString)) {
			mErrorLayout.showError(getResources().getString(R.string.user_fullname_require), true, MsgType.Error);
		} else if (TextUtils.isEmpty(emailString)) {
			mErrorLayout.showError(getResources().getString(R.string.email_require_message), true, MsgType.Error);
		} else if (!ValidationUtil.isValidEmail(emailString)) {
			mErrorLayout.showError(getResources().getString(R.string.email_notvalid_message), true, MsgType.Error);
		} else if (TextUtils.isEmpty(passwordString)) {
			mErrorLayout.showError(getResources().getString(R.string.password_require_message), true, MsgType.Error);
		} else if (passwordString.length() < 5) {
			mErrorLayout.showError(getResources().getString(R.string.password_length_message), true, MsgType.Error);
		} else if (TextUtils.isEmpty(cPasswordString)) {
			mErrorLayout.showError(getResources().getString(R.string.cpassword_require_message), true, MsgType.Error);
		} else if (!passwordString.equals(cPasswordString)) {
			mErrorLayout.showError(getResources().getString(R.string.password_not_match_message), true, MsgType.Error);
		} else if (TextUtils.isEmpty(profileType)) {
			mErrorLayout.showError(getResources().getString(R.string.profile_type_is_require), true, MsgType.Error);
		} else if (TextUtils.isEmpty(dob)) {
			mErrorLayout.showError(getResources().getString(R.string.dob_require), true, MsgType.Error);
		} else if (Utility.getDateDifference(dob) < 13) {
			mErrorLayout.showError(getResources().getString(R.string.age_restriction_error), true, MsgType.Error);
		} else {
			isValidated = true;
		}
		return isValidated;
	}

	/** Listeners */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.tnc_tv:
			startActivity(TNCActivity.getIntent(mContext));
			break;
		case R.id.profile_type_et:
			if(HttpConnector.getConnectivityStatus(mContext)==0){
				DialogUtil.showOkDialog(mContext, Utility.FromResouarceToString(mContext, R.string.No_internet_connection),
						Utility.FromResouarceToString(mContext, R.string.Network_erro));
			}
			else{
				if(null!=mUserTypeAL && mUserTypeAL.size()>0){
					displayProfileType();
				}
				else{
					getUserTypeServerRequest();
				}
			}
			
			break;
		case R.id.dob_tv:
			displayDatePicker();
			break;
		}
	}

	private void displayProfileType() {
		
			DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
				@Override
				public void onItemClick(int position, String item) {
					mUserType = mUserTypeAL.get(position).mTypeId;
					mSignupViewHolder.mProfileTypeET.setText(mUserTypeAL.get(position).mTypeName);
				}
	
				@Override
				public void onCancel() {
	
				}
			}, mContext.getString(R.string.user_type_coach), mContext.getString(R.string.user_type_athlete),
			mContext.getString(R.string.user_type_fan));
	}

	private void initDatePicker() {
		mCalender = Calendar.getInstance();
		year = mCalender.get(Calendar.YEAR);
		month = mCalender.get(Calendar.MONTH);
		day = mCalender.get(Calendar.DAY_OF_MONTH);
	}

	private void displayDatePicker() {
		DatePickerDialog dialog = new DatePickerDialog(mContext, pickerListener, year, month, day);
		dialog.getDatePicker().setMaxDate(mCalender.getTimeInMillis());
		dialog.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
		dialog.show();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:

			return new DatePickerDialog(this, pickerListener, year, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;
			DecimalFormat mFormat = new DecimalFormat("00");
			int cMonth = month + 1;
			String cDay = mFormat.format(Double.valueOf(day));

			String mMonth = mFormat.format(Double.valueOf(cMonth));
			// Month is 0 based, just add 1
	//		mSignupViewHolder.mDobTV.setText(new StringBuilder().append(year).append("-").append(mMonth).append("-").append(cDay));
			mSignupViewHolder.mDobTV.setText(new StringBuilder().append(cDay).append(" ").append(Utility.getMOnthNameFromNumber(month)).append(",").append(" ").append(year));
		}
	};

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(0, R.anim.slide_down_dialog);
	}

	/** init location provide for get user location */

	private void getLocation() {
		try {
			locationRequest = LocationRequest.create();
			locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
			fusedLocationProviderApi = LocationServices.FusedLocationApi;
			googleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).build();
			if (googleApiClient != null) {
				googleApiClient.connect();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		try {
			Location location = fusedLocationProviderApi.getLastLocation(googleApiClient);
			if (location != null) {
				Log.e(TAG, "onLocationChanged: " + location);
				mCurLat = (double) (location.getLatitude());
				mCurLog = (double) (location.getLongitude());
				googleApiClient.disconnect();
			} else {
				getLocation();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onConnectionSuspended(int arg0) {

	}

	public class SignupViewHolder {

		public EditText mEmailET, mPasswordET, mCPasswordET, mUserNameET, mProfileTypeET;
		public TextView tncTV, mDobTV, mErrorMessageTV;

		public SignupViewHolder(View view, OnClickListener listener) {

			mEmailET = (EditText) view.findViewById(R.id.email_et);
			mPasswordET = (EditText) view.findViewById(R.id.password_et);
			mCPasswordET = (EditText) view.findViewById(R.id.confirm_password_et);
			mUserNameET = (EditText) view.findViewById(R.id.user_name_et);
			tncTV = (TextView) view.findViewById(R.id.tnc_tv);
			mProfileTypeET = (EditText) view.findViewById(R.id.profile_type_et);
			mDobTV = (TextView) view.findViewById(R.id.dob_tv);
			mErrorMessageTV = (TextView) view.findViewById(R.id.error_message_tv);
			mDobTV.setOnClickListener(listener);
			mProfileTypeET.setOnClickListener(listener);
			tncTV.setOnClickListener(listener);
			mUserNameET.requestFocus();
			FontLoader.setRobotoRegularTypeface(mEmailET, mPasswordET, mCPasswordET, mUserNameET, mProfileTypeET, mDobTV, tncTV);
		}
	}

	/** Server Request Response */

	private void signUpServerRequest() {
		SignUpRequest mSignUpRequest = new SignUpRequest(mContext);
		mSignUpRequest.signUpSever(userNameString, emailString, passwordString, dob, mUserType, Config.DEVICE_TYPE, mDeviceToken,
				String.valueOf(mCurLat), String.valueOf(mCurLog), Utility.getIPAddress(true), "", "");
		mSignUpRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {

					DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
							new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

							Intent intent = VerifyAccountActivity.getIntent(mContext);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
							finish();
						}
					});

				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), getResources().getString(R.string.app_name));

				}
			}
		});
	}

	private void getUserTypeServerRequest() {
		GetUsersTypeRequest mRequest = new GetUsersTypeRequest(mContext);
		mRequest.getUsersTypeServerRequest();
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mUserTypeAL = (ArrayList<UserTypeModel>) data;
				}
				else{
					DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),getResources().getString(R.string.app_name), new OnOkButtonListner() {
						
						@Override
						public void onOkBUtton() {
							finish();
						}
					});
				}
			}
		});
	}
}