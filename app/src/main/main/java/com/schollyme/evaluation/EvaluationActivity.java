package com.schollyme.evaluation;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Evaluation;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.AthleteRequestEvaluationRequest;
import com.vinfotech.request.EvaluationsListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

public class EvaluationActivity extends BaseActivity implements OnClickListener,OnRefreshListener{

	private ErrorLayout mErrorLayout;
	private MyEvaluationsViewHolder myEvaluationsViewHolder;
	private List<Evaluation> mEvaluationsModels;

	private Context mContext;
	private int pageIndex = 1;
	private boolean loadingFlag = false;
	private String mUserGUID;
	private String mUsersName;
	private String mSportsID,mSportsName;
	private int mActiveUserType;
	private String mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_evaluations_activity);
		mContext = this;
		mSportsID = getIntent().getStringExtra("SportsID");
		mSportsName = getIntent().getStringExtra("SportsName");
		mUserGUID = getIntent().getStringExtra("UserGUID");
		mUsersName = getIntent().getStringExtra("UserName");
		mActiveUserType = getIntent().getIntExtra("ActiveUserType",0);
		int rightMenuID = 0;
		myEvaluationsViewHolder = new MyEvaluationsViewHolder(findViewById(R.id.main_rl),this);
		
		mTitle = mUsersName + "'s "+getResources().getString(R.string.Evaluation);
		if(mActiveUserType== Config.USER_TYPE_ATHLETE){
			/**User Type Is athlete*/
			rightMenuID = R.drawable.ic_menu_search;
			setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, rightMenuID,
					getResources().getString(R.string.My_Evaluation), BackListener, FILTERListener);
			myEvaluationsViewHolder.mRequestTV.setText(getResources().getString(R.string.request_evaluations));
			myEvaluationsViewHolder.mRequestTV.setCompoundDrawablesWithIntrinsicBounds(0,0, 0, 0);
			myEvaluationsViewHolder.mBottomRL.setVisibility(View.VISIBLE);

		}else if(mActiveUserType== Config.USER_TYPE_EVALUATOR){
			/**User Type Is Evaluator*/
			rightMenuID = 0;
			setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, rightMenuID,
					mTitle, BackListener, null);
			myEvaluationsViewHolder.mRequestTV.setText(getResources().getString(R.string.write_evaluations));
			myEvaluationsViewHolder.mRequestTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_edit,0, 0, 0);
			myEvaluationsViewHolder.mBottomRL.setVisibility(View.VISIBLE);
		}else if(mActiveUserType== Config.USER_TYPE_PAID_COACH){
			/**User Type Is PaidCoach can only view evaluation from other coach*/
			rightMenuID = 0;
			setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, rightMenuID,
					mTitle, BackListener, null);
			myEvaluationsViewHolder.mBottomRL.setVisibility(View.GONE);
		}

		

		myEvaluationsViewHolder.mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		myEvaluationsViewHolder.mSwipeRefreshWidget.setOnRefreshListener(this);
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));

		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		getMyEvaluations(mUserGUID, mSportsID);
	};

	OnClickListener BackListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			finish();
			
		}
	};
	
	OnClickListener FILTERListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			startActivityForResult(MySportsListActivity.getIntent(mContext, mUserGUID, getString(R.string.select_filter),mSportsID), 2000);
			/*Intent intent = MySportsListActivity.getIntent(mContext, mUserGUID, getString(R.string.select_filter),mSportsID);
		    startActivity(intent);*/
		}
	};
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK && requestCode == 2000){
			mSportsID = data.getStringExtra("SportsID");
			mSportsName  = data.getStringExtra("SportsName");
		}
	};

	private void setAdapter(final List<Evaluation> mEvaluationsList){
		EvaluationAdapter mAdapter = new EvaluationAdapter(mContext,mUserGUID,mErrorLayout,new DeleteEvaluationListener());
		mAdapter.setList(mEvaluationsList);
		myEvaluationsViewHolder.mMyEvaluationsLV.setAdapter(mAdapter);
		myEvaluationsViewHolder.mMyEvaluationsLV.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int pos,
					long arg3) {
				startActivity(EvaluationDetailActivity.getIntent(mContext, mUserGUID,mTitle, mEvaluationsList.get(pos),mEvaluationsList.get(pos).EvaluationGUID,""));
			}
		});
		if(mEvaluationsList.size()==0){
			myEvaluationsViewHolder.mNoRecordTV.setVisibility(View.VISIBLE);
		}
		else{
			myEvaluationsViewHolder.mNoRecordTV.setVisibility(View.GONE);
		}
	}

	public class MyEvaluationsViewHolder{

		private ListView mMyEvaluationsLV;
		private SwipeRefreshLayout mSwipeRefreshWidget;
		private TextView mRequestTV,mNoRecordTV;
		private RelativeLayout mBottomRL;

		public MyEvaluationsViewHolder(View view, OnClickListener listener) {
			mMyEvaluationsLV = (ListView) findViewById(R.id.mysports_lv);
			mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
			mRequestTV = (TextView) view.findViewById(R.id.action_tv);
			mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
			mBottomRL = (RelativeLayout) view.findViewById(R.id.bottom_action_rl);
			mRequestTV.setOnClickListener(listener);
			mBottomRL.setOnClickListener(listener);
			FontLoader.setRobotoRegularTypeface(mRequestTV,mNoRecordTV);
		}
	}

	@Override
	public void onRefresh() {
		getMyEvaluations(mUserGUID, mSportsID);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_tv:
		case R.id.bottom_action_rl:
			
			//User type in Athlete,can Request for evaluation to coach
			if(mActiveUserType==Config.USER_TYPE_ATHLETE && mUserGUID.equals(new LogedInUserModel(mContext).mUserGUID)){
				DialogUtil.showOkCancelDialog(mContext, "", getResources().getString(R.string.request_eval_confirm), new OnClickListener() {

					@Override
					public void onClick(View v) {
						requestEvaluationToCoach(mSportsID);
					}
				}, new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});
				//User type Evaluator, can write evaluation for athlete
			}else if(mActiveUserType==Config.USER_TYPE_EVALUATOR){
				startActivity(WriteEvaluationActivity.getIntent(mContext, mUserGUID, mSportsID, mSportsName,null,"",mTitle));
			}
			break;

		default:
			break;
		}
	}

	public static Intent getIntent(Context mContext,String UserGUID,String UsersName,String SportsID,String SportsName,int ActiveUserType){
		Intent intent = new Intent(mContext,EvaluationActivity.class);
		intent.putExtra("UserGUID", UserGUID);
		intent.putExtra("UserName", UsersName);
		intent.putExtra("SportsID", SportsID);
		intent.putExtra("SportsName", SportsName);
		intent.putExtra("ActiveUserType", ActiveUserType);
		return intent;
	}

	private void getMyEvaluations(String mUserGUID,String mSportsID){
		EvaluationsListRequest mRequest = new EvaluationsListRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				myEvaluationsViewHolder.mSwipeRefreshWidget.setRefreshing(false);
				if(success){
					mEvaluationsModels = (List<Evaluation>) data;
					setAdapter(mEvaluationsModels);
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.EvaluationsListServerRequest(mUserGUID,mSportsID);
	}
	
	public class DeleteEvaluationListener{
		public void onDelete(){
			getMyEvaluations(mUserGUID, mSportsID);
		}
	}

	private void requestEvaluationToCoach(String mSportsID){
		AthleteRequestEvaluationRequest mRequest = new AthleteRequestEvaluationRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mErrorLayout.showError(data.toString(), true,MsgType.Success);
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.EvaluationsListServerRequest(mSportsID);
	}
}