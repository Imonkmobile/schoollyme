package com.schollyme.evaluation;

import java.util.List;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.schollyme.R;
import com.schollyme.model.SportsModel;
import com.vinfotech.utility.FontLoader;

public class UsersSportsListAdapter extends BaseAdapter {
	private StateViewHolder mStateViewHolder;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<SportsModel>  mSports;
	private String mSelectedFilter;
	private String mHeaderText;

	public UsersSportsListAdapter(Context context,String SelectedFilter,String HeaderText) {
		this.mContext = context;
		this.mSelectedFilter = SelectedFilter;
		this.mHeaderText = HeaderText;
		this.mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(List<SportsModel> list) {
		this.mSports = list;
		//setFilter(null);
		notifyDataSetChanged();
	}



	@Override
	public int getCount() {
		return mSports.size();
	}

	@Override
	public SportsModel getItem(int position) {
		return mSports.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		mStateViewHolder = new StateViewHolder();
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_item_view, parent, false);
			mStateViewHolder.mTitleTV = (TextView) convertView.findViewById(R.id.title_tv);
			mStateViewHolder.mSelectIB = (ImageButton) convertView.findViewById(R.id.select_ib);
			convertView.setTag(mStateViewHolder);
		} else {
			mStateViewHolder = (StateViewHolder) convertView.getTag();
		}
		mStateViewHolder.mTitleTV.setText(mSports.get(position).mSportsName);
		mStateViewHolder.mTitleTV.setTag(position);

		FontLoader.setRobotoRegularTypeface(mStateViewHolder.mTitleTV);
		convertView.setClickable(false);

		/*if(mHeaderText!=null && !TextUtils.isEmpty(mHeaderText) && !TextUtils.isEmpty(mSelectedFilter) && mSelectedFilter.equals(mSports.get(position).mSportsID)){
			mStateViewHolder.mSelectIB.setVisibility(View.VISIBLE);
		}
		else{
			mStateViewHolder.mSelectIB.setVisibility(View.GONE);
		}*/

		return convertView;
	}
	
	public void resetFilter(String SelectedFilter){
		mSelectedFilter = SelectedFilter;
		notifyDataSetChanged();
	}

	private class StateViewHolder {
		public TextView mTitleTV;
		public ImageButton mSelectIB;
	}
}