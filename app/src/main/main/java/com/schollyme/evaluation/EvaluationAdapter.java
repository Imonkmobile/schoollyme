package com.schollyme.evaluation;

import java.util.List;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.evaluation.EvaluationActivity.DeleteEvaluationListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Evaluation;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.EvaluationDeleteRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

public class EvaluationAdapter extends BaseAdapter {
	private static final String TAG = EvaluationAdapter.class.getSimpleName();

	private LayoutInflater mLayoutInflater;
	private List<Evaluation> mEvaluations;
	private Context mContext;
	private String mUserGUID;
	private ErrorLayout mErrorLayout;
	private DeleteEvaluationListener mDeleteEvaluationListener;

	public EvaluationAdapter(Context context,String UserGUID,ErrorLayout ErrorLayout,DeleteEvaluationListener mListener) {
		mContext = context;
		mUserGUID = UserGUID;
		mErrorLayout = ErrorLayout;
		this.mDeleteEvaluationListener = mListener;
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setList(List<Evaluation> list) {
		this.mEvaluations = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mEvaluations ? 0 : mEvaluations.size();
	}

	@Override
	public Evaluation getItem(int position) {
		return mEvaluations.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.evaluations_row, null);

			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final Evaluation evaluation = getItem(position);

		ImageLoaderUniversal.ImageLoadRound(mContext, Config.IMAGE_URL_PROFILE + evaluation.CreatedBy.ProfilePicture, viewHolder.userIv,
				ImageLoaderUniversal.option_Round_Image);

		RatingBar ratingBar = new RatingBar(new ContextThemeWrapper(mContext, R.style.StarRatingBar), null, 0);
		ratingBar.setIsIndicator(true);
		ratingBar.setRating(evaluation.RateValue);
		viewHolder.ratingsRl.addView(ratingBar);
		
		viewHolder.rightSubTitleTv.setText(Utility.getTimeFromDate(evaluation.CreatedDate));
		viewHolder.titleTv.setText(evaluation.CreatedBy.UserName);
		viewHolder.subTitleTv.setText(evaluation.Location);
		viewHolder.descTv.setText(evaluation.Sport.mSportsName);
		
		if(new LogedInUserModel(mContext).mUserGUID.equals(mUserGUID)){
			viewHolder.mOptionIB.setVisibility(View.VISIBLE);
			viewHolder.mOptionIB.setTag(position);
			viewHolder.mOptionIB.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(final View v) {
					final int pos = (int) v.getTag();
					final Evaluation mEval = mEvaluations.get(pos);
					DialogUtil.showOkCancelDialog(mContext, "", mContext.getString(R.string.delete_evaluation_confirmation), new OnClickListener() {

						@Override
						public void onClick(View v) {
							deleteEvaluationRequest(mEval.EvaluationGUID,pos);
						}
					}, new OnClickListener() {

						@Override
						public void onClick(View v) {

						}
					});
				}
			});
		}
		else{
			viewHolder.mOptionIB.setVisibility(View.GONE);
		}
		

		return convertView;
	}

	private class ViewHolder {
		private ImageView userIv;
		private RelativeLayout ratingsRl;
		private TextView titleTv, subTitleTv, descTv, rightSubTitleTv;
		private ImageButton mOptionIB;

		private ViewHolder(View view) {
			userIv = (ImageView) view.findViewById(R.id.user_iv);
			ratingsRl = (RelativeLayout) view.findViewById(R.id.ratings_rl);
			rightSubTitleTv = (TextView) view.findViewById(R.id.right_sub_title_tv);
			titleTv = (TextView) view.findViewById(R.id.title_tv);
			subTitleTv = (TextView) view.findViewById(R.id.sub_title_tv);
			descTv = (TextView) view.findViewById(R.id.desc_tv);
			mOptionIB = (ImageButton) view.findViewById(R.id.more_option_iv);
			
			FontLoader.setRobotoMediumTypeface(titleTv);
			FontLoader.setRobotoRegularTypeface(subTitleTv, descTv, rightSubTitleTv);
		}
	}
	
	private void deleteEvaluationRequest(String mEvaluationGUID,final int pos){
		
		EvaluationDeleteRequest mRequest = new EvaluationDeleteRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {
			
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mErrorLayout.showError(data.toString(), true,MsgType.Success);
					mEvaluations.remove(pos);
					notifyDataSetChanged();
					mDeleteEvaluationListener.onDelete();
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.deleteEvaluationServer(mEvaluationGUID);
	}
}