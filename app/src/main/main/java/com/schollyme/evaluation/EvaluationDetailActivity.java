package com.schollyme.evaluation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Evaluation;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.EvaluationDeleteRequest;
import com.vinfotech.request.EvaluationsDetailRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

public class EvaluationDetailActivity extends BaseActivity{

	private ErrorLayout mErrorLayout;
	private ViewHolder mViewHolder;
	private Evaluation mEvaluation;
	private Context mContext;
	private String mUserGUID;
	private String mEvaluationGUID;
	private String mFromActivity;
	private String mTitle;

	private void performBackAction(){
		Intent intent = DashboardActivity.getIntent(this, 5, "");
		startActivity(intent);
		this.finish();
	}
	
	@Override
	public void onBackPressed() {
		performBack();
	}

	private void performBack(){
		if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
			Intent intent = DashboardActivity.getIntent(this, 5, "");
			startActivity(intent);
			this.finish();
		} else
			super.onBackPressed();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.evaluation_detail_activity);
		mEvaluation = getIntent().getParcelableExtra("Evaluation");
		mFromActivity = getIntent().getStringExtra("fromActivity");
		mTitle = getIntent().getStringExtra("Title");
		mContext = this;
		mErrorLayout = new ErrorLayout(findViewById(R.id.con_rl));
		mUserGUID = getIntent().getStringExtra("UserGUID");
		mEvaluationGUID = getIntent().getStringExtra("EvaluationGUID");

		mViewHolder = new ViewHolder(findViewById(R.id.main_rl));
		if(new LogedInUserModel(mContext).mUserGUID.equals(mUserGUID)  || new LogedInUserModel(mContext).mUserProfileURL.equals(mUserGUID) || 
				getString(R.string.My_Evaluation).equals(mTitle)){
			setHeader(findViewById(R.id.header_layout), R.drawable.icon_back,  R.drawable.ic_dots_header,
					getResources().getString(R.string.My_Evaluation), new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
						performBackAction();
					}
					else{
						finish();
					}
				}
			}, new OnClickListener() {

				@Override
				public void onClick(View v) {
					DialogUtil.showOkCancelDialog(mContext, "", mContext.getString(R.string.delete_evaluation_confirmation), new OnClickListener() {

						@Override
						public void onClick(View v) {
							deleteEvaluationRequest(mEvaluation.EvaluationGUID);
						}
					}, new OnClickListener() {

						@Override
						public void onClick(View v) {

						}
					});
				}
			});
		}else{
			setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, 0,
					mTitle, new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
						performBackAction();
					}
					else{
						finish();
					}
				}
			}, null);
		}

		if(null== mEvaluation){
			geEvaluations(mEvaluationGUID);
		}
		else{
			setData();
		}



		/*	if(new LogedInUserModel(mContext).mUserGUID.equals(mUserGUID)){
			mViewHolder.mOptionIB.setVisibility(View.VISIBLE);
			mViewHolder.mOptionIB.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					DialogUtil.showOkCancelDialog(mContext, "", mContext.getString(R.string.delete_evaluation_confirmation), new OnClickListener() {

						@Override
						public void onClick(View v) {
							deleteEvaluationRequest(mEvaluation.EvaluationGUID);
						}
					}, new OnClickListener() {

						@Override
						public void onClick(View v) {

						}
					});
				}
			});
		}
		else{
			mViewHolder.mOptionIB.setVisibility(View.GONE);
		}*/

	}


	private void setData(){
		ImageLoaderUniversal.ImageLoadRound(mContext, Config.IMAGE_URL_PROFILE + mEvaluation.CreatedBy.ProfilePicture, mViewHolder.userIv,
				ImageLoaderUniversal.option_Round_Image);

		RatingBar ratingBar = new RatingBar(new ContextThemeWrapper(mContext, R.style.StarRatingBar), null, 0);
		ratingBar.setIsIndicator(true);
		ratingBar.setRating(mEvaluation.RateValue);
		mViewHolder.ratingsRl.addView(ratingBar);
		ratingBar.setRating(mEvaluation.RateValue);
		mViewHolder.mCreatedDate.setText(Utility.getTimeFromDate(mEvaluation.CreatedDate));
		mViewHolder.createrNameTV.setText(mEvaluation.CreatedBy.UserName);
		mViewHolder.mLocationTV.setText(mEvaluation.Location);
		mViewHolder.mSportsTv.setText(mEvaluation.Sport.mSportsName);
		mViewHolder.mEvalTitleTV.setText(mEvaluation.Title);
		mViewHolder.mEvalDescTV.setText(mEvaluation.Description);
	}

	public static Intent getIntent(Context mContext,String UserGUID,String mTitle,Evaluation mEvaluation,String EvaluationGUID,String fromActivity){
		Intent intent = new Intent(mContext,EvaluationDetailActivity.class);
		intent.putExtra("UserGUID", UserGUID);
		intent.putExtra("Title", mTitle);
		intent.putExtra("EvaluationGUID", EvaluationGUID);
		intent.putExtra("Evaluation", mEvaluation);
		intent.putExtra("fromActivity", fromActivity);
		return intent;
	}

	private class ViewHolder{

		private ImageView userIv;
		private RelativeLayout ratingsRl;
		private TextView createrNameTV, mCreatedDate, mLocationTV, mSportsTv,mEvalTitleTV,mEvalDescTV;

		private ViewHolder(View view) {

			userIv = (ImageView) view.findViewById(R.id.user_iv);
			createrNameTV = (TextView) view.findViewById(R.id.creator_tv);
			ratingsRl = (RelativeLayout) view.findViewById(R.id.ratings_rl);
			mCreatedDate = (TextView) view.findViewById(R.id.created_date_tv);
			mLocationTV = (TextView) view.findViewById(R.id.location_tv);
			mSportsTv = (TextView) view.findViewById(R.id.sports_tv);

			mEvalTitleTV = (TextView) view.findViewById(R.id.eval_title_tv);
			mEvalDescTV = (TextView) view.findViewById(R.id.eval_desc_tv);

			FontLoader.setRobotoMediumTypeface(createrNameTV);
			FontLoader.setRobotoRegularTypeface( mCreatedDate, mLocationTV, mSportsTv,mEvalTitleTV,mEvalDescTV);
		}
	}

	private void deleteEvaluationRequest(String mEvaluationGUID){

		EvaluationDeleteRequest mRequest = new EvaluationDeleteRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mErrorLayout.showError(data.toString(), true,MsgType.Success);
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							finish();
						}
					}, 1000);

				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.deleteEvaluationServer(mEvaluationGUID);
	}


	private void geEvaluations(String mEvaluationGUID){
		EvaluationsDetailRequest mRequest = new EvaluationsDetailRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if(success){
					mEvaluation = (Evaluation) data;
					setData();
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.EvaluationsDetailServerRequest(mEvaluationGUID);
	}


	/**/
}