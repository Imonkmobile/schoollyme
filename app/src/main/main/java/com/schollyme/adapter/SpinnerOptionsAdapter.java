package com.schollyme.adapter;

import java.util.ArrayList;

import com.schollyme.R;
import com.schollyme.SpinnerActivity;
import com.schollyme.model.UserTypeModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SpinnerOptionsAdapter extends BaseAdapter{
	
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private ArrayList<UserTypeModel> mAL;
	private ViewHolder mHolder;
	
	public SpinnerOptionsAdapter(Context context){
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(mContext);
	}
	
	public void setList(ArrayList<UserTypeModel> list){
		this.mAL = list;
	}

	@Override
	public int getCount() {
		if(null!=mAL){
			return mAL.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		mHolder = new ViewHolder();
		if(null==convertView){
			convertView = mLayoutInflater.inflate(R.layout.spinner_options_item, parent,false);
			mHolder.mTitleTV = (TextView) convertView.findViewById(R.id.title_tv);
			convertView.setTag(mHolder);
		}
		else{
			mHolder = (ViewHolder) convertView.getTag();
		}
		if(position==SpinnerActivity.selectedItemPosition){
			mHolder.mTitleTV.setBackgroundColor(mContext.getResources().getColor(R.color.translucent_color));
			mHolder.mTitleTV.setTextColor(mContext.getResources().getColor(R.color.white));
		}
		else{
			mHolder.mTitleTV.setBackgroundColor(mContext.getResources().getColor(R.color.transparent_color));
			mHolder.mTitleTV.setTextColor(mContext.getResources().getColor(R.color.app_text_color));
		}
		mHolder.mTitleTV.setText(mAL.get(position).mTypeName);
		return convertView;
	}

   private class ViewHolder {
	   public TextView mTitleTV;
   }
}
