package com.schollyme.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.TranscriptsRequestModel;
import com.vinfotech.request.AcceptDenyTranscriptsRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

public class TranscriptsRequestAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private ArrayList<TranscriptsRequestModel> mTranscriptsRequestModelAL;
	private TranscriptsRequestViewHolder mTranscriptsRequestViewHolder;
	private ErrorLayout mErrorLayout;

	public TranscriptsRequestAdapter(Context context, ErrorLayout errorLayout) {
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(mContext);
		this.mErrorLayout = errorLayout;
	}

	public void setList(ArrayList<TranscriptsRequestModel> mList) {
		this.mTranscriptsRequestModelAL = mList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mTranscriptsRequestModelAL ? 0 : mTranscriptsRequestModelAL.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(final int position, View contentView, ViewGroup parent) {
		mTranscriptsRequestViewHolder = new TranscriptsRequestViewHolder();

		if (contentView == null) {
			contentView = mLayoutInflater.inflate(R.layout.friend_request_item_fragment, parent, false);
			mTranscriptsRequestViewHolder.mFriendNameTV = (TextView) contentView.findViewById(R.id.user_name_tv);
			mTranscriptsRequestViewHolder.mFriendTypeTV = (TextView) contentView.findViewById(R.id.user_status_tv);
			mTranscriptsRequestViewHolder.mTranscriptsRequestAcceptBtn = (Button) contentView.findViewById(R.id.accept_button);
			mTranscriptsRequestViewHolder.mTranscriptsRequestDenyBtn = (Button) contentView.findViewById(R.id.deny_button);
			mTranscriptsRequestViewHolder.mFriendIV = (ImageView) contentView.findViewById(R.id.profile_image_iv);
			mTranscriptsRequestViewHolder.mMessageTV = (TextView) contentView.findViewById(R.id.message_tv);
			FontLoader.setRobotoMediumTypeface(mTranscriptsRequestViewHolder.mFriendNameTV, mTranscriptsRequestViewHolder.mTranscriptsRequestAcceptBtn,
					mTranscriptsRequestViewHolder.mTranscriptsRequestDenyBtn);
			FontLoader.setRobotoRegularTypeface(mTranscriptsRequestViewHolder.mFriendTypeTV);
			contentView.setTag(mTranscriptsRequestViewHolder);
		} else {
			mTranscriptsRequestViewHolder = (TranscriptsRequestViewHolder) contentView.getTag();
		}

		if(mTranscriptsRequestModelAL.get(position).mStatus.equals("ACCEPT")){
			mTranscriptsRequestViewHolder.mTranscriptsRequestAcceptBtn.setVisibility(View.GONE);
			mTranscriptsRequestViewHolder.mTranscriptsRequestDenyBtn.setVisibility(View.GONE);
			mTranscriptsRequestViewHolder.mFriendTypeTV.setVisibility(View.VISIBLE);
			mTranscriptsRequestViewHolder.mFriendNameTV.setVisibility(View.GONE);
			String mUserName = mTranscriptsRequestModelAL.get(position).mUserFName+" "+mTranscriptsRequestModelAL.get(position).mUserLName;
			SpannableString mSpanText = Utility.clickableTextSpanBlack(mContext, mContext.getResources().getString(R.string.transcripts_request_accepted), "","", mUserName);
			mTranscriptsRequestViewHolder.mFriendTypeTV.setVisibility(View.GONE);
			mTranscriptsRequestViewHolder.mMessageTV.setVisibility(View.VISIBLE);
			mTranscriptsRequestViewHolder.mMessageTV.setText(mSpanText);
			
		//	mTranscriptsRequestViewHolder.mFriendTypeTV.setGravity(Gravity.CENTER_VERTICAL);
		}
		else if(mTranscriptsRequestModelAL.get(position).mStatus.equals("DENY")){
			mTranscriptsRequestViewHolder.mTranscriptsRequestAcceptBtn.setVisibility(View.GONE);
			mTranscriptsRequestViewHolder.mTranscriptsRequestDenyBtn.setVisibility(View.GONE);
			mTranscriptsRequestViewHolder.mFriendTypeTV.setVisibility(View.VISIBLE);
			mTranscriptsRequestViewHolder.mFriendNameTV.setVisibility(View.GONE);
			String mUserName = mTranscriptsRequestModelAL.get(position).mUserFName+" "+mTranscriptsRequestModelAL.get(position).mUserLName;
			SpannableString mSpanText = Utility.clickableTextSpanBlack(mContext, mContext.getResources().getString(R.string.transcripts_request_rejected), "","", mUserName);
			mTranscriptsRequestViewHolder.mMessageTV.setVisibility(View.VISIBLE);
			mTranscriptsRequestViewHolder.mMessageTV.setText(mSpanText);
			mTranscriptsRequestViewHolder.mFriendTypeTV.setVisibility(View.GONE);
		}
		else{
			mTranscriptsRequestViewHolder.mFriendTypeTV .setVisibility(View.GONE);
			mTranscriptsRequestViewHolder.mTranscriptsRequestAcceptBtn.setTag(position);
			mTranscriptsRequestViewHolder.mTranscriptsRequestAcceptBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {

					final int pos = (Integer) view.getTag();
					String muserName = mTranscriptsRequestModelAL.get(position).mUserFName+" "+mTranscriptsRequestModelAL.get(position).mUserLName;
					String message = mContext.getString(R.string.are_you_sure_share_transcripts)+" "+muserName;
					DialogUtil.showOkCancelDialog(mContext, mContext.getString(R.string.share_transcript),
							message, new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							TranscriptsRequestModel mModel = mTranscriptsRequestModelAL.get(pos);
							acceptDenyTranscriptsRequestOfCoach(mModel.mUserGUID,"ACCEPT",pos);
						}

					}, new OnClickListener() {

						@Override
						public void onClick(View arg0) {

						}

					});
				}
			});

			mTranscriptsRequestViewHolder.mTranscriptsRequestDenyBtn.setTag(position);
			mTranscriptsRequestViewHolder.mTranscriptsRequestDenyBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					final int pos = (Integer) view.getTag();
					String muserName = mTranscriptsRequestModelAL.get(position).mUserFName+" "+mTranscriptsRequestModelAL.get(position).mUserLName;
					String message = mContext.getString(R.string.are_you_sure_deny_transcripts)/*+" "+muserName*/;
					DialogUtil.showOkCancelDialog(mContext, mContext.getString(R.string.deny_transcript),
							message, new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							TranscriptsRequestModel mModel = mTranscriptsRequestModelAL.get(pos);
							acceptDenyTranscriptsRequestOfCoach(mModel.mUserGUID,"DENY",pos);
						}

					}, new OnClickListener() {

						@Override
						public void onClick(View arg0) {

						}

					});
					
				}
			});
			mTranscriptsRequestViewHolder.mFriendTypeTV.setVisibility(View.VISIBLE);
			mTranscriptsRequestViewHolder.mFriendTypeTV.setText(Utility.getFormattedDate(mTranscriptsRequestModelAL.get(position).mCreatedDate,mContext));
		}
		
		mTranscriptsRequestViewHolder.mFriendNameTV.setText(mTranscriptsRequestModelAL.get(position).mUserFName + " "
				+ mTranscriptsRequestModelAL.get(position).mUserLName);
		mTranscriptsRequestViewHolder.mFriendNameTV.setTag(position);
		mTranscriptsRequestViewHolder.mFriendTypeTV.setTag(position);
		mTranscriptsRequestViewHolder.mFriendIV.setTag(position);
		mTranscriptsRequestViewHolder.mMessageTV.setTag(position);
		mTranscriptsRequestViewHolder.mFriendNameTV.setOnClickListener(UserProfileLineListener);
		mTranscriptsRequestViewHolder.mFriendTypeTV.setOnClickListener(UserProfileLineListener);
		mTranscriptsRequestViewHolder.mMessageTV.setOnClickListener(UserProfileLineListener);
		mTranscriptsRequestViewHolder.mFriendIV.setOnClickListener(UserProfileLineListener);
		String imageUrl = Config.IMAGE_URL_PROFILE + mTranscriptsRequestModelAL.get(position).mUserProfilePicture;
		ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mTranscriptsRequestViewHolder.mFriendIV,
				ImageLoaderUniversal.option_Round_Image);

		return contentView;
	}

	OnClickListener UserProfileLineListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			int pos = (Integer) view.getTag();
			TranscriptsRequestModel mModel = mTranscriptsRequestModelAL.get(pos);

			if (new LogedInUserModel(mContext).mUserGUID.equals(mModel.mUserGUID)) {
			} else {
				mContext.startActivity(FriendProfileActivity.getIntent(mContext,mModel.mUserGUID, mModel.mUserProfileURL, "TranscriptsRequestAdapter"));
			}
		}
	};

	private class TranscriptsRequestViewHolder {
		private TextView mFriendNameTV, mFriendTypeTV,mMessageTV;
		private ImageView mFriendIV;
		private Button mTranscriptsRequestAcceptBtn, mTranscriptsRequestDenyBtn;
	}

	private void acceptDenyTranscriptsRequestOfCoach(String friendGUID,final String status,final int pos) {

		AcceptDenyTranscriptsRequest mRequest = new AcceptDenyTranscriptsRequest(mContext);
		mRequest.getAcceptDenyTranscriptsRequest(friendGUID,status);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					if(status.equals("ACCEPT")){
						TranscriptsRequestModel model = mTranscriptsRequestModelAL.get(pos);
						model.mStatus = "ACCEPT";
						mTranscriptsRequestModelAL.set(pos, model);
						setList(mTranscriptsRequestModelAL);
					}
					else{
						TranscriptsRequestModel model = mTranscriptsRequestModelAL.get(pos);
						model.mStatus = "DENY";
						mTranscriptsRequestModelAL.set(pos, model);
						setList(mTranscriptsRequestModelAL);
						/*mTranscriptsRequestModelAL.remove(pos);
						notifyDataSetChanged();*/
					}

					mErrorLayout.showError(data.toString(), true, MsgType.Success);
				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
				}
			}
		});
	}
}