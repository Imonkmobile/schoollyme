package com.schollyme.adapter;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import chintan.khetiya.android.Twitter_code.Twitt_Sharing;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.media.MediaViewerActivity;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewsFeed;
import com.schollyme.team.TeamPageActivity;
import com.schollyme.wall.MediaViewerWallActivity;
import com.schollyme.wall.SingleMediaViewWallActivity;
import com.vinfotech.request.AlbumCreateRequest;
import com.vinfotech.request.DeleteFeedRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.CalStartEndUtil;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.ImageUtil;

public class PostFeedAdapter extends BaseAdapter {
	private LayoutInflater mLayoutInflater;
	private Context mContext;
	private float mExtraSpace, mImgWidth;
	private List<NewsFeed> mNewsFeeds;
	private OnItemClickListenerPost onItemClickListenerPost;

	public final String consumer_key = "DAIEsfXAb0IgmpUcunHBORtVD";
	public final String secret_key = "NmuIFsjTHiwrQkVpBJHNiZOtotXsoXklnpqeMcc4h657eplb4a";
	private String mUserGUID;
	private boolean mShareEnabled = true, mWriteEnabled = true, mShowBuzzMarker = true;

	public PostFeedAdapter(Context context, OnItemClickListenerPost onItemClickListenerPost, String mUserGUID_) {
		mLayoutInflater = LayoutInflater.from(context);
		this.onItemClickListenerPost = onItemClickListenerPost;
		mContext = context;
		mNewsFeeds = new ArrayList<NewsFeed>();
		this.mUserGUID = mUserGUID_;
		int[] screenDimen = new int[2];
		DisplayUtil.probeScreenSize(context, screenDimen);
		mExtraSpace = context.getResources().getDimension(R.dimen.space_mid10);
		mImgWidth = screenDimen[0] - mExtraSpace;
	}

	public void setList(List<NewsFeed> list) {
		mNewsFeeds = list;
		notifyDataSetChanged();
	}

	public void setList(List<NewsFeed> list, boolean shareEnabled) {
		mNewsFeeds = list;
		mShareEnabled = shareEnabled;
		notifyDataSetChanged();
	}

	public void setWriteEnabled(boolean enabled, boolean showBuzzMarker) {
		mWriteEnabled = enabled;
		mShowBuzzMarker = showBuzzMarker;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mNewsFeeds ? 0 : mNewsFeeds.size();
	}

	// String string_img_url = null, string_msg = null;

	public void onClickTwitt(final NewsFeed newsfeed, final String string_img_url, final String string_msg) {
		if (isNetworkAvailable()) {
			final Twitt_Sharing twitt = new Twitt_Sharing((Activity) mContext, consumer_key, secret_key);
			String imgPath = "";
			if (!TextUtils.isEmpty(string_img_url)) {
				if (MediaViewerActivity.isYouTubeVideo(string_img_url)) {
					imgPath = MediaViewerActivity.getYoutubeVideoThumb(string_img_url);
				} else {
					if (null != newsfeed.Album.AlbumMedias && newsfeed.Album.AlbumMedias.size() > 0
							&& AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(newsfeed.Album.AlbumMedias.get(0).MediaType)) {
						imgPath = Config.getCoverPath(newsfeed.Album, Config.getVideoToJPG(newsfeed.Album.AlbumMedias.get(0).ImageName),
								true);
					} else {
						imgPath = Config.getCoverPath(newsfeed.Album, newsfeed.Album.AlbumMedias.get(0).ImageName, false);
					}
				}
				ImageLoaderUniversal.getImageLoader().loadImage(imgPath, new SimpleImageLoadingListener() {
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						File file = ImageUtil.saveBitmap(loadedImage, Environment.getExternalStorageDirectory(), "twitt.jpg");

						if (MediaViewerActivity.isYouTubeVideo(string_img_url)) {
							twitt.shareToTwitter(string_img_url, file);
						} else {
							String mediaUrl = "";
							if (null != newsfeed.Album.AlbumMedias && newsfeed.Album.AlbumMedias.size() > 0
									&& AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(newsfeed.Album.AlbumMedias.get(0).MediaType)) {
								mediaUrl = Config.getCoverPath(newsfeed.Album, newsfeed.Album.AlbumMedias.get(0).ImageName, true, false);
							}
							twitt.shareToTwitter(mediaUrl, file);
						}
					}

					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
						Toast.makeText(mContext, "Twitter: unsupported media.", Toast.LENGTH_SHORT).show();
					}
				});
			} else if (!TextUtils.isEmpty(string_msg)) {
				twitt.shareToTwitter(string_msg, null);
			}

		} else {
			// showToast("No Network Connection Available !!!");
		}
	}

	// when user will click on twitte then first that will check that is
	// internet exist or not
	public boolean isNetworkAvailable() {
		ConnectivityManager connectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public NewsFeed getItem(int position) {
		return mNewsFeeds.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 100 + position;
	}

	public static final int ConvertViewID = 56565;

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.post_multiple_row, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (position == 0 && mWriteEnabled) {
			viewHolder.mWritePostLL.setVisibility(View.VISIBLE);
			viewHolder.mMainLL.setVisibility(View.GONE);
			String imageUrl = Config.IMAGE_URL_PROFILE + new LogedInUserModel(mContext).mUserPicURL;
			ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, viewHolder.mUserIV, ImageLoaderUniversal.option_Round_Image);
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onItemClickListenerPost.onClickItems(ConvertViewID, position, null);
				}
			});
		} else {
			viewHolder.mWritePostLL.setVisibility(View.GONE);
			viewHolder.mMainLL.setVisibility(View.VISIBLE);
			convertView.setOnClickListener(null);

			final NewsFeed newsFeedModel = getItem(position);
			final List<AlbumMedia> albumMedias = newsFeedModel.Album.AlbumMedias;
			final Activity activity = ((Activity) mContext);
			ImageLoaderUniversal.ImageLoadRound(mContext, Config.IMAGE_URL_PROFILE + newsFeedModel.UserProfilePicture,
					viewHolder.mOtherUserIV, ImageLoaderUniversal.option_Round_Image);

			final String message = getMessage(newsFeedModel);
			setLinkText(viewHolder.mTittleOhterTV, message);
			// viewHolder.mTittleOhterTV.setText(Html.fromHtml(message));
			if (newsFeedModel.IsOwner == 1 || newsFeedModel.IsEntityOwner == 1) {
				viewHolder.mMenuIV.setVisibility(View.VISIBLE);
			} else {
				viewHolder.mMenuIV.setVisibility(View.INVISIBLE);
			}
			viewHolder.mbuzzMarkerView.setVisibility(mShowBuzzMarker && newsFeedModel.Mybuzz ? View.VISIBLE : View.INVISIBLE);
			viewHolder.mPrivacyIv.setImageResource(newsFeedModel.Visibility == AlbumCreateRequest.VISIBILITY_PUBLIC ? R.drawable.ic_globe
					: R.drawable.ic_lock_grey);

			if (newsFeedModel.ShareAllowed == 1 && mShareEnabled && newsFeedModel.Visibility != 3) {

				viewHolder.mShareTV.setVisibility(View.VISIBLE);
				viewHolder.share_bottom_rl.setVisibility(View.VISIBLE);
			} else {
				viewHolder.mShareTV.setVisibility(View.GONE);
				viewHolder.share_bottom_rl.setVisibility(View.GONE);
			}

			viewHolder.mPostCommnetTV.setText(Html.fromHtml(newsFeedModel.PostContent.trim()));
			if ("".equals(newsFeedModel.PostContent.trim())) {
				viewHolder.mPostCommnetTV.setVisibility(View.GONE);
			} else {
				viewHolder.mPostCommnetTV.setVisibility(View.VISIBLE);
			}
			viewHolder.mLikeTV.setSelected(newsFeedModel.IsLike == 0 ? false : true);
			updateCommentCount(viewHolder.mCommentsTV, newsFeedModel);
			updateLikeCount(viewHolder.mLikesTV, newsFeedModel);

			Calendar cal = getCalTime(newsFeedModel.CreatedDate);
			if (null != cal) {
				viewHolder.mTimeAgoTV.setText(CalStartEndUtil.getDifference(mContext, cal));
			} else {
				viewHolder.mTimeAgoTV.setText((newsFeedModel.CreatedDate));
			}
			String mediaUrl = "";
			if (albumMedias != null) {
				viewHolder.mContainerMediaRl.setVisibility(View.VISIBLE);
				if (albumMedias.size() == 0) {
					viewHolder.mContainerMediaRl.setVisibility(View.GONE);

				} else if (albumMedias.size() == 1) {
					viewHolder.mWallPostVP.setVisibility(View.GONE);
					viewHolder.mWallPostIV.setVisibility(View.VISIBLE);
					LayoutParams layoutParams = viewHolder.mWallPostIV.getLayoutParams();
					layoutParams.width = (int) (mImgWidth - mExtraSpace * 3.5f);
					layoutParams.height = layoutParams.width;
					viewHolder.mWallPostIV.setLayoutParams(layoutParams);

					if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
						viewHolder.mPlayIV.setVisibility(View.GONE);
						viewHolder.mLoaderUIL.setVisibility(View.GONE);
						ImageLoaderUniversal.ImageLoadSquare(mContext,
								Config.getCoverPath(newsFeedModel.Album, albumMedias.get(0).ImageName, false), viewHolder.mWallPostIV,
								ImageLoaderUniversal.option_normal_Image_Thumbnail);
					} else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
						viewHolder.mPlayIV.setVisibility(View.VISIBLE);

						ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext,
								Config.getCoverPath(newsFeedModel.Album, Config.getVideoToJPG(albumMedias.get(0).ImageName), true),
								viewHolder.mWallPostIV, ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoaderUIL);

					} else if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
						viewHolder.mPlayIV.setVisibility(View.VISIBLE);
						ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext,
								MediaViewerActivity.getYoutubeVideoThumb(albumMedias.get(0).ImageName), viewHolder.mWallPostIV,
								ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoaderUIL);
					}

				} else if (albumMedias.size() > 1) {
					viewHolder.mLoaderUIL.setVisibility(View.GONE);
					viewHolder.mPlayIV.setVisibility(View.GONE);
					viewHolder.mWallPostVP.setVisibility(View.VISIBLE);
					viewHolder.mWallPostIV.setVisibility(View.GONE);
					ViewPagerAdapter mViewPagerAdapter = new ViewPagerAdapter(albumMedias, newsFeedModel.Album);
					mediaUrl = mViewPagerAdapter.getMediaURl();

					LayoutParams layoutParams = viewHolder.mWallPostVP.getLayoutParams();
					// layoutParams.width = (int) mImgWidth-10;
					layoutParams.width = (int) LayoutParams.MATCH_PARENT;
					layoutParams.height = (int) (mImgWidth * 0.66f);
					viewHolder.mWallPostVP.setPageMargin(20);
					viewHolder.mWallPostVP.setLayoutParams(layoutParams);
					viewHolder.mWallPostVP.setAdapter(mViewPagerAdapter);
				}
			} else {
				viewHolder.mContainerMediaRl.setVisibility(View.GONE);
			}

			viewHolder.mMenuIV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					showMenuOptionDialog(newsFeedModel, position);
				}
			});
			viewHolder.mShareTV.setTag(mediaUrl);
			viewHolder.mShareTV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					DialogUtil.showOkCancelDialog(v.getContext(), "", v.getContext().getString(R.string.Are_share_this_post),
							new OnClickListener() {

								@Override
								public void onClick(View v) {
									onItemClickListenerPost.onClickItems(R.id.share_tv, position, newsFeedModel);
								}
							}, null);

				}
			});

			viewHolder.mLikeTV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					DisplayUtil.bounceView(mContext, viewHolder.mLikeTV);
					onItemClickListenerPost.onClickItems(R.id.like_tv, position, newsFeedModel);
				}
			});
			viewHolder.mCommentTV.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onItemClickListenerPost.onClickItems(R.id.comment_tv, position, newsFeedModel);

				}
			});
			viewHolder.mLikesTV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onItemClickListenerPost.onClickItems(R.id.likes_tv, position, newsFeedModel);
				}
			});

			viewHolder.mCommentsTV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onItemClickListenerPost.onClickItems(R.id.comments_tv, position, newsFeedModel);
				}
			});

			viewHolder.mWallPostIV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (AlbumMedia.MEDIA_TYPE_YOUTUBE

					.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
						playYouTubeURl(albumMedias.get(0).ImageName, activity);

					} else if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
						// ArrayList<AlbumMedia> albumMediasss = new
						// ArrayList<AlbumMedia>(
						// albumMedias);
						// activity.startActivityForResult(
						// MediaViewerActivity.getIntent(activity,
						// albumMediasss, 0, newsFeedModel.Album),
						// MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);

						if (newsFeedModel.Album.AlbumName.startsWith("Wall Photos")) {
							activity.startActivity(SingleMediaViewWallActivity.getIntent(activity, albumMedias.get(0).ImageName, "IMAGE",
									true));
							activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
						} else {

							Activity activity = ((Activity) mContext);
							ArrayList<AlbumMedia> albumMedias_ = new ArrayList<AlbumMedia>(albumMedias);
							System.out.println("ACTIVITY TYPE-- " + newsFeedModel.ActivityType);
							if ("ShareMediaSelf".equals(newsFeedModel.ActivityType)) {

								activity.startActivityForResult(MediaViewerWallActivity.getIntent(activity, 0, newsFeedModel.Album, true,
										albumMedias_.get(0).MediaGUID), MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
								activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

							} else {

								activity.startActivityForResult(
										MediaViewerWallActivity.getIntent(activity, 0, newsFeedModel.Album, false, ""),
										MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
								activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

							}

							// activity.startActivityForResult(MediaViewerWallActivity.getIntent(activity,
							// 0, newsFeedModel.Album),
							// MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
							// activity.overridePendingTransition(R.anim.fade_in,
							// R.anim.fade_out);

						}

					} else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
						if (newsFeedModel.Album.AlbumName.startsWith("Wall Video")) {
							activity.startActivity(SingleMediaViewWallActivity.getIntent(activity, albumMedias.get(0).ImageName, "VIDEO",
									true));
							activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
						} else {

							Activity activity = ((Activity) mContext);
							ArrayList<AlbumMedia> albumMedias_ = new ArrayList<AlbumMedia>(albumMedias);
							activity.startActivityForResult(MediaViewerActivity.getIntent(activity, albumMedias_, 0, newsFeedModel.Album),
									MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
							activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
						}

					}

				}
			});

			// viewHolder.mWallPostVP.setOnClickListener(new OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// activity.startActivityForResult(MediaGridActivity
			// .getIntent(activity,zz, newsFeedModel.Album),
			// MediaGridActivity.REQ_CODE_MEDIA_GRID_ACTIVITY);
			// }
			// });

		}
		return convertView;
	}

	public void playYouTubeURl(String url, Activity activity) {

		String id = MediaViewerActivity.getYouTubeVideoId(url);
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
			activity.startActivity(intent);
		} catch (ActivityNotFoundException ex) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(MediaViewerActivity.youtubePrefix2 + id));
			activity.startActivity(intent);
		}

	}

	protected void showMenuOptionDialog(final NewsFeed newsFeedModel, final int position) {
		boolean allowTwitterShare = false;
		if ((null != newsFeedModel.Album && null != newsFeedModel.Album.AlbumMedias && newsFeedModel.Album.AlbumMedias.size() > 0)
				|| !TextUtils.isEmpty(newsFeedModel.PostContent)) {
			allowTwitterShare = true;
		}
		final String[] options = allowTwitterShare ? new String[] { mContext.getString(R.string.post_twitter), mContext.getString(R.string.Delete) } : new String[] { mContext.getString(R.string.Delete) };
		DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {

			@Override
			public void onItemClick(int position, String item) {
				switch (position) {

				case 0:
					if (options.length == 1) {
						showDeleteDialog(newsFeedModel, position);
					} else {
						Album malbum = newsFeedModel.Album;
						if (null != malbum.AlbumMedias && malbum.AlbumMedias.size() > 0) {
							onClickTwitt(newsFeedModel, newsFeedModel.Album.AlbumMedias.get(0).ImageName, newsFeedModel.PostContent);
						} else {
							onClickTwitt(newsFeedModel, "", newsFeedModel.PostContent);
						}
					}

					break;
				case 1:
					showDeleteDialog(newsFeedModel, position);
					break;
				}
			}

			@Override
			public void onCancel() {

			}
		}, options);

	}

	public interface OnItemClickListenerPost {
		void onClickItems(int ID, int position, NewsFeed newsFeed);

	}

	protected void showDeleteDialog(final NewsFeed newsFeedModel, final int Position) {
		DialogUtil.showOkCancelDialog(mContext, R.string.Delete, mContext.getString(R.string.Delete_post),
				mContext.getString(R.string.Confirm_delete_Post), new OnClickListener() {

					@Override
					public void onClick(View v) {
						mDeleteFeedRequest = new DeleteFeedRequest(mContext);
						mDeleteFeedRequest.deleteNewsFeedFromServer(newsFeedModel.ActivityGUID);
						mDeleteFeedRequest.setRequestListener(new RequestListener() {
							@Override
							public void onComplete(boolean success, Object data, int totalRecords) {
								if (success) {
									mNewsFeeds.remove(newsFeedModel);
									notifyDataSetChanged();
									// DialogUtil.showOkDialog(mContext,
									// (String) data, "");
								} else {
									DialogUtil.showOkDialog(mContext, (String) data, "");
								}
							}
						});
					}
				}, null);
	}

	private DeleteFeedRequest mDeleteFeedRequest;

	private void updateCommentCount(TextView textView, NewsFeed newsFeed) {
		textView.setText(newsFeed.NoOfComments + " "
				+ (textView.getResources().getString(1 == newsFeed.NoOfComments ? R.string.Comment : R.string.Comments)));

	}

	private void updateLikeCount(TextView textView, NewsFeed newsFeed) {
		textView.setText(newsFeed.NoOfLikes + " "
				+ (textView.getResources().getString(1 == newsFeed.NoOfLikes ? R.string.Like : R.string.Likes)));
	}

	// private void updateLikeButtonStatus(NewsFeed newsFeed) {
	//
	// if (newsFeed.IsLike == 0) {
	//
	// newsFeed.IsLike = 1;
	// newsFeed.NoOfLikes++;
	// } else {
	//
	// newsFeed.IsLike = 0;
	// newsFeed.NoOfLikes--;
	// }
	// notifyDataSetChanged();
	//
	// }

	/*
	 * For Tittle in message and pass newsfeed object
	 */

	private static final String USER_MARKER = "{{User}}";
	private static final String ENTITY_MARKER = "{{Entity}}";
	private static final String COUNT_MARKER = "{{count}}";
	private static final String ALBUM_TYPE_MARKER = "{{AlbumType}}";
	private static final String SUBJECT_MARKER = "{{SUBJECT}}";
	private static final String OBJECT_MARKER = "{{OBJECT}}";
	private static final String ENTITYTYPE_MARKER = "{{ENTITYTYPE}}";

	public static String ACTIVITYTYPE_AlbumAdded = "AlbumAdded";
	public static String ACTIVITYTYPE_AlbumUpdate = "AlbumUpdated";
	public static String ACTIVITYTYPE_GroupJoined = "GroupJoined";
	public static String ACTIVITYTYPE_GroupPostAdded = "GroupPostAdded";
	public static String ACTIVITYTYPE_PagePost = "PagePost";
	public static String ACTIVITYTYPE_FriendAdded = "FriendAdded";
	public static String ACTIVITYTYPE_Share = "Share";
	public static String ACTIVITYTYPE_ShareMedia = "ShareMedia";
	public static String ACTIVITYTYPE_ShareSelf = "ShareSelf";
	public static String ACTIVITYTYPE_ShareMediaSelf = "ShareMediaSelf";
	public static String ACTIVITYTYPE_Post = "Post";
	public static String ACTIVITYTYPE_PostSelf = "PostSelf";
	public static String ACTIVITYTYPE_DefaultAlbumAdded = "DefaultAlbumAdded";

	public String getMessage(NewsFeed newsFeed) {

		String message = newsFeed.Message;

		if (mContext instanceof TeamPageActivity) {
			if (message.startsWith(USER_MARKER) && !newsFeed.ActivityType.equals(ACTIVITYTYPE_GroupJoined)) {
				message = message.substring(0, USER_MARKER.length());
			}
		}

		if (message.contains(USER_MARKER)) {
			// if (newsFeed.UserGUID.equals(mUserGUID)) {
			// message = message.replace(USER_MARKER,
			// getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID +
			// "&UserProfileLink=" + "NOTREDIRECTION", "Profile"));
			// // message = message.replace(USER_MARKER, newsFeed.UserName);
			// } else {

			if (null != newsFeed.ActivityType && newsFeed.ActivityType.equals("GroupPostAdded")) {

				if (mContext instanceof TeamPageActivity) {
					message = message.replace(
							USER_MARKER,
							getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL,
									"Profile"));
				} else {
					message = message.replace(
							USER_MARKER,
							getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL,
									"Profile"))
							+ " "
							+ mContext.getResources().getString(R.string.posted_in)
							+ " "
							+ getLink(newsFeed.EntityName, "TeamGUID=" + newsFeed.EntityGUID, "Group");
				}

			} else {
				message = message.replace(
						USER_MARKER,
						getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL,
								"Profile"));
			}
			// }

		}

		if (newsFeed.ActivityType.equals(ACTIVITYTYPE_AlbumAdded)) {

			if (message.contains(ENTITY_MARKER)) {
				// ALBUMNAMECASE
				// message = message.replace(ENTITY_MARKER,
				// newsFeed.EntityName);
				message = message.replace(
						ENTITY_MARKER,
						getLink(newsFeed.EntityName, "UserGUID=" + newsFeed.UserGUID + "&FromQuery=FromQuery&AlbumGUID="
								+ newsFeed.Album.AlbumGUID, "Added"));

			}
			if (message.contains(COUNT_MARKER)) {

				message = message.replace(COUNT_MARKER, "" + newsFeed.Count);

			}
			if (message.contains(ALBUM_TYPE_MARKER)) {
				if (null != newsFeed.Album && null != newsFeed.Album.AlbumName) {
					message = message.replace(ALBUM_TYPE_MARKER, newsFeed.Album.AlbumType);
				} else {
					message = message.replace(ALBUM_TYPE_MARKER, "");
				}
			}

		} else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_AlbumUpdate)) {

			if (message.contains(COUNT_MARKER)) {

				message = message.replace(COUNT_MARKER, "" + newsFeed.Count);

			}

			if (message.contains(ALBUM_TYPE_MARKER)) {
				if (null != newsFeed.Album && null != newsFeed.Album.AlbumName) {
					message = message.replace(ALBUM_TYPE_MARKER, newsFeed.Album.AlbumType);
				} else {
					message = message.replace(ALBUM_TYPE_MARKER, "");
				}
			}
			if (message.contains(ENTITY_MARKER)) {
				message = message.replace(ENTITY_MARKER, newsFeed.EntityName);
			}

		} else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_GroupJoined)) {

			if (message.contains(ENTITY_MARKER)) {

				if (mContext instanceof TeamPageActivity) {
					message = message.replace(ENTITY_MARKER, "");
					// message = message.replace(ENTITY_MARKER,
					// newsFeed.EntityName);
				} else {
					message = message.replace(ENTITY_MARKER,
							getLink(newsFeed.EntityName, "TeamGUID=" + newsFeed.EntityGUID + "&FromQuery=FromQuery", "Group"));
				}

			}

		} else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_GroupPostAdded)) {

			if (message.contains(ENTITY_MARKER)) {
				message = message.replace(ENTITY_MARKER, newsFeed.EntityName);
			}
		} else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_PagePost)) {
			if (message.contains(ENTITY_MARKER)) {
				message = message.replace(ENTITY_MARKER, newsFeed.EntityName);
			}

		} else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_FriendAdded)) {

			if (message.contains(ENTITY_MARKER)) {

				// message = message.replace(ENTITY_MARKER,
				// newsFeed.EntityName);

				message = message.replace(
						ENTITY_MARKER,
						getLink(newsFeed.EntityName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.EntityProfileURL,
								"Profile"));
			}
		} else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_Share) || newsFeed.ActivityType.equals(ACTIVITYTYPE_ShareMedia)) {

			if (newsFeed.UserGUID.equals(mUserGUID)) {

				message = message.replace(SUBJECT_MARKER,
						getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + "NOTREDIRECTION", "Profile"));

			} else {
				message = message.replace(
						SUBJECT_MARKER,
						getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL,
								"Profile"));
			}

			if (message.contains(ENTITYTYPE_MARKER)) {
				// if (newsFeed.Album.l) {
				// message = message.replace(ENTITYTYPE_MARKER,"Post" )
				// }else {
				// message = message.replace(ENTITYTYPE_MARKER,"Post" )
				//
				// // AlbumType first Index
				// }

				/* SHARE TYPE MISSING */
				message = message.replace(ENTITYTYPE_MARKER, newsFeed.EntityType);
			}

			if (message.contains(OBJECT_MARKER)) {
				message = message.replace(
						OBJECT_MARKER,
						getLink(newsFeed.ActivityOwner, "UserGUID=" + newsFeed.EntityGUID + "&UserProfileLink="
								+ newsFeed.ActivityOwnerLink, "Profile"));

				// message = message.replace(OBJECT_MARKER,
				// newsFeed.EntityName);
			}
		} else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_ShareSelf) || newsFeed.ActivityType.equals(ACTIVITYTYPE_ShareMediaSelf)) {

			// if (newsFeed.UserGUID.equals(mUserGUID)) {
			//
			// message = message.replace(SUBJECT_MARKER, newsFeed.UserName);
			// } else {
			message = message.replace(SUBJECT_MARKER,
					getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL, "Profile"));
			// }

			if (message.contains(ENTITYTYPE_MARKER)) {
				message = message.replace(ENTITYTYPE_MARKER, newsFeed.EntityType);
			}

			if (message.contains(OBJECT_MARKER)) {
				LogedInUserModel mLogedInUserModel = new LogedInUserModel(mContext);
				if (newsFeed.ActivityOwnerLink.equals(newsFeed.UserProfileURL)) {

					if (mLogedInUserModel.mUserGender.equals("1")) {
						message = message.replace(OBJECT_MARKER, mContext.getResources().getString(R.string.his));
					} else {
						message = message.replace(OBJECT_MARKER, mContext.getResources().getString(R.string.her));
					}
					if (message.contains("his's")) {
						message = message.replace("his's", "his");
					} else if (message.contains("her's")) {
						message = message.replace("her's", "her");
					}

					/*
					 * message = message.replace( OBJECT_MARKER,
					 * getLink(newsFeed.ActivityOwner, "UserGUID=" +
					 * newsFeed.EntityGUID + "&UserProfileLink=" +
					 * newsFeed.ActivityOwnerLink, "Profile"));
					 */
				} else {
					message = message.replace(
							OBJECT_MARKER,
							getLink(newsFeed.ActivityOwner, "UserGUID=" + newsFeed.EntityGUID + "&UserProfileLink="
									+ newsFeed.ActivityOwnerLink, "Profile"));
				}

			}

		} else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_Post)) {

			if (message.contains(OBJECT_MARKER)) {

				message = message.replace(
						OBJECT_MARKER,
						getLink(newsFeed.EntityName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.EntityProfileURL,
								"Profile"));
				// message = message.replace(OBJECT_MARKER,
				// newsFeed.EntityName);
			}
			if (message.contains(SUBJECT_MARKER)) {
				// if (newsFeed.UserGUID.equals(mUserGUID)) {
				// message = message.replace(SUBJECT_MARKER, newsFeed.UserName);
				// } else {
				message = message.replace(
						SUBJECT_MARKER,
						getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL,
								"Profile"));
				// }
			}

		} else if ((newsFeed.ActivityType.equals(ACTIVITYTYPE_PostSelf))) {
			if (message.contains(SUBJECT_MARKER)) {
				// if (newsFeed.UserGUID.equals(mUserGUID)) {
				// message = message.replace(SUBJECT_MARKER, newsFeed.UserName);
				// } else {
				message = message.replace(
						SUBJECT_MARKER,
						getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL,
								"Profile"));
				// }
			}
		} else if ((newsFeed.ActivityType.equals(ACTIVITYTYPE_DefaultAlbumAdded))) {

		}

		return message;
	}

	private String getLink(String disp, String data, String forClass) {
		LogedInUserModel mLogedInUserModel = new LogedInUserModel(mContext);

		String Link = "";
		if (forClass.equals("Profile")) {
			if (data.contains("UserProfileLink=" + mLogedInUserModel.mUserProfileURL)) {
				Link = "<a href='schollyme-dashboard://app.postfeed.wall/params?" + data + "'>" + disp + "</a>";
			} else {
				Link = "<a href='schollyme-userprofile://app.postfeed.wall/params?" + data + "'>" + disp + "</a>";
			}
		} else if (forClass.equals("Added")) {

			Link = "<a href='mediagrid://app.postfeed.wall/params?" + data + "'>" + disp + "</a>";
			// schollyme-mediagrid
		} else if (forClass.equals("Group")) {

			Link = "<a href='group://app.team.wall/params?" + data + "'>" + disp + "</a>";
			// schollyme-mediagrid
		}

		return Link;
	}

	private class ViewHolder {
		private LinearLayout mWritePostLL, mMainLL;;
		private ImageView mUserIV, mOtherUserIV, mMenuIV, mPrivacyIv;
		private TextView mTittleOhterTV, mTimeAgoTV, mLikesTV, mCommentsTV, mLikeTV, mCommentTV, mShareTV;
		private ViewPager mWallPostVP;
		private ImageView mWallPostIV, mPlayIV;
		private RelativeLayout mContainerMediaRl;
		private ProgressBar mLoaderUIL;
		private TextView mPostCommnetTV;
		private View mbuzzMarkerView;

		private RelativeLayout like_bottom_rl, comment_bottom_rl, share_bottom_rl;

		//
		private ViewHolder(View view) {
			mContainerMediaRl = (RelativeLayout) view.findViewById(R.id.container_media);
			mWallPostVP = (ViewPager) view.findViewById(R.id.wall_post_vp);
			mWallPostVP.setPageMargin((int) view.getResources().getDimension(R.dimen.space_tinny2));
			mWallPostIV = (ImageView) view.findViewById(R.id.wall_post_iv);
			mWritePostLL = (LinearLayout) view.findViewById(R.id.write_post_ll);
			mLoaderUIL = (ProgressBar) view.findViewById(R.id.loading_pb);
			mMainLL = (LinearLayout) view.findViewById(R.id.main_fl);
			mUserIV = (ImageView) view.findViewById(R.id.user_iv);
			mPlayIV = (ImageView) view.findViewById(R.id.play_iv);
			mOtherUserIV = (ImageView) view.findViewById(R.id.user_other_iv);
			mMenuIV = (ImageView) view.findViewById(R.id.menu_iv);
			mPrivacyIv = (ImageView) view.findViewById(R.id.privacy_iv);
			mTimeAgoTV = (TextView) view.findViewById(R.id.time_tv);
			mTittleOhterTV = (TextView) view.findViewById(R.id.title_other_tv);
			mTittleOhterTV.setMovementMethod(LinkMovementMethod.getInstance());
			mLikesTV = (TextView) view.findViewById(R.id.likes_tv);
			mCommentsTV = (TextView) view.findViewById(R.id.comments_tv);
			mLikeTV = (TextView) view.findViewById(R.id.like_tv);
			mCommentTV = (TextView) view.findViewById(R.id.comment_tv);
			mShareTV = (TextView) view.findViewById(R.id.share_tv);
			mPostCommnetTV = (TextView) view.findViewById(R.id.post_content_tv);
			like_bottom_rl = (RelativeLayout) view.findViewById(R.id.like_bottom_rl);
			comment_bottom_rl = (RelativeLayout) view.findViewById(R.id.comment_bottom_rl);
			share_bottom_rl = (RelativeLayout) view.findViewById(R.id.share_bottom_rl);
			mbuzzMarkerView = view.findViewById(R.id.buzz_marker_view);

			// FontLoader.setRobotoMediumTypeface(mTittleOhterTV,mTittleOhterTV);
			FontLoader.setRobotoRegularTypeface(mTittleOhterTV, mTimeAgoTV, mCommentsTV, mLikesTV, mLikeTV, mCommentTV, mShareTV,
					mPostCommnetTV);
			FontLoader.SetFontToWholeView(mContext, mWritePostLL, FontLoader.getRobotoRegular(mContext));

		}
	}

	private class ViewPagerAdapter extends PagerAdapter {

		LayoutInflater li;
		List<AlbumMedia> mListMediasPager;
		private ImageView mMediaIV;
		private ProgressBar mLoaderUIL;
		View vl;
		private ImageButton mPlayButton;
		private Album mAlbum;

		public ViewPagerAdapter(List<AlbumMedia> _mListMediasPager, Album _album) {
			if (null != _mListMediasPager) {
				mListMediasPager = _mListMediasPager;
				mAlbum = _album;
			}
		}

		public String getMediaURl() {
			if (null != mListMediasPager && mListMediasPager.size() > 0) {
				return Config.IMAGE_URL_UPLOAD + Config.ALBUM_FOLDER + mListMediasPager.get(0).ImageName;
			}
			return "";
		}

		@Override
		public float getPageWidth(int position) {
			return 0.90f;
		}

		public int getCount() {
			return null == mListMediasPager ? 0 : mListMediasPager.size();
		}

		public Object instantiateItem(View collection, final int position) {
			li = mLayoutInflater = LayoutInflater.from(mContext);
			vl = li.inflate(R.layout.wall_post_pager_items, null);
			mMediaIV = (ImageView) vl.findViewById(R.id.media_pager_iv);
			mLoaderUIL = (ProgressBar) vl.findViewById(R.id.loading_pb);
			mPlayButton = (ImageButton) vl.findViewById(R.id.play_btn);
			AlbumMedia albumMedia = mListMediasPager.get(position);

			if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(albumMedia.MediaType)) {
				String imagePath = Config.getCoverPath(mAlbum, albumMedia.ImageName, false, true);
				ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, imagePath, mMediaIV,
						ImageLoaderUniversal.DiaplayOptionForProgresser, mLoaderUIL);
				mPlayButton.setVisibility(View.GONE);
			} else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(albumMedia.MediaType)) {
				mPlayButton.setVisibility(View.VISIBLE);
				String thumbUrl = Config.VIDEO_URL_UPLOAD + Config.ALBUM_FOLDER + Config.VIDEO_FOLDER
						+ albumMedia.ImageName.replaceAll("mp4", "jpg");
				ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, thumbUrl, mMediaIV,
						ImageLoaderUniversal.DiaplayOptionForProgresser, mLoaderUIL);

			} else if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(albumMedia.MediaType)) {
				mPlayButton.setVisibility(View.VISIBLE);
				ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext,
						MediaViewerActivity.getYoutubeVideoThumb(albumMedia.ImageName), mMediaIV,
						ImageLoaderUniversal.DiaplayOptionForProgresser, mLoaderUIL);
			}

			mMediaIV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Activity activity = ((Activity) mContext);

					if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(mListMediasPager.get(0).MediaType)) {
						playYouTubeURl(mListMediasPager.get(0).ImageName, (Activity) mContext);
					} else {
						activity.startActivityForResult(MediaViewerWallActivity.getIntent(activity, position, mAlbum, false, ""),
								MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
						activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

					}

					// activity.startActivityForResult(MediaViewerActivity.getIntent(activity,
					// albumMedias,0, mAlbum),
					// MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
					// getActivity().overridePendingTransition(R.anim.fade_in,
					// R.anim.fade_out);
				}
			});

			((ViewPager) collection).addView(vl, 0);
			return vl;
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {

		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == ((View) arg1);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}
	}

	Calendar cal = Calendar.getInstance();

	public Calendar getCalTime(String dateString) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			cal.setTime(sdf.parse(dateString));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TimeZone tz = cal.getTimeZone();
		int Offset = tz.getOffset(cal.getTimeInMillis());
		cal.setTimeInMillis(cal.getTimeInMillis() + Offset);

		return cal;
	}

	public void setLinkText(TextView textView, String text) {
		textView.setMovementMethod(LinkMovementMethod.getInstance());
		Spannable spannable = (Spannable) Html.fromHtml(text);
		for (URLSpan span : spannable.getSpans(0, spannable.length(), URLSpan.class)) {
			spannable.setSpan(new UnderlineSpan() {
				public void updateDrawState(TextPaint tp) {
					tp.setUnderlineText(false);
				}
			}, spannable.getSpanStart(span), spannable.getSpanEnd(span), 0);
		}
		textView.setText(spannable);
	}

}
