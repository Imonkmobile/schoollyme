package com.schollyme.adapter;

import java.util.List;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.InboxModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

public class InboxAdapter extends BaseAdapter{

	private InboxViewHolder mInboxViewHolder;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<InboxModel> mInboxAL;

	public InboxAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(List<InboxModel> mAList){
		this.mInboxAL = mAList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mInboxAL ? 0 : mInboxAL.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}


	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		mInboxViewHolder = new InboxViewHolder();
		if(convertView==null){
			convertView = mLayoutInflater.inflate(R.layout.inbox_item_view, parent,false);
			mInboxViewHolder.mProfileIV = (ImageView) convertView.findViewById(R.id.profile_image_iv);
			mInboxViewHolder.mUserNameTV = (TextView) convertView.findViewById(R.id.user_name_tv);
			mInboxViewHolder.mMessageTV = (TextView) convertView.findViewById(R.id.message_tv);
			mInboxViewHolder.mDateTV = (TextView) convertView.findViewById(R.id.time_tv);
			mInboxViewHolder.mMessageCountTV = (TextView) convertView.findViewById(R.id.message_count_tv);
			FontLoader.setRobotoMediumTypeface(mInboxViewHolder.mDateTV,mInboxViewHolder.mMessageCountTV);
			FontLoader.setRobotoBoldTypeface(mInboxViewHolder.mUserNameTV);
			FontLoader.setRobotoRegularTypeface(mInboxViewHolder.mMessageTV);
			convertView.setTag(mInboxViewHolder);
		}
		else{
			mInboxViewHolder = (InboxViewHolder) convertView.getTag();
		}
		String imageUrl;
		if(new LogedInUserModel(mContext).mUserGUID.equals(mInboxAL.get(position).mSenderUserGUID)){
			String userName = mInboxAL.get(position).mReceiverUserFName+" "+mInboxAL.get(position).mReceiverUserLName;
			mInboxViewHolder.mUserNameTV.setText(Html.fromHtml(userName));
			imageUrl  = Config.IMAGE_URL_PROFILE+mInboxAL.get(position).mReceiverProfilePicture;
		}
		else{
			String userName = mInboxAL.get(position).mSenderUserFName+" "+mInboxAL.get(position).mSenderUserLName;
			mInboxViewHolder.mUserNameTV.setText(Html.fromHtml(userName));
			imageUrl  = Config.IMAGE_URL_PROFILE+mInboxAL.get(position).mSenderProfilePicture;
		}

		ImageLoaderUniversal.ImageLoadRound(mContext,imageUrl, mInboxViewHolder.mProfileIV, ImageLoaderUniversal.option_Round_Image);

		mInboxViewHolder.mMessageTV.setText(mInboxAL.get(position).mSubject);
	//	mInboxViewHolder.mDateTV.setText(Utility.getTimeFromDate(mInboxAL.get(position).mCreatedDate));
		String nDate = Utility.getFormattedDate(mInboxAL.get(position).mCreatedDate,mContext);
		if(nDate.equalsIgnoreCase("Today")){
			mInboxViewHolder.mDateTV.setText(Utility.getTimeFromDate(mInboxAL.get(position).mCreatedDate));
		}
		else{
			mInboxViewHolder.mDateTV.setText(nDate);
		}
		
		
		String count = mInboxAL.get(position).mUnreadCount;
		int mCount = Integer.parseInt(count);
		if(mCount>0){
			mInboxViewHolder.mMessageCountTV.setVisibility(View.VISIBLE);
			mInboxViewHolder.mMessageCountTV.setText(mInboxAL.get(position).mUnreadCount);
			mInboxViewHolder.mMessageTV.setTextColor(mContext.getResources().getColor(R.color.lt_grey_text));
			mInboxViewHolder.mDateTV.setTextColor(mContext.getResources().getColor(R.color.inbox_msg_time_color));
		//	mInboxViewHolder.mUserNameTV.setTypeface(null, Typeface.BOLD);
			FontLoader.setRobotoBoldTypeface(mInboxViewHolder.mUserNameTV);
		}
		else{
			mInboxViewHolder.mMessageCountTV.setVisibility(View.GONE);
			mInboxViewHolder.mMessageTV.setTextColor(mContext.getResources().getColor(R.color.user_status_color));
			mInboxViewHolder.mDateTV.setTextColor(mContext.getResources().getColor(R.color.signup_normal_text_color));
		//	mInboxViewHolder.mUserNameTV.setTypeface(null, Typeface.NORMAL);
			FontLoader.setRobotoRegularTypeface(mInboxViewHolder.mUserNameTV);
		}


		return convertView;
	}

	private class InboxViewHolder{

		public ImageView mProfileIV;
		public TextView mUserNameTV,mMessageTV,mDateTV,mMessageCountTV;
	}
}