package com.schollyme.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.SchollyMeApplication.SongLoadListener;
import com.schollyme.handler.DeleteLayout;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Song;
import com.schollyme.model.SongSource;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class SongAdapter extends BaseAdapter {
    private static final String TAG = SongAdapter.class.getSimpleName();

    private LayoutInflater mLayoutInflater;
    private List<Song> mSongs;
    private Context mContext;
    private ContentType mContentType;
    private String mSongString, mSongsString;
    private boolean mEnableHeader = false, mEnableSelection = false;
    private Song mPlayingSong = null;
    private DeleteLayout mDeleteLayout;
    private SchollyMeApplication mSchollyMeApplication;
    private ErrorLayout mErrorLayout;

    public enum ContentType {
        PlaylistSongs, AllSongs, Last7Day;
    }

    public SongAdapter(Context context, ContentType contentType, ErrorLayout errorLayout) {
        mContext = context;
        mContentType = contentType;
        mErrorLayout = errorLayout;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSongString = context.getResources().getString(R.string.Song);
        mSongsString = context.getResources().getString(R.string.Songs);

        Application application = ((Activity) context).getApplication();
        if (application instanceof SchollyMeApplication) {
            mSchollyMeApplication = (SchollyMeApplication) application;
            MediaPlayer mp = mSchollyMeApplication.getMediaPlayer();
            if (null != mSchollyMeApplication) {
                mSchollyMeApplication.setSongPlayCompletionListener(new SchollyMeApplication.SongPlayCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        setPlayingSong(null);
                    }
                });
            }
        }
    }

    public void setList(List<Song> list) {
        this.mSongs = list;
        if (null != mSongs && null != mSchollyMeApplication) {
            for (Song song : list) {
                if (mSchollyMeApplication.isPlaying()
                        && (song.SongURL.equalsIgnoreCase(mSchollyMeApplication.getPlayingUrl()) || song.SongPreviewURL
                        .equalsIgnoreCase(mSchollyMeApplication.getPlayingUrl()))) {
                    mPlayingSong = song;
                    break;
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setPlayingSong(Song song) {
        mPlayingSong = song;
        notifyDataSetChanged();
    }

    public void setDeleteLayout(DeleteLayout deleteLayout) {
        mDeleteLayout = deleteLayout;
    }

    private int mSetEnableHeader = -1;

    public void enableHeader(boolean enable) {
        mSetEnableHeader = (enable ? 1 : 0);
        notifyDataSetChanged();
    }

    public boolean isEnabledHeader() {
        return mEnableHeader;
    }

    public int getSelectionCount() {
        int count = 0;
        if (null != mSongs) {
            for (Song song : mSongs) {
                if (song.selected) {
                    count++;
                }
            }
        }
        return count;
    }

    public void selectSingle(int position) {
        if (null != mSongs) {
            for (int i = 0; i < mSongs.size(); i++) {
                mSongs.get(i).selected = i == position;
            }
        }
        notifyDataSetChanged();
    }

    public List<Song> getSelections() {
        List<Song> songs = new ArrayList<Song>();
        if (null != mSongs) {
            for (Song song : mSongs) {
                if (song.selected) {
                    songs.add(song);
                }
            }
        }
        if (Config.DEBUG) {
            Log.d(TAG, "getSelections count=" + songs.size());
        }
        return songs;
    }

    @Override
    public int getCount() {
        return null == mSongs ? 0 : mSongs.size();
    }

    @Override
    public Song getItem(int position) {
        return mSongs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.song_row, null);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Song song = getItem(position);

        ImageLoaderUniversal.ImageLoadRound(mContext, song.ImageURL, viewHolder.songIv, ImageLoaderUniversal.option_Round_Image);
        viewHolder.typeIv.setImageResource(SongSource.getSongSourceImgResId(song.SourceName));
        if (null != mDeleteLayout) {
            viewHolder.overlayIv.setBackgroundResource(R.drawable.white_round_circle);
            viewHolder.overlayIv.setImageResource(R.drawable.icon_grey_tick);
            viewHolder.overlayIv.setVisibility(song.selected ? View.VISIBLE : View.GONE);
            viewHolder.songIv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (mEnableHeader && position == 0) {
                        return;
                    }
                    song.selected = !song.selected;
                    notifyDataSetChanged();
                    if (getSelectionCount() > 0) {
                        mDeleteLayout.showDelete();
                    } else {
                        mDeleteLayout.hideDelete();
                    }
                }
            });
        }

        viewHolder.titleTv.setText(song.Title);
        viewHolder.subTitleTv.setText(song.ArtistName);
        viewHolder.descTv.setText(song.AlbumName);

        if (mEnableSelection) {
            if (song.existing) {
                viewHolder.rightIv.setImageResource(R.drawable.ic_green_tick);
                viewHolder.rightIv.setVisibility(View.VISIBLE);
            } else {
                viewHolder.rightIv.setImageResource(R.drawable.icon_red_tick);
                viewHolder.rightIv.setVisibility(song.selected ? View.VISIBLE : View.GONE);
            }
        } else {
            int playingPos = (null != mSchollyMeApplication ? mSchollyMeApplication.getPlayingPos() : -1);
            final boolean sameSong = (position == playingPos);
            viewHolder.bufferingPb.setVisibility((sameSong && mSchollyMeApplication.isBuffering()) ? View.VISIBLE : View.GONE);
            final boolean songPlaying = (null != mPlayingSong && song.SongGUID.equalsIgnoreCase(mPlayingSong.SongGUID) && (ContentType.Last7Day == mContentType ? sameSong : true));
            viewHolder.rightIv.setImageResource(songPlaying ? R.drawable.ic_mid_pause : R.drawable.ic_mid_play);
            viewHolder.rightIv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if (HttpConnector.getConnectivityStatus(mContext) == 0) {
                        mErrorLayout.showError(v.getResources().getString(R.string.No_internet_connection), true, MsgType.Error);
                        return;
                    }
                    if (null == mSchollyMeApplication) {
                        return;
                    }
                    mSchollyMeApplication.setPlayingPos(position);
                    if (song.equals(mPlayingSong)) {
                        if (mSchollyMeApplication.isPlaying()) {
                            mSchollyMeApplication.pauseSong();
                            setPlayingSong(null);
                        } else {
                            mSchollyMeApplication.playSong();
                            setPlayingSong(mPlayingSong);
                        }
                    } else {
                        final String songPlayUrl = TextUtils.isEmpty(song.SongURL) ? song.SongPreviewURL : song.SongURL;
                        if (mSchollyMeApplication.playSong(song.Title,songPlayUrl, new SongLoadListener() {

                            @Override
                            public void onLoad(boolean success, String url, MediaPlayer mediaPlayer) {
                                if (!url.equalsIgnoreCase(songPlayUrl)) {
                                    setPlayingSong(null);
                                    return;
                                }
                                // v.setEnabled(true);
                                if (success) {
                                    setPlayingSong(song);
                                    mSchollyMeApplication.setSongPlayCompletionListener(new SchollyMeApplication.SongPlayCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mp) {
                                            setPlayingSong(null);
                                        }
                                    });
                                } else {
                                    mErrorLayout.showError(v.getResources().getString(R.string.This_song_can_not), true, MsgType.Error);
                                    notifyDataSetChanged();
                                }
                            }
                        })) {
                            // v.setEnabled(false);
                            notifyDataSetChanged();
                        } else if (null != mErrorLayout && mSchollyMeApplication.isBuffering()) {
                            mErrorLayout.showError(v.getResources().getString(R.string.Buffering), true, MsgType.Error);
                        } else if (null != mErrorLayout) {
                            mErrorLayout.showError(v.getResources().getString(R.string.Invalid_song_URL), true, MsgType.Error);
                        }
                    }
                }
            });
        }
        if (mEnableHeader) {
            if (position == 0) {
                viewHolder.headerTv.setText(R.string.CURRENT_SONG_OF_THE_DAY);
                viewHolder.headerTv.setVisibility(View.VISIBLE);
                viewHolder.separator1.setVisibility(View.VISIBLE);
            } else if (position == 1) {
                viewHolder.headerTv.setText(R.string.MORE_SONGS);
                viewHolder.headerTv.setVisibility(View.VISIBLE);
                viewHolder.separator1.setVisibility(View.VISIBLE);
            } else {
                viewHolder.headerTv.setVisibility(View.GONE);
                viewHolder.separator1.setVisibility(View.GONE);
            }
        } else {
            viewHolder.headerTv.setVisibility(View.GONE);
            viewHolder.separator1.setVisibility(View.GONE);
        }

        return convertView;
    }

    private class ViewHolder {
        private ImageView songIv, typeIv, rightIv, overlayIv;
        private TextView titleTv, subTitleTv, descTv, headerTv;
        private View separator1;
        private ProgressBar bufferingPb;

        private ViewHolder(View view) {
            overlayIv = (ImageView) view.findViewById(R.id.overlay_iv);
            songIv = (ImageView) view.findViewById(R.id.song_iv);
            typeIv = (ImageView) view.findViewById(R.id.type_iv);
            rightIv = (ImageView) view.findViewById(R.id.right_iv);
            titleTv = (TextView) view.findViewById(R.id.title_tv);
            subTitleTv = (TextView) view.findViewById(R.id.sub_title_tv);
            descTv = (TextView) view.findViewById(R.id.desc_tv);
            headerTv = (TextView) view.findViewById(R.id.header_tv);
            separator1 = view.findViewById(R.id.separator1);
            bufferingPb = (ProgressBar) view.findViewById(R.id.buffering_pb);
            bufferingPb.setVisibility(View.GONE);

            FontLoader.setRobotoMediumTypeface(titleTv);
            FontLoader.setRobotoRegularTypeface(subTitleTv, descTv, headerTv);

            setVisibilityAsPerList();
        }

        private void setVisibilityAsPerList() {
            songIv.setVisibility(View.VISIBLE);
            typeIv.setVisibility(View.VISIBLE);
            rightIv.setVisibility(View.VISIBLE);
            overlayIv.setVisibility(View.VISIBLE);
            titleTv.setVisibility(View.VISIBLE);
            subTitleTv.setVisibility(View.VISIBLE);
            descTv.setVisibility(View.VISIBLE);
            headerTv.setVisibility(View.VISIBLE);
            separator1.setVisibility(View.VISIBLE);
            switch (mContentType) {
                case PlaylistSongs:
                    overlayIv.setVisibility(View.GONE);
                    typeIv.setVisibility(View.GONE);
                    rightIv.setImageResource(R.drawable.ic_mid_play);
                    if (-1 == mSetEnableHeader) {
                        mEnableHeader = true;
                    } else {
                        mEnableHeader = (1 == mSetEnableHeader);
                    }
                    mEnableSelection = false;
                    break;
                case AllSongs:
                    overlayIv.setVisibility(View.GONE);
                    typeIv.setVisibility(View.GONE);
                    rightIv.setImageResource(R.drawable.icon_red_tick);
                    headerTv.setVisibility(View.GONE);
                    separator1.setVisibility(View.GONE);
                    if (-1 == mSetEnableHeader) {
                        mEnableHeader = false;
                    } else {
                        mEnableHeader = (1 == mSetEnableHeader);
                    }
                    mEnableSelection = true;
                    break;
                case Last7Day:
                    overlayIv.setVisibility(View.GONE);
                    rightIv.setImageResource(R.drawable.ic_mid_play);
                    if (-1 == mSetEnableHeader) {
                        mEnableHeader = true;
                    } else {
                        mEnableHeader = (1 == mSetEnableHeader);
                    }
                    mEnableSelection = false;
                    break;

                default:
                    break;
            }
        }
    }
}
