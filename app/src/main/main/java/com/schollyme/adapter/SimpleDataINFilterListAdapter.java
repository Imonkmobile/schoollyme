package com.schollyme.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.schollyme.R;
import com.vinfotech.utility.FontLoader;

public class SimpleDataINFilterListAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mLayoutInflater;

	private ArrayList<String> mList;
	private OnclearListner mOnclearListner;

	public SimpleDataINFilterListAdapter(Context context, OnclearListner onclearListner) {
		this.mContext = context;
		this.mLayoutInflater = LayoutInflater.from(mContext);
		this.mList = new ArrayList<String>();
		this.mOnclearListner = onclearListner;

	}

	public interface OnclearListner {

		public void onclear(int posistion);

	}

	public void setList(ArrayList<String> List) {
		mList.clear();
		this.mList = List;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public String getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.simple_list_item_fk, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.mClearIB.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mList.remove(position);
				mOnclearListner.onclear(position);
				notifyDataSetChanged();
			}
		});

		return convertView;
	}

	private class ViewHolder {

		private TextView mMainTV;
		private ImageButton mClearIB;

		private ViewHolder(View view) {

			mMainTV = (TextView) view.findViewById(R.id.simple_tv);
			mClearIB = (ImageButton) view.findViewById(R.id.clear_iv);
			FontLoader.setRobotoRegularTypeface(mMainTV);

		}
	}
}