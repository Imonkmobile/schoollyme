package com.schollyme.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.ImageLoaderUniversal;

public class AlbumDetailAdapter extends BaseAdapter {
	private static final String TAG = AlbumAdapter.class.getSimpleName();
	private LayoutInflater mLayoutInflater;
	// private List<Album> mAlbums;

	private List<AlbumMedia> mList;
	  private int mImgWidth = 0;
	private Context mContext;
	private boolean mPhotos;

	private boolean setSelectable = false;

	public AlbumDetailAdapter(Context context) {
		mContext = context;

		mLayoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		int[] screenDimen = new int[2];
		DisplayUtil.probeScreenSize(context, screenDimen);
		float spaceBwItems = context.getResources().getDimension(
				R.dimen.space_tinny4);
		mImgWidth = (int) ((screenDimen[0] - spaceBwItems) / 3f);
		
//		if (Config.DEBUG) {
//			Log.d(TAG, "screenDimen[0]=" + screenDimen[0] + ", screenDimen[1]=" + screenDimen[1] + ", spaceBwItems=" + spaceBwItems
//					+ ", mImgWidth=" + mImgWidth);
//		}
	}

	public void setList(List<AlbumMedia> list) {
		this.mList = list;
		notifyDataSetChanged();
	}

	public void setSelectable(boolean status) {
		setSelectable = status;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mList ? 0 : mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.album_detail_item,
					null);
			convertView.setLayoutParams(new AbsListView.LayoutParams(mImgWidth,
					mImgWidth));
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		AlbumMedia model=mList.get(position);
//		if (model.isSelected==0) {
//			viewHolder.selected_item_iv.setVisibility(View.VISIBLE);
//		} else {
//			viewHolder.selected_item_iv.setVisibility(View.GONE);
//		}

		return convertView;
	}

	private class ViewHolder {
		private ImageView detail_item_iv, selected_item_iv;

		private ViewHolder(View view) {
			detail_item_iv = (ImageView) view.findViewById(R.id.detail_item_iv);
			selected_item_iv = (ImageView) view
					.findViewById(R.id.selected_item_iv);

		}

	}
}
