package com.schollyme.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.NotificationModel;
import com.vinfotech.utility.CalStartEndUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

public class NotificationsAdapter extends BaseAdapter{
	
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<NotificationModel> mNotifications;
	private NotificationViewHolder mNotificationViewHolder;
	
	public NotificationsAdapter(Context context){
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(mContext);
	}
	
	public void setList(List<NotificationModel> mNotificationList){
		this.mNotifications = mNotificationList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null==mNotifications?0:mNotifications.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup parent) {
		mNotificationViewHolder = new NotificationViewHolder();
		if(contentView==null){
			contentView = mLayoutInflater.inflate(R.layout.notification_row_item_layout, parent,false);
			mNotificationViewHolder.mNotificationMessageTV = (TextView) contentView.findViewById(R.id.notification_message_tv);
			mNotificationViewHolder.mNotificationDateTimeTV = (TextView) contentView.findViewById(R.id.notification_date_tv);
			mNotificationViewHolder.mUserProfileIV = (ImageView) contentView.findViewById(R.id.profile_image_iv);
			contentView.setTag(mNotificationViewHolder);
			FontLoader.setRobotoRegularTypeface(mNotificationViewHolder.mNotificationMessageTV,mNotificationViewHolder.mNotificationDateTimeTV);
		}
		else{
			mNotificationViewHolder = (NotificationViewHolder) contentView.getTag();
		}

		Calendar calendar = getCalTime(mNotifications.get(position).mCreatedDate);
		if (null != calendar) {
			mNotificationViewHolder.mNotificationDateTimeTV.setText(CalStartEndUtil.getTimeInformatedform(mContext, calendar));
		} else {
			mNotificationViewHolder.mNotificationDateTimeTV.setText(mNotifications.get(position).mCreatedDate);
		}

	//	mNotificationViewHolder.mNotificationDateTimeTV.setText(Utility.getFormattedDate(mNotifications.get(position).mCreatedDate));
		/*StringBuilder mBuilder = new StringBuilder();
		mBuilder.append(mNotifications.get(position).mUserName);
		mBuilder.append(" ");
		mBuilder.append(mNotifications.get(position).mNotificationText);
		mNotificationViewHolder.mNotificationMessageTV.setText(mBuilder.toString());*/
		if(mNotifications.get(position).mNotificationTypeId==17){
			mNotificationViewHolder.mNotificationMessageTV.setText(Utility.clickableTextSpan(mContext, mNotifications.get(position).mNotificationText, null,null, mNotifications.get(position).mUserName));
		}
		else{
			mNotificationViewHolder.mNotificationMessageTV.setText(Utility.clickableTextSpan(mContext, mNotifications.get(position).mNotificationText, null, mNotifications.get(position).mUserName, null));
		}

		if(mNotifications.get(position).mNotificationTypeId==51 || mNotifications.get(position).mNotificationTypeId==57){
			mNotificationViewHolder.mNotificationMessageTV.setText(mNotifications.get(position).mNotificationText);
		}
		if(mNotifications.get(position).mNotificationTypeId==60){
			mNotificationViewHolder.mNotificationMessageTV.setText(Utility.clickableTextSpan(mContext, mNotifications.get(position).mNotificationText, null, null, null));
		}
		
		String imageUrl = Config.IMAGE_URL_PROFILE + mNotifications.get(position).mProfilePicture;
		ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mNotificationViewHolder.mUserProfileIV,ImageLoaderUniversal.option_Round_Image);
		return contentView;
	}
	
	private class NotificationViewHolder{
		private TextView mNotificationMessageTV,mNotificationDateTimeTV;
		private ImageView mUserProfileIV;
	}

	public Calendar getCalTime(String dateString) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			cal.setTime(sdf.parse(dateString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		TimeZone tz = cal.getTimeZone();
		int Offset = tz.getOffset(cal.getTimeInMillis());
		cal.setTimeInMillis(cal.getTimeInMillis() + Offset);
		return cal;
	}
}