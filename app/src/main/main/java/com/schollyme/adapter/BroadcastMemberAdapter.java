package com.schollyme.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.MessageMember;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class BroadcastMemberAdapter extends BaseAdapter {
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<MessageMember> mMessageMembers;
	private SelectionListener mSelectionListener;

	public BroadcastMemberAdapter(Context context, SelectionListener listener) {
		this.mContext = context;
		this.mSelectionListener = listener;
		mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(List<MessageMember> list) {
		this.mMessageMembers = list;
		notifyDataSetChanged();
	}

	private List<String> selections = new ArrayList<String>();
	public List<String> getSelections() {
		if (null != mMessageMembers) {
			for (MessageMember messageMember : mMessageMembers) {
				if (messageMember.selected) {
					selections.add(messageMember.UserGUID);
				}
			}
		}
		if (Config.DEBUG) {
			Log.d("BroadcastMemberAdapter", "getSelections count=" + selections.size());
		}
		return selections;
	}

	public void select(int position) {
		if (null != mMessageMembers) {
			for (int i = 0; i < mMessageMembers.size(); i++) {
				mMessageMembers.get(i).selected = (i == position || -2 == position);
			}
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mMessageMembers ? 0 : mMessageMembers.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(final int position, View contentView, ViewGroup parent) {
		ViewHolder viewHolder;

		if (contentView == null) {
			contentView = mLayoutInflater.inflate(R.layout.broadcast_member_row, parent, false);

			viewHolder = new ViewHolder(contentView);
			contentView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) contentView.getTag();
		}

		final MessageMember messageMember = mMessageMembers.get(position);

		String userType = messageMember.UserTypeID;
		if (userType.equals("1")) {
			viewHolder.mUserTypeTV.setText(String.valueOf(Config.UserType.Coach));
		} else if (userType.equals("2")) {
			viewHolder.mUserTypeTV.setText(String.valueOf(Config.UserType.Athlete));
		} else if (userType.equals("3")) {
			viewHolder.mUserTypeTV.setText(String.valueOf(Config.UserType.Fan));
		}

		viewHolder.mUserNameTV.setText(messageMember.FirstName + " " + messageMember.LastName);
		viewHolder.mOptionsIv.setVisibility(messageMember.selected ? View.VISIBLE : View.INVISIBLE);

		String imageUrl = Config.IMAGE_URL_PROFILE + messageMember.ProfilePicture;
		ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, viewHolder.mProfilePicIV, ImageLoaderUniversal.option_Round_Image);

		viewHolder.mUserNameTV.setTag(messageMember);
		viewHolder.mProfilePicIV.setTag(messageMember);
		viewHolder.mUserNameTV.setOnClickListener(mOnClickListener);
		viewHolder.mProfilePicIV.setOnClickListener(mOnClickListener);
		
		contentView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				messageMember.selected = !messageMember.selected;
				if(!messageMember.selected){
					selections.removeAll(Arrays.asList(new String[]{messageMember.UserGUID}));
				}
				notifyDataSetChanged();
				if (null != mSelectionListener) {
					mSelectionListener.onSelectionChange(getSelections());
				}
			}
		});

		return contentView;
	}
	
	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			MessageMember messageMember = (MessageMember) view.getTag();

			if (new LogedInUserModel(mContext).mUserGUID.equals(messageMember.UserGUID)) {
				mContext.startActivity(DashboardActivity.getIntent(mContext,6, ""));
			} else {
				mContext.startActivity(FriendProfileActivity.getIntent(mContext, messageMember.UserGUID, messageMember.ProfileURL, "BroadcastMemberAdapter"));
			}
		}
	};

	private class ViewHolder {
		private TextView mUserNameTV, mUserTypeTV;
		private ImageView mProfilePicIV, mOptionsIv;

		public ViewHolder(View view) {
			mUserNameTV = (TextView) view.findViewById(R.id.user_name_tv);
			mUserTypeTV = (TextView) view.findViewById(R.id.user_status_tv);
			mProfilePicIV = (ImageView) view.findViewById(R.id.profile_image_iv);
			mOptionsIv = (ImageView) view.findViewById(R.id.options_iv);

			FontLoader.setRobotoMediumTypeface(mUserNameTV);
			FontLoader.setRobotoRegularTypeface(mUserTypeTV);
		}
	}

	public interface SelectionListener {
		void onSelectionChange(List<String> selections);
	}
}