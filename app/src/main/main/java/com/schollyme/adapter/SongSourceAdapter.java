package com.schollyme.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.handler.DeleteLayout;
import com.schollyme.model.Song;
import com.schollyme.model.SongSource;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class SongSourceAdapter extends BaseAdapter {
	private static final String TAG = SongSourceAdapter.class.getSimpleName();

	private LayoutInflater mLayoutInflater;
	private List<SongSource> mSongSources;
	private Context mContext;
	private String mSongString, mSongsString;

	public enum ContentType {
		ProviderSelector, PlaylistSongs, AllSongs, Last7Day;
	}

	public SongSourceAdapter(Context context) {
		mContext = context;
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mSongString = context.getResources().getString(R.string.Song);
		mSongsString = context.getResources().getString(R.string.Songs);
	}

	public void setList(List<SongSource> list) {
		this.mSongSources = list;
		notifyDataSetChanged();
	}

	public int getSelectionCount() {
		int count = 0;
		if (null != mSongSources) {
			for (SongSource songSource : mSongSources) {
				if (songSource.selected) {
					count++;
				}
			}
		}
		return count;
	}

	public void selectSingle(int position) {
		if (null != mSongSources) {
			for (int i = 0; i < mSongSources.size(); i++) {
				mSongSources.get(i).selected = (i == position);
			}
		}
		notifyDataSetChanged();
	}

	public List<SongSource> getSelections() {
		List<SongSource> songSources = new ArrayList<SongSource>();
		if (null != mSongSources) {
			for (SongSource songSource : mSongSources) {
				if (songSource.selected) {
					songSources.add(songSource);
				}
			}
		}
		if (Config.DEBUG) {
			Log.d(TAG, "getSelections count=" + songSources.size());
		}
		return songSources;
	}

	@Override
	public int getCount() {
		return null == mSongSources ? 0 : mSongSources.size();
	}

	@Override
	public SongSource getItem(int position) {
		return mSongSources.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.song_source_row, null);

			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final SongSource songSource = getItem(position);

		viewHolder.songIv.setImageResource(songSource.ImageResouceId);
		viewHolder.rightIv.setVisibility(songSource.selected ? View.VISIBLE : View.GONE);
		viewHolder.titleTv.setText(songSource.SourceName);
		viewHolder.subTitleTv.setText(songSource.NoOfSongs + " " + (songSource.NoOfSongs == 1 ? mSongString : mSongsString));

		return convertView;
	}

	private class ViewHolder {
		private ImageView songIv, rightIv;
		private TextView titleTv, subTitleTv;

		private ViewHolder(View view) {
			songIv = (ImageView) view.findViewById(R.id.song_iv);
			rightIv = (ImageView) view.findViewById(R.id.right_iv);
			titleTv = (TextView) view.findViewById(R.id.title_tv);
			subTitleTv = (TextView) view.findViewById(R.id.sub_title_tv);

			FontLoader.setRobotoMediumTypeface(titleTv);
			FontLoader.setRobotoRegularTypeface(subTitleTv);
		}
	}
}
