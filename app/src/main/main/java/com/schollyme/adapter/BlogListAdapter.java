package com.schollyme.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.jsoup.Jsoup;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.model.BlogModel;
import com.schollyme.model.NewsFeed;
import com.vinfotech.utility.CalStartEndUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class BlogListAdapter extends BaseAdapter {
	private static final String TAG = BlogListAdapter.class.getSimpleName();

	private LayoutInflater mLayoutInflater;
	private List<BlogModel> mBlogList;
	private Context mContext;
	private OnItemClickBlogList mOnItemClickBlogList;

	public BlogListAdapter(Context context, OnItemClickBlogList onItemClickBlogList) {
		this.mContext = context;
		this.mOnItemClickBlogList = onItemClickBlogList;
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setList(List<BlogModel> list) {
		this.mBlogList = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mBlogList ? 0 : mBlogList.size();

		// return 20;
	}

	@Override
	public BlogModel getItem(int position) {
		return mBlogList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.blog_list_row, null);

			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final BlogModel blogModel = getItem(position);
		if (null != blogModel) {

			Calendar calendar = getCalTime(blogModel.CreatedDate);
			if (null != calendar) {
				viewHolder.mTimeTV.setText(CalStartEndUtil.getTimeInformatedform(mContext, calendar));
			} else {
				viewHolder.mTimeTV.setText(blogModel.CreatedDate);
			}
			viewHolder.mTitleTV.setText(blogModel.Title);
			viewHolder.mAutorTV.setText(mContext.getResources().getString(R.string.By) + " " + blogModel.Author);
			viewHolder.mLikesCountTV.setText(blogModel.NoOfLikes + " "
					+ (mContext.getResources().getString(1 == blogModel.NoOfLikes ? R.string.Like : R.string.Likes)));

			viewHolder.mCommentsCountTV.setText(blogModel.NoOfComments + " "
					+ (mContext.getResources().getString(1 == blogModel.NoOfComments ? R.string.Comment : R.string.Comments)));
			viewHolder.mCommentsCountTV.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mOnItemClickBlogList.onClickItems(R.id.comments_tv, position, blogModel);
				}
			});
			viewHolder.mLikesCountTV.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mOnItemClickBlogList.onClickItems(R.id.likes_tv, position, blogModel);
				}
			});

			viewHolder.mDescriptionTV.setText(Jsoup.parse(blogModel.Description).text());

			// viewHolder.mDescriptionTV.setText(Html.fromHtml(blogModel.Description).toString());
			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mOnItemClickBlogList.onClickItems(5555, position, blogModel);
				}
			});

		}
		return convertView;
	}

	Calendar cal = Calendar.getInstance();

	public Calendar getCalTime(String dateString) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			cal.setTime(sdf.parse(dateString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		TimeZone tz = cal.getTimeZone();
		int Offset = tz.getOffset(cal.getTimeInMillis());
		cal.setTimeInMillis(cal.getTimeInMillis() + Offset);
		return cal;
	}

	public interface OnItemClickBlogList {
		void onClickItems(int ID, int position, BlogModel newsFeed);

	}

	private class ViewHolder {

		private TextView mTimeTV, mTitleTV, mAutorTV, mDescriptionTV, mLikesCountTV, mCommentsCountTV;

		// private ImageView mCoverIV;
		private ViewHolder(View view) {

			mTimeTV = (TextView) view.findViewById(R.id.time_tv);
			mTitleTV = (TextView) view.findViewById(R.id.title_tv);
			mAutorTV = (TextView) view.findViewById(R.id.author_tv);
			mDescriptionTV = (TextView) view.findViewById(R.id.description_tv);
			mLikesCountTV = (TextView) view.findViewById(R.id.likes_tv);
			mCommentsCountTV = (TextView) view.findViewById(R.id.comments_tv);
			FontLoader.setRobotoMediumTypeface(mTitleTV);
			FontLoader.setRobotoRegularTypeface(mTimeTV, mAutorTV, mDescriptionTV, mLikesCountTV, mCommentsCountTV);
		}
	}
}
