package com.schollyme.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.model.FilterBy;
import com.schollyme.search.FilterScreenActivity;
import com.vinfotech.utility.FontLoader;

public class FilterByCategoryAdapter extends BaseAdapter {

	String[] list_string;

	Context mContext;
	LayoutInflater mInflater;
	int unreadCount = 0;
	private FilterBy mFilterBy;
	ArrayList<Boolean> isDefault = new ArrayList<Boolean>();

	public FilterByCategoryAdapter(Context c, String[] list_string_,
			FilterBy filterBy) {
		// TODO Auto-generated method stub
		mContext = c;
		this.mFilterBy = filterBy;
		list_string = list_string_;

	}

	public void NotifyDataChange() {

		notifyDataSetChanged();

	}

	public void setFilteragain(FilterBy filterBy) {
		this.mFilterBy = filterBy;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list_string.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		convertView = mInflater.inflate(R.layout.filter_by_category_row, null);
		View mDotView = (View) convertView.findViewById(R.id.filter_view);
		switch (position) {
		case 0:
			if (null != mFilterBy.getSportsModels()
					&& mFilterBy.getSportsModels().size() > 0) {
				mDotView.setVisibility(View.VISIBLE);
			} else {
				mDotView.setVisibility(View.INVISIBLE);
			}
			break;
		case 1:
			if (null != mFilterBy.getStateModels()
					&& mFilterBy.getStateModels().size() > 0) {
				mDotView.setVisibility(View.VISIBLE);
			} else {
				mDotView.setVisibility(View.INVISIBLE);
			}
			break;
		case 2:
			if (mFilterBy.getGender().equals("")) {
				mDotView.setVisibility(View.INVISIBLE);
			} else {
				mDotView.setVisibility(View.VISIBLE);
			}

			break;
		case 3:
			if (mFilterBy.getUserTypeId().equals("")) {
				mDotView.setVisibility(View.INVISIBLE);
			} else {
				mDotView.setVisibility(View.VISIBLE);
			}
			break;
		case 4:
			if (mFilterBy.getGPA().equals("")) {
				mDotView.setVisibility(View.INVISIBLE);
			} else {
				mDotView.setVisibility(View.VISIBLE);
			}
			break;
		case 5:
			if (mFilterBy.getSATScore().equals("")) {
				mDotView.setVisibility(View.INVISIBLE);
			} else {
				mDotView.setVisibility(View.VISIBLE);
			}
			break;
		case 6:
			if (mFilterBy.getACTScore().equals("")) {
				mDotView.setVisibility(View.INVISIBLE);
			} else {
				mDotView.setVisibility(View.VISIBLE);
			}

			break;

		default:
			break;
		}

		TextView categoty_tv = (TextView) convertView
				.findViewById(R.id.categoty_tv);

		categoty_tv.setText(list_string[position]);

		if (((FilterScreenActivity) mContext).isListSelected(position)) {

			convertView.setBackgroundColor(mContext.getResources().getColor(
					R.color.accept_bg_color));

			categoty_tv.setTextColor(mContext.getResources().getColor(
					R.color.white));
		} else {

			convertView.setBackgroundColor(0);

			categoty_tv.setTextColor(mContext.getResources().getColor(
					R.color.divider_color));
		}

		FontLoader.setRobotoMediumTypeface(categoty_tv);
		// TODO Auto-generated method stub
		return convertView;
	}
}
