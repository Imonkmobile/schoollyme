package com.schollyme.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.media.MediaGridActivity;
import com.schollyme.media.MediaGridFragment;
import com.schollyme.model.LikerModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.MessageMember;
import com.schollyme.model.NewsFeed;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class LikeListAdapter extends BaseAdapter {
	private static final String TAG = AlbumAdapter.class.getSimpleName();
	private LayoutInflater mLayoutInflater;
	private List<LikerModel> mAlbums;
	private Context mContext;

	private OnItemClickListenerLikeList mOnItemClickListenerLikeList;

	public LikeListAdapter(Context context, OnItemClickListenerLikeList _OnItemClickListenerLikeList) {
		mContext = context;
		this.mOnItemClickListenerLikeList = _OnItemClickListenerLikeList;

		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public interface OnItemClickListenerLikeList {
		void onClickItems(int IDS, int Postion, LikerModel newsFeed);

	}

	public void setList(List<LikerModel> list) {
		this.mAlbums = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mAlbums ? 0 : mAlbums.size();
	}

	@Override
	public LikerModel getItem(int position) {
		return mAlbums.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void removeValue(int position) {
		mAlbums.remove(position);
		notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.like_row, null);

			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final LikerModel mLikerModel = getItem(position);

		if (null != mLikerModel) {
			String imageUrl = Config.IMAGE_URL_PROFILE + mLikerModel.ProfilePicture;
			ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, viewHolder.mUserImg, ImageLoaderUniversal.option_Round_Image);
			viewHolder.mUserNameTv.setText(mLikerModel.FirstName + " " + mLikerModel.LastName);
			
			viewHolder.mUserImg.setTag(mLikerModel);
			viewHolder.mUserNameTv.setTag(mLikerModel);
			viewHolder.mUserImg.setOnClickListener(mOnClickListener);
			viewHolder.mUserNameTv.setOnClickListener(mOnClickListener);
			
			if (mLikerModel.IsLiked == 1) {
				viewHolder.mDescTv.setVisibility(View.VISIBLE);
			} else {
				viewHolder.mDescTv.setVisibility(View.GONE);
			}
			viewHolder.mDescTv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mOnItemClickListenerLikeList.onClickItems(R.id.desc_tv, position, mLikerModel);

				}
			});

			viewHolder.container_ll.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mOnItemClickListenerLikeList.onClickItems(R.id.container_ll, position, mLikerModel);

				}
			});

		}

		return convertView;
	}
	
	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			LikerModel likerModel = (LikerModel) view.getTag();

			if (new LogedInUserModel(mContext).mUserGUID.equals(likerModel.UserGUID)) {
				mContext.startActivity(DashboardActivity.getIntent(mContext,6, ""));
			} else {
				mContext.startActivity(FriendProfileActivity.getIntent(mContext, likerModel.UserGUID, likerModel.ProfileLink, "BroadcastMemberAdapter"));
			}
		}
	};

	private class ViewHolder {
		private ImageView mUserImg;
		private TextView mUserNameTv, mDescTv;
		private LinearLayout container_ll;

		private ViewHolder(View view) {
			mUserImg = (ImageView) view.findViewById(R.id.user_iv);
			mUserNameTv = (TextView) view.findViewById(R.id.title_tv);
			mDescTv = (TextView) view.findViewById(R.id.desc_tv);
			container_ll = (LinearLayout) view.findViewById(R.id.container_ll);
			FontLoader.setRobotoRegularTypeface(mUserNameTv, mDescTv);
		}

	}
}
