package com.schollyme.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.schollyme.R;
import com.schollyme.inbox.ChatHistoryActivity;
import com.schollyme.inbox.ForwardMessageActivity;
import com.schollyme.model.ChatModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class ChatHistoryAdapter extends BaseAdapter{

	private Context mContext;
	private List<ChatModel> mChatHistoryAL;
	private static final int TYPE_SENT_MESSAGE = 0;
	private static final int TYPE_RECEIVED_MESSAGE = 1;
	private String mUserGUID;
	private ChatViewHolder mChatViewHolder;	
	private LayoutInflater mLayoutInflater;
	private ListView mListView;
	private String mFriendGUID;

	public ChatHistoryAdapter(Context context,ListView lisView){
		this.mContext = context;
		mUserGUID = new LogedInUserModel(mContext).mUserGUID;
		mLayoutInflater = LayoutInflater.from(mContext);
		this.mListView = lisView;
	}

	public void setList(List<ChatModel> mChatList){
		this.mChatHistoryAL = new ArrayList<ChatModel>();
		this.mChatHistoryAL = mChatList;
		if(null!=mListView){
			if(mChatHistoryAL.size()==0){
				this.mListView.setVisibility(View.GONE);
			}
			else{
				this.mListView.setVisibility(View.VISIBLE);
			}
		}
		notifyDataSetChanged();
	}


	@Override
	public int getCount() {
		return mChatHistoryAL.size();
	}

	@Override
	public Object getItem(int position) {
		if (position < mChatHistoryAL.size())
			return mChatHistoryAL.get(position);
		else
			return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if(mChatHistoryAL.get(position).mUserGUID.equals(mUserGUID)){
			return TYPE_SENT_MESSAGE;
		}
		else{
			return TYPE_RECEIVED_MESSAGE;
		}
	}


	@Override
	public View getView(int position, View contentView, ViewGroup parent) {
		mChatViewHolder = new ChatViewHolder();
		int itemType = getItemViewType(position);
		if(contentView==null){

			if(itemType==TYPE_SENT_MESSAGE){
				contentView = mLayoutInflater.inflate(R.layout.chat_sent_message_view, parent,false);
				mChatViewHolder.mSentMessageTV = (TextView) contentView.findViewById(R.id.message_tv);
				mChatViewHolder.mSentTimeTV = (TextView) contentView.findViewById(R.id.time_tv);
				mChatViewHolder.mSendMessageLL = (LinearLayout) contentView.findViewById(R.id.chat_ll);
				FontLoader.setRobotoRegularTypeface(mChatViewHolder.mSentMessageTV,mChatViewHolder.mSentTimeTV);
				contentView.setTag(mChatViewHolder);
			}
			else if(itemType==TYPE_RECEIVED_MESSAGE){
				contentView = mLayoutInflater.inflate(R.layout.chat_received_message_view, parent,false);
				mChatViewHolder.mReceivedMessageTV = (TextView) contentView.findViewById(R.id.message_tv);
				mChatViewHolder.mReceivTimeTV = (TextView) contentView.findViewById(R.id.time_tv);
				mChatViewHolder.mReceivedMessageLL = (LinearLayout) contentView.findViewById(R.id.chat_ll);
				FontLoader.setRobotoRegularTypeface(mChatViewHolder.mReceivedMessageTV,mChatViewHolder.mReceivTimeTV);
				contentView.setTag(mChatViewHolder);
			}
		}
		else{
			mChatViewHolder = (ChatViewHolder) contentView.getTag();
		}
		if(mChatHistoryAL.get(position).mUserGUID.equals(mUserGUID)){
			mChatViewHolder.mSentMessageTV.setText(Html.fromHtml(mChatHistoryAL.get(position).mMessage));
			mChatViewHolder.mSentTimeTV.setText(Utility.getTimeFromDate(mChatHistoryAL.get(position).mCreatedDate));
			mChatViewHolder.mSendMessageLL.setTag(position);
			mChatViewHolder.mSendMessageLL.setOnLongClickListener(TextClipBoardOptionistener);
		}
		else{
			mFriendGUID = mChatHistoryAL.get(position).mUserGUID;
			mChatViewHolder.mReceivedMessageTV.setText(Html.fromHtml(mChatHistoryAL.get(position).mMessage));
			mChatViewHolder.mReceivTimeTV.setText(Utility.getTimeFromDate(mChatHistoryAL.get(position).mCreatedDate));
			mChatViewHolder.mReceivedMessageLL.setTag(position);
			mChatViewHolder.mReceivedMessageLL.setOnLongClickListener(TextClipBoardOptionistener);
		}
		return contentView;
	}


	OnLongClickListener TextClipBoardOptionistener = new OnLongClickListener() {

		@Override
		public boolean onLongClick(final View v) {
		//	final TextView selectedTV = (TextView) v;
			final int pos = (Integer) v.getTag();
			final String message = mChatHistoryAL.get(pos).mMessage;
			DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
				@Override
				public void onItemClick(int position, String item) {
					if(position==0){
						mContext.startActivity(ForwardMessageActivity.getIntent(mContext,message));
						((Activity) mContext).overridePendingTransition(R.anim.slide_up_dialog, 0);
					}
					else if(position==1){
						Utility.copyToClipBoard(message,mContext);
						Toast.makeText(mContext, "Copied to clipboard", Toast.LENGTH_SHORT).show();
					}
					else if(position==2){
						updateMessageStatus(mChatHistoryAL.get(pos).mMessageGUID,mFriendGUID,pos);
					}
				}

				@Override
				public void onCancel() {

				}
			}, mContext.getString(R.string.forward_message), mContext.getString(R.string.copy_message), mContext.getString(R.string.delete_message));
			return false;
		}
	};

	

	private void updateMessageStatus(final String MessageGUID,final String MessageReceiverGUID,final int pos){
		DialogUtil.showOkDialogButtonLisnter(mContext, mContext.getResources().getString(R.string.delete_confirmation),mContext.getResources().getString(R.string.delete_dialog_header) , new OnOkButtonListner() {
			@Override
			public void onOkBUtton() {
				((ChatHistoryActivity)mContext).updateMessageStatus(MessageGUID,MessageReceiverGUID,false,pos);
			}
		});
	}

	public class ChatViewHolder{
		public TextView mSentMessageTV,mReceivedMessageTV,mSentTimeTV,mReceivTimeTV;
		public LinearLayout mSendMessageLL,mReceivedMessageLL;
	}
}