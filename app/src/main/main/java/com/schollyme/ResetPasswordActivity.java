package com.schollyme;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.vinfotech.request.ResetPasswordRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;

public class ResetPasswordActivity extends BaseActivity implements OnClickListener {

	private EditText mTokenET, mNewPasswordET, mCPasswordET;
	private String mTokenString, mPasswordString;
	private ErrorLayout mErrorLayout;

	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reset_password_activity);
		mContext = this;
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
				getResources().getString(R.string.reset_password_header), new OnClickListener() {
					@Override
					public void onClick(View v) {
						finish();
					}
				}, ResetPasswordListener);
		controlInitialization();
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
		mCPasswordET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					if (validateFields()) {
						ResetPasswordRequest();
					}
					return true;
				}
				return false;
			}
		});
	}

	OnClickListener ResetPasswordListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (validateFields()) {
				ResetPasswordRequest();
			}
		}
	};

	private void controlInitialization() {
		mTokenET = (EditText) findViewById(R.id.token_et);
		mNewPasswordET = (EditText) findViewById(R.id.new_password_et);
		mCPasswordET = (EditText) findViewById(R.id.c_password_et);
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, ResetPasswordActivity.class);
		return intent;
	}

	private boolean validateFields() {
		mTokenString = mTokenET.getText().toString().trim();
		mPasswordString = mNewPasswordET.getText().toString().trim();
		String cPassword = mCPasswordET.getText().toString().trim();
		if (TextUtils.isEmpty(mTokenString)) {
			mErrorLayout.showError(getResources().getString(R.string.enter_token), true, MsgType.Error);
		} else if (TextUtils.isEmpty(mPasswordString)) {
			mErrorLayout.showError(getResources().getString(R.string.password_require_message), true, MsgType.Error);
		} else if (mPasswordString.length() < 5) {
			mErrorLayout.showError(getResources().getString(R.string.password_length_message), true, MsgType.Error);
		} else if (TextUtils.isEmpty(cPassword)) {
			mErrorLayout.showError(getResources().getString(R.string.cpassword_require_message), true, MsgType.Error);
		} else if (!mPasswordString.equals(cPassword)) {
			mErrorLayout.showError(getResources().getString(R.string.password_not_match_message), true, MsgType.Error);
		} else {
			return true;
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		}
	}

	private void ResetPasswordRequest() {
		ResetPasswordRequest mForgotPasswordRequest = new ResetPasswordRequest(mContext);
		mForgotPasswordRequest.ResetPasswordServerRequest(mPasswordString, mTokenString);
		mForgotPasswordRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {

					DialogUtil.showOkCancelDialogButtonLisnter(mContext, getResources().getString(R.string.password_reset_success), getResources()
							.getString(R.string.app_name), new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

							Intent intent = LoginActivity.getIntent(mContext);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
							finish();

						}
					},new OnCancelListener() {
						
						@Override
						public void onCancel(DialogInterface dialog) {
							Intent intent = LoginActivity.getIntent(mContext);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
									| Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
							finish();
						}
					});

				} else {
					mErrorLayout.showError(data.toString(), true, MsgType.Error);
				}
			}
		});
	}
}