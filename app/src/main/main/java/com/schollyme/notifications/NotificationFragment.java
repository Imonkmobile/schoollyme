package com.schollyme.notifications;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.activity.TranscriptsRequestFromCoachActivity;
import com.schollyme.adapter.NotificationsAdapter;
import com.schollyme.evaluation.EvaluationDetailActivity;
import com.schollyme.evaluation.WriteEvaluationActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.friends.FriendsActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NotificationModel;
import com.schollyme.scores.MyScoresActivity;
import com.schollyme.team.TeamPageActivity;
import com.schollyme.wall.MediaViewerWallActivity;
import com.vinfotech.request.NotificationChangeReadStatusRequest;
import com.vinfotech.request.NotificationChangeSeenStatusRequest;
import com.vinfotech.request.NotificationListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;

public class NotificationFragment extends BaseFragment implements OnClickListener,OnRefreshListener {

	private TextView mNoRecordMessageTV;
	private ListView mNotificationLV;
	private Context mContext;
	private int pageIndex = 1;
	private List<NotificationModel> mNotifications;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private NotificationsAdapter mAdapter;
	private boolean loadingFlag = false;
	private ErrorLayout mErrorLayout;

	public static BaseFragment getInstance(Context context) {
		NotificationFragment mFragment = new NotificationFragment();
		return mFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.notification_fragment, container, false);
		mNotificationLV = (ListView) view.findViewById(R.id.notification_lv);
		mNoRecordMessageTV = (TextView) view.findViewById(R.id.no_record_message_tv);
		mContext = getActivity();
		mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_rl));
		BaseFragmentActivity.setHeader(R.drawable.icon_menu,0, getResources().getString(R.string.notifications),
				new OnClickListener() {

			@Override
			public void onClick(View v) {
				((DashboardActivity)v.getContext()).sliderListener();
				//	getNotificationsRequest();	

			}
		}, null);
		DashboardActivity.hideShowActionBar(true);
		mNotifications = new ArrayList<NotificationModel>();

		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		getNotificationsRequest();
		return view;
	}

	@Override
	public void onClick(View arg0) {

	}

	private void displayNotificationsAdapter(){
		if(mNotifications.size()>0){
			mNotificationLV.setVisibility(View.VISIBLE);
			mNoRecordMessageTV.setVisibility(View.GONE);
			mAdapter = new NotificationsAdapter(mContext);
			mAdapter.setList(mNotifications);
			mNotificationLV.setAdapter(mAdapter);
			markNotificationSeenRequest();
			mNotificationLV.setOnScrollListener(new OnScrollListener() {
				boolean bReachedListEnd = false;
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
						Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
						if (null != mAdapter&& !loadingFlag) {
							pageIndex++;
							loadingFlag = true;
							getNotificationsRequest();
						}
					}
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
					bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount/2));
				}
			});

			mNotificationLV.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					markNotificationReadRequest(mNotifications.get(position).mNotificationGUID);
					int typeId = mNotifications.get(position).mNotificationTypeId;
					switch (typeId) {

					case 1:
						//added new post in team page

						String mActvityGUID = mNotifications.get(position).mProfileURL;
						String mTeamGUID = mNotifications.get(position).mEntityGUID;
						((DashboardActivity)mContext).switchFragment(4,mActvityGUID,mTeamGUID,typeId);
						//	startActivity(TeamPageActivity.getIntent(mContext, mTeamGUID,mActvityGUID, 0));
						break;
					case 2:
						//Commented on wall post
					case 3:
						//Like your wall Post
						String nTeamGUID = mNotifications.get(position).mEntityGUID;
						if("PHOTO_ALBUM".equals(mNotifications.get(position).mRefer) || "VIDEO_ALBUM".equals(mNotifications.get(position).mRefer)){
							Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mediagrid://app.postfeed.wall/params?UserGUID=" +new LogedInUserModel(mContext).mUserGUID + "&FromQuery=FromQuery&AlbumGUID="
									+ mNotifications.get(position).mProfileURL));
							startActivity(browserIntent);
						}else if("WALL".equals(mNotifications.get(position).mRefer)){
							((DashboardActivity)mContext).switchFragment(4,mNotifications.get(position).mProfileURL,nTeamGUID,typeId);
						}else if("GROUP".equals(mNotifications.get(position).mRefer)){
							String nActvityGUID = mNotifications.get(position).mProfileURL;
							((DashboardActivity)mContext).switchFragment(4,nActvityGUID,nTeamGUID,1);
							//	startActivity(TeamPageActivity.getIntent(mContext,nTeamGUID,nActvityGUID, 0));
						}


						break;
					case 4:
						//Member added to group
						break;
					case 16:
						//Friend Request Received
						//		markNotificationReadRequest(mNotifications.get(position).mNotificationGUID);
						startActivity(FriendsActivity.getIntent(mContext, 0, ""));
						break;
					case 17:
						//Friend Request Accepted
						//	markNotificationReadRequest(mNotifications.get(position).mNotificationGUID);
						startActivity(FriendProfileActivity.getIntent(mContext, "0", mNotifications.get(position).mProfileURL, "NotificationFragment"));
						break;

					case 18:
						//Tag in Post
						break;

					case 19:
						//Post on Wall
						String sTeamGUID = mNotifications.get(position).mEntityGUID;
						((DashboardActivity)mContext).switchFragment(4,mNotifications.get(position).mProfileURL,sTeamGUID,typeId);
						break;
					case 20:
						//Like On Comment
						break;
					case 21:
						//Tag On Comment
						break;
					case 22:
						//Group Request Received
						break;
					case 23:
						//Group Request Accepted
						break;
					case 24:
						//Group Joined
						break;	
					case 25:
						//Group Blocked
						break;
					case 26:
						//Event Edit notification to host
						break;
					case 27:
						//Event Edit notification to admin
						break;
					case 28:
						//Event Edit notification to attending user
						break;
					case 29:
						//Event Edit notification to user, may attend
						break;

					case 43://Left Team Page
						startActivity(TeamPageActivity.getIntent(mContext, mNotifications.get(position).mEntityGUID,"", 0));
						break;
					case 48:
						//Likes your Photo
						startActivity(MediaViewerWallActivity.getIntent(mContext, mNotifications.get(position).mProfileURL,mNotifications.get(position).mEntityGUID));


						break;

					case 50:
						//commented on your Photo
						startActivity(MediaViewerWallActivity.getIntent(mContext, mNotifications.get(position).mProfileURL,mNotifications.get(position).mEntityGUID));
						break;

						case 56:
							startActivity(FriendProfileActivity.getIntent(mContext, "0", mNotifications.get(position).mProfileURL, "NotificationFragment"));
							break;
						case 51:
						case 57:
						case 58:
						startActivity(MyScoresActivity.getIntent(mContext, new LogedInUserModel(mContext).mUserGUID));
						break;
					case 59:
					case 60:
						//User Marked as Evaluator from admin
						((DashboardActivity)mContext).switchFragment(7,mNotifications.get(position).mProfileURL,"",typeId);
						//		LogedInUserModel.updatePreferenceString(mContext, "isPaidCoach", "1");
						//		LogedInUserModel.updatePreferenceString(mContext, "isEvaluator", "0");

						break;
					case 54:
						startActivity(TranscriptsRequestFromCoachActivity.getIntent(mContext,""));
						break;


					case 64:
						//Evaluation request to coach
						LogedInUserModel model = new LogedInUserModel(mContext);
						if(model.mUserSportsId.equals(mNotifications.get(position).mSportsID)){
							String mTitle = mNotifications.get(position).mUserName+"'s "+getString(R.string.Evaluation); 
							startActivity(WriteEvaluationActivity.getIntent(mContext, mNotifications.get(position).mProfileURL, model.mUserSportsId, model.mUserSportsName,mNotifications.get(position).mEntityGUID,"",mTitle));
						}
						else{
							DialogUtil.showOkDialog(mContext, getResources().getString(R.string.changed_sports), "");
						}
						break;

					case 65:
						//Coach Write Evaluation for athlete request 
						startActivity(EvaluationDetailActivity.getIntent(mContext, mNotifications.get(position).mProfileURL,getString(R.string.My_Evaluation), null, mNotifications.get(position).mEntityGUID,""));
						break;

					case 66:
						//User Marked as PaidCoach from admin
						((DashboardActivity)mContext).switchFragment(7,mNotifications.get(position).mProfileURL,"",typeId);
						//	LogedInUserModel.updatePreferenceString(mContext, "isEvaluator", "1");
						//	LogedInUserModel.updatePreferenceString(mContext, "isPaidCoach", "0");

						break;

					case 67:
						if("PHOTO_ALBUM".equals(mNotifications.get(position).mRefer) || "VIDEO_ALBUM".equals(mNotifications.get(position).mRefer)){
							Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mediagrid://app.postfeed.wall/params?UserGUID=" +new LogedInUserModel(mContext).mUserGUID + "&FromQuery=FromQuery&AlbumGUID="
									+ mNotifications.get(position).mProfileURL));
							startActivity(browserIntent);
						}
						else if("WALL".equals(mNotifications.get(position).mRefer)){
							((DashboardActivity)mContext).switchFragment(4,mNotifications.get(position).mProfileURL,mNotifications.get(position).mEntityGUID,typeId);
						}
						else if("VIDEO".equals(mNotifications.get(position).mRefer) || "PHOTO".equals(mNotifications.get(position).mRefer)){
							startActivity(MediaViewerWallActivity.getIntent(mContext, mNotifications.get(position).mProfileURL,mNotifications.get(position).mEntityGUID));
						}

					default:
						break;
					}
				}
			});
		}
		else{
			mAdapter = new NotificationsAdapter(mContext);
			mAdapter.setList(mNotifications);
			mNotificationLV.setAdapter(mAdapter);
			mNoRecordMessageTV.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * This api will retrive all inapp notification list
	 * */
	private void getNotificationsRequest(){
		NotificationListRequest mRequest =  new NotificationListRequest(mContext);
		
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mSwipeRefreshWidget.setRefreshing(false);
				if(success){
					mNotifications.addAll((Collection<? extends NotificationModel>) data);
					if(pageIndex==1){
						displayNotificationsAdapter();	
					}
					else{
						mAdapter.notifyDataSetChanged();
					}


				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.NotificationListServerRequest(pageIndex);
	}

	/**
	 * This api will mark notification as seen
	 * */
	private void markNotificationSeenRequest(){
		NotificationChangeSeenStatusRequest mRequest = new NotificationChangeSeenStatusRequest(mContext);
		mRequest.getNotificationSeenStatusServerRequest();
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				Log.i("Notification Mark Seen Status", "Users All Notification are marked as seen ::"+data.toString());
				//Do nothing
			}
		});
	}

	/**
	 * This api will mark notification as read
	 * */
	private void markNotificationReadRequest(String mNotificationGUID){
		NotificationChangeReadStatusRequest mRequest = new NotificationChangeReadStatusRequest(mContext);
		mRequest.getNotificationReadStatusServerRequest(mNotificationGUID);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				Log.i("Notification Mark read Status", "Users clicked Notification marked as read ::"+data.toString());
				//Do nothing
			}
		});
	}

	@Override
	public void onRefresh() {
		pageIndex = 1;
		mNotifications = new ArrayList<NotificationModel>();
		getNotificationsRequest();
	}
}