package com.schollyme.songofday;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.Song;
import com.vinfotech.request.SongOfTheDayRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.widget.PagerSlidingTabStrip;

public class SongOfDayFragment extends BaseFragment {
	private static final String TAG = SongOfDayFragment.class.getSimpleName();
	private static final int PAGE_COUNT = 2;

	private PagerSlidingTabStrip mPagerSlidingTabStrip;
	private ViewPager mGenericVp;
	private ListPagerAdapter mListPagerAdapter;
	private ErrorLayout mErrorLayout;

	public static SongOfDayFragment newInstance() {
		SongOfDayFragment songOfDayFragment = new SongOfDayFragment();
		return songOfDayFragment;
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.song_of_day_fragment, null);

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		initHeader();

		mListPagerAdapter = new ListPagerAdapter(getChildFragmentManager());
		mErrorLayout = new ErrorLayout(view);

		mPagerSlidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs_psts);
		mGenericVp = (ViewPager) view.findViewById(R.id.generic_vp);
		mGenericVp.setOffscreenPageLimit(PAGE_COUNT);
		mGenericVp.setAdapter(mListPagerAdapter);
		mPagerSlidingTabStrip.setViewPager(mGenericVp);

		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onStop() {
		SchollyMeApplication schollyMeApplication = (SchollyMeApplication) getActivity().getApplication();
		schollyMeApplication.pauseSong();
		super.onStop();
	}

	public class ListPagerAdapter extends FragmentPagerAdapter {
		private SODPlaylistFragment mSODPlaylistFragment;
		private SODLast7DayFragment mSODLast7DayFragment;

		public ListPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return (getString(position == 0 ? R.string.Playlist : R.string.Last_7_Days));

		}

		@Override
		public int getCount() {
			return PAGE_COUNT;
		}

		@Override
		public Fragment getItem(int position) {
			if (position == 0) {
				mSODPlaylistFragment = SODPlaylistFragment.newInstance(mErrorLayout);
				return mSODPlaylistFragment;
			} else {
				mSODLast7DayFragment = SODLast7DayFragment.newInstance(mErrorLayout);
				return mSODLast7DayFragment;
			}
		}

	}

	private void initHeader(){
		BaseFragmentActivity.setHeader(R.drawable.icon_menu, 0, getResources().getString(R.string.Song_of_the_Day), new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				((DashboardActivity) getActivity()).sliderListener();
			}
		}, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				fetchSongIfNeeded(v);
			}
		});
		DashboardActivity.hideShowActionBar(true);
	}

	private Song mSODSong = null;

	public void fetchSongIfNeeded(final View view) {
		if (null == mSODSong) {
			BaseFragmentActivity.showLoader(true);
			SongOfTheDayRequest songOfTheDayRequest = new SongOfTheDayRequest(view.getContext());
			songOfTheDayRequest.setRequestListener(new BaseRequest.RequestListener() {

				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					if (success) {
						if (null != data && data instanceof Song) {
							mSODSong = (Song) data;
							handleSongPlay(view, mSODSong);
						}
					} else {
						BaseFragmentActivity.showLoader(false);
						mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true,
								ErrorLayout.MsgType.Error);
					}
				}
			});
			songOfTheDayRequest.getSongOfDayFromServer("");
		} else if(null != getActivity()){
			handleSongPlay(view, mSODSong);
		}
	}

	private void handleSongPlay(final View v, Song song) {
		final String songPlayUrl = TextUtils.isEmpty(song.SongURL) ? song.SongPreviewURL : song.SongURL;
		if (TextUtils.isEmpty(songPlayUrl) || !android.util.Patterns.WEB_URL.matcher(songPlayUrl).matches()) {
			BaseFragmentActivity.showLoader(false);
			mErrorLayout.showError(v.getResources().getString(R.string.No_Song_Found), true, ErrorLayout.MsgType.Error);
			return;
		}

		SchollyMeApplication schollyMeApplication = (SchollyMeApplication) getActivity().getApplication();
		if (schollyMeApplication.isPlaying()) {
			BaseFragmentActivity.showLoader(false);
			schollyMeApplication.pauseSong();
		} else if (schollyMeApplication.playSong(song.Title, song.SongURL, new SchollyMeApplication.SongLoadListener() {

			@Override
			public void onLoad(boolean success, String url, MediaPlayer mediaPlayer) {
				BaseFragmentActivity.showLoader(false);
				if (!success) {
					mErrorLayout.showError(v.getResources().getString(R.string.This_song_can_not), true, ErrorLayout.MsgType.Error);
				}
			}
		})) {
			//v.setEnabled(false);
		} else if (null != mErrorLayout && schollyMeApplication.isBuffering()) {
			BaseFragmentActivity.showLoader(false);
			mErrorLayout.showError(v.getResources().getString(R.string.Buffering), true, ErrorLayout.MsgType.Error);
		} else if (null != mErrorLayout) {
			BaseFragmentActivity.showLoader(false);
			mErrorLayout.showError(v.getResources().getString(R.string.Invalid_song_URL), true, ErrorLayout.MsgType.Error);
		}
	}
}
