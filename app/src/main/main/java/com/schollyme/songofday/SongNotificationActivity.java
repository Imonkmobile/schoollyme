package com.schollyme.songofday;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.schollyme.SchollyMeApplication;

public class SongNotificationActivity extends Activity {
    private static final String TAG = SongNotificationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int status = getIntent().getIntExtra("Status", 2);
        Log.d(TAG, "onCreate status=" + status);

        SchollyMeApplication sma = (SchollyMeApplication) getApplication();
        if (0 == status) {
            sma.pauseSongWithoutNotification();
        }else if(status < 0){
            sma.cancelNotifications(true);
        }
        finish();
    }
}
