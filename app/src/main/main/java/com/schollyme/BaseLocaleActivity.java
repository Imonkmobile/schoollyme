package com.schollyme;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.model.LogedInUserModel;
import com.splunk.mint.Mint;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Base activity class. This may be useful in
 * Implementing google analytics or
 * Any app wise implementation
 * 
 * @author Ravi Bhandari
 * 
 */
public abstract class BaseLocaleActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		registerReceiver(mLocaleUpdateReceiver,new IntentFilter("LocaleUpdate"));
	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(mLocaleUpdateReceiver);
		super.onDestroy();

	}

	public abstract void onLocaleUpdate();

	BroadcastReceiver mLocaleUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			onLocaleUpdate();
		}
	};


	public static Intent getLocaleUpdateIntent(){
		return new Intent("LocaleUpdate");
	}
}