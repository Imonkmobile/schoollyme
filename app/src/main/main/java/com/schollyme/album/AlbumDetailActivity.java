package com.schollyme.album;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

import com.schollyme.BaseActivity;
import com.schollyme.R;
import com.schollyme.adapter.AlbumDetailAdapter;
import com.schollyme.model.AlbumMedia;
import com.vinfotech.header.HeaderLayout;

public class AlbumDetailActivity extends BaseActivity implements
		OnClickListener {

	Context mContext;
	AlbumDetailAdapter mAlbumDetailAdapter;
	private VHolder mVHolder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.album_detail_activity);
		mContext = this;

		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back,
				R.drawable.icon_menu, "DYNAMIC ", this, this);

		controlInitialization();
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, AlbumDetailActivity.class);
		return intent;
	}

	private void controlInitialization() {
		mAlbumDetailAdapter = new AlbumDetailAdapter(AlbumDetailActivity.this);
		mVHolder = new VHolder(findViewById(R.id.main_container_rl), this);
		ArrayList<AlbumMedia> list = new ArrayList<AlbumMedia>();

		for (int i = 0; i < 15; i++) {

			JSONObject mJson = new JSONObject();

			// this.MediaGUID = jsonObject.optString("MediaGUID", "");
			// this.ImageName = jsonObject.optString("ImageName", "");
			// this.Caption = jsonObject.optString("Caption", "");
			// this.NoOfComments = jsonObject.optInt("NoOfComments", 0);
			// this.NoOfLikes = jsonObject.optInt("NoOfLikes", 0);
			// this.CreatedDate = jsonObject.optString("CreatedDate", "");
			// this.IsCoverMedia = jsonObject.optInt("IsCoverMedia", 0);
			// this.MediaType = jsonObject.optString("MediaType",
			// MEDIA_TYPE_UNKNOWN);
			//
			// this.isSelected = "False";

			try {
				
				mJson.put("MediaGUID", "20");
				mJson.put("ImageName", "abc.jpg");
				mJson.put("Caption", "TExt");
				mJson.put("NoOfComments", 20);
				mJson.put("NoOfLikes", 5);
				mJson.put("CreatedDate", "20 20 20");
				mJson.put("IsCoverMedia", 0);
				mJson.put("MediaType", "");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			AlbumMedia mAlbumMedia = new AlbumMedia(mJson);

			list.add(mAlbumMedia);

		}
		mAlbumDetailAdapter.setList(list);
	}

	class VHolder {
		private HeaderGridView mHGridViewMain;
		HeaderLayout headerLayout;

		public VHolder(View view, OnClickListener listener) {
			mHGridViewMain = (HeaderGridView) view
					.findViewById(R.id.generic_hgv);
			mHGridViewMain.setAdapter(mAlbumDetailAdapter);
			headerLayout = new HeaderLayout(
					view.findViewById(R.id.header_layout));
			headerLayout.setHeaderValues(R.drawable.icon_back, "Dynamic Name",
					R.drawable.icon_menu);
			headerLayout.setListenerItI(AlbumDetailActivity.this,
					AlbumDetailActivity.this);

			mHGridViewMain.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

				}
			});
			mHGridViewMain
					.setOnItemLongClickListener(new OnItemLongClickListener() {

						@Override
						public boolean onItemLongClick(AdapterView<?> parent,
								View view, int position, long id) {
							mAlbumDetailAdapter.setSelectable(true);
							return true;
						}
					});

		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_button:
			this.finish();
			break;
		default:
			break;
		}
	}

}
