package com.schollyme;

import org.json.JSONObject;

public class NotificationCountModel {

	public int mTotalNotificationRecords;
	public int mTotalMessageRecords;
	public int mTotalPendingFriendRecords;

	public NotificationCountModel(JSONObject mJsonObj){
		try {
			this.mTotalNotificationRecords = mJsonObj.optInt("TotalNotificationRecords",0);
			this.mTotalMessageRecords = mJsonObj.optInt("TotalMessageRecords",0);
			this.mTotalPendingFriendRecords = mJsonObj.optInt("TotalPendingFriendRecords",0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static NotificationCountModel getNotificationCount(String mJson){
		try {
			JSONObject mJoson = new JSONObject(mJson);
			return	new NotificationCountModel(mJoson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toString() {
		return "NotificationCountModel [mTotalNotificationRecords="
				+ mTotalNotificationRecords + ", mTotalMessageRecords="
				+ mTotalMessageRecords + ", mTotalPendingFriendRecords="
				+ mTotalPendingFriendRecords + "]";
	}
}