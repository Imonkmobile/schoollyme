package com.schollyme.commentslike;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.adapter.CommentsListAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.RefreshLayout;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.CommentModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.CommentListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

public class CommentListActivity extends BaseActivity implements OnClickListener {
	public static final int REQ_CODE_COMMENT_LIST = 656;

	private VHolder mVHolder;
	private AlbumMedia mAlbumMedia;
	private CommentListRequest mCommentListRequest;
	private CommentsListAdapter mCommentsListAdapter;
	public static String FOR_ALBUM = "ALBUM";

	public static String FOR_BLOG = "BLOG";
	public static String FOR_MEDIA = "MEDIA";
	public static String FOR_WALLPOST = "WALLPOST";
	private SwipeRefreshLayout mPullToRefreshSrl;

	private RelativeLayout mEmptyRl;
	Album mAlbumDetail;
	private RefreshLayout mRefreshLayout;
	String Caption;
	private ArrayList<CommentModel> mList = new ArrayList<CommentModel>();
	int pageNumbermain = 1;
	private String ActionFor = "";
	private String mBlogGUID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comments_activity);
		initialization();
		getAndSetData();
		mList.clear();
		servicCall(pageNumbermain, true);
	}

	private void servicCall(int pageNumber, boolean showLoader) {
		isLoading = false;
		if (mPullToRefreshSrl.isRefreshing()) {
			mCommentListRequest.setLoader(null);
		} else {
			mCommentListRequest.setLoader(mVHolder.mLoadingPB);
		}

		mCommentListRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mPullToRefreshSrl.setRefreshing(false);
				if (success) {
					List<CommentModel> list = (List<CommentModel>) data;
					mList.addAll(list);
					mCommentsListAdapter.setList(mList);
					if (mCommentsListAdapter.getCount() < 1) {
						mRefreshLayout.show(mEmptyRl, null, R.drawable.icon_no_commnets, R.string.Be_the_first, 0, 0);
					} else {
						mRefreshLayout.hide(mEmptyRl);
					}
					if (totalRecords == 0) {
						isLoading = false;
					} else if (mList.size() < totalRecords) {
						isLoading = true;
						pageNumbermain++;
					} else {
						isLoading = false;
					}
					if (ActionFor.equals(FOR_MEDIA)) {
						mAlbumMedia.NoOfComments = totalRecords;
						Intent in = new Intent();
						in.putExtra("COUNT", totalRecords);
						in.putExtra("AlbumMedia", mAlbumMedia);
						in.putExtra("Album", mAlbumDetail);
						setResult(RESULT_OK, in);

					} else if (ActionFor.equals(FOR_ALBUM)) {
						mAlbumDetail.NoOfComments = totalRecords;
						Intent in = new Intent();
						in.putExtra("COUNT", totalRecords);
						in.putExtra("AlbumMedia", mAlbumMedia);
						in.putExtra("Album", mAlbumDetail);
						setResult(RESULT_OK, in);
					} else if (ActionFor.equals(FOR_BLOG)) {

						Intent in = new Intent();
						in.putExtra("COMMENT_COUNT_BLOG", totalRecords);
						in.putExtra("BLOG_GUID", mBlogGUID);
						setResult(RESULT_OK, in);
					}

				} else {
					isLoading = false;
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
				}

			}
		});
		if (ActionFor.equals(FOR_ALBUM)) {
			mCommentListRequest.getCommentListInServer(mAlbumDetail.ActivityGUID, CommentListRequest.ENTITYTYPE_ACTIVITY, pageNumber);
		} else if (ActionFor.equals("MEDIA")) {
			mCommentListRequest.getCommentListInServer(mAlbumMedia.MediaGUID, CommentListRequest.ENTITYTYPE_MEDIA, pageNumber);
		} else if (ActionFor.equals("WALLPOST")) {
			mCommentListRequest.getCommentListInServer(mAlbumMedia.MediaGUID, CommentListRequest.ENTITYTYPE_ACTIVITY, pageNumber);
		} else if (ActionFor.equals("BLOG")) {
			mCommentListRequest.getCommentListInServer(mBlogGUID, CommentListRequest.ENTITYTYPE_BLOG, pageNumber);
		}
	}

	private ErrorLayout mErrorLayout;

	private void initialization() {

		mCommentListRequest = new CommentListRequest(this);
		mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));
		mCommentsListAdapter = new CommentsListAdapter(this, mErrorLayout);
		mPullToRefreshSrl = (SwipeRefreshLayout) findViewById(R.id.pull_to_refresh_srl);
		mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
		mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				mList.clear();
				mCommentsListAdapter.setList(mList);
				mPullToRefreshSrl.setRefreshing(true);
				servicCall(Config.DEFAULT_PAGE_INDEX, false);
			}
		});

		mVHolder = new VHolder(findViewById(R.id.container_ll), this);
		mRefreshLayout = new RefreshLayout(this);

		mVHolder.mGenricLV.setOnScrollListener(new MyCustomonScrol());
	}

	private void getAndSetData() {

		Parcelable parcelableMedia = getIntent().getParcelableExtra("media");
		Parcelable parcelableAlbum = getIntent().getParcelableExtra("Album");

		ActionFor = getIntent().getStringExtra("FOR");
		Caption = getIntent().getStringExtra("Caption");
		mBlogGUID = getIntent().getStringExtra("blogGUID");

		mAlbumMedia = (AlbumMedia) parcelableMedia;
		mAlbumDetail = (Album) parcelableAlbum;

		if (Caption.equals("")) {
			if (null != mAlbumMedia) {
				Caption = mAlbumMedia.ImageName;
			} else if (null != mAlbumDetail) {
				Caption = mAlbumDetail.CoverMedia;
			} else {

			}

		}

	}

	class VHolder {
		private HeaderLayout headerLayout;

		TextView mWriteCommentsTV, mLikedTV;
		private ListView mGenricLV;
		private TextView mWriteCommentTV;
		private View mTransLayoutBottom;
		private ProgressBar mLoadingPB;

		// ListView
		public VHolder(View view, OnClickListener listener) {

			headerLayout = new HeaderLayout(view.findViewById(R.id.header_layout));
			headerLayout.setHeaderValues(R.drawable.icon_close, getResources().getString(R.string.Comments), 0);
			headerLayout.setListenerItI(CommentListActivity.this, CommentListActivity.this);
			mGenricLV = (ListView) view.findViewById(R.id.genric_lv);
			mLikedTV = (TextView) view.findViewById(R.id.liked_tv);
			mWriteCommentsTV = (TextView) view.findViewById(R.id.write_comments_tv);
			mLoadingPB = (ProgressBar) view.findViewById(R.id.loading_pb);
			mEmptyRl = (RelativeLayout) view.findViewById(R.id.empty_rl);
			mWriteCommentTV = (TextView) view.findViewById(R.id.write_comment_tv);
			mLoadingPB = (ProgressBar) view.findViewById(R.id.middle_pb);

			mGenricLV.setAdapter(mCommentsListAdapter);
			mWriteCommentTV.setOnClickListener(listener);
			FontLoader.setRobotoRegularTypeface(mLikedTV, mWriteCommentTV, mWriteCommentTV);
		}
	}

	boolean isLoading;

	class MyCustomonScrol implements OnScrollListener {
		private int mLastFirstVisibleItem;

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			int k = visibleItemCount + firstVisibleItem;
			if (k >= totalItemCount && isLoading && totalItemCount != 0) {
				servicCall(pageNumbermain, true);
			}
			if (mLastFirstVisibleItem < firstVisibleItem) {
				Log.i("SCROLLING DOWN", "TRUE");
			}
			if (mLastFirstVisibleItem > firstVisibleItem) {
				Log.i("SCROLLING UP", "TRUE");
			}
			mLastFirstVisibleItem = firstVisibleItem;
		}
	}

	@Override
	public void onBackPressed() {
		this.finish();
		this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);

	}

	public static Intent getIntent(Context context, AlbumMedia media, String commentFor, String Caption) {
		Intent intent = new Intent(context, CommentListActivity.class);
		intent.putExtra("media", media);
		intent.putExtra("FOR", commentFor);
		intent.putExtra("Caption", Caption);
		return intent;
	}

	public static Intent getIntent(Context context, Album nAlbum, String Caption) {
		Intent intent = new Intent(context, CommentListActivity.class);
		intent.putExtra("Album", nAlbum);
		intent.putExtra("FOR", "ALBUM");
		intent.putExtra("Caption", Caption);
		return intent;
	}

	public static Intent getIntentBlog(Context context, String blogGUID, String Caption) {
		Intent intent = new Intent(context, CommentListActivity.class);
		intent.putExtra("blogGUID", blogGUID);
		intent.putExtra("FOR", FOR_BLOG);
		intent.putExtra("Caption", Caption);
		return intent;
	}

	//
	final private int REQ_CODE_WRITE_COMMENT = 111;
	public static final int REQ_CODE_WRITE_COMMENT_ALBUM = 112;
	final private int REQ_CODE_WRITE_COMMENT_POST = 1185;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_button: {
			this.finish();
			this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
		}

			break;
		case R.id.write_comment_tv: {

			if (ActionFor.equals("ALBUM")) {

				this.startActivityForResult(WriteCommentActivity.getIntent(this, mAlbumDetail, Caption), REQ_CODE_WRITE_COMMENT_ALBUM);

			} else if (ActionFor.equals("MEDIA")) {

				this.startActivityForResult(WriteCommentActivity.getIntent(this, mAlbumMedia, "MEDIA", Caption), REQ_CODE_WRITE_COMMENT);
			} else if (ActionFor.equals("WALLPOST")) {
				this.startActivityForResult(WriteCommentActivity.getIntent(this, mAlbumMedia, "ACTIVITY", Caption),
						REQ_CODE_WRITE_COMMENT_POST);
			} else if (ActionFor.equals(FOR_BLOG)) {

				this.startActivityForResult(WriteCommentActivity.getIntentBlog(this, mBlogGUID, "BLOG", Caption),
						REQ_CODE_WRITE_COMMENT_POST);
			}

			this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
		}

			break;

		//
		default:
			break;
		}
	}

	public void deleteAndRefresh() {
		if (ActionFor.equals("MEDIA")) {

			int newCount = mAlbumMedia.NoOfComments - 1;
			mAlbumMedia.NoOfComments = newCount;
			mList.clear();
			mCommentsListAdapter.setList(mList);
			servicCall(1, true);
			Intent intent = new Intent("WriteComment");
			intent.putExtra("media", mAlbumMedia);
			sendBroadcast(intent);
			CommentListActivity.this.setResult(RESULT_OK);
		} else if (ActionFor.equals("ALBUM")) {
			int newCount = mAlbumDetail.NoOfComments - 1;
			mAlbumDetail.NoOfComments = newCount;
			mList.clear();
			mCommentsListAdapter.setList(mList);
			servicCall(1, true);

			CommentListActivity.this.setResult(RESULT_OK);
		} else if (ActionFor.equals("WALLPOST")) {

			mList.clear();
			mCommentsListAdapter.setList(mList);
			servicCall(1, true);
			CommentListActivity.this.setResult(RESULT_OK);
		} else if (ActionFor.equals(FOR_BLOG)) {

			mList.clear();
			mCommentsListAdapter.setList(mList);
			servicCall(1, true);
			CommentListActivity.this.setResult(RESULT_OK);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
		case REQ_CODE_WRITE_COMMENT:
			if (resultCode == RESULT_OK) {
				int newCount = mAlbumMedia.NoOfComments + 1;
				mAlbumMedia.NoOfComments = newCount;
				mList.clear();
				mCommentsListAdapter.setList(null);
				servicCall(1, true);

				Intent intent = new Intent("WriteComment");
				intent.putExtra("media", mAlbumMedia);
				sendBroadcast(intent);
				CommentListActivity.this.setResult(RESULT_OK);
			}
			break;
		case REQ_CODE_WRITE_COMMENT_ALBUM:
			if (resultCode == RESULT_OK) {

				int newCount = mAlbumDetail.NoOfComments + 1;
				mAlbumDetail.NoOfComments = newCount;
				mList.clear();
				mCommentsListAdapter.setList(null);
				servicCall(1, true);

				CommentListActivity.this.setResult(RESULT_OK);

			}
			break;
		case REQ_CODE_WRITE_COMMENT_POST:
			if (resultCode == RESULT_OK) {
				mList.clear();
				mCommentsListAdapter.setList(null);
				servicCall(1, true);
				CommentListActivity.this.setResult(RESULT_OK);

			}
			break;

		default:
			break;
		}

		super.onActivityResult(requestCode, resultCode, data);

	}
}
