package com.schollyme.scores;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.BaseActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.MyScores;
import com.vinfotech.request.CoachTranscriptsRequest;
import com.vinfotech.request.GetScoresRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;
import com.vinfotech.widget.FloatLabel;

public class FriendsScoreActivity extends BaseActivity implements OnClickListener{
	
	private ViewHolder mViewHolder;
	private ErrorLayout mErrorLayout;

	private Context mContext;
	private String mUserGUID;
	private LogedInUserModel mLoggedinUser;
	private MyScores mUserScores;
	private String mUserName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friends_scores_activity);
		mViewHolder = new ViewHolder(findViewById(R.id.main_rl),this);
		mErrorLayout = new ErrorLayout(findViewById(R.id.scroll_main_rl));
		
		mContext = this;
		mUserGUID = getIntent().getStringExtra("UserGUID");
		mUserName =  getIntent().getStringExtra("UserName");
		
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back,0,
				mUserName, new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		},null);
		mLoggedinUser = new LogedInUserModel(mContext);
		getScoresRequest();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.upload_transcripts_tv:
				coachTranscriptsRequest(mUserGUID);
			break;
		default:
			break;
		}
	}
	
	private void setUserScores(MyScores mScores){
		
		boolean isAnyScoreVerified = false;
		try {
			mViewHolder.mActScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_sat_score_active, 0, 0, 0);
			mViewHolder.mSatScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_sat_score_active, 0, 0, 0);
			mViewHolder.mGPAScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_gpa_active, 0, 0, 0);
			mViewHolder.mTranscriptsET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_transcript_active, 0, 0, 0);
			/** If athlete view his scores then he can edit and request*/
			if(mLoggedinUser.mUserGUID.equals(mUserGUID)){}
			/** If paid coach view athlete  scores then he can only view*/
			else{
				
				mViewHolder.mActScoreET.setEnabled(false);
				mViewHolder.mGPAScoreET.setEnabled(false);
				mViewHolder.mSatScoreET.setEnabled(false);
				mViewHolder.mActUpdateTV.setClickable(false);
				mViewHolder.mSatUpdateTV.setClickable(false);
				mViewHolder.mGPAUpdateTV.setClickable(false);
				mViewHolder.mTranscriptsUpdateTV.setClickable(false);
				
				/**ACT Score*/
				if(null!=mScores.mACTScore && !TextUtils.isEmpty(mScores.mACTScore) && !mScores.mGPAScore.equals("0.00")){
					isAnyScoreVerified = true;
					mViewHolder.mActRL.setVisibility(View.VISIBLE);
					mViewHolder.mActScoreET.setText(mScores.mACTScore);
					if(mScores.mACTVerificationStatus.equals("VERIFIED")){
						String nDate = Utility.formateDateForScores(mScores.mACTVerificationDate);
						mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.verified_on)+" "+nDate);
					}
					else{
						mViewHolder.mActUpdateTV.setVisibility(View.GONE);
					}
				}
				else{
					mViewHolder.mActRL.setVisibility(View.GONE);
					mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.no_score));
				}
				
				/**GPA Score*/
				if(null!=mScores.mGPAScore && !TextUtils.isEmpty(mScores.mGPAScore)/* && !mScores.mGPAScore.equals("0.00")*/){
					isAnyScoreVerified = true;
					mViewHolder.mGpaRL.setVisibility(View.VISIBLE);
					mViewHolder.mGPAScoreET.setText(mScores.mGPAScore);
					if(mScores.mGPAVerificationStatus.equals("VERIFIED")){
						String nDate = Utility.formateDateForScores(mScores.mGPAVerificationDate);
						mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.verified_on)+" "+nDate);
					}
					else{
						mViewHolder.mGPAUpdateTV.setVisibility(View.GONE);
					}
				}
				else{
					mViewHolder.mGpaRL.setVisibility(View.GONE);
					mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.no_score));
				}
				
				/**SAT Score*/
				if(null!=mScores.mSATScore && !TextUtils.isEmpty(mScores.mSATScore)){
					isAnyScoreVerified = true;
					mViewHolder.mSatRL.setVisibility(View.VISIBLE);
					mViewHolder.mSatScoreET.setText(mScores.mSATScore);
					if(mScores.mSATVerificationStatus.equals("VERIFIED")){
						
						String nDate = Utility.formateDateForScores(mScores.mSATVerificationDate);
						mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.verified_on)+" "+nDate);
					}
					else{
						mViewHolder.mSatUpdateTV.setVisibility(View.GONE);
					}
				}
				else{
					mViewHolder.mSatRL.setVisibility(View.GONE);
					mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.no_score));
				}
				
				/** Transcripts */
				if(null!=mScores.mTranscriptsVerificationStatus && mScores.mTranscriptsVerificationStatus.equalsIgnoreCase("VERIFIED")){
					isAnyScoreVerified = true;
					mViewHolder.mTranscriptsRL.setVisibility(View.VISIBLE);
					if(mScores.mTranscriptsVerificationStatus.equals("VERIFIED")){
						String nDate = Utility.formateDateForScores(mScores.mTranscriptsVerificationDate);
						mViewHolder.mTranscriptsET.setText(getString(R.string.uploadedon_transcripts)+" "+nDate);
						mViewHolder.mTranscriptsUpdateTV.setText(getString(R.string.request_view));
						mViewHolder.mTranscriptsUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
						mViewHolder.mTranscriptsUpdateTV.setClickable(true);
					}
					else{
						mViewHolder.mTranscriptsUpdateTV.setVisibility(View.GONE);
					}
				}
				else{
					mViewHolder.mTranscriptsRL.setVisibility(View.GONE);
				}
				
				if(!isAnyScoreVerified){
					mViewHolder.mNoScoreTV.setVisibility(View.VISIBLE);
				}
				else{
					mViewHolder.mNoScoreTV.setVisibility(View.GONE);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	public static Intent getIntent(Context context,String mUserGUID,String mUserName) {
		Intent intent = new Intent(context, FriendsScoreActivity.class);
		intent.putExtra("UserGUID", mUserGUID);
		intent.putExtra("UserName", mUserName);
		return intent;
	}
	
	/** Get Scores API request*/
	private void getScoresRequest(){
		GetScoresRequest mRequest = new GetScoresRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mUserScores = (MyScores) data;
					setUserScores(mUserScores);
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.GetScoresServerRequest(mUserGUID);
	}
	
	/** Coach request to athlete for transcripts*/
	private void coachTranscriptsRequest(String mAthleteUserGUID){
		CoachTranscriptsRequest mRequest = new CoachTranscriptsRequest(mContext);
		mRequest.getCoachTranscriptRequest(mAthleteUserGUID);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mErrorLayout.showError(getString(R.string.req_successfully), true,MsgType.Success);
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);				}
			}
		});
	}
	
	private class ViewHolder{

		public FloatLabel mGPAFL,mActFL,mSatFL,mTranscriptsFL;
		public EditText mActScoreET,mGPAScoreET,mSatScoreET,mTranscriptsET;
		public TextView mActUpdateTV,mGPAUpdateTV,mSatUpdateTV,mTranscriptsUpdateTV,mNoScoreTV;
		private RelativeLayout mSatRL,mActRL,mGpaRL,mTranscriptsRL;

		public ViewHolder(View view,OnClickListener listener){
			mActFL = (FloatLabel) view.findViewById(R.id.act_et);
			mGPAFL = (FloatLabel) view.findViewById(R.id.gpa_et);
			mSatFL = (FloatLabel) view.findViewById(R.id.sat_et);
			mTranscriptsFL = (FloatLabel) view.findViewById(R.id.transcripts_et);

			mActScoreET = mActFL.getEditText();
			mGPAScoreET = mGPAFL.getEditText();
			mSatScoreET = mSatFL.getEditText();
			mTranscriptsET = mTranscriptsFL.getEditText();
			mTranscriptsET.setEnabled(false);

			mActUpdateTV = (TextView) view.findViewById(R.id.add_act_tv);
			mGPAUpdateTV = (TextView) view.findViewById(R.id.add_gpa_tv);
			mSatUpdateTV = (TextView) view.findViewById(R.id.add_sat_tv);
			mTranscriptsUpdateTV = (TextView) view.findViewById(R.id.upload_transcripts_tv);
			
			mSatRL = (RelativeLayout) view.findViewById(R.id.sat_rl);
			mActRL = (RelativeLayout) view.findViewById(R.id.act_rl);
			mGpaRL = (RelativeLayout) view.findViewById(R.id.gpa_rl);
			mTranscriptsRL = (RelativeLayout) view.findViewById(R.id.transcripts_rl);
			
			mNoScoreTV = (TextView) view.findViewById(R.id.no_record_message_tv);

			mActUpdateTV.setOnClickListener(listener);
			mGPAUpdateTV.setOnClickListener(listener);
			mSatUpdateTV.setOnClickListener(listener);
			mTranscriptsUpdateTV.setOnClickListener(listener);

			FontLoader.setRobotoRegularTypeface(mActScoreET,mGPAScoreET,mSatScoreET,mTranscriptsET,
					mActUpdateTV,mGPAUpdateTV,mSatUpdateTV,mTranscriptsUpdateTV,mNoScoreTV);
		}
	}
}