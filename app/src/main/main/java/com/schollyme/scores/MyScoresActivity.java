package com.schollyme.scores;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.schollyme.BaseActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.MyScores;
import com.vinfotech.request.CoachTranscriptsRequest;
import com.vinfotech.request.GetScoresRequest;
import com.vinfotech.request.UpdateScoresRequest;
import com.vinfotech.request.VerifyScoresRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;
import com.vinfotech.widget.FloatLabel;

public class MyScoresActivity extends BaseActivity implements OnClickListener{

	private ViewHolder mViewHolder;
	private ErrorLayout mErrorLayout;

	private Context mContext;
	private String mUserGUID;
	private LogedInUserModel mLoggedinUser;
	private MyScores mUserScores;
	private int GPAStatusType = 0,ACTStatusType = 0,SATStatusType = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_scores_activity);
		mViewHolder = new ViewHolder(findViewById(R.id.main_rl),this);
		mErrorLayout = new ErrorLayout(findViewById(R.id.scroll_main_rl));
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back,0,
				getResources().getString(R.string.my_score_small), new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		},null);
		mContext = this;
		mUserGUID = getIntent().getStringExtra("UserGUID");
		mLoggedinUser = new LogedInUserModel(mContext);
		getScoresRequest();
	}

	public static Intent getIntent(Context context,String mUserGUID) {
		Intent intent = new Intent(context, MyScoresActivity.class);
		intent.putExtra("UserGUID", mUserGUID);
		return intent;
	}


	private void setUserScores(MyScores mScores){
		try {
			if(mScores.mACTScore==null || TextUtils.isEmpty(mScores.mACTScore)){
				mViewHolder.mActScoreET.requestFocus();
			}
			else if(mScores.mGPAScore==null || TextUtils.isEmpty(mScores.mGPAScore)){
				mViewHolder.mGPAScoreET.requestFocus();
			}
			else if(mScores.mSATScore==null || TextUtils.isEmpty(mScores.mSATScore)){
				mViewHolder.mSatScoreET.requestFocus();
			}


			mViewHolder.mGPAScoreET.setFilters(new InputFilter[] { filter });
			
			mViewHolder.mActScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_sat_score, 0, 0, 0);
			mViewHolder.mSatScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_sat_score, 0, 0, 0);
			mViewHolder.mGPAScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_gpa, 0, 0, 0);
			mViewHolder.mTranscriptsET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_transcript, 0, 0, 0);
			/** If athlete view his scores then he can edit and request*/
			if(mLoggedinUser.mUserGUID.equals(mUserGUID)){

				/** Set status for ACT*/
				resetACT(mScores);
				/** Set status for SAT*/
			    resetSAT(mScores);
				/** Set status for GPA*/
				resetGPA(mScores);
				/** Set status for Transcripts*/
				if(mScores.mTranscriptsVerificationStatus.equals("")){
					mViewHolder.mTranscriptsUpdateTV.setText(getResources().getString(R.string.request_upload));
			//		mViewHolder.mTranscriptsUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
					mViewHolder.mTranscriptsUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
					mViewHolder.mTranscriptsUpdateTV.setClickable(true);
					mViewHolder.mResendTranscriptsTV.setVisibility(View.GONE);
					mViewHolder.mTranscriptsUpdateTV.setVisibility(View.VISIBLE);
				}
				else if(mScores.mTranscriptsVerificationStatus.equals("REQUEST") || mScores.mTranscriptsVerificationStatus.equals("PENDING")){
					mViewHolder.mTranscriptsUpdateTV.setText(getResources().getString(R.string.transcripts_upload_pending));//pending string
			//		mViewHolder.mTranscriptsUpdateTV.setBackgroundDrawable(null);
					mViewHolder.mTranscriptsUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
					mViewHolder.mTranscriptsUpdateTV.setClickable(false);
					mViewHolder.mResendTranscriptsTV.setClickable(true);
					mViewHolder.mResendTranscriptsTV.setVisibility(View.VISIBLE);
					mViewHolder.mTranscriptsUpdateTV.setVisibility(View.VISIBLE);
				}
				else if(mScores.mTranscriptsVerificationStatus.equals("VERIFIED")){
					mViewHolder.mTranscriptsUpdateTV.setText(getString(R.string.upload_new));
					String nDate = Utility.formateDateForScores(mScores.mTranscriptsVerificationDate);
				//	mViewHolder.mTranscriptsUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
					mViewHolder.mTranscriptsET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_transcript_active, 0, 0, 0);
					mViewHolder.mTranscriptsET.setText(getResources().getString(R.string.uploadedon_transcripts)+" "+nDate);
					mViewHolder.mTranscriptsUpdateTV.setTextColor(getResources().getColor(R.color.verified_color));
					mViewHolder.mTranscriptsUpdateTV.setClickable(true);

					mViewHolder.mResendTranscriptsTV.setVisibility(View.GONE);
					mViewHolder.mTranscriptsUpdateTV.setVisibility(View.VISIBLE);
				}
			}
			/** If paid coach view athlete  scores then he can only view*/
			else{
				mViewHolder.mActEditIB.setVisibility(View.GONE);
				mViewHolder.mGPAEditIB.setVisibility(View.GONE);
				mViewHolder.mSatEditIB.setVisibility(View.GONE);

				mViewHolder.mActUpdateTV.setClickable(false);
				mViewHolder.mSatUpdateTV.setClickable(false);
				mViewHolder.mGPAUpdateTV.setClickable(false);
				mViewHolder.mTranscriptsUpdateTV.setClickable(false);
				if(null!=mScores.mACTScore && !TextUtils.isEmpty(mScores.mACTScore) && !mScores.mGPAScore.equals("0.00")){
					mViewHolder.mActScoreET.setText(mScores.mACTScore);
					mViewHolder.mActUpdateTV.setText("");
				}
				else{
					mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.no_score));
				}
				if(null!=mScores.mGPAScore && !TextUtils.isEmpty(mScores.mGPAScore) && !mScores.mGPAScore.equals("0.00")){
					mViewHolder.mGPAScoreET.setText(mScores.mGPAScore);
					mViewHolder.mGPAUpdateTV.setText("");
				}
				else{
					mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.no_score));
				}
				if(null!=mScores.mSATScore && !TextUtils.isEmpty(mScores.mSATScore)){
					mViewHolder.mSatScoreET.setText(mScores.mSATScore);
					mViewHolder.mSatUpdateTV.setText("");
				}
				else{
					mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.no_score));
				}
				if(null!=mScores.mTranscriptsVerificationStatus && mScores.mTranscriptsVerificationStatus.equalsIgnoreCase("VERIFIED")){
					mViewHolder.mTranscriptsUpdateTV.setText(Utility.getFormattedDate(mScores.mTranscriptsVerificationDate,mContext));
					mViewHolder.mTranscriptsUpdateTV.setClickable(true);
				}
				else{
					mViewHolder.mTranscriptsUpdateTV.setText(getResources().getString(R.string.no_transcripts));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mViewHolder.mActScoreET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean b) {
				if(b){
					resetGPA(mUserScores);
					resetSAT(mUserScores);
				}
			}
		});

		mViewHolder.mSatScoreET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean b) {
				if(b){
					resetGPA(mUserScores);
					resetACT(mUserScores);
				}
			}
		});

		mViewHolder.mGPAScoreET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean b) {
				if (b) {
					resetACT(mUserScores);
					resetSAT(mUserScores);
				}
			}
		});
	}

	private void resetACT(MyScores mScores){
		if(null!=mScores.mACTScore && !TextUtils.isEmpty(mScores.mACTScore) && !mScores.mACTScore.equals("0.00")){

			mViewHolder.mActScoreET.setText(mScores.mACTScore);
			mViewHolder.mActScoreET.setEnabled(false);
			mViewHolder.mActScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_sat_score_active, 0, 0, 0);
			if(mScores.mACTVerificationStatus.equals("")){
				mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.request_verification));
				mViewHolder.mActUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
			//	mViewHolder.mActUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
				mViewHolder.mActUpdateTV.setClickable(true);
				mViewHolder.mActEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mACTResendTV.setVisibility(View.GONE);
			}
			else if(mScores.mACTVerificationStatus.equals("REQUEST") || mScores.mACTVerificationStatus.equals("PENDING")){
				mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.request_verification_sent));
				mViewHolder.mActUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
		//		mViewHolder.mActUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mActUpdateTV.setClickable(false);
				mViewHolder.mACTResendTV.setClickable(true);
				mViewHolder.mActEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mACTResendTV.setVisibility(View.VISIBLE);
			}
			else if(mScores.mACTVerificationStatus.equals("UN_VERIFIED")){
				mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.not_verify));
				mViewHolder.mActUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
		//		mViewHolder.mActUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mActUpdateTV.setClickable(false);
				mViewHolder.mActEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mACTResendTV.setVisibility(View.GONE);
			}
			else if(mScores.mACTVerificationStatus.equals("VERIFIED")){
				String nDate = Utility.formateDateForScores(mScores.mACTVerificationDate);
				mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.verified_on) + " " + nDate);
		//		mViewHolder.mActUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mActUpdateTV.setTextColor(getResources().getColor(R.color.verified_color));
				mViewHolder.mActUpdateTV.setClickable(false);
				mViewHolder.mActEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mACTResendTV.setVisibility(View.GONE);
			}
			mViewHolder.mActEditIB.setImageResource(R.drawable.icon_edit_black);
			ACTStatusType = 1;
		}
		else{
	//		mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.add_score));
	//		mViewHolder.mActUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
			mViewHolder.mActEditIB.setVisibility(View.VISIBLE);
			mViewHolder.mActEditIB.setClickable(false);
			mViewHolder.mActEditIB.setImageResource(R.drawable.icon_tick_black_disable);
			mViewHolder.mActUpdateTV.setClickable(false);
			ACTStatusType = 2;
			mViewHolder.mACTResendTV.setVisibility(View.GONE);
		}

	}

	private void resetSAT(MyScores mScores){
		if(null!=mScores.mSATScore && !TextUtils.isEmpty(mScores.mSATScore)){

			mViewHolder.mSatScoreET.setText(mScores.mSATScore);
			mViewHolder.mSatScoreET.setEnabled(false);
			mViewHolder.mSatScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_sat_score_active, 0, 0, 0);

			if(mScores.mSATVerificationStatus.equals("")){
				mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.request_verification));
				mViewHolder.mSatUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
		//		mViewHolder.mSatUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
				mViewHolder.mSatUpdateTV.setClickable(true);
				mViewHolder.mSatEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mSATResendTV.setVisibility(View.GONE);
			}
			else if(mScores.mSATVerificationStatus.equals("REQUEST") || mScores.mSATVerificationStatus.equals("PENDING")){
				mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.request_verification_sent));
				mViewHolder.mSatUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
	//			mViewHolder.mSatUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mSatUpdateTV.setClickable(false);
				mViewHolder.mSATResendTV.setClickable(true);
				mViewHolder.mSatEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mSATResendTV.setVisibility(View.VISIBLE);
			}
			else if(mScores.mSATVerificationStatus.equals("UN_VERIFIED")){
				mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.not_verify));
				mViewHolder.mSatUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
	//			mViewHolder.mSatUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mSatUpdateTV.setClickable(false);
				mViewHolder.mSatEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mSATResendTV.setVisibility(View.GONE);
			}
			else if(mScores.mSATVerificationStatus.equals("VERIFIED")){
				String nDate = Utility.formateDateForScores(mScores.mSATVerificationDate);
				mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.verified_on)+" "+nDate);
		//		mViewHolder.mSatUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mSatUpdateTV.setTextColor(getResources().getColor(R.color.verified_color));
				mViewHolder.mSatUpdateTV.setClickable(false);
				mViewHolder.mSatEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mSATResendTV.setVisibility(View.GONE);
			}
			mViewHolder.mSatEditIB.setImageResource(R.drawable.icon_edit_black);
			SATStatusType =1;
		}
		else{
			SATStatusType = 2;
			mViewHolder.mSatEditIB.setVisibility(View.VISIBLE);
			mViewHolder.mSatEditIB.setImageResource(R.drawable.icon_tick_black_disable);
	//		mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.add_score));
	//		mViewHolder.mSatUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
			mViewHolder.mSatUpdateTV.setClickable(false);
			mViewHolder.mSatEditIB.setClickable(false);
			mViewHolder.mSATResendTV.setVisibility(View.GONE);
		}
	}

	private void resetGPA(MyScores mScores){
		if(null!=mScores.mGPAScore && !TextUtils.isEmpty(mScores.mGPAScore)/* && !mScores.mGPAScore.equals("0.00")*/){

			mViewHolder.mGPAScoreET.setText(mScores.mGPAScore);
			mViewHolder.mGPAScoreET.setEnabled(false);
			mViewHolder.mGPAScoreET.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_gpa_active, 0, 0, 0);
			if(mScores.mGPAVerificationStatus.equals("")){
				mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.request_verification));
				mViewHolder.mGPAUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
			//	mViewHolder.mGPAUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
				mViewHolder.mGPAUpdateTV.setClickable(true);
				mViewHolder.mGPAEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mGPAResendTV.setVisibility(View.GONE);
			}
			else if(mScores.mGPAVerificationStatus.equals("REQUEST") || mScores.mGPAVerificationStatus.equals("PENDING")){
				mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.request_verification_sent));
				mViewHolder.mGPAUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
		//		mViewHolder.mGPAUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mGPAUpdateTV.setClickable(false);
				mViewHolder.mGPAResendTV.setClickable(true);
				mViewHolder.mGPAEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mGPAResendTV.setVisibility(View.VISIBLE);
			}
			else if(mScores.mGPAVerificationStatus.equals("UN_VERIFIED")){
				mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.not_verify));
				mViewHolder.mGPAUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
		//		mViewHolder.mGPAUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mGPAUpdateTV.setClickable(false);
				mViewHolder.mGPAEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mGPAResendTV.setVisibility(View.GONE);
			}
			else if(mScores.mGPAVerificationStatus.equals("VERIFIED")){

				String nDate = Utility.formateDateForScores(mScores.mGPAVerificationDate);
				mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.verified_on)+" "+nDate);
		//		mViewHolder.mGPAUpdateTV.setBackgroundDrawable(null);
				mViewHolder.mGPAUpdateTV.setTextColor(getResources().getColor(R.color.verified_color));
				mViewHolder.mGPAUpdateTV.setClickable(false);
				mViewHolder.mGPAEditIB.setVisibility(View.VISIBLE);
				mViewHolder.mGPAResendTV.setVisibility(View.GONE);
			}
			mViewHolder.mGPAEditIB.setImageResource(R.drawable.icon_edit_black);
			GPAStatusType = 1;
		}
		else{
			mViewHolder.mGPAEditIB.setVisibility(View.VISIBLE);
			GPAStatusType = 2;
			mViewHolder.mGPAEditIB.setImageResource(R.drawable.icon_tick_black_disable);
	//		mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.add_score));
		//	mViewHolder.mGPAUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
			mViewHolder.mGPAUpdateTV.setClickable(false);
			mViewHolder.mGPAEditIB.setClickable(false);
			mViewHolder.mGPAResendTV.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.resend_act_tv:
			if(mViewHolder.mActUpdateTV.getText().toString().equals(getString(R.string.request_verification_sent))){
				verifyScoresRequest("ACT",false);
			}
			break;
		case R.id.resend_sat_tv:
			if(mViewHolder.mSatUpdateTV.getText().toString().equals(getString(R.string.request_verification_sent))){
				verifyScoresRequest("SAT",false);
			}
			break;
		case R.id.resend_gpa_tv:
			if(mViewHolder.mGPAUpdateTV.getText().toString().equals(getString(R.string.request_verification_sent))){
				verifyScoresRequest("GPA",false);
			}
			break;

		case R.id.resend_transcripts_tv:
			if(mViewHolder.mTranscriptsUpdateTV.getText().toString().equals(getString(R.string.transcripts_upload_pending))){
				verifyScoresRequest("TRANSCRIPT",false);
			}
			break;

		case R.id.add_act_tv:
			if(mUserScores!=null && mLoggedinUser.mUserGUID.equals(mUserGUID)){
				if(mViewHolder.mActUpdateTV.getText().toString().equals(getString(R.string.request_verification))){
					//TODO call API to request Verification,When Current score is not blank or zero
					verifyScoresRequest("ACT",false);
				}
				else if(mViewHolder.mActUpdateTV.getText().toString().equals(getString(R.string.add_score)) || mViewHolder.mActUpdateTV.getText().toString().equals(getString(R.string.update_score))){
					//TODO call API to updated score,When Current score is blank or zero
					String mScore = mViewHolder.mActScoreET.getText().toString();
					Utility.hideSoftKeyboard(mViewHolder.mActScoreET);
					if(!TextUtils.isEmpty(mScore)) {
						updateScoresRequest("ACT", mScore);
					}
					else{
						mErrorLayout.showError(getString(R.string.act_score_require),true,MsgType.Error);
					}
				}
			}
			break;

		case R.id.add_gpa_tv:
			if(mUserScores!=null && mLoggedinUser.mUserGUID.equals(mUserGUID)){
				if(mViewHolder.mGPAUpdateTV.getText().toString().equals(getString(R.string.request_verification))){
					//TODO call API to request Verification,When Current score is not blank or zero
					verifyScoresRequest("GPA",false);
				}
				else if(mViewHolder.mGPAUpdateTV.getText().toString().equals(getString(R.string.add_score)) || mViewHolder.mGPAUpdateTV.getText().toString().equals(getString(R.string.update_score))){
					//TODO call API to updated score,When Current score is blank or zero
					String mGPAScore = mViewHolder.mGPAScoreET.getText().toString();
					Double mFormatedGPA = 0.00;
					if (!TextUtils.isEmpty(mGPAScore)) {
						try {
							Utility.hideSoftKeyboard(mViewHolder.mActScoreET);
							mFormatedGPA = Double.parseDouble(mGPAScore);
							updateScoresRequest("GPA", String.valueOf(mFormatedGPA));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else{
						mErrorLayout.showError(getString(R.string.gpa_score_require),true,MsgType.Error);
					}
				}
			}
			break;

		case R.id.add_sat_tv:
			if(mUserScores!=null && mLoggedinUser.mUserGUID.equals(mUserGUID)){
				if(mViewHolder.mSatUpdateTV.getText().toString().equals(getString(R.string.request_verification))){
					//TODO call API to request Verification,When Current score is not blank or zero
					verifyScoresRequest("SAT",false);
				}
				else if(mViewHolder.mSatUpdateTV.getText().toString().equals(getString(R.string.add_score)) || mViewHolder.mSatUpdateTV.getText().toString().equals(getString(R.string.update_score))){
					//TODO call API to updated score,When Current score is blank or zero
					Utility.hideSoftKeyboard(mViewHolder.mActScoreET);
				 	String mSATScore = mViewHolder.mSatScoreET.getText().toString();
					if(!TextUtils.isEmpty(mSATScore)) {
						updateScoresRequest("SAT", mSATScore);
					}
					else{
						mErrorLayout.showError(getString(R.string.sat_score_require), true, MsgType.Error);
					}
				}
			}
			break;

		case R.id.upload_transcripts_tv:
			if((mLoggedinUser.mUserGUID.equals(mUserGUID) ||  mUserGUID.equals(new LogedInUserModel(mContext).mUserName)) && mUserScores!=null){
				if(mUserScores.mTranscriptsVerificationStatus.equals("VERIFIED")){
					DialogUtil.showOkCancelDialog(mContext, getString(R.string.upload_new_transcript),getString(R.string.upload_new_transcripts_confirm), 
							//upload_new_transcript,upload_new_transcript_confirm
							new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							mViewHolder.mTranscriptsUpdateTV.setText(getString(R.string.request_upload));
							mViewHolder.mTranscriptsET.setText("");
							mViewHolder.mTranscriptsUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
							mViewHolder.mTranscriptsUpdateTV.setClickable(true);
							verifyScoresRequest("TRANSCRIPT",true);
						
						}
					}, new OnClickListener() {

						@Override
						public void onClick(View v) {

						}
					} );
				}
				else{
					verifyScoresRequest("TRANSCRIPT",true);
				}
			}
			else{
				coachTranscriptsRequest(mUserGUID);
			}

			break;

		case R.id.edit_act_ib:
			resetGPA(mUserScores);
			resetSAT(mUserScores);
			if(ACTStatusType == 1){

				if(mUserScores!=null && mLoggedinUser.mUserGUID.equals(mUserGUID) && mUserScores.mACTVerificationStatus.equals("VERIFIED")){
					ACTStatusType = 1;
					DialogUtil.showOkCancelDialog(mContext, getString(R.string.edit_verify_score),getString(R.string.edit_verify_score_message),
							new OnClickListener() {

								@Override
								public void onClick(View arg0) {
									mViewHolder.mActEditIB.setVisibility(View.VISIBLE);
									mViewHolder.mActEditIB.setImageResource(R.drawable.icon_tick_black_normal);
									ACTStatusType = 2;
									mViewHolder.mActScoreET.setEnabled(true);
									mViewHolder.mActScoreET.setSelection(mViewHolder.mActScoreET.getText().toString().length());
									mViewHolder.mActScoreET.requestFocus();
									//			mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.update_score));
									//			mViewHolder.mActUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
									mViewHolder.mActUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
									mViewHolder.mActUpdateTV.setClickable(true);
									toggleSoftInputMode();
								}
							}, new OnClickListener() {

								@Override
								public void onClick(View v) {

								}
							} );
				}
				else{
					ACTStatusType = 2;
					mViewHolder.mActEditIB.setVisibility(View.VISIBLE);
					mViewHolder.mActEditIB.setImageResource(R.drawable.icon_tick_black_normal);
					mViewHolder.mActScoreET.setEnabled(true);
					mViewHolder.mActScoreET.setSelection(mViewHolder.mActScoreET.getText().toString().length());
					mViewHolder.mActScoreET.requestFocus();
					//	mViewHolder.mActUpdateTV.setText(getResources().getString(R.string.update_score));
					//	mViewHolder.mActUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
					mViewHolder.mActUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
					mViewHolder.mActUpdateTV.setClickable(false);
					mViewHolder.mACTResendTV.setClickable(false);
					toggleSoftInputMode();
				}
			}
			else if(ACTStatusType == 2 ){
				//TODO call API to updated score,When Current score is blank or zero
				String mScore = mViewHolder.mActScoreET.getText().toString();
				Utility.hideSoftKeyboard(mViewHolder.mActScoreET);
				if(!TextUtils.isEmpty(mScore)) {
					updateScoresRequest("ACT", mScore);
				}
				else{
					mErrorLayout.showError(getString(R.string.act_score_require),true,MsgType.Error);
				}
			}

			break;

		case R.id.edit_gpa_ib:
			resetSAT(mUserScores);
			resetACT(mUserScores);
			if(GPAStatusType==1){

				if(mUserScores!=null && mLoggedinUser.mUserGUID.equals(mUserGUID) && mUserScores.mGPAVerificationStatus.equals("VERIFIED")){
					GPAStatusType = 1;
					DialogUtil.showOkCancelDialog(mContext, getString(R.string.edit_verify_score),getString(R.string.edit_verify_score_message),
							new OnClickListener() {

								@Override
								public void onClick(View arg0) {
									mViewHolder.mGPAEditIB.setVisibility(View.VISIBLE);
									mViewHolder.mGPAEditIB.setImageResource(R.drawable.icon_tick_black_normal);
									GPAStatusType = 2;
									mViewHolder.mGPAScoreET.setEnabled(true);
									mViewHolder.mGPAScoreET.setSelection(mViewHolder.mGPAScoreET.getText().toString().length());
									mViewHolder.mGPAScoreET.requestFocus();
									//		mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.update_score));
									//		mViewHolder.mGPAUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
									mViewHolder.mGPAUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
									mViewHolder.mGPAUpdateTV.setClickable(true);
									toggleSoftInputMode();
								}
							}, new OnClickListener() {

								@Override
								public void onClick(View v) {

								}
							} );

				}
				else{
					GPAStatusType = 2;
					mViewHolder.mGPAEditIB.setVisibility(View.VISIBLE);
					mViewHolder.mGPAEditIB.setImageResource(R.drawable.icon_tick_black_normal);
					mViewHolder.mGPAScoreET.setEnabled(true);
					mViewHolder.mGPAScoreET.setSelection(mViewHolder.mGPAScoreET.getText().toString().length());
					mViewHolder.mGPAScoreET.requestFocus();
					//		mViewHolder.mGPAUpdateTV.setText(getResources().getString(R.string.update_score));
					//		mViewHolder.mGPAUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
					mViewHolder.mGPAUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
					mViewHolder.mGPAUpdateTV.setClickable(false);
					mViewHolder.mGPAResendTV.setClickable(false);
					toggleSoftInputMode();
				}
			}
			else if(GPAStatusType==2){
				if(mUserScores!=null && mLoggedinUser.mUserGUID.equals(mUserGUID)){
					//TODO call API to updated score,When Current score is blank or zero
						String mGPAScore = mViewHolder.mGPAScoreET.getText().toString();
						Double mFormatedGPA = 0.00;
						if (!TextUtils.isEmpty(mGPAScore)) {
							try {
								Utility.hideSoftKeyboard(mViewHolder.mActScoreET);
								mFormatedGPA = Double.parseDouble(mGPAScore);
								updateScoresRequest("GPA", String.valueOf(mFormatedGPA));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						else{
							mErrorLayout.showError(getString(R.string.gpa_score_require),true,MsgType.Error);
						}
					}
			}

			break;

		case R.id.edit_sat_ib:
			resetGPA(mUserScores);
			resetACT(mUserScores);
			if(SATStatusType == 1){

				if(mUserScores!=null && mLoggedinUser.mUserGUID.equals(mUserGUID) && mUserScores.mSATVerificationStatus.equals("VERIFIED")){
					SATStatusType = 1;
					DialogUtil.showOkCancelDialog(mContext, getString(R.string.edit_verify_score),getString(R.string.edit_verify_score_message),
							new OnClickListener() {

								@Override
								public void onClick(View arg0) {
									mViewHolder.mSatEditIB.setVisibility(View.VISIBLE);
									mViewHolder.mSatEditIB.setImageResource(R.drawable.icon_tick_black_normal);
									SATStatusType = 2;
									mViewHolder.mSatScoreET.setEnabled(true);
									mViewHolder.mSatScoreET.setSelection(mViewHolder.mSatScoreET.getText().toString().length());
									mViewHolder.mSatScoreET.requestFocus();
									//			mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.update_score));
									//			mViewHolder.mSatUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
									mViewHolder.mSatUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
									mViewHolder.mSatUpdateTV.setClickable(true);
									toggleSoftInputMode();
								}
							}, new OnClickListener() {

								@Override
								public void onClick(View v) {

								}
							} );


				}
				else{
					SATStatusType = 2;
					mViewHolder.mSatEditIB.setVisibility(View.VISIBLE);
					mViewHolder.mSatEditIB.setImageResource(R.drawable.icon_tick_black_normal);
					mViewHolder.mSatScoreET.setEnabled(true);
					mViewHolder.mSatScoreET.setSelection(mViewHolder.mSatScoreET.getText().toString().length());
					mViewHolder.mSatScoreET.requestFocus();
					//	mViewHolder.mSatUpdateTV.setText(getResources().getString(R.string.update_score));
					//	mViewHolder.mSatUpdateTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.score_button_bg));
					mViewHolder.mSatUpdateTV.setTextColor(getResources().getColor(R.color.add_score_color));
					mViewHolder.mSatUpdateTV.setClickable(false);
					mViewHolder.mSATResendTV.setClickable(false);
					toggleSoftInputMode();
				}
			}
			else if(SATStatusType == 2){
				//TODO call API to updated score,When Current score is blank or zero
				Utility.hideSoftKeyboard(mViewHolder.mActScoreET);
				String mSATScore = mViewHolder.mSatScoreET.getText().toString();
				if(!TextUtils.isEmpty(mSATScore)) {
					updateScoresRequest("SAT", mSATScore);
				}
				else{
					mErrorLayout.showError(getString(R.string.sat_score_require), true, MsgType.Error);
				}
			}

			break;

		default:
			break;
		}
	}

	/** Get Scores API request*/
	private void getScoresRequest(){
		GetScoresRequest mRequest = new GetScoresRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mUserScores = (MyScores) data;
					setUserScores(mUserScores);
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.GetScoresServerRequest(mUserGUID);
	}

	/** Get verify Scores API request*/
	private void verifyScoresRequest(String verificationType, final boolean flag){
		VerifyScoresRequest mRequest = new VerifyScoresRequest(mContext);

		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					if(flag){
						DialogUtil.showOkDialog(mContext,getString(R.string.transcripts_req_msg),"");
					}
					else{
						DialogUtil.showOkDialog(mContext,getString(R.string.score_req_msg),"");
					//	mErrorLayout.showError(data.toString(), true,MsgType.Success);
					}
					getScoresRequest();
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.getVerifyScoresRequest(verificationType);
	}

	/** Get Update Scores API request*/
	private void updateScoresRequest(String verificationType,String mScore){
		UpdateScoresRequest mRequest = new UpdateScoresRequest(mContext);

		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mErrorLayout.showError(data.toString(), true,MsgType.Success);
					getScoresRequest();
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.getUpdateScoresRequest(verificationType,mScore);
	}


	/** Coach request to athlete for transcripts*/
	private void coachTranscriptsRequest(String mAthleteUserGUID){
		CoachTranscriptsRequest mRequest = new CoachTranscriptsRequest(mContext);
		
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					DialogUtil.showOkDialogButtonLisnter(mContext, getResources().getString(R.string.verification_response_email), getResources().getString(R.string.app_name), new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

						}
					});
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.getCoachTranscriptRequest(mAthleteUserGUID);
	}

	private class ViewHolder{

		public FloatLabel mGPAFL,mActFL,mSatFL,mTranscriptsFL;
		public EditText mActScoreET,mGPAScoreET,mSatScoreET,mTranscriptsET;
		public TextView mActUpdateTV,mGPAUpdateTV,mSatUpdateTV,mTranscriptsUpdateTV;
		public ImageButton mActEditIB,mGPAEditIB,mSatEditIB;
		private TextView mSATResendTV,mACTResendTV,mGPAResendTV,mResendTranscriptsTV;

		public ViewHolder(View view,OnClickListener listener){
			//	mActScoreET = (EditText) view.findViewById(R.id.act_et);
			mActFL = (FloatLabel) view.findViewById(R.id.act_et);
			mGPAFL = (FloatLabel) view.findViewById(R.id.gpa_et);
			mSatFL = (FloatLabel) view.findViewById(R.id.sat_et);
			mTranscriptsFL = (FloatLabel) view.findViewById(R.id.transcripts_et);

			mActScoreET = mActFL.getEditText();
			mGPAScoreET = mGPAFL.getEditText();
			mSatScoreET = mSatFL.getEditText();
			mTranscriptsET = mTranscriptsFL.getEditText();
			mTranscriptsET.setEnabled(false);
			

			mActUpdateTV = (TextView) view.findViewById(R.id.add_act_tv);
			mGPAUpdateTV = (TextView) view.findViewById(R.id.add_gpa_tv);
			mSatUpdateTV = (TextView) view.findViewById(R.id.add_sat_tv);
			mTranscriptsUpdateTV = (TextView) view.findViewById(R.id.upload_transcripts_tv);

			mActEditIB  = (ImageButton) view.findViewById(R.id.edit_act_ib);
			mGPAEditIB  = (ImageButton) view.findViewById(R.id.edit_gpa_ib);
			mSatEditIB = (ImageButton) view.findViewById(R.id.edit_sat_ib);

			mSATResendTV = (TextView)view.findViewById(R.id.resend_sat_tv);
			mACTResendTV = (TextView)view.findViewById(R.id.resend_act_tv);
			mGPAResendTV = (TextView)view.findViewById(R.id.resend_gpa_tv);
			mResendTranscriptsTV = (TextView)view.findViewById(R.id.resend_transcripts_tv);

			mSATResendTV.setOnClickListener(listener);
			mACTResendTV.setOnClickListener(listener);
			mGPAResendTV.setOnClickListener(listener);
			mResendTranscriptsTV.setOnClickListener(listener);

			mActEditIB.setOnClickListener(listener);
			mGPAEditIB.setOnClickListener(listener);
			mSatEditIB.setOnClickListener(listener);

			mActUpdateTV.setOnClickListener(listener);
			mGPAUpdateTV.setOnClickListener(listener);
			mSatUpdateTV.setOnClickListener(listener);
			mTranscriptsUpdateTV.setOnClickListener(listener);

			FontLoader.setRobotoRegularTypeface(mActScoreET, mGPAScoreET, mSatScoreET, mTranscriptsET,
					mActUpdateTV, mGPAUpdateTV, mSatUpdateTV, mTranscriptsUpdateTV,
					mSATResendTV,mACTResendTV,mGPAResendTV);

			mActScoreET.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (s.length() > 0) {
						mViewHolder.mActEditIB.setImageResource(R.drawable.icon_tick_black_normal);
						mViewHolder.mActEditIB.setClickable(true);
					} else {
						mViewHolder.mActEditIB.setImageResource(R.drawable.icon_tick_black_disable);
						mViewHolder.mActEditIB.setClickable(false);
					}
				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});

			mGPAScoreET.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (s.length() > 0) {
						mViewHolder.mGPAEditIB.setImageResource(R.drawable.icon_tick_black_normal);
						mViewHolder.mGPAEditIB.setClickable(true);
					} else {
						mViewHolder.mGPAEditIB.setImageResource(R.drawable.icon_tick_black_disable);
						mViewHolder.mGPAEditIB.setClickable(false);
					}
				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});

			mSatScoreET.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (s.length() > 0) {
						mViewHolder.mSatEditIB.setImageResource(R.drawable.icon_tick_black_normal);
						mViewHolder.mSatEditIB.setClickable(true);
					} else {
						mViewHolder.mSatEditIB.setImageResource(R.drawable.icon_tick_black_disable);
						mViewHolder.mSatEditIB.setClickable(false);
					}
				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});
		}
	}

	/**Filter for restrict value in a predefined rage*/
	InputFilter filter = new InputFilter() {
		final int maxDigitsBeforeDecimalPoint = 1;
		final int maxDigitsAfterDecimalPoint = 2;

		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			StringBuilder builder = new StringBuilder(dest);
			builder.replace(dstart, dend, source.subSequence(start, end).toString());
			if (!builder.toString().matches(
					"(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?")) {
				if (source.length() == 0)
					return dest.subSequence(dstart, dend);
				return "";
			}

			return null;
		}
	};

	Calendar cal = Calendar.getInstance();
	public Calendar getCalTime(String dateString) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			cal.setTime(sdf.parse(dateString));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TimeZone tz = cal.getTimeZone();
		int Offset = tz.getOffset(cal.getTimeInMillis());
		cal.setTimeInMillis(cal.getTimeInMillis() + Offset);

		return cal;
	}

	private void toggleSoftInputMode(){
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}
}