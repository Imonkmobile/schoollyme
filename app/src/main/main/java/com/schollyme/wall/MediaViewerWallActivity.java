package com.schollyme.wall;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.SharePostActivity;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.ErrorLayoutListener;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.media.MediaViewerActivity;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.AlbumCreateRequest;
import com.vinfotech.request.AlbumDetailRequest;
import com.vinfotech.request.AlbumListMediaRequest;
import com.vinfotech.request.AlbumMediaDeleteRequest;
import com.vinfotech.request.AlbumSetCoverRequest;
import com.vinfotech.request.EntityViewRequest;
import com.vinfotech.request.GetMediaDetailRequest;
import com.vinfotech.request.LikerListRequest;
import com.vinfotech.request.MarkSpamRequest;
import com.vinfotech.request.MediaDetailRequest;
import com.vinfotech.request.MediaUpdateCaptionRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.DialogUtil.SingleEditListener;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

public class MediaViewerWallActivity extends BaseActivity {
	private static final String TAG = MediaViewerWallActivity.class.getSimpleName();
	public static final int REQ_CODE_MEDIA_VIEWER_ACTIVITY = 1013;

	private ErrorLayout mErrorLayout;
	private MediaDetailRequest mMediaDetailRequest;
	private AlbumMediaDeleteRequest mMediaDeleteRequest;
	private MediaUpdateCaptionRequest mMediaEditRequest;
	private AlbumSetCoverRequest mAlbumSetCoverRequest;
	private MarkSpamRequest mMarkSpamRequest;
	private ArrayList<AlbumMedia> mAlbumMediasFromServer = new ArrayList<AlbumMedia>();
	private LogedInUserModel mLogedInUserModel;

	private VHolder mVHolder;
	private boolean isFromBlog;
	private ArrayList<AlbumMedia> mAlbumMedias;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.media_viewer_activity);

		mLogedInUserModel = new LogedInUserModel(this);
		SchollyMeApplication sma = (SchollyMeApplication)getApplication();
		sma.pauseSong();

		mAlbumMediasFromServer.clear();
		getIntentData();
		initlization();
		if (isFromBlog && null != mAlbumMedias) {
			mAlbumMediasFromServer = mAlbumMedias;
			mViewPagerAdapter.notifyDataSetChanged();
			mViewPagerAdapter.setCurrentItem();
		} else {
			if (null == mAlbum) {
				getAlbumDetails();
			} else {
				initMedia();
			}
		}
	}

	private void initMedia() {
		setupRequestListners();
		if (IsSingle) {
			getSingleMediaDetail();
		} else {
			getListFromServer();
		}
	}

	public void getAlbumDetails() {
		AlbumDetailRequest albumDetailRequest = new AlbumDetailRequest(this);
		albumDetailRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success && null != data) {
					mAlbum = (Album) data;
					initMedia();
				} else {
					Toast.makeText(MediaViewerWallActivity.this, R.string.Something_went_wrong, Toast.LENGTH_SHORT).show();
				}
			}
		});
		albumDetailRequest.getAlbumDetailFromServer(getIntent().getStringExtra("AlbumGUID"));
	};

	private int SelectedPosition = 0;
	private Album mAlbum = null;
	private boolean IsSingle;
	private String mMediaGuid;

	private void getSingleMediaDetail() {
		GetMediaDetailRequest mGetMediaDetailRequest = new GetMediaDetailRequest(this);
		mGetMediaDetailRequest.setLoader(findViewById(R.id.loading_pb));
		mGetMediaDetailRequest.getMediaDetailFromServer(mAlbum.AlbumGUID, mMediaGuid);
		mGetMediaDetailRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					if (null != data) {

						mAlbumMediasFromServer.addAll((List<AlbumMedia>) data);

						mViewPagerAdapter.notifyDataSetChanged();
						mViewPagerAdapter.setCurrentItem();
					}

				} else if (null != data) {
					DialogUtil.showOkDialog(MediaViewerWallActivity.this, (String) data, "");
				}
			}
		});
	}

	// private boolean mFetchedAll = true;

	private void getListFromServer() {
		AlbumListMediaRequest mAlbumListMediaRequest = new AlbumListMediaRequest(this);

		mAlbumListMediaRequest.getAlbumMediaListFromServer(mLogedInUserModel.mUserGUID, mAlbum.AlbumGUID, 1,
				AlbumDetailRequest.SORT_BY_CREATEDDATE, AlbumDetailRequest.ORDER_BY_CREATEDDATE);
		mAlbumListMediaRequest.setLoader(findViewById(R.id.loading_pb));
		mAlbumListMediaRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					if (null != data) {

						mAlbumMediasFromServer.addAll((List<AlbumMedia>) data);
						mViewPagerAdapter.notifyDataSetChanged();
						mViewPagerAdapter.setCurrentItem();
					}

				} else if (null != data) {
					DialogUtil.showOkDialog(MediaViewerWallActivity.this, (String) data, "");
				}
			}
		});
	}

	private void initlization() {
		mErrorLayout = new ErrorLayout(findViewById(R.id.error_layout));
		mMediaDeleteRequest = new AlbumMediaDeleteRequest(this);
		mMediaDetailRequest = new MediaDetailRequest(this);
		mMediaDetailRequest.setLoader(findViewById(R.id.loading_pb));
		mMarkSpamRequest = new MarkSpamRequest(this);
		mToggleLikeRequest = new ToggleLikeRequest(this);
		mMediaEditRequest = new MediaUpdateCaptionRequest(this);
		mAlbumSetCoverRequest = new AlbumSetCoverRequest(this);
		mViewPagerAdapter = new ViewPagerAdapter();

		mVHolder = new VHolder(findViewById(R.id.main_container_rl));
	}

	ViewPagerAdapter mViewPagerAdapter;

	class VHolder {
		private ViewPager mViewPager;
		TextView mDoneTv;

		public VHolder(View view) {
			mViewPager = (ViewPager) view.findViewById(R.id.myfivepanelpager);

			mViewPager.setAdapter(mViewPagerAdapter);
			mDoneTv = (TextView) view.findViewById(R.id.done_tv);
			FontLoader.setRobotoRegularTypeface(mDoneTv);
			mViewPager.setCurrentItem(SelectedPosition);
			mDoneTv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		}
	}

	public void setupRequestListners() {
		mMarkSpamRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mErrorLayout.showError("Media flagged in-appropriate.", true, MsgType.Success);
				} else if (null != data) {
					DialogUtil.showOkDialog(MediaViewerWallActivity.this, (String) data, "");
				}
			}
		});

		mMediaEditRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					setResult(RESULT_OK);
					final String newCaption = (String) data;

					mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).Caption = newCaption;
					mErrorLayout.showError("Media caption updated successfully.", true, MsgType.Success);
					if (null != mOnActionDescTv) {
						mOnActionDescTv.setText(newCaption);
					}
				} else if (null != data) {
					DialogUtil.showOkDialog(MediaViewerWallActivity.this, (String) data, "");
				}
			}
		});

		mMediaDetailRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					AlbumMedia mMedia = (AlbumMedia) data;

					mAlbumMediasFromServer.set(mVHolder.mViewPager.getCurrentItem(), mMedia);
					mViewPagerAdapter.notifyDataSetChanged();

				} else if (null != data) {
					DialogUtil.showOkDialog(MediaViewerWallActivity.this, (String) data, "");
				}
			}
		});

		mMediaDeleteRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {

						@Override
						public void onErrorShown() {
						}

						@Override
						public void onErrorHidden() {
							setResult(RESULT_OK);
							finish();
						}
					});
					mErrorLayout.showError("Media deleted successfully.", true, MsgType.Success);
				} else if (null != data) {
					DialogUtil.showOkDialog(MediaViewerWallActivity.this, (String) data, "");
				}
			}
		});

		mAlbumSetCoverRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mErrorLayout.setErrorLayoutListener(null);
					mErrorLayout.showError("Album cover updated successfully.", true, MsgType.Success);
				} else if (null != data) {
					DialogUtil.showOkDialog(MediaViewerWallActivity.this, (String) data, "");
				}
			}
		});
	}

	private void getIntentData() {

		SelectedPosition = getIntent().getIntExtra("SelectedPosition", 0);
		mAlbum = getIntent().getParcelableExtra("album");
		IsSingle = getIntent().getBooleanExtra("IsSingle", true);
		mMediaGuid = getIntent().getStringExtra("MediaGuid");
		isFromBlog = getIntent().getBooleanExtra("isfromBlog", false);
		mAlbumMedias = getIntent().getParcelableArrayListExtra("albumMedias");
	}

	public static Intent getIntent(Context context, int SelectedPosition, Album album, boolean IsSingle, String MediaGuid) {
		Intent intent = new Intent(context, MediaViewerWallActivity.class);
		intent.putExtra("SelectedPosition", SelectedPosition);
		intent.putExtra("album", album);
		intent.putExtra("IsSingle", IsSingle);
		intent.putExtra("MediaGuid", MediaGuid);
		return intent;
	}

	public static Intent getIntent(Context context, int SelectedPosition, Album album, boolean IsSingle, String MediaGuid,
			boolean isfromBlog, ArrayList<AlbumMedia> albumMedias) {
		Intent intent = new Intent(context, MediaViewerWallActivity.class);
		intent.putExtra("SelectedPosition", SelectedPosition);
		intent.putExtra("album", album);
		intent.putExtra("IsSingle", IsSingle);
		intent.putExtra("MediaGuid", MediaGuid);
		intent.putExtra("isfromBlog", isfromBlog);
		intent.putParcelableArrayListExtra("albumMedias", albumMedias);
		return intent;
	}

	public static Intent getIntent(Context context, String MediaGuid, String AlbumGUID) {
		Intent intent = new Intent(context, MediaViewerWallActivity.class);
		intent.putExtra("SelectedPosition", 0);
		intent.putExtra("AlbumGUID", AlbumGUID);
		intent.putExtra("IsSingle", true);
		intent.putExtra("MediaGuid", MediaGuid);
		return intent;
	}

	private TextView mOnActionDescTv = null;

	public void onActionClick(final AlbumMedia mMedia, TextView descTv) {
		this.mOnActionDescTv = descTv;
		final boolean isOwnder = (mAlbum.IsEditable == 1);
		final boolean isVideo = AlbumCreateRequest.ALBUMTYPE_VIDEO.equalsIgnoreCase(mMedia.MediaType)
				|| AlbumCreateRequest.ALBUMTYPE_YOUTUBE.equalsIgnoreCase(mMedia.MediaType);
		String[] items;
		if (isOwnder && mMedia.IsCoverMedia == 0) {
			items = new String[3];
			items[0] = getString(R.string.Set_as_Album_Cover);
			items[1] = getString(R.string.Edit_Caption);
			items[2] = getString(isVideo ? R.string.Delete_Video : R.string.Delete_Photo);
		} else if (isOwnder && mMedia.IsCoverMedia == 1) {
			items = new String[2];
			// items[0] = getString(R.string.Set_as_Album_Cover);
			items[0] = getString(R.string.Edit_Caption);
			items[1] = getString(isVideo ? R.string.Delete_Video : R.string.Delete_Photo);
		} else {
			items = new String[1];
			items[0] = getString(R.string.Report);
		}

		DialogUtil.showListDialog(this, 0, new OnItemClickListener() {

			@Override
			public void onItemClick(int position, String item) {

				if (isOwnder && mMedia.IsCoverMedia == 0) {

					switch (position) {
					case 0:
						mAlbumSetCoverRequest.setCoverMediaInServer(mAlbum.AlbumGUID, mMedia.MediaGUID);
						break;
					case 1:
						DialogUtil.showSingleEditDialog(MediaViewerWallActivity.this, R.string.Write_caption, mMedia.Caption,
								new SingleEditListener() {

									@Override
									public void onEdit(boolean canceled, EditText editText, String text) {
										if (canceled) {
											Utility.hideSoftKeyboard(editText);
											mErrorLayout.showError("Edit media caption canceled!", true, MsgType.Success);
										} else {
											mMediaEditRequest.updateMediaFromServer(mAlbum.AlbumGUID, mMedia.MediaGUID, text);
										}
									}
								});
						break;
					case 2:
						DialogUtil.showOkCancelDialog(MediaViewerWallActivity.this, R.string.Delete,
								getString(isVideo ? R.string.Delete_Album : R.string.Delete_Photo),
								getString(isVideo ? R.string.Confirm_delete_Video : R.string.Confirm_delete_Photo), new OnClickListener() {

									@Override
									public void onClick(View v) {
										List<AlbumMedia> mAlbumMedias = new ArrayList<AlbumMedia>();
										mAlbumMedias.add(mMedia);
										mMediaDeleteRequest.deleteMediaFromServer(mAlbum.AlbumGUID, mAlbumMedias);
									}
								}, null);

						break;
					default:
						break;
					}

				} else if (isOwnder && mMedia.IsCoverMedia == 1) {
					switch (position) {
					case 0:
						DialogUtil.showSingleEditDialog(MediaViewerWallActivity.this, R.string.Write_caption, mMedia.Caption,
								new SingleEditListener() {

									@Override
									public void onEdit(boolean canceled, EditText editText, String text) {
										Utility.hideSoftKeyboard(editText);
										if (canceled) {
											mErrorLayout.showError("Edit media caption canceled!", true, MsgType.Success);
										} else {
											mMediaEditRequest.updateMediaFromServer(mAlbum.AlbumGUID, mMedia.MediaGUID, text);
										}
									}
								});

						break;
					case 1:
						DialogUtil.showOkCancelDialog(MediaViewerWallActivity.this, R.string.Delete,
								getString(isVideo ? R.string.Delete_Album : R.string.Delete_Photo),
								getString(isVideo ? R.string.Confirm_delete_Video : R.string.Confirm_delete_Photo), new OnClickListener() {

									@Override
									public void onClick(View v) {
										List<AlbumMedia> mAlbumMedias = new ArrayList<AlbumMedia>();
										mAlbumMedias.add(mMedia);
										mMediaDeleteRequest.deleteMediaFromServer(mAlbum.AlbumGUID, mAlbumMedias);
									}
								}, null);

						break;
					default:
						break;
					}

				} else {
					mMarkSpamRequest.showFlagDialog(Config.IMAGE_URL_UPLOAD + Config.ALBUM_FOLDER + mMedia.ImageName);
				}

			}

			@Override
			public void onCancel() {
			}
		}, items);
	}

	private class ViewPagerAdapter extends PagerAdapter {
		public ViewPagerAdapter() {

		}

		public void setCurrentItem() {
			mVHolder.mViewPager.setCurrentItem(SelectedPosition);
			notifyDataSetChanged();
		}

		protected void setViewCount(String mediaGUID, String entityTypeUser) {
			EntityViewRequest mEntityViewRequest = new EntityViewRequest(MediaViewerWallActivity.this);
			mEntityViewRequest.setViewAtServer(mediaGUID, entityTypeUser);
		}

		// @Override
		// public float getPageWidth(int position) {
		// return 0.8f;
		// }
		public int getCount() {
			return mAlbumMediasFromServer.size();
		}

		public Object instantiateItem(View collection, int position) {
			LayoutInflater layoutInflater = MediaViewerWallActivity.this.getLayoutInflater();
			final View view = layoutInflater.inflate(R.layout.media_view_items, null);
			final ViewHolder viewHolder = new ViewHolder(view);

			final AlbumMedia media = mAlbumMediasFromServer.get(position);
			if (null != media) {
				viewHolder.mDescriptionTv.setText(media.Caption);
				viewHolder.mViewCountTV.setText("" + media.ViewCount);
				updateCommentCount(viewHolder.mCommentsTv, media);
				updateLike(viewHolder.mLikeTv, media, viewHolder.mLikesTv);
				viewHolder.mPlayIv.setVisibility(View.GONE);
				if (!isFromBlog) {
					viewHolder.mShareTv.setVisibility(mAlbum.Visibility == AlbumCreateRequest.VISIBILITY_TEAMMATES ? View.GONE
							: View.VISIBLE);
				}

				if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(media.MediaType)) {
					viewHolder.mViewCountTV.setVisibility(View.GONE);
					if (isFromBlog) {
						media.tmpThumb = Config.getMediaPathBLOG(media.ImageName);
					} else {
						media.tmpThumb = Config.getCoverPath(mAlbum, media.ImageName, false, true);
					}
					ImageLoaderUniversal.ImageLoadSquareWithProgressBar(MediaViewerWallActivity.this, media.tmpThumb, viewHolder.mMediaIv,
							ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoadingPb);
				} else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(media.MediaType)) {

					viewHolder.mViewCountTV.setVisibility(View.VISIBLE);
					viewHolder.mLoadingPb.setVisibility(View.GONE);
					if (isFromBlog) {
						media.tmpThumb = Config.getBlogVideopath(media.ImageName).replaceAll("mp4", "jpg");
					} else {
						media.tmpThumb = Config.getCoverPath(mAlbum, Config.getVideoToJPG(media.ImageName), true);
					}

					// ImageLoaderUniversal.ImageLoadSquareWithProgressBar(MediaViewerWallActivity.this,
					// media.tmpThumb, viewHolder.mMediaIv,
					// ImageLoaderUniversal.DiaplayOptionForProgresser,
					// viewHolder.mLoadingPb);

					ImageLoaderUniversal.ImageLoadSquare(MediaViewerWallActivity.this, media.tmpThumb, viewHolder.mMediaIv,
							ImageLoaderUniversal.option_normal_Image_Thumbnail);
					viewHolder.mMediaIv.setEnabled(false);
					viewHolder.mPlayIv.setVisibility(View.VISIBLE);
					final VideoView videoView = viewHolder.mediaVv;
					videoView.setTag(viewHolder);
					viewHolder.mPlayIv.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							String mediaUrl = Config.VIDEO_URL_UPLOAD.replace("https:", "http:") + Config.ALBUM_FOLDER
									+ Config.VIDEO_FOLDER + media.ImageName;

							setViewCount(media.MediaGUID, EntityViewRequest.ENTITY_TYPE_MEDIA);
							prepareVideoDisplay(mediaUrl, videoView);
						}
					});
				} else if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(media.MediaType)) {
					viewHolder.mViewCountTV.setVisibility(View.VISIBLE);
					media.tmpThumb = getYoutubeVideoThumb(media.ImageName);
					ImageLoaderUniversal.ImageLoadSquareWithProgressBar(MediaViewerWallActivity.this, media.tmpThumb, viewHolder.mMediaIv,
							ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoadingPb);
					viewHolder.mMediaIv.setEnabled(false);
					viewHolder.mPlayIv.setVisibility(View.VISIBLE);
					viewHolder.mPlayIv.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							watchVideoAlternate(media.ImageName);
						}
					});
				}
			}
			viewHolder.mLikeTv.setTag(viewHolder);
			viewHolder.mLikeTv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					ViewHolder vh = (ViewHolder) v.getTag();
					mOnActionLikeTV = (TextView) v;
					mOnActionLikesCount = vh.mLikesTv;
					DisplayUtil.bounceView(view.getContext(), viewHolder.mLikeTv);
					LikeMediaToggleService(media, mOnActionLikeTV);
				}
			});

			viewHolder.mActionIB.setTag(viewHolder.mDescriptionTv);
			viewHolder.mActionIB.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onActionClick(media, (TextView) v.getTag());
				}
			});
			viewHolder.mCommentTv.setTag(viewHolder);
			viewHolder.mCommentTv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					ViewHolder vh = (ViewHolder) v.getTag();
					mOnActionCommentTv = vh.mCommentsTv;
					startActivityForResult(CommentListActivity.getIntent(MediaViewerWallActivity.this, media, "MEDIA", media.Caption),
							REQ_CODE_MEDIA_VIEWER_ACTIVITY);
					MediaViewerWallActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

				}
			});

			viewHolder.mCommentsTv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mOnActionCommentTv = (TextView) v;
					startActivityForResult(CommentListActivity.getIntent(MediaViewerWallActivity.this, media, "MEDIA", media.Caption),
							REQ_CODE_MEDIA_VIEWER_ACTIVITY);
					MediaViewerWallActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

				}
			});
			viewHolder.mLikesTv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mOnActionLikeTV = viewHolder.mLikeTv;
					mOnActionLikesCount = (TextView) v;
					startActivityForResult(LikeListActivity.getIntent(MediaViewerWallActivity.this, media.MediaGUID,
							LikerListRequest.ENTITYTYPE_MEDIA, "MEDIAGRID"), LikeListActivity.REQ_CODE_LIKE_ACTIVITY_WALL);
					MediaViewerWallActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
				}
			});

			viewHolder.mShareTv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String mediaUrl = (TextUtils.isEmpty(media.tmpThumb) ? "" : media.tmpThumb);

					// startActivity(SharePostActivity.getIntent(v.getContext(),
					// media.MediaGUID, SharePostRequest.ENTITYTYPE_MEDIA,
					// mLogedInUserModel.mUserGUID, mediaUrl, media.Caption));

					DialogUtil.showOkCancelDialog(v.getContext(), "", v.getContext().getString(R.string.Are_share_this_media),
							new OnClickListener() {

								@Override
								public void onClick(View v) {
									sharePost(media.MediaGUID, SharePostRequest.ENTITYTYPE_MEDIA, mLogedInUserModel.mUserGUID);
								}
							}, null);

				}
			});

			if (isFromBlog) {
				viewHolder.mBottomRL.setVisibility(View.GONE);
				viewHolder.mViewCountTV.setVisibility(View.GONE);

			}
			((ViewPager) collection).addView(view, 0);
			return view;
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {

		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == ((View) arg1);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		private void updateCommentCount(TextView textView, AlbumMedia media) {
			textView.setText(media.NoOfComments + " "
					+ (textView.getResources().getString(1 == media.NoOfComments ? R.string.Comment : R.string.Comments)));
		}

		private void updateLike(TextView textView, AlbumMedia media, TextView mLikeCount) {
			textView.setSelected(media.IsLike == 0 ? false : true);
			mLikeCount.setText(media.NoOfLikes + (media.NoOfLikes == 1 ? " Like" : " Likes"));
		}

		private class ViewHolder {
			private TextView mDescriptionTv, mLikesTv, mCommentsTv, mLikeTv, mViewCountTV, mCommentTv, mShareTv;
			private ImageView mPlayIv;
			private ImageButton mActionIB;
			private ImageView mMediaIv;
			private VideoView mediaVv;
			private ProgressBar mLoadingPb;
			private RelativeLayout mBottomRL;

			public ViewHolder(View view) {
				mDescriptionTv = (TextView) view.findViewById(R.id.description_tv);
				mLikesTv = (TextView) view.findViewById(R.id.likes_tv);
				mCommentsTv = (TextView) view.findViewById(R.id.comments_tv);
				mLikeTv = (TextView) view.findViewById(R.id.like_tv);
				mCommentTv = (TextView) view.findViewById(R.id.comment_tv);
				mShareTv = (TextView) view.findViewById(R.id.share_tv);
				mMediaIv = (ImageView) view.findViewById(R.id.media_iv);
				mViewCountTV = (TextView) view.findViewById(R.id.view_count_tv);
				mPlayIv = (ImageView) view.findViewById(R.id.play_iv);
				mActionIB = (ImageButton) view.findViewById(R.id.action_ib);
				mLoadingPb = (ProgressBar) view.findViewById(R.id.loading_pb);
				mediaVv = (VideoView) view.findViewById(R.id.media_vv);
				mBottomRL = (RelativeLayout) view.findViewById(R.id.bottom_rl);
				mActionIB.setVisibility(View.GONE);
				FontLoader.setRobotoRegularTypeface(mDescriptionTv, mLikesTv, mCommentsTv, mLikeTv, mCommentTv, mShareTv);
			}
		}
	}

	private int mVisibility = WallPostCreateRequest.VISIBILITY_PUBLIC;
	private int mCommentable = WallPostCreateRequest.COMMENT_ON;
	private SharePostRequest mSharePostRequest;

	public void sharePost(String mEntityGUID, String mEntityType, String mModuleEntityGUID) {
		mSharePostRequest = new SharePostRequest(MediaViewerWallActivity.this);
		mSharePostRequest.sharePostInServer(mEntityGUID, mEntityType, "", SharePostRequest.MODULE_ID_USERS, mModuleEntityGUID, mVisibility,
				mCommentable);

		mSharePostRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, final Object data, int totalRecords) {
				if (success) {
					mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {

						@Override
						public void onErrorShown() {
						}

						@Override
						public void onErrorHidden() {

						}
					});
					mErrorLayout.showError(MediaViewerWallActivity.this.getResources().getString(R.string.Media_shared_successfully), true,
							MsgType.Success);
				} else {
					mErrorLayout.showError((String) data, true, MsgType.Error);
				}
			}
		});
	}

	final int REQ_CODE_COMMENT_LIST = 656;
	final String MARKER_mov = "mov";
	final String MARKER_MOV = "MOV";
	final String NEW_MARKER = "mp4";

	private void prepareVideoDisplay(String urlVideo_, final VideoView mediaVv) {
		String urlVideo = urlVideo_;
		if (Config.DEBUG) {
			Log.d(TAG, "prepareVideoDisplay url=" + urlVideo + ", mediaVv=" + mediaVv);
		}
		urlVideo.trim();

		if (urlVideo.contains(MARKER_MOV)) {
			urlVideo = urlVideo.replaceAll(MARKER_MOV, NEW_MARKER);
		}
		if (urlVideo.contains(MARKER_mov)) {
			urlVideo = urlVideo.replaceAll(MARKER_mov, NEW_MARKER);
		}

		if (TextUtils.isEmpty(urlVideo) || !android.util.Patterns.WEB_URL.matcher(urlVideo).matches()) {
			Toast.makeText(this, "Invalid media URL: " + urlVideo, Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		final String url = urlVideo;
		final com.schollyme.wall.MediaViewerWallActivity.ViewPagerAdapter.ViewHolder viewHolder = (com.schollyme.wall.MediaViewerWallActivity.ViewPagerAdapter.ViewHolder) mediaVv
				.getTag();
		mediaVv.setVisibility(View.VISIBLE);
		viewHolder.mPlayIv.setVisibility(View.GONE);
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		final ProgressDialog progressDialog = DialogUtil.createDiloag(MediaViewerWallActivity.this, "Buffering...");
		progressDialog.show();
		try {
			MediaController mediacontroller = new MediaController(MediaViewerWallActivity.this);
			mediacontroller.setAnchorView(mediaVv);
			mediacontroller.setMediaPlayer(mediaVv);
			mediaVv.setMediaController(mediacontroller);
			mediaVv.setVideoURI(Uri.parse(url));
		} catch (Exception e) {
			e.printStackTrace();
			if (null != progressDialog && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

		progressDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				viewHolder.mPlayIv.setVisibility(View.VISIBLE);
			}
		});
		mediaVv.requestFocus();
		mediaVv.setOnPreparedListener(new OnPreparedListener() {
			public void onPrepared(MediaPlayer mp) {
				if (null != progressDialog && progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				viewHolder.mPlayIv.setVisibility(View.GONE);
				viewHolder.mMediaIv.setVisibility(View.GONE);
				mediaVv.start();
			}
		});
		mediaVv.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				viewHolder.mMediaIv.setVisibility(View.VISIBLE);
				viewHolder.mPlayIv.setVisibility(View.VISIBLE);
				viewHolder.mPlayIv.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (!mediaVv.isPlaying()) {
							viewHolder.mPlayIv.setVisibility(View.GONE);
							viewHolder.mMediaIv.setVisibility(View.GONE);
							mediaVv.start();
						}
					}
				});
			}
		});
		mediaVv.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				if (null != progressDialog && progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				viewHolder.mPlayIv.setVisibility(View.VISIBLE);
				DialogUtil.showOkCancelDialog(MediaViewerWallActivity.this, null,
						"Sorry, this video can not be played in this player. Would you like to launch alternate player?",
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								watchVideoAlternate(url);
							}
						}, null);
				return true;
			}
		});
	}

	private ToggleLikeRequest mToggleLikeRequest;

	public void LikeMediaToggleService(AlbumMedia mMedia, final TextView mLike) {
		mToggleLikeRequest.toggleLikeInServer(mMedia.MediaGUID, ToggleLikeRequest.ENTITY_TYPE_MEDIA);
		mToggleLikeRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					if (mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).IsLike == 0) {
						mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).IsLike = 1;
						mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).NoOfLikes++;
					} else {
						mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).IsLike = 0;
						mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).NoOfLikes--;
					}
					mViewPagerAdapter.updateLike(mOnActionLikeTV, mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()),
							mOnActionLikesCount);
				} else {
					DialogUtil.showOkDialog(MediaViewerWallActivity.this, data.toString(), "");
				}

			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
		case REQ_CODE_MEDIA_VIEWER_ACTIVITY:
			if (resultCode == RESULT_OK) {

				// this.setResult(RESULT_OK);

				Intent in = new Intent();
				in.putExtra("AmbumMedias", mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()));
				setResult(RESULT_OK, in);

			}
			break;
		case LikeListActivity.REQ_CODE_LIKE_ACTIVITY_WALL:
			if (resultCode == RESULT_OK) {

				if (mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).IsLike == 0) {
					mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).IsLike = 1;
					mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).NoOfLikes++;
				} else {
					mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).IsLike = 0;
					mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()).NoOfLikes--;
				}
				mViewPagerAdapter.updateLike(mOnActionLikeTV, mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()),
						mOnActionLikesCount);

				Intent in = new Intent();
				in.putExtra("AmbumMedias", mAlbumMediasFromServer.get(mVHolder.mViewPager.getCurrentItem()));
				setResult(RESULT_OK, in);

			}
			break;

		default:
			break;
		}

		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	public void onBackPressed() {

		Intent in = new Intent();
		MediaViewerWallActivity.this.setResult(RESULT_OK, in);
		finish();
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

	public void watchVideoAlternate(String url) {
		if (Config.DEBUG) {
			Log.d(TAG, "watchVideoAlternate url=" + url);
		}
		if (isYouTubeVideo(url)) {
			String id = getYouTubeVideoId(url);
			try {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
				startActivity(intent);
			} catch (ActivityNotFoundException ex) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubePrefix2 + id));
				startActivity(intent);
			}
		} else {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.parse(url), "video/mp4");
			Intent chooser = Intent.createChooser(intent, "Complete action using...");
			if (intent.resolveActivity(getPackageManager()) != null) {
				startActivity(chooser);
			}
		}
	}

	public static final String youtubePrefix1 = "http://youtu.be/";
	public static final String youtubePrefix2 = "https://www.youtube.com/watch?v=";

	public static boolean isYouTubeVideo(String url) {
		return (url.startsWith(youtubePrefix1) || url.startsWith(youtubePrefix2));
	}

	public static String getYouTubeVideoId(String url) {
		String id = url.replaceAll(youtubePrefix1, "").replaceAll(youtubePrefix2, "");
		int eIndex = id.indexOf("=");
		if (eIndex != -1) {
			id = id.substring(eIndex + 1);
		}
		return id;
	}

	public static String getYoutubeVideoThumb(String url) {
		String vidId = getYouTubeVideoId(url);
		String thumb = "http://img.youtube.com/vi/" + vidId + "/default.jpg";
		if (Config.DEBUG) {
			Log.d(TAG, "getYoutubeVideoThumb thumb=" + thumb);
		}
		return thumb;
	}

	@Override
	protected void onStart() {
		mMediaDetailRequest.setActivityStatus(true);
		this.registerReceiver(mCommentBroadcastReceiver, getIntenFilter());
		super.onStart();
	}

	@Override
	protected void onStop() {
		this.unregisterReceiver(mCommentBroadcastReceiver);
		this.mMediaDetailRequest.setActivityStatus(false);
		super.onStop();
	}

	private TextView mOnActionCommentTv = null;
	private TextView mOnActionLikeTV = null;
	private TextView mOnActionLikesCount = null;
	private CommentBroadcastReceiver mCommentBroadcastReceiver = new CommentBroadcastReceiver();

	private class CommentBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			AlbumMedia media = intent.getParcelableExtra("media");
			Intent in = new Intent();
			MediaViewerWallActivity.this.setResult(RESULT_OK, in);

			if (null != media) {
				for (AlbumMedia albumMedia : mAlbumMediasFromServer) {
					if (media.MediaGUID.equalsIgnoreCase(albumMedia.MediaGUID)) {
						albumMedia.NoOfComments = media.NoOfComments;
						if (null != mOnActionCommentTv) {
							mViewPagerAdapter.updateCommentCount(mOnActionCommentTv, media);

						}
						break;
					}
				}
			}
		}
	}

	private IntentFilter getIntenFilter() {
		IntentFilter intentFilter = new IntentFilter("COMMENTADD");
		intentFilter.addAction("WriteComment");
		return intentFilter;
	}
}
