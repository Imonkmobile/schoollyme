package com.schollyme.wall;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.media.MediaViewerActivity;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class SingleMediaViewWallActivity extends BaseActivity implements OnClickListener {

	String mediaType;
	String mediaName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.media_view_items);

		SchollyMeApplication sma = (SchollyMeApplication)getApplication();
		sma.pauseSong();

		initialization();
		getandSetData();
	}

	boolean IsWall;

	private void getandSetData() {
		mediaType = getIntent().getStringExtra("mediaType");
		mediaName = getIntent().getStringExtra("mediaName");
		IsWall = getIntent().getBooleanExtra("IsWall", true);
		String baseURL;

		if (IsWall) {
			if (mediaType.equals("VIDEO")) {
				mVHolder.mPlayIv.setVisibility(View.VISIBLE);
				mVHolder.mediaVv.setVisibility(View.GONE);
				baseURL = Config.VIDEO_URL_UPLOAD + Config.WALL_FOLDER + Config.VIDEO_FOLDER;
				String thumb = baseURL + mediaName.replaceAll("mp4", "jpg");
				ImageLoaderUniversal.ImageLoadSquareWithProgressBar(SingleMediaViewWallActivity.this, thumb, mVHolder.mMediaIv,
						ImageLoaderUniversal.DiaplayOptionForProgresser, mVHolder.mLoadingPb);
			} else {
				mVHolder.mediaVv.setVisibility(View.GONE);
				mVHolder.mPlayIv.setVisibility(View.GONE);
				baseURL = Config.IMAGE_URL_UPLOAD + Config.WALL_FOLDER;
				String thumb = baseURL + mediaName;
				ImageLoaderUniversal.ImageLoadSquareWithProgressBar(SingleMediaViewWallActivity.this, thumb, mVHolder.mMediaIv,
						ImageLoaderUniversal.DiaplayOptionForProgresser, mVHolder.mLoadingPb);
			}
		} else {

			mVHolder.mediaVv.setVisibility(View.GONE);
			mVHolder.mPlayIv.setVisibility(View.GONE);

			String imagePath = Config.getCoverMediaPathBLOG(mediaName);
			ImageLoaderUniversal.ImageLoadSquareWithProgressBar(SingleMediaViewWallActivity.this, imagePath, mVHolder.mMediaIv,
					ImageLoaderUniversal.DiaplayOptionForProgresser, mVHolder.mLoadingPb);

		}

	}

	VHolder mVHolder;

	private void initialization() {
		mVHolder = new VHolder(findViewById(R.id.container_ll), this);

	}

	private class VHolder {

		private ImageView mPlayIv;

		private ImageView mMediaIv;
		private VideoView mediaVv;
		private ProgressBar mLoadingPb;
		private RelativeLayout bottom_rl, top_rl;
		private TextView mDoneTV, mViewCount;

		public VHolder(View view, OnClickListener listener) {

			mMediaIv = (ImageView) view.findViewById(R.id.media_iv);
			mPlayIv = (ImageView) view.findViewById(R.id.play_iv);
			mLoadingPb = (ProgressBar) view.findViewById(R.id.loading_pb);
			mediaVv = (VideoView) view.findViewById(R.id.media_vv);
			bottom_rl = (RelativeLayout) view.findViewById(R.id.bottom_rl);
			top_rl = (RelativeLayout) view.findViewById(R.id.top_rl);
			mViewCount = (TextView) view.findViewById(R.id.view_count_tv);
			mDoneTV = (TextView) view.findViewById(R.id.done_tv);
			top_rl.bringToFront();
			top_rl.setVisibility(View.VISIBLE);
			bottom_rl.setVisibility(View.GONE);
			mDoneTV.setOnClickListener(listener);
			mPlayIv.setOnClickListener(listener);
			mViewCount.setVisibility(View.GONE);
			FontLoader.setRobotoRegularTypeface(mDoneTV);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.play_iv:
			String mediaUrl = Config.VIDEO_URL_UPLOAD.replace("https:", "http:") + Config.WALL_FOLDER + Config.VIDEO_FOLDER + mediaName;
			prepareVideoDisplay(mediaUrl, mVHolder.mediaVv);
			break;

		case R.id.done_tv:
			this.finish();
			this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;

		default:
			break;
		}
	}

	private void prepareVideoDisplay(final String url, final VideoView mediaVv) {
		if (TextUtils.isEmpty(url) || !android.util.Patterns.WEB_URL.matcher(url).matches()) {
			Toast.makeText(this, "Invalid media URL: " + url, Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		mediaVv.setVisibility(View.VISIBLE);
		mVHolder.mPlayIv.setVisibility(View.GONE);
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		final ProgressDialog progressDialog = DialogUtil.createDiloag(SingleMediaViewWallActivity.this, "Buffering...");
		progressDialog.show();
		try {
			MediaController mediacontroller = new MediaController(SingleMediaViewWallActivity.this);
			mediacontroller.setAnchorView(mediaVv);
			mediacontroller.setMediaPlayer(mediaVv);
			mediaVv.setMediaController(mediacontroller);
			mediaVv.setVideoURI(Uri.parse(url));
		} catch (Exception e) {
			e.printStackTrace();
			if (null != progressDialog && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

		mediaVv.requestFocus();
		mediaVv.setOnPreparedListener(new OnPreparedListener() {
			public void onPrepared(MediaPlayer mp) {
				if (null != progressDialog && progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				mVHolder.mPlayIv.setVisibility(View.GONE);
				mVHolder.mMediaIv.setVisibility(View.GONE);
				mediaVv.start();
			}
		});
		mediaVv.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mVHolder.mMediaIv.setVisibility(View.VISIBLE);
				mVHolder.mPlayIv.setVisibility(View.VISIBLE);
				mVHolder.mPlayIv.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (!mediaVv.isPlaying()) {
							mVHolder.mPlayIv.setVisibility(View.GONE);
							mVHolder.mMediaIv.setVisibility(View.GONE);
							mediaVv.start();
						}
					}
				});
			}
		});

		progressDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				mVHolder.mPlayIv.setVisibility(View.VISIBLE);
			}
		});
		mediaVv.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				if (null != progressDialog && progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				mVHolder.mPlayIv.setVisibility(View.VISIBLE);
				DialogUtil.showOkCancelDialog(SingleMediaViewWallActivity.this, null,
						"Sorry, this video can not be played in this player. Would you like to launch alternate player?",
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								mVHolder.mMediaIv.setVisibility(View.VISIBLE);
								mVHolder.mPlayIv.setVisibility(View.VISIBLE);
								watchVideoAlternate(url);
							}
						}, null);
				return true;
			}
		});
	}

	public void watchVideoAlternate(String url) {

		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse(url), "video/mp4");
		Intent chooser = Intent.createChooser(intent, "Complete action using...");
		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivity(chooser);
		}

	}

	public static Intent getIntent(Context context, String mediaName, String mediaType, boolean status) {
		Intent intent = new Intent(context, SingleMediaViewWallActivity.class);
		intent.putExtra("mediaName", mediaName);
		intent.putExtra("mediaType", mediaType);
		intent.putExtra("IsWall", status);
		return intent;
	}

	@Override
	public void onBackPressed() {
		this.finish();
		this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

}
