package com.schollyme.activity;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.Config;
import com.schollyme.MediaPickerActivity;
import com.schollyme.R;
import com.schollyme.adapter.SportsDisplayAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SportsModel;
import com.schollyme.model.StateModel;
import com.schollyme.model.UserModel;
import com.vinfotech.request.AboutRequest;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.request.SetupProfileRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.ExpandableHeightListView;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.ValidationUtil;

public class SetupProfileMandatoryActivity extends MediaPickerActivity implements OnClickListener {

	private SetupProfileViewHolder mSetupProfileViewHolder;
	private Context mContext;
	//	public static ArrayList<StateModel> mStatesAL;
	private StateModel mSelectedStateModel;
	private ArrayList<SportsModel> mSportsAL;
	private String mSelectedStateName, mSelectedStateId;
	private static final int REQ_CODE_SELECT_STATE = 100;
	private static final int REQ_CODE_SELECT_SPORTS = 200;
	private static final int REQ_CODE_SELECT_IMAGE = 300;
	private static final int REQ_CODE_OPTIONAL_FIELDS = 400;
	static final int DATE_PICKER_ID = 1111;
	private Bitmap mPhotoBitmap;
	private boolean isProfileSetup = false;

	private LogedInUserModel mLogedInUserModel;
	private int mUserGender = 0;
	private String mUserName, mUserEmail, mUserDOB, mUserFullName, mUserCurrentSchool, mUserCoachingTitle;

	private String mReqType;
	private UserModel mUserModel;
	private ErrorLayout mErrorLayout;

	@Override
	protected void onStart() {
		super.onStart();
	}

	private void resetLocalPreferance(){
		SharedPreferences mLocalPreferance = getSharedPreferences("LocalPreferance",0);
		Editor mLocalEditor = mLocalPreferance.edit();
		mLocalEditor.putBoolean("isUsed", false);
		mLocalEditor.clear();
		mLocalEditor.commit();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setup_profile_mandatory_activity);
		mSetupProfileViewHolder = new SetupProfileViewHolder(findViewById(R.id.main_rl), this);

		mContext = this;
		mLogedInUserModel = new LogedInUserModel(mContext);
		mReqType = getIntent().getStringExtra("mReqType");

		int leftResId;
		if (mReqType.equals("2")) {
			leftResId = R.drawable.icon_back;
		} else {
			leftResId = 0/*R.drawable.icon_close*/;
		}
		setView(mLogedInUserModel.mUserType);
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
		setHeader(findViewById(R.id.header_layout), leftResId, R.drawable.selector_forward_arrow,
				getResources().getString(R.string.setup_profile_label), new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isProfileSetup){
					AboutActivity.setmUserGUID(mSetupProfileViewHolder.mUserNameET.getText().toString());
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							finish();
						}
					});
				}
				else{
					finish();
				}


			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (setupProfileValidation()) {
					if(cropImageUri==null){
						setupProfileServerRequest(mLogedInUserModel.mUserType, "");
					}
					else{
						uploadMediaServerRequest();
					}
				}
			}
		});
		if (mReqType.equals("2")) {
			AboutServerRequest();
		}
		FontLoader.setRobotoRegularTypeface(mSetupProfileViewHolder.mUserNameET, mSetupProfileViewHolder.mEmailET, mSetupProfileViewHolder.mNameET,
				mSetupProfileViewHolder.mCurrentSchoolET, mSetupProfileViewHolder.mCoachingTitleET,mSetupProfileViewHolder.mDOBTV, 
				mSetupProfileViewHolder.mStateTV, mSetupProfileViewHolder.mSportTV, mSetupProfileViewHolder.mGenderMaleTV, 
				mSetupProfileViewHolder.mGenderFemaleTV);
		setFilds();
		resetLocalPreferance();
	}

	@Override
	public void onBackPressed() {

		super.onBackPressed();
		if(isProfileSetup){
			AboutActivity.setmUserGUID(mSetupProfileViewHolder.mUserNameET.getText().toString());
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					finish();
				}
			});
		}
		else{
			finish();
		}
	}

	private void setdataFields(UserModel mUser) {
		mUserModel = mUser;
		mSetupProfileViewHolder.mDOBTV.setText(Utility.formateDate(mLogedInUserModel.mUserDOB));
		if (mUser.mUserGender.equals("1")) {
			mSetupProfileViewHolder.mGenderFemaleTV.setText("");
			mSetupProfileViewHolder.mGenderMaleTV.setText(getResources().getString(R.string.gender_male));
			mSetupProfileViewHolder.mGenderFemaleTV.setTextColor(getResources().getColor(R.color.text_hint_color));
			mSetupProfileViewHolder.mGenderMaleTV.setTextColor(getResources().getColor(R.color.app_text_color));
			mSetupProfileViewHolder.mGenderMaleTV.setSelected(true);
			mSetupProfileViewHolder.mGenderFemaleTV.setSelected(false);
			mSetupProfileViewHolder.mGenderMaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_male_active, 0, 0, 0);
			mSetupProfileViewHolder.mGenderFemaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_female, 0, 0, 0);
			mUserGender = 1;
		} else {
			mSetupProfileViewHolder.mGenderFemaleTV.setText(getResources().getString(R.string.gender_female));
			mSetupProfileViewHolder.mGenderMaleTV.setText("");
			mSetupProfileViewHolder.mGenderFemaleTV.setTextColor(getResources().getColor(R.color.app_text_color));
			mSetupProfileViewHolder.mGenderMaleTV.setTextColor(getResources().getColor(R.color.text_hint_color));
			mSetupProfileViewHolder.mGenderMaleTV.setSelected(false);
			mSetupProfileViewHolder.mGenderFemaleTV.setSelected(true);
			mSetupProfileViewHolder.mGenderMaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_male, 0, 0, 0);
			mSetupProfileViewHolder.mGenderFemaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_female_active, 0, 0, 0);
			mUserGender = 2;
		}
		mSetupProfileViewHolder.mUserNameET.setText(mUser.mUserName);
		mSetupProfileViewHolder.mCoachingTitleET.setText(mUser.mUserCoachingTitle);
		mSetupProfileViewHolder.mCurrentSchoolET.setText(mUser.mUserCurrentSchool);
		mSetupProfileViewHolder.mStateTV.setText(mUser.mUserStateName);
		mSelectedStateId = mUser.mUserStateId;
		mSelectedStateName = mUser.mUserStateName;
		mSelectedStateModel = new StateModel(mSelectedStateId,mSelectedStateName);
		mSportsAL = mUser.mUserSportsAL;
		displaySelectedSportsAdapter();
		String imageUrl = Config.IMAGE_URL_PROFILE + mUser.mUserProfilePic;
		ImageLoaderUniversal
		.ImageLoadRound(mContext, imageUrl, mSetupProfileViewHolder.mProfileIV, ImageLoaderUniversal.option_Round_Image);

		mSetupProfileViewHolder.mUserNameET.setSelection(mSetupProfileViewHolder.mUserNameET.getText().toString().length());
		mSetupProfileViewHolder.mCoachingTitleET.setSelection(mSetupProfileViewHolder.mCoachingTitleET.getText().toString().length());
		mSetupProfileViewHolder.mCurrentSchoolET.setSelection(mSetupProfileViewHolder.mCurrentSchoolET.getText().toString().length());

	}

	private void setFilds() {
		mSetupProfileViewHolder.mDOBTV.setText(Utility.formateDate(mLogedInUserModel.mUserDOB));
		mSetupProfileViewHolder.mNameET.setText(mLogedInUserModel.mFirstName + " " + mLogedInUserModel.mLastName);
		mSetupProfileViewHolder.mEmailET.setText(mLogedInUserModel.mEmail);

		try {
			if(!TextUtils.isEmpty(mLogedInUserModel.mUserName)){
				mSetupProfileViewHolder.mUserNameET.setText(mLogedInUserModel.mUserName);
			}
			if(!TextUtils.isEmpty(mLogedInUserModel.mUserGender)) {

				if (mLogedInUserModel.mUserGender.equals("1")) {
					mSetupProfileViewHolder.mGenderFemaleTV.setText("");
					mSetupProfileViewHolder.mGenderMaleTV.setText(getResources().getString(R.string.gender_male));
					mSetupProfileViewHolder.mGenderFemaleTV.setTextColor(getResources().getColor(R.color.text_hint_color));
					mSetupProfileViewHolder.mGenderMaleTV.setTextColor(getResources().getColor(R.color.app_text_color));
					mSetupProfileViewHolder.mGenderMaleTV.setSelected(true);
					mSetupProfileViewHolder.mGenderFemaleTV.setSelected(false);
					mSetupProfileViewHolder.mGenderMaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_male_active, 0, 0, 0);
					mSetupProfileViewHolder.mGenderFemaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_female, 0, 0, 0);
					mUserGender = 1;
				} else if (mLogedInUserModel.mUserGender.equals("1")){
					mSetupProfileViewHolder.mGenderFemaleTV.setText(getResources().getString(R.string.gender_female));
					mSetupProfileViewHolder.mGenderMaleTV.setText("");
					mSetupProfileViewHolder.mGenderFemaleTV.setTextColor(getResources().getColor(R.color.app_text_color));
					mSetupProfileViewHolder.mGenderMaleTV.setTextColor(getResources().getColor(R.color.text_hint_color));
					mSetupProfileViewHolder.mGenderMaleTV.setSelected(false);
					mSetupProfileViewHolder.mGenderFemaleTV.setSelected(true);
					mSetupProfileViewHolder.mGenderMaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_male, 0, 0, 0);
					mSetupProfileViewHolder.mGenderFemaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_female_active, 0, 0, 0);
					mUserGender = 2;
				}
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}



	public static Intent getIntent(Context context, String mReqType) {
		Intent intent = new Intent(context, SetupProfileMandatoryActivity.class);
		intent.putExtra("mReqType", mReqType);
		return intent;
	}

	private void setView(int userType) {
		if (userType == Config.USER_TYPE_COACH) {
			mSetupProfileViewHolder.mCurrentSchoolET.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mCoachingTitleET.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mSportTV.setVisibility(View.VISIBLE);
		} else if (userType == Config.USER_TYPE_ATHLETE) {
			mSetupProfileViewHolder.mCurrentSchoolET.setVisibility(View.GONE);
			mSetupProfileViewHolder.mCoachingTitleET.setVisibility(View.GONE);
			mSetupProfileViewHolder.mSportTV.setVisibility(View.VISIBLE);
		} else if (userType == Config.USER_TYPE_FAN) {
			mSetupProfileViewHolder.mCurrentSchoolET.setVisibility(View.GONE);
			mSetupProfileViewHolder.mCoachingTitleET.setVisibility(View.GONE);
			mSetupProfileViewHolder.mSportTV.setVisibility(View.GONE);

			mSetupProfileViewHolder.mDevider8.setVisibility(View.GONE);
			mSetupProfileViewHolder.mDevider9.setVisibility(View.GONE);
			mSetupProfileViewHolder.mDevider10.setVisibility(View.GONE);

		}
	}

	public class SetupProfileViewHolder {

		public EditText mUserNameET, mEmailET, mNameET, mCurrentSchoolET, mCoachingTitleET;
		public ImageView mProfileIV;
		public TextView mDOBTV, mStateTV, mSportTV, mGenderMaleTV, mGenderFemaleTV;
		private ExpandableHeightListView mSportsLV;
		private View mDevider8,mDevider9,mDevider10;
		private RelativeLayout mRelativeLayout,mSportsContainerRL;


		public SetupProfileViewHolder(View view, OnClickListener listener) {
			mUserNameET = (EditText) view.findViewById(R.id.user_name_et);
			mEmailET = (EditText) view.findViewById(R.id.email_et);
			mNameET = (EditText) view.findViewById(R.id.user_full_name_et);
			mCurrentSchoolET = (EditText) view.findViewById(R.id.current_school_et);
			mCoachingTitleET = (EditText) view.findViewById(R.id.coaching_title_et);

			mDOBTV = (TextView) view.findViewById(R.id.dob_tv);
			mStateTV = (TextView) view.findViewById(R.id.state_tv);
			mSportTV = (TextView) view.findViewById(R.id.sports_tv);
			mGenderMaleTV = (TextView) view.findViewById(R.id.gender_male_tv);
			mGenderFemaleTV = (TextView) view.findViewById(R.id.gender_female_tv);
			mProfileIV = (ImageView) view.findViewById(R.id.profile_iv);
			mSportsLV = (ExpandableHeightListView) view.findViewById(R.id.selected_sports_list);
			mRelativeLayout = (RelativeLayout) view.findViewById(R.id.main_rl);
			mRelativeLayout.requestFocus();

			mSportsContainerRL = (RelativeLayout) view.findViewById(R.id.selected_sports_list_rl);

			mDevider8 = view.findViewById(R.id.devider8);
			mDevider9 = view.findViewById(R.id.devider9);
			mDevider10 = view.findViewById(R.id.devider10);

			mSportsLV.setExpanded(true);
			mSportsContainerRL.setOnClickListener(listener);
			mStateTV.setOnClickListener(listener);
			mSportTV.setOnClickListener(listener);
			mProfileIV.setOnClickListener(listener);
			mGenderMaleTV.setOnClickListener(listener);
			mGenderFemaleTV.setOnClickListener(listener);
		}
	}

	private void displayStatesPopup() {
		startActivityForResult(StateListActivity.getIntent(mContext,mSelectedStateModel), REQ_CODE_SELECT_STATE);
	}

	private void displaySportsListPopup() {
		startActivityForResult(SportsListActivity.getIntent(mContext, mLogedInUserModel.mUserType == Config.USER_TYPE_ATHLETE, mSportsAL),
				REQ_CODE_SELECT_SPORTS);
	}

	private void displayImagePickerPopup() {

		DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
			@Override
			public void onItemClick(int position, String item) {
				if(position==0){
					openCamera();
				}
				else{
					openGallery();
				}

			}

			@Override
			public void onCancel() {

			}
		}, mContext.getString(R.string.take_picture), mContext.getString(R.string.load_from_library));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {

			case REQ_CODE_SELECT_IMAGE:
				if (resultCode == RESULT_OK) {
					String result = data.getStringExtra("result");
					if ("CAMERA".equalsIgnoreCase(result)) {
						openCamera();
					} else if ("GALLERY".equalsIgnoreCase(result)) {
						openGallery();
					} else if ("Delete".equalsIgnoreCase(result)) {
						mPhotoBitmap = null;
						mSetupProfileViewHolder.mProfileIV.setImageResource(R.drawable.ic_default_profile);
					}
				}
				break;
			case REQ_CODE_SELECT_STATE:
				if (resultCode == RESULT_OK && data != null) {
					mSelectedStateName = data.getStringExtra("mStateName");
					mSelectedStateId = data.getStringExtra("mStateID");
					mSetupProfileViewHolder.mStateTV.setText(mSelectedStateName);
					mSelectedStateModel = new StateModel(mSelectedStateId,mSelectedStateName);
				}

				break;

			case REQ_CODE_SELECT_SPORTS:
				if (data != null) {
					mSportsAL = data.getParcelableArrayListExtra("data");
					displaySelectedSportsAdapter();
				}
				break;
			case REQ_CODE_OPTIONAL_FIELDS:
				String mUserProfileURL =  data.getStringExtra("mUserGUID");
				startActivity(AboutActivity.getIntent(mContext, mUserProfileURL, "SetupProfileMandatoryActivity"));
				finish();
				break;
			}
		}
	}

	private void displaySelectedSportsAdapter() {
		mSetupProfileViewHolder.mSportsLV.setVisibility(View.VISIBLE);

		SportsDisplayAdapter mAdapter = new SportsDisplayAdapter(mContext);
		mSetupProfileViewHolder.mSportsLV.setAdapter(mAdapter);
		mAdapter.setList(mSportsAL);
	}

	@Override
	// Not used in responce, get the result of gallery selection in
	// below(onCameraImageSelected method) method
	protected void onGalleryImageSelected(String fileUri, Bitmap bitmap) {
	}

	@Override
	protected void onCameraImageSelected(String fileUri, Bitmap bitmap) {
		mPhotoBitmap = bitmap;
		mSetupProfileViewHolder.mProfileIV.setImageBitmap(ImageUtil.getCircledBitmap(mPhotoBitmap));
	}

	@Override
	protected void onVideoCaptured(String videoPath) {
	}

	@Override
	protected void onMediaPickCanceled(int reqCode) {
	}

	OnItemClickListener SelectOptionListener = new OnItemClickListener() {

		@Override
		public void onItemClick(int position, String item) {

		}

		@Override
		public void onCancel() {

		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.profile_iv:
			displayImagePickerPopup();
			break;

		case R.id.state_tv:
			displayStatesPopup();
			break;

		case R.id.sports_tv:
			displaySportsListPopup();
			break;

		case R.id.selected_sports_list_rl:
			displaySportsListPopup();
			break;

		case R.id.gender_male_tv:
			mSetupProfileViewHolder.mGenderFemaleTV.setText("");
			mSetupProfileViewHolder.mGenderMaleTV.setText(getResources().getString(R.string.gender_male));
			mSetupProfileViewHolder.mGenderFemaleTV.setTextColor(getResources().getColor(R.color.text_hint_color));
			mSetupProfileViewHolder.mGenderMaleTV.setTextColor(getResources().getColor(R.color.app_text_color));
			mSetupProfileViewHolder.mGenderMaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_male_active, 0, 0, 0);
			mSetupProfileViewHolder.mGenderFemaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_female, 0, 0, 0);
			mUserGender = 1;
			break;

		case R.id.gender_female_tv:
			mSetupProfileViewHolder.mGenderFemaleTV.setText(getResources().getString(R.string.gender_female));
			mSetupProfileViewHolder.mGenderMaleTV.setText("");
			mSetupProfileViewHolder.mGenderFemaleTV.setTextColor(getResources().getColor(R.color.app_text_color));
			mSetupProfileViewHolder.mGenderMaleTV.setTextColor(getResources().getColor(R.color.text_hint_color));
			mSetupProfileViewHolder.mGenderMaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_male, 0, 0, 0);
			mSetupProfileViewHolder.mGenderFemaleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_female_active, 0, 0, 0);
			mUserGender = 2;
			break;
		}
	}

	private boolean setupProfileValidation() {
		boolean isValidated = false;
		mUserName = mSetupProfileViewHolder.mUserNameET.getText().toString();
		mUserEmail = mSetupProfileViewHolder.mEmailET.getText().toString();
		mUserFullName = mSetupProfileViewHolder.mNameET.getText().toString();
		mUserDOB = mLogedInUserModel.mUserDOB;//mSetupProfileViewHolder.mDOBTV.getText().toString();
		if (TextUtils.isEmpty(mUserFullName)) {
			mErrorLayout.showError(getResources().getString(R.string.user_fullname_require), true, MsgType.Error);
		} else if (Utility.getDateDifference(mUserDOB) < 13) {
			mErrorLayout.showError(getResources().getString(R.string.age_restriction_error), true, MsgType.Error);
		} else if (mUserGender == 0) {
			mErrorLayout.showError(getResources().getString(R.string.select_gender_error), true, MsgType.Error);
		} else if (TextUtils.isEmpty(mUserName)) {
			mErrorLayout.showError(getResources().getString(R.string.username_require_message), true, MsgType.Error);
		} else if (!ValidationUtil.isValidUsername(mUserName)) {
			mErrorLayout.showError(getResources().getString(R.string.invalid_user_name), true, MsgType.Error);
		}else if(mUserName.length()>50){
			mErrorLayout.showError(getResources().getString(R.string.user_name_length_exceed), true, MsgType.Error);
		}else if (TextUtils.isEmpty(mSelectedStateId)) {
			mErrorLayout.showError(getResources().getString(R.string.state_require), true, MsgType.Error);
		}else if(!(mLogedInUserModel.mUserType == Config.USER_TYPE_FAN)){
			if (null == mSportsAL || mSportsAL.size() == 0 ) {
				mErrorLayout.showError(getResources().getString(R.string.sports_require), true, MsgType.Error);
			}
			else {
				if (mLogedInUserModel.mUserType == Config.USER_TYPE_COACH) {
					mUserCurrentSchool = mSetupProfileViewHolder.mCurrentSchoolET.getText().toString();
					mUserCoachingTitle = mSetupProfileViewHolder.mCoachingTitleET.getText().toString();
					if (TextUtils.isEmpty(mUserCurrentSchool)) {
						mErrorLayout.showError(getResources().getString(R.string.current_School_require), true, MsgType.Error);
					} else if (TextUtils.isEmpty(mUserCoachingTitle)) {
						mErrorLayout.showError(getResources().getString(R.string.coaching_title_require), true, MsgType.Error);
					}

					else {
						isValidated = true;
					}
				} else if (mLogedInUserModel.mUserType == Config.USER_TYPE_ATHLETE) {
					isValidated = true;
				} else if (mLogedInUserModel.mUserType == Config.USER_TYPE_FAN) {
					isValidated = true;
				}
			}
		}
		else {
			if (mLogedInUserModel.mUserType == Config.USER_TYPE_COACH) {
				mUserCurrentSchool = mSetupProfileViewHolder.mCurrentSchoolET.getText().toString();
				mUserCoachingTitle = mSetupProfileViewHolder.mCoachingTitleET.getText().toString();
				if (TextUtils.isEmpty(mUserCurrentSchool)) {
					mErrorLayout.showError(getResources().getString(R.string.current_School_require), true, MsgType.Error);
				} else if (TextUtils.isEmpty(mUserCoachingTitle)) {
					mErrorLayout.showError(getResources().getString(R.string.coaching_title_require), true, MsgType.Error);
				}

				else {
					isValidated = true;
				}
			} else if (mLogedInUserModel.mUserType == Config.USER_TYPE_ATHLETE) {
				isValidated = true;
			} else if (mLogedInUserModel.mUserType == Config.USER_TYPE_FAN) {
				isValidated = true;
			}
		}
		return isValidated;

	}

	private void AboutServerRequest() {
		AboutRequest mRequest = new AboutRequest(mContext);
		String mUserLinkUrl = new LogedInUserModel(mContext).mUserProfileURL;
		if(TextUtils.isEmpty(mUserLinkUrl)){
			mUserLinkUrl = new LogedInUserModel(mContext).mUserName;
		}

		mRequest.AboutRequestServer(new LogedInUserModel(mContext).mLoginSessionKey, mUserLinkUrl);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {
					UserModel mModel = (UserModel) data;
					setdataFields(mModel);
				} else {

					DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
							new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {
							finish();
						}
					});
				}
			}
		});
	}


	private void uploadMediaServerRequest() {
		MediaUploadRequest mRequest = new MediaUploadRequest(mContext, false);
		String fileName = ImageUtil.getFileNameFromURI(cropImageUri, mContext);
		File mFile = new File(cropImageUri.getPath());

		BaseRequest.setLoginSessionKey(mLogedInUserModel.mLoginSessionKey);
		mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID,
				MediaUploadRequest.TYPE_PROFILE);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					cropImageUri = null;
					AlbumMedia mAlbumMedia = (AlbumMedia) data;
					setupProfileServerRequest(mLogedInUserModel.mUserType, mAlbumMedia.ImageName);
					mLogedInUserModel.mUserPicURL = mAlbumMedia.ImageName;
					mLogedInUserModel.persist(mContext);
				}
			}
		});
	}


	private void setupProfileServerRequest(int userType,final String profilePicName) {

		SetupProfileRequest mRequest = new SetupProfileRequest(mContext);

		BaseRequest.setLoginSessionKey(mLogedInUserModel.mLoginSessionKey);
		final ArrayList<String> mSportsList = new ArrayList<String>();
		if (userType == 1) {

			mSportsList.add(String.valueOf(mSportsAL.get(0).mSportsID));
			if(mUserModel==null){
				mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
						mSelectedStateId, mSportsList, mUserCurrentSchool, mUserCoachingTitle, "", "", "",
						"", "", "", "", "", "", profilePicName);
			}
			else{
				mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender), 
						mSelectedStateId, mSportsList, mUserCurrentSchool, mUserCoachingTitle, mUserModel.mUserCoachingPhilosophy,
						mUserModel.mUserMentor, mUserModel.mUserAlmaMater, mUserModel.mUserFavAthelet, mUserModel.mUserFavTeam, 
						mUserModel.mUserFavMovie, mUserModel.mUserFavMusic, mUserModel.mUserFavFood, mUserModel.mUserFavTeacher,
						profilePicName);
			}
		} else if (userType == 2) {
			for(int i=0;i<mSportsAL.size();i++){
				mSportsList.add(String.valueOf(mSportsAL.get(i).mSportsID));	
			}
			if(mUserModel==null){
				mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
						mSelectedStateId, mSportsList, "", "", "","", "","", "", "", "", "","", profilePicName, "", "",
						"", "", "", "", "", "", "", "","", "", "","","");
			}
			else{
				mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender), 
						mSelectedStateId, mSportsList, mUserModel.mUserPosition, mUserModel.mUserHomeTeam, mUserModel.mUserJerseyNumber,
						mUserModel.mUserCurrentSchool, mUserModel.mUserClubTeam, mUserModel.mUserFavAthelet, mUserModel.mUserFavTeam, 
						mUserModel.mUserFavMovie, mUserModel.mUserFavMusic, mUserModel.mUserFavFood, mUserModel.mUserFavTeacher,
						profilePicName, mUserModel.mUserFavQuote, mUserModel.mUserHomeAddress, mUserModel.mUserPhoneNumber,
						mUserModel.mUserAlternateEmail, mUserModel.mUserGPAScore, mUserModel.mSATScore, mUserModel.mUserACTScore,
						mUserModel.mUserTranscripts, mUserModel.mUserNCAAEligibility, mUserModel.mUserParentID, mUserModel.mUserParentName,
						mUserModel.mUserParentEmail, mUserModel.mUserParentPhoneNumber, "", mUserModel.mUserHeight);
			}


		} else if (userType == 3) {
			if(mUserModel==null){
				mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
						mSelectedStateId, "", "", "", "", "", "", profilePicName, "");
			}
			else{
				mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
						mSelectedStateId, mUserModel.mUserFavAthelet, mUserModel.mUserFavTeam,mUserModel.mUserFavMovie, 
						mUserModel.mUserFavMusic,mUserModel.mUserFavFood, mUserModel.mUserFavTeacher, profilePicName, mUserModel.mUserFavQuote);
			}
		}
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					isProfileSetup = true;
					updateDataAndCallIntent();

				} else {
					DialogUtil.showOkDialogUncancelableButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
							new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

						}
					});
				}
			}
		});
	}

	private void updateDataAndCallIntent(){

		ArrayList<String> selSportIds = new ArrayList<String>();
		if (null != mSportsAL) {
			for (SportsModel sportsModel : mSportsAL) {
				selSportIds.add(sportsModel.mSportsID);
				if(mLogedInUserModel.mUserType==Config.USER_TYPE_COACH){
					mLogedInUserModel.mUserSportsId = sportsModel.mSportsID;
					mLogedInUserModel.mUserSportsName = sportsModel.mSportsName;
				}
			}
		}

		mLogedInUserModel.mUserName = mUserName;
		mLogedInUserModel.mUserProfileURL = mUserName;
		mLogedInUserModel.mUserGender = String.valueOf(mUserGender);

		mLogedInUserModel.persist(mContext);
		mLogedInUserModel.updatePreference(mContext, "isProfileSetUp", true);


		if (mUserModel == null) {
			mUserModel = new UserModel();
		}

		if (mReqType.equals("1")) {
			mUserModel.mUserName = mSetupProfileViewHolder.mUserNameET.getText().toString();
			mUserModel.mUserEmail = mUserEmail;
			mUserModel.mUserDOB = mUserDOB;
			mUserModel.mUserFullName = mUserFullName;
			if(mLogedInUserModel.mUserType==Config.USER_TYPE_COACH)
				mUserModel.mUserCurrentSchool = mUserCurrentSchool;
			mUserModel.mUserCoachingTitle = mUserCoachingTitle;
			mUserModel.mUserGender = String.valueOf(mUserGender);
			mUserModel.mUserStateId = String.valueOf(mSelectedStateId);
		}
		mUserModel.mUserName = mSetupProfileViewHolder.mUserNameET.getText().toString();
		if(mLogedInUserModel.mUserType==Config.USER_TYPE_COACH)
			mUserModel.mUserCurrentSchool = mUserCurrentSchool;
		mUserModel.mUserCoachingTitle = mUserCoachingTitle;
		mUserModel.mUserGender = String.valueOf(mUserGender);
		mUserModel.mUserStateId = String.valueOf(mSelectedStateId);
		mUserModel.mUserName = mSetupProfileViewHolder.mUserNameET.getText().toString();


		Intent mIntent = SetupProfileOptionalActivity.getIntent(mContext, mUserModel/*, cropImageUri*/, mReqType,
				selSportIds);
		startActivity(mIntent);

		//	finish();
	}
}