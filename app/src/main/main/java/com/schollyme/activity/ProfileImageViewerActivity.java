package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.vinfotech.request.AlbumMediaDeleteRequest;
import com.vinfotech.request.AlbumSetCoverRequest;
import com.vinfotech.request.MarkSpamRequest;
import com.vinfotech.request.MediaDetailRequest;
import com.vinfotech.request.MediaUpdateCaptionRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class ProfileImageViewerActivity extends ActionBarActivity {

    private static final String TAG = ProfileImageViewerActivity.class.getSimpleName();
    public static final int REQ_CODE_MEDIA_VIEWER_ACTIVITY = 1013;

    private ErrorLayout mErrorLayout;
    private String mImageURL;
    private Context mContext;
    private VHolder mVHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_viewer_activity);
        mContext = this;
        initlization();
        getIntentData();

        ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, mImageURL, mVHolder.mProfileImageIV,
                ImageLoaderUniversal.DiaplayProfileOptionForProgresser, mVHolder.mLoadingPb);
    }

    public static Intent getIntent(Context context,String mImageURL) {
        Intent intent = new Intent(context, ProfileImageViewerActivity.class);
        intent.putExtra("ImageURL",mImageURL);
        return intent;
    }

    private void getIntentData() {
       mImageURL = getIntent().getStringExtra("ImageURL");
    }

    private void initlization() {
        mErrorLayout = new ErrorLayout(findViewById(R.id.error_layout));
        mVHolder = new VHolder(findViewById(R.id.main_container_rl));

    }


    class VHolder {
        private TextView mDoneTv;
        private ImageView mProfileImageIV;
        private ProgressBar mLoadingPb;

        public VHolder(View view) {

            mProfileImageIV = (ImageView)view.findViewById(R.id.profile_image_iv);
            mDoneTv = (TextView) view.findViewById(R.id.done_tv);
            mLoadingPb = (ProgressBar) view.findViewById(R.id.loading_pb);
            FontLoader.setRobotoRegularTypeface(mDoneTv);
            mDoneTv.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }
}