package com.schollyme.activity;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.adapter.TranscriptsRequestAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.TranscriptsRequestModel;
import com.vinfotech.request.TranscriptsRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;

public class TranscriptsRequestFromCoachActivity extends BaseActivity implements OnRefreshListener,OnClickListener{

	private Context mContext;
	private ListView mTranscriptsRequestLV;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private TextView mNoRecordTV;
	private boolean loadingFlag = false;
	private ErrorLayout mErrorLayout;
	private int pageIndex = 1;
	private String mFromActivity;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.transcripts_request_activity);
		mErrorLayout = new ErrorLayout(findViewById(R.id.include_error));
		mFromActivity = getIntent().getStringExtra("fromActivity");
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, 0,
				getResources().getString(R.string.transcripts_request), new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mFromActivity.equalsIgnoreCase("GCMIntentService")){
					Intent intent = DashboardActivity.getIntent(TranscriptsRequestFromCoachActivity.this, 5, "FriendsTopFragment");
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					finish();
				}else
					finish();
			}
		}, null);

		mContext = this;
		mNoRecordTV =  (TextView)findViewById(R.id.no_record_message_tv);
		mTranscriptsRequestLV = (ListView) findViewById(R.id.transcripts_lv);
		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
	
		getTranscriptsRequest();
	}
	
	public static Intent getIntent(Context context, String fromActivity ) {
		Intent intent = new Intent(context, TranscriptsRequestFromCoachActivity.class);
		intent.putExtra("fromActivity", fromActivity);
		return intent;
	}

	@Override
	public void onRefresh() {
		getTranscriptsRequest();
	}

	@Override
	public void onClick(View arg0) {
		
	}
	
	@Override
	public void onBackPressed() {
		if(mFromActivity.equalsIgnoreCase("GCMIntentService")){
			Intent intent = DashboardActivity.getIntent(this, 5, "FriendsTopFragment");
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}else
			super.onBackPressed();
	}
	
	private void setAdapter(ArrayList<TranscriptsRequestModel> mList){
		TranscriptsRequestAdapter mAdapter = new TranscriptsRequestAdapter(mContext, mErrorLayout);
		mTranscriptsRequestLV.setAdapter(mAdapter);
		Collections.reverse(mList);
		mAdapter.setList(mList);
		if(mList.size()==0){
			mNoRecordTV.setVisibility(View.VISIBLE);
		}
	}
	
	
	private void getTranscriptsRequest(){
		TranscriptsRequest mRequest = new TranscriptsRequest(mContext);
		
		mRequest.setRequestListener(new RequestListener() {
			
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mSwipeRefreshWidget.setRefreshing(false);
				if(success){
					ArrayList<TranscriptsRequestModel> mList = (ArrayList<TranscriptsRequestModel>) data;
					setAdapter(mList);
				}
				else{
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
			}
		});
		mRequest.TranscriptsListServerRequest(pageIndex,Config.PAGE_SIZE);
	}
}