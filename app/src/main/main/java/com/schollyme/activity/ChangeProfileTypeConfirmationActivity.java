package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

import com.schollyme.BaseActivity;
import com.schollyme.LoginActivity;
import com.schollyme.R;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.ChangeUserTypeRequest;
import com.vinfotech.request.LogoutRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

import org.w3c.dom.Text;

public class ChangeProfileTypeConfirmationActivity extends BaseActivity {


    private TextView  mOption1TV,mOption2TV,mOption3TV,mHeaderTV,mProceedTV;
    private Context mContext;
    private int UserType;
    private LogedInUserModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile_type_confirmation);
        mContext = this;
        mOption1TV = (TextView)findViewById(R.id.changetype1_tv);
        mOption2TV = (TextView)findViewById(R.id.changetype2_tv);
        mOption3TV = (TextView)findViewById(R.id.changetype3_tv);
        mHeaderTV =  (TextView)findViewById(R.id.changetype_header_tv);
        mProceedTV = (TextView)findViewById(R.id.proceed_tv);
        FontLoader.setRobotoRegularTypeface(mOption1TV, mOption2TV, mOption3TV,mHeaderTV,mProceedTV);
        UserType = getIntent().getIntExtra("UserType", 0);
        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, 0,
                getResources().getString(R.string.change_user_type), new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }, null);
        mProceedTV.setOnClickListener(ProcessedListener);
        mModel = new LogedInUserModel(mContext);
        if(mModel.mUserType==1){
            mOption3TV.setVisibility(View.VISIBLE);
            mOption3TV.setText(getString(R.string.change_profile4));
        }else if(mModel.mUserType==2){
            mOption3TV.setVisibility(View.VISIBLE);
            mOption3TV.setText(getString(R.string.change_profile3));
        }else if(mModel.mUserType==3){
            mOption3TV.setVisibility(View.INVISIBLE);
        }

    }


    View.OnClickListener ProcessedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogUtil.showOkCancelDialog(mContext, getResources().getString(R.string.app_name),
                    getResources().getString(R.string.change_user_type_confirm), new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            changeUserTypeCall(UserType);
                        }
                    }, new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                        }
                    });

        }
    };


    private void changeUserTypeCall(int UserTypeID){
        ChangeUserTypeRequest mRequest = new ChangeUserTypeRequest(mContext);
        mRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if(success){
                    logoutCall();
                }
                else{
                    DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                            getResources().getString(R.string.app_name), null);
                }
            }
        });
        mRequest.ChangeUserTypeRequest(UserTypeID);
    }

    private void logoutCall(){
        LogoutRequest mRequest = new LogoutRequest(mContext);

        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {

                    mModel.removeUserData(mContext);
                    Intent intent = LoginActivity.getIntent(mContext);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                            getResources().getString(R.string.app_name), null);
                }
            }
        });
        mRequest.LogoutServerRequest();
    }


    public static Intent getIntent(Context context,int mUserType) {
        Intent intent = new Intent(context, ChangeProfileTypeConfirmationActivity.class);
        intent.putExtra("UserType",mUserType);
        return intent;
    }

}
