package com.schollyme.activity;

import java.lang.reflect.Field;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.schollyme.BaseActivity;
import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.UserModel;
import com.vinfotech.request.SetupProfileRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.ValidationUtil;

public class SetupProfileOptionalActivity extends BaseActivity implements OnClickListener {

	private SetupProfileViewHolder mSetupProfileViewHolder;
	private Context mContext;
	private LogedInUserModel mLogedInUserModel;
	private int mUserGender = 0;
	private String mUserEmail, mUserDOB, mUserFullName, mUserName, mUserCurrentSchool, mUserCoachingTitle, mSelectedStateId;
	private ArrayList<String> mSelectedSportsId;
	//	private Uri cropImageUri;
	private String mReqType;
	private UserModel mUserModel;
	private String  selectedFeet,selectedInch;
	private SharedPreferences mLocalPreferance;
	private Dialog mDisplayDialog;
	private ErrorLayout mErrorLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setup_profile_optional_activity);
		mSetupProfileViewHolder = new SetupProfileViewHolder(findViewById(R.id.main_rl), this);
		mContext = this;
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
				getResources().getString(R.string.setup_profile_label), new OnClickListener() {
			@Override
			public void onClick(View v) {
				setLocalPreferance();
				finish();
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				updateListener();
			}
		});
		mLogedInUserModel = new LogedInUserModel(mContext);
		setView(mLogedInUserModel.mUserType);
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
		getDataFromIntentAndInitFields();
		if(isLocalPreferance()){
			getFromLocalPreferance();
		}
		FontLoader.setRobotoMediumTypeface(mSetupProfileViewHolder.mMoreAboutMeTV,mSetupProfileViewHolder.mCoachMoreAboutTV,/*mSetupProfileViewHolder.mMyScoreTV,*/mSetupProfileViewHolder.mMyFavTV,mSetupProfileViewHolder.mMySportsTV);
		FontLoader.setRobotoRegularTypeface(mSetupProfileViewHolder.mHomeAddressET, mSetupProfileViewHolder.mPhoneNumberET, 
				mSetupProfileViewHolder.mAlternateEmailET, mSetupProfileViewHolder.mParentNameET, mSetupProfileViewHolder.mParentHomeET, 
				mSetupProfileViewHolder.mParentEmailET,mSetupProfileViewHolder.mCoachingPhilosophyET, mSetupProfileViewHolder.mMentorET, 
				mSetupProfileViewHolder.mAlmaMeterET/*, mSetupProfileViewHolder.mAthleteGPAET, mSetupProfileViewHolder.mAtheleteSATET,
				mSetupProfileViewHolder.mAthleteACTET, mSetupProfileViewHolder.mAthleteTranscriptsET,
				mSetupProfileViewHolder.mFavAthleteET*/, mSetupProfileViewHolder.mFavTeamET, mSetupProfileViewHolder.mFavMovieET, 
				mSetupProfileViewHolder.mFavMusicET, mSetupProfileViewHolder.mFavFoodET, mSetupProfileViewHolder.mFavTeacher, 
				mSetupProfileViewHolder.mFavQuote, mSetupProfileViewHolder.mSportPositionET,mSetupProfileViewHolder.mSportHomeTeamET, 
				mSetupProfileViewHolder.mSportJerseyET, mSetupProfileViewHolder.mSportNameOfCurrentSchoolET, mSetupProfileViewHolder.mSportNameOfClubTeam, 
				mSetupProfileViewHolder.mSportFavAthlete, mSetupProfileViewHolder.mSportFavTeam,mSetupProfileViewHolder.mSkipTV,mSetupProfileViewHolder.mHeightTV,
				mSetupProfileViewHolder.mNCAAET);
	}

	private void updateListener() {
		if(validateFields(mLogedInUserModel.mUserType)){
			setupProfileServerRequest(mLogedInUserModel.mUserType, "");
		}
	}

	private boolean validateFields(int userType){
		if(userType==2){

			String mJerseyNo = mSetupProfileViewHolder.mSportJerseyET.getText().toString().trim();
			String mPhoneNumber = mSetupProfileViewHolder.mPhoneNumberET.getText().toString().trim();
			String mParentPhoneNumber = mSetupProfileViewHolder.mParentHomeET.getText().toString().trim();
			String mAlternateEmail = mSetupProfileViewHolder.mAlternateEmailET.getText().toString().trim();
			String mParentEmail = mSetupProfileViewHolder.mParentEmailET.getText().toString().trim();
			
			if(!TextUtils.isEmpty(mAlternateEmail) && !ValidationUtil.isValidEmail(mAlternateEmail)){
				mErrorLayout.showError(getResources().getString(R.string.alternate_email_invalid),true, MsgType.Error);
				return false;
			}
			else if(!TextUtils.isEmpty(mParentEmail) && !ValidationUtil.isValidEmail(mParentEmail)){
				mErrorLayout.showError(getResources().getString(R.string.parents_email_invalid),true, MsgType.Error);
				return false;
			}
			else if(!TextUtils.isEmpty(mPhoneNumber) && mPhoneNumber.length()>14){
				mErrorLayout.showError(getResources().getString(R.string.phone_number_error),true, MsgType.Error);
				return false;
			}
			else if(!TextUtils.isEmpty(mParentPhoneNumber) && mParentPhoneNumber.length()>14){
				mErrorLayout.showError(getResources().getString(R.string.parent_phone_number_error),true, MsgType.Error);
				return false;
			}
			else if(!TextUtils.isEmpty(mJerseyNo) && mJerseyNo.length()>4){
				mErrorLayout.showError(getResources().getString(R.string.jeysey_number_error),true, MsgType.Error);
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return true;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		setLocalPreferance();
	}

	private void getDataFromIntentAndInitFields() {

		
		/*
		 * String mUri = getIntent().getStringExtra("cropImageUri");
		 * 	if (!TextUtils.isEmpty(mUri)) {
			cropImageUri = Uri.parse(mUri);
		}*/
		mReqType = getIntent().getStringExtra("mReqType");
		mUserModel = (UserModel) getIntent().getSerializableExtra("mUserData");
		if (mUserModel != null) {

			mUserEmail = mUserModel.mUserEmail;
			mUserDOB = mUserModel.mUserDOB;
			mUserFullName = mUserModel.mUserFullName;
			mUserName = mUserModel.mUserName;
			mUserCurrentSchool = mUserModel.mUserCurrentSchool;
			mUserCoachingTitle = mUserModel.mUserCoachingTitle;
			mUserGender = Integer.parseInt(mUserModel.mUserGender);
			mSelectedStateId = mUserModel.mUserStateId;
			mSelectedSportsId = getIntent().getStringArrayListExtra("mSelectedSportsId");

			mSetupProfileViewHolder.mFavAthleteET.setText(mUserModel.mUserFavAthelet);
			mSetupProfileViewHolder.mFavTeamET.setText(mUserModel.mUserFavTeam);
			mSetupProfileViewHolder.mFavQuote.setText(mUserModel.mUserFavQuote);
			mSetupProfileViewHolder.mHomeAddressET.setText(mUserModel.mUserHomeAddress);
			mSetupProfileViewHolder.mPhoneNumberET.setText(mUserModel.mUserPhoneNumber);
			mSetupProfileViewHolder.mAlternateEmailET.setText(mUserModel.mUserAlternateEmail);
			mSetupProfileViewHolder.mParentNameET.setText(mUserModel.mUserParentName);
			mSetupProfileViewHolder.mParentHomeET.setText(mUserModel.mUserParentPhoneNumber);
			mSetupProfileViewHolder.mParentEmailET.setText(mUserModel.mUserParentEmail);
			String mUserGPA = mUserModel.mUserGPAScore;
			if(null!= mUserGPA && mUserGPA.startsWith("0")){
				mUserGPA = mUserGPA.replace("0", "");
			}
		/*	mSetupProfileViewHolder.mAthleteGPAET.setText(String.valueOf(mUserGPA));
			mSetupProfileViewHolder.mAtheleteSATET.setText(mUserModel.mSATScore);
			mSetupProfileViewHolder.mAthleteACTET.setText(mUserModel.mUserACTScore);
			mSetupProfileViewHolder.mAthleteTranscriptsET.setText(mUserModel.mUserTranscripts);*/
			mSetupProfileViewHolder.mFavMovieET.setText(mUserModel.mUserFavMovie);
			mSetupProfileViewHolder.mFavMusicET.setText(mUserModel.mUserFavMusic);
			mSetupProfileViewHolder.mFavFoodET.setText(mUserModel.mUserFavFood);
			mSetupProfileViewHolder.mFavTeacher.setText(mUserModel.mUserFavTeacher);
			mSetupProfileViewHolder.mSportPositionET.setText(mUserModel.mUserPosition);
			mSetupProfileViewHolder.mSportHomeTeamET.setText(mUserModel.mUserHomeTeam);
			mSetupProfileViewHolder.mSportJerseyET.setText(mUserModel.mUserJerseyNumber);

			mSetupProfileViewHolder.mSportHomeTeamET.setText(mUserModel.mUserHomeTeam);
			mSetupProfileViewHolder.mSportNameOfClubTeam.setText(mUserModel.mUserClubTeam);
			mSetupProfileViewHolder.mNCAAET.setText(mUserModel.mUserNCAAEligibility);
			if(!TextUtils.isEmpty(mUserModel.mUserHeight)){
				String convertedValue = Utility.convertInchesToFeetAndInch(Integer.parseInt(mUserModel.mUserHeight),mContext);
				mSetupProfileViewHolder.mHeightTV.setText(convertedValue);
				if(!TextUtils.isEmpty(convertedValue)){
					String[] arr = convertedValue.split(" "); 
					selectedFeet = arr[0]+" "+arr[1];
					selectedInch = arr[2]+" "+arr[3];
				}
			}
			if(!TextUtils.isEmpty(mUserModel.mUserCoachingPhilosophy)){
				mSetupProfileViewHolder.mCoachingPhilosophyET.setText(String.valueOf(mUserModel.mUserCoachingPhilosophy));
			}
			if(!TextUtils.isEmpty(mUserModel.mUserAlmaMater)){
				mSetupProfileViewHolder.mAlmaMeterET.setText(String.valueOf(mUserModel.mUserAlmaMater));
			}
			if(!TextUtils.isEmpty(mUserModel.mUserMentor) ){
				mSetupProfileViewHolder.mMentorET.setText(String.valueOf(mUserModel.mUserMentor));
			}

			mSetupProfileViewHolder.mSportFavAthlete.setText(mUserModel.mUserFavAthelet);
			mSetupProfileViewHolder.mSportFavTeam.setText(mUserModel.mUserFavTeam);
			mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setText(mUserModel.mUserCurrentSchool);

			setCursor();
		}
	}

	private void setCursor(){
		mSetupProfileViewHolder.mFavAthleteET.setSelection(mSetupProfileViewHolder.mFavAthleteET.getText().toString().length());
		mSetupProfileViewHolder.mFavTeamET.setSelection(mSetupProfileViewHolder.mFavTeamET.getText().toString().length());
		mSetupProfileViewHolder.mFavQuote.setSelection(mSetupProfileViewHolder.mFavQuote.getText().toString().length());
		mSetupProfileViewHolder.mHomeAddressET.setSelection(mSetupProfileViewHolder.mHomeAddressET.getText().toString().length());
		mSetupProfileViewHolder.mPhoneNumberET.setSelection(mSetupProfileViewHolder.mPhoneNumberET.getText().toString().length());
		mSetupProfileViewHolder.mAlternateEmailET.setSelection(mSetupProfileViewHolder.mAlternateEmailET.getText().toString().length());
		mSetupProfileViewHolder.mParentNameET.setSelection(mSetupProfileViewHolder.mParentNameET.getText().toString().length());
		mSetupProfileViewHolder.mParentHomeET.setSelection(mSetupProfileViewHolder.mParentHomeET.getText().toString().length());
		mSetupProfileViewHolder.mParentEmailET.setSelection(mSetupProfileViewHolder.mParentEmailET.getText().toString().length());
		/*mSetupProfileViewHolder.mAthleteGPAET.setSelection(mSetupProfileViewHolder.mAthleteGPAET.getText().toString().length());
		mSetupProfileViewHolder.mAtheleteSATET.setSelection(mSetupProfileViewHolder.mAtheleteSATET.getText().toString().length());
		mSetupProfileViewHolder.mAthleteACTET.setSelection(mSetupProfileViewHolder.mAthleteACTET.getText().toString().length());
		mSetupProfileViewHolder.mAthleteTranscriptsET.setSelection(mSetupProfileViewHolder.mAthleteTranscriptsET.getText().toString().length());
		*/mSetupProfileViewHolder.mFavMovieET.setSelection(mSetupProfileViewHolder.mFavMovieET.getText().toString().length());
		mSetupProfileViewHolder.mFavMusicET.setSelection(mSetupProfileViewHolder.mFavMusicET.getText().toString().length());
		mSetupProfileViewHolder.mFavFoodET.setSelection(mSetupProfileViewHolder.mFavFoodET.getText().toString().length());
		mSetupProfileViewHolder.mFavTeacher.setSelection(mSetupProfileViewHolder.mFavTeacher.getText().toString().length());
		mSetupProfileViewHolder.mSportPositionET.setSelection(mSetupProfileViewHolder.mSportPositionET.getText().toString().length());
		mSetupProfileViewHolder.mSportHomeTeamET.setSelection(mSetupProfileViewHolder.mSportHomeTeamET.getText().toString().length());
		mSetupProfileViewHolder.mSportJerseyET.setSelection(mSetupProfileViewHolder.mSportJerseyET.getText().toString().length());

		mSetupProfileViewHolder.mSportHomeTeamET.setSelection(mSetupProfileViewHolder.mSportHomeTeamET.getText().toString().length());
		mSetupProfileViewHolder.mSportNameOfClubTeam.setSelection(mSetupProfileViewHolder.mSportNameOfClubTeam.getText().toString().length());
		mSetupProfileViewHolder.mNCAAET.setSelection(mSetupProfileViewHolder.mNCAAET.getText().toString().length());
		mSetupProfileViewHolder.mCoachingPhilosophyET.setSelection(String.valueOf(mSetupProfileViewHolder.mCoachingPhilosophyET.getText().toString()).length());
		mSetupProfileViewHolder.mAlmaMeterET.setSelection(String.valueOf(mSetupProfileViewHolder.mAlmaMeterET.getText().toString()).length());
		mSetupProfileViewHolder.mMentorET.setSelection(String.valueOf(mSetupProfileViewHolder.mMentorET.getText().toString()).length());
		mSetupProfileViewHolder.mSportFavAthlete.setSelection(mSetupProfileViewHolder.mSportFavAthlete.getText().toString().length());
		mSetupProfileViewHolder.mSportFavTeam.setSelection(mSetupProfileViewHolder.mSportFavTeam.getText().toString().length());
		mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setSelection(mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.getText().toString().length());
	}

	public static Intent getIntent(Context context, UserModel mUserModel/*, Uri cropImageUri*/, String mReqType, ArrayList<String> list) {
		Intent mIntent = new Intent(context, SetupProfileOptionalActivity.class);
		mIntent.putExtra("mReqType", mReqType);
		mIntent.putExtra("mUserData", mUserModel);
		mIntent.putExtra("mSelectedSportsId", list);
		/*if (cropImageUri != null) {
			mIntent.putExtra("cropImageUri", cropImageUri.toString());
		}*/
		return mIntent;
	}

	private void setView(int userType) {
		if (userType == Config.USER_TYPE_COACH) {
			mSetupProfileViewHolder.mAthleteAboutSpecificRL.setVisibility(View.GONE);
			mSetupProfileViewHolder.mAthleteSportsRL.setVisibility(View.GONE);
			mSetupProfileViewHolder.mCoachAboutSpecificRL.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mFavAthleteET.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mFavTeamET.setVisibility(View.VISIBLE);
	//		mSetupProfileViewHolder.mAthleteScoreRL.setVisibility(View.GONE);
			mSetupProfileViewHolder.mFavQuote.setVisibility(View.GONE);
			mSetupProfileViewHolder.teacherBelowView.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.favTeacherBelowView.setVisibility(View.GONE);
			mSetupProfileViewHolder.favQuoteBelowView.setVisibility(View.GONE);
			mSetupProfileViewHolder.mDevider18.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mDevider19.setVisibility(View.VISIBLE);

		} else if (userType == Config.USER_TYPE_ATHLETE) {
			mSetupProfileViewHolder.mAthleteAboutSpecificRL.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mAthleteSportsRL.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mCoachAboutSpecificRL.setVisibility(View.GONE);
			mSetupProfileViewHolder.mFavAthleteET.setVisibility(View.GONE);
			mSetupProfileViewHolder.mFavTeamET.setVisibility(View.GONE);
	//		mSetupProfileViewHolder.mAthleteScoreRL.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mFavQuote.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.teacherBelowView.setVisibility(View.GONE);
			mSetupProfileViewHolder.favTeacherBelowView.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.favQuoteBelowView.setVisibility(View.GONE);
		/*	mSetupProfileViewHolder.mAthleteGPAET.setFilters(new InputFilter[] { filter });

			mSetupProfileViewHolder.mDevider18.setVisibility(View.GONE);
			mSetupProfileViewHolder.mDevider19.setVisibility(View.GONE);
			mSetupProfileViewHolder.mAthleteACTET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_NEXT) {
						mSetupProfileViewHolder.mNCAAET.requestFocus();
						return true;
					}
					return false;
				}
			});*/

		} else if (userType == Config.USER_TYPE_FAN) {
			mSetupProfileViewHolder.mAthleteAboutSpecificRL.setVisibility(View.GONE);
			mSetupProfileViewHolder.mAthleteSportsRL.setVisibility(View.GONE);
			mSetupProfileViewHolder.mCoachAboutSpecificRL.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mCoachingPhilosophyET.setVisibility(View.GONE);
			mSetupProfileViewHolder.mMentorET.setVisibility(View.GONE);
			mSetupProfileViewHolder.mFavAthleteET.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mFavTeamET.setVisibility(View.VISIBLE);
	//		mSetupProfileViewHolder.mAthleteScoreRL.setVisibility(View.GONE);
			mSetupProfileViewHolder.mFavQuote.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.teacherBelowView.setVisibility(View.GONE);
			mSetupProfileViewHolder.favTeacherBelowView.setVisibility(View.GONE);
			mSetupProfileViewHolder.favQuoteBelowView.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mDevider18.setVisibility(View.VISIBLE);
			mSetupProfileViewHolder.mDevider19.setVisibility(View.VISIBLE);
		}
	}

	InputFilter filter = new InputFilter() {
		final int maxDigitsBeforeDecimalPoint = 1;
		final int maxDigitsAfterDecimalPoint = 2;

		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			StringBuilder builder = new StringBuilder(dest);
			builder.replace(dstart, dend, source.subSequence(start, end).toString());
			if (!builder.toString().matches(
					"(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?")) {
				if (source.length() == 0)
					return dest.subSequence(dstart, dend);
				return "";
			}

			return null;
		}
	};

	public class SetupProfileViewHolder {

		public EditText mHomeAddressET, mPhoneNumberET, mAlternateEmailET, mParentNameET, mParentHomeET, mParentEmailET,
		mCoachingPhilosophyET, mMentorET, mAlmaMeterET, /*mAthleteGPAET, mAtheleteSATET, mAthleteACTET, mAthleteTranscriptsET,*/mNCAAET,
		mFavAthleteET, mFavTeamET, mFavMovieET, mFavMusicET, mFavFoodET, mFavTeacher, mFavQuote, mSportPositionET,
		mSportHomeTeamET, mSportJerseyET, mSportNameOfCurrentSchoolET, mSportNameOfClubTeam, mSportFavAthlete, mSportFavTeam;

		public TextView mSkipTV,mMoreAboutMeTV,mCoachMoreAboutTV,/*mMyScoreTV,*/mMyFavTV,mMySportsTV,mHeightTV;
	//	private TextView mTranscriptVerifyTV;
		

		private View teacherBelowView, favTeacherBelowView, favQuoteBelowView, mDevider18, mDevider19;

		public RelativeLayout mAthleteAboutSpecificRL, mCoachAboutSpecificRL, mAthleteSportsRL/*, mAthleteScoreRL*/;

		public SetupProfileViewHolder(View view, OnClickListener listener) {

			mMoreAboutMeTV = (TextView) view.findViewById(R.id.more_about_me_tv);
			mCoachMoreAboutTV = (TextView) view.findViewById(R.id.more_about_me_coach_tv);
	//		mMyScoreTV = (TextView) view.findViewById(R.id.my_score_tv);
			mMyFavTV = (TextView) view.findViewById(R.id.my_favorite_tv);
			mMySportsTV = (TextView) view.findViewById(R.id.my_sports_tv);
			mHeightTV = (TextView) view.findViewById(R.id.height_tv);

			mHomeAddressET = (EditText) view.findViewById(R.id.home_address_et);
			mPhoneNumberET = (EditText) view.findViewById(R.id.phone_number_et);
			mAlternateEmailET = (EditText) view.findViewById(R.id.alternate_email_et);
			mParentNameET = (EditText) view.findViewById(R.id.parent_name_et);
			mParentHomeET = (EditText) view.findViewById(R.id.parent_home_et);
			mParentEmailET = (EditText) view.findViewById(R.id.parent_email_et);
			mCoachingPhilosophyET = (EditText) view.findViewById(R.id.coaching_philosophy_et);
			mMentorET = (EditText) view.findViewById(R.id.mentor_et);
			mAlmaMeterET = (EditText) view.findViewById(R.id.alma_meter_et1);
			/*mAthleteGPAET = (EditText) view.findViewById(R.id.gpa_et);
			mAtheleteSATET = (EditText) view.findViewById(R.id.sat_et);
			mAthleteACTET = (EditText) view.findViewById(R.id.act_score_et);
			mAthleteTranscriptsET = (EditText) view.findViewById(R.id.transcripts_et);*/
			mNCAAET = (EditText) view.findViewById(R.id.ncaa_et);
			mFavAthleteET = (EditText) view.findViewById(R.id.fav_athlete_et);
			mFavTeamET = (EditText) view.findViewById(R.id.fav_team_et);
			mFavMovieET = (EditText) view.findViewById(R.id.fav_movie_et);
			mFavMusicET = (EditText) view.findViewById(R.id.fav_music_et);
			mFavFoodET = (EditText) view.findViewById(R.id.fav_food_et);
			mFavTeacher = (EditText) view.findViewById(R.id.fav_teacher_et);
			mFavQuote = (EditText) view.findViewById(R.id.fav_quote_et);
			mSportPositionET = (EditText) view.findViewById(R.id.position_et);
			mSportHomeTeamET = (EditText) view.findViewById(R.id.home_team_et);
			mSportJerseyET = (EditText) view.findViewById(R.id.jersey_et);
			mSportNameOfCurrentSchoolET = (EditText) view.findViewById(R.id.name_of_school_et);
			mSportNameOfClubTeam = (EditText) view.findViewById(R.id.name_of_club_team_et);
			mSportFavAthlete = (EditText) view.findViewById(R.id.favorite_athlete_et);
			mSportFavTeam = (EditText) view.findViewById(R.id.favorite_team_et);
			mAthleteAboutSpecificRL = (RelativeLayout) view.findViewById(R.id.athlete_more_about_unique_rl);
			mCoachAboutSpecificRL = (RelativeLayout) view.findViewById(R.id.coach_more_about_rl);
			mAthleteSportsRL = (RelativeLayout) view.findViewById(R.id.athlete_sports_rl);
	//		mAthleteScoreRL = (RelativeLayout) view.findViewById(R.id.athlete_score_rl);
			mSkipTV = (TextView) view.findViewById(R.id.skip_tv);
			teacherBelowView = view.findViewById(R.id.teacher_below_view);
			favTeacherBelowView = view.findViewById(R.id.fav_team_below_view);
			favQuoteBelowView = view.findViewById(R.id.fav_quote_below_view);
			mDevider18 = view.findViewById(R.id.devider18);
			mDevider19 = view.findViewById(R.id.devider19);
			mSkipTV.setOnClickListener(listener);
			mHeightTV.setOnClickListener(listener);
			
			/*mTranscriptVerifyTV = (TextView) view.findViewById(R.id.upload_transcripts_tv);
			mTranscriptVerifyTV.setOnClickListener(listener);*/
		}
	}

	public void displayHeightDialog() {

		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		dialog.setContentView(R.layout.height_view);
		final Spinner mFeetSpinner = (Spinner) dialog.findViewById(R.id.feet_spinner);
		final Spinner mInchSpinner = (Spinner) dialog.findViewById(R.id.inch_spinner);
		TextView mDoneTV = (TextView) dialog.findViewById(R.id.done_tv);
		try {
			Field popup = Spinner.class.getDeclaredField("mPopup");
			popup.setAccessible(true);

			// Get private mPopup member variable and try cast to ListPopupWindow
			android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(mFeetSpinner);

			// Set popupWindow height to 500px
			popupWindow.setHeight(220);
		}
		catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		try {
			Field popup = Spinner.class.getDeclaredField("mPopup");
			popup.setAccessible(true);

			// Get private mPopup member variable and try cast to ListPopupWindow
			android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(mInchSpinner);

			// Set popupWindow height to 500px
			popupWindow.setHeight(220);
		}
		catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}

		//set previously selected value
		String[] feetsArr = getResources().getStringArray(R.array.height_feet_array);
		for(int k=0;k<feetsArr.length;k++){
			if(feetsArr[k].equals(selectedFeet)){
				mFeetSpinner.setSelection(k);
			}
		}

		String[] inchesArr = getResources().getStringArray(R.array.height_inches_array);
		for(int k=0;k<inchesArr.length;k++){
			if(inchesArr[k].equals(selectedInch)){
				mInchSpinner.setSelection(k);
			}
		}

		mDoneTV.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				selectedFeet = (String) mFeetSpinner.getSelectedItem();
				selectedInch = (String) mInchSpinner.getSelectedItem();
				Log.v("selectedInch", selectedInch);
				Log.v("selectedFeet", selectedFeet);
				mSetupProfileViewHolder.mHeightTV.setText(selectedFeet+" "+selectedInch);
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.skip_tv:
			updateDataAndCallIntent();
			//updateListener();
			/*if (cropImageUri != null) {
				uploadMediaServerRequest(true);
			} else {
				setupProfileSkipFieldsServerRequest(mLogedInUserModel.mUserType, "");
			}*/
			break;

		case R.id.height_tv:
			displayHeightDialog();
			break;
			
	/*	case R.id.upload_transcripts_tv:
			verifyScoresRequest("TRANSCRIPT");*/

		default:
			break;
		}
	}

	/*private void verifyScoresRequest(String verificationType){
		VerifyScoresRequest mRequest = new VerifyScoresRequest(mContext);
		mRequest.getVerifyScoresRequest(verificationType);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					DialogUtil.showOkDialogButtonLisnter(mContext, getResources().getString(R.string.verification_response_email), getResources().getString(R.string.app_name), new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

						}
					});
				}
				else{
					DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name), new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

						}
					});
				}
			}
		});
	}*/

	private void setupProfileServerRequest(int userType, String profilePicName) {

		SetupProfileRequest mRequest = new SetupProfileRequest(mContext);
		String mFavAthlete = mSetupProfileViewHolder.mFavAthleteET.getText().toString().trim();
		String mFavTeam = mSetupProfileViewHolder.mFavTeamET.getText().toString().trim();
		String mFavMovie = mSetupProfileViewHolder.mFavMovieET.getText().toString().trim();
		String mFavMusic = mSetupProfileViewHolder.mFavMusicET.getText().toString().trim();
		String mFavFood = mSetupProfileViewHolder.mFavFoodET.getText().toString().trim();
		String mFavTeacher = mSetupProfileViewHolder.mFavTeacher.getText().toString().trim();
		String mFavQuote = mSetupProfileViewHolder.mFavQuote.getText().toString().trim();
		BaseRequest.setLoginSessionKey(mLogedInUserModel.mLoginSessionKey);
		if (userType == 1) {
			String mCoachingPhilosophy = mSetupProfileViewHolder.mCoachingPhilosophyET.getText().toString().trim();
			String mMentor = mSetupProfileViewHolder.mMentorET.getText().toString().trim();
			String mAlmaMeter = mSetupProfileViewHolder.mAlmaMeterET.getText().toString().trim();

			mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
					mSelectedStateId, mSelectedSportsId, mUserCurrentSchool, mUserCoachingTitle, mCoachingPhilosophy, mMentor, mAlmaMeter,
					mFavAthlete, mFavTeam, mFavMovie, mFavMusic, mFavFood, mFavTeacher, profilePicName);
		} else if (userType == 2) {
			String mSportFavAthleteString = mSetupProfileViewHolder.mSportFavAthlete.getText().toString().trim();
			String mSportFavTeamString = mSetupProfileViewHolder.mSportFavTeam.getText().toString().trim();
			String mPosition = mSetupProfileViewHolder.mSportPositionET.getText().toString().trim();
			String mHomeTeam = mSetupProfileViewHolder.mSportHomeTeamET.getText().toString().trim();

			String mJerseyNumber = mSetupProfileViewHolder.mSportJerseyET.getText().toString().trim();
			String mNameOfCurrentSchool = mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.getText().toString().trim();
			String mNameOfClubTeam = mSetupProfileViewHolder.mSportNameOfClubTeam.getText().toString().trim();

			String mHomeAdress = mSetupProfileViewHolder.mHomeAddressET.getText().toString().trim();
			String mPhoneNumber = mSetupProfileViewHolder.mPhoneNumberET.getText().toString().trim();
			String mAlternateEmail = mSetupProfileViewHolder.mAlternateEmailET.getText().toString().trim();
			/*String mGPA = mSetupProfileViewHolder.mAthleteGPAET.getText().toString().trim();
			String mACT = mSetupProfileViewHolder.mAthleteACTET.getText().toString().trim();
			String mSAT = mSetupProfileViewHolder.mAtheleteSATET.getText().toString().trim();*/
			//	String mHeight = mSetupProfileViewHolder.mHeightTV.getText().toString();
			String mNCAAEligibility = mSetupProfileViewHolder.mNCAAET.getText().toString().trim();
			String mInches = String.valueOf(Utility.convertFeetAndInchToInches(selectedFeet,selectedInch));
		//	String mTranscript = mSetupProfileViewHolder.mAthleteTranscriptsET.getText().toString().trim();
			String mParentID = "";
			String mParentName = mSetupProfileViewHolder.mParentNameET.getText().toString().trim();
			String mParentEmail = mSetupProfileViewHolder.mParentEmailET.getText().toString().trim();
			if(!TextUtils.isEmpty(mParentEmail)){
				if(ValidationUtil.isValidEmail(mParentEmail)){
					String mParentPhone = mSetupProfileViewHolder.mParentHomeET.getText().toString().trim();
					String mParentRelation = "";
					/*Double mFormatedGPA = 0.00;
					if (!TextUtils.isEmpty(mGPA)) {
						try {
							mFormatedGPA = Double.parseDouble(mGPA);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}*/
					mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
							mSelectedStateId, mSelectedSportsId, mPosition, mHomeTeam, mJerseyNumber, mNameOfCurrentSchool, mNameOfClubTeam,
							mSportFavAthleteString, mSportFavTeamString, mFavMovie, mFavMusic, mFavFood, mFavTeacher, profilePicName, mFavQuote, mHomeAdress,
							mPhoneNumber, mAlternateEmail,mUserModel.mUserGPAScore, mUserModel.mUserACTScore, mUserModel.mUserACTScore, "", mNCAAEligibility, mParentID,
							mParentName, mParentEmail, mParentPhone, mParentRelation,mInches);
				}
				else{
					DialogUtil.showOkDialogUncancelableButtonLisnter(mContext, getResources().getString(R.string.parents_email_invalid), getResources().getString(R.string.app_name),new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {
							mSetupProfileViewHolder.mParentEmailET.requestFocus();
							mSetupProfileViewHolder.mParentEmailET.setSelection(mSetupProfileViewHolder.mParentEmailET.getText().toString().length());
						}
					});
				}
			}
			else{
				String mParentPhone = mSetupProfileViewHolder.mParentHomeET.getText().toString().trim();
				String mParentRelation = "";
			//	Double mFormatedGPA = 0.00;
				/*if (!TextUtils.isEmpty(mGPA)) {
					try {
						mFormatedGPA = Double.parseDouble(mGPA);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}*/
				mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
						mSelectedStateId, mSelectedSportsId, mPosition, mHomeTeam, mJerseyNumber, mNameOfCurrentSchool, mNameOfClubTeam,
						mSportFavAthleteString, mSportFavTeamString, mFavMovie, mFavMusic, mFavFood, mFavTeacher, profilePicName, mFavQuote, mHomeAdress,
						mPhoneNumber, mAlternateEmail, mUserModel.mUserGPAScore, mUserModel.mSATScore, mUserModel.mUserACTScore, "", mNCAAEligibility, mParentID,
						mParentName, mParentEmail, mParentPhone, mParentRelation,mInches);
			}
		} else if (userType == 3) {
			mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
					mSelectedStateId, mFavAthlete, mFavTeam, mFavMovie, mFavMusic, mFavFood, mFavTeacher, profilePicName, mFavQuote);
		}
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {

					mDisplayDialog = DialogUtil.showOkDialogUncancelableButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
							new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

							updateDataAndCallIntent();
						}
					});
					mDisplayDialog.setOnKeyListener(new OnKeyListener() {

						@Override
						public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent arg2) {
							if (keyCode == KeyEvent.KEYCODE_BACK) {
								mDisplayDialog.dismiss();
								updateDataAndCallIntent();
							}

							return false;
						}
					});

				} else {
					DialogUtil.showOkDialogUncancelableButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
							new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

						}
					});
				}
			}
		});
	}

	private void updateDataAndCallIntent(){

		Intent intent = null;
		if (mReqType.equals("2")) {
			Intent in = AboutActivity.getIntent(mContext, mUserName, "SetupProfileMandatoryActivity");
			in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(in);
		} else {
			intent = DashboardActivity.getIntent(mContext,3, "");
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
		finish();
	}

	private void setLocalPreferance(){

		mLocalPreferance = getSharedPreferences("LocalPreferance",0);
		Editor mLocalEditor = mLocalPreferance.edit();
		mLocalEditor.putBoolean("isUsed", true);
		mLocalEditor.putString("mHomeAddressET", mSetupProfileViewHolder.mHomeAddressET.getText().toString());
		mLocalEditor.putString("mPhoneNumberET",  mSetupProfileViewHolder.mPhoneNumberET.getText().toString());
		mLocalEditor.putString("mAlternateEmailET", mSetupProfileViewHolder.mAlternateEmailET.getText().toString());
		mLocalEditor.putString("mParentNameET", mSetupProfileViewHolder.mParentNameET.getText().toString());
		mLocalEditor.putString("mParentHomeET", mSetupProfileViewHolder.mParentHomeET.getText().toString());
		mLocalEditor.putString("mParentEmailET", mSetupProfileViewHolder.mParentEmailET.getText().toString());
		mLocalEditor.putString("mCoachingPhilosophyET", mSetupProfileViewHolder.mCoachingPhilosophyET.getText().toString());
		mLocalEditor.putString("mMentorET", mSetupProfileViewHolder.mMentorET.getText().toString());
		mLocalEditor.putString("mAlmaMeterET", mSetupProfileViewHolder.mAlmaMeterET.getText().toString());
		/*mLocalEditor.putString("mAthleteGPAET", mSetupProfileViewHolder.mAthleteGPAET.getText().toString());
		mLocalEditor.putString("mAtheleteSATET", mSetupProfileViewHolder.mAtheleteSATET.getText().toString());
		mLocalEditor.putString("mAthleteACTET", mSetupProfileViewHolder.mAthleteACTET.getText().toString());
		mLocalEditor.putString("mAthleteTranscriptsET", mSetupProfileViewHolder.mAthleteTranscriptsET.getText().toString());*/
		mLocalEditor.putString("mFavAthleteET", mSetupProfileViewHolder.mFavAthleteET.getText().toString());
		mLocalEditor.putString("mFavTeamET", mSetupProfileViewHolder.mFavTeamET.getText().toString());
		mLocalEditor.putString("mFavMovieET", mSetupProfileViewHolder.mFavMovieET.getText().toString());
		mLocalEditor.putString("mFavMusicET", mSetupProfileViewHolder.mFavMusicET.getText().toString());
		mLocalEditor.putString("mFavFoodET", mSetupProfileViewHolder.mFavFoodET.getText().toString());
		mLocalEditor.putString("mFavTeacher", mSetupProfileViewHolder.mFavTeacher.getText().toString());
		mLocalEditor.putString("mFavQuote", mSetupProfileViewHolder.mFavQuote.getText().toString());
		mLocalEditor.putString("mSportPositionET", mSetupProfileViewHolder.mSportPositionET.getText().toString());
		mLocalEditor.putString("mSportHomeTeamET", mSetupProfileViewHolder.mSportHomeTeamET.getText().toString());
		mLocalEditor.putString("mSportJerseyET", mSetupProfileViewHolder.mSportJerseyET.getText().toString());
		mLocalEditor.putString("mSportNameOfCurrentSchoolET", mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.getText().toString());
		mLocalEditor.putString("mSportNameOfClubTeam", mSetupProfileViewHolder.mSportNameOfClubTeam.getText().toString());
		mLocalEditor.putString("mSportFavAthlete", mSetupProfileViewHolder.mSportFavAthlete.getText().toString());
		mLocalEditor.putString("mSportFavTeam", mSetupProfileViewHolder.mSportFavTeam.getText().toString());
		mLocalEditor.putString("mHeightTV", mSetupProfileViewHolder.mHeightTV.getText().toString());
		mLocalEditor.putString("mNCAAET", mSetupProfileViewHolder.mNCAAET.getText().toString());
		mLocalEditor.commit();
	}

	private boolean isLocalPreferance(){
		mLocalPreferance = getSharedPreferences("LocalPreferance",0);
		return mLocalPreferance.getBoolean("isUsed", false);
	}

	private void resetLocalPreferance(){
		mLocalPreferance = getSharedPreferences("LocalPreferance",0);
		Editor mLocalEditor = mLocalPreferance.edit();
		mLocalEditor.putBoolean("isUsed", false);
		mLocalEditor.clear();
		mLocalEditor.commit();
	}

	private void getFromLocalPreferance(){

		mLocalPreferance = getSharedPreferences("LocalPreferance",0);

		mSetupProfileViewHolder.mHomeAddressET.setText(mLocalPreferance.getString("mHomeAddressET", mUserModel.mUserHomeAddress).trim());
		mSetupProfileViewHolder.mPhoneNumberET.setText(mLocalPreferance.getString("mPhoneNumberET", mUserModel.mUserPhoneNumber).trim()); 
		mSetupProfileViewHolder.mAlternateEmailET.setText(mLocalPreferance.getString("mAlternateEmailET", mUserModel.mUserAlternateEmail).trim());
		mSetupProfileViewHolder.mParentNameET.setText(mLocalPreferance.getString("mParentNameET", mUserModel.mUserParentName).trim());
		mSetupProfileViewHolder.mParentHomeET .setText(mLocalPreferance.getString("mParentHomeET", mUserModel.mUserParentPhoneNumber).trim());
		mSetupProfileViewHolder.mParentEmailET.setText(mLocalPreferance.getString("mParentEmailET", mUserModel.mUserParentEmail).trim());
		mSetupProfileViewHolder.mCoachingPhilosophyET.setText(mLocalPreferance.getString("mCoachingPhilosophyET", mUserModel.mUserCoachingPhilosophy).trim());
		mSetupProfileViewHolder.mMentorET.setText(mLocalPreferance.getString("mMentorET", mUserModel.mUserMentor).trim());
		mSetupProfileViewHolder.mAlmaMeterET.setText(mLocalPreferance.getString("mAlmaMeterET", mUserModel.mUserAlmaMater).trim());
		/*mSetupProfileViewHolder.mAthleteGPAET.setText(mLocalPreferance.getString("mAthleteGPAET", mUserModel.mUserGPAScore).trim());
		mSetupProfileViewHolder.mAtheleteSATET.setText(mLocalPreferance.getString("mAtheleteSATET", mUserModel.mSATScore).trim());
		mSetupProfileViewHolder.mAthleteACTET.setText(mLocalPreferance.getString("mAthleteACTET", mUserModel.mUserACTScore).trim());
		mSetupProfileViewHolder.mAthleteTranscriptsET.setText(mLocalPreferance.getString("mAthleteTranscriptsET", mUserModel.mUserTranscripts).trim());
		*/mSetupProfileViewHolder.mFavAthleteET.setText(mLocalPreferance.getString("mFavAthleteET", mUserModel.mUserFavAthelet).trim());
		mSetupProfileViewHolder.mFavTeamET.setText(mLocalPreferance.getString("mFavTeamET", mUserModel.mUserFavTeam).trim());
		mSetupProfileViewHolder.mFavMovieET.setText(mLocalPreferance.getString("mFavMovieET", mUserModel.mUserFavMovie).trim());

		mSetupProfileViewHolder.mFavMusicET.setText(mLocalPreferance.getString("mFavMusicET", mUserModel.mUserFavMusic).trim());
		mSetupProfileViewHolder.mFavFoodET.setText(mLocalPreferance.getString("mFavFoodET", mUserModel.mUserFavFood).trim());
		mSetupProfileViewHolder.mFavTeacher.setText(mLocalPreferance.getString("mFavTeacher", mUserModel.mUserFavTeacher).trim());
		mSetupProfileViewHolder.mFavQuote.setText(mLocalPreferance.getString("mFavQuote", mUserModel.mUserFavQuote).trim());
		mSetupProfileViewHolder.mSportPositionET.setText(mLocalPreferance.getString("mSportPositionET", mUserModel.mUserPosition).trim());
		mSetupProfileViewHolder.mSportHomeTeamET.setText(mLocalPreferance.getString("mSportHomeTeamET", mUserModel.mUserHomeTeam).trim());
		mSetupProfileViewHolder.mSportJerseyET.setText(mLocalPreferance.getString("mSportJerseyET", mUserModel.mUserJerseyNumber).trim());
		mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setText(mLocalPreferance.getString("mSportNameOfCurrentSchoolET", mUserModel.mUserCurrentSchool).trim());
		mSetupProfileViewHolder.mSportNameOfClubTeam.setText(mLocalPreferance.getString("mSportNameOfClubTeam", mUserModel.mUserClubTeam).trim());
		mSetupProfileViewHolder.mSportFavAthlete.setText(mLocalPreferance.getString("mSportFavAthlete", mUserModel.mUserFavAthelet).trim());
		mSetupProfileViewHolder.mSportFavTeam.setText(mLocalPreferance.getString("mSportFavTeam", mUserModel.mUserFavTeam).trim());
		mSetupProfileViewHolder.mHeightTV.setText(mLocalPreferance.getString("mHeightTV", mUserModel.mUserHeight).trim());
		mSetupProfileViewHolder.mNCAAET.setText(mLocalPreferance.getString("mNCAAET", mUserModel.mUserNCAAEligibility).trim());
		resetLocalPreferance();
		setCursor();
	}
}