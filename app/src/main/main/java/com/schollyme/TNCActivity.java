package com.schollyme;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;

/**
 * Activity class. This may be useful in
 * Display TNC for app
 * 
 * @author Ravi Bhandari
 * 
 */
public class TNCActivity extends BaseActivity{
	
	private WebView mWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tnc_activity);
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back,0, getResources().getString(R.string.tnc_header),new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		}, null);
		mWebView = (WebView) findViewById(R.id.web_view);
		mWebView.getSettings().setJavaScriptEnabled(true);

	//	mWebView.loadUrl("file:///android_asset/TERMS_OF_USE.html");
		mWebView.loadUrl("http://14.1.29.130/terms");
	}

	public static Intent getIntent(Context context) {
		Intent intent1 = new Intent(context, TNCActivity.class);
		return intent1;
	}

}
