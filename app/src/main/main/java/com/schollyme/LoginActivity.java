package com.schollyme;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.schollyme.activity.SetupProfileMandatoryActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.LoginRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.LocaleUtil;
import com.vinfotech.utility.Utility;

/**
 * Activity class. This may be useful in Login of app user implementation
 * 
 * @author Ravi Bhandari
 * 
 */
public class LoginActivity extends GCMBaseActivity implements OnClickListener,
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {

	private EditText mEmailEt, mPasswordEt;
	private TextView mForgotPasswordTv, mSignupTv, mSignupHeaderTV,
			mCopyRightTV;
	private Button mLoginBtn;
	private View mDividerView;
	private ImageView mAppLogoIV;
	private RelativeLayout mMainContainerRL;
	private LinearLayout mBottomLL;

	private String mEmailStr, mPassStr;
	private Context mContext;
	private double mCurLat = 0.0, mCurLog = 0.0;
	private static final String TAG = "Signup";
	private LocationRequest locationRequest;
	private GoogleApiClient googleApiClient;
	private FusedLocationProviderApi fusedLocationProviderApi;
	private ErrorLayout mErrorLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LocaleUtil localeUtil = new LocaleUtil(this);
		if(TextUtils.isEmpty(localeUtil.mLocaleCode)) {
			localeUtil.setLocale("en", savedInstanceState);
		}
		else{
			localeUtil.setLocale(localeUtil.mLocaleCode, savedInstanceState);
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
	//	new LocaleUtil(this).setLocale("es",savedInstanceState);
		mContext = this;
		mAppLogoIV = (ImageView) findViewById(R.id.app_icon);
		mCopyRightTV = (TextView) findViewById(R.id.copyright_tv);
		FontLoader.setRobotoRegularTypeface(mCopyRightTV);
		showAnimation();
		mErrorLayout = new ErrorLayout(findViewById(R.id.bg_container_rl));
	}

	private void showAnimation() {
		Animation slideUp = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_center_to_up);
		mAppLogoIV.startAnimation(slideUp);
		slideUp.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				LogedInUserModel model = new LogedInUserModel(mContext);
				if (!model.mLoginStatus) {
					controlInitialization();
					getLocation();
				} else {
					if (model.isProfileSetUp) {
						startActivity(DashboardActivity.getIntent(mContext, 4, ""));
					} else {
						startActivity(SetupProfileMandatoryActivity.getIntent(
								mContext, "1"));
					}
					finish();
				}
			}
		});
	}

	private boolean validateLogin() {
		mEmailStr = mEmailEt.getText().toString();
		mPassStr = mPasswordEt.getText().toString();
		if (TextUtils.isEmpty(mEmailStr)) {
			mErrorLayout.showError(
					getResources().getString(
							R.string.email_username_require_message), true,
					MsgType.Error);
		}else if (TextUtils.isEmpty(mPassStr)) {
			mErrorLayout
					.showError(
							getResources().getString(
									R.string.password_require_message), true,
							MsgType.Error);
		} else {
			return true;
		}

		return false;
	}

	/** init location provide for get user location */
	private void getLocation() {
		try {
			locationRequest = LocationRequest.create();
			locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
			fusedLocationProviderApi = LocationServices.FusedLocationApi;
			googleApiClient = new GoogleApiClient.Builder(this)
					.addApi(LocationServices.API).addConnectionCallbacks(this)
					.build();
			if (googleApiClient != null) {
				googleApiClient.connect();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		try {
			Location location = fusedLocationProviderApi
					.getLastLocation(googleApiClient);
			if (location != null) {
				Log.e(TAG, "onLocationChanged: " + location);
				mCurLat = (double) (location.getLatitude());
				mCurLog = (double) (location.getLongitude());
				googleApiClient.disconnect();
			} else {
				getLocation();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onConnectionSuspended(int arg0) {

	}

	private void controlInitialization() {

		mEmailEt = (EditText) findViewById(R.id.email_et);
		mPasswordEt = (EditText) findViewById(R.id.password_et);
		mDividerView = findViewById(R.id.divider_view1);
		mLoginBtn = (Button) findViewById(R.id.Login_button);
		mForgotPasswordTv = (TextView) findViewById(R.id.forgot_password_tv);
		mSignupTv = (TextView) findViewById(R.id.signup_tv);
		mSignupHeaderTV = (TextView) findViewById(R.id.signup_header_tv);
		mMainContainerRL = (RelativeLayout) findViewById(R.id.bg_container_rl);

		mBottomLL = (LinearLayout) findViewById(R.id.bottom_lll);
		mLoginBtn.setOnClickListener(this);
		mSignupTv.setOnClickListener(this);
		mForgotPasswordTv.setOnClickListener(this);
		mMainContainerRL.setBackgroundResource(R.drawable.bg_login_img);
		mEmailEt.setVisibility(View.VISIBLE);
		mPasswordEt.setVisibility(View.VISIBLE);
		mDividerView.setVisibility(View.VISIBLE);
		mLoginBtn.setVisibility(View.VISIBLE);
		mBottomLL.setVisibility(View.VISIBLE);
		mForgotPasswordTv.setVisibility(View.VISIBLE);
		mCopyRightTV.setVisibility(View.GONE);
		FontLoader.setRobotoRegularTypeface(mEmailEt, mPasswordEt,
				mForgotPasswordTv, mSignupHeaderTV);
		FontLoader.setRobotoBoldTypeface(mSignupTv);
		FontLoader.setRobotoMediumTypeface(mLoginBtn);
		mPasswordEt
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {
							if (validateLogin()) {
								loginRequest();
							}
							return true;
						}
						return false;
					}
				});
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, LoginActivity.class);
		return intent;
	}

	/** ///////......... Listeners ........../////////// **/
	@Override
	public void onClick(View view) {

		switch (view.getId()) {
		case R.id.Login_button:
			if (validateLogin()) {
				loginRequest();
			}
			break;
		case R.id.signup_tv:
			startActivity(SignupActivity.getIntent(mContext));
			overridePendingTransition(R.anim.slide_up_dialog, 0);
			break;
		case R.id.forgot_password_tv:
			Intent intent = ForgotPasswordActivity.getIntent(mContext);
			intent.putExtra("user_input", mEmailEt.getText().toString());
			startActivity(intent);
			break;
		}
	}

	/** /////////// WEB SERVICE IMPLEMENTATION /////////////// **/
	private void loginRequest() {
		LoginRequest loginRequest = new LoginRequest(this);// Utility.md5(mPassStr)
		loginRequest.LoginRequestServer(mEmailStr, mPassStr,
				Config.DEVICE_TYPE, mDeviceToken, String.valueOf(mCurLat),
				String.valueOf(mCurLog), Utility.getIPAddress(true), "");
		loginRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data,
					int totalRecords) {
				if (success) {
					LogedInUserModel model = (LogedInUserModel) data;

					Intent intent;
					if (model.mUserStatusId == 1) {
						model.updatePreference(mContext, "mLoginStatus", false);
						intent = new Intent(mContext,
								VerifyAccountActivity.class);
						startActivity(intent);
					} else {
						model.updatePreference(mContext, "mLoginStatus", true);
						BaseRequest.setLoginSessionKey(new LogedInUserModel(
								mContext).mLoginSessionKey);
						if (model.isProfileSetUp) {
							intent = DashboardActivity.getIntent(mContext,4, "");
						} else {
							intent = SetupProfileMandatoryActivity.getIntent(
									mContext, "1");
						}

						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_CLEAR_TASK
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
						finish();
					}
				} else {
					String message = data.toString();
					if (TextUtils.isEmpty(message)) {
						DialogUtil.showOkDialog(
								mContext,
								getResources().getString(
										R.string.login_server_error_message),
								getResources().getString(R.string.app_name));
					} else {
						DialogUtil.showOkDialog(mContext, message,
								getResources().getString(R.string.app_name));
					}
				}
			}
		});
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {

	}
}