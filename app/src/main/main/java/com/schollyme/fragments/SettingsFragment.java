package com.schollyme.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.Config;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.activity.ChangePasswordActivity;
import com.schollyme.activity.ChangeUserTypeActivity;
import com.schollyme.activity.LanguageChangeActivity;
import com.vinfotech.utility.FontLoader;

public class SettingsFragment extends BaseFragment implements OnClickListener{
	
	private Context mContext;
	private TextView mNotificationTV,mChangePasswordTV,mChangeLanguageTV,mChangeUserTypeTV;
	private Switch mSwitch;

	private SharedPreferences mNotificationPreferance;
//	private EditText mNotificationET,mChangePasswordET,mChangeLanguageET;

	public static BaseFragment getInstance() {
		SettingsFragment mSettingsFragment = new SettingsFragment();
		return mSettingsFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.settings_fragment_layout, container, false);
		BaseFragmentActivity.setHeader(R.drawable.icon_menu, 0, getResources().getString(R.string.settings), new OnClickListener() {

			@Override
			public void onClick(View v) {
					((DashboardActivity) mContext).sliderListener();
			}
		}, null);
		DashboardActivity.hideShowActionBar(true);
		mChangeLanguageTV = (TextView)view.findViewById(R.id.language_change_tv);
		mChangePasswordTV = (TextView)view.findViewById(R.id.change_password_tv);
		mNotificationTV = (TextView)view.findViewById(R.id.notification_tv);
		mChangeUserTypeTV = (TextView)view.findViewById(R.id.change_usertype_tv);
		mSwitch = (Switch) view.findViewById(R.id.notification_switch);
		mContext = getActivity();
		mChangeLanguageTV.setOnClickListener(this);
		mChangePasswordTV.setOnClickListener(this);
		mNotificationTV.setOnClickListener(this);
		mChangeUserTypeTV.setOnClickListener(this);
		FontLoader.setRobotoRegularTypeface(mNotificationTV, mChangePasswordTV, mChangeLanguageTV,mChangeUserTypeTV);
		mNotificationPreferance = mContext.getSharedPreferences(Config.NOTIFICATION_SETTINGS,0);
		if(mNotificationPreferance.getBoolean("isOn",true)){
			mSwitch.setChecked(true);
		}
		else{
			mSwitch.setChecked(false);
		}
		mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				if (isChecked) {
					updateNotificationSettings(true);
				} else {
					updateNotificationSettings(false);
				}
			}
		});

		return view;
	}

	private void updateNotificationSettings(boolean setting){
		SharedPreferences.Editor editor = mNotificationPreferance.edit();
		editor.putBoolean("isOn",setting);
		editor.commit();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.change_password_tv:
				startActivity(ChangePasswordActivity.getIntent(mContext));
				break;
			case R.id.language_change_tv:
				startActivity(LanguageChangeActivity.getIntent(mContext));
				break;
			case R.id.notification_tv:
				break;
			case R.id.change_usertype_tv:
				startActivity(ChangeUserTypeActivity.getIntent(mContext));
				break;
		}
	}


	@Override
	public void onLocaleUpdate() {
		super.onLocaleUpdate();
		setLocalChanges();
	}


	private void setLocalChanges(){
		BaseFragmentActivity.setHeader(R.drawable.icon_menu, 0, getResources().getString(R.string.settings), new OnClickListener() {

			@Override
			public void onClick(View v) {
				((DashboardActivity) mContext).sliderListener();
			}
		}, null);
		mNotificationTV.setText(getResources().getString(R.string.notifications));
		mChangePasswordTV.setText(getResources().getString(R.string.change_password_setting));
		mChangeLanguageTV.setText(getResources().getString(R.string.language));

		if(mSwitch.isChecked()){
			mSwitch.setTextOn(getResources().getString(R.string.on));
		}
		else{
			mSwitch.setTextOff(getResources().getString(R.string.off));
		}

	}

}