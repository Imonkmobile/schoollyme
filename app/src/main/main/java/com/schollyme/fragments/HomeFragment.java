package com.schollyme.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.Config;
import com.schollyme.CreateWallPostActivity;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.activity.FriendProfileActivity;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.adapter.PostFeedAdapter.OnItemClickListenerPost;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.friends.FriendsActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

public class HomeFragment extends BaseFragment implements OnClickListener {
	public TextView mFindFriendTV, mGrowNetworkTV;
	public LogedInUserModel mLogedInUserModel;
	private NewsFeedRequest mNewsFeedRequest;
	private NFFilter mNFFilter;
	private PostFeedAdapter mPostFeedAdapter;
	private List<NewsFeed> mNewsFeeds = new ArrayList<>();
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private Context mContext;
	private ErrorLayout mErrorLayout;
	private String mActivityGUID = "",mTeamGUID;
	private int mNotificationType;
	private String mFromActivity;

	final private int REQ_FROM_HOME_LIKELIST = 5555;
	final private int REQ_FROM_HOME_COMMENTLIST = 5556;
	final private int REQ_FROM_HOME_WRITE_COMMENT = 58554;

	public static HomeFragment getInstance(int ModuleID, String EntityGUID) {
		HomeFragment homeFragment = new HomeFragment();
		homeFragment.mNFFilter = new NFFilter(ModuleID, EntityGUID);
		homeFragment.mNFFilter.AllActivity = NewsFeedRequest.ALL_ACTIVITY_NEWS_FEED;
		return homeFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		mLogedInUserModel = new LogedInUserModel(mContext);
	}

	private ListView genericLv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.home_fragment, container, false);
		mFromActivity = getArguments().getString("fromActivity");
		mActivityGUID = getArguments().getString("mActivityGUID");
		mTeamGUID = getArguments().getString("mTeamGUID");
		mNotificationType  = getArguments().getInt("mNotificationType");
		return view;
	}

	private ProgressBar mLoadingCenter;

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mNewsFeeds.add(new NewsFeed(null));
		initialization(view);
		
		mLoadingCenter.post(new Runnable() {
			
			@Override
			public void run() {
				if(null==mActivityGUID || TextUtils.isEmpty(mActivityGUID)){
					if(null!=mNFFilter)
					getFeedRequest(mNFFilter);
				}
				else{
					mNFFilter.ActivityGUID = mActivityGUID;
					mNFFilter.EntityGUID = mTeamGUID;
					if(mNotificationType==1){
						mNFFilter.ModuleID = 1;
					}
					else{
						mNFFilter.ModuleID = 3;
					}
					
					mNFFilter.AllActivity = 0;
					getFeedRequest(mNFFilter);
					
					mNFFilter.AllActivity = 1;
					mNFFilter.ModuleID = 3;
				}
			//	getFeedRequest(mNFFilter);
			}
		});
	}

	private View mFooterRL;
	int ClickLocation = 0;

	public void ItemClickListner(int ID, int position, NewsFeed newsFeed) {
		ClickLocation = position;
		switch (ID) {
		case R.id.share_tv:

			if (newsFeed.ShareAllowed == 1) {
				sharePost(newsFeed);
			} else {
				Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
			}
			break;

		case R.id.like_tv:
			LikeMediaToggleService(newsFeed, position);
			break;
		case R.id.likes_tv:
			if (newsFeed.NoOfLikes != 0) {
				startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID, "ACTIVITY", "WALL"),
						REQ_FROM_HOME_LIKELIST);
				getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
			}
			break;

		case R.id.comment_tv: {

			getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
			AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
			String captionForWriteCommentScreen = "";
			if (!newsFeed.PostContent.equals("")) {
				captionForWriteCommentScreen = newsFeed.PostContent;
			} else {

				if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
					if (!newsFeed.Album.AlbumMedias.get(0).ImageName.equals("")) {

						captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).Caption;
					} else {
						captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
					}
				}
			}

			startActivityForResult(WriteCommentActivity.getIntent(getActivity(), albumMedia, "ACTIVITY", captionForWriteCommentScreen),
					REQ_FROM_HOME_WRITE_COMMENT);
			getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);

		}
			break;
		case R.id.comments_tv:

			AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
			String captionForWriteCommentScreen = "";
			if (!newsFeed.PostContent.equals("")) {
				captionForWriteCommentScreen = newsFeed.PostContent;
			} else {

				if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
				}

			}

			startActivityForResult(CommentListActivity.getIntent(mContext, albumMedia, "WALLPOST", captionForWriteCommentScreen),
					REQ_FROM_HOME_COMMENTLIST);
			getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

			break;

		case PostFeedAdapter.ConvertViewID:

			startActivityForResult(
					CreateWallPostActivity.getIntent(mContext, WallPostCreateRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID, true),
					CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
			break;

		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.find_friends_tv:
			startActivity(FriendsActivity.getIntent(v.getContext(), 0, ""));
			break;

		default:
			break;
		}
	}

	private LinearLayout mEmptyLL;

	// inir_ll
	public void initialization(View view) {
		DashboardActivity.hideShowActionBar(true);
		BaseFragmentActivity.setHeader(R.drawable.icon_menu, 0, getResources().getString(R.string.Feed), new OnClickListener() {
			@Override
			public void onClick(View v) {
				((DashboardActivity)mContext).sliderListener();
			}
		}, null);
		mFindFriendTV = (TextView) view.findViewById(R.id.find_friends_tv);
		mGrowNetworkTV = (TextView) view.findViewById(R.id.grow_network_tv);
		mErrorLayout = new ErrorLayout(view.findViewById(R.id.bg_container_rl));
		mFindFriendTV.setOnClickListener(this);
		mEmptyLL = (LinearLayout) view.findViewById(R.id.inir_ll);
		mLoadingCenter = (ProgressBar) view.findViewById(R.id.loading_center_pb);
		mPostFeedAdapter = new PostFeedAdapter(view.getContext(), new OnItemClickListenerPost() {

			@Override
			public void onClickItems(int ID, int position, NewsFeed newsFeed) {
				ItemClickListner(ID, position, newsFeed);
			}
		}, mLogedInUserModel.mUserGUID);
		mNewsFeedRequest = new NewsFeedRequest(view.getContext());
		genericLv = (ListView) view.findViewById(R.id.generic_lv);

		mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);
		mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
		mNoMoreTV.setVisibility(View.INVISIBLE);
		mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
		mLoaderBottomPB.setVisibility(View.INVISIBLE);
		mLoaderBottomPB.getIndeterminateDrawable()
				.setColorFilter(getResources().getColor(R.color.red_normal_color), PorterDuff.Mode.SRC_IN);
		genericLv.addFooterView(mFooterRL);
		// mFooterRL.setVisibility(visibility);
		genericLv.setAdapter(mPostFeedAdapter);
		/*
		 * mGrowNetworkTV.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { startActivityForResult(
		 * CreateWallPostActivity.getIntent(v.getContext(),
		 * WallPostCreateRequest.MODULE_ID_USERS),
		 * CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY); } });
		 */
		FontLoader.setRobotoRegularTypeface(mNoMoreTV,mGrowNetworkTV,mFindFriendTV);

		genericLv.setOnScrollListener(new MyCustomonScrol());

		mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
		// mSwipeRefreshLayout.setVisibility(View.GONE);
		mSwipeRefreshLayout.setColorScheme(R.color.red_normal_color);

		mSwipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
				getFeedRequest(mNFFilter);
			}
		});

	}

	public void sharePost(NewsFeed newsFeedModel) {
		SharePostRequest mSharePostRequest = new SharePostRequest(mContext);

	
		mSharePostRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {
					mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, MsgType.Success);

				} else {
					mErrorLayout.showError((String) data, true, MsgType.Error);
				}
			}
		});
		mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
				NewsFeedRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID, 1, 1);
	}

	private void resetList() {

		mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
		mNewsFeeds.clear();
		mNewsFeeds.add(new NewsFeed(null));
		mPostFeedAdapter.setList(mNewsFeeds);
	}

	private TextView mNoMoreTV;
	private ProgressBar mLoaderBottomPB;

	private void getFeedRequest(final NFFilter nffilter) {

		if (getConnectivityStatus(mContext) == 0) {
			mSwipeRefreshLayout.setRefreshing(false);
		}
		isLoading = false;
		mNoMoreTV.setVisibility(View.INVISIBLE);
		
		if(mSwipeRefreshLayout.isRefreshing()){
			mNewsFeedRequest.setLoader(null);
		} else {
			mNewsFeedRequest.setLoader(nffilter.PageNo <= 1 ? mLoadingCenter : mLoaderBottomPB);
		}

		
		mNewsFeedRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mSwipeRefreshLayout.setRefreshing(false);

				if (success) {

					if (null != data) {
						if (mNFFilter.PageNo == 1) {
							resetList();
						}
						List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
						mNewsFeeds.addAll(newsFeeds);
						mPostFeedAdapter.setList(mNewsFeeds);

						if (totalRecords == 0) {
							isLoading = false;
							mEmptyLL.setVisibility(View.VISIBLE);
							if(mActivityGUID!=null && !TextUtils.isEmpty(mActivityGUID)){
								mFindFriendTV.setText(getString(R.string.no_feeds_to_show));
								mGrowNetworkTV.setVisibility(View.GONE);
							}
							else{
								mFindFriendTV.setText(getString(R.string.find_friends));
								mGrowNetworkTV.setVisibility(View.VISIBLE);
							}

						} else if (mNewsFeeds.size() < totalRecords) {
							mEmptyLL.setVisibility(View.GONE);
							isLoading = true;
							int newPage = mNFFilter.PageNo + 1;
							mNFFilter.PageNo = newPage;
							mEmptyLL.setVisibility(View.GONE);

						} else {
							mEmptyLL.setVisibility(View.GONE);
							isLoading = false;
							mNoMoreTV.setVisibility(View.VISIBLE);
							mSwipeRefreshLayout.setRefreshing(false);
						}

						if (mNFFilter.PageNo == 1) {
							mNoMoreTV.setVisibility(View.INVISIBLE);
						} else {
							mNoMoreTV.setVisibility(View.VISIBLE);
						}

					}
				}else{
					isLoading = false;
					mEmptyLL.setVisibility(View.GONE);
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
				}
				mSwipeRefreshLayout.setRefreshing(false);
				mActivityGUID = "";
			}
		});
		mNewsFeedRequest.getNewsFeedListInServer(nffilter);
	}

	public int getConnectivityStatus(Context context) {
		if (null == context) {
			return 0;
		}
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
 	NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
 	if (null != activeNetwork && activeNetwork.isConnected()) {
 		if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
				return 1;
			}

		if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
				return 2;
			}
		}
		return 0;
	}

	@Override
	public void onStart() {
		mNewsFeedRequest.setActivityStatus(true);
		super.onStart();
	}

	@Override
	public void onStop() {
		mNewsFeedRequest.setActivityStatus(false);
		super.onStop();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		mNewsFeedRequest.setActivityStatus(true);

		switch (requestCode) {
		case REQ_FROM_HOME_LIKELIST:
			if (resultCode == getActivity().RESULT_OK) {
				if (data != null) {
					mNewsFeeds.get(ClickLocation).NoOfLikes = data.getIntExtra("COUNT", 0);
					mNewsFeeds.get(ClickLocation).IsLike = data.getIntExtra("BUTTONSTATUS", 0);
					mPostFeedAdapter.setList(mNewsFeeds);
				}
			}
			break;
		case REQ_FROM_HOME_COMMENTLIST:
			if (resultCode == getActivity().RESULT_OK) {
				if (data != null) {
					mNewsFeeds.get(ClickLocation).NoOfComments = data.getIntExtra("COUNT", 0);
					mPostFeedAdapter.setList(mNewsFeeds);
				}

			}
			break;

		case CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY:
			if (resultCode == getActivity().RESULT_OK) {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						resetList();
						getFeedRequest(mNFFilter);
					}
				}, 500);
			}
			break;

		case REQ_FROM_HOME_WRITE_COMMENT:
			if (resultCode == getActivity().RESULT_OK) {
				mNewsFeeds.get(ClickLocation).NoOfComments++;
				mPostFeedAdapter.setList(mNewsFeeds);
			}

		default:
			break;
		}

		super.onActivityResult(requestCode, resultCode, data);

	}

	private ToggleLikeRequest mToggleLikeRequest;

	public void LikeMediaToggleService(final NewsFeed newsFeed, final int position) {
		mToggleLikeRequest = new ToggleLikeRequest(mContext);

		mToggleLikeRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {

					if (newsFeed.IsLike == 0) {

						newsFeed.IsLike = 1;
						newsFeed.NoOfLikes++;
					} else {

						newsFeed.IsLike = 0;
						newsFeed.NoOfLikes--;
					}
					mPostFeedAdapter.notifyDataSetChanged();

				} else {
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
				}

			}
		});
		mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");
	}

	boolean isLoading;

	class MyCustomonScrol implements OnScrollListener {
		private int mLastFirstVisibleItem;

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			int k = visibleItemCount + firstVisibleItem;

			if (k >= totalItemCount && isLoading && totalItemCount != 0) {
				getFeedRequest(mNFFilter);
			}
			if (mLastFirstVisibleItem < firstVisibleItem) {
				Log.i("SCROLLING DOWN", "TRUE");
			}
			if (mLastFirstVisibleItem > firstVisibleItem) {
				Log.i("SCROLLING UP", "TRUE");
			}
			mLastFirstVisibleItem = firstVisibleItem;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (null != FriendProfileActivity.sLastProfileLink) {
			FriendProfileActivity.sLastProfileLink = null;
		}

	}
}