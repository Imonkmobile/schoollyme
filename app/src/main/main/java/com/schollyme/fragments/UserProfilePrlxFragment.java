package com.schollyme.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.BuzzActivity;
import com.schollyme.Config;
import com.schollyme.CreateWallPostActivity;
import com.schollyme.DashboardActivity;
import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.SchollyMeApplication.SongLoadListener;
import com.schollyme.activity.AboutActivity;
import com.schollyme.activity.ProfileImageViewerActivity;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.adapter.PostFeedAdapter.OnItemClickListenerPost;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.evaluation.MySportsListActivity;
import com.schollyme.friends.FriendsActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.media.AlbumGridActivity;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.schollyme.model.Song;
import com.schollyme.model.UserModel;
import com.schollyme.scores.MyScoresActivity;
import com.schollyme.team.TeamActivity;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.SongOfTheDayRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

import java.util.ArrayList;
import java.util.List;

public class UserProfilePrlxFragment extends BaseFragment implements OnClickListener, OnRefreshListener {

	private UserProfileActivityViewHolder mUserProfileActivityViewHolder;

	private Context mContext;
	private ErrorLayout mErrorLayout;
	private NFFilter mNFFilter;
	private PostFeedAdapter mPostFeedAdapter;
	private ListView genericLv;
	private List<NewsFeed> mNewsFeeds = new ArrayList<NewsFeed>();
	LogedInUserModel loggedinUserModel;
	final private int REQ_FROM_HOME_LIKELIST = 5555;
	final private int REQ_FROM_HOME_COMMENTLIST = 5556;
	final private int REQ_FROM_HOME_WRITE_COMMENT = 58554;
	private View mViewMain;
	private SwipeRefreshLayout mSwipeRefreshWidget;

	private TextView mNoMoreTV;
	private ProgressBar mLoaderBottomPB, mLoadingCenter;
	private String mActivityGUID = "";
	private UserModel mUserModel;
	

	public static UserProfilePrlxFragment getInstance() {
		
		UserProfilePrlxFragment homeFragment = new UserProfilePrlxFragment();
		return homeFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		loggedinUserModel = new LogedInUserModel(mContext);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mViewMain = inflater.inflate(R.layout.user_profile_fragment_layout, null);
		mActivityGUID = getArguments().getString("mActivityGUID");
		BaseFragmentActivity.setHeader(R.drawable.icon_menu, R.drawable.selector_song_of_day, "", new OnClickListener() {
			@Override
			public void onClick(View v) {
				((DashboardActivity)mContext).sliderListener();
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				fetchSongIfNeeded(v);
			}
		});
		return mViewMain;
	}

	private View mFooterRL;
	private View mBlankView;
	private TextView mNoFeedTV;

	@SuppressWarnings("deprecation")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mUserProfileActivityViewHolder = new UserProfileActivityViewHolder(view.findViewById(R.id.main_rl), this);

		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.red_normal_color);

		mSwipeRefreshWidget.setOnRefreshListener(this);
		mErrorLayout = new ErrorLayout(view.findViewById(R.id.profile_image_name_rl));

		String mUserTypeName = "";
		if (loggedinUserModel.mUserType == 1) {
			mUserTypeName = String.valueOf(Config.UserType.Coach);
		} else if (loggedinUserModel.mUserType == 2) {
			mUserTypeName = String.valueOf(Config.UserType.Athlete);
		} else if (loggedinUserModel.mUserType == 3) {
			mUserTypeName = String.valueOf(Config.UserType.Fan);
		}
		mBlankView = (View) view.findViewById(R.id.blankView);
		mNoFeedTV = (TextView) view.findViewById(R.id.no_feed_tv);

		mLoadingCenter = (ProgressBar) view.findViewById(R.id.loading_center_pb);
		mUserProfileActivityViewHolder.mUserType.setText(mUserTypeName);
		mUserProfileActivityViewHolder.mUserName.setText(loggedinUserModel.mFirstName + " " + loggedinUserModel.mLastName);
		String imageUrl = Config.IMAGE_URL_PROFILE + loggedinUserModel.mUserPicURL;
		ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mUserProfileActivityViewHolder.mProfileImageIv,
				ImageLoaderUniversal.option_Round_Image);
		if(!TextUtils.isEmpty(loggedinUserModel.mUserPicURL)){
			final String mOUrl = Config.IMAGE_URL_PROFILE_ORIGINAL+loggedinUserModel.mUserPicURL;
			mUserProfileActivityViewHolder.mProfileImageIv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					startActivity(ProfileImageViewerActivity.getIntent(mContext,mOUrl));
				}
			});
		}

		mNewsFeeds.add(new NewsFeed(null));
		mNFFilter = new NFFilter(NewsFeedRequest.MODULE_ID_USERS, loggedinUserModel.mUserGUID);
		mNFFilter.ActivityGUID = ((DashboardActivity) mContext).mPostNotiLink;

		mPostFeedAdapter = new PostFeedAdapter(mContext, new OnItemClickListenerPost() {

			@Override
			public void onClickItems(int ID, int position, NewsFeed newsFeed) {
				ItemClickListner(ID, position, newsFeed);
			}
		}, loggedinUserModel.mUserGUID);
		mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);
		mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
		mNoMoreTV.setVisibility(View.INVISIBLE);
		mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
		mLoaderBottomPB.setVisibility(View.INVISIBLE);
		mLoaderBottomPB.getIndeterminateDrawable()
				.setColorFilter(getResources().getColor(R.color.red_normal_color), PorterDuff.Mode.SRC_IN);

		genericLv = (ListView) view.findViewById(R.id.genric_lv);
		genericLv.addFooterView(mFooterRL);
		genericLv.setAdapter(mPostFeedAdapter);
		genericLv.setOnScrollListener(new MyCustomonScrol());
		if(null==mActivityGUID || TextUtils.isEmpty(mActivityGUID)){
			getFeedRequest(mNFFilter);
		}
		else{
			mNFFilter.ActivityGUID = mActivityGUID;
			getFeedRequest(mNFFilter);
			mActivityGUID = "";
		}
		

		FontLoader.setRobotoMediumTypeface(mUserProfileActivityViewHolder.mUserName, mUserProfileActivityViewHolder.mAboutTabTV,
				mUserProfileActivityViewHolder.mMediaTabTV, mUserProfileActivityViewHolder.mFriendsTabTV,
				mUserProfileActivityViewHolder.mTeamTabTV, mUserProfileActivityViewHolder.mMoreTv);
		FontLoader.setRobotoRegularTypeface(mNoFeedTV, mUserProfileActivityViewHolder.mUserType,
				mUserProfileActivityViewHolder.mFriendActionTV);
	}

	private Song mSODSong = null;

	private void fetchSongIfNeeded(final View view) {
		if (null == mSODSong) {
			BaseFragmentActivity.showLoader(true);
			SongOfTheDayRequest songOfTheDayRequest = new SongOfTheDayRequest(view.getContext());
			songOfTheDayRequest.setRequestListener(new RequestListener() {

				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					if (success) {
						if (null != data && data instanceof Song) {
							mSODSong = (Song) data;
							handleSongPlay(view, mSODSong);
						}
					} else {
						BaseFragmentActivity.showLoader(false);
						mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true,
								MsgType.Error);
					}
				}
			});
			songOfTheDayRequest.getSongOfDayFromServer("");
		} else if(null != getActivity()){
			handleSongPlay(view, mSODSong);
		}
	}

	private void handleSongPlay(final View v, final Song song) {
		final String songPlayUrl = TextUtils.isEmpty(song.SongURL) ? song.SongPreviewURL : song.SongURL;
		if (TextUtils.isEmpty(songPlayUrl) || !android.util.Patterns.WEB_URL.matcher(songPlayUrl).matches()) {
			BaseFragmentActivity.showLoader(false);
			mErrorLayout.showError(v.getResources().getString(R.string.No_Song_Found), true, MsgType.Error);
			return;
		}

		SchollyMeApplication schollyMeApplication = (SchollyMeApplication) getActivity().getApplication();
		if (schollyMeApplication.isPlaying()) {
			BaseFragmentActivity.showLoader(false);
			schollyMeApplication.pauseSong();
		} else if (schollyMeApplication.playSong(song.Title, song.SongURL, new SongLoadListener() {

			@Override
			public void onLoad(boolean success, String url, MediaPlayer mediaPlayer) {
				BaseFragmentActivity.showLoader(false);
				if (!success && song.SongURL.equalsIgnoreCase(url)) {
					mErrorLayout.showError(v.getResources().getString(R.string.This_song_can_not), true, MsgType.Error);
				}
			}
		})) {
			//v.setEnabled(false);
		} else if (null != mErrorLayout && schollyMeApplication.isBuffering()) {
			BaseFragmentActivity.showLoader(false);
			mErrorLayout.showError(v.getResources().getString(R.string.Buffering), true, MsgType.Error);
		} else if (null != mErrorLayout) {
			BaseFragmentActivity.showLoader(false);
			mErrorLayout.showError(v.getResources().getString(R.string.Invalid_song_URL), true, MsgType.Error);
		}
	}

	NewsFeedRequest mNewsFeedRequest;

	private void getFeedRequest(final NFFilter nffilter) {
		mNewsFeedRequest = new NewsFeedRequest(mContext);
		mNewsFeedRequest.getNewsFeedListInServer(nffilter);
		mNoMoreTV.setVisibility(View.INVISIBLE);
		if (nffilter.PageNo == 1) {
			if (mPostFeedAdapter.getCount() <= 1 || mPostFeedAdapter == null) {
				mNewsFeedRequest.setLoader(mLoadingCenter);
			} else {
				mNewsFeedRequest.setLoader(mBlankView);
			}

			mLoaderBottomPB.setVisibility(View.INVISIBLE);
		} else {
			mNewsFeedRequest.setLoader(mLoaderBottomPB);
			mLoaderBottomPB.setVisibility(View.VISIBLE);
		}

		mNewsFeedRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mSwipeRefreshWidget.setRefreshing(false);
				if (success) {

					if (null != data) {

						if (mNFFilter.PageNo == 1) {
							resetList();
						}
						List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
						mNewsFeeds.addAll(newsFeeds);
						mPostFeedAdapter.setList(mNewsFeeds);

						if (totalRecords == 0) {
							isLoading = false;
							mNoMoreTV.setVisibility(View.INVISIBLE);
							mNoFeedTV.setVisibility(View.VISIBLE);

						} else if (mNewsFeeds.size() <= totalRecords) {

							isLoading = true;
							int newPage = mNFFilter.PageNo + 1;
							mNFFilter.PageNo = newPage;
							mNoFeedTV.setVisibility(View.GONE);
							mNoMoreTV.setVisibility(View.VISIBLE);
						} else {
							mNoMoreTV.setVisibility(View.VISIBLE);
							isLoading = false;
							mSwipeRefreshWidget.setRefreshing(false);
						}
						if (mNFFilter.PageNo == 1) {
							mNoMoreTV.setVisibility(View.INVISIBLE);
						} else {
							mNoMoreTV.setVisibility(View.VISIBLE);
						}
					}
				} else {
					isLoading = false;
					DialogUtil.showOkDialog(mContext, (String) data, "");
				}
			}
		});
	}

	int ClickLocation = 0;

	public void ItemClickListner(int ID, int position, NewsFeed newsFeed) {
		ClickLocation = position;
		switch (ID) {
		case R.id.share_tv:

			if (newsFeed.ShareAllowed == 1) {
				sharePost(newsFeed);
			} else {
				Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
			}
			break;

		case R.id.like_tv:
			LikeMediaToggleService(newsFeed, position);
			break;
		case R.id.likes_tv:
			if (newsFeed.NoOfLikes != 0) {
				startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID, "ACTIVITY", "WALL"),
						REQ_FROM_HOME_LIKELIST);
				getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
			}
			break;

		case R.id.comment_tv: {
			getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
			AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
			String captionForWriteCommentScreen = "";
			if (!newsFeed.PostContent.equals("")) {
				captionForWriteCommentScreen = newsFeed.PostContent;
			} else {

				if (!newsFeed.Album.AlbumMedias.get(0).ImageName.equals("")) {

					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).Caption;
				} else {
					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
				}

			}

			startActivityForResult(WriteCommentActivity.getIntent(getActivity(), albumMedia, "ACTIVITY", captionForWriteCommentScreen),
					REQ_FROM_HOME_WRITE_COMMENT);
			getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
		}
			break;
		case R.id.comments_tv:

			AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
			String captionForWriteCommentScreen = "";
			if (!newsFeed.PostContent.equals("")) {
				captionForWriteCommentScreen = newsFeed.PostContent;
			} else {

				if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
				}

			}

			startActivityForResult(CommentListActivity.getIntent(mContext, albumMedia, "WALLPOST", captionForWriteCommentScreen),
					REQ_FROM_HOME_COMMENTLIST);
			getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

			break;

		case PostFeedAdapter.ConvertViewID:

			startActivityForResult(
					CreateWallPostActivity.getIntent(mContext, WallPostCreateRequest.MODULE_ID_USERS, loggedinUserModel.mUserGUID, true),
					CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
			break;
		default:
			break;
		}
	}

	boolean isLoading;

	class MyCustomonScrol implements OnScrollListener {
		private int mLastFirstVisibleItem;

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			int k = visibleItemCount + firstVisibleItem;

			if (k >= totalItemCount && isLoading && totalItemCount != 0) {
				getFeedRequest(mNFFilter);
			} else {
				Log.v("onScroll", "Inside Else");
			}

			if (mLastFirstVisibleItem < firstVisibleItem) {
				Log.i("SCROLLING DOWN", "TRUE");
			}
			if (mLastFirstVisibleItem > firstVisibleItem) {
				Log.i("SCROLLING UP", "TRUE");
			}

			mLastFirstVisibleItem = firstVisibleItem;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		mNewsFeedRequest.setActivityStatus(true);

		switch (requestCode) {
		case REQ_FROM_HOME_LIKELIST:
			if (resultCode == getActivity().RESULT_OK) {
				if (data != null) {
					mNewsFeeds.get(ClickLocation).NoOfLikes = data.getIntExtra("COUNT", 0);
					mNewsFeeds.get(ClickLocation).IsLike = data.getIntExtra("BUTTONSTATUS", 0);
					mPostFeedAdapter.setList(mNewsFeeds);
				}
			}
			break;
		case REQ_FROM_HOME_COMMENTLIST:
			if (resultCode == getActivity().RESULT_OK) {
				if (data != null) {
					mNewsFeeds.get(ClickLocation).NoOfComments = data.getIntExtra("COUNT", 0);
					mPostFeedAdapter.setList(mNewsFeeds);
				}
			}
			break;

		case CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY:
			if (resultCode == getActivity().RESULT_OK) {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						mNFFilter.PageNo = 1;
						getFeedRequest(mNFFilter);
					}
				}, 500);
			}
			break;

		case REQ_FROM_HOME_WRITE_COMMENT:
			if (resultCode == getActivity().RESULT_OK) {
				mNewsFeeds.get(ClickLocation).NoOfComments++;
				mPostFeedAdapter.setList(mNewsFeeds);
			}

			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void resetList() {
		mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
		mNewsFeeds.clear();
		mNewsFeeds.add(new NewsFeed(null));
		mPostFeedAdapter.setList(mNewsFeeds);
	}

	public void LikeMediaToggleService(final NewsFeed newsFeed, final int position) {
		ToggleLikeRequest mToggleLikeRequest = new ToggleLikeRequest(mContext);
		mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");

		mToggleLikeRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {

					if (newsFeed.IsLike == 0) {

						newsFeed.IsLike = 1;
						newsFeed.NoOfLikes++;
					} else {

						newsFeed.IsLike = 0;
						newsFeed.NoOfLikes--;
					}
					mPostFeedAdapter.notifyDataSetChanged();

				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), "");
				}

			}
		});
	}

	public void sharePost(NewsFeed newsFeedModel) {
		SharePostRequest mSharePostRequest = new SharePostRequest(mContext);

		mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
				NewsFeedRequest.MODULE_ID_USERS, loggedinUserModel.mUserGUID, 1, 1);
		mSharePostRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {
					mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, MsgType.Success);
				} else {
					mErrorLayout.showError((String) data, true, MsgType.Error);
				}
			}
		});
	}

	private void setUserData(UserModel mModel) {

		mUserProfileActivityViewHolder.mUserName.setText(mModel.mUserFullName);
		String mUserType = mModel.mUserTypeID;
		String mUserTypeName = "";
		if (mUserType.equals("1")) {
			mUserTypeName = String.valueOf(Config.UserType.Coach);
		} else if (mUserType.equals("2")) {
			mUserTypeName = String.valueOf(Config.UserType.Athlete);
		} else if (mUserType.equals("3")) {
			mUserTypeName = String.valueOf(Config.UserType.Fan);
		}
		mUserProfileActivityViewHolder.mUserType.setText(mUserTypeName);
		String imageUrl = Config.IMAGE_URL_PROFILE + mModel.mUserProfilePic;
		ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mUserProfileActivityViewHolder.mProfileImageIv,
				ImageLoaderUniversal.option_Round_Image);


		String mUserFriendStatus = mModel.mFriendStatus;
		if (mUserFriendStatus.equals("1")) {
			mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
			mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.friend_label));
			mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.VISIBLE);
		} else if (mUserFriendStatus.equals("2")) {
			mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
			mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.request_pending_label));
			mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
		} else if (mUserFriendStatus.equals("3")) {
			mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
			mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.accept__friend_label));
			mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
		} else if (mUserFriendStatus.equals("4")) {
			mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
			mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.add_label));
			mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_add_friend, 0, 0, 0);
			mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
		} else {
			mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
		}

		mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);

		mUserProfileActivityViewHolder.mFriendActionTV.setOnClickListener(this);
	}

	public static Intent getIntent(Context context, String mUserGUID, String userProfileLink) {
		Intent intent = new Intent(context, UserProfilePrlxFragment.class);
		intent.putExtra("UserGUID", mUserGUID);
		intent.putExtra("UserProfileLink", userProfileLink);
		return intent;
	}

	public class UserProfileActivityViewHolder {

		// public ScrollParrallaxViewX scrollView;
		public TextView mUserName;
		public TextView mUserType;
		public TextView mFriendActionTV;
		public ImageView mProfileImageIv;
		public RelativeLayout mUserInfoRL;
		public TextView mAboutTabTV, mMediaTabTV, mFriendsTabTV, mTeamTabTV, mMoreTv;

		public UserProfileActivityViewHolder(View view, OnClickListener listener) {
			// scrollView = (ScrollParrallaxViewX)
			// view.findViewById(R.id.scrollViewX1);
			mProfileImageIv = (ImageView) view.findViewById(R.id.profile_image_iv);
			mUserName = (TextView) view.findViewById(R.id.user_name_tv);
			mUserType = (TextView) view.findViewById(R.id.user_status_tv);
			mUserInfoRL = (RelativeLayout) view.findViewById(R.id.profile_image_name_rl);
			mAboutTabTV = (TextView) view.findViewById(R.id.about_tab_tv);
			mMediaTabTV = (TextView) view.findViewById(R.id.media_tab_tv);
			mFriendsTabTV = (TextView) view.findViewById(R.id.friends_tab_tv);
			mTeamTabTV = (TextView) view.findViewById(R.id.team_tab_tv);
			mMoreTv = (TextView) view.findViewById(R.id.more_tab_tv);
			mFriendActionTV = (TextView) view.findViewById(R.id.friend_action_tv);
			mAboutTabTV.setOnClickListener(listener);
			mFriendsTabTV.setOnClickListener(listener);
			mMediaTabTV.setOnClickListener(listener);
			mTeamTabTV.setOnClickListener(listener);
			mMoreTv.setOnClickListener(listener);

			mFriendActionTV.setOnClickListener(listener);
			
			if(loggedinUserModel.mUserType==Config.USER_TYPE_ATHLETE){
				mMoreTv.setVisibility(View.VISIBLE);	
			}
			else{
				mMoreTv.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.about_tab_tv:
			startActivity(AboutActivity.getIntent(mContext, new LogedInUserModel(mContext).mUserProfileURL, "UserProfileFragment"));
			break;

		case R.id.friends_tab_tv:
			startActivity(FriendsActivity.getIntent(mContext, 1, ""));
			break;

		case R.id.media_tab_tv:
			startActivity(AlbumGridActivity.getIntent(mContext, loggedinUserModel.mUserGUID, "My Albums"));
			break;

		case R.id.team_tab_tv:
			startActivity(TeamActivity.getIntent(mContext, 0));
			break;

		case R.id.more_tab_tv:
			showMoreOptionsListDialog(true);
			break;

		case R.id.friend_action_tv:
			break;

		default:
			break;
		}
	}
	

	@Override
	public void onRefresh() {
		if (HttpConnector.getConnectivityStatus(mContext) == 0) {
			DialogUtil.showOkDialogButtonLisnter(mContext, getResources().getString(R.string.No_internet_connection), getResources()
					.getString(R.string.app_name), new OnOkButtonListner() {

				@Override
				public void onOkBUtton() {

				}
			});
			mSwipeRefreshWidget.setRefreshing(false);
		} else {
			mNFFilter.PageNo = 1;
			if (mNFFilter.ActivityGUID != null) {
				if (!mNFFilter.ActivityGUID.equalsIgnoreCase("")) {
					mNFFilter.ActivityGUID = "";
					((DashboardActivity) mContext).mPostNotiLink = "";
				}
			}
			mNFFilter.PageNo = 1;
			getFeedRequest(mNFFilter);

		}

	}
	
	private void showMoreOptionsListDialog(final boolean athelete){
		final String[] options = new String[]{getString(R.string.Evaluation), getString(R.string.My_Buzz), getString(R.string.my_score_small)};
		DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {

			@Override
			public void onItemClick(int position, String item) {
				if(position==0){
					startActivity(MySportsListActivity.getIntent(mContext,loggedinUserModel.mUserGUID,getString(R.string.select_sport_label),""));
				}
				else if(position==1){
					startActivity(BuzzActivity.getIntent(mContext, loggedinUserModel.mUserGUID,""));
				}
				else if(position==2){
					startActivity(MyScoresActivity.getIntent(mContext, loggedinUserModel.mUserGUID));
				}
			}

			@Override
			public void onCancel() {
				
			}
			
		},options);
	}
	

	private void showMoreOptions(final boolean athelete) {
		final PopupWindow popup = new PopupWindow(getActivity());
		View layout = getActivity().getLayoutInflater().inflate(R.layout.user_profile_pop_menu, null);
		FontLoader.SetFontToWholeView(getActivity(), layout, FontLoader.getRobotoRegular(getActivity()));
		popup.setContentView(layout);
		popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
		View evaluationsView = layout.findViewById(R.id.evaluation_menu);
		evaluationsView.setVisibility(athelete ? View.VISIBLE : View.GONE);
		evaluationsView.findViewById(R.id.evaluation_menu).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popup.dismiss();
				startActivity(MySportsListActivity.getIntent(mContext,loggedinUserModel.mUserGUID,getString(R.string.select_sport_label),""));
			}
		});
		layout.findViewById(R.id.my_buzz_menu).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				popup.dismiss();
				startActivity(BuzzActivity.getIntent(v.getContext(), loggedinUserModel.mUserGUID,""));
			}
		});
		popup.setOutsideTouchable(true);
		popup.setFocusable(true);

		popup.showAsDropDown(mUserProfileActivityViewHolder.mMoreTv);
	}
}