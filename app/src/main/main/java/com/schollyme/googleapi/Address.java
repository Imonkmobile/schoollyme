package com.schollyme.googleapi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Address implements Serializable {
	private static final long serialVersionUID = -3563412207791391651L;

	public String description;
	public String reference;
	public String country = "";
	public String shortCode = "";
	public String locality = "";
	public String stateName = "";
	public List<String> mTerms;
	public String placeId;

	public Address(String postID, String userID, String createdDate, String postLikeID, String fullName, String profilePic) {
		super();

	}

	public Address(JSONObject jsonObject) {
		if (null != jsonObject) {
			description = jsonObject.optString("description", "");
			reference = jsonObject.optString("reference", "");
			placeId = jsonObject.optString("place_id", "");
			mTerms = new ArrayList<String>();
			try {

				mTerms = getTerms(jsonObject.getJSONArray("terms"));
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}

	public List<String> getTerms(JSONArray termsArray) {
		JSONObject jsInnerObj;
		List<String> tempList = new ArrayList<String>();
		for (int i = 0; i < termsArray.length(); i++) {
			try {
				jsInnerObj = termsArray.getJSONObject(i);

				tempList.add(jsInnerObj.optString("value"));
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		return tempList;
	}

	@Override
	public String toString() {
		return "Address [description=" + description + ", reference=" + reference + ", country=" + country + ", shortCode=" + shortCode
				+ ", locality=" + locality + ", stateName=" + stateName + ", mTerms=" + mTerms + ", placeId=" + placeId + "]";
	}
}
