package com.schollyme.googleapi;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.Config;
import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;

public class GooglePlaceRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = GooglePlaceRequest.class.getSimpleName();
	public static final String ADDRESS_API_KEY = "AIzaSyAxkOLi9kRqCIZ4jVVbz-0QSGI_Q33wsUg";

	private static final int REQ_CODE_ADDRESS_LIST = 1;
	private static final int REQ_CODE_ADDRESS_DETAIL = 2;
	private static final int REQ_CODE_ADDRESS_INFO = 3;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private boolean mRunInBg = false;

	private List<Address> mAddresses;
	private Address mAddress;
	private GooglePlace mGooglePlace;

	public GooglePlaceRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_ADDRESS_LIST, 0, mMessage);
			}
		});
	}

	public void getAddressListInServer(String text) {
		Log.v(TAG, "getAddressListInServer text=" + text);
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting address list...");
			}
			return;
		}

		String url = null;
		try {
			url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + URLEncoder.encode(text, "UTF-8")
					+ "&sensor=false&types=%28cities%29&key=" + URLEncoder.encode(ADDRESS_API_KEY, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			url = "GooglePlaceRequest INVALID_URL_FORMATTED";
			e.printStackTrace();
		}

		mRequesting = true;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync(url, REQ_CODE_ADDRESS_LIST, "get", mRunInBg, null, null, UrlType.EXTERNAL);
	}

	public void getAddressDetailInServer(Address address) {
		Log.v(TAG, "getAddressDetailInServer address=" + address);
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting address detail...");
			}
			return;
		}

		String url = null;
		try {
			url = "https://maps.googleapis.com/maps/api/place/details/json?reference=" + URLEncoder.encode(address.reference, "UTF-8")
					+ "&sensor=true&key=" + URLEncoder.encode(ADDRESS_API_KEY, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			url = "GooglePlaceRequest INVALID_URL_FORMATTED";
			e.printStackTrace();
		}

		mRequesting = true;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync(url, REQ_CODE_ADDRESS_DETAIL, "get", mRunInBg, null, null, UrlType.EXTERNAL);
	}

	public void getAddressInfoInServer(String placeId) {
		Log.v(TAG, "getAddressInfoInServer placeId=" + placeId);
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting address info...");
			}
			return;
		}

		String url = null;
		try {
			url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + URLEncoder.encode(placeId, "UTF-8") + "&key="
					+ URLEncoder.encode(ADDRESS_API_KEY, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			url = "GooglePlaceRequest INVALID_URL_FORMATTED";
			e.printStackTrace();
		}

		mRequesting = true;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync(url, REQ_CODE_ADDRESS_INFO, "get", mRunInBg, null, null, UrlType.EXTERNAL);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
		mRunInBg = (null == view);
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {
		switch (reqCode) {
		case REQ_CODE_ADDRESS_LIST:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Address list successful mAddresses=" + mAddresses);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAddresses, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get address list. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;
		case REQ_CODE_ADDRESS_DETAIL:
			mRequesting = false;
			if (parseDetail(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Address detail successful mAddress=" + mAddress);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAddress, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get address detail. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;
		case REQ_CODE_ADDRESS_INFO:
			mRequesting = false;
			if (parseInfo(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Address info successful mGooglePlace=" + mGooglePlace);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mGooglePlace, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get address info. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		try {
			JSONObject jsonObject = new JSONObject(json);
			JSONArray jsArray = jsonObject.getJSONArray("predictions");
			mAddresses = new ArrayList<Address>();

			for (int i = 0; i < jsArray.length(); i++) {
				JSONObject jsObjectInner = jsArray.getJSONObject(i);
				mAddresses.add(new Address(jsObjectInner));

			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean parseDetail(String json) {
		try {
			JSONObject jsonObject = new JSONObject(json);
			JSONObject mainObject = jsonObject.getJSONObject("result");
			JSONArray jsArray = mainObject.getJSONArray("address_components");
			if (jsArray != null && jsArray.length() > 0) {
				for (int i = 0; i < jsArray.length(); i++) {
					JSONObject jsObjectInner = jsArray.getJSONObject(i);
					JSONArray jsArrayInner = jsObjectInner.getJSONArray("types");
					if (jsArrayInner.getString(0).equals("country")) {
						mAddress.country = jsObjectInner.getString("long_name");
						mAddress.shortCode = jsObjectInner.getString("short_name");
					}
					if (jsArrayInner.getString(0).equalsIgnoreCase("administrative_area_level_1")) {
						mAddress.stateName = jsObjectInner.getString("long_name");
					}
					if (jsArrayInner.getString(0).equalsIgnoreCase("locality")) {
						mAddress.locality = jsObjectInner.getString("long_name");
					}
				}
				return true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean parseInfo(String json) {
		if (null != json) {
			try {
				JSONObject jsonObject = new JSONObject(json);
				String status = jsonObject.optString("status", "");
				if ("OK".equalsIgnoreCase(status)) {
					// Definitely address are there
					JSONArray jsonArray = jsonObject.optJSONArray("results");
					JSONObject gpJSONObject = jsonObject.optJSONObject("result");
					if (null != jsonArray && jsonArray.length() > 0) {
						mGooglePlace = new GooglePlace(jsonArray.getJSONObject(0));
					} else if (null != gpJSONObject) {
						mGooglePlace = new GooglePlace(gpJSONObject);
					}
				} else if ("ZERO_RESULTS".equalsIgnoreCase(status)) {
					// No address found
				}
				return true;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
