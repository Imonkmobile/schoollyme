package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LikeDetailModel {
	public String FirstName;
	public String LastName;
	public String ProfilePicture;
	public String CityName;
	public String CountryName;
	public String ProfileLink;

 
	
	
	
	// {
	// "FirstName": "Suresh",
	// "LastName": "Patidar",
	// "ProfilePicture": "9dbdf18db67ba781627e9fd3e5640919.jpg",
	// "CityName": "Bhopal",
	// "CountryName": "India",
	// "ProfileLink": "suresh"
	// },

	public LikeDetailModel(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			FirstName = jsonObject.optString("FirstName", "");
			LastName = jsonObject.optString("LastName", "");
			ProfilePicture = jsonObject.optString("ProfilePicture", "");
			CityName = jsonObject.optString("CityName", "");
			CountryName = jsonObject.optString("CountryName", "");
			ProfileLink = jsonObject.optString("ProfileLink", "");

		}
	}

	public static List<LikeDetailModel> getLikeDetailList(JSONArray jsonArray) {
		List<LikeDetailModel> list = new ArrayList<LikeDetailModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new LikeDetailModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	@Override
	public String toString() {
		return "Comment [FirstName=" + FirstName + ", LastName=" + LastName
				+ ", ProfilePicture=" + ProfilePicture + ", CityName="
				+ CityName + ", CountryName=" + CountryName + ", ProfileLink="
				+ ProfileLink + "]";
	}

}
