package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class SportsModel implements Parcelable {
	public String mSportsID;
	public String mSportsName;
	public boolean isSelected = false;

	public SportsModel(JSONObject mJson) {
		if (mJson != null) {
			this.mSportsID = mJson.optString("SportsID");
			this.mSportsName = mJson.optString("Name");
		}
	}
	
	public SportsModel(JSONObject mJson,boolean flag) {
		if (mJson != null) {
			this.mSportsID = mJson.optString("SportsID");
			this.mSportsName = mJson.optString("SportsName");
		}
	}

	public JSONObject toJSONObject() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("SportsID", TextUtils.isEmpty(mSportsID) ? "" : mSportsID);
			jsonObject.put("Name", TextUtils.isEmpty(mSportsName) ? "" : mSportsName);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public static List<SportsModel> getSportsModels(JSONArray jsonArray) {
		List<SportsModel> list = new ArrayList<SportsModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new SportsModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public static JSONArray toJSONArray(List<SportsModel> list) {
		JSONArray jsonArray = new JSONArray();
		if (null != list) {
			for (SportsModel sportsModel : list) {
				jsonArray.put(sportsModel.mSportsID);
			}
		}
		return jsonArray;
	}

	public SportsModel(Parcel in) {
		mSportsID = in.readString();
		mSportsName = in.readString();
		isSelected = (in.readInt() == 1);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mSportsID);
		dest.writeString(mSportsName);
		dest.writeInt(isSelected ? 1 : 0);
	}

	public int describeContents() {
		return 0;
	}

	public static final Creator CREATOR = new Creator() {
		public SportsModel createFromParcel(Parcel in) {
			return new SportsModel(in);
		}

		public SportsModel[] newArray(int size) {
			return new SportsModel[size];
		}
	};

	@Override
	public String toString() {
		return "SportsModel [mSportsID=" + mSportsID + ", mSportsName=" + mSportsName + ", isSelected=" + isSelected + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (null != o && o instanceof SportsModel) {
			return this.mSportsID.equalsIgnoreCase(((SportsModel) o).mSportsID);
		}
		return super.equals(o);
	}

}