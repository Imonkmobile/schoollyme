package com.schollyme.model;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;

public class SignUpModel {
	private static final String PREF_NAME = "SignUpPref";
	public String mAccessToken;
	public String mNewUserId;
	public String isFacebookUser;
	public String isGPlusUser;
	public String mZipcode;
	public String mUserGuid;
	public String mProfileImageURL;
	public String mUserPassword;

	public SignUpModel(String loginSessionKey, String mNewUserId, String isFacebookUser,String isGPlusUser,String mZipcode,String mUserGuid,String profileImageURL) {
		super();
		this.mAccessToken = loginSessionKey;
		this.mNewUserId = mNewUserId;
		this.isFacebookUser = isFacebookUser;
		this.isGPlusUser = isGPlusUser;
		this.mZipcode = mZipcode;
		this.mUserGuid = mUserGuid;
		this.mProfileImageURL = profileImageURL;
	}

	public SignUpModel(JSONObject json) {
		if (json != null) {
			this.mAccessToken = json.optString("AccessToken");;
			this.mNewUserId = json.optString("NewUserId");;
			this.isFacebookUser = json.optString("isFacebookUser");;
			this.isGPlusUser = json.optString("isGPlusUser");;
			this.mZipcode = json.optString("Zipcode");;
			this.mUserGuid = json.optString("UserGuid");;
		}
	}

	public void persist(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.putString("mAccessToken", mAccessToken);
		prefsEditor.putString("mNewUserId", mNewUserId);
		prefsEditor.putString("isFacebookUser", isFacebookUser);
		prefsEditor.putString("isGPlusUser", isGPlusUser);
		prefsEditor.putString("mZipcode", mZipcode);
		prefsEditor.putString("mUserGuid", mUserGuid);
		prefsEditor.putString("mProfileImageURL", mProfileImageURL);
		prefsEditor.commit();
	}
	
	public void removePreference(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.clear();
		prefsEditor.commit();
	}

	public SignUpModel(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		this.mAccessToken = sharedPreferences.getString("mAccessToken", "");
		this.mNewUserId = sharedPreferences.getString("mNewUserId", "");
		this.isFacebookUser = sharedPreferences.getString("isFacebookUser", "0");
		this.isGPlusUser = sharedPreferences.getString("isGPlusUser", "0");
		this.mZipcode = sharedPreferences.getString("mZipcode", "");
		this.mUserGuid = sharedPreferences.getString("mUserGuid", "");
		this.mProfileImageURL = sharedPreferences.getString("mProfileImageURL", "");
		this.mUserPassword = sharedPreferences.getString("mUserPassword", "");
	}
	
	public SignUpModel(String key,String value,Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.putString(key, value);
		prefsEditor.commit();
	}
}