package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Evaluation implements Parcelable {
	public String EvaluationGUID;
	public String Title;
	public String Location;
	public String Description;
	public int RateValue;
	public int CanDelete;
	public String CreatedDate;
	public CreatedBy CreatedBy;
	public SportsModel Sport;

	public Evaluation(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			this.EvaluationGUID = jsonObject.optString("EvaluationGUID", "");
			this.Title = jsonObject.optString("Title", "");
			this.Location = jsonObject.optString("Location", "");
			this.Description = jsonObject.optString("Description", "");
			this.RateValue = jsonObject.optInt("RateValue", 0);
			this.CanDelete = jsonObject.optInt("CanDelete", 0);
			this.CreatedDate = jsonObject.optString("CreatedDate", "");
			this.CreatedBy = new CreatedBy(jsonObject.optJSONObject("CreatedBy"));
			this.Sport = new SportsModel(jsonObject.optJSONObject("Sport"),true);
		}
	}

	public static List<Evaluation> getEvaluations(JSONArray jsonArray) {
		List<Evaluation> list = new ArrayList<Evaluation>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new Evaluation(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public Evaluation(Parcel in) {
		EvaluationGUID = in.readString();
		Title = in.readString();
		Location = in.readString();
		Description = in.readString();
		RateValue = in.readInt();
		CanDelete = in.readInt();
		CreatedDate = in.readString();
		try {
			JSONObject jsonObjectCb = new JSONObject(in.readString());
			CreatedBy = new CreatedBy(jsonObjectCb);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			JSONObject jsonObjectSport = new JSONObject(in.readString());
			Sport = new SportsModel(jsonObjectSport);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(EvaluationGUID);
		dest.writeString(Title);
		dest.writeString(Location);
		dest.writeString(Description);
		dest.writeInt(RateValue);
		dest.writeInt(CanDelete);
		dest.writeString(CreatedDate);
		dest.writeString(null == CreatedBy ? "{}" : CreatedBy.toJSONObject().toString());
		dest.writeString(null == Sport ? "{}" : Sport.toJSONObject().toString());
	}

	public int describeContents() {
		return 0;
	}

	public static final Creator CREATOR = new Creator() {
		public Evaluation createFromParcel(Parcel in) {
			return new Evaluation(in);
		}

		public Evaluation[] newArray(int size) {
			return new Evaluation[size];
		}
	};
}
