package com.schollyme.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class StateModel implements Parcelable {

	public String mStateId;
	public String mStateName;
	public String mStateCode;
	public boolean isSelected = false;

	public StateModel(JSONObject jsonObj) {
		if (jsonObj != null) {
			this.mStateId = jsonObj.optString("StateID");
			this.mStateName = jsonObj.optString("Name");
			this.mStateCode = jsonObj.optString("ShortCode");
		}
	}

	public static JSONArray toJSONArray(List<StateModel> list) {
		JSONArray jsonArray = new JSONArray();
		if (null != list) {
			for (StateModel stateModel : list) {
				jsonArray.put(stateModel.mStateId);
			}
		}
		return jsonArray;
	}
	
	public StateModel(String stateId,String stateName){
		mStateId = stateId;
		mStateName = stateName;
	}

	public StateModel(Parcel in) {
		mStateId = in.readString();
		mStateName = in.readString();
		mStateCode = in.readString();
		isSelected = (in.readInt() == 1);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mStateId);
		dest.writeString(mStateName);
		dest.writeString(mStateCode);
		dest.writeInt(isSelected ? 1 : 0);
	}

	public int describeContents() {
		return 0;
	}

	public static final Creator CREATOR = new Creator() {
		public StateModel createFromParcel(Parcel in) {
			return new StateModel(in);
		}

		public StateModel[] newArray(int size) {
			return new StateModel[size];
		}
	};

	@Override
	public boolean equals(Object o) {
		if (null != o && o instanceof StateModel) {
			return this.mStateId.trim().equalsIgnoreCase(((StateModel) o).mStateId.trim());
		}
		return super.equals(o);
	}

	@Override
	public String toString() {
		return "StateModel [mStateId=" + mStateId + ", mStateName=" + mStateName + ", mStateCode=" + mStateCode + ", isSelected="
				+ isSelected + "]";
	}
}
