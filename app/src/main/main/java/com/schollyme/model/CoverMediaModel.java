package com.schollyme.model;

import org.json.JSONObject;

public class CoverMediaModel {
	public String MediaGUID;// ":1,
	public String ImageName;
	public String Caption;
	public String MediaType;
	public int VideoLength;
	public String ConversionStatus;
	public String CreatedDate;

	//
	// "CoverMedia": {
	// "MediaGUID": "16bb92ca-899a-bed5-1d91-2db947a06e1d",
	// "ImageName": "b472a9a4f7c9ba4131814a39ba6ce72a.png",
	// "Caption": "",
	// "MediaType": "Image",
	// "VideoLength": 0,
	// "ConversionStatus": "Finished",
	// "CreatedDate": "2015-06-04 06:05:46"
	//
	public CoverMediaModel(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			MediaGUID = jsonObject.optString("MediaGUID").trim();
			ImageName = jsonObject.optString("ImageName").trim();
			Caption = jsonObject.optString("Caption").trim();
			MediaType = jsonObject.optString("MediaType").trim();
			VideoLength = jsonObject.optInt("VideoLength", 0);
			ConversionStatus = jsonObject.optString("ConversionStatus").trim();
			CreatedDate = jsonObject.optString("CreatedDate").trim();

		}
	}

}
