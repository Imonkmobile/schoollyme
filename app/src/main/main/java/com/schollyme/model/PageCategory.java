package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PageCategory {
	
	public String mCategoryID;
	public String mModuleID;
	public String mParentID;
	public String mName;
	public String mIcon;
	
	
	public PageCategory(JSONObject mJson){
		try {
			this.mCategoryID = mJson.optString("CategoryID");
			this.mModuleID = mJson.optString("ModuleID");
			this.mParentID = mJson.optString("ParentID");
			this.mName = mJson.optString("Name");
			this.mIcon = mJson.optString("Icon");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static List<PageCategory> getPageCategorys(JSONArray jsonArray) {
		List<PageCategory> list = new ArrayList<PageCategory>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new PageCategory(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}