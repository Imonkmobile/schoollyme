package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class ConferenceModel implements Parcelable{
	
	public String mConferenceId;
	public String mConferenceName;
	public boolean isSelected = false;
	
	public ConferenceModel(JSONObject mJsonObj){
		try {
			this.mConferenceId = mJsonObj.optString("ConferenceID");
			this.mConferenceName = mJsonObj.optString("Name");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static List<ConferenceModel> getConferences(JSONArray jsonArray) {
		List<ConferenceModel> list = new ArrayList<ConferenceModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new ConferenceModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	
	public ConferenceModel(Parcel in) {
		mConferenceId = in.readString();
		mConferenceName = in.readString();
		isSelected = (in.readInt() == 1);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mConferenceId);
		dest.writeString(mConferenceName);
		dest.writeInt(isSelected ? 1 : 0);
	}

	public int describeContents() {
		return 0;
	}

	public static final Creator CREATOR = new Creator() {
		public ConferenceModel createFromParcel(Parcel in) {
			return new ConferenceModel(in);
		}

		public ConferenceModel[] newArray(int size) {
			return new ConferenceModel[size];
		}
	};

	@Override
	public String toString() {
		return "ConferenceModel [mConferenceId=" + mConferenceId
				+ ", mConferenceName=" + mConferenceName + ", isSelected="
				+ isSelected + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (null != o && o instanceof ConferenceModel) {
			return this.mConferenceId.equalsIgnoreCase(((ConferenceModel) o).mConferenceId);
		}
		return super.equals(o);
	}
}