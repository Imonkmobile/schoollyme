package com.schollyme.model;

import org.json.JSONObject;

import android.content.Context;

public class RenovateUser {
	
	public String mFirstName;
	public String mLastName;
	public String mEmail;
	public String mPasssword;
	public String mZipcode;
	public String mRenovateUrl;
	public String mFacebookID;
	public String mGPlusID;
	public String mUserPicURL;
	public String mUserCoverPicURL;
	public String mUserDescription;
	public String mUserType;
	
	public RenovateUser(JSONObject userJson) {
	  try{
		  if(null!=userJson){
			  this.mFirstName = userJson.optString("FirstName", "");
			  this.mLastName = userJson.optString("LastName", "");
			  this.mEmail = userJson.optString("Email", "");
			  this.mPasssword = userJson.optString("Password", "");
			  this.mZipcode = userJson.optString("ZipCode", "");
			  this.mRenovateUrl = userJson.optString("RenovateUrl", "");
			  this.mFacebookID = userJson.optString("FacebookId", "");
			  this.mGPlusID = userJson.optString("GPlusId", "");
			  this.mUserPicURL = userJson.optString("UserPic", "");
			  this.mUserCoverPicURL = userJson.optString("CoverPic", "");
			  this.mUserDescription = userJson.optString("Description", "");
			  this.mUserType = userJson.optString("UserType", "");
		  }
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	}
	
	public RenovateUser(Context context) {
		
	}

}
