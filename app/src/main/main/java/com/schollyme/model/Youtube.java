package com.schollyme.model;

import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Youtube implements Parcelable {
	public String type;
	public String provider_url;
	public String thumbnail_url;
	public String author_name;
	public String title;
	public int thumbnail_width;
	public int width;
	public String version;
	public String html;
	public int thumbnail_height;
	public int height;
	public String author_url;
	public String provider_name;
	public int video_length;

	public Youtube(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			this.type = jsonObject.optString("type", "");
			this.provider_url = jsonObject.optString("provider_url", "");
			this.thumbnail_url = jsonObject.optString("thumbnail_url", "");
			this.author_name = jsonObject.optString("author_name", "");
			this.title = jsonObject.optString("title", "");
			this.thumbnail_width = jsonObject.optInt("thumbnail_width", 0);
			this.width = jsonObject.optInt("width", 0);
			this.version = jsonObject.optString("version", "");
			this.html = jsonObject.optString("html", "");
			this.thumbnail_height = jsonObject.optInt("thumbnail_height", 0);
			this.height = jsonObject.optInt("height", 0);
			this.author_url = jsonObject.optString("author_url", "");
			this.provider_name = jsonObject.optString("provider_name", "");
		}
	}

	public Youtube(JSONObject itemsObject, JSONObject snippetObject, JSONObject detailsObject) {
		super();
		if (null != itemsObject && null != snippetObject && null != detailsObject) {
			this.type = itemsObject.optString("kind", "");
			this.provider_url = itemsObject.optString("provider_url", "");
			this.thumbnail_url = itemsObject.optString("thumbnail_url", "");
			this.author_name = itemsObject.optString("author_name", "");
			this.title = snippetObject.optString("title", "");
			this.thumbnail_width = itemsObject.optInt("thumbnail_width", 0);
			this.width = itemsObject.optInt("width", 0);
			this.version = itemsObject.optString("version", "");
			this.html = itemsObject.optString("html", "");
			this.thumbnail_height = itemsObject.optInt("thumbnail_height", 0);
			this.height = itemsObject.optInt("height", 0);
			this.author_url = itemsObject.optString("author_url", "");
			this.provider_name = itemsObject.optString("provider_name", "");
			this.video_length = parseDuration(detailsObject.optString("duration", ""));
		}
	}

	private int parseDuration(String duration) {
		String hrs = "0", mns = "0", secs = "0";
		hrs = getDigitBeforeChar(duration, 'H');
		mns = getDigitBeforeChar(duration, 'M');
		secs = getDigitBeforeChar(duration, 'S');
		int hrsMillis = parseInt(hrs) * 60 * 60 * 1000;
		int minsMillis = parseInt(mns) * 60 * 1000;
		int secsMillis = parseInt(secs) * 1000;
		return hrsMillis + minsMillis + secsMillis;
	}

	private String getDigitBeforeChar(String s, char c) {
		if (null != s) {
			int index = s.indexOf(c);
			if (index >= 0) {
				int prevCharIndex = 0;
				for (int i = index - 1; i >= 0; i--) {
					if (!Character.isDigit(s.charAt(i))) {
						prevCharIndex = i + 1;
						break;
					}
				}
				return s.substring(prevCharIndex, index);
			}
		}
		return "0";
	}

	private int parseInt(String i) {
		try {
			return Integer.parseInt(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public Youtube(Parcel in) {
		type = in.readString();
		provider_url = in.readString();
		thumbnail_url = in.readString();
		author_name = in.readString();
		title = in.readString();
		thumbnail_width = in.readInt();
		width = in.readInt();
		version = in.readString();
		html = in.readString();
		thumbnail_height = in.readInt();
		height = in.readInt();
		author_url = in.readString();
		provider_name = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(type);
		dest.writeString(provider_url);
		dest.writeString(thumbnail_url);
		dest.writeString(author_name);
		dest.writeString(title);
		dest.writeInt(thumbnail_width);
		dest.writeInt(width);
		dest.writeString(version);
		dest.writeString(html);
		dest.writeInt(thumbnail_height);
		dest.writeInt(height);
		dest.writeString(author_url);
		dest.writeString(provider_name);
	}

	public static final Creator CREATOR = new Creator() {
		public Youtube createFromParcel(Parcel in) {
			return new Youtube(in);
		}

		public Youtube[] newArray(int size) {
			return new Youtube[size];
		}
	};

	@Override
	public String toString() {
		return "Youtube [type=" + type + ", provider_url=" + provider_url + ", thumbnail_url=" + thumbnail_url + ", author_name="
				+ author_name + ", title=" + title + ", thumbnail_width=" + thumbnail_width + ", width=" + width + ", version=" + version
				+ ", html=" + html + ", thumbnail_height=" + thumbnail_height + ", height=" + height + ", author_url=" + author_url
				+ ", provider_name=" + provider_name + "]";
	}

}
