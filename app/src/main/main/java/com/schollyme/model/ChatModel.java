package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import com.vinfotech.utility.Utility;

public class ChatModel {

	public String mMessageGUID;
	public String mUserGUID;
	public String mMessage;
	public String mCreatedDate;
	public String mModifiedDate;
	public String mChatDay;
	public String mUserProfileURL;
	public String mUserTypeId;
	public UserModel mSenderUser;
	public UserModel mLoggedinUser;

	public ChatModel(JSONObject mJson,Context mContext){
		try {
			this.mMessageGUID = mJson.optString("MessageGUID");
			this.mUserGUID = mJson.optString("UserGUID");
			this.mMessage = mJson.optString("Body");
			this.mCreatedDate = mJson.optString("CreatedDate");
			this.mModifiedDate = mJson.optString("ModifiedDate");
			this.mUserProfileURL = mJson.optString("ProfileURL");
			this.mUserTypeId = mJson.optString("UserTypeID");
			if(!TextUtils.isEmpty(this.mModifiedDate)){
				this.mChatDay = Utility.getFormattedDate(this.mCreatedDate,mContext);
			}
			else{
				this.mChatDay = "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private ChatModel(UserModel mModel,int type){
		if(type ==1 ){
			mLoggedinUser = mModel;
		}
		else{
			mSenderUser = mModel;
		}
	}


	public static List<ChatModel> getMessageDetail(JSONObject jsonObj,String loggedInUserGUID,Context mContext) {
		List<ChatModel> list = new ArrayList<ChatModel>();
		if (jsonObj != null) {
			JSONArray jsonArray = jsonObj.optJSONArray("Messages");
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new ChatModel(jsonArray.getJSONObject(i),mContext));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			JSONArray jsonUsersArray = jsonObj.optJSONArray("Users");
			try {
				for(int i=0;i<jsonUsersArray.length();i++){
					JSONObject mUserJson = jsonUsersArray.getJSONObject(i);
					if(mUserJson.optString("UserGUID").equals(loggedInUserGUID)){
						new ChatModel(new UserModel(mUserJson),1);
					}
					else{
						new ChatModel(new UserModel(mUserJson),2);
						
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return list;
	}
}