package com.schollyme.model;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.schollyme.Config;

public class LogedInUserModel {

	private static final String PREF_NAME = "UserPref";

	public String mLoginSessionKey;
	public boolean mLoginStatus;
	public String mUserGUID;
	public String mUserProfileURL;
	public String mUserName;
	public String mFirstName;
	public String mLastName;
	public String mEmail;
	public String mUserPicURL;
	public String mUserDOB;
	public String mLatitude;
	public String mLongitude;
	public static String IsVisitAlbumDetail = "IsVisitAlbumDetail";
	public int mUserType;
	public int mUserStatusId;
	public String mUserGender;
	public boolean isProfileSetUp = false;
	public boolean isPasswordChanged = false;
	public String isPaidCoach;
	public String isEvaluator;
	public boolean isFirstInstall = true;
	public String mUserSportsId;
	public String mUserSportsName;

	public LogedInUserModel() {

	}

	public LogedInUserModel(JSONObject json) {
		if (json != null) {
			this.mUserGUID = json.optString("UserGUID");
			this.mUserProfileURL = json.optString("ProfileUrl");
			this.mLoginSessionKey = json.optString("LoginSessionKey");
			this.mUserName = json.optString("Username");
			this.mFirstName = json.optString("FirstName");
			this.mLastName = json.optString("LastName");
			this.mEmail = json.optString("Email");
			this.mLatitude = json.optString("Latitude");
			this.mLongitude = json.optString("Longitude");
			this.isPaidCoach = json.optString("IsPaidCoach");
			this.isEvaluator = json.optString("IsEvaluator");
			this.mUserPicURL = json.optString("ProfilePicture");
			this.mUserGender = json.optString("Gender");
			String typeId = json.optString("UserTypeID");
			this.mUserDOB = json.optString("DOB");
			this.mUserType = Integer.parseInt(typeId);
			String status = json.optString("StatusID");
			if (TextUtils.isEmpty(status)) {
				this.mUserStatusId = 0;
			} else {
				this.mUserStatusId = Integer.parseInt(status);
			}
			String passSet = json.optString("IsPasswordChange");
			if (passSet.equals("0")) {
				this.isPasswordChanged = true;
			} else {
				this.isPasswordChanged = false;
			}
			String isProfileSet = json.optString("ProfileSetup");
			if (isProfileSet.equals("1")) {
				this.isProfileSetUp = true;
			} else {
				this.isProfileSetUp = false;
			}
			
			JSONArray sportsArray = json.optJSONArray("Sports");
			if(sportsArray!=null){
				
				for(int i=0;i<1;i++){
					SportsModel model = new SportsModel(sportsArray.optJSONObject(i));
					mUserSportsId = model.mSportsID;
					mUserSportsName = model.mSportsName;
				}
			}

		}
	}

	public LogedInUserModel(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		this.mUserGUID = sharedPreferences.getString("mUserGUID", "");
		this.mLoginSessionKey = sharedPreferences.getString("mLoginSessionKey", "");
		this.mUserProfileURL = sharedPreferences.getString("mUserProfileURL", mUserProfileURL);
		this.mUserName = sharedPreferences.getString("Username", "");
		this.mFirstName = sharedPreferences.getString("FirstName", "");
		this.mLastName = sharedPreferences.getString("LastName", "");
		this.mEmail = sharedPreferences.getString("mEmail", "");
		this.mUserPicURL = sharedPreferences.getString("mUserProfilePicture", "");
		this.mUserType = sharedPreferences.getInt("mUserType", 3);
		this.mUserStatusId = sharedPreferences.getInt("mUserStatusId", 1);
		this.mUserGender = sharedPreferences.getString("Gender", "");
		this.isPasswordChanged = sharedPreferences.getBoolean("isPasswordChanged", true);
		this.isProfileSetUp = sharedPreferences.getBoolean("isProfileSetUp", false);
		this.mLoginStatus = sharedPreferences.getBoolean("mLoginStatus", false);
		this.mUserDOB = sharedPreferences.getString("mUserDOB", "");
		this.isPaidCoach = sharedPreferences.getString("isPaidCoach", "0");
		this.isEvaluator = sharedPreferences.getString("isEvaluator", "0");
		this.mUserSportsName = sharedPreferences.getString("mUserSportsName", mUserSportsName);
		this.mUserSportsId = sharedPreferences.getString("mUserSportsId", mUserSportsId);
	}

	public void persist(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.putString("mUserGUID", mUserGUID);
		prefsEditor.putString("mLoginSessionKey", mLoginSessionKey);
		prefsEditor.putString("Username", mUserName);
		prefsEditor.putString("FirstName", mFirstName);
		prefsEditor.putString("LastName", mLastName);
		prefsEditor.putString("mEmail", mEmail);
		prefsEditor.putString("mUserDOB", mUserDOB);
		prefsEditor.putString("Gender", mUserGender);
		prefsEditor.putString("mUserProfilePicture", mUserPicURL);
		prefsEditor.putString("mUserProfileURL", mUserProfileURL);
		prefsEditor.putInt("mUserType", mUserType);
		prefsEditor.putInt("mUserStatusId", mUserStatusId);
		prefsEditor.putBoolean("isPasswordChanged", isPasswordChanged);
		prefsEditor.putBoolean("isProfileSetUp", isProfileSetUp);

		// prefsEditor.putBoolean("mLoginStatus", false);
		// prefsEditor.putString("mUserDOB", mUserDOB);
		prefsEditor.putString("isPaidCoach", isPaidCoach);
		prefsEditor.putString("isEvaluator", isEvaluator);
		
		prefsEditor.putString("mUserSportsName", mUserSportsName);
		prefsEditor.putString("mUserSportsId", mUserSportsId);

		prefsEditor.commit();
	}

	public void setIsVisitScreen(Context context, String onScreen, boolean value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

		prefsEditor.putBoolean(onScreen, value);
		prefsEditor.commit();
	}

	public boolean getIsVisitScreen(Context context, String onScreen) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

		return sharedPreferences.getBoolean(onScreen, false);
	}

	public void updatePreference(Context context, String key, boolean value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

		prefsEditor.putBoolean(key, value);
		prefsEditor.commit();
	}

	public static void updatePreferenceString(Context context, String key, String value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

		prefsEditor.putString(key, value);
		prefsEditor.commit();
	}

	public void removeUserData(Context mContext) {
		SharedPreferences sharedPreferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.clear();
		prefsEditor.commit();
	}

	// New Shared Preferance
	public static boolean isEmptyPreferance(Context context) {

		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		String strSavedMem1 = sharedPreferences.getString("mUserProfileURL", "");
		if (strSavedMem1.equals(""))
			return true;
		else
			return false;
	}

	public boolean isAthlete() {
		return Config.USER_TYPE_ATHLETE == mUserType;
	}

	@Override
	public String toString() {
		return "LogedInUserModel [mLoginSessionKey=" + mLoginSessionKey + ", mLoginStatus=" + mLoginStatus + ", mUserGUID=" + mUserGUID
				+ ", mUserProfileURL=" + mUserProfileURL + ", mUserName=" + mUserName + ", mFirstName=" + mFirstName + ", mLastName="
				+ mLastName + ", mEmail=" + mEmail + ", mUserPicURL=" + mUserPicURL + ", mUserDOB=" + mUserDOB + ", mLatitude=" + mLatitude
				+ ", mLongitude=" + mLongitude + ", mUserType=" + mUserType + ", mUserStatusId=" + mUserStatusId + ", mUserGender="
				+ mUserGender + ", isProfileSetUp=" + isProfileSetUp + ", isPasswordChanged=" + isPasswordChanged + 
				", isPaidCoach="+ isPaidCoach +", mUserSportsName="+ mUserSportsName +", mUserSportsId="+ mUserSportsId +
				", isEvaluator=" + isEvaluator + ", isFirstInstall=" + isFirstInstall + "]";
	}
}