package com.schollyme.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class UserModel implements Serializable {

	public String mUserGUID;
	public String mUserFullName;
	public String mUserProfilePic;
	public String mUserProfileURL;
	public String mUserTypeID;
	public String mUserName;
	public String mUserEmail;
	public String mUserGender;
	public String mUserDOB;
	public String mUserStateName;
	public String mUserStateId;
	public String mUserSports;
	public String mUserHomeAddress;
	public String mUserPhoneNumber;
	public String mUserParentID;
	public String mUserParentName;
	public String mUserParentPhoneNumber;
	public String mUserParentEmail;
	public String mUserAlternateEmail;
	public String mUserGPAScore;
	public String mUserACTScore;
	public String mSATScore;
	public String mUserTranscripts;
	public String mUserNCAAEligibility;
	public String mUserHeight;

	public String mUserFavMovie;
	public String mUserFavMusic;
	public String mUserFavFood;
	public String mUserFavTeacher;
	public String mUserFavQuote;
	public String mUserPosition;
	public String mUserHomeTeam;
	public String mUserJerseyNumber;
	public String mUserCurrentSchool;
	public String mUserClubTeam;

	public String mUserFavAthelet;
	public String mUserFavTeam;
	public String mUserIsEvaluator;
	public String mUserIsPaidCoach;

	public String mUserCoachingTitle;
	public String mUserCoachingPhilosophy;
	public String mUserMentor;
	public String mUserAlmaMater;
	public String mFriendStatus;
	
	public String mGPAVerificationStatus;
	public String mACTVerificationStatus;
	public String mSATVerificationStatus;
	public String mTranscriptVerificationStatus;
	
	public String mGPAVerifiedDate;
	public String mACTVerifiedDate;
	public String mSATVerifiedDate;
	public String mTranscriptsVerifiedDate;
	
	public String UserGUID;
	public int IsCreateOfficialPage;
	

	public transient ArrayList<SportsModel> mUserSportsAL;

	public UserModel() {

	}

	public UserModel(JSONObject mJson) {
		if (mJson != null) {
			
			this.mUserGUID = mJson.optString("UserGUID");
			String fName = mJson.optString("FirstName");
			String lName = mJson.optString("LastName");
			this.mUserFullName = fName + " " + lName;
			this.mUserProfilePic = mJson.optString("ProfilePicture");
			this.mUserName = mJson.optString("Username");
			this.mUserEmail = mJson.optString("Email");
			this.mUserGender = mJson.optString("Gender");
			this.mUserDOB = mJson.optString("DOB");
			this.mUserStateId = mJson.optString("StateID");
			this.mUserStateName = mJson.optString("StateName");

			JSONArray jArray = mJson.optJSONArray("Sports");
			this.mUserSportsAL = getUserSports(jArray);
			this.mUserHomeAddress = mJson.optString("HomeAddress");
			this.mUserPhoneNumber = mJson.optString("PhoneNumber");
			JSONArray jSArray = mJson.optJSONArray("Parents");
			if (jSArray != null) {
				JSONObject parentJson = jSArray.optJSONObject(0);
				if (parentJson != null) {
					this.mUserParentName = parentJson.optString("ParentsName");
					this.mUserParentID = parentJson.optString("ParentID");
					this.mUserParentPhoneNumber = parentJson.optString("ParentsPhoneNumber");
					this.mUserParentEmail = parentJson.optString("ParentsEmail");
				
				}
			}
			this.mUserHeight = mJson.optString("Height");
			this.mUserAlternateEmail = mJson.optString("AlternateEmail");
			
			this.mUserNCAAEligibility = mJson.optString("NCAAEligibility");
			this.mUserFavMovie = mJson.optString("FavoriteMovie");
			this.mUserFavMusic = mJson.optString("FavoriteMusic");
			this.mUserFavFood = mJson.optString("FavoriteFood");
			this.mUserFavTeacher = mJson.optString("FavoriteTeacher");

			this.mUserFavQuote = mJson.optString("FavoriteQuote");

			this.mUserPosition = mJson.optString("Position");
			this.mUserHomeTeam = mJson.optString("HT");
			this.mUserJerseyNumber = mJson.optString("JerseyNumber");
			this.mUserCurrentSchool = mJson.optString("School");
			this.mUserClubTeam = mJson.optString("ClubTeam");
			this.mUserFavAthelet = mJson.optString("FavoriteAthlete");
			this.mUserFavTeam = mJson.optString("FavoriteTeam");
			this.mUserIsEvaluator = mJson.optString("IsEvaluator");
			this.mUserIsPaidCoach = mJson.optString("IsPaidCoach");

			this.mUserCoachingTitle = mJson.optString("CoachingTitle");
			this.mUserCoachingPhilosophy = mJson.optString("CoachingPhilosophy");
			this.mUserMentor = mJson.optString("Mentor");
			this.mUserAlmaMater = mJson.optString("AlmaMater");
			this.mFriendStatus = mJson.optString("FriendStatus");

			this.mUserProfileURL = mJson.optString("ProfileURL");
			this.mUserTypeID = mJson.optString("UserTypeID");
			this.UserGUID = mJson.optString("UserGUID");
			this.IsCreateOfficialPage = mJson.optInt("IsCreateOfficialPage");
			
			JSONObject mGPAJson = mJson.optJSONObject("GPA");
			if(null!=mGPAJson){
				this.mUserGPAScore = mGPAJson.optString("Score");
				this.mGPAVerificationStatus = mGPAJson.optString("VerificationStatus");
				this.mGPAVerifiedDate = mGPAJson.optString("VerificationDate");
			}
			else{
				this.mUserGPAScore = mJson.optString("GPAScore");
			}
			
			JSONObject mACTJson = mJson.optJSONObject("ACT");
			if(null!=mACTJson){
				this.mUserACTScore = mACTJson.optString("Score");
				this.mACTVerificationStatus = mACTJson.optString("VerificationStatus");
				this.mACTVerifiedDate = mACTJson.optString("VerificationDate");
			}
			else{
				this.mUserACTScore = mJson.optString("ACTScore");
			}
			
			JSONObject mSATJson = mJson.optJSONObject("SAT");
			if(null!=mSATJson){
				this.mSATScore = mSATJson.optString("Score");
				this.mSATVerificationStatus = mSATJson.optString("VerificationStatus");
				this.mSATVerifiedDate = mSATJson.optString("VerificationDate");
			}
			else{
				this.mSATScore = mJson.optString("SATScore");
			}
			
			JSONObject mTranscriptsJson = mJson.optJSONObject("TRANSCRIPT_UPLOAD");
			if(null!=mTranscriptsJson){
				this.mUserTranscripts = mTranscriptsJson.optString("Score");
				this.mTranscriptVerificationStatus = mTranscriptsJson.optString("VerificationStatus");
				this.mTranscriptsVerifiedDate = mTranscriptsJson.optString("VerificationDate");
			}
		}
	}

	private ArrayList<SportsModel> getUserSports(JSONArray jArray) {
		ArrayList<SportsModel> nUserSportsAL = new ArrayList<SportsModel>();
		if (jArray != null) {
			for (int i = 0; i < jArray.length(); i++) {
				nUserSportsAL.add(new SportsModel(jArray.optJSONObject(i)));
			}
		}
		return nUserSportsAL;
	}

	public static List<UserModel> getUserModels(JSONArray jsonArray) {
		List<UserModel> list = new ArrayList<UserModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new UserModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public void persist(Context mContext) {

	}

}
