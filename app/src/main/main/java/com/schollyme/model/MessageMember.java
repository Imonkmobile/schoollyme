package com.schollyme.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

public class MessageMember {

	public String FirstName;
	public String LastName;
	public String ProfilePicture;
	public String UserGUID;
	public String UserTypeID;
	public String ProfileURL;

	public boolean selected = false;

	public MessageMember(JSONObject jsonObject) {
		if (null != jsonObject) {
			this.FirstName = jsonObject.optString("FirstName");
			this.LastName = jsonObject.optString("LastName");
			this.ProfilePicture = jsonObject.optString("ProfilePicture");
			this.UserGUID = jsonObject.optString("UserGUID");
			this.UserTypeID = jsonObject.optString("UserTypeID");
			this.ProfileURL = jsonObject.optString("ProfileURL");
			if (TextUtils.isEmpty(this.ProfileURL)) {
				this.ProfileURL = jsonObject.optString("ProfileLink");
			}
		}
	}

	public static List<MessageMember> getMessageMembers(JSONArray jsonArray) {
		List<MessageMember> list = new ArrayList<MessageMember>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new MessageMember(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof MessageMember) {
			return ((MessageMember) o).UserGUID.equals(UserGUID);
		}
		return super.equals(o);
	}

	@Override
	public String toString() {
		return "MessageMember [FirstName=" + FirstName + ", LastName=" + LastName + ", ProfilePicture=" + ProfilePicture + ", UserGUID="
				+ UserGUID + ", UserTypeID=" + UserTypeID + ", ProfileURL=" + ProfileURL + ", selected=" + selected + "]";
	}

}