package com.schollyme.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class NewYoutubeUrl implements Parcelable {
	public String Url;
	public String Caption;
	public String Description;
	public String Keyword;
	public String SportsID;
	public int VideoLength;

	public NewYoutubeUrl(String url, String caption, String description, String keyword, String sportsID, int videoLength) {
		super();
		Url = url;
		Caption = caption;
		Description = description;
		Keyword = keyword;
		SportsID = sportsID;
		VideoLength = videoLength;
	}

	public JSONObject toJSONObject() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("Url", TextUtils.isEmpty(Url) ? "" : Url);
			jsonObject.put("Caption", TextUtils.isEmpty(Caption) ? "" : Caption);
			jsonObject.put("Description", TextUtils.isEmpty(Description) ? "" : Description);
			jsonObject.put("Keyword", TextUtils.isEmpty(Keyword) ? "" : Keyword);
			jsonObject.put("SportsID", TextUtils.isEmpty(SportsID) ? "" : SportsID);
			jsonObject.put("VideoLength", VideoLength);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public static JSONArray getJSONArray(List<NewYoutubeUrl> list) {
		JSONArray jsonArray = new JSONArray();
		if (null != list) {
			for (NewYoutubeUrl newYoutubeUrl : list) {
				jsonArray.put(newYoutubeUrl.toJSONObject());
			}
		}
		return jsonArray;
	}

	public NewYoutubeUrl(Parcel in) {
		Url = in.readString();
		Caption = in.readString();
		Description = in.readString();
		Keyword = in.readString();
		SportsID = in.readString();
		VideoLength = in.readInt();
	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(Url);
		dest.writeString(Caption);
		dest.writeString(Description);
		dest.writeString(Keyword);
		dest.writeString(SportsID);
		dest.writeInt(VideoLength);
	}

	public static final Creator CREATOR = new Creator() {
		public NewYoutubeUrl createFromParcel(Parcel in) {
			return new NewYoutubeUrl(in);
		}

		public NewYoutubeUrl[] newArray(int size) {
			return new NewYoutubeUrl[size];
		}
	};

	@Override
	public String toString() {
		return "NewYoutubeUrl [Url=" + Url + ", Caption=" + Caption + ", Description=" + Description + ", Keyword=" + Keyword
				+ ", SportsID=" + SportsID + ", VideoLength=" + VideoLength + "]";
	}

}
