package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class Song implements Parcelable {
	public String SourceGUID;
	public String PlayListGUID;
	public String SourceName;// amazon
	public String SongGUID;
	public String Title;// Driftwood
	public String ArtistName;// Cody Sympson
	public String AlbumName;// Free
	public String SongPreviewURL;
	public String SongURL;
	public String ImageURL;// https://upload.wikimedia.org/wikipedia/en/8/8d/Album_this_is_your_song_cover.jpg
	public String SourceSongID;// 12345
	public String header;// CURRENT SONG OF THE DAY
	public boolean selected;
	public boolean existing;

	public Song(String id, String provider, String header, String title, String cover, String author, String album) {
		super();
		this.SourceName = provider;
		this.Title = title;
		this.ArtistName = author;
		this.AlbumName = album;
		this.ImageURL = cover;
		this.SourceSongID = id;
		this.header = header;
		this.selected = false;
		this.existing = false;
	}

	public Song(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			this.SourceGUID = jsonObject.optString("SourceGUID", "");
			this.PlayListGUID = jsonObject.optString("PlayListGUID", "");
			this.SourceName = jsonObject.optString("SourceName", "");
			this.SongGUID = jsonObject.optString("SongGUID", "");
			this.Title = jsonObject.optString("Title", "0");
			this.ArtistName = jsonObject.optString("ArtistName", "");
			this.AlbumName = jsonObject.optString("AlbumName", "");
			this.SongPreviewURL = jsonObject.optString("SongPreviewURL", "");
			this.SongURL = jsonObject.optString("SongURL", "");
			this.ImageURL = jsonObject.optString("ImageURL", "");
			this.SourceSongID = jsonObject.optString("SourceSongID", "");
		}
	}

	public boolean isValid(){
		return !TextUtils.isEmpty(SongGUID);
	}

	public JSONObject toJSONObject(boolean onlyGUID) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("SongGUID", TextUtils.isEmpty(SongGUID) ? "" : SongGUID);
			if (!onlyGUID) {
				jsonObject.put("Title", TextUtils.isEmpty(Title) ? "" : Title);
				jsonObject.put("ArtistName", TextUtils.isEmpty(ArtistName) ? "" : ArtistName);
				jsonObject.put("AlbumName", TextUtils.isEmpty(AlbumName) ? "" : AlbumName);
				jsonObject.put("SongPreviewURL", TextUtils.isEmpty(SongPreviewURL) ? "" : SongPreviewURL);
				jsonObject.put("SongURL", TextUtils.isEmpty(SongURL) ? "" : SongURL);
				jsonObject.put("ImageURL", TextUtils.isEmpty(ImageURL) ? "" : ImageURL);
				jsonObject.put("SourceSongID", TextUtils.isEmpty(SourceSongID) ? "" : SourceSongID);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public static JSONArray getJSONArray(List<Song> list, boolean onlyGUID) {
		JSONArray jsonArray = new JSONArray();
		if (null != list) {
			for (Song song : list) {
				jsonArray.put(song.toJSONObject(onlyGUID));
			}
		}
		return jsonArray;
	}

	public static List<Song> getSongs(JSONArray jsonArray) {
		List<Song> list = new ArrayList<Song>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new Song(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public Song(Parcel in) {
		PlayListGUID = in.readString();
		SourceName = in.readString();
		SongGUID = in.readString();
		Title = in.readString();
		ArtistName = in.readString();
		AlbumName = in.readString();
		SongPreviewURL = in.readString();
		SongURL = in.readString();
		ImageURL = in.readString();
		SourceSongID = in.readString();
		header = in.readString();
		selected = in.readInt() == 1;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(PlayListGUID);
		dest.writeString(SourceName);
		dest.writeString(SongGUID);
		dest.writeString(Title);
		dest.writeString(ArtistName);
		dest.writeString(AlbumName);
		dest.writeString(SongPreviewURL);
		dest.writeString(SongURL);
		dest.writeString(ImageURL);
		dest.writeString(SourceSongID);
		dest.writeString(header);
		dest.writeInt(selected ? 1 : 0);
	}

	public int describeContents() {
		return 0;
	}

	public static final Creator CREATOR = new Creator() {
		public Song createFromParcel(Parcel in) {
			return new Song(in);
		}

		public Song[] newArray(int size) {
			return new Song[size];
		}
	};

	@Override
	public boolean equals(Object o) {
		if (null != o && o instanceof Song) {
			return this.SourceSongID.equalsIgnoreCase(((Song) o).SourceSongID);
		}
		return super.equals(o);
	}

	@Override
	public String toString() {
		return "Song{" +
				"SourceGUID='" + SourceGUID + '\'' +
				", PlayListGUID='" + PlayListGUID + '\'' +
				", SourceName='" + SourceName + '\'' +
				", SongGUID='" + SongGUID + '\'' +
				", Title='" + Title + '\'' +
				", ArtistName='" + ArtistName + '\'' +
				", AlbumName='" + AlbumName + '\'' +
				", SongPreviewURL='" + SongPreviewURL + '\'' +
				", SongURL='" + SongURL + '\'' +
				", ImageURL='" + ImageURL + '\'' +
				", SourceSongID='" + SourceSongID + '\'' +
				", header='" + header + '\'' +
				", selected=" + selected +
				'}';
	}
}
