package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BlogModel {
	public String BlogGUID;// ":1,
	public String Title;
	public String Description;
	public String Author;
	public int NoOfComments;
	public int NoOfLikes;
	public String CreatedDate;
	public String ModifiedDate;
	public CoverMediaModel mConCoverMediaModel;
	public List<AlbumMedia> mMediaList;
	public int IsLike;

	// "BlogGUID": "341e30da-560e-0f86-5c3e-36cc8ebd1d4a",
	// "Title": "Title",
	// "Description": "Description",
	// "Author": "Daniel",
	// "NoOfComments": 10,
	// "NoOfLikes": "2",
	// "CreatedDate": "2015-05-07 05:07:36",
	// "ModifiedDate": "2015-05-07 05:07:36",
	public BlogModel(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			BlogGUID = jsonObject.optString("BlogGUID").trim();
			Title = jsonObject.optString("Title").trim();
			Description = jsonObject.optString("Description").trim();
			Author = jsonObject.optString("Author").trim();
			NoOfComments = jsonObject.optInt("NoOfComments", 0);
			NoOfLikes = jsonObject.optInt("NoOfLikes", 0);
			CreatedDate = jsonObject.optString("CreatedDate");
			ModifiedDate = jsonObject.optString("ModifiedDate");
			mConCoverMediaModel = new CoverMediaModel(jsonObject.optJSONObject("CoverMedia"));
			List<AlbumMedia> list = AlbumMedia.getAlbumMedias(jsonObject.optJSONArray("Media"));
			mMediaList = list;
			IsLike = jsonObject.optInt("IsLike", 0);
		}
	}

	public static List<BlogModel> getBlogList(JSONArray jsonArray) {
		List<BlogModel> list = new ArrayList<BlogModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new BlogModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

}
