package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TranscriptsRequestModel {
	
	public String mUserGUID;
	public String mUserFName;
	public String mUserLName;
	public String mUserProfileURL;
	public String mUserProfilePicture;
	public String mCreatedDate;
	public String mStatus;
	
	public TranscriptsRequestModel(JSONObject jsonObject){
		if(jsonObject!=null){
			this.mUserGUID = jsonObject.optString("UserGUID");
			this.mUserFName = jsonObject.optString("FirstName");
			this.mUserLName = jsonObject.optString("LastName");
			this.mUserProfileURL = jsonObject.optString("ProfileURL");
			this.mUserProfilePicture = jsonObject.optString("ProfilePicture");
			this.mCreatedDate = jsonObject.optString("CreatedDate");
			this.mStatus = jsonObject.optString("Status");
		}
	}
	
	
	public static List<TranscriptsRequestModel> getTranscriptsRequest(JSONArray jsonArray) {
		List<TranscriptsRequestModel> list = new ArrayList<TranscriptsRequestModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new TranscriptsRequestModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}


}
