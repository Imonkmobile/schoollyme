package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.schollyme.R;

import android.os.Parcel;
import android.os.Parcelable;

public class SongSource implements Parcelable {
	public static final int DEFAULT_SONG_SOUCE_IMG_RES_ID = R.drawable.icon_no_media;

	public String SourceGUIID;
	public String PlayListGUID;
	public String SourceName;
	public int NoOfSongs;
	public int CurrentSongOfTheDay;
	public String CreatedDate;
	public String ModifiedDate;
	public boolean selected = false;
	public int ImageResouceId = R.drawable.ic_launcher;

	public SongSource(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			this.SourceGUIID = jsonObject.optString("SourceGUIID", "0");
			this.PlayListGUID = jsonObject.optString("PlayListGUID", "");
			this.SourceName = jsonObject.optString("SourceName", "");
			this.NoOfSongs = jsonObject.optInt("NoOfSongs", 0);
			this.CurrentSongOfTheDay = jsonObject.optInt("CurrentSongOfTheDay", 0);
			this.CreatedDate = jsonObject.optString("CreatedDate", "");
			this.ModifiedDate = jsonObject.optString("ModifiedDate", "");
			this.ImageResouceId = getSongSourceImgResId(this.SourceName);
		}
	}

	public static int getSongSourceImgResId(String sourceName) {
		int imgResId = DEFAULT_SONG_SOUCE_IMG_RES_ID;
		if ("Spotify".equalsIgnoreCase(sourceName)) {
			imgResId = R.drawable.ic_spotify;
		} else if ("iTunes".equalsIgnoreCase(sourceName)) {
			imgResId = R.drawable.ic_itunes;
		} else if ("SchollyMe".equalsIgnoreCase(sourceName)) {
			imgResId = R.drawable.ic_schollyme;
		}
		return imgResId;
	}

	public static List<SongSource> getSongSources(JSONArray jsonArray) {
		List<SongSource> list = new ArrayList<SongSource>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new SongSource(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public SongSource(Parcel in) {
		SourceGUIID = in.readString();
		PlayListGUID = in.readString();
		SourceName = in.readString();
		NoOfSongs = in.readInt();
		CurrentSongOfTheDay = in.readInt();
		CreatedDate = in.readString();
		ModifiedDate = in.readString();
		ImageResouceId = in.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(SourceGUIID);
		dest.writeString(PlayListGUID);
		dest.writeString(SourceName);
		dest.writeInt(NoOfSongs);
		dest.writeInt(CurrentSongOfTheDay);
		dest.writeString(CreatedDate);
		dest.writeString(ModifiedDate);
		dest.writeInt(ImageResouceId);
	}

	public int describeContents() {
		return 0;
	}

	public static final Creator CREATOR = new Creator() {
		public SongSource createFromParcel(Parcel in) {
			return new SongSource(in);
		}

		public SongSource[] newArray(int size) {
			return new SongSource[size];
		}
	};

	@Override
	public String toString() {
		return "SongSource [SourceGUIID=" + SourceGUIID + ", PlayListGUID=" + PlayListGUID + ", SourceName=" + SourceName + ", NoOfSongs="
				+ NoOfSongs + ", CurrentSongOfTheDay=" + CurrentSongOfTheDay + ", CreatedDate=" + CreatedDate + ", ModifiedDate="
				+ ModifiedDate + ", selected=" + selected + ", ImageResouceId=" + ImageResouceId + "]";
	}

}
