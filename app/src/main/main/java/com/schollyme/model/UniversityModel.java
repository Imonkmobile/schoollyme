package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class UniversityModel implements Parcelable{
	
	public String mUniversityId;
	public String mConferenceId;
	public String mUniversityName;
	public String mUniversityEmail;
	public String mUniversityContactName;
	public String mUniversityPhoneNumber;
	public String mUniversityPosition;
	public boolean isSelected = false;
	
	
	public UniversityModel(JSONObject mJsonObj){
		try {
			this.mConferenceId = mJsonObj.optString("ConferenceID");
			this.mUniversityId = mJsonObj.optString("UniversityID");
			this.mUniversityName = mJsonObj.optString("Name");
			this.mUniversityEmail = mJsonObj.optString("Email");
			this.mUniversityContactName = mJsonObj.optString("ContactName");
			this.mUniversityPhoneNumber = mJsonObj.optString("PhoneNumber");
			this.mUniversityPosition = mJsonObj.optString("Position");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static List<UniversityModel> getUniversities(JSONArray jsonArray) {
		List<UniversityModel> list = new ArrayList<UniversityModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new UniversityModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	public UniversityModel(Parcel in) {
		mConferenceId = in.readString();
		mUniversityId = in.readString();
		mUniversityName = in.readString();
		mUniversityEmail = in.readString();
		mUniversityContactName = in.readString();
		mUniversityPhoneNumber = in.readString();
		mUniversityPosition = in.readString();
		isSelected = (in.readInt() == 1);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mConferenceId);
		dest.writeString(mUniversityId);
		dest.writeString(mUniversityName);
		dest.writeString(mUniversityEmail);
		dest.writeString(mUniversityContactName);
		dest.writeString(mUniversityPhoneNumber);
		dest.writeString(mUniversityPosition);
		
		dest.writeInt(isSelected ? 1 : 0);
	}

	public int describeContents() {
		return 0;
	}

	public static final Creator CREATOR = new Creator() {
		public UniversityModel createFromParcel(Parcel in) {
			return new UniversityModel(in);
		}

		public UniversityModel[] newArray(int size) {
			return new UniversityModel[size];
		}
	};

	

	@Override
	public String toString() {
		return "UniversityModel [mUniversityId=" + mUniversityId
				+ ", mConferenceId=" + mConferenceId + ", mUniversityName="
				+ mUniversityName + ", mUniversityEmail=" + mUniversityEmail
				+ ", mUniversityContactName=" + mUniversityContactName
				+ ", mUniversityPhoneNumber=" + mUniversityPhoneNumber
				+ ", mUniversityPosition=" + mUniversityPosition + "]";
	}


	@Override
	public boolean equals(Object o) {
		if (null != o && o instanceof UniversityModel) {
			return this.mUniversityId.equalsIgnoreCase(((UniversityModel) o).mUniversityId);
		}
		return super.equals(o);
	}
}