package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TeamMembers {
	
	private String mTeamUserGUID;
	private String mTeamUserName;
	private String mTeamUserProfilePicURL;
	private boolean isAdmin;
	
	public TeamMembers(JSONObject mJsonObject){
		try {
			this.mTeamUserGUID = mJsonObject.optString("");
			this.mTeamUserName = mJsonObject.optString("");
			this.mTeamUserProfilePicURL = mJsonObject.optString("");
			String mIsAdmin = mJsonObject.optString("");
			if(mIsAdmin.equals("1")){
				this.isAdmin = true;
			}
			else{
				this.isAdmin = false; 
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<TeamMembers> getTeamMembers(JSONArray jsonArray) {
		List<TeamMembers> list = new ArrayList<TeamMembers>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new TeamMembers(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}