package com.schollyme.model;

import org.json.JSONObject;

public class UserTypeModel {

	public String mTypeName;
	public String mTypeId;


	public UserTypeModel(JSONObject mJson){
		if(mJson!=null){
			this.mTypeName = mJson.optString("Name");
			this.mTypeId = mJson.optString("UserTypeID");
		}
	}
}