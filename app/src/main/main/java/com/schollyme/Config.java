package com.schollyme;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.schollyme.model.Album;

public class Config {
	/**
	 * Fixed, it is android device
	 */
	public static final String DEVICE_TYPE_ID = "AndroidPhone";

	public static final String NOTIFICATION_SETTINGS = "NotificationSettings";
	/**
	 * Message to show when request is left without completion
	 */
	public static final String REQUEST_STOPPED_MSG = "Activity is not running, quitting request";
	/**
	 * Debugging is on, print all logs
	 */
	public static final boolean DEBUG = true;

	public static final int ERROR_MESSAGE_DISPLAY_TIME = 3000;
	/**
	 * App wide page size
	 */
	public static final int PAGE_SIZE = 20;
	public static final int PAGE_SIZE_10 = 10;
	/**
	 * App wide default page index
	 */
	public static final int DEFAULT_PAGE_INDEX = 1;
	/**
	 * Application mode development-devel or production-prod
	 */
	public static final String ENVIRONMENT = "devel";

// public static final String ENVIRONMENT = "staging";

	public static int PAGINATION_PAGE_SIZE = 20;
	/** Number of items per page */
	public static String DEVICE_TYPE = "AndroidPhone";

	public static final int USER_TYPE_COACH = 1;
	public static final int USER_TYPE_ATHLETE = 2;
	public static final int USER_TYPE_FAN = 3;
	public static final int USER_TYPE_EVALUATOR = 4;
	public static final int USER_TYPE_PAID_COACH = 5;

	public enum UserType {
		Coach, Athlete, Fan
	}

	/**
	 * Page Type
	 * 
	 * */

	public static final String PAGE_TYPE_OFFICIAL = "1";
	public static final String PAGE_TYPE_FAN = "2";

	public static final String ABOUT_TAB = "Aboute";
	public static final String Media_TAB = "Media";
	public static final String Friends_TAB = "Aboute";
	public static final String Team_TAB = "Team";

	/**
	 * Activity type for team page an admin can perform
	 * 
	 * */

	public static final String IN_ACTIVATE_PAGE = "Inactive";
	public static final String ACTIVATE_PAGE = "Active";
	public static final String DELETE_PAGE = "Delete";

	/**
	 * Base API URL
	 */
	public static String SERVICE_URL;
	/**
	 * Base Upload data URL
	 */
	public static String UPLOAD_URL;
	/**
	 * Base URL for getting image from server, in case image path only contains
	 * name
	 */
	public static String IMAGE_URL_PROFILE;
	public static String IMAGE_URL_PROFILE_ORIGINAL;
	public static String IMAGE_URL_UPLOAD;
	public static String VIDEO_URL_UPLOAD;
	public static String COVER_URL;
	public static String DEFAULT_USER_PROFILE_IMAGE_URL = "140_profile_default.jpg";
	public static String DEFAULT_USER_COVER_IMAGE_URL = "812_cover_default.jpg";
	public static String ALBUM_FOLDER = "album/";
	public static String VIDEO_FOLDER = "video/";
	public static String THUMB_FOLDER = "196x196/";
	public static String WALL_FOLDER = "wall/";
	public static String PROFILE_FOLDER = "profile/";

	public static String BLOG_FOLDER = "blog/";
	/**
	 * 
	 * Message change status
	 */
	public static final int MOVE_MESSGAE_TO_TRASH = 12;
	public static final long MESSAGE_WINDOW_SYNC_DELAY = 30 * 1000;
	public static final long NOTIFICATION_UPDATE_DELAY = 10 * 1000;

	/**
	 * Static block initialization
	 */

	/*
	 * After Select Category of Professional/Client ,get URL of category type
	 */
	public static String SELECTION_URL = "";

	/*
	 * Get IP Address of Device
	 */
	public static String DeviceIPAddress = "";

	/*
	 * Get Device ID
	 */
	public static String DEVICE_ID = "";

	static {
		if ("devel".equalsIgnoreCase(ENVIRONMENT)) {
			SERVICE_URL = "http://103.21.54.68/527-schollyme/api/"; // http://192.168.0.18/527-schollyme/api/
			IMAGE_URL_PROFILE = "https://schollyme-qa.s3-us-west-1.amazonaws.com/uploads/profile/196x196/";
			IMAGE_URL_PROFILE_ORIGINAL = "https://schollyme-qa.s3-us-west-1.amazonaws.com/uploads/profile/";
			IMAGE_URL_UPLOAD = "https://schollyme-qa.s3-us-west-1.amazonaws.com/uploads/";
			VIDEO_URL_UPLOAD = "https://schollyme-qa.s3-us-west-1.amazonaws.com/uploads/";
			COVER_URL = "https://schollyme-qa.s3-us-west-1.amazonaws.com/uploads/profilebanner/";
		} else if ("staging".equalsIgnoreCase(ENVIRONMENT)) {
			SERVICE_URL = "http://45.55.250.99/api/";
			IMAGE_URL_PROFILE = "https://schollyme-live.s3-us-west-1.amazonaws.com/uploads/profile/196x196/";
			IMAGE_URL_PROFILE_ORIGINAL = "https://schollyme-qa.s3-us-west-1.amazonaws.com/uploads/profile/";
			IMAGE_URL_UPLOAD = "https://schollyme-live.s3-us-west-1.amazonaws.com/uploads/";
			VIDEO_URL_UPLOAD = "https://schollyme-live.s3-us-west-1.amazonaws.com/uploads/";
			COVER_URL = "https://schollyme-live.s3-us-west-1.amazonaws.com/uploads/profilebanner/";
		} else if ("prod".equalsIgnoreCase(ENVIRONMENT)) {

		} else {
			SERVICE_URL = "SERVICE_URL not set";
			UPLOAD_URL = "UPLOAD_URL not set";
			IMAGE_URL_PROFILE = "IMAGE_URL not set";
		}
	}

	/**
	 * Maximum video length allowed in bytes
	 */
	public static final long MAX_VIDEO_LEN_BYTES = 40 * 1024 * 1024;

	/**
	 * Maximum video length allowed in milli seconds
	 */
	public static final long MAX_VIDEO_LEN_MILLIS = 10 * 60 * 1000;

	/**
	 * Error shown when no response received or unrecognized response received
	 */
	public static final String DEFAULT_SERVICE_ERROR = "Can not connect to server!";

	public static int getWindowWidth(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		int width = displayMetrics.widthPixels;
		int height = displayMetrics.heightPixels;
		return width;
	}

	public static String FOLDER_WALL_PHOTOS = "Wall Photos";
	public static String FOLDER_WALL_VIDEO = "Wall Videos";
	public static String FOLDER_PROFILE_PHOTOS = "Profile Photos";

	public static boolean isWallFolder(String folder) {
		if (FOLDER_WALL_PHOTOS.equalsIgnoreCase(folder) || FOLDER_WALL_VIDEO.equalsIgnoreCase(folder)
				|| FOLDER_PROFILE_PHOTOS.equalsIgnoreCase(folder)) {
			return true;
		}
		return false;
	}

	public static String getCoverPath(Album album, String name, boolean isVideo) {
		return getCoverPath(album, name, isVideo, false);
	}

	public static String getCoverPath(Album album, String name, boolean isVideo, boolean fullImage) {
		String mainFolder = Config.ALBUM_FOLDER;
		if (FOLDER_WALL_PHOTOS.equalsIgnoreCase(album.AlbumName) || FOLDER_WALL_VIDEO.equalsIgnoreCase(album.AlbumName)) {
			mainFolder = Config.WALL_FOLDER;
		} else if (FOLDER_PROFILE_PHOTOS.equalsIgnoreCase(album.AlbumName)) {
			mainFolder = Config.PROFILE_FOLDER;
		}
		String subFolder = (isVideo ? Config.VIDEO_FOLDER : (fullImage ? "" : Config.THUMB_FOLDER));
		return Config.IMAGE_URL_UPLOAD + mainFolder + subFolder + name;
	}

	public static String getCoverMediaPathBLOG(String name) {
		return Config.IMAGE_URL_UPLOAD + Config.BLOG_FOLDER + name;
	}

	public static String getMediaPathBLOG(String name) {
		return Config.IMAGE_URL_UPLOAD + Config.BLOG_FOLDER + name;
	}

	// https://schollyme-qa.s3-us-west-1.amazonaws.com/uploads/blog/video/a2125abe20974e2de9f999d02a5dfebf.jpg

	public static String getBlogVideopath(String name) {
		return Config.IMAGE_URL_UPLOAD + Config.BLOG_FOLDER + Config.VIDEO_FOLDER + name;
	}

	public static String getVideoToJPG(String imgName) {
		if (!TextUtils.isEmpty(imgName)) {
			if (imgName.endsWith("mp4")) {
				imgName = imgName.replaceAll(".mp4", ".jpg");
			} else if (imgName.endsWith("MOV")) {
				imgName = imgName.replaceAll(".MOV", ".jpg");
			} else if (imgName.endsWith("mov")) {
				imgName = imgName.replaceAll(".mov", ".jpg");
			}
		}
		return imgName;
	}
}
