package com.schollyme;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.activity.DashboardActivity;
import com.schollyme.media.MediaPickerActivity;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.utility.Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Base activity class. This may be useful in<br/>
 * Implementing google analytics or <br/>
 * Any app wise implementation
 *
 * @author ravi
 */
public class BaseFragmentActivity extends BaseLocaleActivity {

    private static TextView actionBarTitle;
    private static ImageView rightButton;
    private static ImageView leftButton;
    private static ProgressBar loadingPb;
    private static Context context;

    protected static final int REQ_CODE_TAKE_FROM_CAMERA = 500;
    protected static final int REQ_CODE_PICK_FROM_GALLERY = 501;
    protected static final int REQ_CODE_CROP_PHOTO = 502;
    protected static final int IMAGE_DIMENSION = 1080;
    protected static final int REQ_CODE_RECORD_VIDEO = 503;
    protected static final int REQ_CODE_SELECT_IMAGE = 300;

    private static final String IMAGE_UNSPECIFIED = "image/*";
    protected Uri capturedImageUri, cropImageUri;
    protected Bitmap capturedImageBitmap = null;
    private LogedInUserModel mLogedInUserModel;
    protected Boolean isCropCall = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        if (TextUtils.isEmpty(BaseRequest.getLoginSessionKey())) {
            BaseRequest.setLoginSessionKey(new LogedInUserModel(this).mLoginSessionKey);
        }
    }

    public static void setHeader(int leftButtonId, int rightButtonId, String title, OnClickListener
            leftButtonListener, OnClickListener rightButtonListener) {

        actionBarTitle = (TextView) ((Activity) context).findViewById(R.id.header_tv);
        rightButton = (ImageView) ((Activity) context).findViewById(R.id.right_button);
        leftButton = (ImageView) ((Activity) context).findViewById(R.id.back_button);
        loadingPb = (ProgressBar) ((Activity) context).findViewById(R.id.loading_h_pb);
        actionBarTitle.setText(title);
        leftButton.setImageResource(leftButtonId);
        leftButton.setOnClickListener(leftButtonListener);
        rightButton.setImageResource(rightButtonId);
        rightButton.setOnClickListener(rightButtonListener);

        rightButton.setVisibility(rightButtonId > 0 ? View.VISIBLE : View.GONE);
    }

    public static void setHeaderTitle(String title) {
        actionBarTitle.setText(title);
    }

    public static void hideRightButton() {
        rightButton.setVisibility(View.INVISIBLE);
    }

    public static void showLoader(boolean show) {
        loadingPb.setVisibility(show ? View.VISIBLE : View.GONE);
        rightButton.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    public static ImageView getRightButton() {
        return rightButton;
    }

    @Override
    public void onLocaleUpdate() {

    }

    private final int mOutputX =  960, mAspectX = 1;
    private int mOutputY =  960, mAspectY = 1;

    protected void openCamera(float aspect) {
        mOutputY = (int)(mOutputX / aspect);
        mAspectY = (int) aspect;
        openCamera();
    }

    /**
     * This method use for take picture from camera and crop function
     */

    protected void openCamera() {

        initTmpUris();

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
        intent.putExtra("return-data", true);
        try {
            // Start a camera capturing activity
            // REQUEST_CODE_TAKE_FROM_CAMERA is an integer tag you
            // defined to identify the activity in onActivityResult()
            // when it returns
            startActivityForResult(intent, REQ_CODE_TAKE_FROM_CAMERA);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
        return;
    }

    protected void initTmpUris() {

        File proejctDirectory = new File(Environment.getExternalStorageDirectory()
                + File.separator + "SchollyMeCache");
        if (!proejctDirectory.exists()) {
            proejctDirectory.mkdir();
        } else {
            // delete all old files
            for (File file : proejctDirectory.listFiles()) {
                if (file.getName().startsWith("tmp_")) {
                    file.delete();
                }
            }
        }
        // Construct temporary image path and name to save the taken photo
        capturedImageUri = Uri.fromFile(new File(proejctDirectory,
                "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
        File extraOutputFile = new File(proejctDirectory,
                "croped_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        extraOutputFile.setWritable(true);
        cropImageUri = Uri.fromFile(extraOutputFile);
    }

    protected void openGallery(float aspect) {
        mOutputY = (int)(mOutputX / aspect);
        mAspectY = (int) aspect;
        openGallery();
    }

    /**
     * This method use for open gallery and crop image using default crop of
     * android
     */
    protected void openGallery() {
        System.out.println("MediaPickerActivity - openGallery SDK_INT=" + Build.VERSION.SDK_INT);
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Complete action using"), REQ_CODE_PICK_FROM_GALLERY);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, REQ_CODE_PICK_FROM_GALLERY);
        }
    }

    /*
    * This method use for Crop image taken from camera
    */
    private void cropImage() {
        // Use existing crop activity.
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");
        intent.setDataAndType(capturedImageUri, IMAGE_UNSPECIFIED); // Uri to the image you want to crop
		/*intent.putExtra("outputX", 296);
		intent.putExtra("outputY", 296);*/
        if (DashboardActivity.imageCaptureType.equalsIgnoreCase("Profile")) {
            intent.putExtra("outputX", mOutputX);
            intent.putExtra("outputY", mOutputY);
            intent.putExtra("aspectX", mAspectX);
            intent.putExtra("aspectY", mAspectY);
        } else {
            intent.putExtra("outputX", 960);
            intent.putExtra("outputY", 480);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 0.5);
        }
        intent.putExtra("scale", true);
        intent.putExtra("circleCrop", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cropImageUri);
        startActivityForResult(intent, REQ_CODE_CROP_PHOTO);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_TAKE_FROM_CAMERA:
                if (resultCode == RESULT_OK) {
                    System.out.println("REQ_CODE_TAKE_FROM_CAMERA imageUri: " + capturedImageUri);
                    if(isCropCall) {
                        isCropCall = false;
                        cropImage();
                    }
                } else {
                    //onMediaPickCanceled(REQ_CODE_TAKE_FROM_CAMERA);
                }
                break;
            case REQ_CODE_PICK_FROM_GALLERY:
                if (resultCode == RESULT_OK && data.getData() != null) {
                    if(isCropCall) {
                        isCropCall = false;
                        initTmpUris();
                        capturedImageUri = data.getData();
                        cropImage();
                    }
                } else {
                    //onMediaPickCanceled(REQ_CODE_PICK_FROM_GALLERY);
                }
                break;
            case REQ_CODE_CROP_PHOTO:
                if (resultCode == RESULT_OK) {
                    try {
                        capturedImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                                cropImageUri);
                        Utility.LogP("REQ_CODE_CROP_PHOTO ","cropImageUri: "+cropImageUri.toString());
                        onCameraImageSelected(cropImageUri.toString(), capturedImageBitmap);
                    } catch (FileNotFoundException e) {
                        Utility.LogP("REQ_CODE_CROP_PHOTO ","FileNotFoundException: "+cropImageUri.toString());
                        e.printStackTrace();
                    } catch (IOException e) {
                        Utility.LogP("REQ_CODE_CROP_PHOTO ","IOException: "+cropImageUri.toString());
                        e.printStackTrace();
                    }
                } else {
                    //onMediaPickCanceled(REQ_CODE_CROP_PHOTO);
                }
                break;
//            case REQ_CODE_RECORD_VIDEO:
//                if (resultCode == RESULT_OK) {
//                    Uri vid = data.getData();
//                    onVideoCaptured(getVideoPathFromURI(vid));
//                } else {
//                    //onMediaPickCanceled(REQ_CODE_RECORD_VIDEO);
//                }
//                break;

            default:
                break;
        }
    }

    private String getVideoPathFromURI(Uri contentUri) {
        String videoPath = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            videoPath = cursor.getString(column_index);
        }
        cursor.close();
        return videoPath;
    }

//    public void onGalleryImageSelected(String fileUri, Bitmap bitmap) {
//        File mFile = new File(cropImageUri.getPath());
//        if(mFile.exists()) {
//            if (DashboardActivity.imageCaptureType.equalsIgnoreCase("Profile")) {
//                uploadMediaServerRequest(MediaUploadRequest.TYPE_PROFILE);
//            } else {
//                uploadMediaServerRequest(MediaUploadRequest.TYPE_GROUP_COVER);
//            }
//        } else {
//
//        }
//    }

    public void onCameraImageSelected(String fileUri, Bitmap bitmap) {

        File mFile = new File(cropImageUri.getPath());
        if(mFile.exists()) {
            if (DashboardActivity.imageCaptureType.equalsIgnoreCase("Profile")) {
                uploadMediaServerRequest(MediaUploadRequest.TYPE_PROFILE, "profile");
            } else {
                uploadMediaServerRequest(MediaUploadRequest.TYPE_GROUP_COVER, "cover");
            }
        } else {

        }
    }

    /**
     * It will upload user profile pic to server and return a media id,
     * that will be appended to user profile
     */
    private void uploadMediaServerRequest(String imageType, final String type) {

        mLogedInUserModel = new LogedInUserModel(this);
        MediaUploadRequest mRequest = new MediaUploadRequest(context, false);
        String fileName = ImageUtil.getFileNameFromURI(cropImageUri, context);
        File mFile = new File(cropImageUri.getPath());

        BaseRequest.setLoginSessionKey(mLogedInUserModel.mLoginSessionKey);
        mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_USERS,
                mLogedInUserModel.mUserGUID, imageType); //MediaUploadRequest.TYPE_PROFILE
        mRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    cropImageUri = null;
                    AlbumMedia mAlbumMedia = (AlbumMedia) data;
                    mLogedInUserModel.mUserPicURL = mAlbumMedia.ImageName;
                    mLogedInUserModel.persist(context);
                    Toast.makeText(context, "Successfully uploaded "+type+" image.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    //public void onVideoCaptured(String videoPath);

    //public void onMediaPickCanceled(int reqCode);
}