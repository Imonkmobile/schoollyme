package com.schollyme.inbox;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.schollyme.BaseFragmentActivity;
import com.schollyme.media.CreateAlbumActivity;
import com.schollyme.model.Album;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AlbumListRequest;
import com.vinfotech.utility.Config;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.R;
import com.schollyme.adapter.InboxAdapter;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.model.FriendModel;
import com.schollyme.model.InboxModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.InboxChangeMessageStatusRequest;
import com.vinfotech.request.InboxRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

/**
 * Activity class. This may be useful in display list of previous chat with users
 *
 * @author Ravi Bhandari
 */
public class InboxFragment extends BaseFragment implements OnClickListener, OnRefreshListener {

    private Context mContext;
    private SwipeMenuListView mInboxLV;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private List<InboxModel> mUserInboxModelAL, mFilterAL;
    private InboxAdapter mAdapter;
    private EditText mFriendSearchET;
    private ImageButton mClearIB;
    private int pageNumber = 1;
    private int selectedPosition;
    private TextView mNoRecordTV;
    private boolean isProcessing = false;
    private Timer timer;
    private RelativeLayout mMainLL;
    private LinearLayout search_view_ll;
    private boolean loadingFlag = false;

    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;

    public static BaseFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        InboxFragment mFragment = new InboxFragment();
        mFragment.mCurrentPageToShow = currentPageToShow;
        mFragment.mHeaderLayout = headerLayout;
        mFragment.mShowBackBtn = showBack;

        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inbox_activity, container, false);
        mContext = getActivity();

        pageNumber = 1;
        initHeader();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mInboxLV = (SwipeMenuListView) view.findViewById(R.id.invite_user_lv);
        mFriendSearchET = (EditText) view.findViewById(R.id.search_et);
        mClearIB = (ImageButton) view.findViewById(R.id.clear_text_ib);
        mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
        search_view_ll = (LinearLayout) view.findViewById(R.id.search_view_ll);
        search_view_ll.setVisibility(View.GONE);

        mMainLL = (RelativeLayout) view.findViewById(R.id.main_ll);
        mMainLL.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                Utility.hideSoftKeyboard(mFriendSearchET);
                return false;
            }
        });

        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
                R.color.text_hint_color);

        mSwipeRefreshWidget.setOnRefreshListener(this);
        DashboardActivity.hideShowActionBar(true);
        mFriendSearchET.addTextChangedListener(new CustomeTextWatcher());
        mClearIB.setOnClickListener(this);
        FontLoader.setRobotoRegularTypeface(mFriendSearchET, mNoRecordTV);
        mUserInboxModelAL = new ArrayList<InboxModel>();

        syncMessage();

        mFriendSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utility.hideSoftKeyboard(mFriendSearchET);
                    return true;
                }
                return false;
            }
        });

        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) view.findViewById(R.id.addNewMessageMenu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                fabMenu.collapse();
                startActivity(NewMessageFindUserActivity.getIntent(mContext));
                ((Activity) mContext).overridePendingTransition(R.anim.slide_up_dialog, 0);
            }

            @Override
            public void onMenuCollapsed() {

            }
        });
    }

    private void initHeader() {

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                getResources().getString(R.string.message), R.drawable.ic_search_header);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.hideSoftKeyboard(mFriendSearchET);
                ((DashboardActivity) mContext).sliderListener();
            }
        }, new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                if (search_view_ll.getVisibility() == View.VISIBLE) {
                    search_view_ll.setVisibility(View.GONE);
                } else {
                    search_view_ll.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mFriendSearchET.setText("");
        getInboxRequest();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (null != timer) {
            timer.cancel();
        }
    }

    private void setInboxAdapter() {

        mAdapter = new InboxAdapter(mContext);
        if (mFilterAL.size() == 0) {
            mAdapter.setList(mFilterAL);
            mInboxLV.setAdapter(mAdapter);
            mNoRecordTV.setVisibility(View.VISIBLE);
            mNoRecordTV.bringToFront();
        } else {
            mNoRecordTV.setVisibility(View.GONE);
            mInboxLV.setVisibility(View.VISIBLE);
            mInboxLV.setMenuCreator(creator);
            mInboxLV.setAdapter(mAdapter);
            mAdapter.setList(mFilterAL);
            mInboxLV.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    String userName;
                    FriendModel mFriendModel = new FriendModel();
                    if (new LogedInUserModel(mContext).mUserGUID.equals(mFilterAL.get(position).mSenderUserGUID)) {
                        mFriendModel.mFirstName = mFilterAL.get(position).mReceiverUserFName;
                        mFriendModel.mLastName = mFilterAL.get(position).mReceiverUserLName;
                        mFriendModel.mProfilePicture = mFilterAL.get(position).mReceiverProfilePicture;
                        mFriendModel.mUserGUID = mFilterAL.get(position).mReceiverUserGUID;
                        mFriendModel.mProfileLink = mFilterAL.get(position).mReceiverUserProfileURL;
                        mFriendModel.mUserTypeID = mFilterAL.get(position).mReceiverUserTypeId;

                        userName = mFilterAL.get(position).mReceiverUserFName + " " + mFilterAL.get(position).mReceiverUserLName;
                    } else {
                        mFriendModel.mFirstName = mFilterAL.get(position).mSenderUserFName;
                        mFriendModel.mLastName = mFilterAL.get(position).mSenderUserLName;
                        mFriendModel.mProfilePicture = mFilterAL.get(position).mSenderProfilePicture;
                        mFriendModel.mUserGUID = mFilterAL.get(position).mSenderUserGUID;
                        mFriendModel.mProfileLink = mFilterAL.get(position).mSenderUserProfileURL;
                        mFriendModel.mUserTypeID = mFilterAL.get(position).mSenderUserTypeId;

                        userName = mFilterAL.get(position).mSenderUserFName + " " + mFilterAL.get(position).mSenderUserLName;
                    }

                    startActivity(ChatHistoryActivity.getIntent(mContext, userName,
                            mFriendModel, mFilterAL.get(position).mMessageGUID).putExtra("Title",
                            getResources().getString(R.string.message))
                            .putExtra("ProfilePicture", mFriendModel.mProfilePicture));

                }
            });

            mInboxLV.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int swipeItemIndex) {

                    selectedPosition = position;
                    mAdapter.notifyDataSetChanged();
                    updateMessageStatus(mFilterAL.get(position).mMessageGUID);
                    return false;
                }
            });

            mInboxLV.setOnScrollListener(new OnScrollListener() {
                boolean bReachedListEnd = false;

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
                        Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
                        if (null != mAdapter && !loadingFlag) {
                        }
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount / 2));
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear_text_ib:
                mFriendSearchET.setText("");
                mAdapter.setList(mFilterAL);
                break;
        }
    }

    public class CustomeTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable arg0) {

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence chars, int arg1, int arg2, int arg3) {
            if (chars.length() > 0) {
                mFilterAL = new ArrayList<InboxModel>();
                if (null != mUserInboxModelAL) {
                    for (int i = 0; i < mUserInboxModelAL.size(); i++) {
                        String fName = "", lName = "";
                        if (null != mUserInboxModelAL.get(i).mSenderUserFName && null != mUserInboxModelAL.get(i).mReceiverUserFName) {
                            if (!mUserInboxModelAL.get(i).mSenderUserFName.toLowerCase().equalsIgnoreCase("you")) {
                                fName = mUserInboxModelAL.get(i).mSenderUserFName.toLowerCase();
                                lName = mUserInboxModelAL.get(i).mSenderUserLName.toLowerCase();
                            } else {
                                fName = mUserInboxModelAL.get(i).mReceiverUserFName.toLowerCase();
                                lName = mUserInboxModelAL.get(i).mReceiverUserLName.toLowerCase();
                            }

                            String searchStr = chars.toString().toLowerCase();
                            if (fName.contains(searchStr) || lName.contains(searchStr)) {
                                mFilterAL.add(mUserInboxModelAL.get(i));
                            }
                        }
                    }
                }
                if (mAdapter != null)
                    mAdapter.setList(mFilterAL);
                if (mFilterAL.size() > 0) {
                    mInboxLV.setVisibility(View.VISIBLE);
                    mNoRecordTV.setVisibility(View.GONE);
                } else {
                    mInboxLV.setVisibility(View.GONE);
                    mNoRecordTV.setVisibility(View.VISIBLE);
                }
            } else {
                if (mAdapter != null) {
                    mInboxLV.setVisibility(View.VISIBLE);
                    mNoRecordTV.setVisibility(View.GONE);
                    mAdapter.setList(mUserInboxModelAL);
                } else {
                    mNoRecordTV.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    SwipeMenuCreator creator = new SwipeMenuCreator() {
        @Override
        public void create(SwipeMenu menu) {
            SwipeMenuItem deleteItem = new SwipeMenuItem(mContext);
            deleteItem.setBackground(new ColorDrawable(getResources().getColor(R.color.red)));
            deleteItem.setWidth(dp2px(65));
            deleteItem.setTitle(getResources().getString(R.string.Delete));
            deleteItem.setTitleSize(14);
            deleteItem.setIcon(R.drawable.ic_close_header);
            deleteItem.setTitleColor(Color.WHITE);
            menu.addMenuItem(deleteItem);
        }
    };

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    private void syncMessage() {
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {

            @Override
            public void run() {
                if (!isProcessing) {
                    getInboxRequest();
                }
            }
        };
        timer.schedule(doAsynchronousTask, Config.MESSAGE_WINDOW_SYNC_DELAY, Config.MESSAGE_WINDOW_SYNC_DELAY);
    }

    private void getInboxRequest() {
        isProcessing = true;
        InboxRequest mRequest = new InboxRequest(mContext);
        mRequest.getInboxServerRequest("Inbox", String.valueOf(pageNumber), String.valueOf(50),
                mFriendSearchET.getText().toString());
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                isProcessing = false;
                loadingFlag = false;
                if (success) {
                    mUserInboxModelAL = (List<InboxModel>) data;
                    mFilterAL = mUserInboxModelAL;
                    setInboxAdapter();
                }
                mSwipeRefreshWidget.setRefreshing(false);
            }
        });
    }

    private void updateMessageStatus(String MessageGUID) {
        isProcessing = true;
        InboxChangeMessageStatusRequest mRequest = new InboxChangeMessageStatusRequest(mContext);
        mRequest.getInboxChangeStatusServerRequest(MessageGUID, "", String.valueOf(Config.MOVE_MESSGAE_TO_TRASH), true);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                isProcessing = false;
                if (mUserInboxModelAL.size() > selectedPosition) {
                    mUserInboxModelAL.remove(selectedPosition);
                }
                mAdapter.setList(mFilterAL);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onRefresh() {
        pageNumber = 1;
        mUserInboxModelAL = new ArrayList<InboxModel>();
        getInboxRequest();
    }
}