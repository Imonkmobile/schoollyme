package com.schollyme;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.activity.SignupActivity;
import com.schollyme.adapter.SpinnerOptionsAdapter;
import com.schollyme.model.UserTypeModel;

public class SpinnerActivity extends Activity implements OnClickListener{

	private ListView mOptionsLV;
	private LinearLayout mContainerLl;
	private RelativeLayout mContainerRl;
	private TextView mDoneTV;
	private Context mContext;
	private String mSelectedText,mSelectedID;
	public static int selectedItemPosition = 2;
	private SpinnerOptionsAdapter mAdapter;
	private ArrayList<UserTypeModel> mAL;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.spinner_activity);
		mContext = this;
		mOptionsLV = (ListView) findViewById(R.id.options_lv);
		mContainerRl = (RelativeLayout) findViewById(R.id.confirmation_parent_rl);
		mContainerLl = (LinearLayout) findViewById(R.id.view_contaniner_layout_confirm_rl);
		mDoneTV = (TextView) findViewById(R.id.done_tv);
		mDoneTV.setOnClickListener(this);
		mAL = new ArrayList<UserTypeModel>();
		/*mAL.add(String.valueOf(UserType.Coach));
		mAL.add(String.valueOf(UserType.Athlete));
		mAL.add(String.valueOf(UserType.Fan));*/
		mAL = SignupActivity.mUserTypeAL;
				
		mAdapter = new SpinnerOptionsAdapter(SpinnerActivity.this);
		mAdapter.setList(mAL);
		mOptionsLV.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
		mOptionsLV.setSelection(2);
		startAnimation();
		mSelectedText =  mAL.get(2).mTypeName;
		mSelectedID =  mAL.get(2).mTypeId;
		mOptionsLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectedItemPosition = position;
				mAdapter.notifyDataSetChanged();
				mSelectedText = mAL.get(position).mTypeName;
				mSelectedID  = mAL.get(position).mTypeId;
			}
		});
	}


	public static Intent getIntent(Context context) {
		Intent intent1 = new Intent(context, SpinnerActivity.class);
		return intent1;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent();
		intent.putExtra("selectedOption", mAL.get(2).mTypeName);
		intent.putExtra("selectedID", mAL.get(2).mTypeId);
		intent.putExtra("reqestCode", 100);
		reverseAnimation(intent);
	}
	/**
	 * Animation from bottom to top
	 * */
	private void startAnimation() {
		// Cancels any animations for this container.
		mContainerLl.clearAnimation();
		mContainerRl.clearAnimation();

		Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up_dialog);
		mContainerLl.startAnimation(animation);

		AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
	}

	/**
	 * Animation from top to bottom
	 * */
	private void reverseAnimation(final Intent intent) {
		mContainerLl.clearAnimation();
		Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_down_dialog);
		mContainerLl.startAnimation(animation);
		mContainerLl.setVisibility(View.GONE);

		AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
		mContainerRl.setVisibility(View.GONE);

		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				setResult(RESULT_OK, intent);
				finish();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		});
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.done_tv:
			Intent intent = new Intent();
			intent.putExtra("selectedOption", mSelectedText);
			intent.putExtra("selectedID", mSelectedID);
			intent.putExtra("reqestCode", 100);
			reverseAnimation(intent);
			break;

		default:
			break;
		}
	}

}
