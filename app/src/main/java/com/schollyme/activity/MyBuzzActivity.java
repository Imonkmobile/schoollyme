package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.fragments.BuzzFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.schollyme.scores.MyScoresActivity;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
import java.util.List;

public class MyBuzzActivity extends BaseFragmentActivity {

    private static final String TAG = BuzzFragment.class.getSimpleName();

    private Context mContext;
    private ErrorLayout mErrorLayout;
    private String mUserGUID, mFriendsName;
    private LogedInUserModel mLogedinUser;
    private boolean isShareAllow = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_buzz);
        this.mContext = this;
        mUserGUID = getIntent().getStringExtra("UserGUID");
        mFriendsName = getIntent().getStringExtra("FriendsName")+"'s "+getString(R.string.Buzz);
        initHeader();
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        mLogedinUser = new LogedInUserModel(mContext);

       /* titleName = getString(R.string.My_Buzz);
        if (!mLogedinUser.mUserGUID.equals(mUserGUID)) {
            isShareAllow = true;
            titleName = mFriendsName+"'s "+getString(R.string.My_Buzz);
        }*/

        initFeedRequest();

    }

    public static Intent getIntent(Context mContext, String UserGUID, String mFriendsName) {
        Intent intent = new Intent(mContext, MyBuzzActivity.class);
        intent.putExtra("UserGUID", UserGUID);
        intent.putExtra("FriendsName", mFriendsName);
        return intent;
    }

    private void initHeader() {

        setHeader(R.drawable.icon_back, 0, mFriendsName, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        },null);

        /*mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu, titleName, 0);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashboardActivity) getActivity()).sliderListener();
            }
        }, null);*/
    }

    private View mFooterRL;
    private TextView mNoMoreTV;
    private TextView mNoFeedTV;
    private ProgressBar mLoaderBottomPB, mLoadingCenter;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private ListView mGnericLv;

    private PostFeedAdapter mPostFeedAdapter;
    private NFFilter mNFFilter;
    private List<NewsFeed> mNewsFeeds = new ArrayList<NewsFeed>();

    private NewsFeedRequest mNewsFeedRequest;

    private void initFeedRequest() {
        mNFFilter = new NFFilter(NewsFeedRequest.MODULE_ID_USERS, mUserGUID);
        mNFFilter.MyBuzz = true;
        mPostFeedAdapter = new PostFeedAdapter(mContext, new PostFeedAdapter.OnItemClickListenerPost() {

            @Override
            public void onClickItems(int ID, int position, NewsFeed newsFeed) {
                onItemClick(ID, position, newsFeed);
            }
        }, mUserGUID);
        mPostFeedAdapter.setWriteEnabled(false, false);

        mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);

        mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
        mNoMoreTV.setVisibility(View.INVISIBLE);

        mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
        mLoaderBottomPB.setVisibility(View.INVISIBLE);
        mLoaderBottomPB.getIndeterminateDrawable().setColorFilter(getResources()
                .getColor(R.color.header_bg_color), PorterDuff.Mode.SRC_IN);

        mNoFeedTV = (TextView) findViewById(R.id.no_feed_tv);
        FontLoader.setRobotoRegularTypeface(mNoMoreTV, mNoFeedTV);

        mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.pull_to_refresh_srl);
        mSwipeRefreshWidget.setColorScheme(R.color.header_bg_color);
        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mSwipeRefreshWidget.setRefreshing(true);
                getFeedRequest(mNFFilter);
            }
        });

        mGnericLv = (ListView) findViewById(R.id.genric_lv);
        mGnericLv.addFooterView(mFooterRL);
        mGnericLv.setAdapter(mPostFeedAdapter);
        mGnericLv.setOnScrollListener(mOnScrollListener);

        getFeedRequest(mNFFilter);
    }

    private boolean mFetchedAll = true;
    private AbsListView.OnScrollListener mOnScrollListener = new AbsListView.OnScrollListener() {
        private boolean bReachedListEnd;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (Config.DEBUG) {
                Log.d(TAG, "onScrollStateChanged bReachedListEnd=" + bReachedListEnd
                        + ", mFetchedAll=" + mFetchedAll + ", scrollState="
                        + scrollState);
            }
            if (bReachedListEnd && !mFetchedAll) {
                mFetchedAll = true;
                mNFFilter.PageNo++;

                getFeedRequest(mNFFilter);
            }
        }

        @Override
        public void onScroll(AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
            bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);
            if (Config.DEBUG) {
                Log.d(TAG, "onScroll bReachedListEnd=" + bReachedListEnd + ", firstVisibleItem="
                        + firstVisibleItem + ", visibleItemCount="
                        + visibleItemCount + ", totalItemCount=" + totalItemCount);
            }
        }
    };


    private void getFeedRequest(final NFFilter nffilter) {
        if (null == mNewsFeedRequest) {
            mNewsFeedRequest = new NewsFeedRequest(mContext);
        }
        mNewsFeedRequest.getNewsFeedListInServer(nffilter);
        mNoMoreTV.setVisibility(View.INVISIBLE);
        if (nffilter.PageNo == 1) {
            if (mPostFeedAdapter.getCount() <= 1 || mPostFeedAdapter == null) {
                mNewsFeedRequest.setLoader(mLoadingCenter);
            } else {
                mNewsFeedRequest.setLoader(null);
            }

            mLoaderBottomPB.setVisibility(View.INVISIBLE);
        } else {
            mNewsFeedRequest.setLoader(mLoaderBottomPB);
            mLoaderBottomPB.setVisibility(View.VISIBLE);
        }

        mNewsFeedRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mSwipeRefreshWidget.setRefreshing(false);
                if (success) {
                    if (null != data) {
                        if (mNFFilter.PageNo == 1) {
                            resetList();
                        }
                        List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
                        mFetchedAll = (newsFeeds.size() < Config.PAGE_SIZE);
                        mNewsFeeds.addAll(newsFeeds);
                        //	mPostFeedAdapter.setList(mNewsFeeds, false);
                        if (isShareAllow) {
                            mPostFeedAdapter.setList(mNewsFeeds, true);
                        } else {
                            mPostFeedAdapter.setList(mNewsFeeds, false);
                        }

                        if (mNewsFeeds.size() < 1) {
                            mNoMoreTV.setVisibility(View.INVISIBLE);
                            mNoFeedTV.setVisibility(View.VISIBLE);
                        } else if (mFetchedAll && mNFFilter.PageNo > 1) {
                            mNoMoreTV.setVisibility(View.VISIBLE);
                        } else {
                            mNoMoreTV.setVisibility(View.INVISIBLE);
                        }
                    }
                } else {
                    mFetchedAll = false;
                    DialogUtil.showOkDialog(mContext, (String) data, "");
                }
            }
        });
    }

    private void resetList() {
        mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
        mNewsFeeds.clear();
        // mNewsFeeds.add(new NewsFeed(null));
        //	mPostFeedAdapter.setList(mNewsFeeds, false);
        if (isShareAllow) {
            mPostFeedAdapter.setList(mNewsFeeds, true);
        } else {
            mPostFeedAdapter.setList(mNewsFeeds, false);
        }
    }

    int ClickLocation = 0;

    public void onItemClick(int ID, int position, NewsFeed newsFeed) {
        ClickLocation = position;
        switch (ID) {
            case R.id.share_tv:
                if (newsFeed.ShareAllowed == 1) {
                    sharePost(newsFeed);
                } else {
                    Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.like_tv:
                likeMediaToggleService(newsFeed, position);
                break;
            case R.id.likes_tv:
                if (newsFeed.NoOfLikes != 0) {
                    startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID, "ACTIVITY", "WALL"),
                            LikeListActivity.REQ_FROM_HOME_LIKELIST);
                    this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
                }
                break;

            case R.id.comment_tv: {
                AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
                WriteCommentActivity.addFullPathIfNeeded(newsFeed, albumMedia);
                String captionForWriteCommentScreen = "";
                if (!newsFeed.PostContent.equals("")) {
                    captionForWriteCommentScreen = newsFeed.PostContent;
                } else {

                    if (!newsFeed.Album.AlbumMedias.get(0).ImageName.equals("")) {

                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).Caption;
                    } else {
                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
                    }

                }

                startActivityForResult(WriteCommentActivity.getIntent(this, albumMedia,
                        "ACTIVITY", captionForWriteCommentScreen),
                        WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY);
                this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
            }
            break;
            case R.id.comments_tv:

                AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
                WriteCommentActivity.addFullPathIfNeeded(newsFeed, albumMedia);
                String captionForWriteCommentScreen = "";
                if (!newsFeed.PostContent.equals("")) {
                    captionForWriteCommentScreen = newsFeed.PostContent;
                } else {
                    if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
                    }
                }

                startActivityForResult(CommentListActivity.getIntent(mContext, albumMedia,
                        "WALLPOST", captionForWriteCommentScreen),
                        CommentListActivity.REQ_CODE_COMMENT_LIST);
                this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

                break;

            case PostFeedAdapter.ConvertViewID:
                startActivityForResult(CreateWallPostActivity.getIntent(mContext,
                        WallPostCreateRequest.MODULE_ID_USERS, mUserGUID),
                        CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
                break;
            default:
                break;
        }
    }

    public void sharePost(NewsFeed newsFeedModel) {

        SharePostRequest mSharePostRequest = new SharePostRequest(mContext);
        mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
                NewsFeedRequest.MODULE_ID_USERS, mUserGUID, 1, 1);
        mSharePostRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, ErrorLayout.MsgType.Success);
                } else {
                    mErrorLayout.showError((String) data, true, ErrorLayout.MsgType.Error);
                }
            }
        });
    }

    public void likeMediaToggleService(final NewsFeed newsFeed, final int position) {

        ToggleLikeRequest mToggleLikeRequest = new ToggleLikeRequest(mContext);
        mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");
        mToggleLikeRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    if (newsFeed.IsLike == 0) {

                        newsFeed.IsLike = 1;
                        newsFeed.NoOfLikes++;
                    } else {

                        newsFeed.IsLike = 0;
                        newsFeed.NoOfLikes--;
                    }
                    mPostFeedAdapter.notifyDataSetChanged();

                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), "");
                }

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mNewsFeedRequest.setActivityStatus(true);

        switch (requestCode) {
            case LikeListActivity.REQ_FROM_HOME_LIKELIST:
                if (resultCode == this.RESULT_OK) {
                    if (data != null) {
                        mNewsFeeds.get(ClickLocation).NoOfLikes = data.getIntExtra("COUNT", 0);
                        mNewsFeeds.get(ClickLocation).IsLike = data.getIntExtra("BUTTONSTATUS", 0);
                        if (isShareAllow) {
                            mPostFeedAdapter.setList(mNewsFeeds, true);
                        } else {
                            mPostFeedAdapter.setList(mNewsFeeds, false);
                        }

                    }
                }
                break;
            case CommentListActivity.REQ_CODE_COMMENT_LIST:
                if (resultCode == this.RESULT_OK) {
                    if (data != null) {
                        mNewsFeeds.get(ClickLocation).NoOfComments = data.getIntExtra("COUNT", 0);
                        //	mPostFeedAdapter.setList(mNewsFeeds, false);
                        if (isShareAllow) {
                            mPostFeedAdapter.setList(mNewsFeeds, true);
                        } else {
                            mPostFeedAdapter.setList(mNewsFeeds, false);
                        }
                    }
                }
                break;

            case CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY:
                if (resultCode == this.RESULT_OK) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mNFFilter.PageNo = 1;
                            getFeedRequest(mNFFilter);
                        }
                    }, 500);
                }
                break;

            case WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY:
                if (resultCode == this.RESULT_OK) {
                    mNewsFeeds.get(ClickLocation).NoOfComments++;
                    //mPostFeedAdapter.setList(mNewsFeeds, false);
                    if (isShareAllow) {
                        mPostFeedAdapter.setList(mNewsFeeds, true);
                    } else {
                        mPostFeedAdapter.setList(mNewsFeeds, false);
                    }
                }

                break;

            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
