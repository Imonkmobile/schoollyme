package com.schollyme.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.schollyme.GCMBaseActivity;
import com.schollyme.R;
import com.schollyme.TNCActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.UserTypeModel;
import com.vinfotech.request.GetUsersTypeRequest;
import com.vinfotech.request.SignUpRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.Config.UserType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.ValidationUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Activity class. This may be useful in Signup implementation
 *
 * @author Ravi Bhandari
 */
public class SignupActivity extends GCMBaseActivity implements OnClickListener, GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "Signup";
    private SignupViewHolder mSignupViewHolder;
    private Context mContext;
    protected int mMsgResId = 0;
    static final int DATE_PICKER_ID = 1111;
    private int year, month, day;
    private Calendar mCalender;
    private boolean isDOBSelected = false;
    private String emailString, passwordString, cPasswordString, userNameString, profileType, dob;
    private double mCurLat = 0.0, mCurLog = 0.0;
    public static ArrayList<UserTypeModel> mUserTypeAL;
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private FusedLocationProviderApi fusedLocationProviderApi;

    private String mUserType = "3";
    private ErrorLayout mErrorLayout;
    private boolean Facebook = false;
    private String Username = "", Email = "";
    private String UserSocialID = "";
    private LoginButton mFB_Iv;
    private TextView facebook_Iv;
    private CallbackManager callbackManager;
    private String mEmailStr = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        mContext = SignupActivity.this;
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        initHeader();

        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        mSignupViewHolder = new SignupViewHolder(findViewById(R.id.main_rl), this);
        profileType = String.valueOf(UserType.Fan);

        Utility.textSpann(mContext, (TextView) findViewById(R.id.tnc_tv),
                getResources().getString(R.string.tnc_text));

        initDatePicker();
        getLocation();
        getUserTypeServerRequest();

        facebook_Iv = (TextView) findViewById(R.id.facebook_Iv);
        facebook_Iv.setOnClickListener(this);

        mFB_Iv = (LoginButton) findViewById(R.id.fb_login_button);
        mFB_Iv.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        mFB_Iv.registerCallback(callbackManager, mFacebookCallback);

        viewListnres();
    }

    private void initHeader() {

        setHeader(findViewById(R.id.header_layout), R.drawable.selector_cancel, R.drawable.selector_confirm,
                getResources().getString(R.string.signup_label), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utility.hideSoftKeyboard(mSignupViewHolder.mEmailET);
                        finish();
                        overridePendingTransition(0, R.anim.slide_down_dialog);
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        if (Facebook) {
//                            if (validateSignUpFacebook()) {
//                                signUpServerRequestFacebook(UserSocialID);
//                            }
//                        } else {
                        if (validateSignUp()) {
                            signUpServerRequest();
                        }
                        //}
                    }
                });

    }

    @SuppressWarnings("ResourceAsColor")
    private void viewListnres() {

        mSignupViewHolder.mUserNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSignupViewHolder.mUserNameTV.setTextColor(Color.parseColor("#01579B"));
                    mSignupViewHolder.mUserNameET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSignupViewHolder.mUserNameTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSignupViewHolder.mUserNameET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mSignupViewHolder.mEmailET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSignupViewHolder.mEmailTV.setTextColor(Color.parseColor("#01579B"));
                    mSignupViewHolder.mEmailET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSignupViewHolder.mEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSignupViewHolder.mEmailET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mSignupViewHolder.mPasswordET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSignupViewHolder.mPasswordTV.setTextColor(Color.parseColor("#01579B"));
                    mSignupViewHolder.mPasswordET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSignupViewHolder.mPasswordTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSignupViewHolder.mPasswordET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mSignupViewHolder.mCPasswordET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSignupViewHolder.mCPasswordTV.setTextColor(Color.parseColor("#01579B"));
                    mSignupViewHolder.mCPasswordET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSignupViewHolder.mCPasswordTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSignupViewHolder.mCPasswordET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnView() {

        mSignupViewHolder.mProfileTypeTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSignupViewHolder.mProfileTypeET.setTextColor(Color.parseColor("#666666"));
        mSignupViewHolder.mProfileTypeET.setBackgroundResource(R.drawable.ic_gray_line);
    }

    private void viewChangeDefaultColor() {

        mSignupViewHolder.mProfileTypeTV.setTextColor(Color.parseColor("#01579B"));
        mSignupViewHolder.mProfileTypeET.setTextColor(Color.parseColor("#000000"));
        mSignupViewHolder.mProfileTypeET.setBackgroundResource(R.drawable.ic_blue_line);
        mSignupViewHolder.mUserNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSignupViewHolder.mUserNameET.setTextColor(Color.parseColor("#666666"));
        mSignupViewHolder.mUserNameET.setBackgroundResource(R.drawable.ic_gray_line);
        mSignupViewHolder.mEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSignupViewHolder.mEmailET.setTextColor(Color.parseColor("#666666"));
        mSignupViewHolder.mEmailET.setBackgroundResource(R.drawable.ic_gray_line);
        mSignupViewHolder.mPasswordTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSignupViewHolder.mPasswordET.setTextColor(Color.parseColor("#666666"));
        mSignupViewHolder.mPasswordET.setBackgroundResource(R.drawable.ic_gray_line);
        mSignupViewHolder.mCPasswordTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSignupViewHolder.mCPasswordET.setTextColor(Color.parseColor("#666666"));
        mSignupViewHolder.mCPasswordET.setBackgroundResource(R.drawable.ic_gray_line);
    }

    FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            String accessToken = loginResult.getAccessToken().getToken();
            Log.i("accessToken", accessToken);

            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.i("Signup ", response.toString());
                            // Get facebook data from login
                            Bundle bFacebookData = getFacebookData(object);
                            if (bFacebookData != null) {
                                Facebook = true;
                                //String SocialType = "Facebook API";

                                String UserSocialID = bFacebookData.getString("idFacebook");
                                String Username = bFacebookData.getString("first_name") + " "
                                        + bFacebookData.getString("last_name");

                                mEmailStr = bFacebookData.getString("email");
                                if (mEmailStr != null && mEmailStr.length()>0) {
                                    emailString = mEmailStr;
                                }

                                if (Username.length()>0) {
                                    userNameString = Username;
                                }

                                if (Facebook) {
                                    if (validateSignUpFacebook()) {
                                        signUpServerRequestFacebook(UserSocialID);
                                    }
                                }
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
            // Parámetros que pedimos a facebook
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            // Toast.makeText(LoginActivity.this, "Login cancelled by user!", Toast.LENGTH_LONG).show();
            System.out.println("Facebook Login failed!!");

        }

        @Override
        public void onError(FacebookException e) {
            // Toast.makeText(LoginActivity.this, "Login unsuccessful!", Toast.LENGTH_LONG).show();
            System.out.println("Facebook Login failed!!");
        }
    };

    public static Intent getIntent(Context context) {
        Intent intent1 = new Intent(context, SignupActivity.class);
        return intent1;
    }

    public static Intent getIntentFacebook(Context context, String Username, String Email, String UserSocialID) {
        Intent intent1 = new Intent(context, SignupActivity.class);
        intent1.putExtra("Username", Username);
        intent1.putExtra("Email", Email);
        intent1.putExtra("UserSocialID", UserSocialID);
        intent1.putExtra("Facebook", true);
        return intent1;
    }

    /**
     * validate signup parameters
     */
    private boolean validateSignUp() {

        boolean isValidated = false;

        emailString = mSignupViewHolder.mEmailET.getText().toString().trim();
        passwordString = mSignupViewHolder.mPasswordET.getText().toString().trim();
        cPasswordString = mSignupViewHolder.mCPasswordET.getText().toString().trim();
        userNameString = mSignupViewHolder.mUserNameET.getText().toString().trim();
        profileType = mSignupViewHolder.mProfileTypeET.getText().toString().trim();

        DecimalFormat mFormat = new DecimalFormat("00");
        int cMonth = month + 1;

        String mMonth = mFormat.format(Double.valueOf(cMonth));
        String mDay = mFormat.format(Double.valueOf(day));

        dob = new StringBuilder().append(year).append("-").append(mMonth).append("-")
                .append(mDay).toString();//mSignupViewHolder.mDobTV.getText().toString();
        if (TextUtils.isEmpty(userNameString)) {
            mErrorLayout.showError(getResources().getString(R.string.user_fullname_require), true, MsgType.Error);
        } else if (TextUtils.isEmpty(emailString)) {
            mErrorLayout.showError(getResources().getString(R.string.email_require_message), true, MsgType.Error);
        } else if (!ValidationUtil.isValidEmail(emailString)) {
            mErrorLayout.showError(getResources().getString(R.string.email_notvalid_message), true, MsgType.Error);
        } else if (TextUtils.isEmpty(passwordString)) {
            mErrorLayout.showError(getResources().getString(R.string.password_require_message), true, MsgType.Error);
        } else if (passwordString.length() < 5) {
            mErrorLayout.showError(getResources().getString(R.string.password_length_message), true, MsgType.Error);
        } else if (TextUtils.isEmpty(cPasswordString)) {
            mErrorLayout.showError(getResources().getString(R.string.cpassword_require_message), true, MsgType.Error);
        } else if (!passwordString.equals(cPasswordString)) {
            mErrorLayout.showError(getResources().getString(R.string.password_not_match_message), true, MsgType.Error);
        } else if (TextUtils.isEmpty(profileType)) {
            mErrorLayout.showError(getResources().getString(R.string.profile_type_is_require), true, MsgType.Error);
        } /*else if (!isDOBSelected) {
            dob = "";
			return true;
			//mErrorLayout.showError(getResources().getString(R.string.dob_require), true, MsgType.Error);
		} else if (isDOBSelected && Utility.getDateDifference(dob) < 13) {
			mErrorLayout.showError(getResources().getString(R.string.age_restriction_error), true, MsgType.Error);
		}*/ else {
            isValidated = true;
        }
        return isValidated;
    }


    private boolean validateSignUpFacebook() {
        boolean isValidated = false;

        emailString = mSignupViewHolder.mEmailET.getText().toString().trim();
        userNameString = mSignupViewHolder.mUserNameET.getText().toString().trim();
        profileType = mSignupViewHolder.mProfileTypeET.getText().toString().trim();

        DecimalFormat mFormat = new DecimalFormat("00");
        int cMonth = month + 1;

        String mMonth = mFormat.format(Double.valueOf(cMonth));
        String mDay = mFormat.format(Double.valueOf(day));

        dob = new StringBuilder().append(year).append("-").append(mMonth).append("-").append(mDay).toString();
        if (TextUtils.isEmpty(userNameString)) {
            mErrorLayout.showError(getResources().getString(R.string.user_fullname_require), true, MsgType.Error);
        } else if (TextUtils.isEmpty(emailString)) {
            mErrorLayout.showError(getResources().getString(R.string.email_require_message), true, MsgType.Error);
        } else if (!ValidationUtil.isValidEmail(emailString)) {
            mErrorLayout.showError(getResources().getString(R.string.email_notvalid_message), true, MsgType.Error);
        } else if (TextUtils.isEmpty(profileType)) {
            mErrorLayout.showError(getResources().getString(R.string.profile_type_is_require), true, MsgType.Error);
        } else {
            isValidated = true;
        }
        return isValidated;
    }

    /**
     * Listeners
     */
    @SuppressWarnings("ResourceAsColor")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tnc_tv:
                startActivity(TNCActivity.getIntent(mContext));
                break;
            case R.id.profile_type_et:
                viewChangeDefaultColor();
                if (HttpConnector.getConnectivityStatus(mContext) == 0) {
                    DialogUtil.showOkDialog(mContext, Utility.FromResouarceToString(mContext,
                            R.string.No_internet_connection),
                            Utility.FromResouarceToString(mContext, R.string.Network_erro));
                } else {
                    if (null != mUserTypeAL && mUserTypeAL.size() > 0) {
                        displayProfileType();
                    } else {
                        getUserTypeServerRequest();
                    }
                }

                break;
            case R.id.dob_tv:
                displayDatePicker();
                break;
            case R.id.facebook_Iv:
                LoginManager.getInstance().logOut();
                if (validateSignUpFacebook()) {
                    mFB_Iv.performClick();
                }

                break;
        }
    }

    /**
     * It will display user profile type
     * either Athlete/Coach/Fan
     */
    private void displayProfileType() {

        DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {
                        mUserType = mUserTypeAL.get(position).mTypeId;
                        mSignupViewHolder.mProfileTypeET.setText(mUserTypeAL.get(position).mTypeName);
                    }

                    @Override
                    public void onCancel() {

                    }
                }, mContext.getString(R.string.user_type_coach), mContext.getString(R.string.user_type_athlete),
                mContext.getString(R.string.user_type_fan));
    }

    private void initDatePicker() {
        mCalender = Calendar.getInstance();
        year = mCalender.get(Calendar.YEAR);
        month = mCalender.get(Calendar.MONTH);
        day = mCalender.get(Calendar.DAY_OF_MONTH);
    }


    /**
     * Display date picker dialog for pick DOB from user
     */
    private void displayDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(mContext, pickerListener, year, month, day);
        dialog.getDatePicker().setMaxDate(mCalender.getTimeInMillis());
        dialog.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        dialog.show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }


    /**
     * Listener for date picker invoke when user select any date from picker dialog
     */
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            isDOBSelected = true;
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            DecimalFormat mFormat = new DecimalFormat("00");
            int cMonth = month + 1;
            String cDay = mFormat.format(Double.valueOf(day));

            String mMonth = mFormat.format(Double.valueOf(cMonth));
            // Month is 0 based, just add 1
            mSignupViewHolder.mDobTV.setText(new StringBuilder().append(cDay).append(" ")
                    .append(Utility.getMOnthNameFromNumber(month)).append(",")
                    .append(" ").append(year));
        }
    };

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.slide_down_dialog);
    }

    /**
     * init location provide for get user location
     */

    private void getLocation() {
        try {
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
            fusedLocationProviderApi = LocationServices.FusedLocationApi;
            googleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(this).build();
            if (googleApiClient != null) {
                googleApiClient.connect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Location listener invoke when device connected to location provider
     */
    @Override
    public void onConnected(Bundle arg0) {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = fusedLocationProviderApi.getLastLocation(googleApiClient);
            if (location != null) {
                Log.e(TAG, "onLocationChanged: " + location);
                mCurLat = (double) (location.getLatitude());
                mCurLog = (double) (location.getLongitude());
                googleApiClient.disconnect();
            } else {
                getLocation();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {

    }

    public class SignupViewHolder {

        public EditText mEmailET, mPasswordET, mCPasswordET, mUserNameET, mProfileTypeET;
        public TextView mUserNameTV, mEmailTV, mPasswordTV, mCPasswordTV, mProfileTypeTV, tncTV, mDobTV, mErrorMessageTV;

        public SignupViewHolder(View view, OnClickListener listener) {

            mUserNameET = (EditText) view.findViewById(R.id.user_name_et);
            mUserNameTV = (TextView) view.findViewById(R.id.user_name_tv);

            mEmailTV = (TextView) view.findViewById(R.id.email_tv);
            mEmailET = (EditText) view.findViewById(R.id.email_et);

            mPasswordET = (EditText) view.findViewById(R.id.password_et);
            mPasswordTV = (TextView) view.findViewById(R.id.password_tv);

            mCPasswordET = (EditText) view.findViewById(R.id.confirm_password_et);
            mCPasswordTV = (TextView) view.findViewById(R.id.confirm_password_tv);

            mProfileTypeET = (EditText) view.findViewById(R.id.profile_type_et);
            mProfileTypeTV = (TextView) view.findViewById(R.id.profile_type_tv);

            tncTV = (TextView) view.findViewById(R.id.tnc_tv);
            mDobTV = (TextView) view.findViewById(R.id.dob_tv);
            mErrorMessageTV = (TextView) view.findViewById(R.id.error_message_tv);

            mDobTV.setOnClickListener(listener);
            mProfileTypeET.setOnClickListener(listener);
            tncTV.setOnClickListener(listener);
            mUserNameET.requestFocus();

            Facebook = getIntent().getBooleanExtra("Facebook", false);
            if (Facebook) {
                Username = getIntent().getStringExtra("Username");
                Email = getIntent().getStringExtra("Email");
                mUserNameET.setText(Username);
                mEmailET.setText(Email);
                mPasswordTV.setVisibility(View.GONE);
                mPasswordET.setVisibility(View.GONE);
                mCPasswordTV.setVisibility(View.GONE);
                mCPasswordET.setVisibility(View.GONE);
                UserSocialID = getIntent().getStringExtra("UserSocialID");

            } else {
                mPasswordTV.setVisibility(View.VISIBLE);
                mPasswordET.setVisibility(View.VISIBLE);
                mCPasswordTV.setVisibility(View.VISIBLE);
                mCPasswordET.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Server Request Response
     */
    private void signUpServerRequest() {
        if (!isDOBSelected) {
            dob = "";
        }
        SignUpRequest mSignUpRequest = new SignUpRequest(mContext);
        mSignUpRequest.signUpSever(userNameString, emailString, passwordString, dob, mUserType,
                Config.DEVICE_TYPE, mDeviceToken, String.valueOf(mCurLat), String.valueOf(mCurLog),
                Utility.getIPAddress(true), "", "");
        mSignUpRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {

                    DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {

                                    Intent intent = VerifyAccountActivity.getIntent(mContext);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            });

                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), getResources().getString(R.string.app_name));

                }
            }
        });
    }

    private void signUpServerRequestFacebook(String userSocialID) {
        if (!isDOBSelected) {
            dob = "";
        }
        SignUpFacebookRequest mSignUpRequest = new SignUpFacebookRequest(mContext);
        mSignUpRequest.SignupFacebookRequestServer(userNameString, emailString, dob, mUserType, Config.DEVICE_TYPE, mDeviceToken,
                String.valueOf(mCurLat), String.valueOf(mCurLog), Utility.getIPAddress(true), "", "", userSocialID);
        mSignUpRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, final Object data, int totalRecords) {

                if (success) {

                    new LogedInUserModel(mContext).updatePreference(mContext, "mLoginStatus", true);
                    BaseRequest.setLoginSessionKey(new LogedInUserModel(mContext).mLoginSessionKey);
                    Intent intent = SetupProfileMandatoryActivity.getIntent(mContext, "1");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
//                    DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
//                            new OnOkButtonListner() {
//                                @Override
//                                public void onOkBUtton() {
//
//
//
//                                    Intent intent = SetupProfileMandatoryActivity.getIntent(mContext, "2");
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
//                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    startActivity(intent);
//                                    finish();
//
//                                }
//                            });

                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), getResources().getString(R.string.app_name));

                }
            }
        });
    }

    /**
     * Get type of user list from server
     */
    private void getUserTypeServerRequest() {

        GetUsersTypeRequest mRequest = new GetUsersTypeRequest(mContext);
        mRequest.getUsersTypeServerRequest();
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mUserTypeAL = (ArrayList<UserTypeModel>) data;
                } else {
                    DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                            getResources().getString(R.string.app_name), new OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {
                                    finish();
                                }
                            });
                }
            }
        });
    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));


            return bundle;
        } catch (JSONException e) {
            Log.d(TAG, "Error parsing JSON");
        }
        return null;
    }

    private void SignupViaFacebook(String username, String mEmailStr, String dob, String socialType,
                                   String deviceType, String deviceid, String mCurLat, String mCurLog,
                                   String ipAddress, String resolution, String mDeviceToken, String userSocialID) {

        SignUpFacebookRequest signupRequest = new SignUpFacebookRequest(this);// Utility.md5(mPassStr)
        signupRequest.SignupFacebookRequestServer(username, mEmailStr, dob, socialType, deviceType,
                deviceid, mCurLat, mCurLog, ipAddress, resolution, mDeviceToken, userSocialID);
        signupRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {

//                    SignUpModel model = (SignUpModel) data;
//                    model.persist(mContext);
                    Intent intent = DashboardActivity.getIntent(mContext, 2, "");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                } else {
                    String message = data.toString();
                    if (TextUtils.isEmpty(message)) {
                        DialogUtil.showOkDialog(mContext, getResources().getString(
                                R.string.login_server_error_message),
                                getResources().getString(R.string.app_name));
                    } else {
                        DialogUtil.showOkDialog(mContext, message,
                                getResources().getString(R.string.app_name));
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }
}