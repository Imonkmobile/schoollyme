package com.schollyme.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.luminous.pick.LuminousGallery.LuminousAction;
import com.luminous.pick.LuminousGalleryActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.ErrorLayoutListener;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.media.MediaViewerActivity;
import com.schollyme.media.UploadYoutubeActivity;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewMedia;
import com.schollyme.model.NewYoutubeUrl;
import com.schollyme.model.UploadingMedia;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.MarshMallowPermission;
import com.vinfotech.utility.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.luminous.pick.LuminousGalleryAdapter.MAX_MEDIA_SEL_COUNT;

public class CreateWallPostActivity extends BaseActivity implements OnClickListener {

    private static final String TAG = CreateWallPostActivity.class.getSimpleName();
    public static final int REQ_CODE_CREATE_WALL_POST_ACTIVITY = 1031;
    public static final int REQ_CODE_CREATE_WALL_POST_ACTIVITY_USER_PROFILE = 1032;
    private TextView mPrivacyTv, mMarkAsBuzzTv, mMarkAsKiltTv;
    private EditText mPostEt;
    protected static final int REQ_CODE_TAKE_FROM_CAMERA = 500;
    protected static final int REQ_CODE_CROP_PHOTO = 502;
    private final int mOutputX = 960, mAspectX = 1;
    private int mOutputY = 960, mAspectY = 1;
    private static final String IMAGE_UNSPECIFIED = "image/*";
    protected Bitmap capturedImageBitmap = null;

    private MediaUploadRequest mMediaUploadRequest;
    private WallPostCreateRequest mWallPostCreateRequest;
    private HeaderLayout mHeaderLayout;
    private ErrorLayout mErrorLayout;
    private LogedInUserModel mLogedInUserModel;
    protected Uri capturedImageUri, cropImageUri;

    private int mModuleID;
    private int mVisibility = WallPostCreateRequest.VISIBILITY_PUBLIC;
    private int mCommentable = WallPostCreateRequest.COMMENT_ON;
    private String mPostContent;
    private ArrayList<NewYoutubeUrl> mNewYoutubeUrls;
    private boolean mIsImage = false;
    private String mUserGUID;
    private ImageView mPrivacyIV;
    private boolean isTeamPage = false, mMarkAsBuzz = false, mMarkAsKilt = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_wall_post_activity);

        mModuleID = getIntent().getIntExtra("moduleID", -1);
        mUserGUID = getIntent().getStringExtra("userGUID");
        isTeamPage = getIntent().getBooleanExtra("isTeamPage", false);

        boolean enableMyBuzz = getIntent().getBooleanExtra("enableMyBuzz", false);
        if (Config.DEBUG) {
            Log.d("CreateWallPostActivity", "onCreate mModuleID=" + mModuleID + ", enableMyBuzz=" + enableMyBuzz);
        }
        if (mModuleID < 1) {
            Toast.makeText(this, "Invalid module id.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mPrivacyIV = (ImageView) findViewById(R.id.privacy_iv);
        mHeaderLayout = new HeaderLayout(findViewById(R.id.container_ll));
        mHeaderLayout.setHeaderValues(R.drawable.ic_close, getResources().getString(R.string.Post), R.drawable.ic_send_msgs);
        mHeaderLayout.setListenerItI(this, this);
        mHeaderLayout.enableRightButton(false);
        mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));
        mLogedInUserModel = new LogedInUserModel(this);
        mWallPostCreateRequest = new WallPostCreateRequest(this);
        mWallPostCreateRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, final Object data, int totalRecords) {
                dismissProgressDialog();
                if (success) {
                    mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {
                        @Override
                        public void onErrorShown() {
                        }

                        @Override
                        public void onErrorHidden() {
                            setResultAndFinish(data);
                        }
                    });
                    mErrorLayout.showError(getString(R.string.Posted_successfully), true, MsgType.Success);
                } else {
                    mHeaderLayout.mRightIb1.setEnabled(true);
                    mErrorLayout.showError((String) data, true, MsgType.Error);
                }
            }
        });

        mMediaUploadRequest = new MediaUploadRequest(this, true);
        mMediaUploadRequest.setRequestListener(mRequestListener);

        mPrivacyTv = (TextView) findViewById(R.id.privacy_tv);
        mPrivacyTv.setOnClickListener(this);
        mMarkAsBuzzTv = (TextView) findViewById(R.id.mark_as_buzz_tv);
        mMarkAsKiltTv = (TextView) findViewById(R.id.mark_as_kilt_tv);
        mMarkAsBuzzTv.setVisibility(enableMyBuzz && mLogedInUserModel.isAthlete() ? View.VISIBLE : View.GONE);
        mMarkAsBuzzTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                updateMarkAsBuzz(!mMarkAsBuzz);
            }
        });

        mMarkAsKiltTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                updateMarkAsKilt(!mMarkAsKilt);
            }
        });
        findViewById(R.id.camera_ll).setOnClickListener(this);
        findViewById(R.id.photo_ll).setOnClickListener(this);
        findViewById(R.id.video_ll).setOnClickListener(this);

        ImageLoaderUniversal.ImageLoadRound(this, Config.IMAGE_URL_PROFILE + mLogedInUserModel.mUserPicURL,
                (ImageView) findViewById(R.id.user_iv), ImageLoaderUniversal.option_Round_Image);
        mPostEt = (EditText) findViewById(R.id.post_et);
        mPostEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (mUploadingMedias.size() == 0 && mPostEt.getText().toString().trim().equals("")) {
                    /*if(null!=mNewYoutubeUrls &&  mNewYoutubeUrls.size() == 0){
                        mHeaderLayout.enableRightButton(false);
					}*/

                } else {
                    mHeaderLayout.enableRightButton(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        updateData();

        String mLoggedinUserId = new LogedInUserModel(this).mUserGUID;
        if (!mLoggedinUserId.equals(mUserGUID)) {
            mPrivacyTv.setClickable(false);
            mPrivacyTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        if (isTeamPage) {
            mPrivacyTv.setText(R.string.Teammates);
            mPrivacyTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock_white, 0,
                    R.drawable.ic_arrow_down, 0);
        } else {
            mPrivacyTv.setText(R.string.Public);
            mPrivacyTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_date_time_white, 0,
                    R.drawable.ic_arrow_down, 0);
        }
        updateMarkAsBuzz(mMarkAsBuzz);
        updateMarkAsKilt(mMarkAsKilt);

        MarshMallowPermission marshMallowPermission = new MarshMallowPermission(this);
        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        }
    }

    public static Intent getIntent(Context context, int moduleID, String userGUID) {
        Intent intent = new Intent(context, CreateWallPostActivity.class);
        intent.putExtra("moduleID", moduleID);
        intent.putExtra("userGUID", userGUID);
        return intent;
    }

    public static Intent getIntent(Context context, int moduleID, String userGUID, boolean enableMyBuzz) {
        Intent intent = new Intent(context, CreateWallPostActivity.class);
        intent.putExtra("moduleID", moduleID);
        intent.putExtra("userGUID", userGUID);
        intent.putExtra("enableMyBuzz", enableMyBuzz);
        return intent;
    }

    public static Intent getIntent(Context context, int moduleID, boolean isTeamPage, String userGUID) {
        Intent intent = new Intent(context, CreateWallPostActivity.class);
        intent.putExtra("moduleID", moduleID);
        intent.putExtra("userGUID", userGUID);
        intent.putExtra("isTeamPage", isTeamPage);
        return intent;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_button:

                discardMediaDialog();

                break;
            case R.id.right_button:

                if (isValid()) {
                    try {
                        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(mPostEt.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mHeaderLayout.mRightIb1.setEnabled(false);
                    if (mUploadingMedias.size() > 0 && (null == mNewYoutubeUrls || mNewYoutubeUrls.size() == 0)) {
                        if (mIsImage) {
                            uploadMedia();
                        } else if (validateVidSizeWithDialogs(CreateWallPostActivity.this,
                                mUploadingMedias.get(0).path, mErrorLayout)) {
                            if (mMarkAsKilt) {
                                if (validateVidSizefifteenSecondDialogs(CreateWallPostActivity.this,
                                        mUploadingMedias.get(0).path, mErrorLayout)) {
                                    uploadMedia();
                                } else {
                                    /*mUploadingMedias.clear();
                                    initMultiPhotoUploadUI();*/
                                    mHeaderLayout.mRightIb1.setEnabled(true);
                                }
                            } else {
                                uploadMedia();
                            }
                        }
                    } else {
                        if (null == mProgressDialog) {
                            mProgressDialog = DialogUtil.createDiloag(CreateWallPostActivity.this, getString(R.string.Posting));
                        } else {
                            mProgressDialog.setMessage(getString(R.string.Posting));
                        }
                        if (!mProgressDialog.isShowing()) {
                            mProgressDialog.show();
                        }
                        mWallPostCreateRequest.createWallPostInServer(mPostContent, mModuleID, mUserGUID,
                                mVisibility, mCommentable, mMarkAsBuzz, mMarkAsKilt, null, mNewYoutubeUrls);
                    }
                }
                break;
            case R.id.privacy_tv:

                DialogUtil.showListDialogCustom(v.getContext(), R.string.set_privacy, new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {

                        switch (position) {
                            case 0:
                                mPrivacyTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_date_time_white, 0,
                                        R.drawable.ic_arrow_down, 0);
                                break;
                            case 1:
                                mPrivacyTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_lock_white, 0,
                                        R.drawable.ic_arrow_down, 0);
                                break;

                            default:
                                break;
                        }
                        mPrivacyTv.setText(item);
                    }

                    @Override
                    public void onCancel() {

                    }
                }, getString(R.string.Public), getString(R.string.Teammates));
                break;
            case R.id.camera_ll:
                if (mUploadingMedias != null) {
                    if (mUploadingMedias.size() >= 5) {
                        DialogUtil.showOkDialog(v.getContext(), "You can select max " + MAX_MEDIA_SEL_COUNT + " items.", "");
                    } else {
                        openCamera();
                    }
                }

                break;
            case R.id.photo_ll:
                startActivityForResult(LuminousGalleryActivity.getIntent(LuminousAction.ACTION_MULTIPLE_PICK),
                        REQ_CODE_LUMINOUS_GALLERY_MULTIPICK_ACTIVITY);
                break;
            case R.id.video_ll:
                startActivityForResult(LuminousGalleryActivity.getIntentVideo(LuminousAction.ACTION_PICK, "Wall", mMarkAsKilt),
                        LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY);
//                startActivityForResult(UploadYoutubeActivity.getIntent(CreateWallPostActivity.this),
//                        UploadYoutubeActivity.REQ_CODE_UPLOAD_YOUTUBE_ACTIVITY);
                break;
            default:
                break;
        }
    }

    /**
     * This method use for take picture from camera and crop function
     */

    protected void openCamera() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 0);
            return;
        }

        initTmpUris();

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
        intent.putExtra("return-data", true);
        try {
            // Start a camera capturing activity
            // REQUEST_CODE_TAKE_FROM_CAMERA is an integer tag you
            // defined to identify the activity in onActivityResult()
            // when it returns
            startActivityForResult(intent, REQ_CODE_TAKE_FROM_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    private final int REQ_CODE_LUMINOUS_GALLERY_MULTIPICK_ACTIVITY = 1098;
    private LinearLayout mThumbContainerLl;
    private int mCurrIndex = 0;
    private List<UploadingMedia> mUploadingMedias = new ArrayList<UploadingMedia>();
    private Map<Integer, View> mViews = new HashMap<Integer, View>();

    private void initMultiPhotoUploadUI() {
        mThumbContainerLl = (LinearLayout) findViewById(R.id.thumb_container_ll);
        addMediaInLayout();
    }

    private void addMediaInLayout() {

        mThumbContainerLl.removeAllViews();
        mViews.clear();
        if (mUploadingMedias.size() > 0) {
            if (!mIsImage) {
                mMarkAsKiltTv.setVisibility(View.VISIBLE);
            } else {
                mMarkAsKiltTv.setVisibility(View.GONE);
            }
        } else {
            mMarkAsKiltTv.setVisibility(View.GONE);
        }

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final int imgWidth = (int) getResources().getDimension(R.dimen.space_mid10) * 11;
        if (null != mNewYoutubeUrls && mNewYoutubeUrls.size() > 0) {
            for (int i = 0; i < mNewYoutubeUrls.size(); i++) {
                final View view = layoutInflater.inflate(R.layout.upload_media_item, null);
                final View containerRl = view.findViewById(R.id.container_rl);
                containerRl.setTag(i);

                final NewYoutubeUrl newYoutubeUrl = mNewYoutubeUrls.get(i);

                ImageView mediaIv = (ImageView) view.findViewById(R.id.media_iv);
                mediaIv.getLayoutParams().width = imgWidth;
                mediaIv.getLayoutParams().height = imgWidth;
                ImageView typeIv = (ImageView) view.findViewById(R.id.type_iv);

                String thumbUrl = getYoutubeVideoThumb(newYoutubeUrl.Url);
                ImageLoaderUniversal.ImageLoadSquare(this, thumbUrl, mediaIv, ImageLoaderUniversal.option_normal_Image);
                typeIv.setVisibility(View.VISIBLE);

                mThumbContainerLl.addView(view);
                mViews.put(i, containerRl);
                mHeaderLayout.enableRightButton(true);
                view.findViewById(R.id.close_iv).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mThumbContainerLl.removeView(view);
                        mNewYoutubeUrls.remove(newYoutubeUrl);
                        if (mUploadingMedias.size() == 0 && TextUtils.isEmpty(mPostEt.getText().toString())) {
                            mHeaderLayout.enableRightButton(false);
                        }
                    }
                });
            }
        } else if (null != mUploadingMedias && mUploadingMedias.size() > 0) {

            for (int i = 0; i < mUploadingMedias.size(); i++) {
                final View view = layoutInflater.inflate(R.layout.upload_media_item, null);
                final View containerRl = view.findViewById(R.id.container_rl);
                containerRl.setTag(i);

                final UploadingMedia uploadingMedia = mUploadingMedias.get(i);
                ImageView mediaIv = (ImageView) view.findViewById(R.id.media_iv);
                mediaIv.getLayoutParams().width = imgWidth;
                mediaIv.getLayoutParams().height = imgWidth;
                ImageView typeIv = (ImageView) view.findViewById(R.id.type_iv);
                if (mIsImage) {
                    ImageLoaderUniversal.ImageLoadSquare(this, "file://" + uploadingMedia.path, mediaIv,
                            ImageLoaderUniversal.option_normal_Image_Thumbnail);
                    typeIv.setVisibility(View.INVISIBLE);
                } else {
                    loadVideoThumb(uploadingMedia.path, mediaIv);
                    typeIv.setVisibility(View.VISIBLE);
                }

                mThumbContainerLl.addView(view);
                mViews.put(i, containerRl);

                view.findViewById(R.id.close_iv).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mThumbContainerLl.removeView(view);
                        mUploadingMedias.remove(uploadingMedia);
                        if (mUploadingMedias.size() == 0) {
                            if (mMarkAsKiltTv.getVisibility() == View.VISIBLE) {
                                mMarkAsKiltTv.setVisibility(View.GONE);
                            }
                        }
                        if (mUploadingMedias.size() == 0 && TextUtils.isEmpty(mPostEt.getText().toString())) {
                            mHeaderLayout.enableRightButton(false);
                        }
                    }
                });
            }
        }
    }

    public boolean validateVidSizeWithDialogs(Context context, String videoPath, ErrorLayout errorLayout) {
        Utility.showLog("videoPath      " + videoPath);
        File file = new File(videoPath);
        if (!file.exists()) {
            if (null != errorLayout) {
                errorLayout.showError(context.getString(R.string.Invalid_file), true, MsgType.Error);
            } else {
                DialogUtil.showOkDialog(context, context.getString(R.string.Invalid_file), "");
            }
            return false;
        } else if (file.length() > Config.MAX_VIDEO_LEN_BYTES) {
            if (null != errorLayout) {
                errorLayout.showError(context.getString(R.string.Video_is_larger_than_40MB), true, MsgType.Error);
            } else {
                DialogUtil.showOkDialog(context, context.getString(R.string.Video_is_larger_than_40MB),
                        context.getString(R.string.Video_Limt_Crossed));
            }
            return false;
        } else if (getVideoDuration(videoPath) > Config.MAX_VIDEO_LEN_MILLIS) {
            if (null != errorLayout) {
                errorLayout.showError(context.getString(R.string.Video_is_longer_than_6), true, MsgType.Error);
            } else {
                DialogUtil.showOkDialog(context, context.getString(R.string.Video_is_longer_than_6),
                        context.getString(R.string.Video_Limt_Crossed));
            }
            return false;
        }

        return true;
    }

    public boolean validateVidSizefifteenSecondDialogs(Context context, String videoPath, ErrorLayout errorLayout) {

        if (getVideoDuration(videoPath) > Config.MAX_VIDEO_LEN_MILLIS_TEN_SECOND) {
            if (null != errorLayout) {
                errorLayout.showError(context.getString(R.string.Video_is_longer_than_15_second), false, MsgType.Error);
            } else {
                DialogUtil.showOkDialog(context, context.getString(R.string.Video_is_longer_than_15_second),
                        context.getString(R.string.Video_Limt_Crossed));
            }
            return false;
        }

        return true;
    }

    private void updateMarkAsBuzz(boolean checked) {
        mMarkAsBuzz = checked;
        mMarkAsBuzzTv.setTextColor(mMarkAsBuzz ? Color.WHITE : getResources().getColor(R.color.verify_text_color));
        mMarkAsBuzzTv.setBackgroundResource(mMarkAsBuzz ? R.drawable.circle_red_rect : R.drawable.circle_gray_rect);
        mMarkAsBuzzTv.setPadding(20, 15, 20, 15);
    }

    private void updateMarkAsKilt(boolean checked) {
        mMarkAsKilt = checked;
        mMarkAsKiltTv.setTextColor(mMarkAsKilt ? Color.WHITE : getResources().getColor(R.color.verify_text_color));
        mMarkAsKiltTv.setBackgroundResource(mMarkAsKilt ? R.drawable.circle_red_rect : R.drawable.circle_gray_rect);
        mMarkAsKiltTv.setPadding(20, 15, 20, 15);
    }


    public long getVideoDuration(String videoPath) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(videoPath);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        try {
            return Long.parseLong(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    private void updateData() {
        mPrivacyTv.setText((mVisibility == WallPostCreateRequest.VISIBILITY_PUBLIC ? R.string.Public : R.string.Teammates));
        mPostEt.setText(mPostContent);
    }

    private boolean isValid() {

        mVisibility = (getString(R.string.Public).equalsIgnoreCase(mPrivacyTv.getText().toString().trim())
                ? WallPostCreateRequest.VISIBILITY_PUBLIC
                : WallPostCreateRequest.VISIBILITY_TEAMMATES);
        mPostContent = mPostEt.getText().toString().trim();

        if (null != mNewYoutubeUrls && mNewYoutubeUrls.size() > 0) {
            return true;
        } else if (mUploadingMedias.size() > 0) {
            return true;
        } else if (mPostContent.length() == 0) {
            mErrorLayout.showError(getString(R.string.Please_enter_post_content), true, MsgType.Error);
            return false;
        }
        return true;
    }

    private void handleRightBtnEnable() {
        if (mPostEt.getText().toString().trim().length() == 0 && mUploadingMedias.size() == 0) {
            mHeaderLayout.enableRightButton(false);
        } else {
            mHeaderLayout.enableRightButton(true);
        }
    }

    private ArrayList<NewMedia> mNewMeidas = new ArrayList<NewMedia>();

    private RequestListener mRequestListener = new RequestListener() {
        @Override
        public void onComplete(boolean success, Object data, int totalRecords) {
            if (success) {
                AlbumMedia mAlbumMedia = (AlbumMedia) data;
                mNewMedia.MediaGUID = mAlbumMedia.MediaGUID;
                mNewMeidas.add(mNewMedia);

                mCurrIndex++;
                uploadMedia();
            } else if (null != data) {
                dismissProgressDialog();
                DialogUtil.showOkCancelDialog(CreateWallPostActivity.this, R.string.ok_label,
                        R.string.cancel_caps, null, (String) data, null, null);
            }
        }
    };

    private ProgressDialog mProgressDialog = null;
    private NewMedia mNewMedia = null;

    private void uploadMedia() {
        if (mCurrIndex >= mUploadingMedias.size()) {
            mProgressDialog.setMessage(getString(R.string.Posting));
            mWallPostCreateRequest.createWallPostInServer(mPostContent, mModuleID,
                    mUserGUID, mVisibility, mCommentable, mMarkAsBuzz, mMarkAsKilt,
                    mNewMeidas, mNewYoutubeUrls);
            return;
        }
        if (null == mProgressDialog) {
            mProgressDialog = DialogUtil.createDiloag(CreateWallPostActivity.this, composeProgressMessage());
        }
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
        mProgressDialog.setMessage(composeProgressMessage());

        File file = new File(mUploadingMedias.get(mCurrIndex).path);

        if (!mIsImage) {

        }

        mNewMedia = new NewMedia("", file.getName(), mPostContent, "", "");
        mMediaUploadRequest.uploadMediaInServer(mIsImage, file, mNewMedia.Caption,
                MediaUploadRequest.MODULE_ID_ALBUM, mUserGUID,
                MediaUploadRequest.TYPE_WALL);
    }

    private String composeProgressMessage() {
        if (mIsImage && mUploadingMedias.size() > 1) {
            return String.format(getString(R.string.Uploading_d_of_d), (mCurrIndex + 1), mUploadingMedias.size());
        }
        return getString(mIsImage ? R.string.Uploading_photo : R.string.Uploading_video);
    }

    private void dismissProgressDialog() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void setResultAndFinish(Object object) {
        Intent data = new Intent();
        if (null != object) {
            setResult(RESULT_OK, data);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
        this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
    }

    public void loadVideoThumb(final String path, final ImageView imageView) {
        imageView.setImageResource(R.drawable.ic_deault_media);
        new Thread(new Runnable() {

            @Override
            public void run() {
                File file = new File(path);
                final Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(file.getPath(),
                        MediaStore.Images.Thumbnails.MINI_KIND);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (null != thumbnail && null != imageView) {
                            imageView.setImageBitmap(thumbnail);
                        }
                    }
                });
            }
        }).start();
    }

    private void showMediaUploadDialog() {

        DialogUtil.showListDialog(this, R.string.Add_Media, new OnItemClickListener() {

            @Override
            public void onItemClick(int position, String item) {
                switch (position) {
                    case 0:
                        startActivityForResult(LuminousGalleryActivity.getIntent(LuminousAction.ACTION_MULTIPLE_PICK),
                                REQ_CODE_LUMINOUS_GALLERY_MULTIPICK_ACTIVITY);
                        break;
                    case 1:
                        startActivityForResult(LuminousGalleryActivity.getIntentVideo(LuminousAction.ACTION_PICK, "Wall", mMarkAsKilt),
                                LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY);
                        break;
                    case 2:
                        startActivityForResult(UploadYoutubeActivity.getIntent(CreateWallPostActivity.this),
                                UploadYoutubeActivity.REQ_CODE_UPLOAD_YOUTUBE_ACTIVITY);
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onCancel() {
            }
        }, getString(R.string.Add_Photo), getString(R.string.Add_Video), getString(R.string.Add_Youtube_Video));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Config.DEBUG) {
            Log.d(TAG, "onActivityResult requestCode =" + requestCode + ", resultCode=" + resultCode);
        }
        switch (requestCode) {
            case LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY:
                if (Activity.RESULT_OK == resultCode) {
                    mIsImage = data.getBooleanExtra("sel_type_img", true);
                    final String mediaPath = data.getStringExtra("data");
                    mUploadingMedias.clear();
                    mUploadingMedias.add(new UploadingMedia("", mediaPath));
                    if (!mIsImage) {
                        validateVidSizeWithDialogs(CreateWallPostActivity.this, mediaPath, mErrorLayout);
                    }
                    mNewYoutubeUrls = null;
                    handleRightBtnEnable();
                    initMultiPhotoUploadUI();

                }
                break;
            case REQ_CODE_LUMINOUS_GALLERY_MULTIPICK_ACTIVITY:
                if (Activity.RESULT_OK == resultCode) {
                    mNewYoutubeUrls = null;
                    mIsImage = data.getBooleanExtra("sel_type_img", true);
                    final String[] imagePaths = data.getStringArrayExtra("data");
                    if (Config.DEBUG) {
                        Log.d(TAG, "onActivityResult imagePaths=" + imagePaths + ", mIsImage=" + mIsImage);
                    }
                    if ((null != imagePaths && imagePaths.length > 0)) {
                        mUploadingMedias.clear();
                        for (int i = 0; i < imagePaths.length; i++) {
                            mHeaderLayout.enableRightButton(true);
                            mUploadingMedias.add(new UploadingMedia("", imagePaths[i]));
                        }
                        initMultiPhotoUploadUI();
                    }
                }
                break;
            case UploadYoutubeActivity.REQ_CODE_UPLOAD_YOUTUBE_ACTIVITY:
                if (Activity.RESULT_OK == resultCode) {
                    mNewYoutubeUrls = data.getParcelableArrayListExtra("newYoutubeUrls");
                    mNewMeidas.clear();
                    mUploadingMedias.clear();
                    mIsImage = false;
                    handleRightBtnEnable();
                    initMultiPhotoUploadUI();
                }
                break;
            case REQ_CODE_TAKE_FROM_CAMERA:
                if (resultCode == RESULT_OK) {
                    System.out.println("REQ_CODE_TAKE_FROM_CAMERA imageUri: " + capturedImageUri);
                    cropImage();
                }
                break;
            case REQ_CODE_CROP_PHOTO:
                if (resultCode == RESULT_OK) {
                    mIsImage = false;
                    String mediaPath = "";
                    try {
                        capturedImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), cropImageUri);
                        //onCameraImageSelected(cropImageUri.toString(), capturedImageBitmap);
                        mediaPath = cropImageUri.getPath();
                        System.out.println("Camera mediaPath:" + mediaPath);

                        mUploadingMedias.add(new UploadingMedia("", mediaPath));
                        mIsImage = true;
                        mNewYoutubeUrls = null;
                        handleRightBtnEnable();
                        initMultiPhotoUploadUI();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (!mIsImage) {
                        validateVidSizeWithDialogs(CreateWallPostActivity.this, mediaPath, mErrorLayout);
                    }
                }
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getYoutubeVideoThumb(String url) {
        String vidId = MediaViewerActivity.getYouTubeVideoId(url);
        String thumb = "http://img.youtube.com/vi/" + vidId + "/default.jpg";
        if (Config.DEBUG) {
            Log.d(TAG, "getYoutubeVideoThumb thumb=" + thumb);
        }
        return thumb;
    }

    @Override
    public void onBackPressed() {
        //	super.onBackPressed();
        discardMediaDialog();
    }

    private void discardMediaDialog() {
        if (isPostContentAdded()) {
            DialogUtil.showYesNoDialogWithTittle(CreateWallPostActivity.this,
                    getString(R.string.discard_content), getString(R.string.discard_confirmation), new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            discardAndExit();
                        }
                    });
        } else {
            discardAndExit();
        }
    }

    private boolean isPostContentAdded() {
        if (!TextUtils.isEmpty(mPostEt.getText().toString())) {
            return true;
        }
        if (null != mNewYoutubeUrls && mNewYoutubeUrls.size() > 0) {
            return true;
        } else if (mUploadingMedias.size() > 0) {
            return true;
        }
        return false;
    }

    private void discardAndExit() {
        try {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(mPostEt.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        } catch (Exception e) {
            e.printStackTrace();
        }
        setResultAndFinish(null);
    }

    protected void initTmpUris() {
        File proejctDirectory = new File(Environment.getExternalStorageDirectory() + File.separator + "SchollyMeCache");
        if (!proejctDirectory.exists()) {
            proejctDirectory.mkdir();
        } else {
            // delete all old files
            for (File file : proejctDirectory.listFiles()) {
                if (file.getName().startsWith("tmp_")) {
                    file.delete();
                }
            }
        }
        // Construct temporary image path and name to save the taken
        // photo
        capturedImageUri = Uri.fromFile(new File(proejctDirectory, "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
        File extraOutputFile = new File(proejctDirectory, "croped_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        extraOutputFile.setWritable(true);
        cropImageUri = Uri.fromFile(extraOutputFile);
    }

    /*
     * This method use for Crop image taken from camera
	 */
    private void cropImage() {


        // Use existing crop activity.
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");
        intent.setDataAndType(capturedImageUri, IMAGE_UNSPECIFIED); // Uri to the image you want to crop
        /*intent.putExtra("outputX", 296);
        intent.putExtra("outputY", 296);*/
        intent.putExtra("outputX", mOutputX);
        intent.putExtra("outputY", mOutputY);
        intent.putExtra("aspectX", mAspectX);
        intent.putExtra("aspectY", mAspectY);
        intent.putExtra("scale", true);
        intent.putExtra("circleCrop", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cropImageUri);
        startActivityForResult(intent, REQ_CODE_CROP_PHOTO);
    }
}
