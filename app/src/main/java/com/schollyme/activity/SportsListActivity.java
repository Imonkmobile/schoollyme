package com.schollyme.activity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.schollyme.R;
import com.schollyme.adapter.SportsListAdapter;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.SportsModel;
import com.vinfotech.request.GetSportsRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class SportsListActivity extends BaseActivity implements OnClickListener,OnRefreshListener {
	public static final int REQ_CODE_SPORT_LIST_ACTIVITY = 1027;

	private ListView mOptionsLV;
	private AutoCompleteTextView mSearchACTV;
	private LinearLayout mContainerLl;
	private LinearLayout mContainerRl;
	private Context mContext;
	private SportsListAdapter mAdapter;
	private boolean isMulipleselction = false;
	private ArrayList<SportsModel> mSelections;
	private SwipeRefreshLayout mSwipeRefreshWidget;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sports_list_activity);
		mContext = this;
		mOptionsLV = (ListView) findViewById(R.id.items_lv);
		mSearchACTV = (AutoCompleteTextView) findViewById(R.id.search_actv);
		mSearchACTV.setHint(getResources().getString(R.string.search_sport));
		mContainerRl = (LinearLayout) findViewById(R.id.confirmation_parent_rl);
		mContainerLl = (LinearLayout) findViewById(R.id.view_contaniner_layout_confirm_rl);
		mSearchACTV.setText("");
		isMulipleselction = getIntent().getBooleanExtra("isMulipleselction", false);

		initHeader();
		
		mSelections = getIntent().getParcelableArrayListExtra("selections");
		if (null != mSelections) {
			mAllSelections.addAll(mSelections);
		}
		getSportsRequest();
		startAnimation();
		FontLoader.setRobotoRegularTypeface(mSearchACTV);


		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
	}

	private void initHeader() {

		int headerTextRes = 0;
		if(isMulipleselction){
			headerTextRes = R.string.select_sports_label;
		}
		else{
			headerTextRes = R.string.select_sport_label;
		}

		setHeader(findViewById(R.id.header_layout), R.drawable.ic_close, R.drawable.selector_confirm,
				getResources().getString(headerTextRes), new OnClickListener() {

					@Override
					public void onClick(View v) {
						Utility.hideSoftKeyboard(mSearchACTV);
						reverseAnimation(null);
					}
				}, new OnClickListener() {

					@Override
					public void onClick(View v) {
						Utility.hideSoftKeyboard(mSearchACTV);
						if (mAdapter != null) {
							ArrayList<SportsModel> selections;
							if (isMulipleselction) {
								selections = new ArrayList<>(mAllSelections);
							} else {
								selections = mAdapter.getSelections();
							}
							if (selections.size() > 0) {
								Intent intent = new Intent();
								intent.putParcelableArrayListExtra("data", selections);
								setResult(RESULT_OK, intent);
								reverseAnimation(intent);
							} else {
								Toast.makeText(SportsListActivity.this, getResources().getString(R.string.select_one_sports), Toast.LENGTH_SHORT).show();
							}
						} else {
							reverseAnimation(null);
						}
					}
				});
	}

	private void getSportsRequest() {
		if(HttpConnector.getConnectivityStatus(mContext)==0){
			
			DialogUtil.showOkDialog(mContext, Utility.FromResouarceToString(mContext, R.string.No_internet_connection),
					Utility.FromResouarceToString(mContext, R.string.Network_erro));
			if(null!=mSwipeRefreshWidget)
			mSwipeRefreshWidget.setRefreshing(false);
			
		}
		else{
			GetSportsRequest mRequest = new GetSportsRequest(mContext);
			mRequest.setRequestListener(new RequestListener() {

				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					mSwipeRefreshWidget.setRefreshing(false);
					if (success) {
						@SuppressWarnings("unchecked")
						List<SportsModel> sportModels = (List<SportsModel>) data;
						if (null != sportModels && null != mSelections) {
							for (SportsModel sportsModel : sportModels) {
								if (mSelections.contains(sportsModel)) {
									sportsModel.isSelected = true;
								}
							}
						}
						setAdapter(sportModels);

					}
				}
			});
			mRequest.GetSportsServerRequest();
		}
	}

	private Set<SportsModel> mAllSelections = new HashSet<SportsModel>();

	private void setAdapter(List<SportsModel> sportsModels) {
		mAdapter = new SportsListAdapter(SportsListActivity.this);
		mAdapter.setList(sportsModels);
		mOptionsLV.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
		new SearchHandler(mSearchACTV).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				mAdapter.setFilter(text);
			}
		});
		mOptionsLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (!isMulipleselction) {
					mAdapter.select(false);
					mAdapter.getItem(position).isSelected = true;
				} else {
					SportsModel sportsModel = mAdapter.getItem(position);
					sportsModel.isSelected = !sportsModel.isSelected;
					if (sportsModel.isSelected) {
						mAllSelections.add(sportsModel);
					} else {
						for (SportsModel sm : mAllSelections) {
							if (sm.mSportsID.equalsIgnoreCase(sportsModel.mSportsID)) {
								mAllSelections.remove(sm);
								break;
							}
						}
					}
				}
				mAdapter.notifyDataSetChanged();
			}
		});
	}

	public static Intent getIntent(Context context, boolean isMulipleselction, ArrayList<SportsModel> selections) {
		Intent intent = new Intent(context, SportsListActivity.class);
		intent.putExtra("isMulipleselction", isMulipleselction);
		intent.putParcelableArrayListExtra("selections", selections);
		return intent;
	}

	/**
	 * Animation from bottom to top
	 * */
	private void startAnimation() {
		// Cancels any animations for this container.
		mContainerLl.clearAnimation();
		mContainerRl.clearAnimation();

		Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up_dialog);
		mContainerLl.startAnimation(animation);

		AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
	}

	/**
	 * Animation from top to bottom
	 * */
	private void reverseAnimation(final Intent intent) {
		mContainerLl.clearAnimation();
		Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_down_dialog);
		mContainerLl.startAnimation(animation);
		mContainerLl.setVisibility(View.GONE);

		AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
		mContainerRl.setVisibility(View.GONE);

		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				finish();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}
	}

	@Override
	public void onRefresh() {
		getSportsRequest();
	}
}