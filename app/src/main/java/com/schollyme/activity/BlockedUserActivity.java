package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.adapter.BlockedUserAdapter;
import com.schollyme.model.FriendModel;
import com.vinfotech.request.BlockedUserListRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;

/**
 * Created by ravib on 1/19/2016.
 */
public class BlockedUserActivity extends BaseActivity {

    private ListView mBlockedUserLV;
    private Context mContext;
    private BlockedUserAdapter mAdapter;
    private ArrayList<FriendModel> mUsersAL;
    private TextView mNoitemTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blocked_user_activity);
        mContext = this;

        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.ic_white_tick,
                getResources().getString(R.string.blocked_user), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

        mNoitemTV = (TextView) findViewById(R.id.no_record_message_tv);
        mBlockedUserLV = (ListView) findViewById(R.id.user_lv);
        mAdapter = new BlockedUserAdapter(mContext);
        mBlockedUserLV.setAdapter(mAdapter);

        getBlockedUsers();
        FontLoader.setRobotoRegularTypeface(mNoitemTV);
    }

    private void setAdapter() {

        mAdapter.setList(mUsersAL);
        if (mUsersAL.size() == 0) {
            mNoitemTV.setVisibility(View.VISIBLE);
            mBlockedUserLV.setVisibility(View.GONE);
        } else {
            mNoitemTV.setVisibility(View.GONE);
            mBlockedUserLV.setVisibility(View.VISIBLE);
        }
    }

    public void getBlockedUsers() {

        BlockedUserListRequest request = new BlockedUserListRequest(mContext);
        request.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mUsersAL = (ArrayList<FriendModel>) data;
                } else {
                    mUsersAL = new ArrayList<FriendModel>();
                }
                setAdapter();
            }
        });
        request.BlockedUserListServerRequest();
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, BlockedUserActivity.class);
        return intent;
    }
}
