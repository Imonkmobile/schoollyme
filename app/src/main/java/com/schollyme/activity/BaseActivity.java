package com.schollyme.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.BaseLocaleActivity;
import com.schollyme.R;
import com.schollyme.model.LogedInUserModel;
import com.splunk.mint.Mint;
import com.vinfotech.utility.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.FontLoader;

/**
 * Base activity class. This may be useful in
 * Implementing google analytics or
 * Any app wise implementation
 *
 * @author Ravi Bhandari
 */
public abstract class BaseActivity extends BaseLocaleActivity {

    private static List<Activity> sActivities = new ArrayList<>();
    // private ActionBar mActionBar;
    private static TextView actionBarTitle;
    private static ImageView rightButton;
    private static ImageView leftButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sActivities.add(this);
    /*	LocaleUtil mLocaleUtil = new LocaleUtil(this);
		mLocaleUtil.setLocale(mLocaleUtil.mLocaleCode, savedInstanceState);*/

        if (!"devel".equalsIgnoreCase(Config.ENVIRONMENT)) {
            try {
                Mint.initAndStartSession(this, getString(R.string.splunk_api_key));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (TextUtils.isEmpty(BaseRequest.getLoginSessionKey())) {
            BaseRequest.setLoginSessionKey(new LogedInUserModel(this).mLoginSessionKey);
        }

        //	new NetworkMonitor().initNetowkMonitorReceiver();
    }

    @Override
    protected void onDestroy() {
        sActivities.remove(this);
        super.onDestroy();

    }

    public static void finishAllActivities() {
        for (int i = 0; i < sActivities.size(); i++) {
            sActivities.get(i).finish();
        }
    }

    public static void setHeader(View view, int leftButtonId, int rightButtonId, String title,
                                 OnClickListener leftButtonListener, OnClickListener rightButtonListener) {

        actionBarTitle = (TextView) view.findViewById(R.id.header_tv);
        rightButton = (ImageView) view.findViewById(R.id.right_button);
        leftButton = (ImageView) view.findViewById(R.id.back_button);
        FontLoader.setRobotoMediumTypeface(actionBarTitle);
        actionBarTitle.setText(title);
        leftButton.setImageResource(leftButtonId);
        rightButton.setImageResource(rightButtonId);
        rightButton.setOnClickListener(rightButtonListener);
        leftButton.setOnClickListener(leftButtonListener);
    }

    @Override
    public void onLocaleUpdate() {

    }
}