package com.schollyme.activity;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SignUpModel;
import com.vinfotech.utility.Config;
import com.vinfotech.request.SignUpRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONObject;

/**
 * Created by prashantj on 3/28/2017.
 */
public class SignUpFacebookRequest extends BaseRequest implements HttpConnector.HttpResponseListener {

    public static final String TAG = SignUpRequest.class.getSimpleName();
    private static final int REQ_CODE_SIGNUP = 1;
    private Context mContext;
    private boolean mRequesting;
    private HttpConnector mHttpConnector;
    private SignUpModel mSignUpModule;
    private LogedInUserModel logedInUserModel;

    public SignUpFacebookRequest(Context context) {
        this.mContext = context;
        mRequesting = false;
        mHttpConnector = new HttpConnector(context);
        mHttpConnector.setHttpResponseListener(this);
    }


    public void SignupFacebookRequestServer(String Username, String Email, String DOB, String UserTypeID,
                                            String DeviceType, String DeviceID, String Latitude, String Longitude,
                                            String IPAddress, String Resolution, String Token, String userSocialID) {

        if (mRequesting || !mActivityLive) {
            if (Config.DEBUG) {
                Log.v(TAG, "You Already Login....");
            }
            return;
        }

        mRequesting = true;
        String jsonData = getSignInJson(Username, Email, DOB, UserTypeID, DeviceType, DeviceID, Latitude,
                Longitude, IPAddress, Resolution, Token, userSocialID);
        mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
        mHttpConnector.executeAsync("signup/", REQ_CODE_SIGNUP, "post", false, jsonData, null, HttpConnector.UrlType.SERVICE);

    }

    public void setLoader(View view) {
        mHttpConnector.setLoader(view);
    }

    public static String getSignInJson(String Username, String Email, String DOB, String UserTypeID,
                                       String DeviceType, String DeviceID, String Latitude, String Longitude,
                                       String IPAddress, String Resolution, String Token, String userSocialID) {
        JSONObject requestObject = null;
        requestObject = JSONUtil.getJSONObject("Name", Username, "Email", Email, "DOB", DOB, "UserTypeID",
                UserTypeID, "DeviceType", DeviceType, "DeviceID", DeviceID, "Latitude", Latitude, "Longitude",
                Longitude, "IPAddress", IPAddress, "Resolution", Resolution,
                "Token", Token, "UserSocialID", userSocialID, "Password", "", "SocialType", "Facebook API");
        return null == requestObject ? "" : requestObject.toString();
    }

    @Override
    public void onResponse(int reqCode, int statusCode, String json) {

        switch (reqCode) {
            case REQ_CODE_SIGNUP:
                mRequesting = false;
                if (parse(json)) {
                    if (Config.DEBUG) {
                        Log.v(TAG, "SignUp Details=" + getMessage());
                    }
                    if (null != mRequestListener) {
                        mRequestListener.onComplete(true, getMessage(), 0);
                    }
                } else {
                    if (Config.DEBUG) {
                        Log.e("", "Failed to SignUp. Error: " + getMessage());
                    }
                    if (null != mRequestListener) {
                        mRequestListener.onComplete(false, getMessage(), 0);
                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onCancel(boolean canceled) {
    }

    @Override
    public void onProgressChange(int progress) {
    }

    @Override
    protected boolean parse(String json) {
        if (super.parse("signup", json) && isSuccess()) {
            mSignUpModule = new SignUpModel(getDataObject());
            mSignUpModule.persist(mContext);
            logedInUserModel = new LogedInUserModel(getDataObject());
            logedInUserModel.persist(mContext);
            return true;
        }
        return false;
    }

    @Override
    public void setActivityStatus(boolean live) {
        super.mActivityLive = live;
    }

}
