package com.schollyme.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.adapter.SportsDisplayAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.media.MediaPickerActivity;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SportsModel;
import com.schollyme.model.StateModel;
import com.schollyme.model.UserModel;
import com.vinfotech.request.AboutRequest;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.request.SetupProfileRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.ExpandableHeightListView;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.utility.MarshMallowPermission;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.ValidationUtil;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Activity class. This may be useful in setup user profile.
 * It will be call in two different schanario -
 * 1. After first login to app user has to comple his profile basic information after that he can browse through app.
 * Until user not complete setup profile he/she will redirect to this screen.
 * <p>
 * 2. User can click edit profile any time from app, then same screen will open, and can update his profile information
 *
 * @author Ravi Bhandari
 */
public class SetupProfileMandatoryActivity extends MediaPickerActivity implements OnClickListener {

    private SetupProfileViewHolder mSetupProfileViewHolder;
    private Context mContext;
    //public static ArrayList<StateModel> mStatesAL;
    private StateModel mSelectedStateModel;
    private ArrayList<SportsModel> mSportsAL;
    private String mSelectedStateName, mSelectedStateId;
    private static final int REQ_CODE_SELECT_STATE = 100;
    private static final int REQ_CODE_SELECT_SPORTS = 200;
    private static final int REQ_CODE_SELECT_IMAGE = 300;
    private static final int REQ_CODE_OPTIONAL_FIELDS = 400;
    private Bitmap mPhotoBitmap;
    private boolean isProfileSetup = false;

    private LogedInUserModel mLogedInUserModel;
    private int mUserGender = 0;
    private String mUserName, mUserEmail, mUserDOB, mUserFullName, mUserCurrentSchool, mUserCoachingTitle;

    private String mReqType;
    private UserModel mUserModel;
    private ErrorLayout mErrorLayout;
    static final int DATE_PICKER_ID = 1111;
    private int year, month, day;
    private Calendar mCalender;
    private boolean isDOBSelected = false;
    private String[] options;


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void resetLocalPreferance() {
        SharedPreferences mLocalPreferance = getSharedPreferences("LocalPreferance", 0);
        Editor mLocalEditor = mLocalPreferance.edit();
        mLocalEditor.putBoolean("isUsed", false);
        mLocalEditor.clear();
        mLocalEditor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup_profile_mandatory_activity);
        mContext = this;
        mLogedInUserModel = new LogedInUserModel(mContext);
        mSetupProfileViewHolder = new SetupProfileViewHolder(findViewById(R.id.main_rl), this);

        mReqType = getIntent().getStringExtra("mReqType");
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        options = new String[]{getResources().getString(R.string.gender_male), getResources().getString(R.string.gender_female)};

        initHeader();
        setView(mLogedInUserModel.mUserType);

        if (mReqType.equals("2")) {
            AboutServerRequest();
        }

        setFilds();
        viewListnres();
        resetLocalPreferance();
    }

    private void initHeader() {

        int leftResId;
        if (mReqType.equals("2")) {
            leftResId = R.drawable.icon_back;
        } else {
            leftResId = 0/*R.drawable.icon_close*/;
        }

        setHeader(findViewById(R.id.header_layout), leftResId, R.drawable.ic_check_header,
                getResources().getString(R.string.setup_profile_label), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isProfileSetup) {
                            AboutActivity.setmUserGUID(mSetupProfileViewHolder.mUserNameET.getText().toString());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = DashboardActivity.getIntent(mContext, DashboardActivity.HOME_FRAGMENTINDEX, "");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        } else {
                            finish();
                        }
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (setupProfileValidation()) {
                            if (cropImageUri == null) {
                                setupProfileServerRequest(mLogedInUserModel.mUserType, "");
                            } else {
                                uploadMediaServerRequest();
                            }
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        MarshMallowPermission marshMallowPermission = new MarshMallowPermission(this);
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        }

        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isProfileSetup) {
            AboutActivity.setmUserGUID(mSetupProfileViewHolder.mUserNameET.getText().toString());
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    finish();
                }
            });
        } else {
            finish();
        }
    }

    /**
     * Set pre saved data to form when user land to setup profile
     */
    private void setdataFields(UserModel mUser) {
        setDateTOCalender();
        mUserModel = mUser;

        if (!TextUtils.isEmpty(mUserModel.mUserDOB) && !mUserModel.mUserDOB.equals("0000-00-00"))
            mSetupProfileViewHolder.mDOBTV.setText(Utility.formateDate(mUserModel.mUserDOB));
        if (mSetupProfileViewHolder.mDOBTV.length() > 0) {
            mSetupProfileViewHolder.clearRecordsImg.setVisibility(View.VISIBLE);
        } else {
            mSetupProfileViewHolder.clearRecordsImg.setVisibility(View.GONE);
        }
        if (mUser.mUserGender.equals("1")) {
            mSetupProfileViewHolder.mGenderMaleTV.setText(getResources().getString(R.string.gender_male));
            mUserGender = 1;
        } else {
            mSetupProfileViewHolder.mGenderMaleTV.setText(getResources().getString(R.string.gender_female));
            mUserGender = 2;
        }
        mSetupProfileViewHolder.mUserNameET.setText(mUser.mUserName);
        mSetupProfileViewHolder.mCoachingTitleET.setText(mUser.mUserCoachingTitle);
        mSetupProfileViewHolder.mCurrentSchoolET.setText(mUser.mUserCurrentSchool);
        mSetupProfileViewHolder.mStateTV.setText(mUser.mUserStateName);
        mSelectedStateId = mUser.mUserStateId;
        mSelectedStateName = mUser.mUserStateName;
        mSelectedStateModel = new StateModel(mSelectedStateId, mSelectedStateName);
        mSportsAL = mUser.mUserSportsAL;
        displaySelectedSportsAdapter();
        String imageUrl = Config.IMAGE_URL_PROFILE + mUser.mUserProfilePic;
        Utility.LogP("setdataFields ", "imageUrl: " + imageUrl);
        ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mSetupProfileViewHolder.mProfileIV,
                ImageLoaderUniversal.option_Round_Image);

        mSetupProfileViewHolder.mUserNameET.setSelection(mSetupProfileViewHolder.mUserNameET.getText().toString().length());
        mSetupProfileViewHolder.mCoachingTitleET.setSelection(mSetupProfileViewHolder.mCoachingTitleET.getText().toString().length());
        mSetupProfileViewHolder.mCurrentSchoolET.setSelection(mSetupProfileViewHolder.mCurrentSchoolET.getText().toString().length());

    }

    /**
     * Set pre saved data to form when user land to setup profile
     * and show/hide fields according to type of user
     */
    private void setFilds() {
        setDateTOCalender();
        if (!TextUtils.isEmpty(mLogedInUserModel.mUserDOB) && !mLogedInUserModel.mUserDOB.equals("0000-00-00"))
            mSetupProfileViewHolder.mDOBTV.setText(Utility.formateDate(mLogedInUserModel.mUserDOB));
            mSetupProfileViewHolder.mNameET.setText(mLogedInUserModel.mFirstName + " " + mLogedInUserModel.mLastName);
            mSetupProfileViewHolder.mEmailET.setText(mLogedInUserModel.mEmail);

        try {
            if (!TextUtils.isEmpty(mLogedInUserModel.mUserName)) {
                mSetupProfileViewHolder.mUserNameET.setText(mLogedInUserModel.mUserName);
            }
            if (!TextUtils.isEmpty(mLogedInUserModel.mUserGender)) {

                if (mLogedInUserModel.mUserGender.equals("1")) {
                    mSetupProfileViewHolder.mGenderMaleTV.setText(getResources().getString(R.string.gender_male));
                    mUserGender = 1;
                } else {
                    mSetupProfileViewHolder.mGenderMaleTV.setText(getResources().getString(R.string.gender_female));
                    mUserGender = 2;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Intent getIntent(Context context, String mReqType) {
        Intent intent = new Intent(context, SetupProfileMandatoryActivity.class);
        intent.putExtra("mReqType", mReqType);
        return intent;
    }

    private void setView(int userType) {
        if (userType == Config.USER_TYPE_COACH) {
            mSetupProfileViewHolder.layoutSchoolCoaching.setVisibility(View.VISIBLE);
            mSetupProfileViewHolder.mSportLabelTV.setVisibility(View.VISIBLE);
            mSetupProfileViewHolder.mSportsContainerRL.setVisibility(View.VISIBLE);
        } else if (userType == Config.USER_TYPE_ATHLETE) {
            mSetupProfileViewHolder.layoutSchoolCoaching.setVisibility(View.GONE);
            mSetupProfileViewHolder.mSportLabelTV.setVisibility(View.VISIBLE);
            mSetupProfileViewHolder.mSportsContainerRL.setVisibility(View.VISIBLE);
        } else if (userType == Config.USER_TYPE_FAN) {
            mSetupProfileViewHolder.layoutSchoolCoaching.setVisibility(View.GONE);
            mSetupProfileViewHolder.mSportLabelTV.setVisibility(View.GONE);
            mSetupProfileViewHolder.mSportsContainerRL.setVisibility(View.GONE);
            mSetupProfileViewHolder.mDevider8.setVisibility(View.GONE);

        }
    }

    @SuppressWarnings("ResourceAsColor")
    private void viewListnres() {

        mSetupProfileViewHolder.mNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mNameTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mNameET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mNameTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mNameET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mSetupProfileViewHolder.mUserNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mUserNameTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mUserNameET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mUserNameTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mUserNameET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mSetupProfileViewHolder.mCurrentSchoolET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mCurrentSchoolTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mCurrentSchoolET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mCurrentSchoolTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mCurrentSchoolET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mSetupProfileViewHolder.mCoachingTitleET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mCoachingTitleTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mCoachingTitleET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mCoachingTitleTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mCoachingTitleET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnView() {

        mSetupProfileViewHolder.mDOBLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mDOBTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mDOBTV.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mStateLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mStateTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mStateTV.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mSportLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mDevider8.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mGenderMaleLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mGenderMaleTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mGenderMaleTV.setBackgroundResource(R.drawable.ic_gray_line);
    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnDobView() {

        mSetupProfileViewHolder.mNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mEmailET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mEmailET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mUserNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mUserNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mUserNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mCurrentSchoolTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCurrentSchoolET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCurrentSchoolET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mCoachingTitleTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCoachingTitleET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCoachingTitleET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mDOBLabelTV.setTextColor(Color.parseColor("#01579B"));
        mSetupProfileViewHolder.mDOBTV.setTextColor(Color.parseColor("#000000"));
        mSetupProfileViewHolder.mDOBTV.setBackgroundResource(R.drawable.ic_blue_line);

        mSetupProfileViewHolder.mStateLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mStateTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mStateTV.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mSportLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mDevider8.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mGenderMaleLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mGenderMaleTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mGenderMaleTV.setBackgroundResource(R.drawable.ic_gray_line);
    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnStateView() {

        mSetupProfileViewHolder.mNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mEmailET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mEmailET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mUserNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mUserNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mUserNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mCurrentSchoolTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCurrentSchoolET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCurrentSchoolET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mCoachingTitleTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCoachingTitleET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCoachingTitleET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mDOBLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mDOBTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mDOBTV.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mStateLabelTV.setTextColor(Color.parseColor("#01579B"));
        mSetupProfileViewHolder.mStateTV.setTextColor(Color.parseColor("#000000"));
        mSetupProfileViewHolder.mStateTV.setBackgroundResource(R.drawable.ic_blue_line);

        mSetupProfileViewHolder.mSportLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mDevider8.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mGenderMaleLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mGenderMaleTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mGenderMaleTV.setBackgroundResource(R.drawable.ic_gray_line);
    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnSportsView() {

        mSetupProfileViewHolder.mNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mEmailET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mEmailET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mUserNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mUserNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mUserNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mCurrentSchoolTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCurrentSchoolET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCurrentSchoolET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mCoachingTitleTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCoachingTitleET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCoachingTitleET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mDOBLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mDOBTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mDOBTV.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mStateLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mStateTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mStateTV.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mSportLabelTV.setTextColor(Color.parseColor("#01579B"));
        mSetupProfileViewHolder.mDevider8.setBackgroundResource(R.drawable.ic_blue_line);

        mSetupProfileViewHolder.mGenderMaleLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mGenderMaleTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mGenderMaleTV.setBackgroundResource(R.drawable.ic_gray_line);
    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnGenderView() {

        mSetupProfileViewHolder.mNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mEmailET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mEmailET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mUserNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mUserNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mUserNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mCurrentSchoolTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCurrentSchoolET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCurrentSchoolET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mCoachingTitleTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCoachingTitleET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCoachingTitleET.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mDOBLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mDOBTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mDOBTV.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mStateLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mStateTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mStateTV.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mSportLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mDevider8.setBackgroundResource(R.drawable.ic_gray_line);

        mSetupProfileViewHolder.mGenderMaleLabelTV.setTextColor(Color.parseColor("#01579B"));
        mSetupProfileViewHolder.mGenderMaleTV.setTextColor(Color.parseColor("#000000"));
        mSetupProfileViewHolder.mGenderMaleTV.setBackgroundResource(R.drawable.ic_blue_line);
    }

    public class SetupProfileViewHolder {

        public ImageView mProfileIV, clearRecordsImg;
        public EditText mNameET, mEmailET, mUserNameET, mCurrentSchoolET, mCoachingTitleET;
        public TextView mNameTV, mEmailTV, mUserNameTV, mCurrentSchoolTV, mCoachingTitleTV;
        public TextView mDOBTV, mDOBLabelTV, mStateTV, mStateLabelTV, mSportLabelTV, mProfileTV,
                mGenderMaleTV, mGenderMaleLabelTV;
        private ExpandableHeightListView mSportsLV;
        private View mDevider8;
        private LinearLayout layoutSchoolCoaching;
        private RelativeLayout mRelativeLayout, mSportsContainerRL;


        public SetupProfileViewHolder(View view, OnClickListener listener) {

            mNameTV = (TextView) view.findViewById(R.id.user_full_name_tv);
            mNameET = (EditText) view.findViewById(R.id.user_full_name_et);
            mEmailTV = (TextView) view.findViewById(R.id.email_tv);
            mEmailET = (EditText) view.findViewById(R.id.email_et);
            mUserNameTV = (TextView) view.findViewById(R.id.user_name_tv);
            mUserNameET = (EditText) view.findViewById(R.id.user_name_et);
            System.out.println("social type    "+new LogedInUserModel(mContext).getSocialType(mContext));
            if(mLogedInUserModel.getSocialType(mContext).length()==0){
                mUserNameET.setEnabled(true);
            }else{
                mUserNameET.setEnabled(false);

            }

            mCurrentSchoolTV = (TextView) view.findViewById(R.id.current_school_label_tv);
            mCurrentSchoolET = (EditText) view.findViewById(R.id.current_school_et);
            mCoachingTitleTV = (TextView) view.findViewById(R.id.coaching_title_label_tv);
            mCoachingTitleET = (EditText) view.findViewById(R.id.coaching_title_et);
            mDOBLabelTV = (TextView) view.findViewById(R.id.dob_label_tv);
            mDOBTV = (TextView) view.findViewById(R.id.dob_tv);
            clearRecordsImg = (ImageView) view.findViewById(R.id.clearRecordsImg);
            clearRecordsImg.setVisibility(View.GONE);

            mStateLabelTV = (TextView) view.findViewById(R.id.state_label_tv);
            mStateTV = (TextView) view.findViewById(R.id.state_tv);
            mGenderMaleLabelTV = (TextView) view.findViewById(R.id.gender_male_label_tv);
            mGenderMaleTV = (TextView) view.findViewById(R.id.gender_male_tv);

            mSportLabelTV = (TextView) view.findViewById(R.id.sports_tv);
            mSportsContainerRL = (RelativeLayout) view.findViewById(R.id.selected_sports_list_rl);
            mDevider8 = view.findViewById(R.id.devider8);
            mSportsLV = (ExpandableHeightListView) view.findViewById(R.id.selected_sports_list);
            mSportsLV.setExpanded(true);

            layoutSchoolCoaching = (LinearLayout) view.findViewById(R.id.layout_school_coaching);
            mProfileIV = (ImageView) view.findViewById(R.id.profile_iv);
            mProfileTV = (TextView) view.findViewById(R.id.profile_tv);
            mRelativeLayout = (RelativeLayout) view.findViewById(R.id.main_rl);
            mRelativeLayout.requestFocus();
            mSportsContainerRL.setOnClickListener(listener);
            mDOBTV.setOnClickListener(listener);
            clearRecordsImg.setOnClickListener(listener);
            mStateTV.setOnClickListener(listener);
            mSportLabelTV.setOnClickListener(listener);
            mProfileIV.setOnClickListener(listener);
            mProfileTV.setOnClickListener(listener);
            mGenderMaleTV.setOnClickListener(listener);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_tv:
                displayImagePickerPopup();
                break;
            case R.id.profile_iv:
                displayImagePickerPopup();
                break;
            case R.id.dob_tv:
                changeColorOnDobView();
                displayDatePicker();
                break;
            case R.id.clearRecordsImg:
                isDOBSelected = false;
                mSetupProfileViewHolder.mDOBTV.setText("");
                mSetupProfileViewHolder.clearRecordsImg.setVisibility(View.GONE);
                break;
            case R.id.state_tv:
                changeColorOnStateView();
                displayStatesPopup();
                break;
            case R.id.sports_tv:
                changeColorOnSportsView();
                displaySportsListPopup();
                break;
            case R.id.selected_sports_list_rl:
                changeColorOnSportsView();
                displaySportsListPopup();
                break;
            case R.id.gender_male_tv:
                changeColorOnGenderView();
                DialogUtil.showListCustomDialog(this, 0, new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {
                        mSetupProfileViewHolder.mGenderMaleTV.setText(item);
                        if (item.equalsIgnoreCase("male")) {
                            mUserGender = 1;
                        } else {
                            mUserGender = 2;
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                }, 0, options);
                break;
        }
    }


    /**
     * Display popup to select state
     */
    private void displayStatesPopup() {
        startActivityForResult(StateListActivity.getIntent(mContext, mSelectedStateModel),
                REQ_CODE_SELECT_STATE);
    }

    /**
     * Display popup to select sports
     * Athlete can select multiple sport and coach can select only one sport at a time
     */
    private void displaySportsListPopup() {
        startActivityForResult(SportsListActivity.getIntent(mContext, mLogedInUserModel.mUserType
                == Config.USER_TYPE_ATHLETE, mSportsAL), REQ_CODE_SELECT_SPORTS);
    }

    /**
     * When user click change profile image then dialog will open
     * to choose image from either galley or open camera
     */
    private void displayImagePickerPopup() {

        DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
            @Override
            public void onItemClick(int position, String item) {
                if (position == 0) {
                    openCamera();
                } else {
                    openGallery();
                }
            }

            @Override
            public void onCancel() {

            }
        }, mContext.getString(R.string.take_picture), mContext.getString(R.string.load_from_library));
    }

    /**
     * Handel on activity result response, when user start any activity and that activity
     * will return some value to calling activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case REQ_CODE_SELECT_IMAGE:
                    if (resultCode == RESULT_OK) {
                        String result = data.getStringExtra("result");
                        if ("CAMERA".equalsIgnoreCase(result)) {
                            openCamera();
                        } else if ("GALLERY".equalsIgnoreCase(result)) {
                            openGallery();
                        } else if ("Delete".equalsIgnoreCase(result)) {
                            mPhotoBitmap = null;
                            mSetupProfileViewHolder.mProfileIV.setImageResource(R.drawable.ic_default_profile);
                        }
                    }
                    break;
                case REQ_CODE_SELECT_STATE:
                    if (resultCode == RESULT_OK && data != null) {
                        mSelectedStateName = data.getStringExtra("mStateName");
                        mSelectedStateId = data.getStringExtra("mStateID");
                        mSetupProfileViewHolder.mStateTV.setText(mSelectedStateName);
                        mSelectedStateModel = new StateModel(mSelectedStateId, mSelectedStateName);
                    }

                    break;

                case REQ_CODE_SELECT_SPORTS:
                    if (data != null) {
                        mSportsAL = data.getParcelableArrayListExtra("data");
                        displaySelectedSportsAdapter();
                    }
                    break;
                case REQ_CODE_OPTIONAL_FIELDS:
                    String mUserProfileURL = data.getStringExtra("mUserGUID");
                    startActivity(AboutActivity.getIntent(mContext, mUserProfileURL, "SetupProfileMandatoryActivity"));
                    finish();
                    break;
            }
        }
    }

    /**
     * It will display list of selected sports
     */
    private void displaySelectedSportsAdapter() {

        mSetupProfileViewHolder.mSportsLV.setVisibility(View.VISIBLE);
        SportsDisplayAdapter mAdapter = new SportsDisplayAdapter(mContext);
        mSetupProfileViewHolder.mSportsLV.setAdapter(mAdapter);
        mAdapter.setList(mSportsAL);
    }

    @Override
    // Not used in responce, get the result of gallery selection in
    // below(onCameraImageSelected method) method
    protected void onGalleryImageSelected(String fileUri, Bitmap bitmap) {
    }

    @Override
    protected void onCameraImageSelected(String fileUri, Bitmap bitmap) {
        mPhotoBitmap = bitmap;
        mSetupProfileViewHolder.mProfileIV.setImageBitmap(ImageUtil.getCircledBitmap(mPhotoBitmap));
    }

    @Override
    protected void onVideoCaptured(String videoPath) {
    }

    @Override
    protected void onMediaPickCanceled(int reqCode) {

    }

    private void setDateTOCalender() {
        String userDOB = mLogedInUserModel.mUserDOB;
        if (TextUtils.isEmpty(userDOB) || userDOB.equals("0000-00-00")) {
            initDatePicker();
        } else {
            isDOBSelected = true;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = format.parse(userDOB);
                mCalender = Calendar.getInstance();
                mCalender.setTimeInMillis(date.getTime());
                year = mCalender.get(Calendar.YEAR);
                month = mCalender.get(Calendar.MONTH) + 1;
                day = mCalender.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void initDatePicker() {
        mCalender = Calendar.getInstance();
        year = mCalender.get(Calendar.YEAR);
        month = mCalender.get(Calendar.MONTH);
        day = mCalender.get(Calendar.DAY_OF_MONTH);
    }


    /**
     * Display date picker dialog for pick DOB from user
     */
    private void displayDatePicker() {
        mCalender = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(mContext, pickerListener, year, month, day);
        dialog.getDatePicker().setMaxDate(mCalender.getTimeInMillis());
        dialog.getDatePicker().setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        dialog.show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }


    /**
     * Listener for date picker invoke when user select any date from picker dialog
     */
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            isDOBSelected = true;
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            DecimalFormat mFormat = new DecimalFormat("00");
            int cMonth = month + 1;
            String cDay = mFormat.format(Double.valueOf(day));

            String mMonth = mFormat.format(Double.valueOf(cMonth));
            // Month is 0 based, just add 1
            mSetupProfileViewHolder.mDOBTV.setText(new StringBuilder().append(cDay).append(" ")
                    .append(Utility.getMOnthNameFromNumber(month)).append(",").append(" ").append(year));

            if (mSetupProfileViewHolder.mDOBTV.length() > 0) {
                mSetupProfileViewHolder.clearRecordsImg.setVisibility(View.VISIBLE);
            }
        }
    };

    /**
     * Validate setup profile form
     */
    private boolean setupProfileValidation() {

        boolean isValidated = false;
        mUserName = mSetupProfileViewHolder.mUserNameET.getText().toString();
        mUserEmail = mSetupProfileViewHolder.mEmailET.getText().toString();
        mUserFullName = mSetupProfileViewHolder.mNameET.getText().toString();
        DecimalFormat mFormat = new DecimalFormat("00");
        int cMonth = month + 1;

        String mMonth = mFormat.format(Double.valueOf(cMonth));
        String mDay = mFormat.format(Double.valueOf(day));
        mUserDOB = new StringBuilder().append(year).append("-").append(mMonth).append("-")
                .append(mDay).toString();//mSignupViewHolder.mDobTV.getText().toString();

        //	mUserDOB = mLogedInUserModel.mUserDOB;//mSetupProfileViewHolder.mDOBTV.getText().toString();
        if (TextUtils.isEmpty(mUserFullName)) {
            mErrorLayout.showError(getResources().getString(R.string.user_fullname_require), true, MsgType.Error);
        }
        /*else if (!isDOBSelected) {
            mUserDOB = "";
		}*//* else if (isDOBSelected && Utility.getDateDifference(mUserDOB) < 13) {
            mErrorLayout.showError(getResources().getString(R.string.age_restriction_error), true, MsgType.Error);
		}*/
        /* else if (Utility.getDateDifference(mUserDOB) < 13) {
            mErrorLayout.showError(getResources().getString(R.string.age_restriction_error), true, MsgType.Error);
		}*/
        else if (mUserGender == 0) {
            mErrorLayout.showError(getResources().getString(R.string.select_gender_error), true, MsgType.Error);
        } else if (TextUtils.isEmpty(mUserName)) {
            mErrorLayout.showError(getResources().getString(R.string.username_require_message), true, MsgType.Error);
        } else if (!ValidationUtil.isValidUsername(mUserName)) {
            mErrorLayout.showError(getResources().getString(R.string.invalid_user_name), true, MsgType.Error);
        } else if (mUserName.length() > 50) {
            mErrorLayout.showError(getResources().getString(R.string.user_name_length_exceed), true, MsgType.Error);
        } else if (TextUtils.isEmpty(mSelectedStateId)) {
            mErrorLayout.showError(getResources().getString(R.string.state_require), true, MsgType.Error);
        } else if (!(mLogedInUserModel.mUserType == Config.USER_TYPE_FAN)) {
            if (null == mSportsAL || mSportsAL.size() == 0) {
                mErrorLayout.showError(getResources().getString(R.string.sports_require), true, MsgType.Error);
            } else {
                if (mLogedInUserModel.mUserType == Config.USER_TYPE_COACH) {
                    mUserCurrentSchool = mSetupProfileViewHolder.mCurrentSchoolET.getText().toString();
                    mUserCoachingTitle = mSetupProfileViewHolder.mCoachingTitleET.getText().toString();
                    if (TextUtils.isEmpty(mUserCurrentSchool)) {
                        mErrorLayout.showError(getResources().getString(R.string.current_School_require), true, MsgType.Error);
                    } else if (TextUtils.isEmpty(mUserCoachingTitle)) {
                        mErrorLayout.showError(getResources().getString(R.string.coaching_title_require), true, MsgType.Error);
                    } else {
                        isValidated = true;
                    }
                } else if (mLogedInUserModel.mUserType == Config.USER_TYPE_ATHLETE) {
                    isValidated = true;
                } else if (mLogedInUserModel.mUserType == Config.USER_TYPE_FAN) {
                    isValidated = true;
                }
            }
        } else {
            if (mLogedInUserModel.mUserType == Config.USER_TYPE_COACH) {
                mUserCurrentSchool = mSetupProfileViewHolder.mCurrentSchoolET.getText().toString();
                mUserCoachingTitle = mSetupProfileViewHolder.mCoachingTitleET.getText().toString();
                if (TextUtils.isEmpty(mUserCurrentSchool)) {
                    mErrorLayout.showError(getResources().getString(R.string.current_School_require), true, MsgType.Error);
                } else if (TextUtils.isEmpty(mUserCoachingTitle)) {
                    mErrorLayout.showError(getResources().getString(R.string.coaching_title_require), true, MsgType.Error);
                } else {
                    isValidated = true;
                }
            } else if (mLogedInUserModel.mUserType == Config.USER_TYPE_ATHLETE) {
                isValidated = true;
            } else if (mLogedInUserModel.mUserType == Config.USER_TYPE_FAN) {
                isValidated = true;
            }
        }
        return isValidated;

    }


    /**
     * Get user saved information from server,
     * it will called only when user click edit profile
     */
    private void AboutServerRequest() {

        AboutRequest mRequest = new AboutRequest(mContext);
        String mUserLinkUrl = new LogedInUserModel(mContext).mUserProfileURL;
        if (TextUtils.isEmpty(mUserLinkUrl)) {
            mUserLinkUrl = new LogedInUserModel(mContext).mUserName;
        }

        mRequest.AboutRequestServer(new LogedInUserModel(mContext).mLoginSessionKey, mUserLinkUrl);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    UserModel mModel = (UserModel) data;
                    setdataFields(mModel);
                } else {

                    DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {
                                    finish();
                                }
                            });
                }
            }
        });
    }

    /**
     * It will upload user profile pic to server and return a media id,
     * that will be appended to user profile
     */
    private void uploadMediaServerRequest() {

        MediaUploadRequest mRequest = new MediaUploadRequest(mContext, false);
        String fileName = ImageUtil.getFileNameFromURI(cropImageUri, mContext);
        File mFile = new File(cropImageUri.getPath());

        BaseRequest.setLoginSessionKey(mLogedInUserModel.mLoginSessionKey);
        mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_USERS,
                mLogedInUserModel.mUserGUID, MediaUploadRequest.TYPE_PROFILE);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    cropImageUri = null;
                    AlbumMedia mAlbumMedia = (AlbumMedia) data;
                    setupProfileServerRequest(mLogedInUserModel.mUserType, mAlbumMedia.ImageName);
                    mLogedInUserModel.mUserPicURL = mAlbumMedia.ImageName;
                    mLogedInUserModel.persist(mContext);
                }
            }
        });
    }

    /**
     * Update profile info to server
     */
    private void setupProfileServerRequest(int userType, final String profilePicName) {
        if (!isDOBSelected) {
            mUserDOB = "";
            LogedInUserModel logedInUserModelnew = new LogedInUserModel();
            logedInUserModelnew.mUserDOB = "";
            logedInUserModelnew.persist(mContext);
        }

        SetupProfileRequest mRequest = new SetupProfileRequest(mContext);
        BaseRequest.setLoginSessionKey(mLogedInUserModel.mLoginSessionKey);
        final ArrayList<String> mSportsList = new ArrayList<String>();
        if (userType == 1) {

            mSportsList.add(String.valueOf(mSportsAL.get(0).mSportsID));
            if (mUserModel == null) {
                mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB,
                        String.valueOf(mUserGender), mSelectedStateId, mSportsList, mUserCurrentSchool,
                        mUserCoachingTitle, "", "", "", "", "", "", "", "", "", profilePicName);
            } else {
                mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
                        mSelectedStateId, mSportsList, mUserCurrentSchool, mUserCoachingTitle, mUserModel.mUserCoachingPhilosophy,
                        mUserModel.mUserMentor, mUserModel.mUserAlmaMater, mUserModel.mUserFavAthelet, mUserModel.mUserFavTeam,
                        mUserModel.mUserFavMovie, mUserModel.mUserFavMusic, mUserModel.mUserFavFood, mUserModel.mUserFavTeacher,
                        profilePicName);
            }
        } else if (userType == 2) {
            for (int i = 0; i < mSportsAL.size(); i++) {
                mSportsList.add(String.valueOf(mSportsAL.get(i).mSportsID));
            }
            if (mUserModel == null) {
                mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
                        mSelectedStateId, mSportsList, "", "", "", "", "", "", "", "", "", "", "", profilePicName, "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "");
            } else {
                mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
                        mSelectedStateId, mSportsList, mUserModel.mUserPosition, mUserModel.mUserHomeTeam, mUserModel.mUserJerseyNumber,
                        mUserModel.mUserCurrentSchool, mUserModel.mUserClubTeam, mUserModel.mUserFavAthelet, mUserModel.mUserFavTeam,
                        mUserModel.mUserFavMovie, mUserModel.mUserFavMusic, mUserModel.mUserFavFood, mUserModel.mUserFavTeacher,
                        profilePicName, mUserModel.mUserFavQuote, mUserModel.mUserHomeAddress, mUserModel.mUserPhoneNumber,
                        mUserModel.mUserAlternateEmail, mUserModel.mUserGPAScore, mUserModel.mSATScore, mUserModel.mUserACTScore,
                        mUserModel.mUserTranscripts, mUserModel.mUserNCAAEligibility, mUserModel.mUserParentID, mUserModel.mUserParentName,
                        mUserModel.mUserParentEmail, mUserModel.mUserParentPhoneNumber, "", mUserModel.mUserHeight);
            }


        } else if (userType == 3) {
            if (mUserModel == null) {
                mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
                        mSelectedStateId, "", "", "", "", "", "", profilePicName, "");
            } else {
                mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
                        mSelectedStateId, mUserModel.mUserFavAthelet, mUserModel.mUserFavTeam, mUserModel.mUserFavMovie,
                        mUserModel.mUserFavMusic, mUserModel.mUserFavFood, mUserModel.mUserFavTeacher, profilePicName, mUserModel.mUserFavQuote);
            }
        }
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    isProfileSetup = true;
                    updateDataAndCallIntent();

                } else {
                    DialogUtil.showOkDialogUncancelableButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {

                                }
                            });
                }
            }
        });
    }


    /**
     * Update user profile info in local preferance and call profile setup optional activity
     */
    private void updateDataAndCallIntent() {

        ArrayList<String> selSportIds = new ArrayList<String>();
        if (null != mSportsAL) {
            for (SportsModel sportsModel : mSportsAL) {
                selSportIds.add(sportsModel.mSportsID);
                if (mLogedInUserModel.mUserType == Config.USER_TYPE_COACH) {
                    mLogedInUserModel.mUserSportsId = sportsModel.mSportsID;
                    mLogedInUserModel.mUserSportsName = sportsModel.mSportsName;
                }
            }
        }

        mLogedInUserModel.mUserName = mUserName;
        mLogedInUserModel.mUserDOB = mUserDOB;
        mLogedInUserModel.mUserProfileURL = mUserName;
        mLogedInUserModel.mUserGender = String.valueOf(mUserGender);

        mLogedInUserModel.persist(mContext);
        mLogedInUserModel.updatePreference(mContext, "isProfileSetUp", true);


        if (mUserModel == null) {
            mUserModel = new UserModel();
        }
        mUserModel.mUserFullName = mUserFullName;
        if (mReqType.equals("1")) {
            mUserModel.mUserName = mSetupProfileViewHolder.mUserNameET.getText().toString();
            mUserModel.mUserEmail = mUserEmail;
            mUserModel.mUserDOB = mUserDOB;

            if (mLogedInUserModel.mUserType == Config.USER_TYPE_COACH)
                mUserModel.mUserCurrentSchool = mUserCurrentSchool;
            mUserModel.mUserCoachingTitle = mUserCoachingTitle;
            mUserModel.mUserGender = String.valueOf(mUserGender);
            mUserModel.mUserStateId = String.valueOf(mSelectedStateId);
        }
        mUserModel.mUserName = mSetupProfileViewHolder.mUserNameET.getText().toString();
        if (mLogedInUserModel.mUserType == Config.USER_TYPE_COACH)
            mUserModel.mUserCurrentSchool = mUserCurrentSchool;
        mUserModel.mUserCoachingTitle = mUserCoachingTitle;
        mUserModel.mUserGender = String.valueOf(mUserGender);
        mUserModel.mUserStateId = String.valueOf(mSelectedStateId);
        mUserModel.mUserName = mSetupProfileViewHolder.mUserNameET.getText().toString();

        Intent mIntent = SetupProfileOptionalActivity.getIntent(mContext, mUserModel, mReqType, selSportIds);
        mIntent.putExtra("isProfileSetup", isProfileSetup);
        startActivity(mIntent);
        finish();
    }
}