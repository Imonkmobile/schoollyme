package com.schollyme.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.vinfotech.request.ResetPasswordRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;

/**
 * Activity class. This may be useful in Reset/Change Password of app user
 * implementation
 *
 * @author Ravi Bhandari
 */
public class ResetPasswordActivity extends BaseActivity implements OnClickListener {

    private EditText mTokenET, mNewPasswordET, mCPasswordET;
    private TextView mTokenTV, mNewPasswordTV, mCPasswordTV;
    private String mTokenString, mPasswordString;
    private ErrorLayout mErrorLayout;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password_activity);
        mContext = this;

        initHeader();
        controlInitialization();
        viewListnres();
    }

    private void initHeader() {

        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
                getResources().getString(R.string.reset_password_header), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validateFields()) {
                            ResetPasswordRequest();
                        }
                    }
                });
    }

    private void controlInitialization() {

        mTokenET = (EditText) findViewById(R.id.token_et);
        mNewPasswordET = (EditText) findViewById(R.id.new_password_et);
        mCPasswordET = (EditText) findViewById(R.id.c_password_et);
        mTokenTV = (TextView) findViewById(R.id.token_tv);
        mNewPasswordTV = (TextView) findViewById(R.id.new_password_tv);
        mCPasswordTV = (TextView) findViewById(R.id.c_password_tv);

        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
    }

    @SuppressWarnings("ResourceAsColor")
    private void viewListnres() {

        mTokenET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mTokenTV.setTextColor(Color.parseColor("#01579B"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mTokenTV.setTextColor(Color.parseColor("#cfcfcf"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
            }
        });

        mNewPasswordET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mNewPasswordTV.setTextColor(Color.parseColor("#01579B"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mNewPasswordTV.setTextColor(Color.parseColor("#cfcfcf"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
            }
        });

        mCPasswordET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mCPasswordTV.setTextColor(Color.parseColor("#01579B"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mCPasswordTV.setTextColor(Color.parseColor("#cfcfcf"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
            }
        });

        mCPasswordET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateFields()) {
                        ResetPasswordRequest();
                    }
                    return true;
                }
                return false;
            }
        });

    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, ResetPasswordActivity.class);
        return intent;
    }


    /**
     * Validate fileds require for rest/change password process
     */
    private boolean validateFields() {
        mTokenString = mTokenET.getText().toString().trim();
        mPasswordString = mNewPasswordET.getText().toString().trim();
        String cPassword = mCPasswordET.getText().toString().trim();
        if (TextUtils.isEmpty(mTokenString)) {
            mErrorLayout.showError(getResources().getString(R.string.enter_token), true, MsgType.Error);
        } else if (TextUtils.isEmpty(mPasswordString)) {
            mErrorLayout.showError(getResources().getString(R.string.password_require_message), true, MsgType.Error);
        } else if (mPasswordString.length() < 5) {
            mErrorLayout.showError(getResources().getString(R.string.password_length_message), true, MsgType.Error);
        } else if (TextUtils.isEmpty(cPassword)) {
            mErrorLayout.showError(getResources().getString(R.string.cpassword_require_message), true, MsgType.Error);
        } else if (!mPasswordString.equals(cPassword)) {
            mErrorLayout.showError(getResources().getString(R.string.password_not_match_message), true, MsgType.Error);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }


    /**
     * Server request for change password
     */
    private void ResetPasswordRequest() {

        ResetPasswordRequest mForgotPasswordRequest = new ResetPasswordRequest(mContext);
        mForgotPasswordRequest.ResetPasswordServerRequest(mPasswordString, mTokenString);
        mForgotPasswordRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {

                    DialogUtil.showOkCancelDialogButtonLisnter(mContext, getResources().getString(R.string.password_reset_success), getResources()
                            .getString(R.string.app_name), new OnOkButtonListner() {

                        @Override
                        public void onOkBUtton() {

                            Intent intent = LoginActivity.getIntent(mContext);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();

                        }
                    }, new OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            Intent intent = LoginActivity.getIntent(mContext);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });

                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
    }
}