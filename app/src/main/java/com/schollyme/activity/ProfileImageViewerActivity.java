package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.widget.TouchImageView;


/**
 * This class used for Display User profile picture on full screen view.
 * @Auther  - Ravi Bhandari
 * */
public class ProfileImageViewerActivity extends BaseActivity {

    private static final String TAG = ProfileImageViewerActivity.class.getSimpleName();
    public static final int REQ_CODE_MEDIA_VIEWER_ACTIVITY = 1013;

    private ErrorLayout mErrorLayout;
    private String mImageURL;
    private Context mContext;
    private VHolder mVHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.profile_viewer_activity);
        mContext = this;
        initlization();
        getIntentData();

        ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, mImageURL, mVHolder.mProfileImageIV,
                ImageLoaderUniversal.DiaplayProfileOptionForProgresser, mVHolder.mLoadingPb);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public static Intent getIntent(Context context,String mImageURL) {
        Intent intent = new Intent(context, ProfileImageViewerActivity.class);
        intent.putExtra("ImageURL",mImageURL);
        return intent;
    }

    private void getIntentData() {
       mImageURL = getIntent().getStringExtra("ImageURL");
    }

    private void initlization() {
        mErrorLayout = new ErrorLayout(findViewById(R.id.error_layout));
        mVHolder = new VHolder(findViewById(R.id.main_container_rl));

    }

    class VHolder {
        private TextView mDoneTv;
        private TouchImageView mProfileImageIV;
        private ProgressBar mLoadingPb;

        public VHolder(View view) {

            mProfileImageIV = (TouchImageView)view.findViewById(R.id.profile_image_iv);
            mDoneTv = (TextView) view.findViewById(R.id.done_tv);
            mLoadingPb = (ProgressBar) view.findViewById(R.id.loading_pb);
            FontLoader.setRobotoRegularTypeface(mDoneTv);
            mDoneTv.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }
}