package com.schollyme.activity;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.schollyme.R;
import com.schollyme.adapter.StateListAdapter;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.StateModel;
import com.vinfotech.request.GetStatesRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class StateListActivity extends BaseActivity implements OnClickListener,OnRefreshListener {

	private ListView mOptionsLV;
	private AutoCompleteTextView mSearchACTV;
	private LinearLayout mContainerLl;
	private LinearLayout mContainerRl;
	private Context mContext;
	private StateListAdapter mStateAdapter;
	private StateModel mSelections = null;
	private SwipeRefreshLayout mSwipeRefreshWidget;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sports_list_activity);
		mContext = this;
		mOptionsLV = (ListView) findViewById(R.id.items_lv);
		mSearchACTV = (AutoCompleteTextView) findViewById(R.id.search_actv);
		mSearchACTV.setHint(getResources().getString(R.string.search_state));
		mContainerRl = (LinearLayout) findViewById(R.id.confirmation_parent_rl);
		mContainerLl = (LinearLayout) findViewById(R.id.view_contaniner_layout_confirm_rl);
		mSearchACTV.setText("");
		setHeader(findViewById(R.id.header_layout), R.drawable.ic_close, R.drawable.selector_confirm,
				getResources().getString(R.string.select_state_label), new OnClickListener() {

			@Override
			public void onClick(View v) {
				Utility.hideSoftKeyboard(mSearchACTV);
				reverseAnimation(null);
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				Utility.hideSoftKeyboard(mSearchACTV);
				if (mStateAdapter != null) {

					if (mSelections!=null) {
						Intent intent = new Intent();
						intent.putExtra("mStateID", mSelections.mStateId);
						intent.putExtra("mStateName", mSelections.mStateName);
						setResult(RESULT_OK, intent);
						reverseAnimation(intent);
					}
					else {
						Toast.makeText(StateListActivity.this, getResources().getString(R.string.select_one_state), Toast.LENGTH_SHORT).show();
					}
				} else {
					reverseAnimation(null);
				}
			}
		});
		mSelections = getIntent().getParcelableExtra("selections");
		/*if (null != mSelections) {
			mAllSelections.addAll(mSelections);
		}*/
		getStateRequest();
		startAnimation();
		FontLoader.setRobotoRegularTypeface(mSearchACTV);
		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		mSearchACTV.setHint(getResources().getString(R.string.search_state));

	}

	private void setAdapter(final List<StateModel> stateModels) {
		mStateAdapter = new StateListAdapter(StateListActivity.this);
		mStateAdapter.setList(stateModels);
		mOptionsLV.setAdapter(mStateAdapter);
		mStateAdapter.notifyDataSetChanged();
		new SearchHandler(mSearchACTV).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				mStateAdapter.setFilter(text);
			}
		});
		mOptionsLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mSelections = mStateAdapter.getItem(position);

				for(int i=0;i<stateModels.size();i++){
					StateModel mCModel  = stateModels.get(i);
					if(mCModel.mStateId.equals(mSelections.mStateId)){
						mSelections.isSelected = !mSelections.isSelected;
						stateModels.set(i, mSelections);
					}
					else{
						mCModel.isSelected = false;
						stateModels.set(i, mCModel);
					}
				}
				//			mStateAdapter.setList(stateModels);
				mStateAdapter.notifyDataSetChanged();
				//	mSelections = null;
			}
		});
	}

	public static Intent getIntent(Context context, StateModel selections) {
		Intent intent = new Intent(context, StateListActivity.class);
		intent.putExtra("selections", selections);
		return intent;
	}

	/**
	 * Animation from bottom to top
	 * */
	private void startAnimation() {
		// Cancels any animations for this container.
		mContainerLl.clearAnimation();
		mContainerRl.clearAnimation();

		Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up_dialog);
		mContainerLl.startAnimation(animation);

		AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
	}

	/**
	 * Animation from top to bottom
	 * */
	private void reverseAnimation(final Intent intent) {
		mContainerLl.clearAnimation();
		Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_down_dialog);
		mContainerLl.startAnimation(animation);
		mContainerLl.setVisibility(View.GONE);

		AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
		mContainerRl.setVisibility(View.GONE);

		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				finish();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}
	}

	private void getStateRequest() {
		if(HttpConnector.getConnectivityStatus(mContext)==0){
			DialogUtil.showOkDialog(mContext, Utility.FromResouarceToString(mContext, R.string.No_internet_connection),
					Utility.FromResouarceToString(mContext, R.string.Network_erro));
			if(null!=mSwipeRefreshWidget)
			mSwipeRefreshWidget.setRefreshing(false);
		}
		else{
			GetStatesRequest mRequest = new GetStatesRequest(mContext);
			mRequest.GetStatesServerRequest();
			mRequest.setRequestListener(new RequestListener() {

				@SuppressWarnings("unchecked")
				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					mSwipeRefreshWidget.setRefreshing(false);
					if (success) {
						@SuppressWarnings("unchecked")
						List<StateModel> stateModels = (List<StateModel>) data;
						if (null != stateModels && null != mSelections) {
							for (StateModel sportsModel : stateModels) {
								if (mSelections.equals(sportsModel)) {
									sportsModel.isSelected = true;
								}
							}
						}
						setAdapter(stateModels);
					}
				}
			});
		}
	}

	@Override
	public void onRefresh() {
		getStateRequest();
	}
}