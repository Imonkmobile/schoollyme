package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.schollyme.BaseLocaleActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.LocaleUtil;

/**
 * Activity class. This may be useful in Change language for app user
 * Language can change to either English/Spanish
 *
 * @author Ravi Bhandari
 */
public class LanguageChangeActivity extends BaseActivity implements View.OnClickListener {

    //private Context mContext;
    //private ErrorLayout mErrorLayout;
    private TextView mEnglishTV, mSpanishTV;

    private String mDefaultLocal;
    private Bundle mSavedInstanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_change_activity);
        //mContext = this;
        mSavedInstanceState = savedInstanceState;
        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
                getResources().getString(R.string.language), new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }, new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        LocaleUtil mLocaleUtil = new LocaleUtil(LanguageChangeActivity.this);
                        final boolean mLocalUpdated = !mLocaleUtil.mLocaleCode.equals(mDefaultLocal);
                        mLocaleUtil.setLocale(mDefaultLocal, mSavedInstanceState);
                        finish();

                        if (mLocalUpdated) {
                            sendBroadcast(BaseLocaleActivity.getLocaleUpdateIntent());
                        }
                    }
                });

       // mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        mEnglishTV = (TextView) findViewById(R.id.language_eng_tv);
        mSpanishTV = (TextView) findViewById(R.id.language_es_tv);
        mEnglishTV.setOnClickListener(this);
        mSpanishTV.setOnClickListener(this);
        FontLoader.setRobotoMediumTypeface(mEnglishTV, mSpanishTV);
        LocaleUtil mLocal = new LocaleUtil(this);
        mDefaultLocal = mLocal.mLocaleCode;

        if (null != mDefaultLocal && mDefaultLocal.equals("es")) {
            mSpanishTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_blue_tick, 0);
            mEnglishTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            mDefaultLocal = "es";
        } else {
            mEnglishTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_blue_tick, 0);
            mSpanishTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            mDefaultLocal = "en";
        }
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, LanguageChangeActivity.class);
        return intent;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.language_eng_tv:
                mEnglishTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_blue_tick, 0);
                mSpanishTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mDefaultLocal = "en";
                break;
            case R.id.language_es_tv:
                mSpanishTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_blue_tick, 0);
                mEnglishTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mDefaultLocal = "es";
                break;
        }
    }
}
