package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.adapter.UserTypeAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.UserTypeModel;
import com.vinfotech.request.GetUsersTypeRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.DialogUtil;

import java.util.ArrayList;
/**
 * Activity class. This may be useful in change user profile type
 * User can change his profile to any ATHLETE/COACH/FAN profile type
 * @author Ravi Bhandari
 *
 */
public class ChangeUserTypeActivity extends BaseActivity implements View.OnClickListener{

    private static Context mContext;
    private ListView mUserTypeLV;
    private TextView mNoRecordTV;
    private boolean loadingFlag = false;
    private static ErrorLayout mErrorLayout;
    public static ArrayList<UserTypeModel> mUserTypeAL;
    private LogedInUserModel mLogedInUserModel;
    private int mUserType  = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_usertype_activity);
        mContext = this;
        mUserTypeLV = (ListView) findViewById(R.id.user_type_lv);

        mLogedInUserModel = new LogedInUserModel(mContext);
        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
                getResources().getString(R.string.profile_type), new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateUserType();
                    }
                });

        getUserTypeServerRequest();
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, ChangeUserTypeActivity.class);
        return intent;
    }

    @Override
    public void onClick(View v) {

    }

    private void setAdapter(final ArrayList<UserTypeModel>  list){
        mUserType = mLogedInUserModel.mUserType;
        final UserTypeAdapter mAdapter = new UserTypeAdapter(mContext,mUserType);
        mAdapter.setList(list);
        mUserTypeLV.setAdapter(mAdapter);
        mUserTypeLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mUserType = Integer.parseInt(list.get(position).mTypeId);
                mAdapter.changeUserType(mUserType);
            }
        });
    }


    private void getUserTypeServerRequest() {
        GetUsersTypeRequest mRequest = new GetUsersTypeRequest(mContext);
        mRequest.getUsersTypeServerRequest();
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mUserTypeAL = (ArrayList<UserTypeModel>) data;
                    setAdapter(mUserTypeAL);
                } else {
                    DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name), new DialogUtil.OnOkButtonListner() {

                        @Override
                        public void onOkBUtton() {
                            finish();
                        }
                    });
                }
            }
        });
    }

    private void updateUserType(){
        if(mLogedInUserModel.mUserType==mUserType){
            //Nothing will happen as  user doen not change usertype
        }
        else{
            startActivity(ChangeProfileTypeConfirmationActivity.getIntent(mContext,mUserType));
        }
    }
}
