package com.schollyme.activity;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONObject;

/**
 * Created by prashantj on 3/28/2017.
 */
public class LoginFacebookRequest extends BaseRequest implements HttpConnector.HttpResponseListener {

    public static final String TAG = LoginFacebookRequest.class.getSimpleName();
    private static final int REQ_CODE_LOGIN = 1;

    private Context mContext;
    private boolean mRequesting;
    private HttpConnector mHttpConnector;

    private LogedInUserModel mLogedInUserModel;
    private Activity mActivity;
    private String Username = "", Email = "", UserSocialID = "";

    public LoginFacebookRequest(Context context) {
        this.mContext = context;
        mRequesting = false;
        mHttpConnector = new HttpConnector(context);
        mHttpConnector.setHttpResponseListener(this);
    }

    public void LoginFacebookRequestServer(String mDeviceToken, String deviceType, String mEmailStr, String ipAddress,
                                           String mCurLat, String mCurLog, String socialType, String deviceToken,
                                           String userSocialID, String username, Activity loginActivity) {

        if (mRequesting || !mActivityLive) {
            if (Config.DEBUG) {
                Log.v(TAG, "You Already Login....");
            }
            return;
        }

        mRequesting = true;
        mActivity = loginActivity;
        String jsonData = getSignInJson(mDeviceToken, deviceType, mEmailStr, ipAddress, mCurLat,
                mCurLog, socialType, deviceToken, userSocialID, username);
        mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
        mHttpConnector.executeAsync("login/", REQ_CODE_LOGIN, "post", false,
                jsonData, null, HttpConnector.UrlType.SERVICE);

    }

    public void setLoader(View view) {
        mHttpConnector.setLoader(view);
    }

    private String getSignInJson(String mDeviceToken, String deviceType, String mEmailStr,
                                 String ipAddress, String mCurLat, String mCurLog, String socialType,
                                 String deviceToken, String userSocialID, String username) {

        JSONObject jsonObj = JSONUtil.getJSONObject("DeviceID", mDeviceToken, "DeviceType",
                deviceType, "Email", mEmailStr, "IPAddress", ipAddress,
                "Latitude", mCurLat, "Longitude", mCurLog, "SocialType", socialType,
                "Token", "", "UserSocialID", userSocialID, "Username", username);

        Username = username;
        Email = mEmailStr;
        UserSocialID = userSocialID;

        return null == jsonObj ? "" : jsonObj.toString();

    }

    @Override
    public void onResponse(int reqCode, int statusCode, String json) {

        switch (reqCode) {
            case REQ_CODE_LOGIN:
                mRequesting = false;
                if (parse(json)) {
                    if (Config.DEBUG) {
                        Log.v(TAG, "onResponse " + mLogedInUserModel);
                    }
                    if (null != mRequestListener) {
                        try {
                            mLogedInUserModel = new LogedInUserModel(getDataObject());
                            mLogedInUserModel.persist(mContext);
                            mRequestListener.onComplete(true, mLogedInUserModel, 0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (getResponseCode().equals("501")) {
                    try {
                        mLogedInUserModel = new LogedInUserModel(getDataObject());
                        mLogedInUserModel.persist(mContext);
                        mRequestListener.onComplete(true, mLogedInUserModel, 0);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (getResponseCode().equals("504")) {
                    try {
                        mContext.startActivity(SignupActivity.getIntentFacebook(mContext, Username, Email, UserSocialID));
                        mActivity.overridePendingTransition(R.anim.slide_up_dialog, 0);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // Handling Error Messages
                    if (Config.DEBUG) {
                        Log.e(TAG, "Failed to SignIn. Error: " + getMessage());
                    }
                    if (null != mRequestListener) {
                        mRequestListener.onComplete(false, getMessage(), 0);
                    }
                }
                break;

            default:
                break;
        }
    }


    @Override
    public void onCancel(boolean canceled) {

    }

    @Override
    public void onProgressChange(int progress) {

    }

    @Override
    public void setActivityStatus(boolean live) {
        super.mActivityLive = live;
        mHttpConnector.setActivityStatus(mActivityLive);
    }

    @Override
    protected boolean parse(String json) {
        if (super.parse("login", json) && isSuccess()) {

            return true;
        }
        return false;
    }

}
