package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.vinfotech.utility.Config;
import com.schollyme.GCMBaseActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.LoginRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.LocaleUtil;
import com.vinfotech.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Activity class. This may be useful in Login of app user implementation
 *
 * @author Ravi Bhandari
 */
public class LoginActivity extends GCMBaseActivity implements OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private EditText mEmailEt, mPasswordEt;
    private TextView mForgotPasswordTv, mSignupTv, mSignupHeaderTV, mCopyRightTV;
    private LinearLayout main_rl;
    private Button mLoginBtn;
    private View mDividerView;
    private ImageView mAppLogoIV;
    private RelativeLayout mMainContainerRL;
    private LinearLayout mBottomLL;

    private String mEmailStr, mPassStr;
    private Context mContext;
    private double mCurLat = 0.0, mCurLog = 0.0;
    private static final String TAG = "Signup";
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private FusedLocationProviderApi fusedLocationProviderApi;
    private ErrorLayout mErrorLayout;
    private CallbackManager callbackManager;
    private LoginButton mFB_Iv;
    private TextView facebook_Iv;

    //private FrameLayout FrameLayoutFacebook;
    /*public  void check(){
        TestRequest mRequest = new TestRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

			}
		});
		mRequest.call();
	}*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LocaleUtil localeUtil = new LocaleUtil(this);
        if (TextUtils.isEmpty(localeUtil.mLocaleCode)) {
            localeUtil.setLocale("en", savedInstanceState);
        } else {
            localeUtil.setLocale(localeUtil.mLocaleCode, savedInstanceState);
        }
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.login_activity);
        //	new LocaleUtil(this).setLocale("es",savedInstanceState);

        mContext = this;
        mAppLogoIV = (ImageView) findViewById(R.id.app_icon);
        mCopyRightTV = (TextView) findViewById(R.id.copyright_tv);

        showAnimation();
        mErrorLayout = new ErrorLayout(findViewById(R.id.bg_container_rl));

        //getAppKeyHash();
    }


    private void getAppKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;

                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.d("Hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            // TODO Auto-generated catch block
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

    }

    /**
     * Display animation when splash screen load,
     * Move logo icon and display login fields.
     */
    private void showAnimation() {
        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_center_to_up);
        mAppLogoIV.startAnimation(slideUp);

        slideUp.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAppLogoIV.clearAnimation();
                mAppLogoIV.setVisibility(View.GONE);
                LogedInUserModel model = new LogedInUserModel(mContext);
                if (!model.mLoginStatus) {
                    controlInitialization();
                    getLocation();
                } else {
                    if (model.isProfileSetUp) {
                        startActivity(DashboardActivity.getIntent(mContext, DashboardActivity.HOME_FRAGMENTINDEX, ""));
                    } else {
                        startActivity(SetupProfileMandatoryActivity.getIntent(mContext, "1"));
                    }
                    finish();
                }
            }
        });
    }

    /**
     * Validate login require fields
     */
    private boolean validateLogin() {

        mEmailStr = mEmailEt.getText().toString();
        mPassStr = mPasswordEt.getText().toString();
        if (TextUtils.isEmpty(mEmailStr)) {
            mErrorLayout.showError(getResources().getString(R.string.email_username_require_message), true,
                    MsgType.Error);
        } else if (TextUtils.isEmpty(mPassStr)) {
            mErrorLayout.showError(getResources().getString(R.string.password_require_message), true,
                    MsgType.Error);
        } else {
            return true;
        }

        return false;
    }

    /**
     * init location provide for get user location
     */
    private void getLocation() {
        try {
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
            fusedLocationProviderApi = LocationServices.FusedLocationApi;
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .build();
            if (googleApiClient != null) {
                googleApiClient.connect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Location listener invoke when device connected to location provider
     */
    @Override
    public void onConnected(Bundle arg0) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {

    }

    private void getCurrentLocation() {

        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            Location location = fusedLocationProviderApi.getLastLocation(googleApiClient);
            if (location != null) {
                Log.e(TAG, "onLocationChanged: " + location);
                mCurLat = (double) (location.getLatitude());
                mCurLog = (double) (location.getLongitude());
                googleApiClient.disconnect();
            } else {
                getLocation();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void controlInitialization() {

        //FrameLayoutFacebook = (FrameLayout) findViewById(R.id.FrameLayout1);
        mEmailEt = (EditText) findViewById(R.id.email_et);
        mPasswordEt = (EditText) findViewById(R.id.password_et);
        mDividerView = findViewById(R.id.divider_view1);
        mLoginBtn = (Button) findViewById(R.id.Login_button);
        mForgotPasswordTv = (TextView) findViewById(R.id.forgot_password_tv);
        mSignupTv = (TextView) findViewById(R.id.signup_tv);
        mSignupHeaderTV = (TextView) findViewById(R.id.signup_header_tv);
        mMainContainerRL = (RelativeLayout) findViewById(R.id.bg_container_rl);

        main_rl = (LinearLayout) findViewById(R.id.main_rl);
        mBottomLL = (LinearLayout) findViewById(R.id.bottom_lll);
        mLoginBtn.setOnClickListener(this);
        mSignupTv.setOnClickListener(this);
        mForgotPasswordTv.setOnClickListener(this);
        main_rl.setVisibility(View.VISIBLE);


        //mMainContainerRL.setBackgroundResource(R.drawable.icon_login_bg);
//        FrameLayoutFacebook.setVisibility(View.VISIBLE);
//        mEmailEt.setVisibility(View.VISIBLE);
//        mPasswordEt.setVisibility(View.VISIBLE);
//        mDividerView.setVisibility(View.VISIBLE);
//        mLoginBtn.setVisibility(View.VISIBLE);
//        mBottomLL.setVisibility(View.VISIBLE);
//        mForgotPasswordTv.setVisibility(View.VISIBLE);
        mCopyRightTV.setVisibility(View.GONE);

        mPasswordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateLogin()) {
                        loginRequest();
                    }
                    return true;
                }
                return false;
            }
        });


        facebook_Iv = (TextView) findViewById(R.id.facebook_Iv);
        facebook_Iv.setOnClickListener(this);

        mFB_Iv = (LoginButton) findViewById(R.id.fb_login_button);
        mFB_Iv.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        mFB_Iv.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        Bundle bFacebookData = getFacebookData(object);
                        if (bFacebookData != null) {

                            String SocialType = "Facebook API";
                            mEmailStr = bFacebookData.getString("email");

                            String UserSocialID = bFacebookData.getString("idFacebook");
                            String Username = bFacebookData.getString("first_name") + " " + bFacebookData.getString("last_name");

                            LoginViaFacebook(mDeviceToken, Config.DEVICE_TYPE, mEmailStr, Utility.getIPAddress(true),
                                    String.valueOf(mCurLat), String.valueOf(mCurLog), SocialType, mDeviceToken,
                                    UserSocialID, Username);
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
                // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // Toast.makeText(LoginActivity.this, "Login cancelled by user!", Toast.LENGTH_LONG).show();
                System.out.println("Facebook Login failed!!");

            }

            @Override
            public void onError(FacebookException e) {
                // Toast.makeText(LoginActivity.this, "Login unsuccessful!", Toast.LENGTH_LONG).show();
                System.out.println("Facebook Login failed!!");
            }
        });

    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));


            return bundle;
        } catch (JSONException e) {
            Log.d(TAG, "Error parsing JSON");
        }
        return null;
    }

    private void LoginViaFacebook(String mDeviceToken, String deviceType, String mEmailStr, String ipAddress,
                                  String mCurLat, String mCurLog, String socialType, String deviceToken,
                                  String userSocialID, String username) {

        LoginFacebookRequest loginRequest = new LoginFacebookRequest(this);// Utility.md5(mPassStr)
        loginRequest.LoginFacebookRequestServer(mDeviceToken, deviceType, mEmailStr, ipAddress, mCurLat,
                mCurLog, socialType, deviceToken, userSocialID, username, LoginActivity.this);
        loginRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    LogedInUserModel model = (LogedInUserModel) data;
                    Intent intent;
                    if (model.mUserStatusId == 1) {
                        model.updatePreference(mContext, "mLoginStatus", false);
                        intent = new Intent(mContext, VerifyAccountActivity.class);
                        startActivity(intent);
                    } else {
                        LogedInUserModel mLogedInUserModel = new LogedInUserModel(mContext);
                        mLogedInUserModel.setSocialType(mContext,"fb");
                        model.updatePreference(mContext, "mLoginStatus", true);
                        model.setSocialType(mContext,"fb");
                        BaseRequest.setLoginSessionKey(mLogedInUserModel.mLoginSessionKey);
                        if (model.isProfileSetUp) {
                            intent = DashboardActivity.getIntent(mContext, DashboardActivity.HOME_FRAGMENTINDEX, "");
                        } else {
                            intent = SetupProfileMandatoryActivity.getIntent(mContext, "1");
                        }

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    String message = data.toString();
                    if (TextUtils.isEmpty(message)) {
                        DialogUtil.showOkDialog(
                                mContext,
                                getResources().getString(
                                        R.string.login_server_error_message),
                                getResources().getString(R.string.app_name));
                    } else {
                        DialogUtil.showOkDialog(mContext, message,
                                getResources().getString(R.string.app_name));
                    }
                }
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    /**
     * ///////......... Listeners ..........///////////
     **/
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.Login_button:
                if (validateLogin()) {
                    loginRequest();
                }
                break;
            case R.id.signup_tv:
                startActivity(SignupActivity.getIntent(mContext));
                overridePendingTransition(R.anim.slide_up_dialog, 0);
                break;
            case R.id.forgot_password_tv:
                Utility.hideSoftKeyboard(mPasswordEt);
                Intent intent = ForgotPasswordActivity.getIntent(mContext);
                intent.putExtra("user_input", mEmailEt.getText().toString());
                startActivity(intent);
                break;
            case R.id.facebook_Iv:
                LoginManager.getInstance().logOut();
                mFB_Iv.performClick();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }

    /**
     * Server request for login user request
     */
    private void loginRequest() {

        LoginRequest loginRequest = new LoginRequest(this);// Utility.md5(mPassStr)
        loginRequest.LoginRequestServer(mEmailStr, mPassStr, Config.DEVICE_TYPE, mDeviceToken,
                String.valueOf(mCurLat), String.valueOf(mCurLog), Utility.getIPAddress(true), "");

        loginRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    LogedInUserModel model = (LogedInUserModel) data;

                    Intent intent;
                    if (model.mUserStatusId == 1) {
                        model.updatePreference(mContext, "mLoginStatus", false);
                        intent = new Intent(mContext, VerifyAccountActivity.class);
                        startActivity(intent);
                    } else {
                        model.setSocialType(mContext,"");
                        model.updatePreference(mContext, "mLoginStatus", true);
                        BaseRequest.setLoginSessionKey(new LogedInUserModel(mContext).mLoginSessionKey);
                        if (model.isProfileSetUp) {
                            intent = DashboardActivity.getIntent(mContext, DashboardActivity.HOME_FRAGMENTINDEX, "");
                        } else {
                            intent = SetupProfileMandatoryActivity.getIntent(mContext, "1");
                        }

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    String message = data.toString();
                    if (TextUtils.isEmpty(message)) {
                        DialogUtil.showOkDialog(mContext, getResources().getString(
                                R.string.login_server_error_message), getResources().getString(R.string.app_name));
                    } else {
                        DialogUtil.showOkDialog(mContext, message, getResources().getString(R.string.app_name));
                    }
                }
            }
        });
    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {

    }
}