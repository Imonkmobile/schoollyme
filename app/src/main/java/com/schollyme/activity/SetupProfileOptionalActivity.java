package com.schollyme.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.UserModel;
import com.vinfotech.request.SetupProfileRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Activity class. This may be useful in setup user profile.
 * It will be setup option fields.
 * User can skip all fields to update
 *
 * @author Ravi Bhandari
 */
public class SetupProfileOptionalActivity extends BaseActivity implements OnClickListener {

    private SetupProfileViewHolder mSetupProfileViewHolder;
    private Context mContext;
    private LogedInUserModel mLogedInUserModel;
    private int mUserGender = 0;
    private String mUserEmail, mUserDOB, mUserFullName, mUserName, mUserCurrentSchool, mUserCoachingTitle, mSelectedStateId;
    private ArrayList<String> mSelectedSportsId;
    private String mReqType;
    private UserModel mUserModel;
    private String selectedFeet, selectedInch;
    private SharedPreferences mLocalPreferance;
    private Dialog mDisplayDialog;
    private ErrorLayout mErrorLayout;
    private boolean isProfileSetup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup_profile_optional_activity);
        mContext = this;
        initHeader();

        mSetupProfileViewHolder = new SetupProfileViewHolder(findViewById(R.id.main_rl), this);
        mLogedInUserModel = new LogedInUserModel(mContext);
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        isProfileSetup = getIntent().getBooleanExtra("", false);

        getDataFromIntentAndInitFields();
        if (isLocalPreferance()) {
            getFromLocalPreferance();
        }
        viewListnres();

        Utility.LogP("SetupProfileOptional ","mUserType: "+mLogedInUserModel.mUserType);

    }

    private void initHeader() {

        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
                getResources().getString(R.string.setup_profile_label),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setLocalPreferance();
                        if (isProfileSetup) {
                            Intent intent = DashboardActivity.getIntent(mContext, DashboardActivity.HOME_FRAGMENTINDEX, "");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            finish();
                        }
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateListener();
                    }
                });
    }

    private void updateListener() {
        if (validateFields(mLogedInUserModel.mUserType)) {
            setupProfileServerRequest(mLogedInUserModel.mUserType, "");
        }
    }

    /**
     * Validate user setup profile option form.
     * There are all field are optional
     * but if user input value in any field then it should contain any valid value as per require
     */
    private boolean validateFields(int userType) {
        if (userType == 2) {

            String mJerseyNo = mSetupProfileViewHolder.mSportJerseyET.getText().toString().trim();
            String mPhoneNumber = mSetupProfileViewHolder.mPhoneNumberET.getText().toString().trim();
            String mParentPhoneNumber = mSetupProfileViewHolder.mParentHomeET.getText().toString().trim();
            String mAlternateEmail = mSetupProfileViewHolder.mAlternateEmailET.getText().toString().trim();
            String mParentEmail = mSetupProfileViewHolder.mParentEmailET.getText().toString().trim();

            if (!TextUtils.isEmpty(mAlternateEmail) && !ValidationUtil.isValidEmail(mAlternateEmail)) {
                mErrorLayout.showError(getResources().getString(R.string.alternate_email_invalid), true, MsgType.Error);
                return false;
            } else if (!TextUtils.isEmpty(mParentEmail) && !ValidationUtil.isValidEmail(mParentEmail)) {
                mErrorLayout.showError(getResources().getString(R.string.parents_email_invalid), true, MsgType.Error);
                return false;
            } else if (!TextUtils.isEmpty(mPhoneNumber) && mPhoneNumber.length() > 14) {
                mErrorLayout.showError(getResources().getString(R.string.phone_number_error), true, MsgType.Error);
                return false;
            } else if (!TextUtils.isEmpty(mParentPhoneNumber) && mParentPhoneNumber.length() > 14) {
                mErrorLayout.showError(getResources().getString(R.string.parent_phone_number_error), true, MsgType.Error);
                return false;
            } else if (!TextUtils.isEmpty(mJerseyNo) && mJerseyNo.length() > 4) {
                mErrorLayout.showError(getResources().getString(R.string.jeysey_number_error), true, MsgType.Error);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setLocalPreferance();
    }


    /**
     * Get data from intent received from previous activity and set it in form
     */
    private void getDataFromIntentAndInitFields() {

        mReqType = getIntent().getStringExtra("mReqType");
        mUserModel = (UserModel) getIntent().getSerializableExtra("mUserData");
        if (mUserModel != null) {

            /*if(mLogedInUserModel.mUserType == 2) {
                mSetupProfileViewHolder.mFavAthleteET.setVisibility(View.GONE);
                mSetupProfileViewHolder.mFavTeamET.setVisibility(View.GONE);
            } else {
                mSetupProfileViewHolder.mFavAthleteET.setVisibility(View.VISIBLE);
                mSetupProfileViewHolder.mFavTeamET.setVisibility(View.VISIBLE);
            }*/

            mUserEmail = mUserModel.mUserEmail;
            mUserDOB = mUserModel.mUserDOB;
            mUserFullName = mUserModel.mUserFullName;
            mUserName = mUserModel.mUserName;
            mUserCurrentSchool = mUserModel.mUserCurrentSchool;
            mUserCoachingTitle = mUserModel.mUserCoachingTitle;
            mUserGender = Integer.parseInt(mUserModel.mUserGender);
            mSelectedStateId = mUserModel.mUserStateId;
            mSelectedSportsId = getIntent().getStringArrayListExtra("mSelectedSportsId");

            mSetupProfileViewHolder.mFavAthleteET.setText(mUserModel.mUserFavAthelet);
            mSetupProfileViewHolder.mFavTeamET.setText(mUserModel.mUserFavTeam);
            mSetupProfileViewHolder.mFavQuote.setText(mUserModel.mUserFavQuote);
            mSetupProfileViewHolder.mHomeAddressET.setText(mUserModel.mUserHomeAddress);
            mSetupProfileViewHolder.mPhoneNumberET.setText(mUserModel.mUserPhoneNumber);
            mSetupProfileViewHolder.mAlternateEmailET.setText(mUserModel.mUserAlternateEmail);
            mSetupProfileViewHolder.mParentNameET.setText(mUserModel.mUserParentName);
            mSetupProfileViewHolder.mParentHomeET.setText(mUserModel.mUserParentPhoneNumber);
            mSetupProfileViewHolder.mParentEmailET.setText(mUserModel.mUserParentEmail);
            String mUserGPA = mUserModel.mUserGPAScore;
            if (null != mUserGPA && mUserGPA.startsWith("0")) {
                mUserGPA = mUserGPA.replace("0", "");
            }

            mSetupProfileViewHolder.mFavMovieET.setText(mUserModel.mUserFavMovie);
            mSetupProfileViewHolder.mFavMusicET.setText(mUserModel.mUserFavMusic);
            mSetupProfileViewHolder.mFavFoodET.setText(mUserModel.mUserFavFood);
            mSetupProfileViewHolder.mFavTeacher.setText(mUserModel.mUserFavTeacher);
            mSetupProfileViewHolder.mSportPositionET.setText(mUserModel.mUserPosition);
            mSetupProfileViewHolder.mSportHomeTeamET.setText(mUserModel.mUserHomeTeam);
            mSetupProfileViewHolder.mSportJerseyET.setText(mUserModel.mUserJerseyNumber);

            mSetupProfileViewHolder.mSportHomeTeamET.setText(mUserModel.mUserHomeTeam);
            mSetupProfileViewHolder.mSportNameOfClubTeam.setText(mUserModel.mUserClubTeam);
            mSetupProfileViewHolder.mNCAAET.setText(mUserModel.mUserNCAAEligibility);
            if (!TextUtils.isEmpty(mUserModel.mUserHeight)) {
                String convertedValue = Utility.convertInchesToFeetAndInch(Integer.parseInt(mUserModel.mUserHeight), mContext);
                mSetupProfileViewHolder.mHeightTV.setText(convertedValue);
                if (!TextUtils.isEmpty(convertedValue)) {
                    String[] arr = convertedValue.split(" ");
                    selectedFeet = arr[0] + " " + arr[1];
                    selectedInch = arr[2] + " " + arr[3];
                }
            }
            if (!TextUtils.isEmpty(mUserModel.mUserCoachingPhilosophy)) {
                mSetupProfileViewHolder.mCoachingPhilosophyET.setText(String.valueOf(mUserModel.mUserCoachingPhilosophy));
            }
            if (!TextUtils.isEmpty(mUserModel.mUserAlmaMater)) {
                mSetupProfileViewHolder.mAlmaMeterET.setText(String.valueOf(mUserModel.mUserAlmaMater));
            }
            if (!TextUtils.isEmpty(mUserModel.mUserMentor)) {
                mSetupProfileViewHolder.mMentorET.setText(String.valueOf(mUserModel.mUserMentor));
            }

            /*mSetupProfileViewHolder.mSportFavAthlete.setText(mUserModel.mUserFavAthelet);
            mSetupProfileViewHolder.mSportFavTeam.setText(mUserModel.mUserFavTeam);*/
            mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setText(mUserModel.mUserCurrentSchool);

            setCursor();
        }
    }

    private void setCursor() {
        /*mSetupProfileViewHolder.mFavAthleteET.setSelection(mSetupProfileViewHolder.mFavAthleteET.getText().toString().length());
        mSetupProfileViewHolder.mFavTeamET.setSelection(mSetupProfileViewHolder.mFavTeamET.getText().toString().length());
        mSetupProfileViewHolder.mFavQuote.setSelection(mSetupProfileViewHolder.mFavQuote.getText().toString().length());
        mSetupProfileViewHolder.mHomeAddressET.setSelection(mSetupProfileViewHolder.mHomeAddressET.getText().toString().length());
        mSetupProfileViewHolder.mPhoneNumberET.setSelection(mSetupProfileViewHolder.mPhoneNumberET.getText().toString().length());
        mSetupProfileViewHolder.mAlternateEmailET.setSelection(mSetupProfileViewHolder.mAlternateEmailET.getText().toString().length());
        mSetupProfileViewHolder.mParentNameET.setSelection(mSetupProfileViewHolder.mParentNameET.getText().toString().length());
        mSetupProfileViewHolder.mParentHomeET.setSelection(mSetupProfileViewHolder.mParentHomeET.getText().toString().length());
        mSetupProfileViewHolder.mParentEmailET.setSelection(mSetupProfileViewHolder.mParentEmailET.getText().toString().length());
        mSetupProfileViewHolder.mFavMovieET.setSelection(mSetupProfileViewHolder.mFavMovieET.getText().toString().length());
        mSetupProfileViewHolder.mFavMusicET.setSelection(mSetupProfileViewHolder.mFavMusicET.getText().toString().length());
        mSetupProfileViewHolder.mFavFoodET.setSelection(mSetupProfileViewHolder.mFavFoodET.getText().toString().length());
        mSetupProfileViewHolder.mFavTeacher.setSelection(mSetupProfileViewHolder.mFavTeacher.getText().toString().length());
        mSetupProfileViewHolder.mSportPositionET.setSelection(mSetupProfileViewHolder.mSportPositionET.getText().toString().length());
        mSetupProfileViewHolder.mSportHomeTeamET.setSelection(mSetupProfileViewHolder.mSportHomeTeamET.getText().toString().length());
        mSetupProfileViewHolder.mSportJerseyET.setSelection(mSetupProfileViewHolder.mSportJerseyET.getText().toString().length());

        mSetupProfileViewHolder.mSportHomeTeamET.setSelection(mSetupProfileViewHolder.mSportHomeTeamET.getText().toString().length());
        mSetupProfileViewHolder.mSportNameOfClubTeam.setSelection(mSetupProfileViewHolder.mSportNameOfClubTeam.getText().toString().length());
        mSetupProfileViewHolder.mNCAAET.setSelection(mSetupProfileViewHolder.mNCAAET.getText().toString().length());
        mSetupProfileViewHolder.mCoachingPhilosophyET.setSelection(String.valueOf(mSetupProfileViewHolder.mCoachingPhilosophyET.getText().toString()).length());
        mSetupProfileViewHolder.mAlmaMeterET.setSelection(String.valueOf(mSetupProfileViewHolder.mAlmaMeterET.getText().toString()).length());
        mSetupProfileViewHolder.mMentorET.setSelection(String.valueOf(mSetupProfileViewHolder.mMentorET.getText().toString()).length());
        mSetupProfileViewHolder.mSportFavAthlete.setSelection(mSetupProfileViewHolder.mSportFavAthlete.getText().toString().length());
        mSetupProfileViewHolder.mSportFavTeam.setSelection(mSetupProfileViewHolder.mSportFavTeam.getText().toString().length());
        mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setSelection(mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.getText().toString().length());*/
    }

    public static Intent getIntent(Context context, UserModel mUserModel/*, Uri cropImageUri*/,
                                   String mReqType, ArrayList<String> list) {
        Intent mIntent = new Intent(context, SetupProfileOptionalActivity.class);
        mIntent.putExtra("mReqType", mReqType);
        mIntent.putExtra("mUserData", mUserModel);
        mIntent.putExtra("mSelectedSportsId", list);
        return mIntent;
    }

    public class SetupProfileViewHolder {

        public EditText mHomeAddressET, mPhoneNumberET, mAlternateEmailET, mParentNameET, mParentHomeET,
                mParentEmailET, mCoachingPhilosophyET, mMentorET, mAlmaMeterET, mNCAAET, mFavAthleteET, mFavTeamET, mFavMovieET,
                mFavMusicET, mFavFoodET, mFavTeacher, mFavQuote, mSportPositionET, mSportHomeTeamET, mSportJerseyET,
                mSportNameOfCurrentSchoolET, mSportNameOfClubTeam/*, mSportFavAthlete, mSportFavTeam*/;

        private TextView mHomeAddressTV, mPhoneNumberTV, mAlternateEmailTV, mParentNameTV, mParentHomeTV,
                mParentEmailTV, mCoachingPhilosophyTV, mMentorTV, mAlmaMeterTV, mNCAATV, mFavAthleteTV,
                mFavTeamTV, mFavMovieTV, mFavMusicTV, mFavFoodTV, mFavTeacherTV, mFavQuoteTV,
                mSportPositionTV, mSportHomeTeamTV, mSportJerseyTV, mSportNameOfCurrentSchoolTV,
                mSportNameOfClubTeamTV/*, mSportFavAthleteTV, mSportFavTeamTV*/;

        private TextView mSkipTV, mHeightTV, mHeightTitleTV;
        public RelativeLayout mAthleteAboutSpecificRL, mCoachAboutSpecificRL, mAthleteSportsRL;
        public RelativeLayout mAlmaMeterRL, mMentorRL, mCoachingPhilosophyRL;

        public SetupProfileViewHolder(View view, OnClickListener listener) {

            mHeightTV = (TextView) view.findViewById(R.id.height_tv);
            mHeightTitleTV = (TextView) view.findViewById(R.id.height_label_tv);

            mHomeAddressET = (EditText) view.findViewById(R.id.home_address_et);
            mHomeAddressTV = (TextView) view.findViewById(R.id.home_address_tv);

            mPhoneNumberET = (EditText) view.findViewById(R.id.phone_number_et);
            mPhoneNumberTV = (TextView) view.findViewById(R.id.phone_number_tv);

            mAlternateEmailET = (EditText) view.findViewById(R.id.alternate_email_et);
            mAlternateEmailTV = (TextView) view.findViewById(R.id.alternate_email_tv);

            mParentNameET = (EditText) view.findViewById(R.id.parent_name_et);
            mParentNameTV = (TextView) view.findViewById(R.id.parent_name_tv);

            mParentHomeET = (EditText) view.findViewById(R.id.parent_home_et);
            mParentHomeTV = (TextView) view.findViewById(R.id.parent_home_tv);

            mParentEmailET = (EditText) view.findViewById(R.id.parent_email_et);
            mParentEmailTV = (TextView) view.findViewById(R.id.parent_email_tv);

            mCoachingPhilosophyET = (EditText) view.findViewById(R.id.coaching_philosophy_et);
            mCoachingPhilosophyTV = (TextView) view.findViewById(R.id.coaching_philosophy_tv);

            mMentorET = (EditText) view.findViewById(R.id.mentor_et);
            mMentorTV = (TextView) view.findViewById(R.id.mentor_tv);

            mAlmaMeterET = (EditText) view.findViewById(R.id.alma_meter_et1);
            mAlmaMeterTV = (TextView) view.findViewById(R.id.alma_meter_tv);

            mNCAAET = (EditText) view.findViewById(R.id.ncaa_et);
            mNCAATV = (TextView) view.findViewById(R.id.ncaa_tv);

            mFavAthleteET = (EditText) view.findViewById(R.id.fav_athlete_et);
            mFavAthleteTV = (TextView) view.findViewById(R.id.fav_athlete_tv);

            mFavTeamET = (EditText) view.findViewById(R.id.fav_team_et);
            mFavTeamTV = (TextView) view.findViewById(R.id.fav_team_tv);

            mFavMovieET = (EditText) view.findViewById(R.id.fav_movie_et);
            mFavMovieTV = (TextView) view.findViewById(R.id.fav_movie_tv);

            mFavMusicET = (EditText) view.findViewById(R.id.fav_music_et);
            mFavMusicTV = (TextView) view.findViewById(R.id.fav_music_tv);

            mFavFoodET = (EditText) view.findViewById(R.id.fav_food_et);
            mFavFoodTV = (TextView) view.findViewById(R.id.fav_food_tv);

            mFavTeacher = (EditText) view.findViewById(R.id.fav_teacher_et);
            mFavTeacherTV = (TextView) view.findViewById(R.id.fav_teacher_tv);

            mFavQuote = (EditText) view.findViewById(R.id.fav_quote_et);
            mFavQuoteTV = (TextView) view.findViewById(R.id.fav_quote_tv);

            mSportPositionET = (EditText) view.findViewById(R.id.position_et);
            mSportPositionTV = (TextView) view.findViewById(R.id.position_tv);

            mSportHomeTeamET = (EditText) view.findViewById(R.id.home_team_et);
            mSportHomeTeamTV = (TextView) view.findViewById(R.id.home_team_tv);

            mSportJerseyET = (EditText) view.findViewById(R.id.jersey_et);
            mSportJerseyTV = (TextView) view.findViewById(R.id.jersey_tv);

            mSportNameOfCurrentSchoolET = (EditText) view.findViewById(R.id.name_of_school_et);
            mSportNameOfCurrentSchoolTV = (TextView) view.findViewById(R.id.name_of_school_tv);

            mSportNameOfClubTeam = (EditText) view.findViewById(R.id.name_of_club_team_et);
            mSportNameOfClubTeamTV = (TextView) view.findViewById(R.id.name_of_club_team_tv);

            /*mSportFavAthlete = (EditText) view.findViewById(R.id.favorite_athlete_et);
            mSportFavAthleteTV = (TextView) view.findViewById(R.id.favorite_athlete_tv);

            mSportFavTeam = (EditText) view.findViewById(R.id.favorite_team_et);
            mSportFavTeamTV = (TextView) view.findViewById(R.id.favorite_team_tv);*/

            mCoachingPhilosophyRL = (RelativeLayout) view.findViewById(R.id.coaching_philosophy_rl);
            mMentorRL = (RelativeLayout) view.findViewById(R.id.mentor_rl);
            mAlmaMeterRL = (RelativeLayout) view.findViewById(R.id.alma_meter_rl);


            mAthleteAboutSpecificRL = (RelativeLayout) view.findViewById(R.id.athlete_more_about_unique_rl);
            mCoachAboutSpecificRL = (RelativeLayout) view.findViewById(R.id.coach_more_about_rl);
            mAthleteSportsRL = (RelativeLayout) view.findViewById(R.id.athlete_sports_rl);
            mSkipTV = (TextView) view.findViewById(R.id.skip_tv);

            mSkipTV.setOnClickListener(listener);
            mHeightTV.setOnClickListener(listener);
            mNCAAET.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showYearPicker();
                }
            });
            showFieldsAccordingToUserType();
        }
        public void showFieldsAccordingToUserType() {
            LogedInUserModel logedInUserModel = new LogedInUserModel(mContext);


            if (logedInUserModel.mUserType == 1) {
                mAthleteAboutSpecificRL.setVisibility(View.GONE);
                mCoachAboutSpecificRL.setVisibility(View.VISIBLE);
                mCoachingPhilosophyRL.setVisibility(View.VISIBLE);
                mMentorRL.setVisibility(View.VISIBLE);
                mAthleteSportsRL.setVisibility(View.GONE);
            } else if (logedInUserModel.mUserType == 2) {
                mAthleteAboutSpecificRL.setVisibility(View.VISIBLE);
                mCoachAboutSpecificRL.setVisibility(View.GONE);

            } else if (logedInUserModel.mUserType == 3) {
                mAthleteAboutSpecificRL.setVisibility(View.GONE);
                mCoachAboutSpecificRL.setVisibility(View.VISIBLE);
                mCoachingPhilosophyRL.setVisibility(View.GONE);
                mMentorRL.setVisibility(View.GONE);
                mAthleteSportsRL.setVisibility(View.GONE);
            }
        }
    }



    public void displayHeightDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.height_view);
        final Spinner mFeetSpinner = (Spinner) dialog.findViewById(R.id.feet_spinner);
        final Spinner mInchSpinner = (Spinner) dialog.findViewById(R.id.inch_spinner);
        TextView mDoneTV = (TextView) dialog.findViewById(R.id.done_tv);
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(mFeetSpinner);

            // Set popupWindow height to 500px
            popupWindow.setHeight(220);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(mInchSpinner);

            // Set popupWindow height to 500px
            popupWindow.setHeight(220);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        //set previously selected value
        String[] feetsArr = getResources().getStringArray(R.array.height_feet_array);
        for (int k = 0; k < feetsArr.length; k++) {
            if (feetsArr[k].equals(selectedFeet)) {
                mFeetSpinner.setSelection(k);
            }
        }

        String[] inchesArr = getResources().getStringArray(R.array.height_inches_array);
        for (int k = 0; k < inchesArr.length; k++) {
            if (inchesArr[k].equals(selectedInch)) {
                mInchSpinner.setSelection(k);
            }
        }

        mDoneTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                selectedFeet = (String) mFeetSpinner.getSelectedItem();
                selectedInch = (String) mInchSpinner.getSelectedItem();
                Log.v("selectedInch", selectedInch);
                Log.v("selectedFeet", selectedFeet);
                mSetupProfileViewHolder.mHeightTV.setText(selectedFeet + " " + selectedInch);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skip_tv:
                updateDataAndCallIntent();
                break;
            case R.id.height_tv:
                viewChangeDefaultColor();
                displayHeightDialog();
                break;
            default:
                break;
        }
    }

    private void setupProfileServerRequest(int userType, String profilePicName) {

        SetupProfileRequest mRequest = new SetupProfileRequest(mContext);
        String mFavAthlete = mSetupProfileViewHolder.mFavAthleteET.getText().toString().trim();
        String mFavTeam = mSetupProfileViewHolder.mFavTeamET.getText().toString().trim();
        String mFavMovie = mSetupProfileViewHolder.mFavMovieET.getText().toString().trim();
        String mFavMusic = mSetupProfileViewHolder.mFavMusicET.getText().toString().trim();
        String mFavFood = mSetupProfileViewHolder.mFavFoodET.getText().toString().trim();
        String mFavTeacher = mSetupProfileViewHolder.mFavTeacher.getText().toString().trim();
        String mFavQuote = mSetupProfileViewHolder.mFavQuote.getText().toString().trim();

        BaseRequest.setLoginSessionKey(mLogedInUserModel.mLoginSessionKey);

        if (userType == 1) {
            String mCoachingPhilosophy = mSetupProfileViewHolder.mCoachingPhilosophyET.getText().toString().trim();
            String mMentor = mSetupProfileViewHolder.mMentorET.getText().toString().trim();
            String mAlmaMeter = mSetupProfileViewHolder.mAlmaMeterET.getText().toString().trim();

            mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
                    mSelectedStateId, mSelectedSportsId, mUserCurrentSchool, mUserCoachingTitle,
                    mCoachingPhilosophy, mMentor, mAlmaMeter, mFavAthlete, mFavTeam, mFavMovie,
                    mFavMusic, mFavFood, mFavTeacher, profilePicName);

        } else if (userType == 2) {

            String mSportFavAthleteString = mSetupProfileViewHolder.mFavAthleteET.getText().toString().trim();
            String mSportFavTeamString = mSetupProfileViewHolder.mFavTeamET.getText().toString().trim();
            String mPosition = mSetupProfileViewHolder.mSportPositionET.getText().toString().trim();
            String mHomeTeam = mSetupProfileViewHolder.mSportHomeTeamET.getText().toString().trim();
            String mJerseyNumber = mSetupProfileViewHolder.mSportJerseyET.getText().toString().trim();
            String mNameOfCurrentSchool = mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.getText().toString().trim();
            String mNameOfClubTeam = mSetupProfileViewHolder.mSportNameOfClubTeam.getText().toString().trim();
            String mHomeAdress = mSetupProfileViewHolder.mHomeAddressET.getText().toString().trim();
            String mPhoneNumber = mSetupProfileViewHolder.mPhoneNumberET.getText().toString().trim();
            String mAlternateEmail = mSetupProfileViewHolder.mAlternateEmailET.getText().toString().trim();
            String mNCAAEligibility = mSetupProfileViewHolder.mNCAAET.getText().toString().trim();
            String mInches = String.valueOf(Utility.convertFeetAndInchToInches(selectedFeet, selectedInch));
            String mParentID = "";
            String mParentName = mSetupProfileViewHolder.mParentNameET.getText().toString().trim();
            String mParentEmail = mSetupProfileViewHolder.mParentEmailET.getText().toString().trim();

            if (!TextUtils.isEmpty(mParentEmail)) {
                if (ValidationUtil.isValidEmail(mParentEmail)) {
                    String mParentPhone = mSetupProfileViewHolder.mParentHomeET.getText().toString().trim();
                    String mParentRelation = "";

                    mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB,
                            String.valueOf(mUserGender), mSelectedStateId, mSelectedSportsId, mPosition,
                            mHomeTeam, mJerseyNumber, mNameOfCurrentSchool, mNameOfClubTeam,
                            mSportFavAthleteString, mSportFavTeamString, mFavMovie, mFavMusic,
                            mFavFood, mFavTeacher, profilePicName, mFavQuote, mHomeAdress,
                            mPhoneNumber, mAlternateEmail, mUserModel.mUserGPAScore, mUserModel.mUserACTScore,
                            mUserModel.mUserACTScore, "", mNCAAEligibility, mParentID,
                            mParentName, mParentEmail, mParentPhone, mParentRelation, mInches);
                } else {
                    DialogUtil.showOkDialogUncancelableButtonLisnter(mContext, getResources().getString(R.string.parents_email_invalid),
                            getResources().getString(R.string.app_name), new OnOkButtonListner() {
                                @Override
                                public void onOkBUtton() {
                                    mSetupProfileViewHolder.mParentEmailET.requestFocus();
                                    mSetupProfileViewHolder.mParentEmailET.setSelection(mSetupProfileViewHolder
                                            .mParentEmailET.getText().toString().length());
                                }
                            });
                }
            } else {
                String mParentPhone = mSetupProfileViewHolder.mParentHomeET.getText().toString().trim();
                String mParentRelation = "";

                mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB, String.valueOf(mUserGender),
                        mSelectedStateId, mSelectedSportsId, mPosition, mHomeTeam, mJerseyNumber, mNameOfCurrentSchool, mNameOfClubTeam,
                        mSportFavAthleteString, mSportFavTeamString, mFavMovie, mFavMusic, mFavFood, mFavTeacher, profilePicName,
                        mFavQuote, mHomeAdress, mPhoneNumber, mAlternateEmail, mUserModel.mUserGPAScore, mUserModel.mSATScore,
                        mUserModel.mUserACTScore, "", mNCAAEligibility, mParentID,
                        mParentName, mParentEmail, mParentPhone, mParentRelation, mInches);
            }
        } else if (userType == 3) {
            mRequest.SetupProfileSeverRequest(mUserFullName, mUserName, mUserEmail, mUserDOB,
                    String.valueOf(mUserGender), mSelectedStateId, mFavAthlete, mFavTeam, mFavMovie, mFavMusic,
                    mFavFood, mFavTeacher, profilePicName, mFavQuote);
        }

        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {

                    mDisplayDialog = DialogUtil.showOkDialogUncancelableButtonLisnter(mContext, data.toString(),
                            getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {
                                @Override
                                public void onOkBUtton() {
                                    updateDataAndCallIntent();
                                }
                            });

                    mDisplayDialog.setOnKeyListener(new OnKeyListener() {
                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent arg2) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                mDisplayDialog.dismiss();
                                updateDataAndCallIntent();
                            }
                            return false;
                        }
                    });

                } else {

                    DialogUtil.showOkDialogUncancelableButtonLisnter(mContext, data.toString(),
                            getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {
                                @Override
                                public void onOkBUtton() {

                                }
                            });
                }
            }
        });
    }

    private void updateDataAndCallIntent() {

        Intent intent = null;
        if (mReqType.equals("2")) {
//            Intent in = AboutActivity.getIntent(mContext, mUserName, "SetupProfileMandatoryActivity");
//            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(in);
            if (isProfileSetup) {
                intent = DashboardActivity.getIntent(mContext, DashboardActivity.HOME_FRAGMENTINDEX, "");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                finish();
            }
        } else {
            intent = DashboardActivity.getIntent(mContext, DashboardActivity.HOME_FRAGMENTINDEX, "");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        finish();
    }

    private void setLocalPreferance() {

        mLocalPreferance = getSharedPreferences("LocalPreferance", 0);
        Editor mLocalEditor = mLocalPreferance.edit();
        mLocalEditor.putBoolean("isUsed", true);
        mLocalEditor.putString("mHomeAddressET", mSetupProfileViewHolder.mHomeAddressET.getText().toString());
        mLocalEditor.putString("mPhoneNumberET", mSetupProfileViewHolder.mPhoneNumberET.getText().toString());
        mLocalEditor.putString("mAlternateEmailET", mSetupProfileViewHolder.mAlternateEmailET.getText().toString());
        mLocalEditor.putString("mParentNameET", mSetupProfileViewHolder.mParentNameET.getText().toString());
        mLocalEditor.putString("mParentHomeET", mSetupProfileViewHolder.mParentHomeET.getText().toString());
        mLocalEditor.putString("mParentEmailET", mSetupProfileViewHolder.mParentEmailET.getText().toString());
        mLocalEditor.putString("mCoachingPhilosophyET", mSetupProfileViewHolder.mCoachingPhilosophyET.getText().toString());
        mLocalEditor.putString("mMentorET", mSetupProfileViewHolder.mMentorET.getText().toString());
        mLocalEditor.putString("mAlmaMeterET", mSetupProfileViewHolder.mAlmaMeterET.getText().toString());
        /*mLocalEditor.putString("mAthleteGPAET", mSetupProfileViewHolder.mAthleteGPAET.getText().toString());
        mLocalEditor.putString("mAtheleteSATET", mSetupProfileViewHolder.mAtheleteSATET.getText().toString());
		mLocalEditor.putString("mAthleteACTET", mSetupProfileViewHolder.mAthleteACTET.getText().toString());
		mLocalEditor.putString("mAthleteTranscriptsET", mSetupProfileViewHolder.mAthleteTranscriptsET.getText().toString());*/
        mLocalEditor.putString("mFavAthleteET", mSetupProfileViewHolder.mFavAthleteET.getText().toString());
        mLocalEditor.putString("mFavTeamET", mSetupProfileViewHolder.mFavTeamET.getText().toString());
        mLocalEditor.putString("mFavMovieET", mSetupProfileViewHolder.mFavMovieET.getText().toString());
        mLocalEditor.putString("mFavMusicET", mSetupProfileViewHolder.mFavMusicET.getText().toString());
        mLocalEditor.putString("mFavFoodET", mSetupProfileViewHolder.mFavFoodET.getText().toString());
        mLocalEditor.putString("mFavTeacher", mSetupProfileViewHolder.mFavTeacher.getText().toString());
        mLocalEditor.putString("mFavQuote", mSetupProfileViewHolder.mFavQuote.getText().toString());
        mLocalEditor.putString("mSportPositionET", mSetupProfileViewHolder.mSportPositionET.getText().toString());
        mLocalEditor.putString("mSportHomeTeamET", mSetupProfileViewHolder.mSportHomeTeamET.getText().toString());
        mLocalEditor.putString("mSportJerseyET", mSetupProfileViewHolder.mSportJerseyET.getText().toString());
        mLocalEditor.putString("mSportNameOfCurrentSchoolET", mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.getText().toString());
        mLocalEditor.putString("mSportNameOfClubTeam", mSetupProfileViewHolder.mSportNameOfClubTeam.getText().toString());
        mLocalEditor.putString("mSportFavAthlete", mSetupProfileViewHolder.mFavAthleteET.getText().toString());
        mLocalEditor.putString("mSportFavTeam", mSetupProfileViewHolder.mFavTeamET.getText().toString());
        mLocalEditor.putString("mHeightTV", mSetupProfileViewHolder.mHeightTV.getText().toString());
        mLocalEditor.putString("mNCAAET", mSetupProfileViewHolder.mNCAAET.getText().toString());
        mLocalEditor.commit();
    }

    private boolean isLocalPreferance() {
        mLocalPreferance = getSharedPreferences("LocalPreferance", 0);
        return mLocalPreferance.getBoolean("isUsed", false);
    }

    private void resetLocalPreferance() {
        mLocalPreferance = getSharedPreferences("LocalPreferance", 0);
        Editor mLocalEditor = mLocalPreferance.edit();
        mLocalEditor.putBoolean("isUsed", false);
        mLocalEditor.clear();
        mLocalEditor.commit();
    }

    private void getFromLocalPreferance() {

        mLocalPreferance = getSharedPreferences("LocalPreferance", 0);

        mSetupProfileViewHolder.mHomeAddressET.setText(mLocalPreferance.getString("mHomeAddressET", mUserModel.mUserHomeAddress).trim());
        mSetupProfileViewHolder.mPhoneNumberET.setText(mLocalPreferance.getString("mPhoneNumberET", mUserModel.mUserPhoneNumber).trim());
        mSetupProfileViewHolder.mAlternateEmailET.setText(mLocalPreferance.getString("mAlternateEmailET", mUserModel.mUserAlternateEmail).trim());
        mSetupProfileViewHolder.mParentNameET.setText(mLocalPreferance.getString("mParentNameET", mUserModel.mUserParentName).trim());
        mSetupProfileViewHolder.mParentHomeET.setText(mLocalPreferance.getString("mParentHomeET", mUserModel.mUserParentPhoneNumber).trim());
        mSetupProfileViewHolder.mParentEmailET.setText(mLocalPreferance.getString("mParentEmailET", mUserModel.mUserParentEmail).trim());
        mSetupProfileViewHolder.mCoachingPhilosophyET.setText(mLocalPreferance.getString("mCoachingPhilosophyET", mUserModel.mUserCoachingPhilosophy).trim());
        mSetupProfileViewHolder.mMentorET.setText(mLocalPreferance.getString("mMentorET", mUserModel.mUserMentor).trim());
        mSetupProfileViewHolder.mAlmaMeterET.setText(mLocalPreferance.getString("mAlmaMeterET", mUserModel.mUserAlmaMater).trim());
        mSetupProfileViewHolder.mFavAthleteET.setText(mLocalPreferance.getString("mFavAthleteET", mUserModel.mUserFavAthelet).trim());
        mSetupProfileViewHolder.mFavTeamET.setText(mLocalPreferance.getString("mFavTeamET", mUserModel.mUserFavTeam).trim());
        mSetupProfileViewHolder.mFavMovieET.setText(mLocalPreferance.getString("mFavMovieET", mUserModel.mUserFavMovie).trim());

        mSetupProfileViewHolder.mFavMusicET.setText(mLocalPreferance.getString("mFavMusicET", mUserModel.mUserFavMusic).trim());
        mSetupProfileViewHolder.mFavFoodET.setText(mLocalPreferance.getString("mFavFoodET", mUserModel.mUserFavFood).trim());
        mSetupProfileViewHolder.mFavTeacher.setText(mLocalPreferance.getString("mFavTeacher", mUserModel.mUserFavTeacher).trim());
        mSetupProfileViewHolder.mFavQuote.setText(mLocalPreferance.getString("mFavQuote", mUserModel.mUserFavQuote).trim());
        mSetupProfileViewHolder.mSportPositionET.setText(mLocalPreferance.getString("mSportPositionET", mUserModel.mUserPosition).trim());
        mSetupProfileViewHolder.mSportHomeTeamET.setText(mLocalPreferance.getString("mSportHomeTeamET", mUserModel.mUserHomeTeam).trim());
        mSetupProfileViewHolder.mSportJerseyET.setText(mLocalPreferance.getString("mSportJerseyET", mUserModel.mUserJerseyNumber).trim());
        mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setText(mLocalPreferance.getString("mSportNameOfCurrentSchoolET", mUserModel.mUserCurrentSchool).trim());
        mSetupProfileViewHolder.mSportNameOfClubTeam.setText(mLocalPreferance.getString("mSportNameOfClubTeam", mUserModel.mUserClubTeam).trim());
        /*mSetupProfileViewHolder.mSportFavAthlete.setText(mLocalPreferance.getString("mSportFavAthlete", mUserModel.mUserFavAthelet).trim());
        mSetupProfileViewHolder.mSportFavTeam.setText(mLocalPreferance.getString("mSportFavTeam", mUserModel.mUserFavTeam).trim());*/
        mSetupProfileViewHolder.mHeightTV.setText(mLocalPreferance.getString("mHeightTV", mUserModel.mUserHeight).trim());
        mSetupProfileViewHolder.mNCAAET.setText(mLocalPreferance.getString("mNCAAET", mUserModel.mUserNCAAEligibility).trim());
        resetLocalPreferance();
        setCursor();
    }

    private void showYearPicker() {
        try {
            final Calendar c = Calendar.getInstance();
            int year = 2015;
            int y = c.get(Calendar.YEAR);
            int changCount = 9;
            int totalYears = y - year + changCount;

            int total = 0;

            String years[] = new String[totalYears];

            for (int i = 0; i < totalYears; i++) {
                int mYear = year + i;
                if (y == mYear) {
                    break;
                }
                total = i;
                years[i] = String.valueOf(mYear);
            }
            for (int j = 0; j < changCount; j++) {
                int cYear = y + j;
                years[total + j + 1] = String.valueOf(cYear);
            }
            DialogUtil.showListDialog(mContext, 0, new DialogUtil.OnItemClickListener() {
                @Override
                public void onItemClick(int position, String item) {
                    if (!TextUtils.isEmpty(item))
                        mSetupProfileViewHolder.mNCAAET.setText(item);
                }

                @Override
                public void onCancel() {

                }
            }, years);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("ResourceAsColor")
    private void viewListnres() {

        mSetupProfileViewHolder.mHomeAddressET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mHomeAddressTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mHomeAddressET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mHomeAddressTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mHomeAddressET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mSetupProfileViewHolder.mPhoneNumberET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mPhoneNumberTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mPhoneNumberET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mPhoneNumberTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mPhoneNumberET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mAlternateEmailET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mAlternateEmailTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mAlternateEmailET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mAlternateEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mAlternateEmailET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mParentNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mParentNameTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mParentNameET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mParentNameTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mParentNameET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mParentHomeET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mParentHomeTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mParentHomeET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mParentHomeTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mParentHomeET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mParentEmailET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mParentEmailTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mParentEmailET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mParentEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mParentEmailET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mCoachingPhilosophyET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mCoachingPhilosophyTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mCoachingPhilosophyET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mCoachingPhilosophyTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mCoachingPhilosophyET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mMentorET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mMentorTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mMentorET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mMentorTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mMentorET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mAlmaMeterET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mAlmaMeterTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mAlmaMeterET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mAlmaMeterTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mAlmaMeterET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mNCAAET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mNCAATV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mNCAAET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mNCAATV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mNCAAET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mFavAthleteET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mFavAthleteTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mFavAthleteET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mFavAthleteTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mFavAthleteET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mFavTeamET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mFavTeamTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mFavTeamET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mFavTeamTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mFavTeamET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mFavMovieET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mFavMovieTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mFavMovieET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mFavMovieTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mFavMovieET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mFavMusicET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mFavMusicTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mFavMusicET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mFavMusicTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mFavMusicET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mFavFoodET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mFavFoodTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mFavFoodET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mFavFoodTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mFavFoodET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mFavTeacher.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mFavTeacherTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mFavTeacher.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mFavTeacherTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mFavTeacher.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mFavQuote.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mFavQuoteTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mFavQuote.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mFavQuoteTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mFavQuote.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mSportPositionET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mSportPositionTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mSportPositionET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mSportPositionTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mSportPositionET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mSportHomeTeamET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mSportHomeTeamTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mSportHomeTeamET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mSportHomeTeamTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mSportHomeTeamET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mSportJerseyET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mSportJerseyTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mSportJerseyET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mSportJerseyTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mSportJerseyET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mSportNameOfCurrentSchoolTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mSportNameOfCurrentSchoolTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mSportNameOfClubTeam.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mSportNameOfClubTeamTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mSportNameOfClubTeam.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mSportNameOfClubTeamTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mSportNameOfClubTeam.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        /*mSetupProfileViewHolder.mSportFavAthlete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mSetupProfileViewHolder.mSportFavAthleteTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mSportFavAthlete.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mSportFavAthleteTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mSportFavAthlete.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });
        mSetupProfileViewHolder.mSportFavTeam.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {

                if (hasFocus) {
                    mSetupProfileViewHolder.mSportFavTeamTV.setTextColor(Color.parseColor("#01579B"));
                    mSetupProfileViewHolder.mSportFavTeam.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mSetupProfileViewHolder.mSportFavTeamTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mSetupProfileViewHolder.mSportFavTeam.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });*/

    }

    private void changeColorOnView() {

        mSetupProfileViewHolder.mHeightTitleTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mHeightTV.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mHeightTV.setBackgroundResource(R.drawable.ic_gray_line);
    }

    private void viewChangeDefaultColor() {

        mSetupProfileViewHolder.mHeightTitleTV.setTextColor(Color.parseColor("#01579B"));
        mSetupProfileViewHolder.mHeightTV.setTextColor(Color.parseColor("#000000"));
        mSetupProfileViewHolder.mHeightTV.setBackgroundResource(R.drawable.ic_blue_line);
        mSetupProfileViewHolder.mHomeAddressTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mHomeAddressET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mHomeAddressET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mPhoneNumberTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mPhoneNumberET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mPhoneNumberET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mAlternateEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mAlternateEmailET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mAlternateEmailET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mParentNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mParentNameET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mParentNameET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mParentHomeTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mParentHomeET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mParentHomeET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mParentEmailTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mParentEmailET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mParentEmailET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mCoachingPhilosophyTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mCoachingPhilosophyET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mCoachingPhilosophyET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mMentorTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mMentorET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mMentorET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mAlmaMeterTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mAlmaMeterET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mAlmaMeterET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mNCAATV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mNCAAET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mNCAAET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mFavAthleteTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mFavAthleteET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mFavAthleteET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mFavTeamTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mFavTeamET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mFavTeamET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mFavMovieTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mFavMovieET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mFavMovieET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mFavMusicTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mFavMusicET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mFavMusicET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mFavFoodTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mFavFoodET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mFavFoodET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mFavTeacherTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mFavTeacher.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mFavTeacher.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mFavQuoteTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mFavQuote.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mFavQuote.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mSportPositionTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mSportPositionET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mSportPositionET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mSportHomeTeamTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mSportHomeTeamET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mSportHomeTeamET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mSportJerseyTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mSportJerseyET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mSportJerseyET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mSportNameOfCurrentSchoolTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mSportNameOfCurrentSchoolET.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mSportNameOfClubTeamTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mSportNameOfClubTeam.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mSportNameOfClubTeam.setBackgroundResource(R.drawable.ic_gray_line);
        /*mSetupProfileViewHolder.mSportFavAthleteTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mSportFavAthlete.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mSportFavAthlete.setBackgroundResource(R.drawable.ic_gray_line);
        mSetupProfileViewHolder.mSportFavTeamTV.setTextColor(Color.parseColor("#cfcfcf"));
        mSetupProfileViewHolder.mSportFavTeam.setTextColor(Color.parseColor("#666666"));
        mSetupProfileViewHolder.mSportFavTeam.setBackgroundResource(R.drawable.ic_gray_line);*/

    }
}