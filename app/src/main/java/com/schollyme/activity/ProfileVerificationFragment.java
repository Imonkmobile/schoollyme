package com.schollyme.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.VerifyAccountRequest;
import com.vinfotech.request.VerifyProfileRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;

/**
 * Activity class. This may be useful in Verify Account after signup
 * process complete and before first login
 *
 * @author Ravi Bhandari
 */
public class ProfileVerificationFragment extends BaseFragment {

    private Context mContext;
    private Button verifiedProfileBtn;
    private ImageView verifiedLikeImg;
    private TextView login_header_tv;
    private ErrorLayout mErrorLayout;
    private String request_type = "2"; // sent request
    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;

    public static BaseFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        ProfileVerificationFragment mFragment = new ProfileVerificationFragment();
        mFragment.mCurrentPageToShow = currentPageToShow;
        mFragment.mHeaderLayout = headerLayout;
        mFragment.mShowBackBtn = showBack;
        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_verification_fragment, container, false);
        mContext = getActivity();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initHeader();

        verifiedLikeImg = (ImageView) view.findViewById(R.id.verifiedLikeImg);
        login_header_tv = (TextView) view.findViewById(R.id.login_header_tv);
        verifiedLikeImg.setImageResource(R.drawable.ic_profile_unverified);
        verifiedProfileBtn = (Button) view.findViewById(R.id.verifiedProfileBtn);
        verifiedProfileBtn.setOnClickListener(VerifyAccoutListener);

        mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_rl));

        LogedInUserModel mLogedInUserModel = new LogedInUserModel(mContext);
        if (mLogedInUserModel.isProfileVerified.equals("1")) {
            verifiedLikeImg.setImageResource(R.drawable.ic_profile_verified);
            login_header_tv.setTextColor(Color.parseColor("#01579B"));
            login_header_tv.setText(getResources().getString(R.string.verified));
            verifiedProfileBtn.setVisibility(View.GONE);
        } else {
            verifiedProfileBtn.setVisibility(View.VISIBLE);
            verifiedLikeImg.setImageResource(R.drawable.ic_profile_unverified);
            login_header_tv.setTextColor(Color.parseColor("#999999"));
            login_header_tv.setText(getResources().getString(R.string.not_yet_verified_label));
        }
    }

    private void initHeader() {

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                getResources().getString(R.string.profile_verification), 0);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    ((DashboardActivity) getActivity()).sliderListener();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        }, null);
    }

    /**
     * Click listener listen for user click event
     */
    private OnClickListener VerifyAccoutListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            showDialogVerificationConfirm();
        }
    };

    private void showDialogVerificationConfirm() {

        DialogUtil.showTwoButtonDialogLisnter(mContext, getResources().getString(R.string.request_label),
                getResources().getString(R.string.cancel_caps),
                getResources().getString(R.string.request_verification_confirm),
                getResources().getString(R.string.verify_label),
                new OnOkButtonListner() {
                    @Override
                    public void onOkBUtton() {
                        activateAccountServerRequest();
                    }
                }, new OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
    }

    /**
     * Server request for  verify account with verification token
     */
    private void activateAccountServerRequest() {

        VerifyProfileRequest mRequest = new VerifyProfileRequest(mContext);
        mRequest.activateProfileServerRequest(request_type);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    showDialogVerifiedSuccessfully();
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
    }

    private void showDialogVerifiedSuccessfully() {

        DialogUtil.showSingleButtonDialogLisnter(mContext, getResources().getString(R.string.ok_label),
                getResources().getString(R.string.request_sent_verification),
                getResources().getString(R.string.verify_label),
                new OnOkButtonListner() {
                    @Override
                    public void onOkBUtton() {
                        // showing verified screen
                        login_header_tv.setTextColor(Color.parseColor("#0084FF"));
                        login_header_tv.setText(getResources().getString(R.string.request_sent));
                        verifiedProfileBtn.setVisibility(View.GONE);
                    }
                });
    }
}