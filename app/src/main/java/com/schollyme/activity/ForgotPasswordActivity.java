package com.schollyme.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.vinfotech.request.ForgotPasswordRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;
import com.vinfotech.utility.ValidationUtil;

/**
 * Activity class. This may be useful in Recover Password of app user
 *
 * @author Ravi Bhandari
 */
public class ForgotPasswordActivity extends BaseActivity implements OnClickListener {

    private EditText mEmailET;
    private TextView mErrorMessageTV, mHeaderMessageTV;
    private String mEmailStr;

    private Context mContext;
    private ErrorLayout mErrorLayout;
    private boolean isDialogShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpassword_activity);
        mContext = this;

        initHeader();
        controlInitialization();

        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        mEmailET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validateFields()) {
                        forgotPasswordRequest();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void initHeader() {

        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
                getResources().getString(R.string.forgot_password_header),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utility.hideSoftKeyboard(mEmailET);
                        finish();
                    }
                }, ForgotPasswordListener);
    }

    OnClickListener ForgotPasswordListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (validateFields()) {
                forgotPasswordRequest();
            }
        }
    };

    private void controlInitialization() {
        mEmailET = (EditText) findViewById(R.id.email_et);
        mEmailET.setText(getIntent().getStringExtra("user_input"));
        mErrorMessageTV = (TextView) findViewById(R.id.error_message_tv);
        mHeaderMessageTV = (TextView) findViewById(R.id.login_header_tv);
        mEmailET.requestFocus();
        FontLoader.setRobotoRegularTypeface(mHeaderMessageTV, mEmailET, mErrorMessageTV);
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, ForgotPasswordActivity.class);
        return intent;
    }

    /**
     * Validate all require fields
     */
    private boolean validateFields() {
        mEmailStr = mEmailET.getText().toString();
        if (TextUtils.isEmpty(mEmailStr)) {
            // displayErrorMessage(getResources().getString(R.string.enter_email_label));
            mErrorLayout.showError(getResources().getString(R.string.enter_email_label), true, MsgType.Error);
        } else if (!ValidationUtil.isValidEmail(mEmailStr)) {
            mErrorLayout.showError(getResources().getString(R.string.email_notvalid_message), true, MsgType.Error);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onBackPressed() {
        if (isDialogShowing) {
            Intent intent = ResetPasswordActivity.getIntent(mContext);
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Make server rquest for forgot password user request
     */
    private void forgotPasswordRequest() {
        ForgotPasswordRequest mForgotPasswordRequest = new ForgotPasswordRequest(mContext);
        mForgotPasswordRequest.ForgetPasswordServerRequest(mEmailStr);
        mForgotPasswordRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    isDialogShowing = true;
                    DialogUtil.showOkCancelDialogButtonLisnter(mContext, getResources().getString(R.string.forgot_password_email), getResources()
                            .getString(R.string.app_name), new OnOkButtonListner() {

                        @Override
                        public void onOkBUtton() {
                            Intent intent = ResetPasswordActivity.getIntent(mContext);
                            startActivity(intent);
                            finish();
                        }

                    }, new OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            Intent intent = ResetPasswordActivity.getIntent(mContext);
                            startActivity(intent);
                            finish();
                        }
                    });

                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                    //	DialogUtil.showOkDialog(mContext, data.toString(), getResources().getString(R.string.app_name));
                }
            }
        });
    }
}