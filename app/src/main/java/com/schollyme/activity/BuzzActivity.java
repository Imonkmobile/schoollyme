package com.schollyme.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.R;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.adapter.PostFeedAdapter.OnItemClickListenerPost;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.vinfotech.utility.Config;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

public class BuzzActivity extends BaseActivity {
	private static final String TAG = BuzzActivity.class.getSimpleName();

	private Context mContext;
	private ErrorLayout mErrorLayout;
	private HeaderLayout mHeaderLayout;
	private String mUserGUID,mFriendsName;
	private LogedInUserModel mLogedinUser;
	private boolean isShareAllow = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.buzz_activity);

		mContext = this;
		mUserGUID = getIntent().getStringExtra("UserGUID");
		mFriendsName = getIntent().getStringExtra("FriendsName");
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
		mHeaderLayout = new HeaderLayout(findViewById(R.id.header_layout));
		mLogedinUser = new LogedInUserModel(mContext);
		if(mLogedinUser.mUserGUID.equals(mUserGUID)){
			mHeaderLayout.setHeaderValues(R.drawable.icon_back, getString(R.string.My_Buzz), 0);
		}
		else{
			isShareAllow = true;
			mHeaderLayout.setHeaderValues(R.drawable.icon_back, mFriendsName, 0);
		}
		
		
		mHeaderLayout.setListenerItI(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		}, null);

		initFeedRequest();
	}

	public static Intent getIntent(Context mContext, String UserGUID,String mFriendsName) {
		Intent intent = new Intent(mContext, BuzzActivity.class);
		intent.putExtra("UserGUID", UserGUID);
		intent.putExtra("FriendsName", mFriendsName);
		return intent;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		mNewsFeedRequest.setActivityStatus(true);

		switch (requestCode) {
		case LikeListActivity.REQ_FROM_HOME_LIKELIST:
			if (resultCode == RESULT_OK) {
				if (data != null) {
					mNewsFeeds.get(ClickLocation).NoOfLikes = data.getIntExtra("COUNT", 0);
					mNewsFeeds.get(ClickLocation).IsLike = data.getIntExtra("BUTTONSTATUS", 0);
					if(isShareAllow){
						mPostFeedAdapter.setList(mNewsFeeds, true);
					}
					else{
						mPostFeedAdapter.setList(mNewsFeeds, false);	
					}
					
				}
			}
			break;
		case CommentListActivity.REQ_CODE_COMMENT_LIST:
			if (resultCode == RESULT_OK) {
				if (data != null) {
					mNewsFeeds.get(ClickLocation).NoOfComments = data.getIntExtra("COUNT", 0);
				//	mPostFeedAdapter.setList(mNewsFeeds, false);
					if(isShareAllow){
						mPostFeedAdapter.setList(mNewsFeeds, true);
					}
					else{
						mPostFeedAdapter.setList(mNewsFeeds, false);	
					}
				}
			}
			break;

		case CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY:
			if (resultCode == RESULT_OK) {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						mNFFilter.PageNo = 1;
						getFeedRequest(mNFFilter);
					}
				}, 500);
			}
			break;

		case WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY:
			if (resultCode == RESULT_OK) {
				mNewsFeeds.get(ClickLocation).NoOfComments++;
				//mPostFeedAdapter.setList(mNewsFeeds, false);
				if(isShareAllow){
					mPostFeedAdapter.setList(mNewsFeeds, true);
				}
				else{
					mPostFeedAdapter.setList(mNewsFeeds, false);	
				}
			}

			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void resetList() {
		mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
		mNewsFeeds.clear();
		// mNewsFeeds.add(new NewsFeed(null));
	//	mPostFeedAdapter.setList(mNewsFeeds, false);
		if(isShareAllow){
			mPostFeedAdapter.setList(mNewsFeeds, true);
		}
		else{
			mPostFeedAdapter.setList(mNewsFeeds, false);	
		}
	}

	private View mFooterRL;
	private TextView mNoMoreTV;
	private TextView mNoFeedTV;
	private ProgressBar mLoaderBottomPB, mLoadingCenter;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private ListView mGnericLv;

	private PostFeedAdapter mPostFeedAdapter;
	private NFFilter mNFFilter;
	private List<NewsFeed> mNewsFeeds = new ArrayList<NewsFeed>();

	private NewsFeedRequest mNewsFeedRequest;

	private void initFeedRequest() {
		mNFFilter = new NFFilter(NewsFeedRequest.MODULE_ID_USERS, mUserGUID);
		mNFFilter.MyBuzz = true;
		mPostFeedAdapter = new PostFeedAdapter(mContext, new OnItemClickListenerPost() {

			@Override
			public void onClickItems(int ID, int position, NewsFeed newsFeed) {
				onItemClick(ID, position, newsFeed);
			}
		}, mUserGUID);
		mPostFeedAdapter.setWriteEnabled(false, false);

		mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);

		mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
		mNoMoreTV.setVisibility(View.INVISIBLE);

		mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
		mLoaderBottomPB.setVisibility(View.INVISIBLE);
		mLoaderBottomPB.getIndeterminateDrawable()
				.setColorFilter(getResources().getColor(R.color.red_normal_color), PorterDuff.Mode.SRC_IN);

		mNoFeedTV = (TextView) findViewById(R.id.no_feed_tv);
		FontLoader.setRobotoRegularTypeface(mNoMoreTV, mNoFeedTV);

		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.pull_to_refresh_srl);
		mSwipeRefreshWidget.setColorScheme(R.color.red_normal_color);
		mSwipeRefreshWidget.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				mSwipeRefreshWidget.setRefreshing(true);
				getFeedRequest(mNFFilter);
			}
		});

		mGnericLv = (ListView) findViewById(R.id.genric_lv);
		mGnericLv.addFooterView(mFooterRL);
		mGnericLv.setAdapter(mPostFeedAdapter);
		mGnericLv.setOnScrollListener(mOnScrollListener);

		getFeedRequest(mNFFilter);
	}

	int ClickLocation = 0;

	public void onItemClick(int ID, int position, NewsFeed newsFeed) {
		ClickLocation = position;
		switch (ID) {
		case R.id.share_tv:
			if (newsFeed.ShareAllowed == 1) {
				sharePost(newsFeed);
			} else {
				Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
			}
			break;

		case R.id.like_tv:
			likeMediaToggleService(newsFeed, position);
			break;
		case R.id.likes_tv:
			if (newsFeed.NoOfLikes != 0) {
				startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID, "ACTIVITY", "WALL"),
						LikeListActivity.REQ_FROM_HOME_LIKELIST);
				overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
			}
			break;

		case R.id.comment_tv: {
			AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
			WriteCommentActivity.addFullPathIfNeeded(newsFeed, albumMedia);
			String captionForWriteCommentScreen = "";
			if (!newsFeed.PostContent.equals("")) {
				captionForWriteCommentScreen = newsFeed.PostContent;
			} else {

				if (!newsFeed.Album.AlbumMedias.get(0).ImageName.equals("")) {

					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).Caption;
				} else {
					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
				}

			}

			startActivityForResult(WriteCommentActivity.getIntent(this, albumMedia, "ACTIVITY", captionForWriteCommentScreen),
					WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY);
			overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
		}
			break;
		case R.id.comments_tv:

			AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
			WriteCommentActivity.addFullPathIfNeeded(newsFeed, albumMedia);
			String captionForWriteCommentScreen = "";
			if (!newsFeed.PostContent.equals("")) {
				captionForWriteCommentScreen = newsFeed.PostContent;
			} else {

				if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
					captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
				}

			}

			startActivityForResult(CommentListActivity.getIntent(mContext, albumMedia, "WALLPOST", captionForWriteCommentScreen),
					CommentListActivity.REQ_CODE_COMMENT_LIST);
			overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

			break;

		case PostFeedAdapter.ConvertViewID:
			startActivityForResult(CreateWallPostActivity.getIntent(mContext, WallPostCreateRequest.MODULE_ID_USERS, mUserGUID),
					CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
			break;
		default:
			break;
		}
	}

	public void sharePost(NewsFeed newsFeedModel) {
		SharePostRequest mSharePostRequest = new SharePostRequest(mContext);

		mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
				NewsFeedRequest.MODULE_ID_USERS, mUserGUID, 1, 1);
		mSharePostRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {
					mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, MsgType.Success);
				} else {
					mErrorLayout.showError((String) data, true, MsgType.Error);
				}
			}
		});
	}

	public void likeMediaToggleService(final NewsFeed newsFeed, final int position) {
		ToggleLikeRequest mToggleLikeRequest = new ToggleLikeRequest(mContext);
		mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");

		mToggleLikeRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

				if (success) {

					if (newsFeed.IsLike == 0) {

						newsFeed.IsLike = 1;
						newsFeed.NoOfLikes++;
					} else {

						newsFeed.IsLike = 0;
						newsFeed.NoOfLikes--;
					}
					mPostFeedAdapter.notifyDataSetChanged();

				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), "");
				}

			}
		});
	}

	private void getFeedRequest(final NFFilter nffilter) {
		if (null == mNewsFeedRequest) {
			mNewsFeedRequest = new NewsFeedRequest(mContext);
		}
		mNewsFeedRequest.getNewsFeedListInServer(nffilter);
		mNoMoreTV.setVisibility(View.INVISIBLE);
		if (nffilter.PageNo == 1) {
			if (mPostFeedAdapter.getCount() <= 1 || mPostFeedAdapter == null) {
				mNewsFeedRequest.setLoader(mLoadingCenter);
			} else {
				mNewsFeedRequest.setLoader(null);
			}

			mLoaderBottomPB.setVisibility(View.INVISIBLE);
		} else {
			mNewsFeedRequest.setLoader(mLoaderBottomPB);
			mLoaderBottomPB.setVisibility(View.VISIBLE);
		}

		mNewsFeedRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mSwipeRefreshWidget.setRefreshing(false);
				if (success) {
					if (null != data) {
						if (mNFFilter.PageNo == 1) {
							resetList();
						}
						List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
						mFetchedAll = (newsFeeds.size() < Config.PAGE_SIZE);
						mNewsFeeds.addAll(newsFeeds);
					//	mPostFeedAdapter.setList(mNewsFeeds, false);
						if(isShareAllow){
							mPostFeedAdapter.setList(mNewsFeeds, true);
						}
						else{
							mPostFeedAdapter.setList(mNewsFeeds, false);	
						}

						if (mNewsFeeds.size() < 1) {
							mNoMoreTV.setVisibility(View.INVISIBLE);
							mNoFeedTV.setVisibility(View.VISIBLE);
						} else if (mFetchedAll && mNFFilter.PageNo > 1) {
							mNoMoreTV.setVisibility(View.VISIBLE);
						} else {
							mNoMoreTV.setVisibility(View.INVISIBLE);
						}
					}
				} else {
					mFetchedAll = false;
					DialogUtil.showOkDialog(mContext, (String) data, "");
				}
			}
		});
	}

	private boolean mFetchedAll = true;
	private OnScrollListener mOnScrollListener = new OnScrollListener() {
		private boolean bReachedListEnd;

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			if (Config.DEBUG) {
				Log.d(TAG, "onScrollStateChanged bReachedListEnd=" + bReachedListEnd + ", mFetchedAll=" + mFetchedAll + ", scrollState="
						+ scrollState);
			}
			if (bReachedListEnd && !mFetchedAll) {
				mFetchedAll = true;
				mNFFilter.PageNo++;

				getFeedRequest(mNFFilter);
			}
		}

		@Override
		public void onScroll(AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
			bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);
			if (Config.DEBUG) {
				Log.d(TAG, "onScroll bReachedListEnd=" + bReachedListEnd + ", firstVisibleItem=" + firstVisibleItem + ", visibleItemCount="
						+ visibleItemCount + ", totalItemCount=" + totalItemCount);
			}
		}
	};
}