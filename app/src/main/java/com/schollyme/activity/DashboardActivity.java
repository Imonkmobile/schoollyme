package com.schollyme.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.adapter.LeftNavigatorAdapter;
import com.schollyme.blog.BlogFragment;
import com.schollyme.evaluation.MySportsListFragment;
import com.schollyme.fragments.AboutmeFragment;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.fragments.BuzzFragment;
import com.schollyme.fragments.HomeFragment;
import com.schollyme.fragments.SettingsFragment;
import com.schollyme.friends.FriendsTopFragment;
import com.schollyme.inbox.InboxFragment;
import com.schollyme.media.KiltdMediaFragment;
import com.schollyme.media.MediaMainFragment;
import com.schollyme.media.MediaTypeFragment;
import com.schollyme.model.FilterBy;
import com.schollyme.model.LeftMenuModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NotificationCountModel;
import com.schollyme.notifications.NotificationFragment;
import com.schollyme.scores.MyScoresFragment;
import com.schollyme.search.FilterListActivity;
import com.schollyme.search.SearchScreenActivity;
import com.schollyme.songofday.SongOfDayFragment;
import com.schollyme.team.TeamsFragment;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.LogoutRequest;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.NotificationChangeSeenStatusRequest;
import com.vinfotech.request.NotificationCountRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.ImageLoaderUniversal;

import java.util.ArrayList;
import java.util.Arrays;

public class DashboardActivity extends BaseFragmentActivity implements OnClickListener {

    private Context mContext;
    private ListView mSlideLV;
    public static DrawerLayout mDrawerLayout;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private NotificationCountModel mModel;
    private BaseFragment mBaseFragment = new BaseFragment();
    private LeftNavigatorAdapter mAdapter;
    private static View mHeaderLayout;
    private TextView mUserNameSliderTV, mUserStatusSliderTV;
    private ImageView feedsTabImg, messagesTabImg, mediaTabImg;
    private RelativeLayout messagesTablayout, mediaTablayout;
    private TextView messagesCountTV, mediaCountTV;

    private ImageView mUserProfileSliderIV;
    public static int lastFrPosition = 0;
    private ArrayList<LeftMenuModel> mMenuAL;
    private int messageNotificationCount = 0, mediaNotificationCount = 0;
    public static String imageCaptureType = "Profile";
    private boolean isSmartSearch = false;

    private int[] SLIDER_NORMAL_RESOURCES_ATHLETE = {R.drawable.ic_about, R.drawable.ic_media,
            R.drawable.ic_song_of_day, R.drawable.ic_notification, R.drawable.ic_my_buzz, R.drawable.ic_my_evaluation,
            R.drawable.ic_my_academics, R.drawable.ic_profile_verification, R.drawable.ic_friends, R.drawable.ic_teams,
            R.drawable.ic_friends, R.drawable.ic_kilt, R.drawable.ic_logout};

    private int[] SLIDER_NORMAL_RESOURCES_COACH = {R.drawable.ic_about, R.drawable.ic_media, R.drawable.ic_song_of_day,
            R.drawable.ic_notification, R.drawable.ic_profile_verification, R.drawable.ic_friends, R.drawable.ic_teams,
            R.drawable.ic_friends, R.drawable.ic_kilt, R.drawable.ic_logout};

    private int[] SLIDER_NORMAL_RESOURCES_FAN = {R.drawable.ic_about, R.drawable.ic_media, R.drawable.ic_song_of_day,
            R.drawable.ic_notification, R.drawable.ic_profile_verification, R.drawable.ic_friends,
            R.drawable.ic_teams, R.drawable.ic_friends, R.drawable.ic_logout};

    private LogedInUserModel mLogedInUserModel;
    public final static int HOME_FRAGMENTINDEX = 14;
    private int fragmentToOpen = HOME_FRAGMENTINDEX;
    public String mPostNotiLink = "";
    public String mFromActivity = "";
    private CallbackManager sCallbackManager;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String mOtherProfileLink = intent.getStringExtra("UserProfileLink");
        if (TextUtils.isEmpty(mOtherProfileLink)) {
//            Uri uri = intent.getData();
//            if (uri != null) {
//                mOtherProfileLink = uri.getQueryParameter("UserProfileLink");
//            }
//
//            if (lastFrPosition != 7) {
//                switchFragment(7, "", "", 0, "");
//            }

        } else {
            fragmentToOpen = getIntent().getIntExtra("FragmentToOpen", DashboardActivity.HOME_FRAGMENTINDEX);
            if (getPreference("isLastBack") && fragmentToOpen != HOME_FRAGMENTINDEX) {
                switchFragment(HOME_FRAGMENTINDEX, "", "", 0, "");
            } else {
                switchFragment(fragmentToOpen, "", "", 0, "");
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        FacebookSdk.sdkInitialize(getApplicationContext());
        sCallbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.navigation_drawer_layout);
        isSmartSearch = false;
        mContext = this;

        //	new ImageLoader().execute();
        mFragmentManager = getSupportFragmentManager();
        mLogedInUserModel = new LogedInUserModel(this);
        controllInitialization();
        mPostNotiLink = getIntent().getStringExtra("UserProfileLink");
        mFromActivity = getIntent().getStringExtra("fromActivity");

        if (!TextUtils.isEmpty(mPostNotiLink)) {
            Uri uri = getIntent().getData();
            if (uri != null) {
                mPostNotiLink = uri.getQueryParameter("UserProfileLink");
            }

            switchFragment(HOME_FRAGMENTINDEX, mPostNotiLink, "", 0, "");
        } else {
            fragmentToOpen = getIntent().getIntExtra("FragmentToOpen", DashboardActivity.HOME_FRAGMENTINDEX);
            switchFragment(fragmentToOpen, "", "", 0, "");
        }

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.icon_menu, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                // switchFragment(5,"");
            }

            // ** Called when a drawer has settled in a completely open state.
            // *//*
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getInAppNotificationCountRequest();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        setMessageNotificationCount();
        setMediaNotificationCount();
    }

    private void controllInitialization() {

        feedsTabImg = (ImageView) findViewById(R.id.feedsTabImg);
        messagesTabImg = (ImageView) findViewById(R.id.messagesTabImg);
        mediaTabImg = (ImageView) findViewById(R.id.mediaTabImg);

        messagesTablayout = (RelativeLayout) findViewById(R.id.messagesTablayout);
        messagesCountTV = (TextView) findViewById(R.id.message_count_tv);

        mediaTablayout = (RelativeLayout) findViewById(R.id.mediaTablayout);
        mediaCountTV = (TextView) findViewById(R.id.media_count_tv);

        feedsTabImg.setOnClickListener(this);
        messagesTabImg.setOnClickListener(this);
        mediaTabImg.setOnClickListener(this);
        changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 255);
        changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 135);
        changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 135);

        mSlideLV = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mHeaderLayout = findViewById(R.id.header_layout);
        setLeftNavigationSlider();
        setHeader(R.drawable.icon_menu, R.drawable.ic_search_header,
                getResources().getString(R.string.app_name), SliderListener, SliderListener);
    }

    /**
     * Handel back press, when left slider is open and user presses back button
     * then close drawer else super class method will be called
     */
    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
        } else {

            if (lastFrPosition == HOME_FRAGMENTINDEX) {
                savePreferences(this, "isLastBack", true);
                super.onBackPressed();
            } else {
                lastFrPosition = HOME_FRAGMENTINDEX;
                setSliderItem(HOME_FRAGMENTINDEX);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUserData();
        setMessageNotificationCount();
        setMediaNotificationCount();

        if (isSmartSearch) {
            isSmartSearch = false;
            switchFragment(HOME_FRAGMENTINDEX, "", "", 0, "");
        }
    }

    private ImageButton mSettingsIB, mFavoriteIB;

    /**
     * THIS METHOD IS FOR DISPLAY SLIDE MENU
     * AND SET ADAPTER FOR IT
     */
    private void setLeftNavigationSlider() {

        int width = (int) (getResources().getDimension(R.dimen.space_mid10) * 16);
        ListView.LayoutParams rlp = new ListView.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, width);
        View header = (View) getLayoutInflater().inflate(R.layout.user_info_view, null);
        mSettingsIB = (ImageButton) header.findViewById(R.id.settings_ib);
        mSettingsIB.setOnClickListener(this);
        mFavoriteIB = (ImageButton) header.findViewById(R.id.favorite_ib);
        mFavoriteIB.setOnClickListener(this);

        if (new LogedInUserModel(mContext).isProfileVerified.equals("1")) {
            mFavoriteIB.setVisibility(View.VISIBLE);
        } else {
            mFavoriteIB.setVisibility(View.GONE);
        }

        // ImageButton mSearchIB = (ImageButton) header.findViewById(R.id.search_ib);
        // mSearchIB.setOnClickListener(this);
        // ImageButton mTranscriptsRequestIB = (ImageButton) header.findViewById(R.id.transcripts_ib);
        // mTranscriptsRequestIB.setOnClickListener(this);

        header.setLayoutParams(rlp);
        String[] sliderOptions;

        if (mLogedInUserModel.mUserType == 1) {
            sliderOptions = getResources().getStringArray(R.array.slide_tab_options_coach);
            mAdapter = new LeftNavigatorAdapter(SLIDER_NORMAL_RESOURCES_COACH, mContext);
        } else if (mLogedInUserModel.mUserType == 2) {
            sliderOptions = getResources().getStringArray(R.array.slide_tab_options_athlete);
            mAdapter = new LeftNavigatorAdapter(SLIDER_NORMAL_RESOURCES_ATHLETE, mContext);
        } else {
            sliderOptions = getResources().getStringArray(R.array.slide_tab_options_fan);
            mAdapter = new LeftNavigatorAdapter(SLIDER_NORMAL_RESOURCES_FAN, mContext);
        }

        ArrayList<String> sliderOptionsAL = new ArrayList<String>(Arrays.asList(sliderOptions));
        mMenuAL = LeftMenuModel.getLeftMenus(sliderOptionsAL);
        mAdapter.setList(mMenuAL);
        if (mSlideLV.getHeaderViewsCount() == 0) {
            mSlideLV.addHeaderView(header, null, false);
        }
        mSlideLV.setAdapter(mAdapter);
        mSlideLV.setOnItemClickListener(NavigationDrawerItemClickListener);

        mUserNameSliderTV = (TextView) header.findViewById(R.id.user_name_tv);
        mUserStatusSliderTV = (TextView) header.findViewById(R.id.user_type_tv);
        mUserProfileSliderIV = (ImageView) header.findViewById(R.id.profile_image_iv);

//        if (new LogedInUserModel(mContext).mUserType == Config.USER_TYPE_ATHLETE) {
//            mTranscriptsRequestIB.setVisibility(View.VISIBLE);
//        } else {
//            mTranscriptsRequestIB.setVisibility(View.GONE);
//        }

    }

    /**
     * Method is used for dsiplay user related information in slide menu
     */
    private void setUserData() {

        LogedInUserModel mModel = new LogedInUserModel(mContext);
        System.out.println("muserFullname     "+mModel.mFirstName + " " + mModel.mLastName);
        mUserNameSliderTV.setText(mModel.mFirstName + " " + mModel.mLastName);
        int mUserType = mModel.mUserType;
        String mUserTypeName = "";
        if (mUserType == 1) {
            mUserTypeName = String.valueOf(Config.UserType.Coach);
        } else if (mUserType == 2) {
            mUserTypeName = String.valueOf(Config.UserType.Athlete);
        } else if (mUserType == 3) {
            mUserTypeName = String.valueOf(Config.UserType.Fan);
        }

        mUserStatusSliderTV.setText(mUserTypeName);
        String imageUrl = Config.IMAGE_URL_PROFILE + mModel.mUserPicURL;
        ImageLoaderUniversal.ImageLoadRoundSlider(mContext, imageUrl, mUserProfileSliderIV,
                ImageLoaderUniversal.option_Round_Image);
    }

    /**
     * This will hide/show togel method used for show/hide action bar
     */
    public static void hideShowActionBar(boolean flag) {
        if (flag) {
            mHeaderLayout.setVisibility(View.VISIBLE);
        } else {
            mHeaderLayout.setVisibility(View.GONE);
        }
    }

    public static Intent getIntent(Context context, int fragmentToOpen, String fromActivity) {

        Intent intent = new Intent(context, DashboardActivity.class);
        intent.putExtra("FragmentToOpen", fragmentToOpen);
        intent.putExtra("fromActivity", fromActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        return intent;

    }

    private HomeFragment mHomeFragment;

    /**
     * Method used for switch between fragment, when user select
     * any menu from left slider then this method will be called
     * and appropriate fragment will render
     */
    public void switchFragment(int position, String mActivityGUID, String mTeamGUID, int mType, String mEntityGUID) {
        lastFrPosition = position;

        changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 255);
        changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 135);
        changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 135);
        isSmartSearch = false;
        switch (position) {
            case 0: // header
                mBaseFragment = null;
                break;
            case 1: // About
                mBaseFragment = null;
                mBaseFragment = AboutmeFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                Bundle bundleAbout = new Bundle();
                bundleAbout.putString("mUserGUID", mLogedInUserModel.mUserGUID);
                bundleAbout.putString("fromActivity", mFromActivity);
                mBaseFragment.setArguments(bundleAbout);
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                    mFragmentTransaction.commit();
                }

                break;

            case 2: //  Media
                mBaseFragment = null;
                mBaseFragment = MediaMainFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                Bundle bundleMedia = new Bundle();
                bundleMedia.putString("mUserGUID", mLogedInUserModel.mUserGUID);
                bundleMedia.putString("fromActivity", mFromActivity);
                mBaseFragment.setArguments(bundleMedia);
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                    mFragmentTransaction.commit();
                }

                break;

            case 3: // song of the day
                mBaseFragment = null;
                mBaseFragment = SongOfDayFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                Bundle bundlePlayList1 = new Bundle();
                bundlePlayList1.putString("TitleName", getResources().getString(R.string.Song_of_the_Day));
                mBaseFragment.setArguments(bundlePlayList1);
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                    mFragmentTransaction.commit();
                }

                break;

            case 4: // Notification

                mBaseFragment = null;
                mBaseFragment = NotificationFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                markNotificationSeenRequest();

                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                    mFragmentTransaction.commit();
                }

                break;
            case 5: // My Buzz
                mBaseFragment = null;
                if (mLogedInUserModel.mUserType == 2) {
                    mBaseFragment = BuzzFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    Bundle bundleBuzz = new Bundle();
                    bundleBuzz.putString("mUserGUID", mLogedInUserModel.mUserGUID);
                    bundleBuzz.putString("fromActivity", mFromActivity);
                    mBaseFragment.setArguments(bundleBuzz);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                } else {

                    mBaseFragment = ProfileVerificationFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    Bundle bundleVerification = new Bundle();
                    bundleVerification.putString("mUserGUID", mLogedInUserModel.mUserGUID);
                    bundleVerification.putString("fromActivity", mFromActivity);
                    mBaseFragment.setArguments(bundleVerification);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                }


                break;
            case 6: // My Evaluation
                mBaseFragment = null;
                if (mLogedInUserModel.mUserType == 2) {
                    mBaseFragment = MySportsListFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    Bundle bundleEvaluation = new Bundle();
                    bundleEvaluation.putString("HeaderText", getString(R.string.My_Evaluation_hader));
                    bundleEvaluation.putString("UserGUID", mLogedInUserModel.mUserGUID);
                    bundleEvaluation.putString("UserName", mLogedInUserModel.mUserName);
                    bundleEvaluation.putString("SportsID", mLogedInUserModel.mUserSportsId);
                    bundleEvaluation.putString("SportsName", mLogedInUserModel.mUserSportsName);
                    bundleEvaluation.putInt("ActiveUserType", mLogedInUserModel.mUserType);
                    bundleEvaluation.putString("SelectedFilter", "");

                    mBaseFragment.setArguments(bundleEvaluation);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                } else {
                    mBaseFragment = FriendsTopFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                }


                break;
            case 7: // My Academics
                if (mLogedInUserModel.mUserType == 2) {

                    mBaseFragment = null;
                    mBaseFragment = MyScoresFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    Bundle bundleAcademics = new Bundle();
                    bundleAcademics.putString("HeaderText", getString(R.string.my_academics));
                    bundleAcademics.putString("UserGUID", mLogedInUserModel.mUserGUID);
                    bundleAcademics.putInt("ActiveUserType", mLogedInUserModel.mUserType);

                    mBaseFragment.setArguments(bundleAcademics);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                } else {
                    mBaseFragment = TeamsFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                }

                break;
            case 8: // Profile Verification

                if (mLogedInUserModel.mUserType == 2) {
                    mBaseFragment = null;
                    mBaseFragment = ProfileVerificationFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    Bundle bundleVerification = new Bundle();
                    bundleVerification.putString("mUserGUID", mLogedInUserModel.mUserGUID);
                    bundleVerification.putString("fromActivity", mFromActivity);
                    mBaseFragment.setArguments(bundleVerification);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                } else {
                    InviteDialog();
                }

                break;
            case 9:
                if (mLogedInUserModel.mUserType == 2) { // Friends
                    mBaseFragment = FriendsTopFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                } else if (mLogedInUserModel.mUserType == 1) { // S.M.A.R.T.
                    if (mLogedInUserModel.isProfileVerified.equals("1")) {
                        isSmartSearch = true;
                        FilterBy mFilterBy = new FilterBy();
                        startActivity(FilterListActivity.getIntent(DashboardActivity.this, mFilterBy)
                                .putExtra("UserType", mLogedInUserModel.mUserType).putExtra("is_smart", true));
                        this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                    } else {
                        String msg = getResources().getString(R.string.smart_verify_msg);
                        String title = getResources().getString(R.string.smart_recruting);
                        DialogUtil.showOkDialogUncancelableButtonLisnter(this, msg, title,
                                new DialogUtil.OnOkButtonListner() {
                                    @Override
                                    public void onOkBUtton() {

                                    }
                                });
                    }
                } else { // Logout
                    mBaseFragment = null;
                    logoutUserRequest();

                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                }


                break;
            case 10: // Teams
                if (mLogedInUserModel.mUserType == 2) {
                    mBaseFragment = TeamsFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                } else {// Logout
                    mBaseFragment = null;
                    logoutUserRequest();
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                }

                break;
            case 11: // Teams

                InviteDialog();

                break;
            case 12:// S.M.A.R.T.
                mBaseFragment = null;
                if (mLogedInUserModel.mUserType == 1) {
                    if (mLogedInUserModel.isProfileVerified.equals("1")) {
                        isSmartSearch = true;
                        FilterBy mFilterBy = new FilterBy();
                        startActivity(FilterListActivity.getIntent(DashboardActivity.this, mFilterBy)
                                .putExtra("UserType", mLogedInUserModel.mUserType).putExtra("is_smart", true));
                        this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                    } else {

                        String msg = getResources().getString(R.string.smart_verify_msg);
                        String title = getResources().getString(R.string.SMART);
                        DialogUtil.showOkDialogUncancelableButtonLisnter(this, msg, title,
                                new DialogUtil.OnOkButtonListner() {
                                    @Override
                                    public void onOkBUtton() {

                                    }
                                });
                    }
                } else {

                    logoutUserRequest();
                    if (mBaseFragment != null) {
                        savePreferences(this, "isLastBack", false);
                        mFragmentManager = getSupportFragmentManager();
                        mFragmentTransaction = mFragmentManager.beginTransaction();
                        mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                        mFragmentTransaction.commit();
                    }
                }

                break;
            case 13: // Logout

                mBaseFragment = null;
                logoutUserRequest();

                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                    mFragmentTransaction.commit();
                }

                break;
            case 14:

                mBaseFragment = null;
                changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 255);
                changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 135);
                changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 135);

                mHomeFragment = HomeFragment.getInstance(NewsFeedRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID,
                        new HeaderLayout(mHeaderLayout));
                Bundle bundle1 = new Bundle();
                bundle1.putString("fromActivity", mFromActivity);
                bundle1.putString("mActivityGUID", mActivityGUID);
                bundle1.putString("mTeamGUID", mTeamGUID);
                bundle1.putInt("mNotificationType", mType);
                bundle1.putString("mEntityGUID", mEntityGUID);
                mHomeFragment.setArguments(bundle1);
                mBaseFragment = mHomeFragment;
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(position));
                    mFragmentTransaction.commit();
                }

                break;
            case 15: // Kilt'd Data

                mBaseFragment = null;
                changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 135);
                changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 135);
                changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 255);
                mBaseFragment = KiltdMediaFragment.getInstance(NewsFeedRequest.MODULE_ID_USERS,
                        mLogedInUserModel.mUserGUID, new HeaderLayout(mHeaderLayout), false);
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    Bundle bundleKiltd = new Bundle();
                    bundleKiltd.putString("mActivityGUID", "");
                    bundleKiltd.putString("mTeamGUID", "");
                    bundleKiltd.putInt("mNotificationType", 1);
                    mBaseFragment.setArguments(bundleKiltd);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(3));
                    mFragmentTransaction.commit();
                }

                break;
            case 16: // Pep Talk

                mBaseFragment = null;
                changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 135);
                changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 135);
                changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 255);
                mBaseFragment = BlogFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(3));
                    mFragmentTransaction.commit();
                }

                break;
            case 17: // playlist

                mBaseFragment = null;
                changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 135);
                changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 135);
                changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 255);
                mBaseFragment = SongOfDayFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                Bundle bundlePlayList = new Bundle();
                bundlePlayList.putString("TitleName", getResources().getString(R.string.play_list));
                mBaseFragment.setArguments(bundlePlayList);

                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(3));
                    mFragmentTransaction.commit();
                }

                break;
            default:
                break;
        }


    }

    /**
     * Click listener listen for user click event on different events
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_ib:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                            mDrawerLayout.closeDrawers();
                        }
                    }
                }, 300);

                lastFrPosition = 0;
                mBaseFragment = null;
                mBaseFragment = SettingsFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf("0"));
                    mFragmentTransaction.commit();
                }

                break;
            case R.id.feedsTabImg:

                mBaseFragment = null;
                lastFrPosition = 0;
                changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 255);
                changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 135);
                changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 135);
                switchFragment(DashboardActivity.HOME_FRAGMENTINDEX, "", "", 0, "");

                break;
            case R.id.messagesTabImg:

                lastFrPosition = 4;
                mBaseFragment = null;
                messageNotificationCount = 0;
                setMessageNotificationCount();
                changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 135);
                changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 255);
                changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 135);

                mBaseFragment = InboxFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment, String.valueOf(4));
                    mFragmentTransaction.commit();
                }

                break;
            case R.id.mediaTabImg:

                mBaseFragment = null;
                lastFrPosition = 3;

                mediaNotificationCount = 0;
                setMediaNotificationCount();
                changeImageOpacity(feedsTabImg, R.drawable.ic_feed_tab_check, 135);
                changeImageOpacity(messagesTabImg, R.drawable.ic_msg_tab_check, 135);
                changeImageOpacity(mediaTabImg, R.drawable.ic_media_tab_check, 255);

                mBaseFragment = MediaTypeFragment.newInstance(0, new HeaderLayout(mHeaderLayout), false);
                Bundle bundleMedia = new Bundle();
                bundleMedia.putString("HeaderText", getResources().getString(R.string.media_label));
                mBaseFragment.setArguments(bundleMedia);
                if (mBaseFragment != null) {
                    savePreferences(this, "isLastBack", false);
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.container_fragment, mBaseFragment,
                            String.valueOf(3));
                    mFragmentTransaction.commit();
                }

                break;
            case R.id.kiltd_media_Txt:

                lastFrPosition = 3;
                switchFragment(15, "", "", 0, "");

                break;
            case R.id.peptalk_media_Txt:

                lastFrPosition = 3;
                switchFragment(16, "", "", 0, "");

                break;
            case R.id.playlist_media_Txt:

                lastFrPosition = 3;
                switchFragment(17, "", "", 0, "");

                break;
            case R.id.search_ib:

                startActivity(SearchScreenActivity.getIntent(DashboardActivity.this)
                        .putExtra("UserType", mLogedInUserModel.mUserType));
                this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                break;
            case R.id.change_profile_photo_tv:

                imageCaptureType = "Profile";
                displayImagePickerPopup();

                break;
            case R.id.change_background_photo_tv:

                imageCaptureType = "Cover";
                displayImagePickerPopup();

                break;
            default:
                break;
        }
    }

    /**
     * When user click change profile image then dialog will open
     * to choose image from either galley or open camera
     */
    private void displayImagePickerPopup() {

        DialogUtil.showListDialog(mContext, 0, new DialogUtil.OnItemClickListener() {
            @Override
            public void onItemClick(int position, String item) {
                isCropCall = true;
                if (position == 0) {
                    openCamera();
                } else {
                    openGallery();
                }
            }

            @Override
            public void onCancel() {

            }
        }, mContext.getString(R.string.take_picture), mContext.getString(R.string.load_from_library));
    }


    private void changeImageOpacity(ImageView image, int drawable, int opacity) {

        Drawable rightArrow = getResources().getDrawable(drawable);
        // setting the opacity (alpha)
        rightArrow.setAlpha(opacity);
        // setting the images on the ImageViews
        image.setImageDrawable(null);
        image.setImageDrawable(rightArrow);
    }

    private void InviteDialog() {

        final Dialog dialog = new Dialog(DashboardActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.invite_dialog_layout);
        // Set dialog title
        dialog.setTitle(getResources().getString(R.string.invite_friends_setting));
        // set values for custom dialog components - text, image and button
        TextView facebooktv = (TextView) dialog.findViewById(R.id.facebooktv);
        TextView smstv = (TextView) dialog.findViewById(R.id.smstv);
        TextView emailtv = (TextView) dialog.findViewById(R.id.emailtv);
        dialog.show();

        facebooktv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                InviteFriends();
                dialog.dismiss();
            }
        });


        smstv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                try {
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.putExtra("sms_body", "I found this cool app SchollyME. It's a really " +
                            "good sports social network for athletes and fans. You should download it and follow me. " +
                            "It's pretty cool. Android: https://play.google.com/store/apps/details?id=com.schollyme&hl=en" +
                            " iOS: https://itunes.apple.com/in/app/schollyme/id1042219957?mt=8");
                    sendIntent.setType("vnd.android-dir/mms-sms");
                    startActivity(sendIntent);
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }
        });

        emailtv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL, "");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Hi");
                intent.putExtra(Intent.EXTRA_TEXT, "I found this cool app SchollyME. It's a really " +
                        "good sports social network for athletes and fans. You should download it and follow me. " +
                        "It's pretty cool. Android: https://play.google.com/store/apps/details?id=com.schollyme&hl=en" +
                        " iOS: https://itunes.apple.com/in/app/schollyme/id1042219957?mt=8");
                startActivity(Intent.createChooser(intent, "Send Email"));

                dialog.dismiss();
            }
        });

    }

    private void InviteFriends() {
        String appLinkUrl, previewImageUrl;

        appLinkUrl = "https://fb.me/1674681219215146";
        previewImageUrl = "https://www.mydomain.com/my_invite_image.jpg";


        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(this, content);
        }


    }

    /**
     * Logout method will logout user,Clear his session from server,
     * delete app cache,database and preferences
     */
    private void logoutUserRequest() {

        DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps,
                getResources().getString(R.string.app_name),
                getResources().getString(R.string.logout_confirm),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LogoutRequest mRequest = new LogoutRequest(mContext);
                        mRequest.LogoutServerRequest();
                        mRequest.setRequestListener(new RequestListener() {
                            @Override
                            public void onComplete(boolean success, Object data, int totalRecords) {
                                if (success) {
                                    AuthenticationClient.logout(mContext);
                                    LogedInUserModel mModel = new LogedInUserModel(mContext);
                                    mModel.removeUserData(mContext);
                                    Intent intent = LoginActivity.getIntent(mContext);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    ((SchollyMeApplication) getApplication()).cancelNotifications(false);
                                    finish();
                                } else {
                                    DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                                            getResources().getString(R.string.app_name), null);
                                }
                            }
                        });
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

        mAdapter.notifyDataSetChanged();
        mSlideLV.clearChoices();
        mSlideLV.setOnItemClickListener(NavigationDrawerItemClickListener);
    }

    /**
     * Listeners
     */

    OnClickListener SliderListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.back_button:
                    v.setBackgroundColor(getResources().getColor(R
                            .color.selector_menu_bgcolor));
                    sliderListener();
                    break;
                case R.id.right_button:
                    callSearchActivity();
                    break;

            }
        }
    };

    public void callSearchActivity() {

        startActivity(SearchScreenActivity.getIntent(DashboardActivity.this)
                .putExtra("UserType", mLogedInUserModel.mUserType));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void sliderListener() {

        getInAppNotificationCountRequest();
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
        /*
         * if(lastFrPosition==5 && mModel.mTotalNotificationRecords>0 ){
		 * switchFragment(5, ""); }
		 */
    }

    OnItemClickListener NavigationDrawerItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            setSliderItem(position);
        }
    };

    public void setSliderItem(int position) {
        if (position >= 0) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mDrawerLayout.closeDrawers();
                    }
                }
            }, 300);
            switchFragment(position, "", "", 0, "");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startNotiDataRefresh();

    }

    @Override
    protected void onStop() {
        mHandler.removeCallbacksAndMessages(null);
        getIntent().removeExtra(null);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private Handler mHandler = new Handler();


    /**
     * Alarm method user for auto refresh notification batch frequently after a
     * predefined time interval
     */
    private void startNotiDataRefresh() {

        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startNotiDataRefresh();
            }
        }, Config.NOTIFICATION_UPDATE_DELAY);

        getInAppNotificationCountRequest();
    }

    private NotificationCountRequest mNotificationCountRequest;

    public void getInAppNotificationCountRequest() {
        if (null == mNotificationCountRequest) {
            mNotificationCountRequest = new NotificationCountRequest(mContext);
        }

        mNotificationCountRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (null != mMenuAL) {
                    try {

                        mModel = (NotificationCountModel) data;

                        LeftMenuModel inbox = mMenuAL.get(1);
                        inbox.mOptionMenuCount = mModel.mTotalMessageRecords;
                        mMenuAL.set(1, inbox);
                        messageNotificationCount = mModel.mTotalMessageRecords;

                        LeftMenuModel pepTalk = mMenuAL.get(2);
                        pepTalk.mOptionMenuCount = mModel.mTotalPepTalk;
                        mMenuAL.set(2, pepTalk);
                        mediaNotificationCount = mModel.mTotalPepTalk;

                        LeftMenuModel notification = mMenuAL.get(3);
                        notification.mOptionMenuCount = mModel.mTotalNotificationRecords;
                        mMenuAL.set(3, notification);

                        if (mLogedInUserModel.mUserType == 2) {
                            LeftMenuModel friendRequest = mMenuAL.get(8);
                            friendRequest.mOptionMenuCount = mModel.mTotalPendingFriendRecords;
                            mMenuAL.set(8, friendRequest);
                        } else {
                            LeftMenuModel friendRequest = mMenuAL.get(5);
                            friendRequest.mOptionMenuCount = mModel.mTotalPendingFriendRecords;
                            mMenuAL.set(5, friendRequest);
                        }

                        if (null != mAdapter) {
                            mAdapter.setList(mMenuAL);
                        }

                        setMediaNotificationCount();
                        setMessageNotificationCount();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        mNotificationCountRequest.getNotificationCountServerRequest();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (null != mHomeFragment) {
            mHomeFragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
        sCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public void setMessageNotificationCount() {
        if (messageNotificationCount == 0) {
            messagesCountTV.setVisibility(View.GONE);
        } else {
            messagesCountTV.setVisibility(View.VISIBLE);
            messagesCountTV.setText("" + messageNotificationCount);
        }
    }

    public void setMediaNotificationCount() {
        if (mediaNotificationCount == 0) {
            mediaCountTV.setVisibility(View.GONE);
        } else {
            mediaCountTV.setVisibility(View.VISIBLE);
            mediaCountTV.setText("" + mediaNotificationCount);
        }
    }


    public void savePreferences(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("BACK_PREF", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getPreference(String key) {
        SharedPreferences shared = getSharedPreferences("BACK_PREF", MODE_PRIVATE);
        return shared.getBoolean(key, false);
    }

    /**
     * This api will mark notification as seen
     */
    private void markNotificationSeenRequest() {

        NotificationChangeSeenStatusRequest mRequest = new NotificationChangeSeenStatusRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                Log.d("Notification Mark Seen Status", "Users All fication are marked as seen ::" + data.toString());
                // Do nothing
            }
        });
        mRequest.getNotificationSeenStatusServerRequest();
    }

    @Override
    public void onLocaleUpdate() {
        super.onLocaleUpdate();
        setLeftNavigationSlider();
        if (null != mBaseFragment) {
            mBaseFragment.onLocaleUpdate();
        }
    }
}