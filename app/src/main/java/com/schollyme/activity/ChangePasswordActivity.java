package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.SignUpModel;
import com.vinfotech.request.ChangePasswordRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.Utility;

/**
 * Activity class. This may be useful in ChangePassword of logedin user
 * implementation
 * 
 * @author Ravi Bhandari
 * 
 */
public class ChangePasswordActivity extends BaseActivity implements OnClickListener {

	private EditText mOldPasswordET, mNewPasswordET, mConfirmPasswordET;
	private ErrorLayout mErrorLayout;
	private Context mContext;
	private boolean isChangePassRequest = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_password_activity);
		mContext = this;
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
				getResources().getString(R.string.change_password_setting), new OnClickListener() {

					@Override
					public void onClick(View v) {

						Utility.hideSoftKeyboard(mOldPasswordET);
						finish();
					}
				}, CHANGE_PASSWORD_LISTENER);
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
		controlInitialization();

	}

	private void controlInitialization() {
		mOldPasswordET = (EditText) findViewById(R.id.old_password_et);
		mNewPasswordET = (EditText) findViewById(R.id.new_password_et);
		mConfirmPasswordET = (EditText) findViewById(R.id.c_password_et);
		SignUpModel mModel = new SignUpModel(mContext);
		if (mModel.isFacebookUser.equals("0") && mModel.isGPlusUser.equals("0")) {
			isChangePassRequest = true;
			mOldPasswordET.setVisibility(View.VISIBLE);
		} else {
			isChangePassRequest = false;
			mOldPasswordET.setVisibility(View.GONE);
		}
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, ChangePasswordActivity.class);
		return intent;
	}

	private boolean isValidate() {
		boolean isValidated = false;
		String oldPasswordString = mOldPasswordET.getText().toString();
		String newPasswordString = mNewPasswordET.getText().toString();
		String confirmPasswordString = mConfirmPasswordET.getText().toString();
		if (isChangePassRequest) {
			if (TextUtils.isEmpty(oldPasswordString)) {
				mErrorLayout.showError(getResources().getString(R.string.old_password_require_message), true, ErrorLayout.MsgType.Error);
				isValidated = false;
			} else if (TextUtils.isEmpty(newPasswordString)) {
				mErrorLayout.showError(getResources().getString(R.string.new_password_require_message), true, ErrorLayout.MsgType.Error);
				isValidated = false;
			} else if (newPasswordString.length()<6) {
				mErrorLayout.showError(getResources().getString(R.string.npassword_length_message), true, ErrorLayout.MsgType.Error);
				isValidated = false;
			} else if (TextUtils.isEmpty(confirmPasswordString)) {
				mErrorLayout.showError(getResources().getString(R.string.confirm_password_require_message), true, ErrorLayout.MsgType.Error);
				isValidated = false;
			} else if (!newPasswordString.equals(confirmPasswordString)) {
				mErrorLayout.showError(getResources().getString(R.string.password_confirm_password_not_match_message), true, ErrorLayout.MsgType.Error);
				isValidated = false;
			} else {
				isValidated = true;
			}
		} else if (TextUtils.isEmpty(newPasswordString)) {
			mErrorLayout.showError(getResources().getString(R.string.new_password_require_message), true, ErrorLayout.MsgType.Error);
			isValidated = false;
		} else if (TextUtils.isEmpty(confirmPasswordString)) {
			mErrorLayout.showError(getResources().getString(R.string.confirm_password_require_message), true, ErrorLayout.MsgType.Error);
			isValidated = false;
		} else if (!newPasswordString.equals(confirmPasswordString)) {
			mErrorLayout.showError(getResources().getString(R.string.password_confirm_password_not_match_message), true, ErrorLayout.MsgType.Error);
			isValidated = false;
		} else {
			isValidated = true;
		}
		return isValidated;
	}

	/** Listeners */
	@Override
	public void onClick(View v) {

	}

	OnClickListener CHANGE_PASSWORD_LISTENER = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (isValidate()) {
				if (isChangePassRequest) {
					changePasswordRequest();
				}

			}
		}
	};

	/** Server Request and Response */
	private void changePasswordRequest() {
		String oldPasswordString = mOldPasswordET.getText().toString();
		String newPasswordString = mNewPasswordET.getText().toString();
		ChangePasswordRequest mRequest = new ChangePasswordRequest(mContext);

		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {

					DialogUtil.showOkDialogButtonLisnter(mContext, getResources().getString(R.string.password_change_success),
							getResources().getString(R.string.app_name), new OnOkButtonListner() {

								@Override
								public void onOkBUtton() {

									finish();
								}
							});

				} else {
					mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Error);
				}
			}
		});

		mRequest.changePasswordServerRequest(oldPasswordString, newPasswordString,
				new SignUpModel(mContext).mAccessToken);
		/*mRequest.changePasswordServerRequest(Utility.md5(oldPasswordString), Utility.md5(newPasswordString),
				new SignUpModel(mContext).mAccessToken);*/
	}
}