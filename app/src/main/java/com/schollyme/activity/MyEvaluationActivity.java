package com.schollyme.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.evaluation.EvaluationActivity;
import com.schollyme.evaluation.MySportsListFragment;
import com.schollyme.evaluation.UsersSportsListAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SportsModel;
import com.vinfotech.request.GetUsersSportsRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Config;

import java.util.List;

public class MyEvaluationActivity extends BaseFragmentActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static ErrorLayout mErrorLayout;
    private MySportsViewHolder mySportsViewHolder;
    private List<SportsModel> mSportsModels;

    private Context mContext;
    private int pageIndex = 1;
    private boolean loadingFlag = false;
    private String mUserGUID;
    private String mHeaderText;
    private String mSelectedFilter;
    private UsersSportsListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_evaluation);
        mContext = this;
        mHeaderText = getIntent().getStringExtra("HeaderText")+"'s "+getResources().getString(R.string.Evaluation);
        mUserGUID = getIntent().getStringExtra("UserGUID");
        mSelectedFilter = getIntent().getStringExtra("SelectedFilter");

        mySportsViewHolder = new MySportsViewHolder(findViewById(R.id.main_rl), this);
        mySportsViewHolder.mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red,
                R.color.app_text_color, R.color.text_hint_color);
        mySportsViewHolder.mSwipeRefreshWidget.setOnRefreshListener(this);
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));

        getMySports(mUserGUID);

        initHeader();
    }

    private void initHeader() {
        setHeader(R.drawable.icon_back, 0, mHeaderText, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        },null);
    }

    private void getMySports(String mUserGUID) {
        GetUsersSportsRequest mRequest = new GetUsersSportsRequest(mContext);
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mySportsViewHolder.mSwipeRefreshWidget.setRefreshing(false);
                if (success) {
                    mSportsModels = (List<SportsModel>) data;
                    setAdapter(mSportsModels);
                } else {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Error);
                }
            }
        });
        mRequest.GetSportsServerRequest(mUserGUID);
    }

    private void setAdapter(final List<SportsModel> mSportsList) {

        mAdapter = new UsersSportsListAdapter(mContext, mSelectedFilter, mHeaderText);
        mAdapter.setList(mSportsList);
        mySportsViewHolder.mMySportLV.setAdapter(mAdapter);
        mySportsViewHolder.mMySportLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                if (!mHeaderText.equals(getString(R.string.select_filter))) {
                    mSelectedFilter = mAdapter.getItem(pos).mSportsID;
                    mAdapter.resetFilter(mSelectedFilter);
                    startActivity(EvaluationActivity.getIntent(mContext, mUserGUID, "",
                            mSportsList.get(pos).mSportsID, mSportsList.get(pos).mSportsName,
                            Config.USER_TYPE_ATHLETE));
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("SportsID", mAdapter.getItem(pos).mSportsID);
                    intent.putExtra("SportsName", mAdapter.getItem(pos).mSportsName);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onRefresh() {

    }

    public class MySportsViewHolder {

        private ListView mMySportLV;
        private SwipeRefreshLayout mSwipeRefreshWidget;

        public MySportsViewHolder(View view, View.OnClickListener listener) {
            mMySportLV = (ListView) view.findViewById(R.id.mysports_lv);
            mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
        }

    }

    public static Intent getIntent(Context mContext, String UserGUID, String mHeaderText, String selectedFilter) {
        Intent intent = new Intent(mContext, MyEvaluationActivity.class);
        intent.putExtra("UserGUID", UserGUID);
        intent.putExtra("HeaderText", mHeaderText);
        intent.putExtra("SelectedFilter", selectedFilter);
        //	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}
