package com.schollyme.commentslike;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.schollyme.activity.BaseActivity;
import com.schollyme.R;
import com.schollyme.friends.FriendProfileActivity;
import com.schollyme.adapter.LikeListAdapter;
import com.schollyme.adapter.LikeListAdapter.OnItemClickListenerLikeList;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.RefreshLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LikerModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.LikerListRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;

public class LikeListActivity extends BaseActivity implements OnClickListener {
	public static final int REQ_FROM_HOME_LIKELIST = 2001;

	private VHolder mVHolder;
	private LikerListRequest mLikerListRequest;
	private LikeListAdapter mLikeListAdapter;
	private String mTypeId, entityType, fromPage;
	private SwipeRefreshLayout mPullToRefreshSrl;
	private RefreshLayout mRefreshLayout;
	public static final int REQ_CODE_LIKE_ACTIVITY_WALL = 4744;
	private RelativeLayout mEmptyRl;
	private String mBlogGIUD;
	private ErrorLayout mErrorLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.like_list_activity);
		getIntentData();
		initalization();
		getLikeList(true);
	}

	private void getLikeList(boolean showLoader) {
		mLikeListAdapter.setList(null);
		mLikerListRequest.setLoader(showLoader ? findViewById(R.id.loading_pb) : null);
		mLikerListRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mPullToRefreshSrl.setRefreshing(false);
				if (success) {
					List<LikerModel> list = (List<LikerModel>) data;
					mEmptyRl.setVisibility(list.size() == 0 ? View.VISIBLE : View.INVISIBLE);
					mLikeListAdapter.setList(list);

					if (fromPage.equals("MEDIAVIEWER")) {
						mAlbumMedia.NoOfLikes = totalRecords;
						Intent in = new Intent();
						in.putExtra("AlbumMedia", mAlbumMedia);
						LikeListActivity.this.setResult(RESULT_OK, in);
					} else if (fromPage.equals("MEDIAGRID")) {
						LikeListActivity.this.setResult(RESULT_OK);
					} else if (fromPage.equals("BLOG")) {
						Intent in = new Intent();
						in.putExtra("LIKE_COUNT_BLOG", totalRecords);
						in.putExtra("BLOG_GUID", mBlogGIUD);
						in.putExtra("BLOG_ISLIKE", IsLike);
						setResult(RESULT_OK, in);
					}

				} else {
					mEmptyRl.setVisibility(View.INVISIBLE);
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);

				}
			}
		});
		mLikerListRequest.getLikerListInServer(mTypeId, entityType);
	}

	AlbumMedia mAlbumMedia;

	private void getIntentData() {
		mTypeId = getIntent().getStringExtra("iD");
		fromPage = getIntent().getStringExtra("fromPage");
		mAlbumMedia = getIntent().getParcelableExtra("AlbumMedia");
		mBlogGIUD = getIntent().getStringExtra("iD");
		IsLike = getIntent().getIntExtra("IsLike", 0);
		entityType = getIntent().getStringExtra("entityType");
		
	}

	private void initalization() {
		mLikerListRequest = new LikerListRequest(this);
		mRefreshLayout = new RefreshLayout(this);
		mErrorLayout = new ErrorLayout(findViewById(R.id.include_error));
		mLikeListAdapter = new LikeListAdapter(this, new OnItemClickListenerLikeList() {

			@Override
			public void onClickItems(int IDS, int Postion, LikerModel likemodel) {

				switch (IDS) {
				case R.id.container_ll:

					if (!likemodel.ProfileLink.equals(new LogedInUserModel(LikeListActivity.this).mUserProfileURL)) {
						LikeListActivity.this.startActivity(FriendProfileActivity.getIntent(LikeListActivity.this, "",
								likemodel.ProfileLink, "LikeListActivity"));

					}
					break;
				case R.id.desc_tv:
					mLikeListAdapter.removeValue(Postion);
					if (mLikeListAdapter.getCount() == 0) {
						mEmptyRl.setVisibility(View.VISIBLE);
					} else {
						mEmptyRl.setVisibility(View.INVISIBLE);
					}

					Intent in = new Intent();
					in.putExtra("COUNT", mLikeListAdapter.getCount());
					in.putExtra("BUTTONSTATUS", 0);
					setResult(RESULT_OK, in);

					if (fromPage.equals("MEDIAVIEWER")) {

						AlbumMedia v = getIntent().getParcelableExtra("AlbumMedia");
						v.NoOfLikes--;
						v.IsLike = 0;
						Intent in2 = new Intent();
						in2.putExtra("AlbumMedia", v);
						LikeListActivity.this.setResult(RESULT_OK, in2);
					} else if (fromPage.equals("MEDIAGRID")) {
						LikeListActivity.this.setResult(RESULT_OK);
					} else if (fromPage.equals("BLOG")) {
						IsLike = 0;
						Intent intent = new Intent();
						intent.putExtra("LIKE_COUNT_BLOG", mLikeListAdapter.getCount());
						intent.putExtra("BLOG_GUID", mBlogGIUD);
						intent.putExtra("BLOG_ISLIKE", IsLike);
						setResult(RESULT_OK, intent);
					}

					LikeMediaToggleService(likemodel, Postion);
					break;

				default:
					break;
				}

			}
		});
		mPullToRefreshSrl = (SwipeRefreshLayout) findViewById(R.id.pull_to_refresh_srl);
		mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
		mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				mPullToRefreshSrl.setRefreshing(true);
				getLikeList(false);
			}
		});
		mVHolder = new VHolder(findViewById(R.id.container_rl), this);
		mVHolder.mLikeLV.setAdapter(mLikeListAdapter);

		mRefreshLayout.show(mEmptyRl, null, R.drawable.no_like_img, R.string.Be_the_first_to_like, 0, 0);
	}

	private ToggleLikeRequest mToggleLikeRequest;

	public void LikeMediaToggleService(final LikerModel likemodel, final int position) {
		mToggleLikeRequest = new ToggleLikeRequest(this);
		mToggleLikeRequest.toggleLikeInServer(mTypeId, entityType);
		mToggleLikeRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					IsLike = 0;
				} else {
					DialogUtil.showOkDialog(LikeListActivity.this, data.toString(), "");
				}

			}
		});
	}

	class VHolder {
		private HeaderLayout headerLayout;
		private ListView mLikeLV;

		// ListView
		public VHolder(View view, OnClickListener listener) {
			headerLayout = new HeaderLayout(view.findViewById(R.id.header_layout));
			headerLayout.setHeaderValues(R.drawable.ic_close, getResources().getString(R.string.Likes), 0);
			headerLayout.setListenerItI(LikeListActivity.this, LikeListActivity.this);
			mLikeLV = (ListView) view.findViewById(R.id.like_lv);
			mEmptyRl = (RelativeLayout) view.findViewById(R.id.empty_rl);
		}
	}

	public static Intent getIntent(Context context, String iD, String entityType, String fromPage) {
		Intent intent = new Intent(context, LikeListActivity.class);
		intent.putExtra("iD", iD);
		intent.putExtra("entityType", entityType);
		intent.putExtra("fromPage", fromPage);
		return intent;
	}

	private int IsLike;

	public static Intent getIntent(Context context, String iD, String entityType, String fromPage, int IsLike) {
		Intent intent = getIntent(context, iD, entityType, fromPage);
		intent.putExtra("IsLike", IsLike);
		return intent;
	}

	public static Intent getIntent(Context context, String iD, String entityType, String fromPage, AlbumMedia mAlbumMedia) {
		Intent intent = new Intent(context, LikeListActivity.class);
		intent.putExtra("iD", iD);
		intent.putExtra("entityType", entityType);
		intent.putExtra("fromPage", fromPage);
		intent.putExtra("AlbumMedia", mAlbumMedia);

		return intent;
	}

	@Override
	public void onBackPressed() {
		this.finish();
		this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.back_button:
			this.finish();
			this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
			break;

		default:
			break;
		}

	}

}
