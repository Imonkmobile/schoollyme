package com.schollyme.commentslike;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewsFeed;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AddCommentRequest;
import com.vinfotech.request.CommentListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

public class WriteCommentActivity extends BaseActivity implements OnClickListener {
	public static final int REQ_CODE_WRITE_COMMENT_ACTIVITY = 1071;

	private VHolder mVHolder;
	private AlbumMedia mAlbumMedia;

	private Album mAlbumDetail;
	private LogedInUserModel mModel;
	private ErrorLayout mErrorLayout;
	private AddCommentRequest mAddCommentRequest;
	private String Caption;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write_comment_activity);
		initialization();
		getAndSetData();
	}

	private String For = "";
	String mBlogGUID;

	private void getAndSetData() {

		Parcelable parcelableMedia = getIntent().getParcelableExtra("media");
		Parcelable parcelableAlbum = getIntent().getParcelableExtra("Album");
		mAlbumMedia = (AlbumMedia) parcelableMedia;
		mAlbumDetail = (Album) parcelableAlbum;
		Caption = getIntent().getStringExtra("Caption");
		For = getIntent().getStringExtra("FOR");
		mBlogGUID = getIntent().getStringExtra("blogGID");
		if (For.equals("MEDIA") || For.equals("ACTIVITY")) {

			if (null != mAlbumMedia) {

				mVHolder.mMediaNameTV.setText(Config.isMediaName(Caption) ? "" : Caption);

				String path = null;
				if(For.equals("ACTIVITY")){
					path = mAlbumMedia.fullCoverPath;
				} else {
					path = Config.IMAGE_URL_UPLOAD + Config.ALBUM_FOLDER + Config.THUMB_FOLDER + mAlbumMedia.ImageName;
				}
				ImageLoaderUniversal.ImageLoadSquare(WriteCommentActivity.this, path, mVHolder.mMediaIV,
						ImageLoaderUniversal.option_normal_Image_Thumbnail);

				String imageUrl = Config.IMAGE_URL_PROFILE + mModel.mUserPicURL;
				ImageLoaderUniversal.ImageLoadRound(WriteCommentActivity.this, imageUrl, mVHolder.mUserIV,
						ImageLoaderUniversal.option_Round_Image);
			}

		} else if (For.equals("ALBUM")) {

			if (null != mAlbumDetail) {

				mVHolder.mMediaNameTV.setText(mAlbumDetail.AlbumName);

				ImageLoaderUniversal.ImageLoadSquare(WriteCommentActivity.this, Config.IMAGE_URL_UPLOAD + Config.ALBUM_FOLDER
						+ Config.THUMB_FOLDER + mAlbumDetail.CoverMedia, mVHolder.mMediaIV,
						ImageLoaderUniversal.option_normal_Image_Thumbnail);

				String imageUrl = Config.IMAGE_URL_PROFILE + mModel.mUserPicURL;
				ImageLoaderUniversal.ImageLoadRound(WriteCommentActivity.this, imageUrl, mVHolder.mUserIV,
						ImageLoaderUniversal.option_Round_Image);
			}

		} else if (For.equals("BLOG")) {
			mVHolder.mMediaNameTV.setText(Caption);
			ImageLoaderUniversal.ImageLoadSquare(WriteCommentActivity.this, getIntent().getStringExtra("imagePath"), mVHolder.mMediaIV,
					ImageLoaderUniversal.option_normal_Image_Thumbnail);
			String imageUrl = Config.IMAGE_URL_PROFILE + mModel.mUserPicURL;
			ImageLoaderUniversal.ImageLoadRound(WriteCommentActivity.this, imageUrl, mVHolder.mUserIV,
					ImageLoaderUniversal.option_Round_Image);
		}

	}

	private void initialization() {
		mErrorLayout = new ErrorLayout(findViewById(R.id.error_layout));
		mVHolder = new VHolder(findViewById(R.id.container_ll), this);
		mAddCommentRequest = new AddCommentRequest(this);
		// addCommentInServer
		mModel = new LogedInUserModel(this);
	}

	public static void addFullPathIfNeeded(NewsFeed newsFeed, AlbumMedia albumMedia){
		if (null != newsFeed.Album && null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() > 0) {
			albumMedia.fullCoverPath = newsFeed.Album.AlbumMedias.get(0).fullCoverPath;
		}
	}

	public void AddCommentService(String Comment) {

		if (For.equals("MEDIA")) {

			mAddCommentRequest.addCommentInServer(Comment, CommentListRequest.ENTITYTYPE_MEDIA, mAlbumMedia.MediaGUID, null);
		} else if (For.equals("ALBUM")) {

			mAddCommentRequest.addCommentInServer(Comment, CommentListRequest.ENTITYTYPE_ACTIVITY, mAlbumDetail.ActivityGUID, null);
		} else if (For.equals("ACTIVITY")) {

			mAddCommentRequest.addCommentInServer(Comment, CommentListRequest.ENTITYTYPE_ACTIVITY, mAlbumMedia.MediaGUID, null);
		} else if (For.equals("BLOG")) {

			mAddCommentRequest.addCommentInServer(Comment, CommentListRequest.ENTITYTYPE_BLOG, mBlogGUID, null);
		}

		mAddCommentRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {

					if (For.equals("MEDIA")) {

						Intent in2 = null;
						in2 = new Intent("COMMENTADD");
						sendBroadcast(in2);

						Intent in = new Intent();
						mAlbumMedia.NoOfComments = mAlbumMedia.NoOfComments + 1;
						in.putExtra("albumMedia", mAlbumMedia);
						setResult(RESULT_OK, in);

					} else if (For.equals("ALBUM")) {
						Intent in2 = null;
						in2 = new Intent("COMMENTADD");
						sendBroadcast(in2);
						Intent in = new Intent();
						setResult(RESULT_OK, in);

					} else if (For.equals("ACTIVITY")) {
						Intent in = new Intent();
						setResult(RESULT_OK, in);

					} else if (For.equals("BLOG")) {
						Intent in = new Intent();
						in.putExtra("BLOG_GUID", mBlogGUID);
						setResult(RESULT_OK, in);
					}
					finishScreen();
				} else {
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
				}
			}
		});
	}

	class VHolder {
		private HeaderLayout headerLayout;
		EditText mCommentET;
		TextView mMediaNameTV;
		ImageView mMediaIV, mUserIV;

		// trans_lay_bottom
		public VHolder(View view, OnClickListener listener) {
			headerLayout = new HeaderLayout(view.findViewById(R.id.header_layout));
			headerLayout.setHeaderValues(R.drawable.ic_close, getResources().getString(R.string.Write_Comment),
					R.drawable.ic_send_msgs);
			headerLayout.setListenerItI(WriteCommentActivity.this, WriteCommentActivity.this);
			mCommentET = (EditText) view.findViewById(R.id.comment_et);

			mMediaNameTV = (TextView) view.findViewById(R.id.media_name_tv);
			mMediaIV = (ImageView) view.findViewById(R.id.media_iv);
			mUserIV = (ImageView) view.findViewById(R.id.user_iv);
			FontLoader.setRobotoMediumTypeface(mMediaNameTV);
			FontLoader.setRobotoRegularTypeface(mCommentET);
		}

	}

	public static Intent getIntent(Context context, AlbumMedia media, String commentFor, String Caption) {
		Intent intent = new Intent(context, WriteCommentActivity.class);
		intent.putExtra("media", media);
		intent.putExtra("FOR", commentFor);
		intent.putExtra("Caption", Caption);
		return intent;
	}

	public static Intent getIntentBlog(Context context, String blogGID, String commentFor, String Caption, String imagePath) {
		Intent intent = new Intent(context, WriteCommentActivity.class);
		intent.putExtra("blogGID", blogGID);
		intent.putExtra("FOR", commentFor);
		intent.putExtra("Caption", Caption);
		intent.putExtra("imagePath", imagePath);
		return intent;
	}

	public static Intent getIntent(Context context, Album nAlbum, String Caption) {
		Intent intent = new Intent(context, WriteCommentActivity.class);
		intent.putExtra("Album", nAlbum);
		intent.putExtra("FOR", "ALBUM");
		intent.putExtra("Caption", Caption);
		return intent;
	}

	@Override
	public void onBackPressed() {
		finishScreen();
	}

	public void finishScreen() {
		Utility.hideSoftKeyboard(mVHolder.mCommentET);
		this.finish();
		this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.back_button: {
			finishScreen();
		}
			break;
		case R.id.right_button: {

			if (mVHolder.mCommentET.getText().toString().trim().equals("")) {

				// mErrorLayout.showError("Please enter description", true,
				// MsgType.Error);
				// Disable Buttom

			} else {
				DisplayUtil.hideKeyboard(mVHolder.mCommentET, this);
				AddCommentService(mVHolder.mCommentET.getText().toString().trim());
			}
		}
			break;

		default:
			break;
		}

	}
}
