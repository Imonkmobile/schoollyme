package com.schollyme.blog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.activity.BaseActivity;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.media.MediaViewerActivity;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.BlogModel;
import com.schollyme.wall.MediaViewerWallActivity;
import com.schollyme.wall.SingleMediaViewWallActivity;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.BlogDetailRequest;
import com.vinfotech.request.CommentListRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.CalStartEndUtil;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class BlogDetailActivity extends BaseActivity implements OnClickListener {

    private Context mContext;
    private String mBlogID;
    private BlogDetailRequest mBlogDetailRequest;
    private VHolder mVHolder;
    private String mCoverImagePath;

    public static final int REQ_CODE_BLOGDETAIL_SCREEN = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blog_detail_activity);
        mContext = BlogDetailActivity.this;
        mViewPagerAdapter = null;

        getIntetData();
        initViews();
        getBlogDetailInList(mBlogID);
    }

    private BlogModel mBlogDetail;

    private void getBlogDetailInList(String blogGuid) {

        mBlogDetailRequest.setLoader(findViewById(R.id.loading_layout_rl));

        mBlogDetailRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    List<BlogModel> bloglistcurrent = (List<BlogModel>) data;
                    mBlogDetail = bloglistcurrent.get(0);
                    setValues(mBlogDetail);
                    findViewById(R.id.blank_view_rl).setVisibility(View.GONE);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                    findViewById(R.id.blank_view_rl).setVisibility(View.VISIBLE);
                }
            }
        });

        mBlogDetailRequest.getBlogDetailInServer(blogGuid);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    mBlogDetail.NoOfComments++;
                    setValues(mBlogDetail);
                }
                break;

            case CommentListActivity.REQ_CODE_COMMENT_LIST:
                if (resultCode == RESULT_OK && null != data) {
                    mBlogDetail.NoOfComments = data.getIntExtra("COMMENT_COUNT_BLOG", 0);
                    setValues(mBlogDetail);
                }
                break;

            case LikeListActivity.REQ_FROM_HOME_LIKELIST:
                if (resultCode == RESULT_OK && null != data) {
                    mBlogDetail.IsLike = data.getIntExtra("BLOG_ISLIKE", 0);
                    mBlogDetail.NoOfLikes = data.getIntExtra("LIKE_COUNT_BLOG", 0);
                    setValues(mBlogDetail);
                }
                break;

            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private ViewPagerAdapter mViewPagerAdapter = null;

    protected void setValues(final BlogModel mBlogDetail) {
        if (null != mBlogDetail) {
            Calendar calendar = getCalTime(mBlogDetail.CreatedDate);
            if (null != calendar) {
                mVHolder.mTimeTV.setText(CalStartEndUtil.getTimeInformatedform(mContext, calendar));
            } else {
                mVHolder.mTimeTV.setText(mBlogDetail.CreatedDate);
            }
            mVHolder.mTitleTV.setText(mBlogDetail.Title);
            mVHolder.mAutorTV.setText(mContext.getResources().getString(R.string.By) + " " +mBlogDetail.Author);
            mVHolder.mLikesCountTV.setText(mBlogDetail.NoOfLikes + ""/*
                    + (mContext.getResources().getString(1 == mBlogDetail.NoOfLikes ? R.string.Like : R.string.Likes))*/);
            mVHolder.mCommentsCountTV.setText(mBlogDetail.NoOfComments + ""/*
					+ (mContext.getResources().getString(1 == mBlogDetail.NoOfComments ? R.string.Comment : R.string.Comments))*/);
            if (null != mBlogDetail.mConCoverMediaModel && null != mBlogDetail.mConCoverMediaModel.ImageName) {

                mCoverImagePath = Config.getCoverMediaPathBLOG(mBlogDetail.mConCoverMediaModel.ImageName);
                ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, mCoverImagePath, mVHolder.mCoverIV,
                        ImageLoaderUniversal.DiaplayOptionForProgresser, mVHolder.mLoaderCoverPB);
                mVHolder.mCoverivRL.setVisibility(View.VISIBLE);
            } else {
                mVHolder.mCoverivRL.setVisibility(View.GONE);
            }
            mVHolder.mDescriptionTV.setText(Html.fromHtml(mBlogDetail.Description));

            if (null != mBlogDetail.mMediaList && mBlogDetail.mMediaList.size() == 1) {
                mVHolder.mContainerMediaRl.setVisibility(View.VISIBLE);
                mVHolder.mWallPostIV.setVisibility(View.VISIBLE);
                LayoutParams layoutParams = mVHolder.mWallPostIV.getLayoutParams();
                layoutParams.width = (int) (mImgWidth - mExtraSpace * 3.5f);
                layoutParams.height = layoutParams.width;
                mVHolder.mWallPostIV.setLayoutParams(layoutParams);

                if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(mBlogDetail.mMediaList.get(0).MediaType)) {
                    mVHolder.mPlayIV.setVisibility(View.GONE);
                    mVHolder.mLoaderUIL.setVisibility(View.VISIBLE);

                    String imagePath = Config.getMediaPathBLOG(mBlogDetail.mMediaList.get(0).ImageName);
                    ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, imagePath, mVHolder.mWallPostIV,
                            ImageLoaderUniversal.DiaplayOptionForProgresser, mVHolder.mLoaderUIL);
                    mCoverImagePath = (TextUtils.isEmpty(mCoverImagePath) ? imagePath : mCoverImagePath);
                } else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(mBlogDetail.mMediaList.get(0).MediaType)) {
                    mVHolder.mPlayIV.setVisibility(View.VISIBLE);
                    mVHolder.mWallPostIV.setVisibility(View.VISIBLE);
                    String imagePath = Config.getBlogVideopath(mBlogDetail.mMediaList.get(0).ImageName).replaceAll("mp4", "jpg");
                    ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, imagePath, mVHolder.mWallPostIV,
                            ImageLoaderUniversal.DiaplayOptionForProgresser, mVHolder.mLoaderUIL);
                    mCoverImagePath = (TextUtils.isEmpty(mCoverImagePath) ? imagePath : mCoverImagePath);
                } else if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(mBlogDetail.mMediaList.get(0).MediaType)) {
                    mVHolder.mPlayIV.setVisibility(View.VISIBLE);
                    mVHolder.mWallPostIV.setVisibility(View.VISIBLE);
                    String imagePath = MediaViewerActivity.getYoutubeVideoThumb(mBlogDetail.mMediaList.get(0).ImageName);
                    ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext,
                            imagePath, mVHolder.mWallPostIV,
                            ImageLoaderUniversal.DiaplayOptionForProgresser, mVHolder.mLoaderUIL);
                    mCoverImagePath = (TextUtils.isEmpty(mCoverImagePath) ? imagePath : mCoverImagePath);
                }

            } else if (null != mBlogDetail.mMediaList && mBlogDetail.mMediaList.size() > 1) {
                mVHolder.mContainerMediaRl.setVisibility(View.VISIBLE);
                mVHolder.mWallPostIV.setVisibility(View.GONE);
                // if (null != mViewPagerAdapter) {

                // }
                if (null == mViewPagerAdapter) {
                    mViewPagerAdapter = new ViewPagerAdapter(mBlogDetail.mMediaList);
                    LayoutParams layoutParams = mVHolder.mWallPostVP.getLayoutParams();
                    layoutParams.width = (int) LayoutParams.MATCH_PARENT;
                    layoutParams.height = (int) (mImgWidth * 0.66f);
                    mVHolder.mWallPostVP.setPageMargin(20);
                    mVHolder.mWallPostVP.setLayoutParams(layoutParams);
                    mVHolder.mWallPostVP.setAdapter(mViewPagerAdapter);
                }

            } else {
                mVHolder.mContainerMediaRl.setVisibility(View.GONE);
            }

            mVHolder.mWallPostIV.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    ArrayList<AlbumMedia> mAlbumMediasFromServer = new ArrayList<AlbumMedia>();
                    mAlbumMediasFromServer.addAll(mBlogDetail.mMediaList);
                    startActivity(MediaViewerWallActivity.getIntent(mContext, 0, null, true,
                            mBlogDetail.mMediaList.get(0).MediaGUID, true, mAlbumMediasFromServer));
                    BlogDetailActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
            mVHolder.mLikeTV.setSelected(mBlogDetail.IsLike == 0 ? false : true);
            Intent in = new Intent();
            in.putExtra("LIKE_COUNT_BLOG", mBlogDetail.NoOfLikes);
            in.putExtra("BLOG_ISLIKE", mBlogDetail.IsLike);
            in.putExtra("COMMENT_COUNT_BLOG", mBlogDetail.NoOfComments);
            in.putExtra("BLOG_GUID", mBlogDetail.BlogGUID);
            setResult(RESULT_OK, in);
        }
    }

    private String removeParaTag(String text) {

        if (text.startsWith("<p>")) {
            text = text.substring(3, text.length());
            text = text.substring(0, text.length() - 4);
        }
        return text.toString();
    }

    public Calendar getCalTime(String dateString) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            cal.setTime(sdf.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TimeZone tz = cal.getTimeZone();
        int Offset = tz.getOffset(cal.getTimeInMillis());
        cal.setTimeInMillis(cal.getTimeInMillis() + Offset);
        return cal;
    }

    private void getIntetData() {
        mBlogID = getIntent().getStringExtra("blodID");
        if (null == mBlogID || mBlogID.equals("")) {
            return;
        }
    }

    private float mExtraSpace, mImgWidth;

    private void initViews() {

        int[] screenDimen = new int[2];
        DisplayUtil.probeScreenSize(mContext, screenDimen);
        mExtraSpace = mContext.getResources().getDimension(R.dimen.space_mid10);
        mImgWidth = screenDimen[0] - mExtraSpace;
        mBlogDetailRequest = new BlogDetailRequest(mContext);
        mVHolder = new VHolder(findViewById(R.id.container_rl));
        mErrorLayout = new ErrorLayout(findViewById(R.id.include_error));
    }

    private ErrorLayout mErrorLayout;

    public class VHolder {

        private HeaderLayout mHeaderLayout;
        private TextView mTimeTV, mTitleTV, mAutorTV, mDescriptionTV, mLikesCountTV, mCommentsCountTV, mSharesCountTV;
        private ImageView mLikeTV, mCommentTV, mShareTV;
        private RelativeLayout mContainerMediaRl;
        private ImageView mCoverIV;
        private ViewPager mWallPostVP;
        private ImageView mWallPostIV, mPlayIV;
        private ProgressBar mLoaderUIL, mLoaderCoverPB;
        private RelativeLayout mCoverivRL;

        public VHolder(View view) {

            mHeaderLayout = new HeaderLayout(view.findViewById(R.id.header_layout));
            mHeaderLayout.setHeaderValues(R.drawable.icon_back, getString(R.string.Blog_detail), 0);
            mHeaderLayout.setListenerItI(BlogDetailActivity.this, BlogDetailActivity.this);

            mTimeTV = (TextView) view.findViewById(R.id.time_tv);
            mTitleTV = (TextView) view.findViewById(R.id.title_tv);
            mAutorTV = (TextView) view.findViewById(R.id.author_tv);
            mDescriptionTV = (TextView) view.findViewById(R.id.description_tv);
            mLikesCountTV = (TextView) view.findViewById(R.id.likes_tv);
            mCommentsCountTV = (TextView) view.findViewById(R.id.comments_tv);
            mSharesCountTV = (TextView) view.findViewById(R.id.shares_tv);
            mCoverIV = (ImageView) view.findViewById(R.id.cover_iv);
            mLikeTV = (ImageView) view.findViewById(R.id.like_tv);
            mCommentTV = (ImageView) view.findViewById(R.id.comment_tv);
            mShareTV = (ImageView) view.findViewById(R.id.share_tv);
            mContainerMediaRl = (RelativeLayout) view.findViewById(R.id.container_media);
            mWallPostVP = (ViewPager) view.findViewById(R.id.wall_post_vp);
            mWallPostVP.setPageMargin((int) view.getResources().getDimension(R.dimen.space_tinny2));
            mWallPostIV = (ImageView) view.findViewById(R.id.wall_post_iv);
            mPlayIV = (ImageView) view.findViewById(R.id.play_iv);
            mLoaderUIL = (ProgressBar) view.findViewById(R.id.loading_pb);
            mLoaderCoverPB = (ProgressBar) view.findViewById(R.id.loading_cover_pb);
            mCoverivRL = (RelativeLayout) view.findViewById(R.id.cover_image_rl);
            mDescriptionTV.setMovementMethod(LinkMovementMethod.getInstance());

            mCoverIV.setOnClickListener(BlogDetailActivity.this);
            mLikesCountTV.setOnClickListener(BlogDetailActivity.this);
            mCommentsCountTV.setOnClickListener(BlogDetailActivity.this);
            mLikeTV.setOnClickListener(BlogDetailActivity.this);
            mCommentTV.setOnClickListener(BlogDetailActivity.this);
            FontLoader.setRobotoMediumTypeface(mTitleTV);
            FontLoader.setRobotoRegularTypeface(mTimeTV, mAutorTV, mDescriptionTV, mLikesCountTV,
                    mCommentsCountTV, mSharesCountTV/*, mLikeTV, mCommentTV*/);
        }
    }

    private ToggleLikeRequest mToggleLikeRequest;

    public void LikeMediaToggleService() {
        mToggleLikeRequest = new ToggleLikeRequest(mContext);
        mToggleLikeRequest.toggleLikeInServer(mBlogDetail.BlogGUID, "BLOG");
        mToggleLikeRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    if (mBlogDetail.IsLike == 0) {
                        mBlogDetail.NoOfLikes++;
                        mBlogDetail.IsLike = 1;
                    } else {
                        mBlogDetail.NoOfLikes--;
                        mBlogDetail.IsLike = 0;
                    }
                    setValues(mBlogDetail);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) :
                            (String) data), true, MsgType.Error);
                }
            }
        });

    }

    public static Intent getIntent(Context mContext, String blodID) {
        Intent intent = new Intent(mContext, BlogDetailActivity.class);
        intent.putExtra("blodID", blodID);
        return intent;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_button:
                this.finish();
                break;
            case R.id.cover_iv:
                startActivity(SingleMediaViewWallActivity.getIntent(this, mBlogDetail.mConCoverMediaModel.ImageName, "IMAGE", false));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.like_tv:
                DisplayUtil.bounceView(mContext, mVHolder.mLikeTV);
                LikeMediaToggleService();
                break;
            case R.id.comment_tv:
                startActivityForResult(WriteCommentActivity.getIntentBlog(mContext, mBlogDetail.BlogGUID, "BLOG", mBlogDetail.Title, mCoverImagePath),
                        WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY);
                BlogDetailActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                break;
            case R.id.likes_tv: {

                startActivityForResult(LikeListActivity.getIntent(mContext, mBlogDetail.BlogGUID, CommentListRequest.ENTITYTYPE_BLOG, "BLOG",
                        mBlogDetail.IsLike), LikeListActivity.REQ_FROM_HOME_LIKELIST);
                BlogDetailActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
            }
            break;

            case R.id.comments_tv:
                startActivityForResult(CommentListActivity.getIntentBlog(mContext, mBlogDetail.BlogGUID, mBlogDetail.Title, mCoverImagePath),
                        CommentListActivity.REQ_CODE_COMMENT_LIST);
                BlogDetailActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                break;

            default:
                break;
        }

    }

    private class ViewPagerAdapter extends PagerAdapter {

        LayoutInflater li;
        List<AlbumMedia> mListMediasPager;
        private ImageView mMediaIV;
        private ProgressBar mLoaderUIL;
        View vl;
        private ImageButton mPlayButton;

        public ViewPagerAdapter(List<AlbumMedia> _mListMediasPager) {
            if (null != _mListMediasPager) {
                mListMediasPager = _mListMediasPager;

            }
        }

        public String getMediaURl() {
            if (null != mListMediasPager && mListMediasPager.size() > 0) {
                return Config.IMAGE_URL_UPLOAD + Config.ALBUM_FOLDER + mListMediasPager.get(0).ImageName;
            }
            return "";
        }

        @Override
        public float getPageWidth(int position) {
            return 0.90f;
        }

        public int getCount() {
            return null == mListMediasPager ? 0 : mListMediasPager.size();
        }

        public Object instantiateItem(View collection, final int position) {
            li = BlogDetailActivity.this.getLayoutInflater();
            vl = li.inflate(R.layout.wall_post_pager_items, null);
            mMediaIV = (ImageView) vl.findViewById(R.id.media_pager_iv);
            mLoaderUIL = (ProgressBar) vl.findViewById(R.id.loading_pb);
            mPlayButton = (ImageButton) vl.findViewById(R.id.play_btn);
            AlbumMedia albumMedia = mListMediasPager.get(position);

            if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(albumMedia.MediaType)) {

                String imagePath = Config.getMediaPathBLOG(albumMedia.ImageName);
                ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, imagePath, mMediaIV,
                        ImageLoaderUniversal.DiaplayOptionForProgresser, mLoaderUIL);

                mPlayButton.setVisibility(View.GONE);
            } else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(albumMedia.MediaType)) {
                mPlayButton.setVisibility(View.VISIBLE);

                String thumbUrl = Config.getBlogVideopath(albumMedia.ImageName).replaceAll("mp4", "jpg");
                ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, thumbUrl, mMediaIV,
                        ImageLoaderUniversal.DiaplayOptionForProgresser, mLoaderUIL);

            } else if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(albumMedia.MediaType)) {
                mPlayButton.setVisibility(View.VISIBLE);
                ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext,
                        MediaViewerActivity.getYoutubeVideoThumb(albumMedia.ImageName), mMediaIV,
                        ImageLoaderUniversal.DiaplayOptionForProgresser, mLoaderUIL);
            }

            mMediaIV.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    ArrayList<AlbumMedia> mAlbumMediasFromServer = new ArrayList<AlbumMedia>();
                    mAlbumMediasFromServer.addAll(mBlogDetail.mMediaList);
                    startActivity(MediaViewerWallActivity.getIntent(mContext, position, null, true,
                            mBlogDetail.mMediaList.get(position).MediaGUID, true, mAlbumMediasFromServer));
                    BlogDetailActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                }
            });

            ((ViewPager) collection).addView(vl, 0);
            return vl;
        }

        @Override
        public void destroyItem(View arg0, int arg1, Object arg2) {

        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {

        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

}
