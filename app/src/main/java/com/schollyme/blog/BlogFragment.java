package com.schollyme.blog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.adapter.BlogListAdapter;
import com.schollyme.adapter.BlogListAdapter.OnItemClickBlogList;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.BlogModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewsFeed;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.BlogListRequest;
import com.vinfotech.request.CommentListRequest;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
import java.util.List;

public class BlogFragment extends BaseFragment implements OnClickListener, OnRefreshListener {

    private TextView mNoRecordMessageTV;
    private ListView mBlogLV;
    private Context mContext;
    private int mPageNumber = 1;
    private BlogListAdapter mBlogListAdapter;
    private ProgressBar mLoadingCenter;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private ProgressBar mLoaderBottomPB;
    private BlogListRequest mBlogListRequest;
    private RelativeLayout mSearchBlodView;
    private EditText mSearchBox;
    private View mMainView;
    private View mFooterRL;
    private TextView mNoMoreTV;
    private ArrayList<BlogModel> mBlogList;

    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;

    public static BaseFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        BlogFragment mFragment = new BlogFragment();
        mFragment.mCurrentPageToShow = currentPageToShow;
        mFragment.mHeaderLayout = headerLayout;
        mFragment.mShowBackBtn = showBack;

        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMainView = inflater.inflate(R.layout.blogs_fragment, container, false);

        setHeader();
        initViews(mMainView);

        mLoadingCenter.post(new Runnable() {
            @Override
            public void run() {
                getBlogList(mPageNumber, mSearchBox.getText().toString());
            }
        });

        return mMainView;
    }

    private void getBlogList(int pageIndex2, String string) {

        isLoading = false;
        mNoMoreTV.setVisibility(View.INVISIBLE);

        if (mSwipeRefreshWidget.isRefreshing()) {
            mBlogListRequest.setLoader(null);
        } else {
            mBlogListRequest.setLoader(pageIndex2 <= 1 ? mLoadingCenter : mLoaderBottomPB);
        }

        mBlogListRequest.getBlogListInServer(string, "" + pageIndex2);
        mBlogListRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    if (mPageNumber == 1) {
                        resetList();
                    }
                    List<BlogModel> bloglistcurrent = (List<BlogModel>) data;
                    mBlogList.addAll(bloglistcurrent);
                    mBlogListAdapter.setList(mBlogList);

                    if (mPageNumber == 1) {
                        mNoMoreTV.setVisibility(View.INVISIBLE);
                    } else {
                        mNoMoreTV.setVisibility(View.VISIBLE);
                    }

                    if (totalRecords == 0) {
                        isLoading = false;
                        mNoRecordMessageTV.setVisibility(View.VISIBLE);

                    } else if (mBlogList.size() < totalRecords) {
                        mNoRecordMessageTV.setVisibility(View.GONE);
                        isLoading = true;
                        mPageNumber++;

                    } else {
                        isLoading = false;
                        mNoRecordMessageTV.setVisibility(View.GONE);
                    }

                } else {
                    isLoading = false;
                    mNoRecordMessageTV.setVisibility(View.GONE);
                    mNoMoreTV.setVisibility(View.INVISIBLE);
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                    // mErrorLayout.showError((null == data ?
                    // getString(R.string.Something_went_wrong) : (String)
                    // data), true, MsgType.Error);

                }
                mSwipeRefreshWidget.setRefreshing(false);
            }

        });
    }

    private void resetList() {
        mPageNumber = 1;
        mBlogList.clear();
        mBlogListAdapter.setList(mBlogList);
    }

    private void setHeader() {

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                getResources().getString(R.string.Blog), R.drawable.ic_search_header);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashboardActivity) getActivity()).sliderListener();
            }
        }, new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSearchBlodView.getVisibility() == View.VISIBLE) {
                    mSearchBlodView.setVisibility(View.GONE);
                } else {
                    mSearchBlodView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private ErrorLayout mErrorLayout;

    private void initViews(View view) {

        mContext = getActivity();
        mErrorLayout = new ErrorLayout(view.findViewById(R.id.bg_container_rl));
        mBlogLV = (ListView) view.findViewById(R.id.blog_lv);
        mSearchBlodView = (RelativeLayout) view.findViewById(R.id.search_view_ll);
        mSearchBox = (EditText) view.findViewById(R.id.search_et);
        mLoadingCenter = (ProgressBar) view.findViewById(R.id.loading_center_pb);
        mNoRecordMessageTV = (TextView) view.findViewById(R.id.no_record_message_tv);
        mLoaderBottomPB = (ProgressBar) view.findViewById(R.id.loading_bottom_pb);
        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);

        mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);
        mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
        mNoMoreTV.setVisibility(View.INVISIBLE);
        mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
        mLoaderBottomPB.setVisibility(View.INVISIBLE);
        mLoaderBottomPB.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.red_normal_color),
                PorterDuff.Mode.SRC_IN);
        mBlogLV.addFooterView(mFooterRL);

        new SearchHandler(mSearchBox).setSearchListener(new SearchListener() {
            @Override
            public void onSearch(String text) {
                mPageNumber = 1;
                getBlogList(mPageNumber, mSearchBox.getText().toString());
            }
        }).setClearView(view.findViewById(R.id.clear_text_ib));

        // mBlogListAdapter = new BlogListAdapter(mContext);
        mBlogListAdapter = new BlogListAdapter(mContext, new OnItemClickBlogList() {
            @Override
            public void onClickItems(int ID, int position, BlogModel blogmodel) {
                switch (ID) {
                    case 5555: {
                        startActivityForResult(BlogDetailActivity.getIntent(mContext, blogmodel.BlogGUID),
                                BlogDetailActivity.REQ_CODE_BLOGDETAIL_SCREEN);
                    }
                    break;
                    case R.id.comments_tv: {
                      /*startActivityForResult(CommentListActivity.getIntentBlog(
                      mContext, blogmodel.BlogGUID, blogmodel.Title),
					  CommentListActivity.REQ_CODE_COMMENT_LIST);
					  getActivity().
					  overridePendingTransition(R.anim.slide_up_in,
					  R.anim.slide_down_in);*/
                        startActivityForResult(CommentListActivity.getIntentBlog(mContext, blogmodel.BlogGUID,
                                blogmodel.Title, blogmodel.mConCoverMediaModel.ImageName),
                                CommentListActivity.REQ_CODE_COMMENT_LIST);
                    }
                    break;

                    case R.id.likes_tv: {
                        startActivityForResult(LikeListActivity.getIntent(mContext
                                , blogmodel.BlogGUID, CommentListRequest.ENTITYTYPE_BLOG,
                                "BLOG", blogmodel.IsLike),
                                LikeListActivity.REQ_FROM_HOME_LIKELIST);
                    }
                    break;
                    case R.id.comments_iv:
                        startActivityForResult(WriteCommentActivity.getIntentBlog(mContext, blogmodel.BlogGUID,
                                "BLOG", blogmodel.Title, blogmodel.mConCoverMediaModel.ImageName),
                                WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY);
                        break;
                    case R.id.shares_iv:

//                        if (blogmodel.ShareAllowed == 1) {
//                            sharePost(blogmodel);
//                        } else {
//                            Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
//                        }
                        break;
                    default:
                        break;
                }
            }
        });
        mBlogListRequest = new BlogListRequest(mContext);
        mBlogList = new ArrayList<BlogModel>();
        mBlogLV.setAdapter(mBlogListAdapter);
        mSwipeRefreshWidget.setColorScheme(R.color.red_normal_color);
        mSwipeRefreshWidget.setOnRefreshListener(this);
        FontLoader.setRobotoRegularTypeface(mNoMoreTV, mNoRecordMessageTV, mSearchBox);
        mBlogLV.setOnScrollListener(new MyCustomonScroll());
        // mBlogLV.setOnItemClickListener(new OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view, int
        // position, long id) {
        // startAc
        // }
        // });
    }

    public void sharePost(NewsFeed newsFeedModel) {

        SharePostRequest mSharePostRequest = new SharePostRequest(mContext);
        mSharePostRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, MsgType.Success);
                } else {
                    mErrorLayout.showError((String) data, true, MsgType.Error);
                }
            }
        });
        LogedInUserModel mLogedInUserModel = new LogedInUserModel();
        mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "BLOG", newsFeedModel.PostContent,
                NewsFeedRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID, 1, 1);
    }

    @Override
    public void onClick(View arg0) {

    }

    @Override
    public void onRefresh() {
        mPageNumber = 1;
        getBlogList(mPageNumber, mSearchBox.getText().toString());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case BlogDetailActivity.REQ_CODE_BLOGDETAIL_SCREEN:
                if (resultCode == Activity.RESULT_OK && null != data) {
                    int updatePosition = 0;
                    String blogGUID = data.getStringExtra("BLOG_GUID");
                    for (int i = 0; i < mBlogList.size(); i++) {
                        if (blogGUID.equals(mBlogList.get(i).BlogGUID)) {
                            updatePosition = i;
                            break;
                        }
                    }
                    mBlogList.get(updatePosition).NoOfLikes = data.getIntExtra("LIKE_COUNT_BLOG", 0);
                    mBlogList.get(updatePosition).NoOfComments = data.getIntExtra("COMMENT_COUNT_BLOG", 0);
                    mBlogList.get(updatePosition).IsLike = data.getIntExtra("BLOG_ISLIKE", 0);
                    mBlogListAdapter.setList(mBlogList);
                    mBlogListAdapter.notifyDataSetChanged();
                }
                break;

            case CommentListActivity.REQ_CODE_COMMENT_LIST:
                if (resultCode == Activity.RESULT_OK && null != data) {

                    int updatePosition = 0;
                    String blogGUID = data.getStringExtra("BLOG_GUID");
                    for (int i = 0; i < mBlogList.size(); i++) {
                        if (blogGUID.equals(mBlogList.get(i).BlogGUID)) {
                            updatePosition = i;
                            break;
                        }
                    }
                    mBlogList.get(updatePosition).NoOfComments = data.getIntExtra("COMMENT_COUNT_BLOG", 0);
                    mBlogListAdapter.setList(mBlogList);

                }
                break;

            case LikeListActivity.REQ_FROM_HOME_LIKELIST:
                if (resultCode == Activity.RESULT_OK && null != data) {
                    int updatePosition = 0;
                    String blogGUID = data.getStringExtra("BLOG_GUID");
                    for (int i = 0; i < mBlogList.size(); i++) {
                        if (blogGUID.equals(mBlogList.get(i).BlogGUID)) {
                            updatePosition = i;
                            break;
                        }
                    }
                    mBlogList.get(updatePosition).NoOfLikes = data.getIntExtra("LIKE_COUNT_BLOG", 0);
                    mBlogList.get(updatePosition).IsLike = data.getIntExtra("BLOG_ISLIKE", 0);
                    mBlogListAdapter.setList(mBlogList);
                }
                break;
            case WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY:
                if (resultCode == Activity.RESULT_OK && null != data) {
                    int updatePosition = 0;
                    String blogGUID = data.getStringExtra("BLOG_GUID");
                    for (int i = 0; i < mBlogList.size(); i++) {
                        if (blogGUID.equals(mBlogList.get(i).BlogGUID)) {
                            updatePosition = i;
                            break;
                        }
                    }
                    mBlogList.get(updatePosition).NoOfComments++;
                    mBlogListAdapter.setList(mBlogList);
                }
                break;

            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public int getConnectivityStatus(Context context) {
        if (null == context) {
            return 0;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (null != activeNetwork && activeNetwork.isConnected()) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return 1;
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return 2;
            }
        }
        return 0;
    }

    boolean isLoading;

    class MyCustomonScroll implements OnScrollListener {
        private boolean bReachedListEnd;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (bReachedListEnd && isLoading) {

                getBlogList(mPageNumber, mSearchBox.getText().toString());
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);

        }
    }

}