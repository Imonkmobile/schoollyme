package com.schollyme.friends;

import android.support.v4.app.Fragment;

public abstract class BaseFriendFragment extends Fragment {
	public abstract void onReload();
	public abstract void onSearchBtnClick();
}