package com.schollyme.friends;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.adapter.OtherUsersFriendAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.FriendModel;
import com.vinfotech.request.FriendsListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;

public class OtherUserFriendsActivity extends BaseActivity implements OnRefreshListener,OnClickListener{

	private Context mContext;
	private ListView mFriendsSuggestionsAL;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private LinearLayout mLinearLayout;
	private EditText mFriendSearchET;
	private ImageButton mClearIB;
	private TextView mNoRecordTV;
	private OtherUsersFriendAdapter mAdapter;
	private ArrayList<FriendModel> mFriendsDataAL;
	private boolean loadingFlag = false;
	private static ErrorLayout mErrorLayout;
	private int pageIndex = 1;
	private String mUserId;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.other_users_friends_activity);
		setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, 0,
				getResources().getString(R.string.friends_header), new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		}, null);

		mContext = this;
		mFriendsSuggestionsAL = (ListView) findViewById(R.id.friends_lv);
		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		mFriendSearchET = (EditText) findViewById(R.id.search_et);
		mClearIB = (ImageButton) findViewById(R.id.clear_text_ib);
		mLinearLayout = (LinearLayout) findViewById(R.id.search_view_ll);
		mNoRecordTV = (TextView) findViewById(R.id.no_record_message_tv);
		mErrorLayout = new ErrorLayout(findViewById(R.id.main_ll));
		mLinearLayout.setVisibility(View.VISIBLE);
		mFriendSearchET.addTextChangedListener(new CustomTextWatcher());
		mClearIB.setOnClickListener(this);
		FontLoader.setRobotoRegularTypeface(mFriendSearchET,mNoRecordTV);
		mUserId = getIntent().getStringExtra("UserGUID");
		mFriendsDataAL = new ArrayList<FriendModel>();
		pageIndex = 1;
		getFriendsRequest(mUserId);
	}

	private void setSuggestedFriendsAdapter(ArrayList<FriendModel> mSuggestionsAL){
		mAdapter = new OtherUsersFriendAdapter(mContext,mErrorLayout);
		if(mSuggestionsAL.size()==0){
			mFriendsSuggestionsAL.setVisibility(View.GONE);
			mSwipeRefreshWidget.setVisibility(View.GONE);
			mNoRecordTV.setVisibility(View.VISIBLE);
		}
		else{
			mNoRecordTV.setVisibility(View.GONE);
			mSwipeRefreshWidget.setVisibility(View.VISIBLE);
			mFriendsSuggestionsAL.setVisibility(View.VISIBLE);
			mAdapter.setList(mSuggestionsAL);
			mFriendsSuggestionsAL.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();
			mFriendsSuggestionsAL.setOnScrollListener(new OnScrollListener() {
				boolean bReachedListEnd = false;
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
						Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
						if (null != mAdapter&& !loadingFlag) {
							pageIndex++;
							loadingFlag = true;
							getFriendsRequest(mUserId);
						}
					}
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
					bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount/2));
				}
			});
		}
		mFriendsDataAL = mSuggestionsAL;
	}

	public static Intent getIntent(Context context,String mUserGUID) {
		Intent intent = new Intent(context, OtherUserFriendsActivity.class);
		intent.putExtra("UserGUID", mUserGUID);
		return intent;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.clear_text_ib:
			mFriendSearchET.setText("");
			if(mAdapter!=null){
				mAdapter.setList(mFriendsDataAL);
				mAdapter.notifyDataSetChanged();
			}
			break;
		}
	}

	@Override
	public void onRefresh() {
		pageIndex = 1;
		mFriendsDataAL = new ArrayList<FriendModel>();
		getFriendsRequest(mUserId);
	}

	private class CustomTextWatcher implements TextWatcher{

		@Override
		public void afterTextChanged(Editable s) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if(s.length()>0){
				mClearIB.setVisibility(View.VISIBLE);
				ArrayList<FriendModel> modelAL = new ArrayList<FriendModel>();
				for(int k=0;k<mFriendsDataAL.size();k++){
					if(mFriendsDataAL.get(k).mFirstName.toLowerCase().contains(s.toString().toLowerCase()) || mFriendsDataAL.get(k).mLastName.toLowerCase().contains(s.toString().toLowerCase())){
						modelAL.add(mFriendsDataAL.get(k));
					}
				}
				if(modelAL.size()>0){
					mAdapter.setList(modelAL);
					mAdapter.notifyDataSetChanged();
					mNoRecordTV.setVisibility(View.GONE);
					mFriendsSuggestionsAL.setVisibility(View.VISIBLE);
					mSwipeRefreshWidget.setVisibility(View.VISIBLE);
				}
				else{
					mNoRecordTV.setVisibility(View.VISIBLE);
					mFriendsSuggestionsAL.setVisibility(View.GONE);
					mSwipeRefreshWidget.setVisibility(View.GONE);
				}
			}
			else{
				mClearIB.setVisibility(View.GONE);
				if(mAdapter!=null){
					mAdapter.setList(mFriendsDataAL);
					mAdapter.notifyDataSetChanged();
					mNoRecordTV.setVisibility(View.GONE);
					mFriendsSuggestionsAL.setVisibility(View.VISIBLE);
					mSwipeRefreshWidget.setVisibility(View.VISIBLE);
				}
			}
		}
	}

	private void getFriendsRequest(String userGUID){

		FriendsListRequest mRequest = new FriendsListRequest(mContext);
		mRequest.FriendsListServerRequest(userGUID, "Friends", "FirstName", "ASC", String.valueOf(pageIndex), String.valueOf(Config.PAGINATION_PAGE_SIZE));
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if(success){
					mFriendsDataAL.addAll((ArrayList<FriendModel>) data);
					if(mAdapter==null){
						setSuggestedFriendsAdapter(mFriendsDataAL);
					}
					else{
						mAdapter.setList(mFriendsDataAL);
					}
				}
				else{
					final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
							getResources().getString(R.string.app_name), new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {
							finish();
						}
					});
					builder.setCancelable(false);
					builder.setOnKeyListener(new Dialog.OnKeyListener() {

						@Override
						public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
							if (keyCode == KeyEvent.KEYCODE_BACK) {
								finish();
								builder.dismiss();
							}
							return true;
						}
					});
				}
				mSwipeRefreshWidget.setRefreshing(false);
			}
		});
	}
}