package com.schollyme.friends;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nirhart.parallaxscroll.views.ParallaxListView;
import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.activity.CreateWallPostActivity;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.schollyme.model.UserModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 30/6/2017.
 */

public class FriendsFeedFragment extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private Context mContext;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private UserModel mUserModel;
    private LogedInUserModel loggedinUserModel;
    private String mOtherGUID;

    private PostFeedAdapter mPostFeedAdapter;
    private ParallaxListView genericLv;
    private TextView mNoFeedTV;
    private View mFooterRL;
    private TextView mNoMoreTV;
    private ProgressBar mLoaderBottomPB, mLoadingCenter;
    private NFFilter mNFFilter;
    private List<NewsFeed> mNewsFeeds = new ArrayList<>();
    final private int REQ_FROM_HOME_LIKELIST = 5555;
    final private int REQ_FROM_HOME_COMMENTLIST = 5556;
    private int ClickLocation = 0;

    public static FriendsFeedFragment newInstance(UserModel mUserModel, LogedInUserModel loggedinUserModel, String mOtherGUID) {
        FriendsFeedFragment friendsFeedFragment = new FriendsFeedFragment();
        friendsFeedFragment.mUserModel = mUserModel;
        friendsFeedFragment.loggedinUserModel = loggedinUserModel;
        friendsFeedFragment.mOtherGUID = mOtherGUID;
        return friendsFeedFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.friend_feed_list_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();

        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
        genericLv = (ParallaxListView) view.findViewById(R.id.list_view);
        mNoFeedTV = (TextView) view.findViewById(R.id.no_feed_tv);
        mNewsFeedRequest = new NewsFeedRequest(mContext);

        mNewsFeeds.add(new NewsFeed(null));
        mBlankView = (View) view.findViewById(R.id.blankView);

        mSwipeRefreshWidget.setColorScheme(R.color.verify_text_color);
        mNFFilter = new NFFilter(NewsFeedRequest.MODULE_ID_USERS, mOtherGUID);

        mSwipeRefreshWidget.setOnRefreshListener(this);
        mLoadingCenter = (ProgressBar) view.findViewById(R.id.loading_center_pb);
        mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);
        mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
        mNoMoreTV.setVisibility(View.INVISIBLE);
        mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
        mLoaderBottomPB.setVisibility(View.INVISIBLE);
        mLoaderBottomPB.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.verify_text_color), PorterDuff.Mode.SRC_IN);

        mPostFeedAdapter = new PostFeedAdapter(mContext, new PostFeedAdapter.OnItemClickListenerPost() {

            @Override
            public void onClickItems(int ID, int position, NewsFeed newsFeed) {
                ItemClickListner(ID, position, newsFeed);
            }
        }, mOtherGUID);

        genericLv.setOnScrollListener(new MyCustomonScrol());
        genericLv.setAdapter(mPostFeedAdapter);
        genericLv.addFooterView(mFooterRL);
        // genericLv.addParallaxedHeaderView(mUserProfileActivityViewHolder.mUserProfileView);
        getFeedRequest(mNFFilter);
    }

    private View mBlankView;
    NewsFeedRequest mNewsFeedRequest;

    private synchronized void getFeedRequest(final NFFilter nffilter) {
        System.out.println(" nffilter :    "+nffilter);
        mNoMoreTV.setVisibility(View.INVISIBLE);
        if (nffilter.PageNo == 1) {
            if (mPostFeedAdapter.getCount() <= 1 || mPostFeedAdapter == null) {
                mNewsFeedRequest.setLoader(mLoadingCenter);
            } else {
                mNewsFeedRequest.setLoader(mBlankView);
            }
            mLoaderBottomPB.setVisibility(View.INVISIBLE);

        } else {
            mNewsFeedRequest.setLoader(mLoaderBottomPB);
            mLoaderBottomPB.setVisibility(View.VISIBLE);
        }
        mNewsFeedRequest.getNewsFeedListInServer(nffilter);
        mNewsFeedRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mSwipeRefreshWidget.setRefreshing(false);
                System.out.println(" nffilter :    "+success);
                if (success) {

                    if (null != data) {
                        if (mNFFilter.PageNo == 1) {
                            resetList();
                        }
                        List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
                        mNewsFeeds.addAll(newsFeeds);
                        mPostFeedAdapter.setList(mNewsFeeds);

                        if (totalRecords == 0) {
                            isLoading = false;
                            mNoFeedTV.setVisibility(View.VISIBLE);

                        } else if (mNewsFeeds.size() < totalRecords) {
                            isLoading = true;
                            int newPage = mNFFilter.PageNo + 1;
                            mNFFilter.PageNo = newPage;
                            mNoFeedTV.setVisibility(View.GONE);

                        } else {
                            mNoMoreTV.setVisibility(View.VISIBLE);
                            isLoading = false;
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                        if (mNFFilter.PageNo == 1) {
                            mNoMoreTV.setVisibility(View.INVISIBLE);
                        } else {
                            mNoMoreTV.setVisibility(View.VISIBLE);
                        }

                    }
                } else {
                    isLoading = false;

                }
            }
        });
    }

    public void ItemClickListner(int ID, int position, NewsFeed newsFeed) {
        ClickLocation = position;
        switch (ID) {
            case R.id.share_tv:

                if (newsFeed.ShareAllowed == 1) {
                    sharePost(newsFeed);
                } else {
                    Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.like_IV:
                LikeMediaToggleService(newsFeed, position);
                break;
            case R.id.likes_tv:
                if (newsFeed.NoOfLikes != 0) {
                    startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID, "ACTIVITY", "WALL"),
                            REQ_FROM_HOME_LIKELIST);
//                    mContext.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
                }
                break;

            //case R.id.comment_tv:
            case R.id.comments_tv:

                AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
                WriteCommentActivity.addFullPathIfNeeded(newsFeed, albumMedia);
                String captionForWriteCommentScreen = "";
                if (!newsFeed.PostContent.equals("")) {
                    captionForWriteCommentScreen = newsFeed.PostContent;
                } else {

                    if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
                    }
                }

                startActivityForResult(CommentListActivity.getIntent(mContext, albumMedia,
                        CommentListActivity.FOR_WALLPOST, captionForWriteCommentScreen),
                        REQ_FROM_HOME_COMMENTLIST);
//                this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

                break;

//            case PostFeedAdapter.ConvertViewID:
//
//                startActivityForResult(CreateWallPostActivity.getIntent(mContext, WallPostCreateRequest.MODULE_ID_USERS, mOtherGUID),
//                        CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
//                break;

            default:
                break;
        }
    }

    boolean isLoading;

    class MyCustomonScrol implements AbsListView.OnScrollListener {
        private int mLastFirstVisibleItem;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int k = visibleItemCount + firstVisibleItem;
            if (k >= totalItemCount && isLoading && totalItemCount != 0) {
                getFeedRequest(mNFFilter);
            }
            if (mLastFirstVisibleItem < firstVisibleItem) {
                Log.i("SCROLLING DOWN", "TRUE");
            }
            if (mLastFirstVisibleItem > firstVisibleItem) {
                Log.i("SCROLLING UP", "TRUE");
            }
            mLastFirstVisibleItem = firstVisibleItem;
        }
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onClick(View v) {

    }

    private void resetList() {
        if (mNFFilter != null)
            mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
        mNewsFeeds.clear();
        mNewsFeeds.add(new NewsFeed(null));
        if (mPostFeedAdapter != null)
            mPostFeedAdapter.setList(mNewsFeeds);
    }


    /**
     * API will be call when user like feed of friends
     */
    public void LikeMediaToggleService(final NewsFeed newsFeed, final int position) {

        ToggleLikeRequest mToggleLikeRequest = new ToggleLikeRequest(mContext);
        mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");

        mToggleLikeRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {

                    if (newsFeed.IsLike == 0) {

                        newsFeed.IsLike = 1;
                        newsFeed.NoOfLikes++;
                    } else {

                        newsFeed.IsLike = 0;
                        newsFeed.NoOfLikes--;
                    }
                    mPostFeedAdapter.notifyDataSetChanged();

                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), "");
                }

            }
        });
    }

    /**
     * Call method when loggedin user share friends wall feed on his wall
     */
    public void sharePost(NewsFeed newsFeedModel) {
        SharePostRequest mSharePostRequest = new SharePostRequest(mContext);
        mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
                NewsFeedRequest.MODULE_ID_USERS, loggedinUserModel.mUserGUID, 1, 1);
        mSharePostRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                /*if (success) {
                    mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, ErrorLayout.MsgType.Success);
                } else {
                    mErrorLayout.showError((String) data, true, ErrorLayout.MsgType.Error);
                }*/
            }
        });
    }
}
