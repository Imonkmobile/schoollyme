package com.schollyme.friends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.schollyme.activity.BaseActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.R;
import com.vinfotech.header.HeaderLayout;

public class FriendsActivity extends BaseActivity {
	private HeaderLayout mHeaderLayout;
	public String mFromActivity ="";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friends_activity);

		mFromActivity = getIntent().getStringExtra("fromActivity");
		
		mHeaderLayout = new HeaderLayout(findViewById(R.id.header_layout));

		int currentPageToShow = getIntent().getIntExtra("currentPageToShow", 0);

		getSupportFragmentManager().beginTransaction().add(R.id.container_review,
				FriendsTopFragment.newInstance(currentPageToShow, mHeaderLayout, true), "FriendTopFr")
				.commit();
	}

	public static Intent getIntent(Context context, int currentPageToShow, String fromActivity) {
		Intent intent = new Intent(context, FriendsActivity.class);
		intent.putExtra("currentPageToShow", currentPageToShow);
		intent.putExtra("fromActivity", fromActivity);
		return intent;
	}
	
	@Override
	public void onBackPressed() {
		
		FriendsTopFragment currentTodayFr = (FriendsTopFragment)getSupportFragmentManager().findFragmentByTag("FriendTopFr");
		if(currentTodayFr != null && currentTodayFr.isVisible()){
			if(mFromActivity.equalsIgnoreCase("GCMIntentService")){
				Intent intent = DashboardActivity.getIntent(this, 4, "FriendsTopFragment");
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			}
			else{
				super.onBackPressed();
			}
		}else
			super.onBackPressed();
	}
	
}