package com.schollyme.friends;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.adapter.FriendSuggestionsAdapter;
import com.schollyme.model.FriendModel;
import com.vinfotech.request.FriendsSuggestionsRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

public class FriendsSuggestionsActivity extends BaseActivity implements OnRefreshListener {

	private Context mContext;
	private ListView mFriendsSuggestionsAL;
	private TextView mNoRecordTV;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private FriendSuggestionsAdapter mAdapter;
	private ArrayList<FriendModel> mFriendsDataAL;
	private int pageIndex = 1;
	private boolean loadingFlag = false;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friend_suggestions_activity);
		mContext = this;
		mFriendsSuggestionsAL = (ListView) findViewById(R.id.friends_lv);
		mNoRecordTV = (TextView) findViewById(R.id.no_record_message_tv);
		FontLoader.setRobotoRegularTypeface(mNoRecordTV);
		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		pageIndex = 1;
		mFriendsDataAL = new ArrayList<FriendModel>();
		getSuggestedFriendRequest();
	}

	private void setSuggestedFriendsAdapter(ArrayList<FriendModel> mSuggestionsAL) {
		mAdapter = new FriendSuggestionsAdapter(mContext);
		if (mSuggestionsAL.size() == 0) {
			mFriendsSuggestionsAL.setVisibility(View.GONE);
			mNoRecordTV.setVisibility(View.VISIBLE);
		} else {
			mNoRecordTV.setVisibility(View.GONE);
			mFriendsSuggestionsAL.setVisibility(View.VISIBLE);
			mAdapter.setList(mSuggestionsAL);
			mFriendsSuggestionsAL.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();
			mFriendsSuggestionsAL.setOnScrollListener(new OnScrollListener() {
				boolean bReachedListEnd = false;

				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
						Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
						if (null != mAdapter && !loadingFlag) {
							pageIndex++;
							loadingFlag = true;
							getSuggestedFriendRequest();
						}
					}
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
					bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount / 2));
				}
			});
		}
	}

	public void getSuggestedFriendRequest() {
		FriendsSuggestionsRequest mRequest = new FriendsSuggestionsRequest(mContext);
		mRequest.FriendsSuggestionsServerRequest("FirstName", "ASC", String.valueOf(pageIndex), String.valueOf(Config.PAGINATION_PAGE_SIZE),mFriendsDataAL.size()==0?false:true);
		mRequest.setRequestListener(new RequestListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mFriendsDataAL.addAll((ArrayList<FriendModel>) data);
					setSuggestedFriendsAdapter(mFriendsDataAL);
				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), getResources().getString(R.string.app_name));
				}
				mSwipeRefreshWidget.setRefreshing(false);
			}
		});
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, FriendsSuggestionsActivity.class);
		return intent;
	}

	@Override
	public void onRefresh() {
		pageIndex = 1;
		mFriendsDataAL = new ArrayList<FriendModel>();
		getSuggestedFriendRequest();
	}
}