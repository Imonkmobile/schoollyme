package com.schollyme.friends;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.R;
import com.schollyme.adapter.FriendRequestAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.FriendsListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;
/**
 * Fragment . This will display list of incoming friend request
 * @author Ravi Bhandari
 */
public class FriendRequestFragment extends BaseFriendFragment implements  OnRefreshListener{

	private static Context mContext;
	private ListView mFriendsSuggestionsAL;
	private TextView mNoRecordTV;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private FriendRequestAdapter mAdapter;
	private ArrayList<FriendModel> mFriendsDataAL;
	private int pageIndex = 1;
	private boolean loadingFlag = false;
	private ErrorLayout mErrorLayout;
	private boolean isRequesting = false;
	private FriendCountListener mFriendCountListener;
	
	public static FriendRequestFragment newInstance(ErrorLayout mLayout, FriendCountListener listener) {
		FriendRequestFragment friendRequestFragment = new FriendRequestFragment();
		friendRequestFragment.mErrorLayout = mLayout;
		friendRequestFragment.mFriendCountListener = listener;
		return friendRequestFragment;
	}

	@Override
	public void onStart() {
		super.onStart();
		mFriendsDataAL = new ArrayList<FriendModel>();
		getFriendsPendingRequest(new LogedInUserModel(mContext).mUserGUID);
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.friends_fragment, null);
		mContext = getActivity();
		mFriendsSuggestionsAL = (ListView) view.findViewById(R.id.friends_lv);
		mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		FontLoader.setRobotoRegularTypeface(mNoRecordTV);
		pageIndex = 1;
		return view;
	}

	private void setSuggestedFriendsAdapter(final ArrayList<FriendModel> mSuggestionsAL){
		mAdapter = new FriendRequestAdapter(mContext,mErrorLayout, mFriendCountListener);
		if(null != mFriendCountListener){
			mFriendCountListener.onCountChange(mSuggestionsAL.size());
		}
		if(mSuggestionsAL.size()==0){
			mNoRecordTV.setVisibility(View.VISIBLE);
			mNoRecordTV.bringToFront();
			mFriendsSuggestionsAL.setAdapter(mAdapter);
			mAdapter.setList(new ArrayList<FriendModel>());
		}
		else{
			mNoRecordTV.setVisibility(View.GONE);
			mSwipeRefreshWidget.setVisibility(View.VISIBLE);
			mFriendsSuggestionsAL.setVisibility(View.VISIBLE);
			mAdapter.setList(mSuggestionsAL);
			mFriendsSuggestionsAL.setAdapter(mAdapter);
			mFriendsSuggestionsAL.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
						long arg3) {
					FriendModel mModel = mSuggestionsAL.get(pos);

					if (new LogedInUserModel(mContext).mUserGUID.equals(mModel.mUserGUID)) {
						mContext.startActivity(DashboardActivity.getIntent(mContext,7, ""));
					} else {
						mContext.startActivity(FriendProfileActivity.getIntent(mContext,mModel.mUserGUID,
								mModel.mProfileLink, "FriendRequestFragment"));
					}
					
				}
			});
		}
	}

	/**
	 * Server request for get list of incoming pending request
	 * */
	private void getFriendsPendingRequest(String userGUID){
		isRequesting = true;
		FriendsListRequest mRequest = new FriendsListRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {
			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				isRequesting = false;
				if(success){
					mFriendsDataAL.addAll((ArrayList<FriendModel>) data);
					setSuggestedFriendsAdapter(mFriendsDataAL);
				} else {
					mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
				}
				mSwipeRefreshWidget.setRefreshing(false);
			}
		});
		mRequest.FriendsListServerRequest(userGUID, "IncomingRequest", "FirstName", "ASC", String.valueOf(pageIndex), String.valueOf(Config.PAGINATION_PAGE_SIZE*2));
	}

	@Override
	public void onRefresh() {
		pageIndex = 1;
		mFriendsDataAL = new ArrayList<FriendModel>();
		getFriendsPendingRequest(new LogedInUserModel(mContext).mUserGUID);
	}

	@Override
	public void onReload() {
		if(!isRequesting){
			if(null != mFriendsSuggestionsAL){
				mFriendsSuggestionsAL.post(new Runnable() {
					
					@Override
					public void run() {
						onRefresh();
					}
				});
			}
		}
	}

	@Override
	public void onSearchBtnClick() {

	}

	public interface FriendCountListener{
		void onCountChange(int count);
	}
}