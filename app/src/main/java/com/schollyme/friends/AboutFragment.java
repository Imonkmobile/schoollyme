package com.schollyme.friends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.activity.AboutActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.UserModel;
import com.vinfotech.request.AboutRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

/**
 * Created by user on 30/6/2017.
 */

public class AboutFragment extends BaseFragment implements View.OnClickListener {
    private AboutActivityViewHolder mAboutActivityViewHolder;
    private Context mContext;
    private static String mUserGUID = "", /*fromActivity = "",*/ aboutType = "";
    private UserModel mUserModel;
    private ErrorLayout mErrorLayout;
    private String headerTitle = "About Me";

    public static AboutFragment newInstance(String mUserGUID, String aboutType) {
        AboutFragment aboutFragment = new AboutFragment();
        aboutFragment.mUserGUID = mUserGUID;
        aboutFragment.aboutType = aboutType;
        return aboutFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = getActivity();
        mAboutActivityViewHolder = new AboutActivityViewHolder(view.findViewById(R.id.main_rl), this);


        setAboutViews();

        mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_rl));
        int rightDrawable = 0;
        if (mUserGUID.equals(new LogedInUserModel(mContext).mUserGUID)
                || mUserGUID.equals(new LogedInUserModel(mContext).mUserName)) {
            rightDrawable = R.drawable.selector_edit;


        } else {
            rightDrawable = 0;
            mAboutActivityViewHolder.mEmailTV.setVisibility(View.GONE);
            mAboutActivityViewHolder.mEmailHeaderTV.setVisibility(View.GONE);
        }
        setFont();
    }
    private void setAboutViews() {

        if(aboutType != null) {
            if(aboutType.equalsIgnoreCase("About Me")) {
                headerTitle = getResources().getString(R.string.about_header);
                mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.VISIBLE);
                mAboutActivityViewHolder.moreAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.favoriteAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.athleteSportsLL.setVisibility(View.GONE);
            } else if(aboutType.equalsIgnoreCase("More About Me")) {
                headerTitle = getResources().getString(R.string.more_about_me);
                mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.moreAboutLL.setVisibility(View.VISIBLE);
                mAboutActivityViewHolder.favoriteAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.athleteSportsLL.setVisibility(View.GONE);
            } else if(aboutType.equalsIgnoreCase("My Favorite")) {
                headerTitle = getResources().getString(R.string.my_favorite);
                mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.moreAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.favoriteAboutLL.setVisibility(View.VISIBLE);
                mAboutActivityViewHolder.athleteSportsLL.setVisibility(View.GONE);
            } else if(aboutType.equalsIgnoreCase("My Sports")) {
                headerTitle = getResources().getString(R.string.my_sport);
                mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.moreAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.favoriteAboutLL.setVisibility(View.GONE);
                mAboutActivityViewHolder.athleteSportsLL.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setFont() {

        FontLoader.setRobotoRegularTypeface(mAboutActivityViewHolder.mFullNameTV, mAboutActivityViewHolder.mEmailTV,
                mAboutActivityViewHolder.mDOBTV, mAboutActivityViewHolder.mStateTV, mAboutActivityViewHolder.mGenderTV,
                mAboutActivityViewHolder.mSportsTV, mAboutActivityViewHolder.mAlternateEmailTV,
                mAboutActivityViewHolder.mHomeAddressTV,
                mAboutActivityViewHolder.mPhoneNumberTV, mAboutActivityViewHolder.mParentNameTV,
                mAboutActivityViewHolder.mParentEmailTV, mAboutActivityViewHolder.mParentPhoneNumberTV,
                mAboutActivityViewHolder.mUserNCAAEligibilityTV,
                mAboutActivityViewHolder.favMovieTV, mAboutActivityViewHolder.favMusicTV,
                mAboutActivityViewHolder.favFoodTV, mAboutActivityViewHolder.favTeacherTV,
                mAboutActivityViewHolder.favQuoteTV, mAboutActivityViewHolder.sportsPositionTV,
                mAboutActivityViewHolder.homeTeamTV, mAboutActivityViewHolder.jerseyNumberTV,
                mAboutActivityViewHolder.currentSchoolTV, mAboutActivityViewHolder.clubTeamTV,
                mAboutActivityViewHolder.favAthleteTV, mAboutActivityViewHolder.favTeamTV);

        FontLoader.setRobotoRegularTypeface(mAboutActivityViewHolder.mFullNameHeaderTV, mAboutActivityViewHolder.mEmailHeaderTV,
                mAboutActivityViewHolder.mDOBHeaderTV, mAboutActivityViewHolder.mStateHeaderTV,
                mAboutActivityViewHolder.mGenderHeaderTV, mAboutActivityViewHolder.mSportsHeaderTV,
                mAboutActivityViewHolder.mHomeAddressHeaderTV, mAboutActivityViewHolder.mPhoneNumberHeaderTV,
                mAboutActivityViewHolder.mParentNameHeaderTV, mAboutActivityViewHolder.mParentEmailHeaderTV,
                mAboutActivityViewHolder.mParentPhoneNumberHeaderTV, mAboutActivityViewHolder.mUserNCAAEligibilityHeaderTV,
                mAboutActivityViewHolder.favMovieHeaderTV, mAboutActivityViewHolder.favMusicHeaderTV,
                mAboutActivityViewHolder.favFoodHeaderTV, mAboutActivityViewHolder.favTeacherHeaderTV,
                mAboutActivityViewHolder.favQuoteHeaderTV, mAboutActivityViewHolder.sportsPositionHeaderTV,
                mAboutActivityViewHolder.homeTeamHeaderTV, mAboutActivityViewHolder.jerseyNumberHeaderTV,
                mAboutActivityViewHolder.currentSchoolHeaderTV, mAboutActivityViewHolder.clubTeamHeaderTV,
                mAboutActivityViewHolder.favAthleteHeaderTV, mAboutActivityViewHolder.favTeamHeaderTV,
                mAboutActivityViewHolder.mHeightHeaderTV);

        FontLoader.setRobotoMediumTypeface(mAboutActivityViewHolder.mMoreAboutTV,
                mAboutActivityViewHolder.mMyFavTV, mAboutActivityViewHolder.mMySportsTV,
                mAboutActivityViewHolder.mHeightTV);
    }

    @Override
    public void onResume() {
        super.onResume();
        AboutServerRequest(mUserGUID);
    }

    /**
     * Check user type and hide and show different profile related information according to type of user
     */
    private void manageFieldsForTypeOfUser(int userType, String isPaidCoach, boolean isSelfProfile) {
        if (userType == 1) {
            mAboutActivityViewHolder.mSportsRL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.GONE);
            mAboutActivityViewHolder.favQuoteRL.setVisibility(View.GONE);
            mAboutActivityViewHolder.athleteSportsLL.setVisibility(View.GONE);
            mAboutActivityViewHolder.favTeamRL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.favAthleteRL.setVisibility(View.VISIBLE);
        } else if (userType == 2) {
            mAboutActivityViewHolder.mSportsRL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.favQuoteRL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.athleteSportsLL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.mSportsHeaderTV.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.favTeamRL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.favAthleteRL.setVisibility(View.VISIBLE);
            if (isPaidCoach.equals("1") || isSelfProfile) {
                mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.VISIBLE);
            } else {
                mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.GONE);
            }
        } else if (userType == 3) {
            mAboutActivityViewHolder.mSportsRL.setVisibility(View.GONE);
            mAboutActivityViewHolder.athleteAboutLL.setVisibility(View.GONE);
            mAboutActivityViewHolder.favQuoteRL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.athleteSportsLL.setVisibility(View.GONE);
            mAboutActivityViewHolder.mSportsHeaderTV.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.favTeamRL.setVisibility(View.VISIBLE);
            mAboutActivityViewHolder.favAthleteRL.setVisibility(View.VISIBLE);
        }
    }

    public static Intent getIntent(Context context, String mUserGUID, String fromAct) {
        Intent intent = new Intent(context, AboutActivity.class);
        intent.putExtra("mUserGUID", mUserGUID);
        intent.putExtra("fromActivity", fromAct);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    private class AboutActivityViewHolder {

        private TextView mFullNameTV, mEmailTV, mDOBTV, mGenderTV, mStateTV, mSportsTV;
        private TextView mHomeAddressTV, mPhoneNumberTV, mParentNameTV, mParentEmailTV, mParentPhoneNumberTV;
        private TextView mHeightTV, mAlternateEmailTV, mHeightHeaderTV;
        private TextView mUserNCAAEligibilityTV;
        private TextView favMovieTV, favMusicTV, favFoodTV, favTeacherTV, favQuoteTV;
        private TextView sportsPositionTV, homeTeamTV, jerseyNumberTV, currentSchoolTV, clubTeamTV, favAthleteTV, favTeamTV;

        private TextView mFullNameHeaderTV, mEmailHeaderTV, mDOBHeaderTV, mStateHeaderTV, mGenderHeaderTV, mSportsHeaderTV;
        private TextView mHomeAddressHeaderTV, mPhoneNumberHeaderTV, mParentNameHeaderTV, mParentEmailHeaderTV, mParentPhoneNumberHeaderTV;
        private TextView mUserNCAAEligibilityHeaderTV;
        private TextView favMovieHeaderTV, favMusicHeaderTV, favFoodHeaderTV, favTeacherHeaderTV, favQuoteHeaderTV;
        private TextView sportsPositionHeaderTV, homeTeamHeaderTV, jerseyNumberHeaderTV, currentSchoolHeaderTV,
                clubTeamHeaderTV, favAthleteHeaderTV, favTeamHeaderTV;

        private TextView mMoreAboutTV, mMyFavTV, mMySportsTV;

        //	private TextView mGPAVerifyTV,mACTVerifyTV,mSATVerifyTV,mGPAVerifiedTV,mACTVerifiedTV,
        // mSATVerifiedTV,mTranscriptVerifyTV,mTranscriptVerifiedTV;

        private LinearLayout athleteAboutLL, moreAboutLL, favoriteAboutLL, athleteSportsLL;
        private RelativeLayout mSportsRL, favQuoteRL, favTeamRL, favAthleteRL;

        public AboutActivityViewHolder(View view, View.OnClickListener listener) {

            mFullNameTV = (TextView) view.findViewById(R.id.full_name_tv);
            mEmailTV = (TextView) view.findViewById(R.id.email_tv);
            mDOBTV = (TextView) view.findViewById(R.id.dob_tv);
            mStateTV = (TextView) view.findViewById(R.id.state_tv);
            mGenderTV = (TextView) view.findViewById(R.id.gender_tv);
            mSportsTV = (TextView) view.findViewById(R.id.sports_tv);
            mHomeAddressTV = (TextView) view.findViewById(R.id.home_address_tv);
            mPhoneNumberTV = (TextView) view.findViewById(R.id.home_phone_tv);
            mParentNameTV = (TextView) view.findViewById(R.id.parent_name_tv);
            mParentEmailTV = (TextView) view.findViewById(R.id.parent_email_tv);
            mParentPhoneNumberTV = (TextView) view.findViewById(R.id.parent_phone_tv);

            mUserNCAAEligibilityTV = (TextView) view.findViewById(R.id.ncaa_tv);
            favMovieTV = (TextView) view.findViewById(R.id.fav_movie_tv);
            favMusicTV = (TextView) view.findViewById(R.id.fav_music_tv);
            favFoodTV = (TextView) view.findViewById(R.id.fav_food_tv);
            favTeacherTV = (TextView) view.findViewById(R.id.fav_teacher_tv);
            favQuoteTV = (TextView) view.findViewById(R.id.fav_quote_tv);
            sportsPositionTV = (TextView) view.findViewById(R.id.sports_position_tv);
            homeTeamTV = (TextView) view.findViewById(R.id.home_team_tv);
            jerseyNumberTV = (TextView) view.findViewById(R.id.jersey_tv);
            currentSchoolTV = (TextView) view.findViewById(R.id.current_school_tv);
            clubTeamTV = (TextView) view.findViewById(R.id.club_team_tv);
            favAthleteTV = (TextView) view.findViewById(R.id.fav_athelet_tv);
            favTeamTV = (TextView) view.findViewById(R.id.fav_team_tv);
            mHeightTV = (TextView) view.findViewById(R.id.height_tv);
            mAlternateEmailTV = (TextView) view.findViewById(R.id.height_tv);
            mHeightHeaderTV = (TextView) view.findViewById(R.id.height_hint_tv);

            mMoreAboutTV = (TextView) view.findViewById(R.id.more_about_me_tv);
            //		mMyScoreTV = (TextView) view.findViewById(R.id.my_score_tv);
            mMyFavTV = (TextView) view.findViewById(R.id.my_fav_tv);
            mMySportsTV = (TextView) view.findViewById(R.id.my_sports_tv);

            mFullNameHeaderTV = (TextView) view.findViewById(R.id.userfullname_tv);
            mEmailHeaderTV = (TextView) view.findViewById(R.id.email_hint_tv);
            mDOBHeaderTV = (TextView) view.findViewById(R.id.dob_hint_tv);
            mStateHeaderTV = (TextView) view.findViewById(R.id.state_hint_tv);
            mGenderHeaderTV = (TextView) view.findViewById(R.id.gender_hint_tv);
            mSportsHeaderTV = (TextView) view.findViewById(R.id.sports_hint_tv);
            mHomeAddressHeaderTV = (TextView) view.findViewById(R.id.home_address_hint_tv);
            mPhoneNumberHeaderTV = (TextView) view.findViewById(R.id.home_phone_hint_tv);
            mParentNameHeaderTV = (TextView) view.findViewById(R.id.parent_name_hint_tv);
            mParentEmailHeaderTV = (TextView) view.findViewById(R.id.parent_email_hint_tv);
            mParentPhoneNumberHeaderTV = (TextView) view.findViewById(R.id.parent_phone_hint_tv);

            mUserNCAAEligibilityHeaderTV = (TextView) view.findViewById(R.id.ncaa_hint_tv);
            favMovieHeaderTV = (TextView) view.findViewById(R.id.fav_movie_hint_tv);
            favMusicHeaderTV = (TextView) view.findViewById(R.id.fav_music_hint_tv);
            favFoodHeaderTV = (TextView) view.findViewById(R.id.fav_food_hint_tv);
            favTeacherHeaderTV = (TextView) view.findViewById(R.id.fav_teacher_hint_tv);
            favQuoteHeaderTV = (TextView) view.findViewById(R.id.fav_quote_hint_tv);
            sportsPositionHeaderTV = (TextView) view.findViewById(R.id.sports_position_hint_tv);
            homeTeamHeaderTV = (TextView) view.findViewById(R.id.home_team_hint_tv);
            jerseyNumberHeaderTV = (TextView) view.findViewById(R.id.jersey_hint_tv);
            currentSchoolHeaderTV = (TextView) view.findViewById(R.id.current_school_hint_tv);
            clubTeamHeaderTV = (TextView) view.findViewById(R.id.club_team_hint_tv);
            favAthleteHeaderTV = (TextView) view.findViewById(R.id.fav_athelet_hint_tv);
            favTeamHeaderTV = (TextView) view.findViewById(R.id.fav_team_hint_tv);


            athleteAboutLL = (LinearLayout) view.findViewById(R.id.athlete_about_rl);
            moreAboutLL = (LinearLayout) view.findViewById(R.id.more_about_rl);
            favoriteAboutLL = (LinearLayout) view.findViewById(R.id.favorite_about_rl);
            athleteSportsLL = (LinearLayout) view.findViewById(R.id.my_sports_rl);
            mSportsRL = (RelativeLayout) view.findViewById(R.id.sports_rl);
            favQuoteRL = (RelativeLayout) view.findViewById(R.id.fav_quote_rl);
            favTeamRL = (RelativeLayout) view.findViewById(R.id.fav_team_rl);
            favAthleteRL = (RelativeLayout) view.findViewById(R.id.fav_athlete_rl);
        }
    }

    /**
     * ...........LISTENERS.........
     */

    @Override
    public void onClick(View v) {

    }
    /**
     * Set user profile related information in respective fields
     */
    private void setdataInFields(UserModel mUser) {

        boolean isSelfProfile = false;
        LogedInUserModel mLoggedInUser = new LogedInUserModel(mContext);
        if (mLoggedInUser.mUserGUID.equals(mUserGUID) || mUserGUID.equals(mLoggedInUser.mUserName)) {
            isSelfProfile = true;
        }
        manageFieldsForTypeOfUser(Integer.parseInt(mUser.mUserTypeID), mLoggedInUser.isPaidCoach, isSelfProfile);

        mAboutActivityViewHolder.mFullNameTV.setText(mUser.mUserFullName);
        mAboutActivityViewHolder.mEmailTV.setText(mUser.mUserEmail);
        LogedInUserModel mLogedInUserModel = new LogedInUserModel(mContext);
        String dob = mUser.mUserDOB;
        if (mUserGUID.equals(mLogedInUserModel.mUserGUID) || mUserGUID.equals(mLogedInUserModel.mUserName)) {
            if (!TextUtils.isEmpty(mLogedInUserModel.mUserDOB) && !mLogedInUserModel.mUserDOB.equals("0000-00-00"))
                mAboutActivityViewHolder.mDOBTV.setText(Utility.formateDate(mLogedInUserModel.mUserDOB));
        } else {
            if (!TextUtils.isEmpty(mUser.mUserDOB) && !mUser.mUserDOB.equals("0000-00-00"))
                mAboutActivityViewHolder.mDOBTV.setText(Utility.formateDateWithoutYear(mUser.mUserDOB));
        }

        if (mUser.mUserGender.equals("1")) {
            mAboutActivityViewHolder.mGenderTV.setText(R.string.gender_male);
        } else {
            mAboutActivityViewHolder.mGenderTV.setText(R.string.gender_female);
        }

        mAboutActivityViewHolder.mStateTV.setText(mUser.mUserStateName);
        StringBuilder mBuilder = new StringBuilder();
        for (int i = 0; i < mUser.mUserSportsAL.size(); i++) {
            mBuilder.append(mUser.mUserSportsAL.get(i).mSportsName);
            if (i != (mUser.mUserSportsAL.size() - 1)) {
                mBuilder.append(", ");
            }
        }
        mAboutActivityViewHolder.mSportsTV.setText(mBuilder.toString());

        mAboutActivityViewHolder.mHomeAddressTV.setText(mUser.mUserHomeAddress);
        mAboutActivityViewHolder.mAlternateEmailTV.setText(mUser.mUserAlternateEmail);
        mAboutActivityViewHolder.mPhoneNumberTV.setText(mUser.mUserPhoneNumber);
        mAboutActivityViewHolder.mParentNameTV.setText(mUser.mUserParentName);
        mAboutActivityViewHolder.mParentEmailTV.setText(mUser.mUserParentEmail);
        mAboutActivityViewHolder.mParentPhoneNumberTV.setText(mUser.mUserParentPhoneNumber);

        mAboutActivityViewHolder.mUserNCAAEligibilityTV.setText(mUser.mUserNCAAEligibility);
        mAboutActivityViewHolder.favMovieTV.setText(mUser.mUserFavMovie);
        mAboutActivityViewHolder.favMusicTV.setText(mUser.mUserFavMusic);
        mAboutActivityViewHolder.favFoodTV.setText(mUser.mUserFavFood);
        mAboutActivityViewHolder.favTeacherTV.setText(mUser.mUserFavTeacher);
        mAboutActivityViewHolder.favQuoteTV.setText(mUser.mUserFavQuote);
        mAboutActivityViewHolder.sportsPositionTV.setText(mUser.mUserPosition);

        mAboutActivityViewHolder.homeTeamTV.setText(mUser.mUserHomeTeam);
        mAboutActivityViewHolder.jerseyNumberTV.setText(mUser.mUserJerseyNumber);
        mAboutActivityViewHolder.currentSchoolTV.setText(mUser.mUserCurrentSchool);
        mAboutActivityViewHolder.clubTeamTV.setText(mUser.mUserClubTeam);
        mAboutActivityViewHolder.favAthleteTV.setText(mUser.mUserFavAthelet);
        mAboutActivityViewHolder.favTeamTV.setText(mUser.mUserFavTeam);

        if (!TextUtils.isEmpty(mUser.mUserHeight)) {
            String nHeight = Utility.convertInchesToFeetAndInch(Integer.parseInt(mUser.mUserHeight), mContext);
            mAboutActivityViewHolder.mHeightTV.setText(nHeight);
        }
    }

    /**
     * Request user profile data from server
     * by using user unique USERGUID
     */
    private void AboutServerRequest(String mUserGUID) {

        AboutRequest mRequest = new AboutRequest(mContext);
        mRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mUserModel = (UserModel) data;
                    setdataInFields(mUserModel);
                } else {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Error);
                }
            }
        });
        mRequest.AboutRequestServer(new LogedInUserModel(mContext).mLoginSessionKey, mUserGUID);
    }
}
