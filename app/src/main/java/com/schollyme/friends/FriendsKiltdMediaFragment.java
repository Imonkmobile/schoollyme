package com.schollyme.friends;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.adapter.FriendKiltdGridAdapter;
import com.schollyme.adapter.KiltdMediaAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.media.AlbumGridFragment;
import com.schollyme.media.KiltdMediaFragment;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.vinfotech.request.KiltdFeedRequest;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/8/2017.
 */

public class FriendsKiltdMediaFragment  extends Fragment {

    private static final String TAG = AlbumGridFragment.class.getSimpleName();
    private TextView mEmptyTv;
    private SwipeRefreshLayout mPullToRefreshSrl;


    private FriendKiltdGridAdapter mAlbumAdapter;
    private ErrorLayout mErrorLayout;

    /*private String mActivityGUID = "", mTeamGUID;
    private int mNotificationType = 0;*/

//    private List<Album> mAlbums = new ArrayList<Album>();
    private List<NewsFeed> mNewsFeeds = new ArrayList<>();
    private int mPageNo = Config.DEFAULT_PAGE_INDEX;
    private boolean mFetchedAll = true;
    private KiltdFeedRequest mNewsFeedRequest;
    private Context mContext;
    private NFFilter mNFFilter;
    private LogedInUserModel mLogedInUserModel;

    public static FriendsKiltdMediaFragment newInstance(ErrorLayout errorLayout,int ModuleID, String EntityGUID,String otherUser) {
        FriendsKiltdMediaFragment albumGridFragment = new FriendsKiltdMediaFragment();
        albumGridFragment.mErrorLayout = errorLayout;
        albumGridFragment.mNFFilter = new NFFilter(ModuleID, EntityGUID);
        albumGridFragment.mNFFilter.FeedUser = otherUser;
        albumGridFragment.mNFFilter.AllActivity = NewsFeedRequest.ALL_ACTIVITY_NEWS_FEED;
        return albumGridFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mLogedInUserModel = new LogedInUserModel(mContext);
        mNewsFeedRequest = new KiltdFeedRequest(mContext);
    }
    @Override
    public void onStop() {
        super.onStop();
//        mAlbumListRequest.setActivityStatus(false);

    }

    @Override
    public void onStart() {
//        mAlbumListRequest.setActivityStatus(true);
        super.onStart();
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.friends_kiltd_media_layout, null);

        /*mActivityGUID = getArguments().getString("mActivityGUID");
        mTeamGUID = getArguments().getString("mTeamGUID");
        mNotificationType = getArguments().getInt("mNotificationType");*/

        return view;
    }

    private View mLoadMoreRl, loaderPb;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mPullToRefreshSrl = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                /*resetList();*/
                mPullToRefreshSrl.setRefreshing(true);
                /*mAlbumListRequest.setLoader(null);
                mAlbumListRequest.getAlbumListInServer(mUserGUID, mAlbumType, mPageNo,
                        AlbumListRequest.SORT_BY_ALBUMNAME,
                        AlbumListRequest.ORDER_BY_DESC);*/
            }
        });
        mEmptyTv = (TextView) view.findViewById(R.id.empty_tv);
        FontLoader.setRobotoRegularTypeface(mEmptyTv);
        loaderPb = view.findViewById(R.id.loader_pb);

        mLoadMoreRl = view.findViewById(R.id.load_more_rl);
        View bgView = mLoadMoreRl.findViewById(R.id.dummy_bg_v);
        bgView.setBackgroundColor(bgView.getResources().getColor(R.color.red_normal_color));
        bgView.setAlpha(0.4f);

        /*mAlbumListRequest = new AlbumListRequest(getActivity());
        mAlbumListRequest.setLoader(loaderPb);
        mAlbumListRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    if (null != data) {
                        @SuppressWarnings("unchecked")
                        List<Album> albums = (List<Album>) data;
                        for (Album album : albums) {
                            album.AlbumType = mAlbumType;
                        }
                        mAlbums.addAll(albums);

                        mFetchedAll = (mAlbums.size() >= totalRecords);
                        mAlbumAdapter.setList(mAlbums);
                        mEmptyTv.setVisibility(mAlbums.size() < 1 ? View.VISIBLE : View.INVISIBLE);
                    }
                } else if (null != data) {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, ErrorLayout.MsgType.Error);
                }
            }
        });*/
        /*mAlbumAdapter = new AlbumAdapter(getActivity(), AlbumListRequest.ALBUM_TYPE_PHOTO.equalsIgnoreCase(mAlbumType),
                AlbumGridActivity.isMe(mUserGUID, mLogedInUserModel));*/
        mAlbumAdapter = new FriendKiltdGridAdapter(mContext, new KiltdMediaAdapter.OnItemClickListenerPost() {
            @Override
            public void onClickItems(int ID, int position, NewsFeed newsFeed) {

            }
        },mLogedInUserModel.mUserGUID);

        GridView albumGv = (GridView) view.findViewById(R.id.generic_gv);
        albumGv.setAdapter(mAlbumAdapter);
        albumGv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                /*if (Config.DEBUG) {
                    Log.d(TAG, "onItemClick position=" + position + ", url=" + mAlbumAdapter.getItem(position));
                }
                if (position == 0 && mAlbumAdapter.hasCreator()) {
                    if (AlbumGridActivity.isMe(mUserGUID, mLogedInUserModel)) {

                        if (mAlbumType.equals(AlbumListRequest.ALBUM_TYPE_PHOTO)) {
                            mCreatorAlbum = new Album(AlbumListRequest.ALBUM_TYPE_PHOTO);
                        } else if (mAlbumType.equals(AlbumListRequest.ALBUM_TYPE_VIDEO)) {
                            mCreatorAlbum = new Album(AlbumListRequest.ALBUM_TYPE_VIDEO);
                        }

                        startActivityForResult(CreateAlbumActivity.getIntent(getActivity(), mCreatorAlbum),
                                CreateAlbumActivity.REQ_CODE_CREATE_ALBUM_ACTIVITY);
                    } else {
                        Toast.makeText(getActivity(), R.string.Create_album_not_allowed, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    startActivityForResult(MediaGridActivity.getIntent(getActivity(),
                            mUserGUID, mAlbumAdapter.getItem(position)),
                            MediaGridActivity.REQ_CODE_MEDIA_GRID_ACTIVITY);
                }*/
            }
        });
        albumGv.setOnScrollListener(new MyCustomonScrol());

        getFeedRequest(mNFFilter);

        /*resetList();
        mAlbumListRequest.getAlbumListInServer(mUserGUID, mAlbumType, mPageNo, AlbumListRequest.SORT_BY_ALBUMNAME,
                AlbumListRequest.ORDER_BY_DESC);*/
        super.onViewCreated(view, savedInstanceState);
    }

    /*private void resetList() {
        mAlbums.clear();
        if (mAlbumAdapter.hasCreator()) {
            mAlbums.add(mCreatorAlbum);
        }
        mPageNo = Config.DEFAULT_PAGE_INDEX;
        mAlbumAdapter.setList(mAlbums);
    }*/


    private void getFeedRequest(final NFFilter nffilter) {

        if (getConnectivityStatus(mContext) == 0) {
            mPullToRefreshSrl.setRefreshing(false);
        }
        isLoading = false;


        if (mPullToRefreshSrl.isRefreshing()) {
            mNewsFeedRequest.setLoader(null);
        } else {
            mNewsFeedRequest.setLoader(loaderPb);
        }

        mNewsFeedRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);

                if (success) {

                    if (null != data) {
                        if (mNFFilter.PageNo == 1) {
                            resetList();
                        }
                        List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
                        if(newsFeeds.size()>0) {
                            newsFeeds.remove(0);
                        }
                        mNewsFeeds.addAll(newsFeeds);

                        mAlbumAdapter.setList(mNewsFeeds);

                        if (totalRecords == 0) {
                            isLoading = false;
                        } else if (mNewsFeeds.size() < totalRecords) {
                            isLoading = true;
                            int newPage = mNFFilter.PageNo + 1;
                            mNFFilter.PageNo = newPage;
                        } else {
                            isLoading = false;

                            mPullToRefreshSrl.setRefreshing(false);
                        }
                    }
                } else {
                    isLoading = false;
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, ErrorLayout.MsgType.Error);
                }

                mPullToRefreshSrl.setRefreshing(false);
            }
        });
        mNewsFeedRequest.getNewsFeedListInServer(nffilter);
    }

    public int getConnectivityStatus(Context context) {
        if (null == context) {
            return 0;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (null != activeNetwork && activeNetwork.isConnected()) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return 1;
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return 2;
            }
        }
        return 0;
    }

    boolean isLoading;

    private class MyCustomonScrol implements AbsListView.OnScrollListener {

        private int mLastFirstVisibleItem;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int k = visibleItemCount + firstVisibleItem;

            if (k >= totalItemCount && isLoading && totalItemCount != 0) {
                getFeedRequest(mNFFilter);
            }
            if (mLastFirstVisibleItem < firstVisibleItem) {
                Log.i("SCROLLING DOWN", "TRUE");
            }
            if (mLastFirstVisibleItem > firstVisibleItem) {
                Log.i("SCROLLING UP", "TRUE");
            }
            mLastFirstVisibleItem = firstVisibleItem;
        }
    }

    public void sharePost(NewsFeed newsFeedModel) {

        SharePostRequest mSharePostRequest = new SharePostRequest(mContext);
        mSharePostRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, ErrorLayout.MsgType.Success);
                } else {
                    mErrorLayout.showError((String) data, true, ErrorLayout.MsgType.Error);
                }
            }
        });
        mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
                NewsFeedRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID, 1, 1);
    }

    private ToggleLikeRequest mToggleLikeRequest;

    public void LikeMediaToggleService(final NewsFeed newsFeed, final int position) {

        mToggleLikeRequest = new ToggleLikeRequest(mContext);
        mToggleLikeRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    if (newsFeed.IsLike == 0) {
                        newsFeed.IsLike = 1;
                        newsFeed.NoOfLikes++;
                    } else {
                        newsFeed.IsLike = 0;
                        newsFeed.NoOfLikes--;
                    }
                    mAlbumAdapter.notifyDataSetChanged();

                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, ErrorLayout.MsgType.Error);
                }

            }
        });
        mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");
    }
    private void resetList() {

        mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
        mNewsFeeds.clear();
        mNewsFeeds.add(new NewsFeed(null));
        mAlbumAdapter.setList(mNewsFeeds);
    }

}
