package com.schollyme.friends;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.activity.AboutActivity;
import com.schollyme.activity.BaseActivity;
import com.schollyme.activity.BuzzActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.activity.MyBuzzActivity;
import com.schollyme.activity.MyEvaluationActivity;
import com.schollyme.activity.ProfileImageViewerActivity;
import com.schollyme.evaluation.EvaluationActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.inbox.ChatHistoryActivity;
import com.schollyme.media.AlbumGridActivity;
import com.schollyme.media.AlbumGridFragment;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.Song;
import com.schollyme.model.SportsModel;
import com.schollyme.model.UserModel;
import com.schollyme.scores.FriendsScoreActivity;
import com.schollyme.scores.MyScoresActivity;
import com.schollyme.songofday.UserSongsActivity;
import com.vinfotech.request.AboutRequest;
import com.vinfotech.request.AlbumListRequest;
import com.vinfotech.request.BlockUserRequest;
import com.vinfotech.request.FriendAddRequest;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SongOfTheDayRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.widget.PagerSlidingTabStrip;

import java.util.HashMap;
import java.util.Map;


/**
 * This class will be user for display friend and non friend profile.
 * In this class friends profile,wall will display with parallax effect
 * Auther - @RaviBhandari
 */
public class FriendProfilePrlxActivity extends BaseActivity implements OnClickListener, OnRefreshListener {

    private UserProfileActivityViewHolder mUserProfileActivityViewHolder;
    private Context mContext;
    private UserModel mUserModel;
    private String mOtherGUID;
    private ErrorLayout mErrorLayout;

    private static final int PAGE_COUNT = 3;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private ViewPager mGenericVp;
    private GridPagerAdapter mGridPagerAdapter;
    private int mCurrentPageToShow = 0;
    private LogedInUserModel loggedinUserModel;

    public static String sLastProfileLink;
    public static String mFromActivity;
    private String[] options;
    private SchollyMeApplication mSchollyMeApplication;

    @Override
    protected void onStop() {
        super.onStop();
        sLastProfileLink = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_profile_prlx_layout);
        mContext = this;

        mSchollyMeApplication = (SchollyMeApplication) getApplication();
        mSchollyMeApplication.cancelNotifications(true);
        loggedinUserModel = new LogedInUserModel(mContext);
        mUserProfileActivityViewHolder = new UserProfileActivityViewHolder(findViewById(R.id.main_rl), this);
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));

        mFromActivity = getIntent().getStringExtra("fromActivity");
        mOtherGUID = getIntent().getStringExtra("OtherGUID");

        String profileLink = getIntent().getStringExtra("UserProfileLink");
        if (TextUtils.isEmpty(profileLink)) {
            Uri uri = getIntent().getData();
            if (null != sLastProfileLink && sLastProfileLink.equalsIgnoreCase(uri.getQueryParameter("UserProfileLink"))) {
                finish();
                return;
            } else {
                if (uri != null) {
                    sLastProfileLink = uri.getQueryParameter("UserProfileLink");
                }
            }
        } else {
            sLastProfileLink = profileLink;
        }

        getFriendProfileServerRequest(sLastProfileLink);

        mSchollyMeApplication.pauseSong();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String mOtherProfileLink = intent.getStringExtra("UserProfileLink");
        if (TextUtils.isEmpty(mOtherProfileLink)) {
            Uri uri = intent.getData();
            String mLink = uri.getQueryParameter("UserProfileLink");
            if (mLink.equals(mOtherProfileLink)) {

            } else {

            }
        } else {

        }
    }


    /**
     * Set friend profile data
     */
    private void setUserData(UserModel mModel) {

        mUserProfileActivityViewHolder.mUserName.setText(mModel.mUserFullName);
        String mUserType = mModel.mUserTypeID;

        if (mModel.mUserIsProfileVerified.equals("1")) {
            mUserProfileActivityViewHolder.mFavoriteIB.setVisibility(View.VISIBLE);
        } else {
            mUserProfileActivityViewHolder.mFavoriteIB.setVisibility(View.GONE);
        }

        String mUserTypeName = "";
        if (mUserType.equals("1")) {
            mUserTypeName = String.valueOf(Config.UserType.Coach);
        } else if (mUserType.equals("2")) {
            mUserTypeName = String.valueOf(Config.UserType.Athlete);
        } else if (mUserType.equals("3")) {
            mUserTypeName = String.valueOf(Config.UserType.Fan);
        }

        mUserProfileActivityViewHolder.mUserType.setText(mUserTypeName);
        String imageUrl = Config.IMAGE_URL_PROFILE + mModel.mUserProfilePic;
        String mCoverURL = Config.COVER_URL + mModel.mCoverPicture;
        ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mUserProfileActivityViewHolder.mProfileImageIv,
                ImageLoaderUniversal.option_Round_Image);
        final String mOUrl = Config.IMAGE_URL_PROFILE_ORIGINAL + mModel.mUserProfilePic;
        mUserProfileActivityViewHolder.mProfileImageIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(ProfileImageViewerActivity.getIntent(mContext, mOUrl));
            }
        });
        ImageLoaderUniversal.ImageLoadSquare(mContext, mCoverURL, mUserProfileActivityViewHolder.mBannerIv,
                ImageLoaderUniversal.option_normal_Image_WTL);

        String mUserFriendStatus = mModel.mFriendStatus;
        if (mUserFriendStatus.equals("1")) {
            mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendActionTVLayout.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendsIB.setVisibility(View.GONE);
            mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.friend_label));
            mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

            // mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.VISIBLE);
        } else if (mUserFriendStatus.equals("2")) {
            mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendActionTVLayout.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendsIB.setVisibility(View.GONE);
            mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.request_pending_label));
            mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            mUserProfileActivityViewHolder.mFriendActionTV.setBackgroundResource(R.drawable.neon_button_border);
            // mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
        } else if (mUserFriendStatus.equals("3")) {
            mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendActionTVLayout.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendsIB.setVisibility(View.GONE);
            mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.accept__friend_label));
            mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            // mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
        } else if (mUserFriendStatus.equals("4")) {
            mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendActionTVLayout.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendsIB.setVisibility(View.GONE);
            mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.add_label));
            mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            // mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
        } else {
            // mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
            mUserProfileActivityViewHolder.mFriendsIB.setVisibility(View.GONE);
        }

        mUserProfileActivityViewHolder.mFriendActionTV.setOnClickListener(this);

        if (mModel.mUserTypeID.equals(String.valueOf(Config.USER_TYPE_ATHLETE))) {
            // mUserProfileActivityViewHolder.mMoreTabTV.setVisibility(View.VISIBLE);
        } else {
            // mUserProfileActivityViewHolder.mMoreTabTV.setVisibility(View.GONE);
        }
    }

    public static Intent getIntent(Context context, String vv, String userProfileLink, String fromActivity) {
        Intent intent = new Intent(context, FriendProfilePrlxActivity.class);

        intent.putExtra("UserProfileLink", userProfileLink);
        intent.putExtra("fromActivity", fromActivity);
        return intent;
    }

    public class UserProfileActivityViewHolder {

        public TextView mUserName;
        public TextView mUserType;
        public TextView mSodTV;
        public TextView mUserProfileTitle;
        public LinearLayout mFriendActionTVLayout;
        public TextView mFriendActionTV;
        public LinearLayout mFriendActionMsgTVLayout;
        public TextView mFriendActionMsgTV;
        public ImageButton mFriendsIB, mFavoriteIB;
        public ImageView mBannerIv, mProfileImageIv;
        public RelativeLayout mUserInfoRL;
        //        public TextView mAboutTabTV, mMediaTabTV, mFriendsTabTV, mMoreTabTV;
        public ImageButton mBackIB, mMessageIB, mSODList, mSodIb, mHomeIB, mMoreIB;
        public ProgressBar mLoadingPb;
//        public View mUserProfileView;

        public UserProfileActivityViewHolder(View view, OnClickListener listener) {


//            mUserProfileView = getLayoutInflater().inflate(R.layout.friends_header_view, null);
            mUserProfileTitle = (TextView) findViewById(R.id.user_profile_title);
            mBannerIv = (ImageView) findViewById(R.id.banner_iv);
            mBannerIv.setVisibility(View.VISIBLE);
            mProfileImageIv = (ImageView) findViewById(R.id.profile_image_iv);
            mFavoriteIB = (ImageButton) findViewById(R.id.favorite_ib);
            mUserName = (TextView) findViewById(R.id.user_name_tv);
            mUserType = (TextView) findViewById(R.id.user_status_tv);
            mUserInfoRL = (RelativeLayout) findViewById(R.id.profile_image_name_rl);
            mUserProfileTitle.setText(getResources().getString(R.string.user_profile));

            /*mAboutTabTV = (TextView) findViewById(R.id.about_tab_tv);
            mMediaTabTV = (TextView) findViewById(R.id.media_tab_tv);
            mFriendsTabTV = (TextView) findViewById(R.id.friends_tab_tv);
            mMoreTabTV = (TextView) findViewById(R.id.more_tab_tv);*/
            mSodTV = (TextView) findViewById(R.id.sod_tv);
            mSodTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_play_tiny, 0);
            mFriendActionTV = (TextView) findViewById(R.id.friend_action_tv);
            mFriendActionTVLayout = (LinearLayout) findViewById(R.id.friend_action_tv_layout);
            mFriendsIB = (ImageButton) findViewById(R.id.friends_active_ib);
            mFriendActionMsgTV = (TextView) findViewById(R.id.friend_action_message_tv);
            mFriendActionMsgTVLayout = (LinearLayout) findViewById(R.id.friend_action_message_tv_layout);
            mBackIB = (ImageButton) view.findViewById(R.id.back_ib);
            mMessageIB = (ImageButton) view.findViewById(R.id.message_ib);
            mSODList = (ImageButton) view.findViewById(R.id.sod_list_ib);
            mSodIb = (ImageButton) view.findViewById(R.id.sod_ib);
            mHomeIB = (ImageButton) view.findViewById(R.id.home_ib);
            mMoreIB = (ImageButton) view.findViewById(R.id.more_ib);
            mLoadingPb = (ProgressBar) view.findViewById(R.id.loading_pb);

           /* mAboutTabTV.setOnClickListener(listener);
            mFriendsTabTV.setOnClickListener(listener);
            mMediaTabTV.setOnClickListener(listener);
            mMoreTabTV.setOnClickListener(listener);*/
            mFriendActionMsgTV.setOnClickListener(listener);
            mBackIB.setOnClickListener(listener);
            mHomeIB.setOnClickListener(listener);
            mMoreIB.setOnClickListener(listener);
            mFriendActionTV.setOnClickListener(listener);
            mMessageIB.setOnClickListener(listener);
            mSODList.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mUserModel) {
                        startActivity(UserSongsActivity.getIntent(v.getContext(),
                                mUserModel.mUserGUID, mUserModel.mUserFullName));
                    }
                }
            });

            mSodTV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mUserModel) {
                        fetchSongIfNeeded(v);
                    }
                }
            });

            mPagerSlidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs_psts);
            mGenericVp = (ViewPager) view.findViewById(R.id.generic_vp);

        }

        public void showLoader(boolean show) {
            mLoadingPb.setVisibility(show ? View.VISIBLE : View.GONE);
            mSodIb.setVisibility(show ? View.INVISIBLE : View.INVISIBLE);

        }
    }

    public void setViewPager() {

        mGridPagerAdapter = new GridPagerAdapter(getSupportFragmentManager());
        mGenericVp.setOffscreenPageLimit(1);
        mGenericVp.setAdapter(mGridPagerAdapter);

        mPagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Fragment baseFragment = mGridPagerAdapter.getBaseFragment(position);
                    /*if (baseFragment != null)
                        baseFragment.onReload();*/
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });
        mPagerSlidingTabStrip.setViewPager(mGenericVp, FriendProfilePrlxActivity.this);
        mGenericVp.postDelayed(new Runnable() {
            @Override
            public void run() {
                mGenericVp.setCurrentItem(mCurrentPageToShow);
            }
        }, 150);
    }

    String[] optionsToShow;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_tab_tv:
                if (mUserModel != null) {
                    if (mOtherGUID.equals(loggedinUserModel.mUserGUID)) {
                        startActivity(AboutActivity.getIntent(mContext, loggedinUserModel.mUserProfileURL, "FriendProfileActivity"));
                    } else {
                        startActivity(AboutActivity.getIntent(mContext, mUserModel.mUserProfileURL, "FriendProfileActivity"));
                    }
                }
                break;
            case R.id.friends_tab_tv:
                if (mUserModel != null) {
                    if (mOtherGUID.equals(loggedinUserModel.mUserGUID)) {
                        startActivity(FriendsActivity.getIntent(mContext, 0, ""));
                    } else {
                        startActivity(OtherUserFriendsActivity.getIntent(mContext, mOtherGUID));
                    }
                }
                break;
            case R.id.media_tab_tv:

                if (mUserModel != null) {
                    if (mOtherGUID.equals(loggedinUserModel.mUserGUID)) {
                        startActivity(AlbumGridActivity.getIntent(mContext, loggedinUserModel.mUserGUID, getString(R.string.My_Albums)));
                    } else {
                        startActivity(AlbumGridActivity.getIntent(mContext, mOtherGUID, mUserModel.mUserFullName + "'s" + " Albums"));
                    }
                }
                break;
            case R.id.more_tab_tv:
                boolean isPaidCoach = false;
                boolean canCoachEvaluateForAthlete = false;
                if (loggedinUserModel.mUserType == 1 && loggedinUserModel.isEvaluator.equals("1")) {
                    for (SportsModel mSport : mUserModel.mUserSportsAL) {
                        if (mSport.mSportsID.equals(loggedinUserModel.mUserSportsId)) {
                            canCoachEvaluateForAthlete = true;
                            showMoreOptionsListDialog(canCoachEvaluateForAthlete, false);
                            break;
                        }
                    }
                } else if (loggedinUserModel.mUserType == 1 && loggedinUserModel.isPaidCoach.equals("1")) {
                    isPaidCoach = true;

                    for (SportsModel mSport : mUserModel.mUserSportsAL) {
                        if (mSport.mSportsID.equals(loggedinUserModel.mUserSportsId)) {
                            canCoachEvaluateForAthlete = true;
                            showMoreOptionsListDialog(canCoachEvaluateForAthlete, true);
                            break;
                        }
                    }
                }

                if (!canCoachEvaluateForAthlete) {
                    showMoreOptionsListDialog(false, isPaidCoach);
                }

                break;
            case R.id.back_ib:
                if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
                    Intent intent = DashboardActivity.getIntent(this, 6, "");
                    startActivity(intent);
                    this.finish();
                } else
                    finish();

                break;
            case R.id.friend_action_tv:
                performActionOnFriendButton();
                break;
            case R.id.message_ib:
                if (mUserModel != null) {
                    String userName = mUserModel.mUserFullName;
                    FriendModel mFriendModel = new FriendModel();
                    mFriendModel.mFirstName = mUserModel.mUserFullName;
                    mFriendModel.mLastName = "";
                    mFriendModel.mProfileLink = mUserModel.mUserProfileURL;
                    mFriendModel.mProfilePicture = mUserModel.mUserProfilePic;
                    mFriendModel.mUserTypeID = mUserModel.mUserTypeID;
                    mFriendModel.mUserGUID = mOtherGUID;
                    mFriendModel.mFriendStatus = mUserModel.mFriendStatus;
                    mContext.startActivity(ChatHistoryActivity.getIntent(mContext, userName, mFriendModel, "0"));
                }
                break;

            case R.id.home_ib:
                Intent intent = DashboardActivity.getIntent(mContext, 4, "");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;

            case R.id.more_ib:

                String firstname = mUserModel.mUserFullName;

                if (mUserModel.mUserFullName.contains(" ")) {
                    firstname = mUserModel.mUserFullName.substring(0, firstname.lastIndexOf(" "));
                }

                if (mUserModel.mUserTypeID.equals("2")) {
                    if (loggedinUserModel.isPaidCoach.equals("0")) {
                        optionsToShow = new String[]{getString(R.string.about_mack) + " " + firstname,
                                firstname + "'s " + getString(R.string.mack_academics),
                                getString(R.string.block_user)};
                    } else {
                        optionsToShow = new String[]{getString(R.string.about_mack) + " " + firstname,
                                firstname + "'s " + getString(R.string.mack_academics),
                                firstname + "'s " + getString(R.string.users_Buzz),
                                firstname + "'s " + getString(R.string.Evaluation),
                                getString(R.string.block_user)};
                    }
                } else {
                    optionsToShow = new String[]{getString(R.string.about_mack) + " " + firstname,
                            getString(R.string.block_user)};
                }

                DialogUtil.showMediaDialog(mContext, 0, new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {

                        switch (position) {
                            case 0:
                                Intent intent = new Intent(FriendProfilePrlxActivity.this, AboutActivity.class);
                                intent.putExtra("mUserGUID", mOtherGUID);
                                intent.putExtra("mUserProfileURL", mUserModel.mUserProfileURL);
                                intent.putExtra("fromActivity", "");
                                intent.putExtra("aboutType", getString(R.string.about_mack) + " " + mUserModel.mUserFullName);
                                intent.putExtra("userType", mUserModel.mUserTypeID);
                                startActivity(intent);
                                break;
                            case 1:
                                if (optionsToShow.length >= 3) {
                                    startActivity(MyScoresActivity.getIntent(mContext, mOtherGUID, mUserModel.mUserFullName.substring(0, mUserModel.mUserFullName.lastIndexOf(" "))));
                                } else {
                                    showBlockDialog();
                                }
                                break;
                            case 2:
                                if(optionsToShow.length == 5){
                                    startActivity(MyBuzzActivity.getIntent(mContext, mOtherGUID, mUserModel.mUserFullName.substring(0, mUserModel.mUserFullName.lastIndexOf(" "))));
                                }else if (optionsToShow.length == 3) {
                                    showBlockDialog();
                                }
                                break;
                            case 3:
                                startActivity(MyEvaluationActivity.getIntent(mContext, mOtherGUID, mUserModel.mUserFullName.substring(0, mUserModel.mUserFullName.lastIndexOf(" ")),""));
                                break;
                            case 4:
                                showBlockDialog();
                                break;

                        }

                        /*if (position == 0) {
                            Intent intent = new Intent(FriendProfilePrlxActivity.this, AboutActivity.class);
                            intent.putExtra("mUserGUID", mOtherGUID);
                            intent.putExtra("mUserProfileURL", mUserModel.mUserProfileURL);
                            intent.putExtra("fromActivity", "");
                            intent.putExtra("aboutType", getString(R.string.about_mack) + " " + mUserModel.mUserFullName);
                            intent.putExtra("userType", mUserModel.mUserTypeID);
                            startActivity(intent);
                        } else if (position == 1) {
                            if (optionsToShow.length >= 3) {
                                startActivity(MyScoresActivity.getIntent(mContext, mOtherGUID));
                            } else {
                                showBlockDialog();
                            }
                        } else {
                            if(optionsToShow.length == 5){

                            }else if (optionsToShow.length == 3) {
                                showBlockDialog();
                            }
                        }*/
                    }

                    @Override
                    public void onCancel() {

                    }
                }, optionsToShow);
                break;
            case R.id.friend_action_message_tv:
                if (mUserModel != null) {
                    String userName = mUserModel.mUserFullName;
                    FriendModel mFriendModel = new FriendModel();
                    mFriendModel.mFirstName = mUserModel.mUserFullName;
                    mFriendModel.mLastName = "";
                    mFriendModel.mProfileLink = mUserModel.mUserProfileURL;
                    mFriendModel.mProfilePicture = mUserModel.mUserProfilePic;
                    mFriendModel.mUserTypeID = mUserModel.mUserTypeID;
                    mFriendModel.mUserGUID = mOtherGUID;
                    mFriendModel.mFriendStatus = mUserModel.mFriendStatus;
                    mContext.startActivity(ChatHistoryActivity.getIntent(mContext, userName, mFriendModel, "0"));
                }
                break;

            default:
                break;
        }
    }

    private void showBlockDialog() {

        DialogUtil.showOkCancelDialog(mContext, R.string.yes, R.string.no, getString(R.string.block_user),
                getString(R.string.block_confirm_msg), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BlockUserRequest mRequest = new BlockUserRequest(mContext);
                        mRequest.setRequestListener(new RequestListener() {
                            @Override
                            public void onComplete(boolean success, Object data, int totalRecords) {
                                if (success) {
                                    mErrorLayout.showError(data.toString(), false, MsgType.Success);
                                    finish();
                                } else {
                                    mErrorLayout.showError(data.toString(), false, MsgType.Error);
                                }
                            }
                        });
                        String friendId = mUserModel.mUserGUID;
                        String userId = loggedinUserModel.mUserGUID;
                        String moduleId = "3";
                        mRequest.BlockUserServerRequest(friendId, moduleId, userId);
                    }
                }, null);
    }

    /**
     * Play song of the of user to whome profile is visited
     */
    private Song mSODSong = null;

    private void getSongOfTheDay() {

        SongOfTheDayRequest songOfTheDayRequest = new SongOfTheDayRequest(FriendProfilePrlxActivity.this);
        songOfTheDayRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    if (null != data && data instanceof Song) {
                        mSODSong = (Song) data;
                        if (!mSODSong.isValidSOD()) {
//                            mUserProfileActivityViewHolder.mSodIb.setImageResource(R.drawable.ic_play_tiny);
//                            mUserProfileActivityViewHolder.mSodIb.setOnClickListener(null);
                        } else if (null != mSODSong && !TextUtils.isEmpty(mSODSong.Title)) {
                            mUserProfileActivityViewHolder.mSodTV.setVisibility(View.VISIBLE);
                            mUserProfileActivityViewHolder.mSodTV.setText(mSODSong.Title);
                        }
                    }
                }
            }
        });
        songOfTheDayRequest.getSongOfDayFromServer(mUserModel.UserGUID);
    }

    private void fetchSongIfNeeded(final View view) {

        if (null == mSODSong) {
            mUserProfileActivityViewHolder.showLoader(true);
            SongOfTheDayRequest songOfTheDayRequest = new SongOfTheDayRequest(view.getContext());
            songOfTheDayRequest.setRequestListener(new RequestListener() {

                @Override
                public void onComplete(boolean success, Object data, int totalRecords) {
                    if (success) {
                        if (null != data && data instanceof Song) {
                            mSODSong = (Song) data;
                            mUserProfileActivityViewHolder.showLoader(false);
                            handleSongPlay(view, mSODSong);
                        }
                    } else {
                        mUserProfileActivityViewHolder.showLoader(false);
                        mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                                : (String) data), true, MsgType.Error);
                    }
                }
            });
            songOfTheDayRequest.getSongOfDayFromServer(mUserModel.UserGUID);
        } else { //if (null != genericLv)
            handleSongPlay(view, mSODSong);
        }
    }

    private void handleSongPlay(final View v, final Song song) {

        final String songPlayUrl = TextUtils.isEmpty(song.SongURL) ? song.SongPreviewURL : song.SongURL;
        if (TextUtils.isEmpty(songPlayUrl) || !android.util.Patterns.WEB_URL.matcher(songPlayUrl).matches()) {
            mUserProfileActivityViewHolder.showLoader(false);
            mErrorLayout.showError(v.getResources().getString(R.string.No_Song_Found), true, MsgType.Error);
            return;
        }

        if (mSchollyMeApplication.isPlaying()) {
            mSchollyMeApplication.pauseSong();
            mUserProfileActivityViewHolder.mSodTV.setCompoundDrawablesWithIntrinsicBounds
                    (0, 0, R.drawable.ic_play_tiny, 0);
            //mUserProfileActivityViewHolder.mSodIb.setImageResource(R.drawable.selector_song_of_day);
        } else if (mSchollyMeApplication.playSong(song.Title, songPlayUrl,
                new SchollyMeApplication.SongLoadListener() {
                    @Override
                    public void onLoad(boolean success, String url, MediaPlayer mediaPlayer) {
                        mUserProfileActivityViewHolder.showLoader(false);
                        if (!url.equalsIgnoreCase(song.SongURL)) {
                            return;
                        }
                        if (!success) {
                            mErrorLayout.showError(v.getResources().getString(R.string.This_song_can_not), true, MsgType.Error);
                            return;
                        }
                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mUserProfileActivityViewHolder.mSodTV.setCompoundDrawablesWithIntrinsicBounds
                                        (0, 0, R.drawable.ic_mid_pause_small, 0);
                                //mUserProfileActivityViewHolder.mSodIb.setImageResource(R.drawable.selector_song_of_day);
                            }
                        });
                        //mUserProfileActivityViewHolder.mSodIb.setImageResource(R.drawable.selector_pause_sod);
                        mUserProfileActivityViewHolder.mSodTV.setCompoundDrawablesWithIntrinsicBounds
                                (0, 0, R.drawable.ic_mid_pause_small, 0);
                    }
                })) {
            mUserProfileActivityViewHolder.showLoader(true);
        } else if (null != mErrorLayout && mSchollyMeApplication.isBuffering()) {
            mUserProfileActivityViewHolder.showLoader(false);
            mErrorLayout.showError(v.getResources().getString(R.string.Buffering), true, MsgType.Error);
        } else if (null != mErrorLayout) {
            mUserProfileActivityViewHolder.showLoader(false);
            mErrorLayout.showError(v.getResources().getString(R.string.Invalid_song_URL), true, MsgType.Error);
        }
    }

    @Override
    protected void onDestroy() {
        ((SchollyMeApplication) getApplication()).cancelNotifications(true);
        super.onDestroy();
    }


    /**
     * Show dialog for display option like Buzz and Scores of a friend
     */
    private void showMoreOptionsListDialog(final boolean isEvaluator, final boolean isPaidCoach) {

        final String mFriendName = mUserModel.mUserFullName + "'s " + getString(R.string.users_Buzz);
        final String mScoresLabel = mUserModel.mUserFullName + "'s " + getString(R.string.friends_score);


        if (isEvaluator) {
            options = new String[]{getString(R.string.Evaluation), mFriendName};
        }
        if (isPaidCoach) {
            options = new String[]{getString(R.string.Evaluation), mFriendName, mScoresLabel};
        } else {
            options = new String[]{mFriendName};
        }

        DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {

            @Override
            public void onItemClick(int position, String item) {
                if (options.length == 1) {
                    startActivity(BuzzActivity.getIntent(mContext, mUserModel.mUserGUID, mFriendName));
                } else if (options.length == 2) {
                    if (position == 0) {
                        if (isPaidCoach) {
                            startActivity(EvaluationActivity.getIntent(mContext, mUserModel.mUserGUID,
                                    mUserModel.mUserFullName, loggedinUserModel.mUserSportsId,
                                    loggedinUserModel.mUserSportsName, Config.USER_TYPE_PAID_COACH));
                        } else {
                            startActivity(EvaluationActivity.getIntent(mContext, mUserModel.mUserGUID,
                                    mUserModel.mUserFullName, loggedinUserModel.mUserSportsId,
                                    loggedinUserModel.mUserSportsName, Config.USER_TYPE_EVALUATOR));
                        }
                    } else if (position == 1) {
                        startActivity(BuzzActivity.getIntent(mContext, mUserModel.mUserGUID, mFriendName));
                    }
                }
                if (options.length == 3) {
                    if (position == 0) {
                        startActivity(EvaluationActivity.getIntent(mContext, mUserModel.mUserGUID,
                                mUserModel.mUserFullName, loggedinUserModel.mUserSportsId,
                                loggedinUserModel.mUserSportsName, Config.USER_TYPE_PAID_COACH));
                    } else if (position == 1) {
                        startActivity(BuzzActivity.getIntent(mContext, mUserModel.mUserGUID, mFriendName));
                    } else if (position == 2) {
                        startActivity(FriendsScoreActivity.getIntent(mContext, mUserModel.mUserGUID, mScoresLabel));
                    }
                }
            }

            @Override
            public void onCancel() {

            }
        }, options);
    }

    private void showMoreOptions(final boolean athelete, final boolean isPaidCoach) {

        final PopupWindow popup = new PopupWindow(mContext);
        View layout = getLayoutInflater().inflate(R.layout.user_profile_pop_menu, null);
        FontLoader.SetFontToWholeView(mContext, layout, FontLoader.getRobotoRegular(mContext));
        popup.setContentView(layout);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        View evaluationsView = layout.findViewById(R.id.evaluation_menu);
        evaluationsView.setVisibility(athelete ? View.VISIBLE : View.GONE);
        evaluationsView.findViewById(R.id.evaluation_menu).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                if (isPaidCoach) {
                    startActivity(EvaluationActivity.getIntent(mContext, mUserModel.mUserGUID,
                            mUserModel.mUserFullName, loggedinUserModel.mUserSportsId,
                            loggedinUserModel.mUserSportsName, Config.USER_TYPE_PAID_COACH));
                } else {
                    startActivity(EvaluationActivity.getIntent(mContext, mUserModel.mUserGUID,
                            mUserModel.mUserFullName, loggedinUserModel.mUserSportsId,
                            loggedinUserModel.mUserSportsName, Config.USER_TYPE_EVALUATOR));
                }

            }
        });
        layout.findViewById(R.id.my_buzz_menu).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
                startActivity(BuzzActivity.getIntent(mContext, mUserModel.mUserGUID, ""));
            }
        });
//        popup.showAsDropDown(mUserProfileActivityViewHolder.mMoreTabTV);
    }

    @Override
    public void onBackPressed() {
        if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
            Intent intent = DashboardActivity.getIntent(this, 6, "");
            startActivity(intent);
            this.finish();
        } else
            super.onBackPressed();
    }

    /**
     * When a loggedin user navigate to some other user profile then he can add him as a friend from friend profile
     */
    private void performActionOnFriendButton() {
        try {
            if (mUserModel.mFriendStatus.equals("1")) {
                // All ready friend no action needed
            } else if (mUserModel.mFriendStatus.equals("2")) {
                // You have sent friend request but other user not confirmed it
            } else if (mUserModel.mFriendStatus.equals("3")) {
                // You have not confirmed the friend request from other user
                DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {
                        String customeMessage = position == 0 ? mContext.getResources().getString(R.string.added_as_friend) : mContext
                                .getResources().getString(R.string.added_as_teammate);
                        String message = mUserModel.mUserFullName + " " + customeMessage;
                        acceptFriendRequest(mOtherGUID, message, String.valueOf(position));
                    }

                    @Override
                    public void onCancel() {

                    }
                }, mContext.getString(R.string.add_as_friend), mContext.getString(R.string.add_as_teammate));
            } else if (mUserModel.mFriendStatus.equals("4")) {
                // Not yet friends,Send friend request
                DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {
                        addFriendRequest(mOtherGUID, "" + position);
                    }

                    @Override
                    public void onCancel() {

                    }
                }, mContext.getString(R.string.add_as_friend), mContext.getString(R.string.add_as_teammate));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Make server request for getting friend profile
     */
    private void getFriendProfileServerRequest(String mUserProfileLink) {


        AboutRequest mRequest = new AboutRequest(mContext);
        mRequest.AboutRequestServer(new LogedInUserModel(mContext).mLoginSessionKey, mUserProfileLink);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mUserModel = (UserModel) data;
                    setUserData(mUserModel);
                    mOtherGUID = mUserModel.UserGUID;

                    setViewPager();
                    getSongOfTheDay();
                } else {
                    String title = "";
                    if (data.toString().equals(getString(R.string.can_not_access_profile))) {
                        title = getString(R.string.user_blocked);
                    } else {
                        title = getResources().getString(R.string.app_name);
                    }
                    final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                            title, new OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {
                                    finish();
                                }
                            });
                    builder.setCancelable(false);
                    builder.setOnKeyListener(new Dialog.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                finish();
                                builder.dismiss();
                            }
                            return true;
                        }
                    });
                }
            }
        });
    }

    /**
     * Send friend request to whome profile is viewing
     */
    private void addFriendRequest(String friendGUID, String teammate) {
        FriendAddRequest mRequest = new FriendAddRequest(mContext);
        mRequest.AddFriendServerRequest(friendGUID, teammate);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(getResources().getString(R.string.friend_request_sent) + " " + mUserModel.mUserFullName, true,
                            MsgType.Success);
                    mUserModel.mFriendStatus = "2";
                    setUserData(mUserModel);
                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
                }
            }
        });
    }

    /**
     * Accept friend request from user to whome profile you are visiting
     */
    private void acceptFriendRequest(String friendGUID, final String message, String teammate) {
        FriendAddRequest mRequest = new FriendAddRequest(mContext);
        mRequest.AddFriendServerRequest(friendGUID, teammate);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mUserModel.mFriendStatus = "1";
                    setUserData(mUserModel);
                    mErrorLayout.showError(message, true, MsgType.Success);
                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
                }
            }
        });
    }

    /**
     * Refresh friends wall content on user swipe to refresh
     */
    @Override
    public void onRefresh() {
        if (HttpConnector.getConnectivityStatus(mContext) == 0) {
            DialogUtil.showOkDialogButtonLisnter(mContext, getResources().getString(R.string.No_internet_connection), getResources()
                    .getString(R.string.app_name), new OnOkButtonListner() {
                @Override
                public void onOkBUtton() {

                }
            });
//            mSwipeRefreshWidget.setRefreshing(false);
        } else {
            /*if (null != mNFFilter) {
                mNFFilter.PageNo = 1;
                getFeedRequest(mNFFilter);
            }*/
        }
    }

    public class GridPagerAdapter extends FragmentPagerAdapter {

        private Map<Integer, Fragment> mBaseFragments;

        @SuppressLint("UseSparseArrays")
        public GridPagerAdapter(FragmentManager fm) {
            super(fm);
            mBaseFragments = new HashMap<Integer, Fragment>();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String tabTitle = "";
            switch (position) {
                case 0:
                    tabTitle = getString(R.string.feed);
                    break;
                case 1:
                    tabTitle = getString(R.string.Media);
                    break;
                case 2:
                    tabTitle = getString(R.string.kilt_label);
                    break;

            }
            return tabTitle;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment mFragment = null;
            switch (position) {
                case 0:
                    mFragment = FriendsFeedFragment.newInstance(mUserModel, loggedinUserModel, mOtherGUID);
                    break;
                case 1:
                    mFragment = AlbumGridFragment.newInstance(mErrorLayout, mOtherGUID, loggedinUserModel,
                            AlbumListRequest.ALBUM_TYPE_PHOTO);
                    break;
                case 2:
                    mFragment = FriendsKiltdMediaFragment.newInstance(mErrorLayout, NewsFeedRequest.MODULE_ID_USERS,
                            new LogedInUserModel(mContext).mUserGUID, mOtherGUID);
                    break;
            }
            mBaseFragments.put(position, mFragment);
            return mFragment;
        }

        public Fragment getBaseFragment(int position) {
            return mBaseFragments.get(position);
        }
    }
}