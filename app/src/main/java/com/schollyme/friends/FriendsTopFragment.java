package com.schollyme.friends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.media.CreateAlbumActivity;
import com.schollyme.model.Album;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.search.SearchScreenActivity;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AlbumListRequest;
import com.vinfotech.utility.Config;
import com.vinfotech.widget.PagerSlidingTabStrip;
import com.vinfotech.widget.PagerSlidingTabStrip.NotificationTabProvider;

import java.util.HashMap;
import java.util.Map;

public class FriendsTopFragment extends BaseFragment {

    private static final int PAGE_COUNT = 2;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private ViewPager mGenericVp;
    private GridPagerAdapter mGridPagerAdapter;
    private ErrorLayout mErrorLayout;
    private int mCurrentPageToShow;
    private boolean mShowBackBtn = true;

    private HeaderLayout mHeaderLayout;
    private Context mContext;
    private Handler mHandler = new Handler();

    public static FriendsTopFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        FriendsTopFragment friendsTopFragment = new FriendsTopFragment();
        friendsTopFragment.mCurrentPageToShow = currentPageToShow;
        friendsTopFragment.mHeaderLayout = headerLayout;
        friendsTopFragment.mShowBackBtn = showBack;
        return friendsTopFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.friends_top_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                        mContext.getResources().getString(R.string.friend_teammates_label), R.drawable.ic_search_header);
                mHeaderLayout.setListenerItI(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mShowBackBtn) {
                            if (((FriendsActivity) getActivity()).mFromActivity.equalsIgnoreCase("GCMIntentService")) {
                                Intent intent = DashboardActivity.getIntent(getActivity(), 12, "FriendsTopFragment");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                getActivity().finish();
                            } else
                                getActivity().finish();
                        } else {
                            ((DashboardActivity) mContext).sliderListener();
                        }
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mGridPagerAdapter.getBaseFriendFragment(mGenericVp.getCurrentItem()).onSearchBtnClick();
                    }
                });
            }
        }, Config.NOTIFICATION_UPDATE_DELAY);


        mGridPagerAdapter = new GridPagerAdapter(getChildFragmentManager());
        mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_container_ll));

        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) view.findViewById(R.id.addFriendsMenu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                fabMenu.collapse();
                startActivity(SearchScreenActivity.getIntent(mContext)
                        .putExtra("UserType", new LogedInUserModel().mUserType));
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }

            @Override
            public void onMenuCollapsed() {

            }
        });

        mPagerSlidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs_psts);
        mGenericVp = (ViewPager) view.findViewById(R.id.generic_vp);
        mGenericVp.setOffscreenPageLimit(1);
        mGenericVp.setAdapter(mGridPagerAdapter);
        mPagerSlidingTabStrip.setOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                BaseFriendFragment baseFriendFragment = mGridPagerAdapter.getBaseFriendFragment(position);
                baseFriendFragment.onReload();
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });

        mPagerSlidingTabStrip.setViewPager(mGenericVp, getActivity());
        mGenericVp.postDelayed(new Runnable() {
            @Override
            public void run() {
                mGenericVp.setCurrentItem(mCurrentPageToShow);
            }
        }, 150);


    }

    public class GridPagerAdapter extends FragmentStatePagerAdapter implements NotificationTabProvider {

        private Map<Integer, BaseFriendFragment> mBaseFriendFragments;
        private String friendCount = "";

        public GridPagerAdapter(FragmentManager fm) {
            super(fm);
            mBaseFriendFragments = new HashMap<Integer, BaseFriendFragment>();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String tabTitle = "";
            switch (position) {
//			case 0:
//				tabTitle = getString(R.string.request_tab);
//				break;
                case 0:
                    tabTitle = getString(R.string.friends_tab);
                    break;
                case 1:
                    tabTitle = getString(R.string.teammates_tab);
                    break;
//			case 3:
//				tabTitle = getString(R.string.outgoing_tab);
//				break;
//			case 4:
//				tabTitle = getString(R.string.suggestions_tab);
//				break;
            }
            return tabTitle;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            BaseFriendFragment mFragment = null;
            switch (position) {
//			case 0:
//				mFragment = FriendRequestFragment.newInstance(mErrorLayout, new FriendCountListener() {
//
//					@Override
//					public void onCountChange(int count) {
//						friendCount = (count > 0 ? Integer.toString(count) : "");
//						mPagerSlidingTabStrip.setViewPager(mGenericVp);
//					}
//				});
//				break;
                case 0:
                    mFragment = FriendsFragment.newInstance(mErrorLayout);
                    break;
                case 1:
                    mFragment = TeammatesFragment.newInstance(mErrorLayout);
                    break;
//			case 3:
//				mFragment = FriendsOutgoingRequestFragment.newInstance(mErrorLayout);
//				break;
//			case 4:
//				mFragment = FriendsSuggestionsFragment.newInstance(mErrorLayout);
//				break;
            }
            mBaseFriendFragments.put(position, mFragment);
            return mFragment;
        }

        public BaseFriendFragment getBaseFriendFragment(int position) {
            return mBaseFriendFragments.get(position);
        }

        @Override
        public String getNotificationText(int position) {

            if (position == 0) {
                return friendCount;
            } else {
                return "";
            }
        }
    }
}