package com.schollyme.friends;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.activity.AboutActivity;
import com.schollyme.activity.BaseActivity;
import com.schollyme.activity.BuzzActivity;
import com.schollyme.activity.CreateWallPostActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.activity.ProfileImageViewerActivity;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.adapter.PostFeedAdapter.OnItemClickListenerPost;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.evaluation.EvaluationActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.inbox.ChatHistoryActivity;
import com.schollyme.media.AlbumGridActivity;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.schollyme.model.Song;
import com.schollyme.model.SportsModel;
import com.schollyme.model.UserModel;
import com.schollyme.scores.FriendsScoreActivity;
import com.schollyme.songofday.UserSongsActivity;
import com.vinfotech.request.AboutRequest;
import com.vinfotech.request.FriendAddRequest;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.SongOfTheDayRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.ImageLoaderUniversal;

import java.util.ArrayList;
import java.util.List;

public class FriendProfileActivity extends BaseActivity implements OnClickListener, OnRefreshListener {

    private UserProfileActivityViewHolder mUserProfileActivityViewHolder;
    private Context mContext;
    private UserModel mUserModel;
    private String mOtherGUID;
    private ErrorLayout mErrorLayout;
    private NFFilter mNFFilter;
    private PostFeedAdapter mPostFeedAdapter;
    private ListView genericLv;
    private List<NewsFeed> mNewsFeeds = new ArrayList<>();
    LogedInUserModel loggedinUserModel;
    final private int REQ_FROM_HOME_LIKELIST = 5555;
    final private int REQ_FROM_HOME_COMMENTLIST = 5556;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    int ClickLocation = 0;
    private View mFooterRL;
    private TextView mNoMoreTV;
    private ProgressBar mLoaderBottomPB, mLoadingCenter;
    public static String sLastProfileLink;
    public static String mFromActivity;
    private String[] options;
    private SchollyMeApplication mSchollyMeApplication;

    @Override
    protected void onStop() {
        super.onStop();
        sLastProfileLink = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_profile_activity_layout);
        /*mContext = this;

		mSchollyMeApplication = (SchollyMeApplication) getApplication();
		mSchollyMeApplication.cancelNotifications(true);
		loggedinUserModel = new LogedInUserModel(mContext);
		mUserProfileActivityViewHolder = new UserProfileActivityViewHolder(findViewById(R.id.main_rl), this);
		mErrorLayout = new ErrorLayout(findViewById(R.id.profile_image_name_rl));
		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.red_normal_color);

		mSwipeRefreshWidget.setOnRefreshListener(this);

		mFromActivity = getIntent().getStringExtra("fromActivity");
		String profileLink = getIntent().getStringExtra("UserProfileLink");

		if (TextUtils.isEmpty(profileLink)) {
			Uri uri = getIntent().getData();

			if (null != sLastProfileLink && sLastProfileLink.equalsIgnoreCase(uri.getQueryParameter("UserProfileLink"))) {
				finish();
				return;
			} else {
				if (uri != null)
					sLastProfileLink = uri.getQueryParameter("UserProfileLink");
			}
		} else {
			sLastProfileLink = profileLink;
		}

		mNoFeedTV = (TextView) findViewById(R.id.no_feed_tv);
		mNewsFeedRequest = new NewsFeedRequest(mContext);
		getFriendProfileServerRequest(sLastProfileLink);
		mNewsFeeds.add(new NewsFeed(null));
		mBlankView = (View) findViewById(R.id.blankView);
		genericLv = (ListView) findViewById(R.id.genric_lv);
		mLoadingCenter = (ProgressBar) findViewById(R.id.loading_center_pb);
		mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);
		mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
		mNoMoreTV.setVisibility(View.INVISIBLE);
		mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
		mLoaderBottomPB.setVisibility(View.INVISIBLE);
		mLoaderBottomPB.getIndeterminateDrawable()
		.setColorFilter(getResources().getColor(R.color.red_normal_color), PorterDuff.Mode.SRC_IN);
		genericLv.addFooterView(mFooterRL);

		genericLv.setOnScrollListener(new MyCustomonScrol());
		FontLoader.setRobotoMediumTypeface(mUserProfileActivityViewHolder.mUserName, mUserProfileActivityViewHolder.mAboutTabTV,
				mUserProfileActivityViewHolder.mMediaTabTV, mUserProfileActivityViewHolder.mFriendsTabTV);
		FontLoader.setRobotoRegularTypeface(mNoFeedTV, mUserProfileActivityViewHolder.mUserType,
				mUserProfileActivityViewHolder.mFriendActionTV);

		mSchollyMeApplication.pauseSong();*/
    }

    private TextView mNoFeedTV;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String mOtherProfileLink = intent.getStringExtra("UserProfileLink");
        if (TextUtils.isEmpty(mOtherProfileLink)) {
            Uri uri = intent.getData();
            String mLink = uri.getQueryParameter("UserProfileLink");
        }
    }

    private View mBlankView;
    NewsFeedRequest mNewsFeedRequest;

    private void getFeedRequest(final NFFilter nffilter) {
        mNoMoreTV.setVisibility(View.INVISIBLE);
        if (nffilter.PageNo == 1) {
            if (mPostFeedAdapter.getCount() <= 1 || mPostFeedAdapter == null) {
                mNewsFeedRequest.setLoader(mLoadingCenter);
            } else {
                mNewsFeedRequest.setLoader(mBlankView);
            }
            mLoaderBottomPB.setVisibility(View.INVISIBLE);

        } else {
            mNewsFeedRequest.setLoader(mLoaderBottomPB);
            mLoaderBottomPB.setVisibility(View.VISIBLE);
        }
        mNewsFeedRequest.getNewsFeedListInServer(nffilter);
        mNewsFeedRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mSwipeRefreshWidget.setRefreshing(false);
                if (success) {

                    if (null != data) {
                        if (mNFFilter.PageNo == 1) {
                            resetList();
                        }
                        List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
                        mNewsFeeds.addAll(newsFeeds);
                        mPostFeedAdapter.setList(mNewsFeeds);

                        if (totalRecords == 0) {
                            isLoading = false;
                            mNoFeedTV.setVisibility(View.VISIBLE);

                        } else if (mNewsFeeds.size() < totalRecords) {
                            isLoading = true;
                            int newPage = mNFFilter.PageNo + 1;
                            mNFFilter.PageNo = newPage;
                            mNoFeedTV.setVisibility(View.GONE);

                        } else {
                            mNoMoreTV.setVisibility(View.VISIBLE);
                            isLoading = false;
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                        if (mNFFilter.PageNo == 1) {
                            mNoMoreTV.setVisibility(View.INVISIBLE);
                        } else {
                            mNoMoreTV.setVisibility(View.VISIBLE);
                        }

                    }
                } else {
                    isLoading = false;

                }
            }
        });
    }

    public void ItemClickListner(int ID, int position, NewsFeed newsFeed) {
        ClickLocation = position;
        switch (ID) {
            case R.id.share_tv:

                if (newsFeed.ShareAllowed == 1) {
                    sharePost(newsFeed);
                } else {
                    Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.like_tv:
                LikeMediaToggleService(newsFeed, position);
                break;
            case R.id.likes_tv:
                if (newsFeed.NoOfLikes != 0) {
                    startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID, "ACTIVITY", "WALL"),
                            REQ_FROM_HOME_LIKELIST);
                    this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
                }
                break;

            case R.id.comment_tv:
            case R.id.comments_tv:

                AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
                WriteCommentActivity.addFullPathIfNeeded(newsFeed, albumMedia);
                String captionForWriteCommentScreen = "";
                if (!newsFeed.PostContent.equals("")) {
                    captionForWriteCommentScreen = newsFeed.PostContent;
                } else {

                    if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
                    }

                }

                startActivityForResult(
                        CommentListActivity.getIntent(mContext, albumMedia, CommentListActivity.FOR_WALLPOST, captionForWriteCommentScreen),
                        REQ_FROM_HOME_COMMENTLIST);
                this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

                break;

            case PostFeedAdapter.ConvertViewID:

                startActivityForResult(CreateWallPostActivity.getIntent(mContext, WallPostCreateRequest.MODULE_ID_USERS, mOtherGUID),
                        CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
                break;

            default:
                break;
        }
    }

    boolean isLoading;

    class MyCustomonScrol implements OnScrollListener {
        private int mLastFirstVisibleItem;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int k = visibleItemCount + firstVisibleItem;
            if (k >= totalItemCount && isLoading && totalItemCount != 0) {
                getFeedRequest(mNFFilter);
            }
            if (mLastFirstVisibleItem < firstVisibleItem) {
                Log.i("SCROLLING DOWN", "TRUE");
            }
            if (mLastFirstVisibleItem > firstVisibleItem) {
                Log.i("SCROLLING UP", "TRUE");
            }
            mLastFirstVisibleItem = firstVisibleItem;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mNewsFeedRequest.setActivityStatus(true);

        switch (requestCode) {
            case REQ_FROM_HOME_LIKELIST:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        mNewsFeeds.get(ClickLocation).NoOfLikes = data.getIntExtra("COUNT", 0);
                        mNewsFeeds.get(ClickLocation).IsLike = data.getIntExtra("BUTTONSTATUS", 0);
                        mPostFeedAdapter.setList(mNewsFeeds);
                    }
                }
                break;
            case REQ_FROM_HOME_COMMENTLIST:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        mNewsFeeds.get(ClickLocation).NoOfComments = data.getIntExtra("COUNT", 0);
                        mPostFeedAdapter.setList(mNewsFeeds);
                    }

                }
                break;

            case CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mNFFilter.PageNo = 1;
                            getFeedRequest(mNFFilter);
                        }
                    }, 500);
                }
                break;

            default:
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    private void resetList() {
        if (mNFFilter != null)
            mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
        mNewsFeeds.clear();
        mNewsFeeds.add(new NewsFeed(null));
        if (mPostFeedAdapter != null)
            mPostFeedAdapter.setList(mNewsFeeds);
    }

    public void LikeMediaToggleService(final NewsFeed newsFeed, final int position) {
        ToggleLikeRequest mToggleLikeRequest = new ToggleLikeRequest(mContext);
        mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");

        mToggleLikeRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {

                    if (newsFeed.IsLike == 0) {

                        newsFeed.IsLike = 1;
                        newsFeed.NoOfLikes++;
                    } else {

                        newsFeed.IsLike = 0;
                        newsFeed.NoOfLikes--;
                    }
                    mPostFeedAdapter.notifyDataSetChanged();

                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), "");
                }

            }
        });
    }

    public void sharePost(NewsFeed newsFeedModel) {
        SharePostRequest mSharePostRequest = new SharePostRequest(mContext);
        mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
                NewsFeedRequest.MODULE_ID_USERS, loggedinUserModel.mUserGUID, 1, 1);
        mSharePostRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, MsgType.Success);
                } else {
                    mErrorLayout.showError((String) data, true, MsgType.Error);
                }
            }
        });
    }

    private void setUserData(UserModel mModel) {

        mUserProfileActivityViewHolder.mUserName.setText(mModel.mUserFullName);
        String mUserType = mModel.mUserTypeID;
        String mUserTypeName = "";
        if (mUserType.equals("1")) {
            mUserTypeName = String.valueOf(Config.UserType.Coach);
        } else if (mUserType.equals("2")) {
            mUserTypeName = String.valueOf(Config.UserType.Athlete);
        } else if (mUserType.equals("3")) {
            mUserTypeName = String.valueOf(Config.UserType.Fan);
        }
        mUserProfileActivityViewHolder.mUserType.setText(mUserTypeName);
        String imageUrl = Config.IMAGE_URL_PROFILE + mModel.mUserProfilePic;
        ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mUserProfileActivityViewHolder.mProfileImageIv,
                ImageLoaderUniversal.option_Round_Image);
        final String mOUrl = Config.IMAGE_URL_PROFILE_ORIGINAL + mModel.mUserProfilePic;
        mUserProfileActivityViewHolder.mProfileImageIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(ProfileImageViewerActivity.getIntent(mContext, mOUrl));
            }
        });

        String mUserFriendStatus = mModel.mFriendStatus;
        if (mUserFriendStatus.equals("1")) {
            mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.friend_label));
            mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.VISIBLE);
        } else if (mUserFriendStatus.equals("2")) {
            mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.request_pending_label));
            mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
        } else if (mUserFriendStatus.equals("3")) {
            mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.accept__friend_label));
            mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
        } else if (mUserFriendStatus.equals("4")) {
            mUserProfileActivityViewHolder.mFriendActionTV.setVisibility(View.VISIBLE);
            mUserProfileActivityViewHolder.mFriendActionTV.setText(getResources().getString(R.string.add_label));
            mUserProfileActivityViewHolder.mFriendActionTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_add_friend, 0, 0, 0);
            mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
        } else {
            mUserProfileActivityViewHolder.mFriendsTabTV.setVisibility(View.GONE);
        }

        mUserProfileActivityViewHolder.mFriendActionTV.setOnClickListener(this);

        if (mModel.mUserTypeID.equals(String.valueOf(Config.USER_TYPE_ATHLETE))) {
            mUserProfileActivityViewHolder.mMoreTabTV.setVisibility(View.VISIBLE);
        } else {
            mUserProfileActivityViewHolder.mMoreTabTV.setVisibility(View.GONE);
        }
    }

    public static Intent getIntent(Context context, String vv, String userProfileLink, String fromActivity) {
        Intent intent = new Intent(context, FriendProfilePrlxActivity.class);
        intent.putExtra("UserProfileLink", userProfileLink);
        intent.putExtra("OtherGUID", vv);
        intent.putExtra("fromActivity", fromActivity);
        return intent;
    }

    public class UserProfileActivityViewHolder {

        //		public ScrollParrallaxViewX scrollView;
        public TextView mUserName;
        public TextView mUserType;
        public TextView mFriendActionTV;
        public ImageView mProfileImageIv;
        public RelativeLayout mUserInfoRL;
        public TextView mAboutTabTV, mMediaTabTV, mFriendsTabTV, mMoreTabTV;
        public ImageButton mBackIB, mMessageIB, mSODList, mSodIb, mHomeIB;
        public ProgressBar mLoadingPb;

        public UserProfileActivityViewHolder(View view, OnClickListener listener) {
            //		scrollView = (ScrollParrallaxViewX) view.findViewById(R.id.scrollViewX1);
            mProfileImageIv = (ImageView) view.findViewById(R.id.profile_image_iv);
            mUserName = (TextView) view.findViewById(R.id.user_name_tv);
            mUserType = (TextView) view.findViewById(R.id.user_status_tv);
            mUserInfoRL = (RelativeLayout) view.findViewById(R.id.profile_image_name_rl);

            mAboutTabTV = (TextView) view.findViewById(R.id.about_tab_tv);
            mMediaTabTV = (TextView) view.findViewById(R.id.media_tab_tv);
            mFriendsTabTV = (TextView) view.findViewById(R.id.friends_tab_tv);
            mMoreTabTV = (TextView) view.findViewById(R.id.more_tab_tv);
            mBackIB = (ImageButton) view.findViewById(R.id.back_ib);
            mFriendActionTV = (TextView) view.findViewById(R.id.friend_action_tv);
            mMessageIB = (ImageButton) view.findViewById(R.id.message_ib);
            mSODList = (ImageButton) view.findViewById(R.id.sod_list_ib);
            mSodIb = (ImageButton) view.findViewById(R.id.sod_ib);
            mHomeIB = (ImageButton) view.findViewById(R.id.home_ib);
            mLoadingPb = (ProgressBar) view.findViewById(R.id.loading_pb);

            mAboutTabTV.setOnClickListener(listener);
            mFriendsTabTV.setOnClickListener(listener);
            mMediaTabTV.setOnClickListener(listener);
            mMoreTabTV.setOnClickListener(listener);
            mBackIB.setOnClickListener(listener);
            mHomeIB.setOnClickListener(listener);
            mFriendActionTV.setOnClickListener(listener);
            mMessageIB.setOnClickListener(listener);
            mSODList.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mUserModel) {
                        startActivity(UserSongsActivity.getIntent(v.getContext(), mUserModel.mUserGUID, mUserModel.mUserFullName));
                    }
                }
            });
            mSodIb.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mUserModel) {
                        fetchSongIfNeeded(v);
                    }
                }
            });
        }

        public void showLoader(boolean show) {
            mLoadingPb.setVisibility(show ? View.VISIBLE : View.GONE);
            mSodIb.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_tab_tv:
                if (mUserModel != null) {
                    if (mOtherGUID.equals(loggedinUserModel.mUserGUID)) {
                        startActivity(AboutActivity.getIntent(mContext, loggedinUserModel.mUserProfileURL, "FriendProfileActivity"));
                    } else {
                        startActivity(AboutActivity.getIntent(mContext, mUserModel.mUserProfileURL, "FriendProfileActivity"));
                    }
                }
                break;
            case R.id.friends_tab_tv:
                if (mUserModel != null) {
                    if (mOtherGUID.equals(loggedinUserModel.mUserGUID)) {
                        startActivity(FriendsActivity.getIntent(mContext, 0, ""));
                    } else {
                        startActivity(OtherUserFriendsActivity.getIntent(mContext, mOtherGUID));
                    }
                }
                break;
            case R.id.media_tab_tv:

                if (mUserModel != null) {
                    if (mOtherGUID.equals(loggedinUserModel.mUserGUID)) {
                        startActivity(AlbumGridActivity.getIntent(mContext, loggedinUserModel.mUserGUID, getString(R.string.My_Albums)));
                    } else {
                        startActivity(AlbumGridActivity.getIntent(mContext, mOtherGUID, mUserModel.mUserFullName + "'s" + " Albums"));
                    }
                }
                break;
            case R.id.more_tab_tv:
                boolean isPaidCoach = false;
                boolean canCoachEvaluateForAthlete = false;
                if (loggedinUserModel.mUserType == 1 && loggedinUserModel.isEvaluator.equals("1")) {
                    for (SportsModel mSport : mUserModel.mUserSportsAL) {
                        if (mSport.mSportsID.equals(loggedinUserModel.mUserSportsId)) {
                            canCoachEvaluateForAthlete = true;
                            //	showMoreOptions(canCoachEvaluateForAthlete,false);
                            showMoreOptionsListDialog(canCoachEvaluateForAthlete, false);
                            break;
                        }
                    }
                } else if (loggedinUserModel.mUserType == 1 && loggedinUserModel.isPaidCoach.equals("1")) {
                    isPaidCoach = true;

                    for (SportsModel mSport : mUserModel.mUserSportsAL) {
                        if (mSport.mSportsID.equals(loggedinUserModel.mUserSportsId)) {
                            canCoachEvaluateForAthlete = true;
                            //showMoreOptions(canCoachEvaluateForAthlete,true);

                            showMoreOptionsListDialog(canCoachEvaluateForAthlete, true);
                            break;
                        }
                    }

                }

                if (!canCoachEvaluateForAthlete) {
                    showMoreOptionsListDialog(false, isPaidCoach);
                }

                break;
            case R.id.back_ib:
                if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
                    Intent intent = DashboardActivity.getIntent(this, 4, "");
                    startActivity(intent);
                    this.finish();
                } else
                    finish();

                break;
            case R.id.friend_action_tv:
                performActionOnFriendButton();
                break;
            case R.id.message_ib:
                if (mUserModel != null) {
                    String userName = mUserModel.mUserFullName;
                    FriendModel mFriendModel = new FriendModel();
                    mFriendModel.mFirstName = mUserModel.mUserFullName;
                    mFriendModel.mLastName = "";
                    mFriendModel.mProfileLink = mUserModel.mUserProfileURL;
                    mFriendModel.mProfilePicture = mUserModel.mUserProfilePic;
                    mFriendModel.mUserTypeID = mUserModel.mUserTypeID;
                    mFriendModel.mUserGUID = mOtherGUID;
                    mFriendModel.mFriendStatus = mUserModel.mFriendStatus;
                    mContext.startActivity(ChatHistoryActivity.getIntent(mContext, userName, mFriendModel, "0"));
                }
                break;

            case R.id.home_ib:
                Intent intent = DashboardActivity.getIntent(mContext, DashboardActivity.HOME_FRAGMENTINDEX, "");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }

    private Song mSODSong = null;

    private void fetchSongIfNeeded(final View view) {
        if (null == mSODSong) {
            mUserProfileActivityViewHolder.showLoader(true);
            SongOfTheDayRequest songOfTheDayRequest = new SongOfTheDayRequest(view.getContext());
            songOfTheDayRequest.setRequestListener(new RequestListener() {

                @Override
                public void onComplete(boolean success, Object data, int totalRecords) {
                    if (success) {
                        if (null != data && data instanceof Song) {
                            mSODSong = (Song) data;
                            handleSongPlay(view, mSODSong);
                        }
                    } else {
                        mUserProfileActivityViewHolder.showLoader(false);
                        mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true,
                                MsgType.Error);
                    }
                }
            });
            songOfTheDayRequest.getSongOfDayFromServer(mUserModel.UserGUID);
        } else if (null != genericLv) {
            handleSongPlay(view, mSODSong);
        }
    }

    private void handleSongPlay(final View v, final Song song) {
        final String songPlayUrl = TextUtils.isEmpty(song.SongURL) ? song.SongPreviewURL : song.SongURL;
        if (TextUtils.isEmpty(songPlayUrl) || !android.util.Patterns.WEB_URL.matcher(songPlayUrl).matches()) {
            mUserProfileActivityViewHolder.showLoader(false);
            mErrorLayout.showError(v.getResources().getString(R.string.No_Song_Found), true, MsgType.Error);
            return;
        }

        if (mSchollyMeApplication.isPlaying()) {
            mSchollyMeApplication.pauseSong();
        } else if (mSchollyMeApplication.playSong(song.Title, songPlayUrl, new SchollyMeApplication.SongLoadListener() {

            @Override
            public void onLoad(boolean success, String url, MediaPlayer mediaPlayer) {
                mUserProfileActivityViewHolder.showLoader(false);
                if (!url.equalsIgnoreCase(song.SongURL)) {
                    return;
                }
                if (!success) {
                    mErrorLayout.showError(v.getResources().getString(R.string.This_song_can_not), true, MsgType.Error);
                }
            }
        })) {
            //v.setEnabled(false);
        } else if (null != mErrorLayout && mSchollyMeApplication.isBuffering()) {
            mUserProfileActivityViewHolder.showLoader(false);
            mErrorLayout.showError(v.getResources().getString(R.string.Buffering), true, MsgType.Error);
        } else if (null != mErrorLayout) {
            mUserProfileActivityViewHolder.showLoader(false);
            mErrorLayout.showError(v.getResources().getString(R.string.Invalid_song_URL), true, MsgType.Error);
        }
    }

    @Override
    protected void onDestroy() {
        ((SchollyMeApplication) getApplication()).cancelNotifications(true);
        super.onDestroy();
    }

    private void showMoreOptionsListDialog(final boolean isEvaluator, final boolean isPaidCoach) {

        final String mFriendName = mUserModel.mUserFullName + "'s " + getString(R.string.users_Buzz);
        final String mScoresLabel = mUserModel.mUserFullName + "'s " + getString(R.string.friends_score);


        if (isEvaluator) {
            options = new String[]{getString(R.string.Evaluation), mFriendName};
        }
        if (isPaidCoach) {
            options = new String[]{getString(R.string.Evaluation), mFriendName, mScoresLabel};
        } else {
            options = new String[]{mFriendName};
        }

        DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {

            @Override
            public void onItemClick(int position, String item) {
                if (options.length == 1) {
                    startActivity(BuzzActivity.getIntent(mContext, mUserModel.mUserGUID, mFriendName));
                } else if (options.length == 2) {
                    if (position == 0) {
                        if (isPaidCoach) {
                            startActivity(EvaluationActivity.getIntent(mContext, mUserModel.mUserGUID, mUserModel.mUserFullName, loggedinUserModel.mUserSportsId,
                                    loggedinUserModel.mUserSportsName, Config.USER_TYPE_PAID_COACH));
                        } else {
                            startActivity(EvaluationActivity.getIntent(mContext, mUserModel.mUserGUID, mUserModel.mUserFullName, loggedinUserModel.mUserSportsId,
                                    loggedinUserModel.mUserSportsName, Config.USER_TYPE_EVALUATOR));
                        }
                    } else if (position == 1) {
                        startActivity(BuzzActivity.getIntent(mContext, mUserModel.mUserGUID, mFriendName));
                    }
                }
                if (options.length == 3) {
                    if (position == 0) {
                        startActivity(EvaluationActivity.getIntent(mContext, mUserModel.mUserGUID, mUserModel.mUserFullName, loggedinUserModel.mUserSportsId,
                                loggedinUserModel.mUserSportsName, Config.USER_TYPE_PAID_COACH));
                    } else if (position == 1) {
                        startActivity(BuzzActivity.getIntent(mContext, mUserModel.mUserGUID, mFriendName));
                    } else if (position == 2) {
                        startActivity(FriendsScoreActivity.getIntent(mContext, mUserModel.mUserGUID, mScoresLabel));
                    }
                }
            }

            @Override
            public void onCancel() {

            }
        }, options);
    }

    @Override
    public void onBackPressed() {
        if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
            Intent intent = DashboardActivity.getIntent(this, 4, "");
            startActivity(intent);
            this.finish();
        } else
            super.onBackPressed();
    }

    private void performActionOnFriendButton() {
        try {
            if (mUserModel.mFriendStatus.equals("1")) {
                // All ready friend no action needed
            } else if (mUserModel.mFriendStatus.equals("2")) {
                // You have sent friend request but other user not confirmed it
            } else if (mUserModel.mFriendStatus.equals("3")) {
                // You have not confirmed the friend request from other user
                DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {
                        String customeMessage = position == 0 ? mContext.getResources().getString(R.string.added_as_friend) : mContext
                                .getResources().getString(R.string.added_as_teammate);
                        String message = mUserModel.mUserFullName + " " + customeMessage;
                        acceptFriendRequest(mOtherGUID, message, String.valueOf(position));
                    }

                    @Override
                    public void onCancel() {

                    }
                }, mContext.getString(R.string.add_as_friend), mContext.getString(R.string.add_as_teammate));
            } else if (mUserModel.mFriendStatus.equals("4")) {
                // Not yet friends,Send friend request
                DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {
                        addFriendRequest(mOtherGUID, "" + position);
                    }

                    @Override
                    public void onCancel() {

                    }
                }, mContext.getString(R.string.add_as_friend), mContext.getString(R.string.add_as_teammate));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFriendProfileServerRequest(String mUserProfileLink) {
        AboutRequest mRequest = new AboutRequest(mContext);
        mRequest.AboutRequestServer(new LogedInUserModel(mContext).mLoginSessionKey, mUserProfileLink);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mUserModel = (UserModel) data;
                    setUserData(mUserModel);
                    mNFFilter = new NFFilter(NewsFeedRequest.MODULE_ID_USERS, mUserModel.UserGUID);
                    mOtherGUID = mUserModel.UserGUID;

                    mPostFeedAdapter = new PostFeedAdapter(mContext, new OnItemClickListenerPost() {

                        @Override
                        public void onClickItems(int ID, int position, NewsFeed newsFeed) {
                            ItemClickListner(ID, position, newsFeed);
                        }
                    }, mOtherGUID);
                    genericLv.setAdapter(mPostFeedAdapter);
                    getFeedRequest(mNFFilter);
                } else {
                    final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                            getResources().getString(R.string.app_name), new OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {
                                    finish();
                                }
                            });
                    builder.setCancelable(false);
                    builder.setOnKeyListener(new Dialog.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                finish();
                                builder.dismiss();
                            }
                            return true;
                        }
                    });
                }
            }
        });
    }

    /**
     * Send friend request to user to whom profiel is open
     */
    private void addFriendRequest(String friendGUID, String teammate) {
        FriendAddRequest mRequest = new FriendAddRequest(mContext);
        mRequest.AddFriendServerRequest(friendGUID, teammate);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(getResources().getString(R.string.friend_request_sent) + " " + mUserModel.mUserFullName, true,
                            MsgType.Success);
                    mUserModel.mFriendStatus = "2";
                    setUserData(mUserModel);
                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
                }
            }
        });
    }

    /**
     * Server request to accept friend request from a user to whom profile is open
     */
    private void acceptFriendRequest(String friendGUID, final String message, String teammate) {
        FriendAddRequest mRequest = new FriendAddRequest(mContext);
        mRequest.AddFriendServerRequest(friendGUID, teammate);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mUserModel.mFriendStatus = "1";
                    setUserData(mUserModel);
                    mErrorLayout.showError(message, true, MsgType.Success);
                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
                }
            }
        });
    }


    /**
     * Call when user pull to refresh friends feeds
     */
    @Override
    public void onRefresh() {
        if (HttpConnector.getConnectivityStatus(mContext) == 0) {
            DialogUtil.showOkDialogButtonLisnter(mContext, getResources().getString(R.string.No_internet_connection), getResources()
                    .getString(R.string.app_name), new OnOkButtonListner() {

                @Override
                public void onOkBUtton() {

                }
            });
            mSwipeRefreshWidget.setRefreshing(false);
        } else {
            if (null != mNFFilter) {
                mNFFilter.PageNo = 1;
                getFeedRequest(mNFFilter);
            }
        }
    }
}