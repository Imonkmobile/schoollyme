package com.schollyme;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.friends.FriendProfileActivity;
import com.schollyme.activity.LoginActivity;
import com.schollyme.activity.TranscriptsRequestFromCoachActivity;
import com.schollyme.evaluation.EvaluationDetailActivity;
import com.schollyme.evaluation.WriteEvaluationActivity;
import com.schollyme.friends.FriendsActivity;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.scores.MyScoresActivity;
import com.schollyme.wall.MediaViewerWallActivity;
import com.vinfotech.utility.Config;

/**
 * Receive a push message from the Google cloud messaging (GCM) service. This
 * class should be modified to include functionality specific to your
 * application. This class must have a no-arg constructor and pass the sender id
 * to the superclass constructor.
 *
 * @author ramanands
 */

public class GCMIntentService extends GCMBaseIntentService {

    private static final String LOG_TAG = GCMIntentService.class.getSimpleName();
    private static int sNotificationId = 103;
    public static final String PROJECT_ID = "141815383664"; //128215161057
    private static GCMRegisterListener sGCMRegisterListener;

    /**
     * Register the device for GCM.
     */
    public static void register(Context mContext) {
        GCMRegistrar.checkDevice(mContext);
        GCMRegistrar.checkManifest(mContext);
        GCMRegistrar.register(mContext, PROJECT_ID);
    }

    public GCMIntentService() {
        super(PROJECT_ID);
    }

    public static void addGCMRegisterListener(GCMRegisterListener listener) {
        GCMIntentService.sGCMRegisterListener = listener;
    }

    /**
     * Called on registration error. This is called in the context of a Service
     * - no dialog or UI.
     */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(LOG_TAG, "onError error id:" + errorId);
        if (null != sGCMRegisterListener) {
            sGCMRegisterListener.onErrored();
        }
    }

    /**
     * Called when a cloud message has been received.
     */
    @Override
    protected void onMessage(Context context, Intent intent) {
        for (String key : intent.getExtras().keySet()) {
            Log.d("GCMIntentService", "onMessage - key=" + key);
        }

        //	String message = null != intent.getExtras().getString("Message") ? intent.getExtras().getString("Message") : intent.getExtras()
        //			.getString("message");
        if (isNotificationEnable()) {
            setNotification(intent);
        }
    }

    /**
     * Called when a registration token has been received.
     */
    @Override
    protected void onRegistered(Context context, String registration) {
        Log.i(LOG_TAG, "Registered Device Start:" + registration);
        if (null != sGCMRegisterListener) {
            sGCMRegisterListener.onRegistered(registration);
        }
    }

    /**
     * Called when the device has been unregistered.
     */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(LOG_TAG, "unRegistered Device registrationId:" + registrationId);
    }


    private boolean isNotificationEnable() {
        SharedPreferences mNotificationPreferance = getSharedPreferences(Config.NOTIFICATION_SETTINGS, 0);
        if (mNotificationPreferance.getBoolean("isOn", true)) {
            return true;
        } else {
            return false;
        }
    }


    private void setNotification(Intent intent) {

        String message = null != intent ? intent.getStringExtra("Message") : "";
        //Bundle[{from=128215161057, collapse_key=do_not_collapse, Message=Shishir posted on you wall.,
        // NotificationType=POST, Badge=13, ProfileURL=4db25a93-775a-c8d6-db1f-2c28b5ddf16e}]
        Log.v("Push Notification", intent.getExtras().toString());
        //Bundle[{collapse_key=do_not_collapse, NotificationType=FRIEND, Message=Ram has added you as a friend.,
        // NotificationTypeID=16, from=128215161057, ProfileURL=ram, Badge=39}]

        //int id = intent.getStringExtra("Message")
        /*NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
		        .setAutoCancel(true)
				.setContentTitle(getResources().getString(R.string.app_name)).setContentText(message);*/


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(message);
        mBuilder.setAutoCancel(true);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);

        String EntityGUID = intent.getStringExtra("EntityGUID");
        String mNameTitle = intent.getStringExtra("UserName");

        Intent transactionIntent = screenRedirection(intent.getStringExtra("NotificationType"),
                intent.getStringExtra("ProfileURL"), EntityGUID,
                intent.getStringExtra("NotificationTypeID"), mNameTitle,
                intent.getStringExtra("Refer"),
                intent.getStringExtra("ProfileURL"));
        transactionIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        sNotificationId = (int) System.currentTimeMillis();
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, sNotificationId,
                transactionIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.

        mNotificationManager.notify(sNotificationId, mBuilder.build());
    }

    private Intent screenRedirection(String type, String link, String EntityGUID, String notiTypeId,
                                     String mUserName, String refer, String profileURL) {

        Intent intent = null;
        if (LogedInUserModel.isEmptyPreferance(this)) {
            intent = new Intent(this, LoginActivity.class);

        } else {

            switch (type) {
                case "POST": // Wall post 18
                    if (refer.equalsIgnoreCase("VIDEO_ALBUM")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("mediagrid://app.postfeed.wall/params?UserGUID="
                                        + new LogedInUserModel(this).mUserGUID + "&FromQuery=FromQuery&AlbumGUID=" + profileURL));
                        startActivity(browserIntent);
                    } else if (refer.equalsIgnoreCase("PHOTO_ALBUM")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("mediagrid://app.postfeed.wall/params?UserGUID="
                                        + new LogedInUserModel(this).mUserGUID + "&FromQuery=FromQuery&AlbumGUID=" + profileURL));
                        startActivity(browserIntent);
                    } else if (refer.equalsIgnoreCase("WALL")) {
                        intent = DashboardActivity.getIntent(this, DashboardActivity.HOME_FRAGMENTINDEX, "GCMIntentService"); // 4

                    } else if (!refer.equalsIgnoreCase("VIDEO_ALBUM") && !refer.equalsIgnoreCase("PHOTO_ALBUM")
                            && !refer.equalsIgnoreCase("WALL")) {
                        intent = new Intent(this, DashboardActivity.class);
                        intent.putExtra("UserProfileLink", link);
                        intent.putExtra("fromActivity", "GCMIntentService");
                    }

                    break;

                case "MEDIA": // Wall post 18
                    if (refer.equalsIgnoreCase("VIDEO")) {
                        startActivity(MediaViewerWallActivity.getIntent(this, profileURL, EntityGUID));
                    } else if (refer.equalsIgnoreCase("PHOTO")) {
                        startActivity(MediaViewerWallActivity.getIntent(this, profileURL, EntityGUID));
                    }

                    break;

                case "SHARED_ACTIVITY": // Wall post 18
                    if (refer.equalsIgnoreCase("VIDEO_ALBUM")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("mediagrid://app.postfeed.wall/params?UserGUID="
                                        + new LogedInUserModel(this).mUserGUID + "&FromQuery=FromQuery&AlbumGUID=" + profileURL));
                        startActivity(browserIntent);
                    } else if (refer.equalsIgnoreCase("PHOTO_ALBUM")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("mediagrid://app.postfeed.wall/params?UserGUID="
                                        + new LogedInUserModel(this).mUserGUID + "&FromQuery=FromQuery&AlbumGUID=" + profileURL));
                        startActivity(browserIntent);
                    } else if (refer.equalsIgnoreCase("VIDEO")) {
                        startActivity(MediaViewerWallActivity.getIntent(this, profileURL, EntityGUID));
                    } else if (refer.equalsIgnoreCase("PHOTO")) {
                        startActivity(MediaViewerWallActivity.getIntent(this, profileURL, EntityGUID));
                    }

                    break;


                case "FRIEND": // Request/Accept Friend Request 1
                    if (notiTypeId.equalsIgnoreCase("16"))
                        intent = FriendsActivity.getIntent(this, 0, "GCMIntentService");
                    else
                        intent = FriendProfileActivity.getIntent(this, "", link, "GCMIntentService");

                    break;

                case "USER_SCORE_VERIFICATION": // Transcript 34
                    //intent = AboutActivity.getIntent(this, link, "GCMIntentService");
                    intent = MyScoresActivity.getIntent(this, new LogedInUserModel(this).mUserGUID, new LogedInUserModel(this).mFirstName);
                    break;

                case "TRANSCRIPT_VIEW": //37
                    intent = TranscriptsRequestFromCoachActivity.getIntent(this, "GCMIntentService");
                    break;

                case "SCORE_VERIFICATION_REQUEST": // Transcript 37
                    intent = MyScoresActivity.getIntent(this, new LogedInUserModel(this).mUserGUID, new LogedInUserModel(this).mFirstName);
                    //AboutActivity.getIntent(this, link, "GCMIntentService");
                    break;

                case "SONG_OF_THE_DAY": // Transcript 37
                    intent = DashboardActivity.getIntent(this, 3, "GCMIntentService");  //1

                    break;

                case "TRANSCRIPT_VIEW_STATUS": // Transcript 37
                    // send on Notification screen.
                    intent = DashboardActivity.getIntent(this, 4, "GCMIntentService"); // 6

                    break;

                case "COACH_MARK_AS_PAID": // Transcript 37
                    LogedInUserModel.updatePreferenceString(this, "IsPaidCoach", "1");
                    LogedInUserModel.updatePreferenceString(this, "isEvaluator", "0");
                    intent = DashboardActivity.getIntent(this, DashboardActivity.HOME_FRAGMENTINDEX, "GCMIntentService"); //7
                    break;

                case "MARKED_AS_EVALUATOR": // Transcript 37
                    LogedInUserModel.updatePreferenceString(this, "IsPaidCoach", "0");
                    LogedInUserModel.updatePreferenceString(this, "isEvaluator", "1");
                    intent = DashboardActivity.getIntent(this, DashboardActivity.HOME_FRAGMENTINDEX, "GCMIntentService"); // 7
                    break;

                case "PAYMENT_CONFIRMATION": // Transcript 37
                    intent = MyScoresActivity.getIntent(this, new LogedInUserModel(this).mUserGUID, new LogedInUserModel(this).mFirstName);
                    //DashboardActivity.getIntent(this, 5, "GCMIntentService");
                    break;

                case "NEW_EVALUATOR_REQUEST": //64
                    LogedInUserModel model = new LogedInUserModel(this);
                    mUserName = mUserName + "'s " + getString(R.string.Evaluation);
                    intent = WriteEvaluationActivity.getIntent(this, link, model.mUserSportsId,
                            model.mUserSportsName, EntityGUID, "GCMIntentService", mUserName);
                    break;

                case "EVALUATION_POST": //65
                    intent = EvaluationDetailActivity.getIntent(this, link, getString(R.string.My_Evaluation), null,
                            EntityGUID, "GCMIntentService");
                    break;

                case "70":
                    intent = DashboardActivity.getIntent(this, DashboardActivity.HOME_FRAGMENTINDEX, "GCMIntentService"); //2
                    break;

                default:
                    intent = DashboardActivity.getIntent(this, DashboardActivity.HOME_FRAGMENTINDEX, "GCMIntentService"); // 4
                    break;
            }
        }
        return intent;

    }

    /**
     * Interface to listen for registration process with the C2DM server
     *
     * @author ramanands
     */
    public interface GCMRegisterListener {
        /**
         * Registered successfully
         *
         * @param deviceId the device id return by the C2DM server on successful
         *                 registration
         */
        void onRegistered(String deviceId);

        /**
         * Error during registration process, may be re-init the process
         */
        void onErrored();
    }
}