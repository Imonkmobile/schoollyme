package com.schollyme.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.adapter.FilterByCategoryAdapter;
import com.schollyme.model.FilterBy;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.utility.DialogUtil;

import java.util.Calendar;

public class FilterListActivity extends BaseFragmentActivity {

    private Context mContext;
    final int REQ_CODE_FILTER_SCREEN = 789;
    final int REQ_CODE_FILTER_SEARCH_SCREEN = 790;
    private FilterByCategoryAdapter mFilterByCategoryAdapter;
    private FilterBy mFilterBy;
    private String selectCategoryforNonCoach[] = new String[4];
    private String selectCategoryforPaidCoach[] = new String[7];
    private LogedInUserModel mLogedInUserModel;
    private int paidType = 0;
    private boolean is_smart;


    private ListView mFilterCAtegotyLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_list);
        mContext = this;
        mFilterBy = getIntent().getParcelableExtra("filterBy");
        if (null == mFilterBy) {
            Toast.makeText(getApplicationContext(), "Invalid Filter", Toast.LENGTH_SHORT).show();
            this.finish();
            return;
        }

        initHeader();

        selectCategoryforNonCoach = new String[]{
                getResources().getString(R.string.team_sport),
                getResources().getString(R.string.state_label),
                getResources().getString(R.string.gender_label),
                getResources().getString(R.string.profile_type),
                getResources().getString(R.string.fav_team_label)
        }; //


        selectCategoryforPaidCoach = new String[]{
                getResources().getString(R.string.team_sport),
                getResources().getString(R.string.state_label),
                getResources().getString(R.string.gender_label),
                getResources().getString(R.string.profile_type),
                getResources().getString(R.string.gpa_hint_label),
                getResources().getString(R.string.sat_label),
                getResources().getString(R.string.recruiting_class),
                getResources().getString(R.string.verified)};
        initialization();
    }

    private void initHeader() {

        is_smart = getIntent().getBooleanExtra("is_smart", false);
        String title = "";
        if (is_smart) {
            title = getString(R.string.SMART);
        } else {
            title = getString(R.string.search_by_common_interest);
        }

        setHeader(R.drawable.ic_close, R.drawable.ic_check_header, title,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        applyNewValuesInfilter();
                    }
                });
    }

    private void applyNewValuesInfilter() {

        if (is_smart) {
            Intent inActivity = new Intent(FilterListActivity.this, SearchScreenActivity.class);
            inActivity.putExtra("filterBy", mFilterBy);
            inActivity.putExtra("UserType", mLogedInUserModel.mUserType);
            startActivity(inActivity);
        } else {
            Intent in2 = new Intent();
            in2.putExtra("filterBy", mFilterBy);
            setResult(RESULT_OK, in2);
        }

        this.finish();
        this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
    }


    private void initialization() {

        mFilterCAtegotyLV = (ListView) findViewById(R.id.filter_categoty_lv);

        mLogedInUserModel = new LogedInUserModel(this);
        if (mLogedInUserModel.isPaidCoach.equals("0")) {
            paidType = 0;
            mFilterByCategoryAdapter = new FilterByCategoryAdapter(FilterListActivity.this,
                    selectCategoryforNonCoach, mFilterBy, paidType);
        } else {
            paidType = 1;
            mFilterByCategoryAdapter = new FilterByCategoryAdapter(FilterListActivity.this,
                    selectCategoryforPaidCoach, mFilterBy, paidType);
        }

        mFilterCAtegotyLV.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mFilterCAtegotyLV.setAdapter(mFilterByCategoryAdapter);
        mFilterCAtegotyLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (is_smart) {
                    if (position == 6) {
                        showYearPicker();

                    }else if(position == 7){
                        if(mFilterBy.getVerified()==0){
                            mFilterBy.setVerified(1);
                        }else{
                            mFilterBy.setVerified(0);
                        }
                        mFilterByCategoryAdapter.notifyDataSetChanged();
                    } else {
                        startActivityForResult(SelectFilterItemActivity.getIntent(FilterListActivity.this, mFilterBy,
                                position, paidType).putExtra("paidType", paidType), REQ_CODE_FILTER_SEARCH_SCREEN);
                    }
                } else {
                    if (position == 6) {
                        showYearPicker();
                    }else if(position == 7){
                        if(mFilterBy.getVerified()==0){
                            mFilterBy.setVerified(1);
                        }else{
                            mFilterBy.setVerified(0);
                        }
                        mFilterByCategoryAdapter.notifyDataSetChanged();
                    } else {
                        startActivityForResult(SelectFilterItemActivity.getIntent(FilterListActivity.this, mFilterBy,
                                position, paidType).putExtra("paidType", paidType), REQ_CODE_FILTER_SCREEN);
                    }
                }


            }
        });
    }

    private void showYearPicker() {
        try {
            final Calendar c = Calendar.getInstance();
            int year = 2015;
            int y = c.get(Calendar.YEAR);
            int changCount = 9;
            int totalYears = y - year + changCount;
            int total = 0;

            String years[] = new String[totalYears];

            for (int i = 0; i < totalYears; i++) {
                int mYear = year + i;
                if (y == mYear) {
                    break;
                }
                total = i;
                years[i] = String.valueOf(mYear);
            }
            for (int j = 0; j < changCount; j++) {
                int cYear = y + j;
                years[total + j + 1] = String.valueOf(cYear);
            }
            DialogUtil.showListDialog(mContext, 0, new DialogUtil.OnItemClickListener() {
                @Override
                public void onItemClick(int position, String item) {
                    if (!TextUtils.isEmpty(item)) {
                        mFilterBy.setRecruitingClass(item);
                        mFilterByCategoryAdapter.notifyDataSetChanged();
                    }
                    //mSetupProfileViewHolder.mNCAAET.setText(item);
                }

                @Override
                public void onCancel() {

                }
            }, years);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Intent getIntent(Context context, FilterBy filterBy) {
        Intent intent = new Intent(context, FilterListActivity.class);
        intent.putExtra("filterBy", filterBy);
        return intent;
    }

    public Boolean isListSelected(int bb) {
        return mFilterCAtegotyLV.isItemChecked(bb);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // mSearchUserRequest.setActivityStatus(false);
        switch (requestCode) {
            case REQ_CODE_FILTER_SCREEN:
                if (resultCode == RESULT_OK) {
                    mFilterBy = data.getParcelableExtra("filterBy");
//                    Intent in3 = new Intent();
//                    in3.putExtra("filterBy", mFilterBy);
//                    setResult(RESULT_OK, in3);
                    //finish();
                }
                break;
            case REQ_CODE_FILTER_SEARCH_SCREEN:
                if (resultCode == RESULT_OK) {
                    mFilterBy = data.getParcelableExtra("filterBy");
//                    Intent inActivity = new Intent(FilterListActivity.this, SearchScreenActivity.class);
//                    inActivity.putExtra("filterBy", mFilterBy);
//                    inActivity.putExtra("UserType", mLogedInUserModel.mUserType);
//                    startActivity(inActivity);
                    //finish();
                }
                break;
            default:
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

}
