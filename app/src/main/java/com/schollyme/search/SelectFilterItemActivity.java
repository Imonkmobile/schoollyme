package com.schollyme.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.adapter.FavTeamListAdapter;
import com.schollyme.adapter.SimpleDataINFilterListAdapter;
import com.schollyme.adapter.SportsListAdapter;
import com.schollyme.adapter.StateListAdapter;
import com.schollyme.model.FavTeamModel;
import com.schollyme.model.FilterBy;
import com.schollyme.model.SportsModel;
import com.schollyme.model.StateModel;
import com.vinfotech.request.GetFavTeamRequest;
import com.vinfotech.request.GetSportsRequest;
import com.vinfotech.request.GetStatesRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Utility;

import java.util.ArrayList;
import java.util.List;

public class SelectFilterItemActivity extends BaseFragmentActivity {

    private static Context context;
    private LinearLayout mGenderLay, mPositionLay, mTypeofUserLay, mGpaLL, mSatLL, mRecruitLL, mActLL;
    private ListView mChoieSportLV, mChoiceStateLV, mPostionEnterLV, mFavoriteTeamLV /*,mRecructingClassLV, mVerifiedLV*/ ;
    private RelativeLayout lay_container, mLoadingRL;
    private CheckedTextView mAtheleteCTV, mFanCTV, mCoachCTV, mAllUserCTV;
    private RadioGroup mGpaRG, mSatRG, mActRG, mGenderRG;
    public static int paidType = 0;

    private FilterBy mFilterBy;
    private int position;
    private List<SportsModel> mSportModels;
    private List<FavTeamModel> mTeamModel;
    private SportsListAdapter mSportsListAdapter;

    private SimpleDataINFilterListAdapter mSimpleDataINFilterListAdapter;
    private StateListAdapter mStateListAdapter;

    private FavTeamListAdapter mFavTeamListAdapter;
    private FilterBy mFilterByLast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_filter_item);
        context = this;
        paidType = getIntent().getIntExtra("paidType", 0);

        initialization();
        setPresetData();
        ChangeListners();
    }

    private void initialization() {

        mGenderLay = (LinearLayout) findViewById(R.id.gender_lay);
        mPositionLay = (LinearLayout) findViewById(R.id.position_lay);
        mTypeofUserLay = (LinearLayout) findViewById(R.id.type_of_user_lay);

        lay_container = (RelativeLayout) findViewById(R.id.lay_container);
        mLoadingRL = (RelativeLayout) findViewById(R.id.loading_rl);
        mChoieSportLV = (ListView) findViewById(R.id.choices_sport_lv);
        mChoiceStateLV = (ListView) findViewById(R.id.choices_state_lv);
        mPostionEnterLV = (ListView) findViewById(R.id.position_entered_lv);
        mFavoriteTeamLV = (ListView) findViewById(R.id.favorite_team_lv);


        mAllUserCTV = (CheckedTextView) findViewById(R.id.all_user_type__ctv);
        mAtheleteCTV = (CheckedTextView) findViewById(R.id.athlete_ctv);
        mCoachCTV = (CheckedTextView) findViewById(R.id.coach_ctv);
        mFanCTV = (CheckedTextView) findViewById(R.id.fan_ctv);

        mGpaLL = (LinearLayout) findViewById(R.id.gpa_ll);
        mSatLL = (LinearLayout) findViewById(R.id.sat_ll);
        mActLL = (LinearLayout) findViewById(R.id.act_ll);
        mGpaRG = (RadioGroup) findViewById(R.id.gpa_rg);
        mSatRG = (RadioGroup) findViewById(R.id.sat_rg);
        mActRG = (RadioGroup) findViewById(R.id.act_rg);
        mGenderRG = (RadioGroup) findViewById(R.id.gender_rg);

        mFilterBy = getIntent().getParcelableExtra("filterBy");
        mFilterByLast = getIntent().getParcelableExtra("filterBy");
        position = getIntent().getIntExtra("position", 0);

        initHeader();

        mSportsListAdapter = new SportsListAdapter(this);
        mStateListAdapter = new StateListAdapter(this);
        mFavTeamListAdapter = new FavTeamListAdapter(this);
        mPostionEnterLV.setAdapter(mSimpleDataINFilterListAdapter);
        mChoieSportLV.setAdapter(mSportsListAdapter);

        mChoiceStateLV.setAdapter(mStateListAdapter);
        mFavoriteTeamLV.setAdapter(mFavTeamListAdapter);
        switch (position) {
            case 0:
                switchHideShowView(mChoieSportLV);
                getSportsRequest();
                break;
            case 1:
                switchHideShowView(mChoiceStateLV);
//                if (null == mStatesList) {
                getStateRequest();
//                }
                break;
            case 2:
                switchHideShowView(mGenderLay);
                break;
            case 3:
                switchHideShowView(mTypeofUserLay);
                break;
            case 4:
                if (paidType == 0) {
                    getFavoriteTeamRequest();
                    switchHideShowView(mFavoriteTeamLV);
                } else {
                    switchHideShowView(mGpaLL);
                }
                break;
            case 5:
                switchHideShowView(mSatLL);
                break;
            case 6:
//                switchHideShowView(mRecructingClassLV);
                break;
            case 7:
                /*switchHideShowView(mVerifiedLV);*/
                break;
            default:
                break;
        }


        mSimpleDataINFilterListAdapter = new SimpleDataINFilterListAdapter(this,
                new SimpleDataINFilterListAdapter.OnclearListner() {

                    @Override
                    public void onclear(int posistion) {

                    }
                });


        mChoieSportLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mSportsListAdapter.getItem(position).isSelected = !mSportsListAdapter.getItem(position).isSelected;
                mSportsListAdapter.notifyDataSetChanged();
                mFilterBy.setSports(mSportsListAdapter.getSelections());
            }
        });

        mChoiceStateLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mStateListAdapter.getItem(position).isSelected) {
                    mStatesList.get(position).isSelected = false;
                } else {
                    mStatesList.get(position).isSelected = true;
                }
                mStateListAdapter.notifyDataSetChanged();
                mFilterBy.setStates(mStateListAdapter.getSelections());
            }
        });

        mFavoriteTeamLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mFavTeamListAdapter.getItem(position).isSelected) {
                    mTeamModel.get(position).isSelected = false;
                } else {
                    mTeamModel.get(position).isSelected = true;
                }
                mFavTeamListAdapter.notifyDataSetChanged();
                mFilterBy.setFavTeam(mFavTeamListAdapter.getSelections());
            }
        });
//        mRecructingClassLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//            }
//        });
        /*mVerifiedLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });*/


        setProfileType();
    }

    private void setProfileType() {

        mAllUserCTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAllUserCTV.setChecked(true);
                mAtheleteCTV.setChecked(false);
                mFanCTV.setChecked(false);
                mCoachCTV.setChecked(false);
                mFilterByLast.setUserTypeId("");
            }
        });

        mAtheleteCTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAllUserCTV.setChecked(false);
                mAtheleteCTV.setChecked(true);
                mFanCTV.setChecked(false);
                mCoachCTV.setChecked(false);
                mFilterByLast.setUserTypeId("2");
            }
        });

        mCoachCTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAllUserCTV.setChecked(false);
                mAtheleteCTV.setChecked(false);
                mCoachCTV.setChecked(true);
                mFanCTV.setChecked(false);
                mFilterByLast.setUserTypeId("1");
            }
        });

        mFanCTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAllUserCTV.setChecked(false);
                mAtheleteCTV.setChecked(false);
                mFanCTV.setChecked(true);
                mCoachCTV.setChecked(false);
                mFilterByLast.setUserTypeId("3");
            }
        });
    }




    private void initHeader() {

        setHeader(R.drawable.ic_back_header, R.drawable.ic_check_header,
                getResources().getString(R.string.select_filter),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent in3 = new Intent();
                        in3.putExtra("filterBy", mFilterByLast);
                        setResult(RESULT_OK, in3);
                        finish();
                    }
                });
    }

    public ArrayList<StateModel> mStatesList;

    private void setPresetData() {
        /*
         * Set Gender last saved
		 */
        if (mFilterBy.getGender().equals("")) {
            mGenderRG.check(R.id.gender_al_rb);
        } else if (mFilterBy.getGender().equals(FilterBy.GENDER_MALE)) {
            mGenderRG.check(R.id.gender_male_rb);
        } else if (mFilterBy.getGender().equals(FilterBy.GENDER_FEMALE)) {
            mGenderRG.check(R.id.gender_female_rb);
        }
		/*
		 * Set USER Type last saved
		 */
        if (mFilterBy.getUserTypeId().equals(FilterBy.USERTYPEID_ALL)) {
            mAllUserCTV.setChecked(true);
            mAtheleteCTV.setChecked(false);
            mFanCTV.setChecked(false);
            mCoachCTV.setChecked(false);
        } else if (mFilterBy.getUserTypeId().equals(FilterBy.USERTYPEID_FAN)) {
            mAllUserCTV.setChecked(false);
            mAtheleteCTV.setChecked(false);
            mFanCTV.setChecked(true);
            mCoachCTV.setChecked(false);
        } else if (mFilterBy.getUserTypeId().equals(FilterBy.USERTYPEID_ATHELETE)) {
            mAllUserCTV.setChecked(false);
            mAtheleteCTV.setChecked(true);
            mFanCTV.setChecked(false);
            mCoachCTV.setChecked(false);
        } else if (mFilterBy.getUserTypeId().equals(FilterBy.USERTYPEID_COACH)) {
            mAllUserCTV.setChecked(false);
            mAtheleteCTV.setChecked(false);
            mFanCTV.setChecked(false);
            mCoachCTV.setChecked(true);
        } else {
            mAllUserCTV.setChecked(true);
            mAtheleteCTV.setChecked(false);
            mFanCTV.setChecked(false);
            mCoachCTV.setChecked(false);
        }

		/*
		 * Set GPA last saved
		 */
        if (mFilterBy.getGPA().equals("")) {
            mGpaRG.check(R.id.gpa_al_rb);
        } else if (mFilterBy.getGPA().equals(FilterBy.GPA_2_5)) {
            mGpaRG.check(R.id.gpa_more_2_5_rb);
        } else if (mFilterBy.getGPA().equals(FilterBy.GPA_3_0)) {
            mGpaRG.check(R.id.gpa_more_3_0_rb);
        } else if (mFilterBy.getGPA().equals(FilterBy.GPA_3_5)) {
            mGpaRG.check(R.id.gpa_more_3_5_rb);
        } else if (mFilterBy.getGPA().equals(FilterBy.GPA_4_0)) {
            mGpaRG.check(R.id.gpa_more_4_0_rb);
        }

		/*
		 * Set SAT last saved
		 */

        if (mFilterBy.getSATScore().equals("")) {
            mSatRG.check(R.id.sat_al_rb);
        } else if (mFilterBy.getSATScore().equals(FilterBy.SAT_1000)) {
            mSatRG.check(R.id.sat_more_1000);
        } else if (mFilterBy.getSATScore().equals(FilterBy.SAT_1500)) {
            mSatRG.check(R.id.sat_more_1500);
        } else if (mFilterBy.getSATScore().equals(FilterBy.SAT_2000)) {
            mSatRG.check(R.id.sat_more_2000);
        }

		/*
		 * Set ACT last saved
		 */

//        if (mFilterBy.getACTScore().equals("")) {
//            mActRG.check(R.id.act_al_rb);
//        } else if (mFilterBy.getACTScore().equals(FilterBy.ACT_15)) {
//            mActRG.check(R.id.act_more_15_rb);
//        } else if (mFilterBy.getACTScore().equals(FilterBy.ACT_20)) {
//            mActRG.check(R.id.act_more_20_rb);
//        } else if (mFilterBy.getACTScore().equals(FilterBy.ACT_25)) {
//            mActRG.check(R.id.act_more_25_rb);
//        } else if (mFilterBy.getACTScore().equals(FilterBy.ACT_30)) {
//            mActRG.check(R.id.act_more_30_rb);
//        }
    }


    private void getSportsRequest() {

        GetSportsRequest mRequest = new GetSportsRequest(this);
        mRequest.setLoader(mLoadingRL);
        mRequest.GetSportsServerRequest();
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mSportModels = (List<SportsModel>) data;
                    List<SportsModel> mSelections = mFilterBy.getSportsModels();
                    if (null != mSportModels && null != mSelections) {
                        for (SportsModel sportsModel : mSportModels) {
                            if (mSelections.contains(sportsModel)) {
                                sportsModel.isSelected = true;
                            }
                        }
                    }
                    mSportsListAdapter.setList(mSportModels);
                }
            }
        });

    }

    private void getFavoriteTeamRequest() {

        GetFavTeamRequest mRequest = new GetFavTeamRequest(this);
        mRequest.setLoader(mLoadingRL);
        mRequest.getFavTeamServerRequest();
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mTeamModel = (List<FavTeamModel>) data;
                    List<FavTeamModel> mSelections = mFilterBy.getmFavTeamModals();
                    if (null != mTeamModel && null != mSelections) {
                        for (FavTeamModel teammodel : mTeamModel) {
                            if (mSelections.contains(teammodel)) {
                                teammodel.isSelected = true;
                            }
                        }
                    }
                    mFavTeamListAdapter.setList(mTeamModel);
                }
            }
        });

    }

    public void ChangeListners() {
        mGenderRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                int checkIDGender = mGenderRG.getCheckedRadioButtonId();
                switch (checkIDGender) {
                    case R.id.gender_al_rb:
                        mFilterBy.setGender(FilterBy.GENDER_ALL);
                        mFilterByLast.setGender("");
                        break;
                    case R.id.gender_male_rb:
                        mFilterBy.setGender(FilterBy.GENDER_MALE);
                        mFilterByLast.setGender("1");
                        break;
                    case R.id.gender_female_rb:
                        mFilterBy.setGender(FilterBy.GENDER_FEMALE);
                        mFilterByLast.setGender("2");
                        break;
                    default:
                        mFilterBy.setGender(FilterBy.GENDER_ALL);
                        mFilterByLast.setGender("");
                        break;

                }
//                mFilterByCategoryAdapter.setFilteragain(mFilterBy);
            }
        });

        mGpaRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                int checkIDGender = mGpaRG.getCheckedRadioButtonId();
                switch (checkIDGender) {
                    case R.id.gpa_al_rb:
                        mFilterBy.setGPA(FilterBy.GPA_ALL);
                        mFilterByLast.setGPA("");
                        break;
                    case R.id.gpa_more_2_5_rb:
                        mFilterBy.setGPA(FilterBy.GPA_2_5);
                        mFilterByLast.setGPA("2.5");
                        break;
                    case R.id.gpa_more_3_0_rb:
                        mFilterBy.setGPA(FilterBy.GPA_3_0);
                        mFilterByLast.setGPA("3.0");
                        break;

                    case R.id.gpa_more_3_5_rb:
                        mFilterBy.setGPA(FilterBy.GPA_3_5);
                        mFilterByLast.setGPA("3.5");
                        break;
                    case R.id.gpa_more_4_0_rb:
                        mFilterBy.setGPA(FilterBy.GPA_4_0);
                        mFilterByLast.setGPA("4.0");
                        break;
                    default:
                        mFilterBy.setGPA(FilterBy.GPA_ALL);
                        mFilterByLast.setGPA("");
                        break;
                }
//                mFilterByCategoryAdapter.setFilteragain(mFilterBy);
            }
        });

        mSatRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                int checkIDGender = mSatRG.getCheckedRadioButtonId();
                switch (checkIDGender) {
                    case R.id.sat_al_rb:
                        mFilterBy.setSATScore(FilterBy.SAT_ALL);
                        mFilterByLast.setSATScore("");
                        break;
                    case R.id.sat_more_1000:
                        mFilterBy.setSATScore(FilterBy.SAT_1000);
                        mFilterByLast.setSATScore("1000");
                        break;
                    case R.id.sat_more_1500:
                        mFilterBy.setSATScore(FilterBy.SAT_1500);
                        mFilterByLast.setSATScore("15000");
                        break;

                    case R.id.sat_more_2000:
                        mFilterBy.setSATScore(FilterBy.SAT_2000);
                        mFilterByLast.setSATScore("2000");
                        break;
                    default:
                        mFilterBy.setSATScore(FilterBy.SAT_ALL);
                        mFilterByLast.setSATScore("");
                        break;
                }
//                mFilterByCategoryAdapter.setFilteragain(mFilterBy);
            }
        });

//        mActRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            public void onCheckedChanged(RadioGroup arg0, int id) {
//                int checkIDGender = mActRG.getCheckedRadioButtonId();
//                switch (checkIDGender) {
//                    case R.id.act_al_rb:
//                        mFilterBy.setACTScore(FilterBy.ACT_ALL);
//                        break;
//                    case R.id.act_more_15_rb:
//                        mFilterBy.setACTScore(FilterBy.ACT_15);
//                        break;
//                    case R.id.act_more_20_rb:
//                        mFilterBy.setACTScore(FilterBy.ACT_20);
//                        break;
//
//                    case R.id.act_more_25_rb:
//                        mFilterBy.setACTScore(FilterBy.ACT_25);
//                        break;
//
//                    case R.id.act_more_30_rb:
//                        mFilterBy.setACTScore(FilterBy.ACT_30);
//                        break;
//
//                    default:
//                        mFilterBy.setACTScore(FilterBy.ACT_30);
//                        break;
//                }
////                mFilterByCategoryAdapter.setFilteragain(mFilterBy);
//            }
//        });

    }

    public static Intent getIntent(Context context, FilterBy mFilterBy, int position, int paidtype) {
        paidType = paidtype;
        Intent intent = new Intent(context, SelectFilterItemActivity.class);
        intent.putExtra("filterBy", mFilterBy);
        intent.putExtra("position", position);
        intent.putExtra("paidType", paidType);
        return intent;
    }

    public void switchHideShowView(View ids) {

        Utility.LogP("","position  " + lay_container.getChildCount() + "  " + ids);
        for (int i = 0; i < lay_container.getChildCount(); i++) {

            View vchild = lay_container.getChildAt(i);
            if (ids.getId() == vchild.getId()) {
                vchild.setVisibility(View.VISIBLE);
            } else {
                vchild.setVisibility(View.GONE);
            }
        }
    }

    private void getStateRequest() {
        GetStatesRequest mRequest = new GetStatesRequest(this);
        mRequest.GetStatesServerRequest();
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mStatesList = (ArrayList<StateModel>) data;

                    List<StateModel> mSelections = mFilterBy.getStateModels();

                    if (null != mStatesList && null != mSelections) {
                        for (StateModel stateModel : mStatesList) {
                            if (mSelections.contains(stateModel)) {
                                stateModel.isSelected = true;
                            }
                        }
                    }
                    mStateListAdapter.setList(mStatesList);

                } else {

                }
            }
        });
    }
}
