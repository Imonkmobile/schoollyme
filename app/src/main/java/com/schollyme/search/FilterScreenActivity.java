package com.schollyme.search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.R;
import com.schollyme.activity.BaseActivity;
import com.schollyme.adapter.FilterByCategoryAdapter;
import com.schollyme.adapter.SimpleDataINFilterListAdapter;
import com.schollyme.adapter.SimpleDataINFilterListAdapter.OnclearListner;
import com.schollyme.adapter.SportsListAdapter;
import com.schollyme.adapter.StateListAdapter;
import com.schollyme.model.FilterBy;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SportsModel;
import com.schollyme.model.StateModel;
import com.vinfotech.request.GetSportsRequest;
import com.vinfotech.request.GetStatesRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ResourceAsColor")
public class FilterScreenActivity extends BaseActivity implements OnClickListener, RadioGroup.OnCheckedChangeListener {

	private VHolder mVHolder;

	private String selectCategoryforNonCoach[] = new String[4];
	private String selectCategoryforPaidCoach[] = new String[7];

	private FilterByCategoryAdapter mFilterByCategoryAdapter;
	private FilterBy mFilterBy;
	private List<SportsModel> mSportModels;
	private SportsListAdapter mSportsListAdapter, mFavTeamListAdapter;

	// Rest
	private SimpleDataINFilterListAdapter mSimpleDataINFilterListAdapter;
	private StateListAdapter mStateListAdapter;
	private LogedInUserModel mLogedInUserModel;
	private FilterBy mFilterByLast;
	private int paidType = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_screen_sctivity);

		mFilterBy = getIntent().getParcelableExtra("filterBy");
		mFilterByLast = getIntent().getParcelableExtra("filterBy");
		if (null == mFilterBy) {
			Toast.makeText(getApplicationContext(), "Invalid Filter", Toast.LENGTH_SHORT).show();
			this.finish();
			return;
		}

		selectCategoryforNonCoach = new String[]{
				getResources().getString(R.string.team_sport),
				getResources().getString(R.string.state_label),
				getResources().getString(R.string.gender_label),
				getResources().getString(R.string.profile_type),
				getResources().getString(R.string.fav_team_label)};


		selectCategoryforPaidCoach = new String[]{
				getResources().getString(R.string.team_sport),
				getResources().getString(R.string.state_label),
				getResources().getString(R.string.gender_label),
				getResources().getString(R.string.profile_type),
				getResources().getString(R.string.gpa_hint_label),
				getResources().getString(R.string.sat_label),
				getResources().getString(R.string.recruiting_class),
				getResources().getString(R.string.verified)};

		initialization();
		// setcolorLollipop();
		switchHideShowView(mVHolder.mChoieSportLV);
		getSportsRequest();
		setPresetData();
		ChangeListners();
	}

	public ArrayList<StateModel> mStatesList;

	private void setPresetData() {
		/*
		 * Set Gender last saved
		 */
		if (mFilterBy.getGender().equals("")) {
			mVHolder.mGenderRG.check(R.id.gender_al_rb);
		} else if (mFilterBy.getGender().equals(FilterBy.GENDER_MALE)) {
			mVHolder.mGenderRG.check(R.id.gender_male_rb);
		} else if (mFilterBy.getGender().equals(FilterBy.GENDER_FEMALE)) {
			mVHolder.mGenderRG.check(R.id.gender_female_rb);
		}
		/*
		 * Set USER Type last saved
		 */
		if (mFilterBy.getUserTypeId().equals(FilterBy.USERTYPEID_ALL)) {
			mVHolder.mAllUserCTV.setChecked(true);
			mVHolder.mAtheleteCTV.setChecked(false);
			mVHolder.mFanCTV.setChecked(false);
			mVHolder.mCoachCTV.setChecked(false);
		} else if (mFilterBy.getUserTypeId().equals(FilterBy.USERTYPEID_FAN)) {
			mVHolder.mAllUserCTV.setChecked(false);
			mVHolder.mAtheleteCTV.setChecked(false);
			mVHolder.mFanCTV.setChecked(true);
			mVHolder.mCoachCTV.setChecked(false);
		} else if (mFilterBy.getUserTypeId().equals(FilterBy.USERTYPEID_ATHELETE)) {
			mVHolder.mAllUserCTV.setChecked(false);
			mVHolder.mAtheleteCTV.setChecked(true);
			mVHolder.mFanCTV.setChecked(false);
			mVHolder.mCoachCTV.setChecked(false);
		} else if (mFilterBy.getUserTypeId().equals(FilterBy.USERTYPEID_COACH)) {
			mVHolder.mAllUserCTV.setChecked(false);
			mVHolder.mAtheleteCTV.setChecked(false);
			mVHolder.mFanCTV.setChecked(false);
			mVHolder.mCoachCTV.setChecked(true);
		} else {
			mVHolder.mAllUserCTV.setChecked(true);
			mVHolder.mAtheleteCTV.setChecked(false);
			mVHolder.mFanCTV.setChecked(false);
			mVHolder.mCoachCTV.setChecked(false);
		}

		/*
		 * Set GPA last saved
		 */
		if (mFilterBy.getGPA().equals("")) {
			mVHolder.mGpaRG.check(R.id.gpa_al_rb);
		} else if (mFilterBy.getGPA().equals(FilterBy.GPA_2_5)) {
			mVHolder.mGpaRG.check(R.id.gpa_more_2_5_rb);
		} else if (mFilterBy.getGPA().equals(FilterBy.GPA_3_0)) {
			mVHolder.mGpaRG.check(R.id.gpa_more_3_0_rb);
		} else if (mFilterBy.getGPA().equals(FilterBy.GPA_3_5)) {
			mVHolder.mGpaRG.check(R.id.gpa_more_3_5_rb);
		} else if (mFilterBy.getGPA().equals(FilterBy.GPA_4_0)) {
			mVHolder.mGpaRG.check(R.id.gpa_more_4_0_rb);
		}

		/*
		 * Set SAT last saved
		 */

		if (mFilterBy.getSATScore().equals("")) {
			mVHolder.mSatRG.check(R.id.sat_al_rb);
		} else if (mFilterBy.getSATScore().equals(FilterBy.SAT_1000)) {
			mVHolder.mSatRG.check(R.id.sat_more_1000);
		} else if (mFilterBy.getSATScore().equals(FilterBy.SAT_1500)) {
			mVHolder.mSatRG.check(R.id.sat_more_1500);
		} else if (mFilterBy.getSATScore().equals(FilterBy.SAT_2000)) {
			mVHolder.mSatRG.check(R.id.sat_more_2000);
		}

		/*
		 * Set ACT last saved
		 */

//		if (mFilterBy.getACTScore().equals("")) {
//			mVHolder.mActRG.check(R.id.act_al_rb);
//		} else if (mFilterBy.getACTScore().equals(FilterBy.ACT_15)) {
//			mVHolder.mActRG.check(R.id.act_more_15_rb);
//		} else if (mFilterBy.getACTScore().equals(FilterBy.ACT_20)) {
//			mVHolder.mActRG.check(R.id.act_more_20_rb);
//		} else if (mFilterBy.getACTScore().equals(FilterBy.ACT_25)) {
//			mVHolder.mActRG.check(R.id.act_more_25_rb);
//		} else if (mFilterBy.getACTScore().equals(FilterBy.ACT_30)) {
//			mVHolder.mActRG.check(R.id.act_more_30_rb);
//		}
	}

	public void ChangeListners() {
		mVHolder.mGenderRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup arg0, int id) {
				int checkIDGender = mVHolder.mGenderRG.getCheckedRadioButtonId();
				switch (checkIDGender) {
				case R.id.gender_al_rb:
					mFilterBy.setGender(FilterBy.GENDER_ALL);
					break;
				case R.id.gender_male_rb:
					mFilterBy.setGender(FilterBy.GENDER_MALE);
					break;
				case R.id.gender_female_rb:
					mFilterBy.setGender(FilterBy.GENDER_FEMALE);
					break;
				default:
					mFilterBy.setGender(FilterBy.GENDER_ALL);
					break;

				}
				mFilterByCategoryAdapter.setFilteragain(mFilterBy);
			}
		});

		mVHolder.mGpaRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup arg0, int id) {
				int checkIDGender = mVHolder.mGpaRG.getCheckedRadioButtonId();
				switch (checkIDGender) {
				case R.id.gpa_al_rb:
					mFilterBy.setGPA(FilterBy.GPA_ALL);
					;
					break;
				case R.id.gpa_more_2_5_rb:
					mFilterBy.setGPA(FilterBy.GPA_2_5);
					;
					break;
				case R.id.gpa_more_3_0_rb:
					mFilterBy.setGPA(FilterBy.GPA_3_0);
					;
					break;

				case R.id.gpa_more_3_5_rb:
					mFilterBy.setGPA(FilterBy.GPA_3_5);
					;
					break;
				case R.id.gpa_more_4_0_rb:
					mFilterBy.setGPA(FilterBy.GPA_4_0);
					;
					break;
				default:
					mFilterBy.setGPA(FilterBy.GPA_ALL);
					break;
				}
				mFilterByCategoryAdapter.setFilteragain(mFilterBy);
			}
		});

		mVHolder.mSatRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup arg0, int id) {
				int checkIDGender = mVHolder.mSatRG.getCheckedRadioButtonId();
				switch (checkIDGender) {
				case R.id.sat_al_rb:
					mFilterBy.setSATScore(FilterBy.SAT_ALL);
					break;
				case R.id.sat_more_1000:
					mFilterBy.setSATScore(FilterBy.SAT_1000);
					break;
				case R.id.sat_more_1500:
					mFilterBy.setSATScore(FilterBy.SAT_1500);
					break;
				case R.id.sat_more_2000:
					mFilterBy.setSATScore(FilterBy.SAT_2000);
					break;

				default:
					mFilterBy.setSATScore(FilterBy.SAT_ALL);
					break;
				}
				mFilterByCategoryAdapter.setFilteragain(mFilterBy);
			}
		});

//		mVHolder.mActRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//			public void onCheckedChanged(RadioGroup arg0, int id) {
//				int checkIDGender = mVHolder.mActRG.getCheckedRadioButtonId();
//				switch (checkIDGender) {
//				case R.id.act_al_rb:
//					mFilterBy.setACTScore(FilterBy.ACT_ALL);
//					break;
//				case R.id.act_more_15_rb:
//					mFilterBy.setACTScore(FilterBy.ACT_15);
//					break;
//				case R.id.act_more_20_rb:
//					mFilterBy.setACTScore(FilterBy.ACT_20);
//					break;
//
//				case R.id.act_more_25_rb:
//					mFilterBy.setACTScore(FilterBy.ACT_25);
//					break;
//
//				case R.id.act_more_30_rb:
//					mFilterBy.setACTScore(FilterBy.ACT_30);
//					break;
//
//				default:
//					mFilterBy.setACTScore(FilterBy.ACT_30);
//					break;
//				}
//				mFilterByCategoryAdapter.setFilteragain(mFilterBy);
//			}
//		});

	}

	public void clearStateAndSport() {

		if (null != mStatesList) {
			for (StateModel stateModel : mStatesList) {
				stateModel.isSelected = false;
			}
			mStateListAdapter.setList(mStatesList);
		}

		if (null != mSportModels) {
			for (SportsModel sportsModel : mSportModels) {
				sportsModel.isSelected = false;
			}
			mSportsListAdapter.setList(mSportModels);
		}

	};

	private void getStateRequest() {

		GetStatesRequest mRequest = new GetStatesRequest(this);
		mRequest.GetStatesServerRequest();
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mStatesList = (ArrayList<StateModel>) data;

					List<StateModel> mSelections = mFilterBy.getStateModels();

					if (null != mStatesList && null != mSelections) {
						for (StateModel stateModel : mStatesList) {
							if (mSelections.contains(stateModel)) {
								stateModel.isSelected = true;
							}
						}
					}
					mStateListAdapter.setList(mStatesList);

				} else {

				}
			}
		});
	}

	private void initialization() {

		mSportsListAdapter = new SportsListAdapter(this);
		mStateListAdapter = new StateListAdapter(this);
		mFavTeamListAdapter = new SportsListAdapter(this);
		mLogedInUserModel = new LogedInUserModel(this);

		if (mLogedInUserModel.isPaidCoach.equals("0")) {
			paidType = 0;
			mFilterByCategoryAdapter = new FilterByCategoryAdapter(FilterScreenActivity.this,
					selectCategoryforNonCoach, mFilterBy, 0);
		} else {
			paidType = 1;
			mFilterByCategoryAdapter = new FilterByCategoryAdapter(FilterScreenActivity.this,
					selectCategoryforPaidCoach, mFilterBy, 1);
		}

		mSimpleDataINFilterListAdapter = new SimpleDataINFilterListAdapter(this, new OnclearListner() {

			@Override
			public void onclear(int posistion) {

			}
		});
		mVHolder = new VHolder(findViewById(R.id.container_ll), this);

		mVHolder.mPostionEnterLV.setAdapter(mSimpleDataINFilterListAdapter);
		mVHolder.mChoieSportLV.setAdapter(mSportsListAdapter);
		mVHolder.mChoiceStateLV.setAdapter(mStateListAdapter);
		mVHolder.mFilterCAtegotyLV.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mVHolder.mFilterCAtegotyLV.setAdapter(mFilterByCategoryAdapter);
		mVHolder.mFilterCAtegotyLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mVHolder.mFilterCAtegotyLV.setItemChecked(position, true);
				switch (position) {
				case 0:
					switchHideShowView(mVHolder.mChoieSportLV);
					break;
				case 1:
					switchHideShowView(mVHolder.mChoiceStateLV);
					if (null == mStatesList) {
						getStateRequest();
					}
					break;
				case 2:
					switchHideShowView(mVHolder.mGenderLay);
					break;
				case 3:
					switchHideShowView(mVHolder.mTypeofUserLay);
					break;
				case 4:
					if (paidType == 0) {
						switchHideShowView(mVHolder.mChoieFavTeamLV);
					} else {
						switchHideShowView(mVHolder.mGpaLL);
					}
					break;

				case 5:
					switchHideShowView(mVHolder.mSatLL);
					break;
				case 6:
					switchHideShowView(mVHolder.mActLL);
					break;

				default:
					break;
				}
			}
		});

		mVHolder.mChoieSportLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mSportsListAdapter.getItem(position).isSelected = !mSportsListAdapter.getItem(position).isSelected;
				mSportsListAdapter.notifyDataSetChanged();
				mFilterBy.setSports(mSportsListAdapter.getSelections());
				mFilterByCategoryAdapter.setFilteragain(mFilterBy);
			}
		});

		mVHolder.mChoieFavTeamLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mSportsListAdapter.getItem(position).isSelected = !mSportsListAdapter.getItem(position).isSelected;
				mSportsListAdapter.notifyDataSetChanged();
				mFilterBy.setSports(mSportsListAdapter.getSelections());
				mFilterByCategoryAdapter.setFilteragain(mFilterBy);
			}
		});


		mVHolder.mChoiceStateLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (mStateListAdapter.getItem(position).isSelected) {

					mStatesList.get(position).isSelected = false;
				} else {
					mStatesList.get(position).isSelected = true;
				}
				mStateListAdapter.notifyDataSetChanged();
				mFilterBy.setStates(mStateListAdapter.getSelections());
				mFilterByCategoryAdapter.setFilteragain(mFilterBy);
			}
		});

		// ;
		mVHolder.mFilterCAtegotyLV.setItemChecked(0, true);
	}

	class VHolder {

		private ListView mFilterCAtegotyLV;
		private ImageButton mClose;
		private LinearLayout mGenderLay, mPositionLay, mTypeofUserLay, mGpaLL, mSatLL, mActLL;
		private ListView mChoieSportLV, mChoiceStateLV, mPostionEnterLV, mChoieFavTeamLV;
		private RelativeLayout lay_container, mLoadingRL;
		private TextView mClearAll, mApplyAll;

		private EditText mPostionET;
		ImageButton mAddPostionIB;
		// private CheckBox mGenderMale, mGenderFemale, mGenderAll;

		private CheckedTextView mAtheleteCTV, mFanCTV, mCoachCTV, mAllUserCTV;
		private RadioGroup mGpaRG, mSatRG, mActRG, mGenderRG;;

		// gender_rg
		// gpa_llsat_ll
		//
		public VHolder(View view, OnClickListener listener) {
			mClose = (ImageButton) view.findViewById(R.id.close_ib);
			mGenderLay = (LinearLayout) view.findViewById(R.id.gender_lay);
			mPositionLay = (LinearLayout) view.findViewById(R.id.position_lay);
			mTypeofUserLay = (LinearLayout) view.findViewById(R.id.type_of_user_lay);

			lay_container = (RelativeLayout) view.findViewById(R.id.lay_container);
			mLoadingRL = (RelativeLayout) view.findViewById(R.id.loading_rl);
			mFilterCAtegotyLV = (ListView) view.findViewById(R.id.filter_categoty_lv);
			mChoieSportLV = (ListView) view.findViewById(R.id.choices_sport_lv);
			mChoieFavTeamLV = (ListView) view.findViewById(R.id.favorite_team_lv);
			mChoiceStateLV = (ListView) view.findViewById(R.id.choices_state_lv);
			mPostionEnterLV = (ListView) view.findViewById(R.id.position_entered_lv);
			mClearAll = (TextView) view.findViewById(R.id.clear_all_tv);
			mApplyAll = (TextView) view.findViewById(R.id.apply_tv);
			mPostionET = (EditText) view.findViewById(R.id.pasition_et);
			mAddPostionIB = (ImageButton) view.findViewById(R.id.position_add_ib);
			// mGenderMale = (CheckBox) view.findViewById(R.id.gender_male_cb);
			// mGenderFemale = (CheckBox)
			// view.findViewById(R.id.gender_female_cb);
			// mGenderAll = (CheckBox) view.findViewById(R.id.gender_both_cb);
			mAtheleteCTV = (CheckedTextView) view.findViewById(R.id.athlete_ctv);
			mFanCTV = (CheckedTextView) view.findViewById(R.id.fan_ctv);
			mCoachCTV = (CheckedTextView) view.findViewById(R.id.coach_ctv);
			mAllUserCTV = (CheckedTextView) view.findViewById(R.id.all_user_type__ctv);

			mGpaLL = (LinearLayout) view.findViewById(R.id.gpa_ll);
			mSatLL = (LinearLayout) view.findViewById(R.id.sat_ll);
			mActLL = (LinearLayout) view.findViewById(R.id.act_ll);
			mGpaRG = (RadioGroup) view.findViewById(R.id.gpa_rg);
			mSatRG = (RadioGroup) view.findViewById(R.id.sat_rg);
			mActRG = (RadioGroup) view.findViewById(R.id.act_rg);
			mGenderRG = (RadioGroup) view.findViewById(R.id.gender_rg);
			mAddPostionIB.setOnClickListener(listener);
			mApplyAll.setOnClickListener(listener);
			mClearAll.setOnClickListener(listener);
			mClose.setOnClickListener(listener);
			mAtheleteCTV.setOnClickListener(listener);
			mFanCTV.setOnClickListener(listener);
			mCoachCTV.setOnClickListener(listener);
			mAllUserCTV.setOnClickListener(listener);
			mActRG.setOnCheckedChangeListener(FilterScreenActivity.this);
			FontLoader.setRobotoMediumTypeface(view.findViewById(R.id.tittle_header_tv), mClearAll, mApplyAll);

			FontLoader.SetFontToWholeView(FilterScreenActivity.this, lay_container, FontLoader.getRobotoRegular(FilterScreenActivity.this));
		}
	}

	public void switchHideShowView(View ids) {

		for (int i = 0; i < mVHolder.lay_container.getChildCount(); i++) {

			View vchild = mVHolder.lay_container.getChildAt(i);
			if (ids.getId() == vchild.getId()) {
				vchild.setVisibility(View.VISIBLE);
			} else {
				vchild.setVisibility(View.GONE);
			}
		}
	}

	public static Intent getIntent(Context context, FilterBy filterBy) {
		Intent intent = new Intent(context, FilterScreenActivity.class);
		intent.putExtra("filterBy", filterBy);
		return intent;
	}

	public Boolean isListSelected(int bb) {
		return mVHolder.mFilterCAtegotyLV.isItemChecked(bb);
	}

	private void getSportsRequest() {
		GetSportsRequest mRequest = new GetSportsRequest(this);
		mRequest.setLoader(mVHolder.mLoadingRL);
		mRequest.GetSportsServerRequest();
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mSportModels = (List<SportsModel>) data;
					List<SportsModel> mSelections = mFilterBy.getSportsModels();
					if (null != mSportModels && null != mSelections) {
						for (SportsModel sportsModel : mSportModels) {
							if (mSelections.contains(sportsModel)) {
								sportsModel.isSelected = true;
							}
						}
					}
					mSportsListAdapter.setList(mSportModels);
				}
			}
		});

	}

	@Override
	public void onBackPressed() {
		this.finish();
		this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.athlete_ctv:
		case R.id.coach_ctv:
		case R.id.fan_ctv:
		case R.id.all_user_type__ctv:

			if (R.id.all_user_type__ctv == v.getId()) {

				mVHolder.mAllUserCTV.setChecked(true);
				mVHolder.mAtheleteCTV.setChecked(false);
				mVHolder.mFanCTV.setChecked(false);
				mVHolder.mCoachCTV.setChecked(false);
				mFilterBy.setUserTypeId(FilterBy.USERTYPEID_ALL);
			} else if (R.id.athlete_ctv == v.getId()) {

				mVHolder.mAllUserCTV.setChecked(false);
				mVHolder.mAtheleteCTV.setChecked(true);
				mVHolder.mFanCTV.setChecked(false);
				mVHolder.mCoachCTV.setChecked(false);
				mFilterBy.setUserTypeId(FilterBy.USERTYPEID_ATHELETE);
			} else if (R.id.coach_ctv == v.getId()) {

				mVHolder.mAllUserCTV.setChecked(false);
				mVHolder.mAtheleteCTV.setChecked(false);
				mVHolder.mFanCTV.setChecked(false);
				mVHolder.mCoachCTV.setChecked(true);
				mFilterBy.setUserTypeId(FilterBy.USERTYPEID_COACH);
			} else if (R.id.fan_ctv == v.getId()) {

				mVHolder.mAllUserCTV.setChecked(false);
				mVHolder.mAtheleteCTV.setChecked(false);
				mVHolder.mFanCTV.setChecked(true);
				mVHolder.mCoachCTV.setChecked(false);
				mFilterBy.setUserTypeId(FilterBy.USERTYPEID_FAN);
			}
			mFilterByCategoryAdapter.setFilteragain(mFilterBy);
			// if (R.id.all_user_type__ctv != v.getId()) {
			//
			// mVHolder.mAllUserCTV.setChecked(false);
			// if (((CheckedTextView) v).isChecked()) {
			// ((CheckedTextView) v).setChecked(false);
			// } else {
			// ((CheckedTextView) v).setChecked(true);
			// }
			// } else {
			// if (((CheckedTextView) v).isChecked()) {
			// ((CheckedTextView) v).setChecked(false);
			// } else {
			// ((CheckedTextView) v).setChecked(true);
			// }
			// mVHolder.mAtheleteCTV.setChecked(false);
			// mVHolder.mFanCTV.setChecked(false);
			// mVHolder.mCoachCTV.setChecked(false);
			//
			// }
			// if (mVHolder.mAtheleteCTV.isChecked() &&
			// mVHolder.mFanCTV.isChecked() && mVHolder.mCoachCTV.isChecked()) {
			// mVHolder.mAllUserCTV.setChecked(true);
			// mVHolder.mAtheleteCTV.setChecked(false);
			// mVHolder.mFanCTV.setChecked(false);
			// mVHolder.mCoachCTV.setChecked(false);
			// }
			// if (!mVHolder.mAtheleteCTV.isChecked() &&
			// !mVHolder.mFanCTV.isChecked() && !mVHolder.mCoachCTV.isChecked())
			// {
			// mVHolder.mAllUserCTV.setChecked(true);
			// mVHolder.mAtheleteCTV.setChecked(false);
			// mVHolder.mFanCTV.setChecked(false);
			// mVHolder.mCoachCTV.setChecked(false);
			// }

			break;

		case R.id.close_ib:
			// Intent in2 = new Intent();
			// in2.putExtra("filterBy", mFilterBy);
			setResult(RESULT_CANCELED);
			this.finish();
			this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
			break;

		case R.id.apply_tv:

			applyNewValuesInfilter();

			break;
		case R.id.clear_all_tv:

			mFilterBy.clearAll();
			clearStateAndSport();
			setPresetData();
			getSportsRequest();
			Intent in3 = new Intent();
			in3.putExtra("filterBy", mFilterByLast);
			setResult(RESULT_OK, in3);
			mFilterByCategoryAdapter.setFilteragain(mFilterBy);

			break;

		case R.id.position_add_ib: {

			if (!mVHolder.mPostionET.getText().toString().trim().equals("")) {

				// mFilterBy.setPositionValue(mVHolder.mPostionET.getText().toString().trim());
				// mVHolder.mPostionET.setText("");
				// setPresetData();
			//	Toast.makeText(getApplicationContext(), "ADD TO LIST", 0).show();
			}
		}

			break;

		default:
			break;
		}

	}

	private void applyNewValuesInfilter() {
		/*
		 * Set Gender
		 */
		// int checkIDGender = mVHolder.mGenderRG.getCheckedRadioButtonId();
		// switch (checkIDGender) {
		// case R.id.gender_al_rb:
		// mFilterBy.setGender(FilterBy.GENDER_ALL);
		// break;
		// case R.id.gender_male_rb:
		// mFilterBy.setGender(FilterBy.GENDER_MALE);
		// break;
		// case R.id.gender_female_rb:
		// mFilterBy.setGender(FilterBy.GENDER_FEMALE);
		// break;
		// default:
		// mFilterBy.setGender(FilterBy.GENDER_ALL);
		// break;
		// }
		//
		// /*
		// * Set UserType
		// */
		// if (mVHolder.mAllUserCTV.isChecked()) {
		// mFilterBy.setUserTypeId(FilterBy.USERTYPEID_ALL);
		// } else if (mVHolder.mFanCTV.isChecked()) {
		// mFilterBy.setUserTypeId(FilterBy.USERTYPEID_FAN);
		// } else if (mVHolder.mCoachCTV.isChecked()) {
		// mFilterBy.setUserTypeId(FilterBy.USERTYPEID_COACH);
		// } else if (mVHolder.mAtheleteCTV.isChecked()) {
		// mFilterBy.setUserTypeId(FilterBy.USERTYPEID_ATHELETE);
		// } else {
		// mFilterBy.setUserTypeId(FilterBy.USERTYPEID_ALL);
		// }
		//
		// /*
		// * Set GPA Value
		// */
		// int checkIDGPA = mVHolder.mGpaRG.getCheckedRadioButtonId();
		// switch (checkIDGPA) {
		// case R.id.gpa_al_rb:
		// mFilterBy.setGPA(FilterBy.GPA_ALL);
		// break;
		// case R.id.gpa_more_2_5_rb:
		// mFilterBy.setGPA(FilterBy.GPA_2_5);
		// break;
		// case R.id.gpa_more_3_0_rb:
		// mFilterBy.setGPA(FilterBy.GPA_3_0);
		// break;
		// case R.id.gpa_more_3_5_rb:
		// mFilterBy.setGPA(FilterBy.GPA_3_5);
		// break;
		// case R.id.gpa_more_4_0_rb:
		// mFilterBy.setGPA(FilterBy.GPA_4_0);
		// break;
		// default:
		// mFilterBy.setGPA(FilterBy.GPA_ALL);
		// break;
		// }
		//
		// /*
		// * Set SAT Value
		// */
		// int checkIDSAT = mVHolder.mSatRG.getCheckedRadioButtonId();
		// switch (checkIDSAT) {
		// case R.id.sat_al_rb:
		// mFilterBy.setSATScore(FilterBy.SAT_ALL);
		// break;
		// case R.id.sat_more_1000:
		// mFilterBy.setSATScore(FilterBy.SAT_1000);
		// break;
		// case R.id.sat_more_1500:
		// mFilterBy.setSATScore(FilterBy.SAT_1500);
		// break;
		// case R.id.sat_more_2000:
		// mFilterBy.setSATScore(FilterBy.SAT_2000);
		// break;
		//
		// default:
		// mFilterBy.setSATScore(FilterBy.SAT_ALL);
		// break;
		// }
		//
		// /*
		// * Set SAT Value
		// */
		// int checkIDACT = mVHolder.mActRG.getCheckedRadioButtonId();
		//
		// switch (checkIDACT) {
		// case R.id.act_al_rb:
		// mFilterBy.setACTScore(FilterBy.ACT_ALL);
		// break;
		// case R.id.act_more_15_rb:
		// mFilterBy.setACTScore(FilterBy.ACT_15);
		// break;
		// case R.id.act_more_20_rb:
		// mFilterBy.setACTScore(FilterBy.ACT_20);
		// break;
		// case R.id.act_more_25_rb:
		// mFilterBy.setACTScore(FilterBy.ACT_25);
		// break;
		// case R.id.act_more_30_rb:
		// mFilterBy.setACTScore(FilterBy.ACT_30);
		// break;
		// default:
		// mFilterBy.setACTScore(FilterBy.ACT_ALL);
		// break;
		// }

		/*
		 * Return new Filter
		 */
		Intent in2 = new Intent();
		in2.putExtra("filterBy", mFilterBy);
		setResult(RESULT_OK, in2);

		this.finish();
		this.overridePendingTransition(R.anim.slide_down_in, R.anim.slide_down_out);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		switch (group.getId()) {
		case R.id.act_rg:

			int radioButtonID = mVHolder.mActRG.getCheckedRadioButtonId();
			// if (radioButtonID == checkedId) {
			// mVHolder.mActRG.clearCheck();
			// }

			break;

		default:
			break;
		}

	}
}
