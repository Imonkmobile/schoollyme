package com.schollyme.handler;

import java.util.concurrent.atomic.AtomicBoolean;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.schollyme.R;
import com.vinfotech.utility.FontLoader;

public class ErrorLayout {
	public static final long LENGTH_SHORT = 2500L;
	public static final long LENGTH_LONG = 5500L;

	public TextView mErrorTv;

	private Animation mMoveUpAnim, mMoveDownAnim;
	private final Handler handler = new Handler();
	private AtomicBoolean mErrorShown;

	public enum MsgType {
		Error, Success, Info, Warning
	}

	public ErrorLayout(View view) {
		mErrorTv = (TextView) view.findViewById(R.id.error_tv);
		mErrorTv.setVisibility(View.GONE);
		FontLoader.setRobotoRegularTypeface(mErrorTv);
		mMoveUpAnim = AnimationUtils.loadAnimation(view.getContext(), R.anim.move_up);
		mMoveDownAnim = AnimationUtils.loadAnimation(view.getContext(), R.anim.move_down);
		mErrorShown = new AtomicBoolean(false);
	}

	public void showError(String error, final boolean shortDuration, MsgType msgType) {
		int color = 0;
		switch (msgType) {
		case Error:
			color = mErrorTv.getResources().getColor(R.color.msg_error_color);
			break;
		case Success:
			color = mErrorTv.getResources().getColor(R.color.msg_success_color);
			break;
		case Info:
			color = mErrorTv.getResources().getColor(R.color.msg_info_color);
			break;
		case Warning:
			color = mErrorTv.getResources().getColor(R.color.msg_warning_color);
			break;

		default:
			color = mErrorTv.getResources().getColor(R.color.msg_info_color);
			break;
		}
		mErrorTv.setBackgroundColor(color);

		showError(error, shortDuration);
	}

	public void showError(String error, final boolean shortDuration) {

		mErrorTv.clearAnimation();
		if (TextUtils.isEmpty(error)) {
			mErrorTv.setVisibility(View.GONE);
		} else if (!mErrorShown.get()) {
			mErrorShown.set(true);
			mErrorTv.setVisibility(View.VISIBLE);
			mErrorTv.bringToFront();
			FontLoader.setRobotoRegularTypeface(mErrorTv);
			mErrorTv.setText(error);
			mErrorTv.startAnimation(mMoveDownAnim);

			if (null != mErrorLayoutListener) {
				mErrorLayoutListener.onErrorShown();
			}

			handler.postDelayed(new Runnable() {

				@Override
				public void run() {
					mErrorTv.startAnimation(mMoveUpAnim);
					mErrorShown.set(false);
					mErrorTv.setVisibility(View.GONE);
					if (null != mErrorLayoutListener) {
						mErrorLayoutListener.onErrorHidden();
					}
				}
			}, shortDuration ? LENGTH_SHORT : LENGTH_LONG);
		}
	}

	private ErrorLayoutListener mErrorLayoutListener;

	public void setErrorLayoutListener(ErrorLayoutListener listener) {
		this.mErrorLayoutListener = listener;
	}

	public interface ErrorLayoutListener {
		void onErrorShown();

		void onErrorHidden();
	}
}
