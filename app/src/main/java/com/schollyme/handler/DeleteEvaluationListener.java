package com.schollyme.handler;

/**
 * Created by user on 2/11/2016.
 */
public interface DeleteEvaluationListener {

    void onDelete();
}
