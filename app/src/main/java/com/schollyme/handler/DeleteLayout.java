package com.schollyme.handler;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.schollyme.R;

public class DeleteLayout {
	private LinearLayout mDeleteLl;

	public DeleteLayout(View view) {
		mDeleteLl = (LinearLayout) view.findViewById(R.id.delete_ll);
		mDeleteLl.setVisibility(View.GONE);
		mDeleteLl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (null != mDeleteLayoutListener) {
					mDeleteLayoutListener.onDeleteClicked();
				}
			}
		});
	}

	public void setData(int delImgResId, int delTxtResId) {
		ImageView imageView = (ImageView) mDeleteLl.findViewById(R.id.delete_iv);
		if (delImgResId > 0) {
			imageView.setVisibility(View.VISIBLE);
			imageView.setImageResource(delImgResId);
		} else {
			imageView.setVisibility(View.GONE);
		}
		TextView textView = (TextView) mDeleteLl.findViewById(R.id.delete_tv);
		if (delTxtResId > 0) {
			textView.setVisibility(View.VISIBLE);
			textView.setText(delTxtResId);
		} else {
			textView.setVisibility(View.GONE);
		}
	}

	public boolean isShowing() {
		return mDeleteLl.getVisibility() == View.VISIBLE;
	}

	public void showDelete() {
		mDeleteLl.setVisibility(View.VISIBLE);
	}

	public void hideDelete() {
		mDeleteLl.setVisibility(View.GONE);
	}

	private DeleteLayoutListener mDeleteLayoutListener;

	public void setDeleteLayoutListener(DeleteLayoutListener listener) {
		this.mDeleteLayoutListener = listener;
	}

	public interface DeleteLayoutListener {
		void onDeleteClicked();
	}
}
