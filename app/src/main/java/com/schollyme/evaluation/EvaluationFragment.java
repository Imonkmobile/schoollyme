package com.schollyme.evaluation;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.DeleteEvaluationListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Evaluation;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.AthleteRequestEvaluationRequest;
import com.vinfotech.request.EvaluationsListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

import java.util.List;

/**
 * Activity class. This may be useful in display user evaluation by evaluator
 *
 * @author Ravi Bhandari
 */
public class EvaluationFragment extends BaseFragment implements OnClickListener, OnRefreshListener {

    private ErrorLayout mErrorLayout;
    private MyEvaluationsViewHolder myEvaluationsViewHolder;
    private List<Evaluation> mEvaluationsModels;

    private Context mContext;
    private String mUserGUID;
    private String mUsersName;
    private String mSportsID, mSportsName;
    private int mActiveUserType;
    private String mTitle;
    private View convertView;
    private int pageIndex = 1;
    private boolean loadingFlag = false;

    public static BaseFragment getInstance(Context context) {
        EvaluationFragment mFragment = new EvaluationFragment();
        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        convertView = inflater.inflate(R.layout.my_evaluations_activity, container, false);
        mContext = getActivity();

        return convertView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        convertView.findViewById(R.id.header_layout).setVisibility(View.GONE);
        mSportsID = getArguments().getString("SportsID");
        mSportsName = getArguments().getString("SportsName");
        mUserGUID = getArguments().getString("UserGUID");
        mUsersName = getArguments().getString("UserName");
        mActiveUserType = getArguments().getInt("ActiveUserType", 0);

        myEvaluationsViewHolder = new MyEvaluationsViewHolder(convertView.findViewById(R.id.main_rl), this);
        mTitle = mUsersName + "'s " + getResources().getString(R.string.Evaluation);
        initHeader();
        myEvaluationsViewHolder.mSwipeRefreshWidget.setColorScheme(R.color.blue,
                R.color.red, R.color.app_text_color, R.color.text_hint_color);
        myEvaluationsViewHolder.mSwipeRefreshWidget.setOnRefreshListener(this);
        mErrorLayout = new ErrorLayout(convertView.findViewById(R.id.main_rl));
    }

    private void initHeader() {

        int rightMenuID = 0;
        if (mActiveUserType == Config.USER_TYPE_ATHLETE) {
            /**User Type Is athlete*/
            rightMenuID = R.drawable.ic_menu_search;
            BaseFragmentActivity.setHeader(R.drawable.icon_menu, rightMenuID, getResources().getString(R.string.My_Evaluation),
                    new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((DashboardActivity) v.getContext()).sliderListener();
                        }
                    }, FILTERListener);
            DashboardActivity.hideShowActionBar(true);

            myEvaluationsViewHolder.mRequestTV.setText(getResources().getString(R.string.request_evaluations));
            myEvaluationsViewHolder.mRequestTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        } else if (mActiveUserType == Config.USER_TYPE_EVALUATOR) {
            /**User Type Is Evaluator*/
            rightMenuID = 0;
            BaseFragmentActivity.setHeader(R.drawable.icon_menu, rightMenuID, mTitle,
                    new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((DashboardActivity) v.getContext()).sliderListener();
                        }
                    }, null);
            DashboardActivity.hideShowActionBar(true);
            myEvaluationsViewHolder.mRequestTV.setText(getResources().getString(R.string.write_evaluations));
            myEvaluationsViewHolder.mRequestTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_edit, 0, 0, 0);

        } else if (mActiveUserType == Config.USER_TYPE_PAID_COACH) {
            /**User Type Is PaidCoach can only view evaluation from other coach*/
            rightMenuID = 0;
            BaseFragmentActivity.setHeader(R.drawable.icon_menu, rightMenuID, mTitle,
                    new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((DashboardActivity) v.getContext()).sliderListener();
                        }
                    }, null);
            DashboardActivity.hideShowActionBar(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getMyEvaluations(mUserGUID, mSportsID);
    }

    OnClickListener FILTERListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivityForResult(MySportsListActivity.getIntent(mContext, mUserGUID,
                    getString(R.string.select_filter), mSportsID), 2000);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK && requestCode == 2000) {
            mSportsID = data.getStringExtra("SportsID");
            mSportsName = data.getStringExtra("SportsName");
        }
    }

    public void setAdapter(final List<Evaluation> mEvaluationsList) {

        EvaluationAdapter mAdapter = new EvaluationAdapter(mContext, mUserGUID, mErrorLayout,
                new DeleteEvaluationListener() {
                    @Override
                    public void onDelete() {
                        getMyEvaluations(mUserGUID, mSportsID);
                    }
                });
        mAdapter.setList(mEvaluationsList);
        myEvaluationsViewHolder.mMyEvaluationsLV.setAdapter(mAdapter);
        myEvaluationsViewHolder.mMyEvaluationsLV.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View arg1, int pos, long arg3) {

                startActivity(EvaluationDetailActivity.getIntent(mContext, mUserGUID, mTitle,
                        mEvaluationsList.get(pos), mEvaluationsList.get(pos).EvaluationGUID, ""));
            }
        });

        if (mEvaluationsList.size() == 0) {
            myEvaluationsViewHolder.mNoRecordTV.setVisibility(View.VISIBLE);
            if (mActiveUserType == Config.USER_TYPE_ATHLETE || mActiveUserType == Config.USER_TYPE_EVALUATOR) {
                myEvaluationsViewHolder.mBottomRL.setVisibility(View.VISIBLE);
            } else {
                myEvaluationsViewHolder.mBottomRL.setVisibility(View.GONE);
            }
        } else {
            myEvaluationsViewHolder.mNoRecordTV.setVisibility(View.GONE);
        }
    }

    public class MyEvaluationsViewHolder {

        private ListView mMyEvaluationsLV;
        private SwipeRefreshLayout mSwipeRefreshWidget;
        private TextView mRequestTV, mNoRecordTV;
        private RelativeLayout mBottomRL;

        public MyEvaluationsViewHolder(View view, OnClickListener listener) {
            mMyEvaluationsLV = (ListView) convertView.findViewById(R.id.mysports_lv);
            mSwipeRefreshWidget = (SwipeRefreshLayout) convertView.findViewById(R.id.swipe_refresh_widget);
            mRequestTV = (TextView) view.findViewById(R.id.action_tv);
            mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
            mBottomRL = (RelativeLayout) view.findViewById(R.id.bottom_action_rl);
            mRequestTV.setOnClickListener(listener);
            mBottomRL.setOnClickListener(listener);
            FontLoader.setRobotoRegularTypeface(mRequestTV, mNoRecordTV);
        }
    }

    @Override
    public void onRefresh() {
        getMyEvaluations(mUserGUID, mSportsID);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_tv:
            case R.id.bottom_action_rl:

                //User type in Athlete,can Request for evaluation to coach
                if (mActiveUserType == Config.USER_TYPE_ATHLETE && mUserGUID.equals(new LogedInUserModel(mContext).mUserGUID)) {
                    DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps,
                            "", getResources().getString(R.string.request_eval_confirm),
                            new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    requestEvaluationToCoach(mSportsID);
                                }
                            }, new OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });

                    //User type Evaluator, can write evaluation for athlete
                } else if (mActiveUserType == Config.USER_TYPE_EVALUATOR) {
                    startActivity(WriteEvaluationActivity.getIntent(mContext, mUserGUID, mSportsID,
                            mSportsName, null, "", mTitle));
                }
                break;

            default:
                break;
        }
    }

    public static Intent getIntent(Context mContext, String UserGUID, String UsersName, String SportsID,
                                   String SportsName, int ActiveUserType) {
        Intent intent = new Intent(mContext, EvaluationFragment.class);
        intent.putExtra("UserGUID", UserGUID);
        intent.putExtra("UserName", UsersName);
        intent.putExtra("SportsID", SportsID);
        intent.putExtra("SportsName", SportsName);
        intent.putExtra("ActiveUserType", ActiveUserType);
        return intent;
    }

    /**
     * Get loggedin user evaluation and display in list
     * Evaluation will be return by type of sports
     * for each sports of athlete a different list will be render
     */
    public void getMyEvaluations(String mUserGUID, String mSportsID) {

        EvaluationsListRequest mRequest = new EvaluationsListRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                myEvaluationsViewHolder.mSwipeRefreshWidget.setRefreshing(false);
                if (success) {
                    mEvaluationsModels = (List<Evaluation>) data;
                    setAdapter(mEvaluationsModels);
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
        mRequest.EvaluationsListServerRequest(mUserGUID, mSportsID);
    }

    /**
     * Delete evaluation request
     */
//    public class DeleteEvaluationListener {
//        public void onDelete() {
//            getMyEvaluations(mUserGUID, mSportsID);
//        }
//    }

    /**
     * Athlete can request for evaluation, all evaluator of same sport will notify to evaluate for athlete
     */
    private void requestEvaluationToCoach(String mSportsID) {
        AthleteRequestEvaluationRequest mRequest = new AthleteRequestEvaluationRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), true, MsgType.Success);
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
        mRequest.EvaluationsListServerRequest(mSportsID);
    }
}