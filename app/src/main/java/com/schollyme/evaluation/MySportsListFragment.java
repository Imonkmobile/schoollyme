package com.schollyme.evaluation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SportsModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.GetUsersSportsRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;

import java.util.List;

public class MySportsListFragment extends BaseFragment implements OnClickListener, OnRefreshListener {

    private static ErrorLayout mErrorLayout;
    private MySportsViewHolder mySportsViewHolder;
    private List<SportsModel> mSportsModels;

    private Context mContext;
    private int pageIndex = 1;
    private boolean loadingFlag = false;
    private String mUserGUID;
    private String mHeaderText;
    private String mSelectedFilter;
    private UsersSportsListAdapter mAdapter;
    private View convertView;

    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;

    public static BaseFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        MySportsListFragment mFragment = new MySportsListFragment();
        mFragment.mCurrentPageToShow = currentPageToShow;
        mFragment.mHeaderLayout = headerLayout;
        mFragment.mShowBackBtn = showBack;
        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        convertView = inflater.inflate(R.layout.evaluations_my_sports_activity, container, false);
        mContext = getActivity();
        convertView.findViewById(R.id.header_layout).setVisibility(View.GONE);
        mHeaderText = getArguments().getString("HeaderText");
        mUserGUID = getArguments().getString("UserGUID");
        mSelectedFilter = getArguments().getString("SelectedFilter");

        return convertView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mySportsViewHolder = new MySportsViewHolder(convertView.findViewById(R.id.main_rl), this);
        mySportsViewHolder.mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red,
                R.color.app_text_color, R.color.text_hint_color);
        mySportsViewHolder.mSwipeRefreshWidget.setOnRefreshListener(this);
        mErrorLayout = new ErrorLayout(convertView.findViewById(R.id.main_rl));

        getMySports(new LogedInUserModel(mContext).mUserGUID);

        initHeader();

    }

    private void initHeader() {

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu, mHeaderText, 0);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashboardActivity) getActivity()).sliderListener();
            }
        }, null);
    }


    private void setAdapter(final List<SportsModel> mSportsList) {

        mAdapter = new UsersSportsListAdapter(mContext, mSelectedFilter, mHeaderText);
        mAdapter.setList(mSportsList);
        mySportsViewHolder.mMySportLV.setAdapter(mAdapter);
        mySportsViewHolder.mMySportLV.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                if (!mHeaderText.equals(getString(R.string.select_filter))) {
                    mSelectedFilter = mAdapter.getItem(pos).mSportsID;
                    mAdapter.resetFilter(mSelectedFilter);
                    startActivity(EvaluationActivity.getIntent(mContext, mUserGUID, "",
                            mSportsList.get(pos).mSportsID, mSportsList.get(pos).mSportsName,
                            Config.USER_TYPE_ATHLETE));
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("SportsID", mAdapter.getItem(pos).mSportsID);
                    intent.putExtra("SportsName", mAdapter.getItem(pos).mSportsName);
                    getActivity().setResult(getActivity().RESULT_OK, intent);
                    getActivity().finish();
                }
            }
        });
    }

    public class MySportsViewHolder {

        private ListView mMySportLV;
        private SwipeRefreshLayout mSwipeRefreshWidget;

        public MySportsViewHolder(View view, OnClickListener listener) {
            mMySportLV = (ListView) view.findViewById(R.id.mysports_lv);
            mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
        }

    }

    private void getMySports(String mUserGUID) {
        GetUsersSportsRequest mRequest = new GetUsersSportsRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mySportsViewHolder.mSwipeRefreshWidget.setRefreshing(false);
                if (success) {
                    mSportsModels = (List<SportsModel>) data;
                    setAdapter(mSportsModels);
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
        mRequest.GetSportsServerRequest(mUserGUID);
    }


    @Override
    public void onRefresh() {
        getMySports(new LogedInUserModel(mContext).mUserGUID);
    }


    @Override
    public void onClick(View v) {

    }

    public static Intent getIntent(Context mContext, String UserGUID, String mHeaderText, String selectedFilter) {
        Intent intent = new Intent(mContext, MySportsListFragment.class);
        intent.putExtra("UserGUID", UserGUID);
        intent.putExtra("HeaderText", mHeaderText);
        intent.putExtra("SelectedFilter", selectedFilter);
        //	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}