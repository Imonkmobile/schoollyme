package com.schollyme.evaluation;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SportsModel;
import com.vinfotech.request.GetUsersSportsRequest;
import com.vinfotech.server.BaseRequest.RequestListener;

public class MySportsListActivity extends BaseActivity implements OnClickListener, OnRefreshListener {

    private static ErrorLayout mErrorLayout;
    private MySportsViewHolder mySportsViewHolder;
    private List<SportsModel> mSportsModels;

    private Context mContext;
    private int pageIndex = 1;
    private boolean loadingFlag = false;
    private String mUserGUID;
    private String mHeaderText;
    private String mSelectedFilter;
    private UsersSportsListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.evaluations_my_sports_activity);
        mContext = this;
        mHeaderText = getIntent().getStringExtra("HeaderText");

        mySportsViewHolder = new MySportsViewHolder(findViewById(R.id.main_rl), this);
        mySportsViewHolder.mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red,
                R.color.app_text_color, R.color.text_hint_color);
        mySportsViewHolder.mSwipeRefreshWidget.setOnRefreshListener(this);
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        mUserGUID = getIntent().getStringExtra("UserGUID");
        mSelectedFilter = getIntent().getStringExtra("SelectedFilter");
        getMySports(new LogedInUserModel(mContext).mUserGUID);

        initHeader();
    }

    private void initHeader() {

        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, 0, mHeaderText,
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mHeaderText.equals(getString(R.string.select_filter))) {
                            Intent intent = new Intent();
                            setResult(RESULT_CANCELED, intent);
                            finish();
                        } else {
                            finish();
                        }
                    }
                }, null);
    }


    private void setAdapter(final List<SportsModel> mSportsList) {

        mAdapter = new UsersSportsListAdapter(mContext, mSelectedFilter, mHeaderText);
        mAdapter.setList(mSportsList);
        mySportsViewHolder.mMySportLV.setAdapter(mAdapter);
        mySportsViewHolder.mMySportLV.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {
                if (!mHeaderText.equals(getString(R.string.select_filter))) {
                    mSelectedFilter = mAdapter.getItem(pos).mSportsID;
                    mAdapter.resetFilter(mSelectedFilter);
                    startActivity(EvaluationActivity.getIntent(mContext, mUserGUID, "",
                            mSportsList.get(pos).mSportsID, mSportsList.get(pos).mSportsName,
                            Config.USER_TYPE_ATHLETE));
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("SportsID", mAdapter.getItem(pos).mSportsID);
                    intent.putExtra("SportsName", mAdapter.getItem(pos).mSportsName);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    public class MySportsViewHolder {

        private ListView mMySportLV;
        private SwipeRefreshLayout mSwipeRefreshWidget;

        public MySportsViewHolder(View view, OnClickListener listener) {
            mMySportLV = (ListView) findViewById(R.id.mysports_lv);
            mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
        }

    }

    private void getMySports(String mUserGUID) {
        GetUsersSportsRequest mRequest = new GetUsersSportsRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mySportsViewHolder.mSwipeRefreshWidget.setRefreshing(false);
                if (success) {
                    mSportsModels = (List<SportsModel>) data;
                    setAdapter(mSportsModels);
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
        mRequest.GetSportsServerRequest(mUserGUID);
    }


    @Override
    public void onRefresh() {
        getMySports(new LogedInUserModel(mContext).mUserGUID);
    }


    @Override
    public void onClick(View v) {

    }

    public static Intent getIntent(Context mContext, String UserGUID, String mHeaderText, String selectedFilter) {
        Intent intent = new Intent(mContext, MySportsListActivity.class);
        intent.putExtra("UserGUID", UserGUID);
        intent.putExtra("HeaderText", mHeaderText);
        intent.putExtra("SelectedFilter", selectedFilter);
        //	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}