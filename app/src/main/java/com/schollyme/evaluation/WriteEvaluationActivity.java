package com.schollyme.evaluation;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.activity.BaseActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.R;
import com.schollyme.googleapi.SearchAddressActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.WriteEvaluationRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

/**
 * Activity class. This may be useful in write evaluation by evaluator
 * <p>
 * * @author Ravi Bhandari
 */
public class WriteEvaluationActivity extends BaseActivity implements OnClickListener {

    private static ErrorLayout mErrorLayout;
    private ViewHolder mViewHolder;
    private HeaderLayout mHeaderLayout;

    private Context mContext;
    private String mUserGUID;
    private String mSportsID, mSportsName;
    private RatingBar mRatingBar;
    private String mFromActivity;
    private String mHeaderText;

    private String mRateValue, mTitle, mDescription, mLocation, mEvaluationRequestGUID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_evaluation_activity);
        mContext = this;
        mViewHolder = new ViewHolder(findViewById(R.id.main_rl), this);

        getValueFromIntent();
        initForm();
        initRatingBar();
        viewListnres();
    }

    private void getValueFromIntent() {
        mSportsID = getIntent().getStringExtra("SportsID");
        mSportsName = getIntent().getStringExtra("SportsName");
        mUserGUID = getIntent().getStringExtra("UserGUID");
        mEvaluationRequestGUID = getIntent().getStringExtra("EvaluationRequestGUID");
        mFromActivity = getIntent().getStringExtra("fromActivity");
        mHeaderText = getIntent().getStringExtra("HeaderText");
    }

    private void performBackAction() {
        Intent intent = DashboardActivity.getIntent(this, 6, "");
        startActivity(intent);
        this.finish();
    }

    private void initForm() {
        mHeaderLayout = new HeaderLayout(findViewById(R.id.main_rl));
        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
                mHeaderText, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Utility.hideSoftKeyboard(null);

                        if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
                            performBackAction();
                        } else {
                            finish();
                        }
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mHeaderLayout.mRightIb1.setEnabled(false);
                        if (ValidateFields()) {
                            writeEvaluation();
                        } else {
                            mHeaderLayout.mRightIb1.setEnabled(true);
                        }

                    }
                });
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        mViewHolder.mSportsTV.setText(mSportsName);
    }

    @SuppressWarnings("ResourceAsColor")
    private void viewListnres() {

        mViewHolder.mTitleET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mViewHolder.mTitleTV.setTextColor(Color.parseColor("#01579B"));
                    mViewHolder.mTitleET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mViewHolder.mTitleTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mViewHolder.mTitleET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mViewHolder.mDescriptionET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mViewHolder.mDescriptionTV.setTextColor(Color.parseColor("#01579B"));
                    mViewHolder.mDescriptionET.setTextColor(Color.parseColor("#000000"));
                    mViewHolder.deviderVI.setBackgroundColor(Color.parseColor("#01579B"));
                } else {
                    mViewHolder.mDescriptionTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mViewHolder.mDescriptionET.setTextColor(Color.parseColor("#666666"));
                    mViewHolder.deviderVI.setBackgroundColor(Color.parseColor("#cfcfcf"));
                }
                changeColorOnView();
            }
        });
    }


    private void changeColorOnView() {

        mViewHolder.mSportsTVLB.setTextColor(Color.parseColor("#cfcfcf"));
        mViewHolder.mSportsTV.setTextColor(Color.parseColor("#666666"));
        mViewHolder.mSportsTV.setBackgroundResource(R.drawable.ic_gray_line);

        mViewHolder.mLocationTVLB.setTextColor(Color.parseColor("#cfcfcf"));
        mViewHolder.mLocationTV.setTextColor(Color.parseColor("#666666"));
        mViewHolder.mLocationTV.setBackgroundResource(R.drawable.ic_gray_line);

    }

    private void changeColorOnViewSelected() {

        mViewHolder.mLocationTVLB.setTextColor(Color.parseColor("#01579B"));
        mViewHolder.mLocationTV.setTextColor(Color.parseColor("#000000"));
        mViewHolder.mLocationTV.setBackgroundResource(R.drawable.ic_blue_line);

        mViewHolder.mSportsTVLB.setTextColor(Color.parseColor("#cfcfcf"));
        mViewHolder.mSportsTV.setTextColor(Color.parseColor("#666666"));
        mViewHolder.mSportsTV.setBackgroundResource(R.drawable.ic_gray_line);

        mViewHolder.mTitleTV.setTextColor(Color.parseColor("#cfcfcf"));
        mViewHolder.mTitleET.setTextColor(Color.parseColor("#666666"));
        mViewHolder.mTitleET.setBackgroundResource(R.drawable.ic_gray_line);

        mViewHolder.mDescriptionTV.setTextColor(Color.parseColor("#cfcfcf"));
        mViewHolder.mDescriptionET.setTextColor(Color.parseColor("#666666"));
        mViewHolder.deviderVI.setBackgroundColor(Color.parseColor("#cfcfcf"));

    }

    private void initRatingBar() {
        mRatingBar = new RatingBar(new ContextThemeWrapper(mContext, R.style.StarRatingBar), null, 0);
        mRatingBar.setIsIndicator(false);
        mRatingBar.setStepSize(1);
        mRatingBar.setNumStars(5);
        mRatingBar.setRating(0);
        mViewHolder.mRatingRL.addView(mRatingBar);
        mRatingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar arg0, float rating, boolean arg2) {

                int mRate = (int) rating;
                mRatingBar.setRating(mRate);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.location_tv:
                changeColorOnViewSelected();
                startActivityForResult(SearchAddressActivity.getIntent(mContext),
                        SearchAddressActivity.SEARCH_REQUEST);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SearchAddressActivity.SEARCH_REQUEST) {

                String mLocation = data.getStringExtra("LOC");
                Log.i("SearchAddressActivity", "SearchAddressActivity : Result LOC = "
                        + data.getStringExtra("LOC") + " googlePlace = " + data.getStringExtra("googlePlace"));
                if (mLocation != null)
                    mViewHolder.mLocationTV.setText(mLocation);
            }
        }
    }

    private class ViewHolder {

        private TextView mHeaderTV, mSportsTV, mLocationTV, mSportsTVLB, mTitleTV, mLocationTVLB, mDescriptionTV;
        private EditText mTitleET, mDescriptionET;
        private RelativeLayout mRatingRL;
        private View deviderVI;

        private ViewHolder(View view, OnClickListener listener) {
            mHeaderTV = (TextView) view.findViewById(R.id.evaluate_header_tv);
            mSportsTVLB = (TextView) view.findViewById(R.id.sports_tv_label);
            mSportsTV = (TextView) view.findViewById(R.id.sports_tv);
            mTitleTV = (TextView) view.findViewById(R.id.title_tv_label);
            mTitleET = (EditText) view.findViewById(R.id.title_et);
            mLocationTVLB = (TextView) view.findViewById(R.id.location_tv_label);
            mLocationTV = (TextView) view.findViewById(R.id.location_tv);
            mDescriptionTV = (TextView) view.findViewById(R.id.description_tv_label);
            mDescriptionET = (EditText) view.findViewById(R.id.description_et);
            deviderVI = (View) view.findViewById(R.id.devider_description);

            mRatingRL = (RelativeLayout) view.findViewById(R.id.ratings_rl);
            mLocationTV.setOnClickListener(listener);
        }
    }

    public static Intent getIntent(Context mContext, String UserGUID, String SportsID, String SportsName,
                                   String mEvaluationRequestGUID, String fromActivity, String HeaderText) {
        Intent intent = new Intent(mContext, WriteEvaluationActivity.class);
        intent.putExtra("UserGUID", UserGUID);
        intent.putExtra("SportsID", SportsID);
        intent.putExtra("EvaluationRequestGUID", mEvaluationRequestGUID);
        intent.putExtra("SportsName", SportsName);
        intent.putExtra("fromActivity", fromActivity);
        intent.putExtra("HeaderText", HeaderText);
        return intent;
    }

    /**
     * Validate fields require for submit an evaluation
     */
    private boolean ValidateFields() {
        mRateValue = String.valueOf((int) mRatingBar.getRating());
        mTitle = mViewHolder.mTitleET.getText().toString();
        mDescription = mViewHolder.mDescriptionET.getText().toString();
        mLocation = mViewHolder.mLocationTV.getText().toString();

        if (TextUtils.isEmpty(mRateValue) || mRateValue.equals("0")) {
            mErrorLayout.showError(getResources().getString(R.string.rating_require), true, MsgType.Error);
            return false;
        } else if (TextUtils.isEmpty(mTitle)) {
            mErrorLayout.showError(getResources().getString(R.string.title_require), true, MsgType.Error);
            return false;
        } else if (mTitle.length() < 2) {
            mErrorLayout.showError(getResources().getString(R.string.title_length2_require), true, MsgType.Error);
            return false;
        } else if (TextUtils.isEmpty(mLocation)) {
            mErrorLayout.showError(getResources().getString(R.string.location_require), true, MsgType.Error);
            return false;
        } else if (TextUtils.isEmpty(mDescription)) {
            mErrorLayout.showError(getResources().getString(R.string.description_require), true, MsgType.Error);
            return false;
        } else if (mDescription.length() < 2) {
            mErrorLayout.showError(getResources().getString(R.string.description_length2_require), true, MsgType.Error);
            return false;
        } else {
            return true;
        }

    }

    /**
     * Submit evaluation to server
     */
    private void writeEvaluation() {
        WriteEvaluationRequest mRequest = new WriteEvaluationRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mErrorLayout.showError(data.toString(), true, MsgType.Success);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
                                performBack();
                            } else {
                                finish();
                            }


                        }
                    }, 2000);
                } else {
                    mHeaderLayout.mRightIb1.setEnabled(true);
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
        mRequest.WriteEvaluationsServerRequest(mUserGUID, mSportsID, mRateValue, mTitle, mDescription, mLocation, mEvaluationRequestGUID);

    }


    @Override
    public void onBackPressed() {
        performBack();
    }

    private void performBack() {
        if ("GCMIntentService".equalsIgnoreCase(mFromActivity)) {
            Intent intent = DashboardActivity.getIntent(this, 6, "");
            startActivity(intent);
            this.finish();
        } else
            super.onBackPressed();
    }
}