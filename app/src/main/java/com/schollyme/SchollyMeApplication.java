package com.schollyme;

import java.io.IOException;
import java.util.Locale;

import android.app.AlarmManager;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.schollyme.songofday.SongNotificationActivity;
import com.vinfotech.utility.Config;

public class SchollyMeApplication extends Application {

    public static String mDeviceLanguage;
    private final static int NOTIFICATION_ID = 123456;
    private static SchollyMeApplication sSMeApp;

    @Override
    public void onCreate() {
        super.onCreate();
        mDeviceLanguage = Locale.getDefault().getLanguage();
        sSMeApp = this;
    }

    private MediaPlayer mMediaPlayer = null;
    private String mLastUrl = null;
    private String mSongTitle;
    private boolean mBuffering = false;
    private int mPlayingPos = -1;
    private SongPlayCompletionListener mSongPlayCompletionListener;
    private SongLoadListener mSongLoadListener;
    private boolean isPaused = false;

    public void setPlayingPos(int pos) {
        mPlayingPos = pos;
    }

    public int getPlayingPos() {
        return mPlayingPos;
    }

    public synchronized boolean playSong(String title, final String url, final SongLoadListener listener) {
        if (Config.DEBUG) {
            Log.d("SchollyMeApplication", "playSong url=" + url);
        }
        if (TextUtils.isEmpty(url) || !android.util.Patterns.WEB_URL.matcher(url).matches()) {
            Log.e("SchollyMeApplication", "playSong invalid url=" + url);
            return false;
        }
        this.mSongTitle = title;
        this.mSongLoadListener = listener;
        this.isPaused = false;
        if (null == mMediaPlayer) {
            mMediaPlayer = new MediaPlayer();
        }
        if (!url.equalsIgnoreCase(mLastUrl)) {
            // set prev playing song to false
            if (null != mLastUrl && null != listener) {
                listener.onLoad(false, mLastUrl, mMediaPlayer);
            }

            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setLooping(true);
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    //song play done
                    setCustomNotification(SchollyMeApplication.this, mSongTitle, 2);
                    if (null != mSongPlayCompletionListener) {
                        mSongPlayCompletionListener.onCompletion(mMediaPlayer);
                    }
                }
            });

            try {
                mMediaPlayer.setDataSource(url);
                mMediaPlayer.setOnPreparedListener(new OnPreparedListener() {

                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        if (!isPaused) {
                            setBuffering(false);
                            mMediaPlayer.start();
                        } else {
                            cancelNotifications(true);
                        }
                        if (null != listener) {
                            listener.onLoad(true, url, mp);
                        }
                        //song play started
                    }
                });
                mMediaPlayer.setOnErrorListener(new OnErrorListener() {

                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        mLastUrl = null;
                        setBuffering(false);
                        //could not play song
                        if (null != listener) {
                            listener.onLoad(false, url, mp);
                            return true;
                        }
                        return false;
                    }
                });

                mMediaPlayer.prepareAsync();

                mLastUrl = url;
                setBuffering(true);

                return true;
            } catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e) {
                e.printStackTrace();
            }
        } else if (mBuffering) {
            return true;
        } else if (!mMediaPlayer.isPlaying()) {
            mMediaPlayer.seekTo(0);
            mMediaPlayer.start();
            setCustomNotification(SchollyMeApplication.this, mSongTitle, 3);
            if (null != listener) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listener.onLoad(true, url, mMediaPlayer);
                    }
                }, 100);
            }
            return true;
        }
        return false;
    }

    private void setBuffering(boolean buffering) {
        mBuffering = buffering;
        if (mBuffering) {
            //buffering has been started
            setCustomNotification(SchollyMeApplication.this, mSongTitle, 1);
            clearNotificationOnSwipe();
        } else {
            //buffring has been ended
            setCustomNotification(SchollyMeApplication.this, mSongTitle, 3);
        }
    }

    public String getPlayingUrl() {
        return mLastUrl;
    }

    public MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    public boolean pauseSong() {
        isPaused = true;
        if (null != mMediaPlayer && mMediaPlayer.isPlaying()) {
            //song has been paused
            setCustomNotification(SchollyMeApplication.this, mSongTitle, 2);
            mMediaPlayer.pause();
            return true;
        }
        return false;
    }

    public boolean pauseSongWithoutNotification() {
        isPaused = true;
        if (null != mMediaPlayer && mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            return true;
        }
        return false;
    }

    public boolean playSong() {
        isPaused = false;
        if (null != mMediaPlayer && !mMediaPlayer.isPlaying()) {
            mMediaPlayer.start();
            setCustomNotification(SchollyMeApplication.this, mSongTitle, 3);
            return true;
        }
        return false;
    }

    public boolean stopSong() {
        isPaused = true;
        if (null != mMediaPlayer) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            return true;
        }
        return false;
    }

    public boolean isBuffering() {
        return mBuffering;
    }

    public boolean isPlaying() {
        if (null != mMediaPlayer && mMediaPlayer.isPlaying()) {
            return true;
        }
        return false;
    }

    public String getSongTitle() {
        return mSongTitle;
    }

    public interface SongLoadListener {
        void onLoad(boolean success, String url, MediaPlayer mediaPlayer);
    }

    public void setSongPlayCompletionListener(SongPlayCompletionListener listener) {
        mSongPlayCompletionListener = listener;
    }

    public interface SongPlayCompletionListener {
        void onCompletion(MediaPlayer mp);
    }

    public void setCustomNotification(Context context, String mSong, int mStatus) {

        final RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.sod_notification_viw);
        Intent intent = new Intent(context, SongNotificationActivity.class);
        intent.putExtra("title", mSong);
        intent.putExtra("Status", mStatus);
        PendingIntent pIntent = PendingIntent.getActivity(context, mStatus, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent cancelIntent = new Intent(context, SongNotificationActivity.class);
        cancelIntent.putExtra("title", mSong);
        cancelIntent.putExtra("Status", 0);
        PendingIntent cancelPIntent = PendingIntent.getActivity(context, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setAutoCancel(false)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent)
                .setContent(remoteViews)
                .setDeleteIntent(cancelPIntent);

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.song_name_tv, mSong);
        if (mStatus == 1) {
            remoteViews.setViewVisibility(R.id.progress, View.VISIBLE);
            remoteViews.setViewVisibility(R.id.play_ib, View.GONE);
            remoteViews.setViewVisibility(R.id.pause_ib, View.GONE);
        } else if (mStatus == 2) {
            remoteViews.setViewVisibility(R.id.progress, View.GONE);
            remoteViews.setViewVisibility(R.id.play_ib, View.VISIBLE);
            remoteViews.setViewVisibility(R.id.pause_ib, View.GONE);

            Intent switchIntent = new Intent(context, SongBroadcastReceiver.class);
            switchIntent.putExtra("Status", 2);
            switchIntent.putExtra("title", mSong);
            PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(context, mStatus,
                    switchIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.play_ib, pendingSwitchIntent);
        } else if (mStatus == 3) {
            remoteViews.setViewVisibility(R.id.progress, View.GONE);
            remoteViews.setViewVisibility(R.id.play_ib, View.GONE);
            remoteViews.setViewVisibility(R.id.pause_ib, View.VISIBLE);

            Intent switchIntent = new Intent(context, SongBroadcastReceiver.class);
            switchIntent.putExtra("Status", 3);
            switchIntent.putExtra("title", mSong);
            PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(context, mStatus,
                    switchIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.pause_ib, pendingSwitchIntent);
        }

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        Notification notification = builder.build();
        //notification.flags = Notification.FLAG_NO_CLEAR;
        notificationmanager.notify(NOTIFICATION_ID, notification);
    }

    public void cancelNotifications(boolean onlySOD) {

        pauseSong();
        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (onlySOD) {
            notificationmanager.cancel(NOTIFICATION_ID);
        } else {
            notificationmanager.cancelAll();
        }
    }

    public static class SongBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra("Status", 2);
            String title = intent.getStringExtra("title");
            Log.d("SongBroadcastReceiver", "onReceive status=" + status);
            if (status == 2) {
                sSMeApp.playSong();
                if (null != sSMeApp.mSongLoadListener) {
                    sSMeApp.mSongLoadListener.onLoad(true, sSMeApp.mLastUrl, sSMeApp.getMediaPlayer());
                }
            } else if (status == 3) {
                sSMeApp.pauseSong();
                if (null != sSMeApp.mSongPlayCompletionListener) {
                    sSMeApp.mSongPlayCompletionListener.onCompletion(sSMeApp.getMediaPlayer());
                }
            }
        }
    }

    private PendingIntent mPendingIntent;
    private android.os.Handler mHandler = new android.os.Handler();

    private void clearNotificationOnSwipe(){
        mHandler.removeCallbacksAndMessages(null);
        AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if(null == mPendingIntent) {
            Intent intent = new Intent(this, SongNotificationActivity.class);
            intent.putExtra("title", mSongTitle);
            intent.putExtra("Status", -1);
            mPendingIntent = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else{
            mgr.cancel(mPendingIntent);
        }
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, mPendingIntent);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                clearNotificationOnSwipe();
            }
        }, 750);
    }
}
