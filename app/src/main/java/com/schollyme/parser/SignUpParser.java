package com.schollyme.parser;

import org.json.JSONObject;

import com.schollyme.model.SignUpModel;


public class SignUpParser extends BaseParser {
	
	private SignUpModel mSignUpModel;

	public boolean parse(String json) {
		boolean ret = super.parse("signup", json);
		if (ret && getResponseCode().equalsIgnoreCase("1")) {
			try {
				
				JSONObject userData = getDataObject()/*.getJSONObject("Data")*/;
				mSignUpModel = new SignUpModel(userData);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			ret = true;
		} else {
			ret = false;
		}
		return ret;
	}
	
	public SignUpModel getmRenovateUser() {
		return mSignUpModel;
	}
}