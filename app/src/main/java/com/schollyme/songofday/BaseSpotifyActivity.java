package com.schollyme.songofday;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.schollyme.activity.BaseActivity;
import com.schollyme.R;
import com.schollyme.model.Song;
import com.schollyme.model.SongSource;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.Spotify;

import java.util.ArrayList;
import java.util.List;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyCallback;
import kaaes.spotify.webapi.android.SpotifyError;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.AlbumSimple;
import kaaes.spotify.webapi.android.models.ArtistSimple;
import kaaes.spotify.webapi.android.models.Pager;
import kaaes.spotify.webapi.android.models.SavedTrack;
import kaaes.spotify.webapi.android.models.Track;

/**
 * Created by ramanands on 10/7/2015.
 */
public abstract class BaseSpotifyActivity extends BaseActivity implements PlayerNotificationCallback, ConnectionStateCallback {

    private static final String CLIENT_ID = "b94de60e99e042e399b608cf8546a918";
    private static final String REDIRECT_URI = "schollyme://callback";
    private static final int REQUEST_CODE = 1337;
    private Player mPlayer;
    private ProgressBar mLoaderPb;
    private SongSource mSongSource;
    private String mAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSongSource = getIntent().getParcelableExtra("songSource");
        if (com.vinfotech.utility.Config.DEBUG) {
            Log.d("SODPSongsActivity", "onCreate mProvider=" + mSongSource);
        }

        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID,
                AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
        builder.setScopes(new String[]{"user-read-private", "streaming"});
        AuthenticationRequest request = builder.build();
        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mLoaderPb = (ProgressBar) findViewById(R.id.loading_pb);
        showLoader(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            mAccessToken = response.getAccessToken();
            if(TextUtils.isEmpty(mAccessToken)){
                showLoader(false);
                finish();
            } else {
                callPlayList();
            }
        } else {
            showLoader(false);
        }
    }

    private void showLoader(final boolean show) {
        if (null != mLoaderPb) {
            mLoaderPb.post(new Runnable() {
                @Override
                public void run() {
                    mLoaderPb.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    protected void callPlayList() {
        final List<Song> songs = new ArrayList<Song>();
        if(null == mAccessToken || mAccessToken.trim().length() < 1){
            onSongListReceive(songs);
            return;
        }
        showLoader(true);
        SpotifyApi api = new SpotifyApi();
        api.setAccessToken(mAccessToken);

        SpotifyService spotify = api.getService();
        spotify.getMySavedTracks(new SpotifyCallback<Pager<SavedTrack>>() {
            @Override
            public void success(Pager<SavedTrack> savedTrackPager, retrofit.client.Response response) {
                showLoader(false);
                //playTrack(savedTrackPager.items.get(1).track.uri);
                if (null != savedTrackPager && null != savedTrackPager.items) {
                    for (SavedTrack savedTrack : savedTrackPager.items) {
                        Track track = savedTrack.track;
                        List<ArtistSimple> artists = track.artists;
                        StringBuilder sb = new StringBuilder();
                        if (null != artists) {
                            for (int i = 0; i < artists.size(); i++) {
                                sb.append(artists.get(i).name);
                                if (i < artists.size() - 1) {
                                    sb.append(", ");
                                }
                            }
                        }
                        AlbumSimple album = track.album;
                        String imageUrl = "";
                        if(null != album.images && album.images.size() > 2){
                            imageUrl = album.images.get(1).url;
                        } else if(null != album.images && album.images.size() > 0){
                            imageUrl = album.images.get(0).url;
                        }
                        Song song = new Song(track.id, getString(R.string.Spotify), "", track.name, imageUrl, sb.toString(), album.name);
                        song.PlayListGUID = mSongSource.SourceGUIID;
                        song.SongURL = track.preview_url;
                        song.SongPreviewURL = track.preview_url;
                        songs.add(song);
                    }
                }
                onSongListReceive(songs);
            }

            @Override
            public void failure(SpotifyError error) {
                Log.e("BaseSpotifyActivity", error.getMessage());
                showLoader(false);
                onSongListReceive(songs);
            }
        });
    }


    private void playTrack(final String track) {
        Config playerConfig = new Config(this, mAccessToken, CLIENT_ID);
        mPlayer = Spotify.getPlayer(playerConfig, this, new Player.InitializationObserver() {
            @Override
            public void onInitialized(Player player) {
                mPlayer.addConnectionStateCallback(BaseSpotifyActivity.this);
                mPlayer.addPlayerNotificationCallback(BaseSpotifyActivity.this);
                mPlayer.play(track);
            }

            @Override
            public void onError(Throwable throwable) {
                Log.e("BaseSpotifyActivity", "Could not initialize player: " + throwable.getMessage());
            }
        });
    }

    @Override
    public void onLoggedIn() {
        Log.d("BaseSpotifyActivity", "User logged in");
    }

    @Override
    public void onLoggedOut() {
        Log.d("BaseSpotifyActivity", "User logged out");
    }

    @Override
    public void onLoginFailed(Throwable error) {
        Log.d("BaseSpotifyActivity", "Login failed");
    }

    @Override
    public void onTemporaryError() {
        Log.d("BaseSpotifyActivity", "Temporary error occurred");
    }

    @Override
    public void onConnectionMessage(String message) {
        Log.d("BaseSpotifyActivity", "Received connection message: " + message);
    }

    @Override
    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {
        Log.d("BaseSpotifyActivity", "Playback event received: " + eventType.name());

    }

    @Override
    public void onPlaybackError(ErrorType errorType, String errorDetails) {
        Log.d("BaseSpotifyActivity", "Playback error received: " + errorType.name());
    }

    @Override
    protected void onDestroy() {
        Spotify.destroyPlayer(this);
        super.onDestroy();
    }

    protected abstract void onSongListReceive(List<Song> list);
}
