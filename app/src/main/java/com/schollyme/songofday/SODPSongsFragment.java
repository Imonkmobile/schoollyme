package com.schollyme.songofday;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.schollyme.R;
import com.schollyme.adapter.SongAdapter;
import com.schollyme.adapter.SongAdapter.ContentType;
import com.schollyme.adapter.SongSourceAdapter;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.DeleteLayout;
import com.schollyme.handler.DeleteLayout.DeleteLayoutListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.Song;
import com.schollyme.model.SongSource;
import com.vinfotech.request.DeleteSongRequest;
import com.vinfotech.request.SetSongOfDayRequest;
import com.vinfotech.request.SongListRequest;
import com.vinfotech.request.SongMainListRequest;
import com.vinfotech.request.SongOfTheDayRequest;
import com.vinfotech.request.SongSourceRequest;
import com.vinfotech.request.SongsPlayListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
import java.util.List;

public class SODPSongsFragment extends BaseFragment {

    private static final String TAG = SODPlaylistActivity.class.getSimpleName();

    private SwipeRefreshLayout mPullToRefreshSrl;
    private ErrorLayout mErrorLayout;
    private SongSourceAdapter mSongSourceAdapter;
    private SongSourceRequest mSongSourceRequest;
    private List<SongSource> mSongSources;
    private static boolean sReload = false;
    private Context mContext;

    public static SODPSongsFragment newInstance(ErrorLayout errorLayout) {
        SODPSongsFragment mSODPSongsFragment = new SODPSongsFragment();
        return mSODPSongsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sod_last7day_fragment, null);
        mContext = getActivity();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

    }

    public static Intent getIntent(Context context, SongSource songSource) {
        Intent intent = new Intent(context, SODPSongsFragment.class);
        intent.putExtra("songSource", songSource);
        return intent;
    }

    private void init(View view) {

        mSongSourceAdapter = new SongSourceAdapter(mContext);
        mPullToRefreshSrl = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSongSourceRequest.setActivityStatus(true);
                mSongSourceRequest.getSongSourceFromServer();
            }
        });

        final ListView genericLv = (ListView) view.findViewById(R.id.generic_lv);
        genericLv.setAdapter(mSongSourceAdapter);
        genericLv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final SongSource songSource = mSongSourceAdapter.getItem(position);
                mSongSourceAdapter.selectSingle(position);
                genericLv.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(SODPSongsActivity.getIntent(mContext, songSource));
                        mSongSourceAdapter.selectSingle(-1);
                    }
                }, 500);
            }
        });

        mSongSourceRequest = new SongSourceRequest(mContext);
        mSongSourceRequest.setLoader(view.findViewById(R.id.loading_pb));
        mSongSourceRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    mSongSources = (List<SongSource>) data;
                    mSongSourceAdapter.setList(mSongSources);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                }
            }
        });
        mSongSourceRequest.getSongSourceFromServer();
    }

    public static void setReload(boolean reload) {
        sReload = reload;
    }

    @Override
    public void onResume() {
        if (sReload) {
            sReload = false;
            mSongSourceRequest.getSongSourceFromServer();
        }
        super.onResume();
    }

}
