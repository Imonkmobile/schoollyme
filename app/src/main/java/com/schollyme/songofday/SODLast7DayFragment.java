package com.schollyme.songofday;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.adapter.SongAdapter;
import com.schollyme.adapter.SongAdapter.ContentType;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Song;
import com.vinfotech.request.SetSongOfDayRequest;
import com.vinfotech.request.SongListRequest;
import com.vinfotech.request.SongOfTheDayRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

public class SODLast7DayFragment extends Fragment {

    private static final String TAG = SODLast7DayFragment.class.getSimpleName();
    private View mLoaderPb;
    private View mEmptyView;
    private View mFooterView;
    private SwipeRefreshLayout mPullToRefreshSrl;
    private ListView mGenericLv;

    private SongAdapter mSongAdapter;
    private ErrorLayout mErrorLayout;
    private SongListRequest mSongListRequest;
    private SongOfTheDayRequest mSongOfTheDayRequest;
    private SetSongOfDayRequest mSetSongOfDayRequest;

    private static boolean sReload = false;
    private boolean mFetchedAll = true;

    private List<Song> mSongs = new ArrayList<Song>();
    private int mPageNo = Config.DEFAULT_PAGE_INDEX;


    public static SODLast7DayFragment newInstance(ErrorLayout errorLayout) {
        SODLast7DayFragment sodLast7DayFragment = new SODLast7DayFragment();
        sodLast7DayFragment.mErrorLayout = errorLayout;
        return sodLast7DayFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sod_last7day_fragment, null);

        mFooterView = inflater.inflate(R.layout.footer_loader_layout, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mSongAdapter = new SongAdapter(view.getContext(), ContentType.Last7Day, mErrorLayout);

        mPullToRefreshSrl = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {
                mPageNo = Config.DEFAULT_PAGE_INDEX;
                mSongOfTheDayRequest.getSongOfDayFromServer("");
            }
        });

        mFooterView.setVisibility(View.GONE);
        mLoaderPb = view.findViewById(R.id.loader_pb);
        mEmptyView = view.findViewById(R.id.empty_tv);

        mGenericLv = (ListView) view.findViewById(R.id.generic_lv);
        mGenericLv.setAdapter(mSongAdapter);
        mGenericLv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song song = mSongAdapter.getItem(position);
                if (position > 0 || !mSongAdapter.isEnabledHeader()) {
                    showSetSongDialog(song, getActivity(), mErrorLayout);
                }
            }
        });

        mGenericLv.setOnScrollListener(new OnScrollListener() {
            private boolean bReachedListEnd;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (Config.DEBUG) {
                    Log.d(TAG, "onScrollStateChanged bReachedListEnd="
                            + bReachedListEnd + ", mFetchedAll=" + mFetchedAll
                            + ", scrollState=" + scrollState);
                }
                if (bReachedListEnd && !mFetchedAll) {
                    mFetchedAll = true;
                    mPageNo++;
                    mSongListRequest.getSongListFromServer("", "", mPageNo, true);
                }
            }

            @Override
            public void onScroll(AbsListView view, final int firstVisibleItem,
                                 final int visibleItemCount, final int totalItemCount) {
                bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);
                if (Config.DEBUG) {
                    Log.d(TAG, "onScroll bReachedListEnd=" + bReachedListEnd + ", firstVisibleItem=" + firstVisibleItem
                            + ", visibleItemCount=" + visibleItemCount + ", totalItemCount=" + totalItemCount);
                }
            }
        });

        init();

        super.onViewCreated(view, savedInstanceState);
    }

    public static void setReload(boolean reload) {
        sReload = reload;
    }

    @Override
    public void onResume() {
        if (sReload) {
            sReload = false;
            mSongOfTheDayRequest.getSongOfDayFromServer("");
        }
        super.onResume();
    }

    private void init() {

        mSongOfTheDayRequest = new SongOfTheDayRequest(getActivity());
        mSongOfTheDayRequest.setLoader(mLoaderPb);
        mSongOfTheDayRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    Song song = (Song) data;
                    initList();
                    if (song.isValid()) {
                        mSongs.add(song);
                        mSongAdapter.setList(mSongs);
                        mSongAdapter.enableHeader(mSongs.size() > 0);
                    }
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                    mPullToRefreshSrl.setRefreshing(false);
                }
                mEmptyView.setVisibility(mSongAdapter.getCount() < 1 ? View.VISIBLE : View.GONE);
                if (null != mSongListRequest) {
                    mSongListRequest.getSongListFromServer("", "", mPageNo, true);
                }
            }
        });
        mSongOfTheDayRequest.getSongOfDayFromServer("");

        mSongListRequest = new SongListRequest(getActivity());
        mSongListRequest.setLoader(mPageNo == Config.DEFAULT_PAGE_INDEX ? mLoaderPb : mFooterView);
        mSongListRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    List<Song> list = (List<Song>) data;
                    mSongs.addAll(list);
                    mSongAdapter.setList(mSongs);
                    mEmptyView.setVisibility(mSongAdapter.getCount() < 1 ? View.VISIBLE : View.GONE);
                    mFetchedAll = (list.size() < Config.PAGE_SIZE);
                    mFooterView.setVisibility(mFetchedAll ? View.GONE : View.VISIBLE);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                }
            }
        });

        mSetSongOfDayRequest = new SetSongOfDayRequest(getActivity());
        mSetSongOfDayRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (null != mErrorLayout) {
                    if (success) {
                        //mErrorLayout.showError("Song set as Song of the Day", true, MsgType.Success);
                        mSongOfTheDayRequest.getSongOfDayFromServer("");
                    } else {
                        mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                                        : (String) data), true, MsgType.Error);
                    }
                }
            }
        });
    }

    private void initList() {
        mSongAdapter = new SongAdapter(mGenericLv.getContext(), ContentType.Last7Day, mErrorLayout);
        mGenericLv.setAdapter(mSongAdapter);
        mPageNo = Config.DEFAULT_PAGE_INDEX;
        mSongs.clear();
        mSongAdapter.setList(mSongs);
    }

    private void showSetSongDialog(final Song song, Activity activity, final ErrorLayout errorLayout) {
        DialogUtil.showOkCancelDialog(activity, R.string.Set, R.string.ok_label, R.string.cancel_caps,
                activity.getString(R.string.Set_Song),
                activity.getString(R.string.Are_you_sure_set_as_SotD), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        List<Song> songs = new ArrayList<Song>();
                        songs.add(song);
                        mSetSongOfDayRequest.setSongOfTheDayInServer(song.PlayListGUID, song.SourceGUID, songs);
                    }
                }, null);
    }
}
