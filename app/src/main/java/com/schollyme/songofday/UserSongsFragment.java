package com.schollyme.songofday;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.Toast;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.adapter.UserSongAdapter;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.model.Song;
import com.vinfotech.request.SongListRequest;
import com.vinfotech.request.SongOfTheDayRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

public class UserSongsFragment extends BaseFragment {
	private static final String TAG = UserSongsFragment.class.getSimpleName();

	private View mLoaderPb;
	private View mEmptyView;
	private View mFooterView;
	private SwipeRefreshLayout mPullToRefreshSrl;

	private UserSongAdapter mSongAdapter;
	private String mUserGUID;

	private SongOfTheDayRequest mSongOfTheDayRequest;
	private SongListRequest mSongListRequest;

	private List<Song> mSongs = new ArrayList<Song>();
	private int mPageNo = Config.DEFAULT_PAGE_INDEX;
	private boolean mFetchedAll = true;

	public static UserSongsFragment newInstance(String UserGUID) {
		UserSongsFragment sodLast7DayFragment = new UserSongsFragment();
		sodLast7DayFragment.mUserGUID = UserGUID;
		return sodLast7DayFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.user_songs_fragment, null);

		mFooterView = inflater.inflate(R.layout.footer_loader_layout, null);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		mSongAdapter = new UserSongAdapter(view.getContext());
		
		mPullToRefreshSrl = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
		mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
		mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				mPageNo = Config.DEFAULT_PAGE_INDEX;
				mSongOfTheDayRequest.getSongOfDayFromServer("");
			}
		});

		mFooterView.setVisibility(View.GONE);
		mLoaderPb = view.findViewById(R.id.loader_pb);
		mEmptyView = view.findViewById(R.id.empty_tv);
		FontLoader.setRobotoRegularTypeface(mEmptyView);

		ListView genericLv = (ListView) view.findViewById(R.id.generic_lv);
		genericLv.setAdapter(mSongAdapter);

		genericLv.setOnScrollListener(new OnScrollListener() {
			private boolean bReachedListEnd;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (Config.DEBUG) {
					Log.d(TAG, "onScrollStateChanged bReachedListEnd=" + bReachedListEnd + ", mFetchedAll=" + mFetchedAll
							+ ", scrollState=" + scrollState);
				}
				if (bReachedListEnd && !mFetchedAll) {
					mFetchedAll = true;
					mPageNo++;

					mSongListRequest.getSongListFromServer("", mUserGUID, mPageNo, false);
				}
			}

			@Override
			public void onScroll(AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
				bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);
				if (Config.DEBUG) {
					Log.d(TAG, "onScroll bReachedListEnd=" + bReachedListEnd + ", firstVisibleItem=" + firstVisibleItem
							+ ", visibleItemCount=" + visibleItemCount + ", totalItemCount=" + totalItemCount);
				}
			}
		});

		init();

		super.onViewCreated(view, savedInstanceState);
	}

	private void init() {
		mSongOfTheDayRequest = new SongOfTheDayRequest(getActivity());
		mSongOfTheDayRequest.setLoader(mLoaderPb);
		mSongOfTheDayRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mSongs.clear();
					mSongs.add((Song) data);
					mSongAdapter.setList(mSongs, mSongs.size() >= 1);
				} else {
					Toast.makeText(getActivity(), (null == data ? getString(R.string.Something_went_wrong) : (String) data),
							Toast.LENGTH_SHORT).show();
					mPullToRefreshSrl.setRefreshing(false);
				}
				mEmptyView.setVisibility(mSongAdapter.getCount() < 1 ? View.VISIBLE : View.GONE);
				if(null != mSongListRequest) {
					mSongListRequest.getSongListFromServer("", mUserGUID, mPageNo, true);
				}
			}
		});
		mSongOfTheDayRequest.getSongOfDayFromServer(mUserGUID);
		
		mSongListRequest = new SongListRequest(getActivity());
		mSongListRequest.setLoader(mPageNo == Config.DEFAULT_PAGE_INDEX ? mLoaderPb : mFooterView);
		mSongListRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mPullToRefreshSrl.setRefreshing(false);
				if (success) {
					List<Song> list = (List<Song>) data;
					mSongs.addAll(list);
					mSongAdapter.setList(mSongs, mSongs.size() >= 1);
					mEmptyView.setVisibility(mSongAdapter.getCount() < 1 ? View.VISIBLE : View.GONE);
					mFetchedAll = (list.size() < Config.PAGE_SIZE);
					mFooterView.setVisibility(mFetchedAll ? View.GONE : View.VISIBLE);
				} else {
					Toast.makeText(getActivity(), (null == data ? getString(R.string.Something_went_wrong) : (String) data),
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
}
