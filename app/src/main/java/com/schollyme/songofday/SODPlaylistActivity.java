package com.schollyme.songofday;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.schollyme.R;
import com.schollyme.activity.AboutActivity;
import com.schollyme.activity.BaseActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.adapter.SongSourceAdapter;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.SongSource;
import com.vinfotech.request.SongSourceRequest;
import com.vinfotech.server.BaseRequest.RequestListener;

public class SODPlaylistActivity extends BaseActivity {

    private static final String TAG = SODPlaylistActivity.class.getSimpleName();

    private SwipeRefreshLayout mPullToRefreshSrl;
    private ErrorLayout mErrorLayout;
    private SongSourceAdapter mSongSourceAdapter;
    private SongSourceRequest mSongSourceRequest;
    private List<SongSource> mSongSources;
    private static boolean sReload = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sod_playlist_fragment);

        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, 0, getString(R.string.play_list_label),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }, null);

        mSongSourceAdapter = new SongSourceAdapter(this);
        mPullToRefreshSrl = (SwipeRefreshLayout) findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSongSourceRequest.setActivityStatus(true);
                mSongSourceRequest.getSongSourceFromServer();
            }
        });

        final ListView genericLv = (ListView) findViewById(R.id.generic_lv);
        genericLv.setAdapter(mSongSourceAdapter);
        genericLv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final SongSource songSource = mSongSourceAdapter.getItem(position);
                mSongSourceAdapter.selectSingle(position);
                genericLv.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        startActivity(SODPSongsActivity.getIntent(SODPlaylistActivity.this, songSource));
                        mSongSourceAdapter.selectSingle(-1);
                    }
                }, 500);
            }
        });

        mSongSourceRequest = new SongSourceRequest(this);
        mSongSourceRequest.setLoader(findViewById(R.id.loading_pb));
        mSongSourceRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    mSongSources = (List<SongSource>) data;
                    mSongSourceAdapter.setList(mSongSources);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                }
            }
        });
        mSongSourceRequest.getSongSourceFromServer();

    }

    public static void setReload(boolean reload) {
        sReload = reload;
    }

    @Override
    public void onResume() {
        if (sReload) {
            sReload = false;
            mSongSourceRequest.getSongSourceFromServer();
        }
        super.onResume();
    }
}
