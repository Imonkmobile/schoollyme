package com.schollyme.songofday;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.adapter.SongAdapter;
import com.schollyme.adapter.SongAdapter.ContentType;
import com.schollyme.handler.DeleteLayout;
import com.schollyme.handler.DeleteLayout.DeleteLayoutListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.ErrorLayoutListener;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Song;
import com.schollyme.model.SongSource;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AddSongRequest;
import com.vinfotech.request.SongListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

public class SODPAllSongsActivity extends BaseSpotifyActivity {

    public static final int REQ_CODE_SODPALL_SONGS_ACTIVITY = 1090;
    private static final String TAG = SODPAllSongsActivity.class.getSimpleName();
    private SwipeRefreshLayout mPullToRefreshSrl;

    private SongSource mSongSource;
    private SongAdapter mSongAdapter;
    private TextView mRightTv;
    private View mEmptyView;
    private ErrorLayout mErrorLayout;
    private DeleteLayout mDeleteLayout;

    private AddSongRequest mAddSongRequest;
    private SongListRequest mSongListRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sod_pallsongs_activity);

        mSongSource = getIntent().getParcelableExtra("songSource");
        if (Config.DEBUG) {
            Log.d("SODPSongsActivity", "onCreate mProvider=" + mSongSource);
        }
        if (null == mSongSource) {
            Toast.makeText(this, "Invalid provider.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        init();

        mSongAdapter = new SongAdapter(this, ContentType.AllSongs, mErrorLayout);
        mPullToRefreshSrl = (SwipeRefreshLayout) findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                callPlayList();
            }
        });
        mEmptyView = findViewById(R.id.empty_tv);

        FontLoader.setRobotoRegularTypeface(mEmptyView);
        View footerView = getLayoutInflater().inflate(R.layout.footer_loader_layout, null);
        footerView.setVisibility(View.INVISIBLE);

        ListView genericLv = (ListView) findViewById(R.id.generic_lv);
        genericLv.addFooterView(footerView);
        genericLv.setAdapter(mSongAdapter);
        genericLv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song song = mSongAdapter.getItem(position);
                if (song.existing) {
                    mErrorLayout.showError(getString(R.string.Song_already_added), true, MsgType.Error);
                    return;
                }
                song.selected = !song.selected;
                mSongAdapter.notifyDataSetChanged();
                updateSelCount();
            }
        });
    }

    public static Intent getIntent(Context context, SongSource songSource) {
        Intent intent = new Intent(context, SODPAllSongsActivity.class);
        intent.putExtra("songSource", songSource);
        return intent;
    }

    private void init() {

        View topView = findViewById(R.id.container_ll);
        HeaderLayout headerLayout = new HeaderLayout(topView);
        headerLayout.setHeaderValues(R.drawable.icon_back, mSongSource.SourceName + " " + getString(R.string.Songs), 0);
        mRightTv = headerLayout.getRightTv();
        headerLayout.setListenerItI(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }, null);

        mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));
        mDeleteLayout = new DeleteLayout(topView);
        mDeleteLayout.setData(R.drawable.ic_add_playlist, R.string.Add_to_Playlist);
        mDeleteLayout.setDeleteLayoutListener(new DeleteLayoutListener() {
            @Override
            public void onDeleteClicked() {
                String message = String.format(getString(R.string.Are_you_sure_add_to_playlist),
                        mSongAdapter.getSelectionCount() + " " + getString(mSongAdapter.getSelectionCount()
                                != 1 ? R.string.songs : R.string.song), mSongSource.SourceName);
                DialogUtil.showOkCancelDialog(SODPAllSongsActivity.this, R.string.Add, R.string.ok_label,
                        R.string.cancel_caps, getString(R.string.Add_Songs), message,
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                List<Song> selections = mSongAdapter.getSelections();
                                mAddSongRequest.addSongInServer(mSongSource.PlayListGUID, selections);
                            }
                        }, null);
            }
        });

        mSongListRequest = new SongListRequest(this);
        mSongListRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    mExistSongs = (List<Song>) data;
                    updateExistingSelection();
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                }
            }
        });

        mSongListRequest.getSongListFromServer(mSongSource.PlayListGUID, "", -1, false);
        mAddSongRequest = new AddSongRequest(this);
        mAddSongRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mDeleteLayout.hideDelete();
                    mSongAdapter.selectSingle(-1);
                    updateSelCount();
                    mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {
                        @Override
                        public void onErrorShown() {
                        }

                        @Override
                        public void onErrorHidden() {
                            finish();
                        }
                    });
                    mErrorLayout.showError(getString(R.string.Song_added_in_playlist), true, MsgType.Success);
                    SODPlaylistActivity.setReload(true);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                }
            }
        });
    }

    private void updateSelCount() {
        if (null != mRightTv) {
            int count = mSongAdapter.getSelectionCount();
            mRightTv.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
            mRightTv.setText(Integer.toString(count));
            if (count > 0) {
                mDeleteLayout.showDelete();
            } else {
                mDeleteLayout.hideDelete();
            }
        }
    }

    private List<Song> mExistSongs, mPSongs;

    private void updateExistingSelection() {
        if (null != mExistSongs && null != mPSongs) {
            for (Song pSong : mPSongs) {
                for (Song eSong : mExistSongs) {
                    String pUrl = (TextUtils.isEmpty(pSong.SongURL) ? pSong.SongPreviewURL : pSong.SongURL);
                    String eUrl = (TextUtils.isEmpty(eSong.SongURL) ? eSong.SongPreviewURL : eSong.SongURL);
                    if (pUrl.equalsIgnoreCase(eUrl)) {
                        pSong.existing = true;
                        break;
                    }
                }
            }
            mSongAdapter.setList(mPSongs);
        }
    }

    @Override
    protected void onSongListReceive(final List<Song> list) {
        mRightTv.post(new Runnable() {
            @Override
            public void run() {
                mEmptyView.setVisibility(list.size() == 0 ? View.VISIBLE : View.GONE);
                mPSongs = list;
                mSongAdapter.setList(mPSongs);
                mPullToRefreshSrl.setRefreshing(false);
                updateExistingSelection();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mDeleteLayout.isShowing()) {
                mDeleteLayout.hideDelete();
            } else {
                this.finish();
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
