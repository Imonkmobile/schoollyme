package com.schollyme.songofday;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.schollyme.R;
import com.schollyme.adapter.SongAdapter;
import com.schollyme.adapter.SongAdapter.ContentType;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.DeleteLayout;
import com.schollyme.handler.DeleteLayout.DeleteLayoutListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.Song;
import com.schollyme.model.SongSource;
import com.vinfotech.request.DeleteSongRequest;
import com.vinfotech.request.SetSongOfDayRequest;
import com.vinfotech.request.SongMainListRequest;
import com.vinfotech.request.SongsPlayListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;

import java.util.ArrayList;
import java.util.List;

public class SODPlaySongsFragment extends BaseFragment {

    private static final String TAG = SODPlaySongsFragment.class.getSimpleName();

    private View mLoaderPb;
    private View mEmptyView;
    private View mFooterView;
    private SwipeRefreshLayout mPullToRefreshSrl;
    private ListView mGenericLv;
    //private SongSource mSongSource;
    private SongAdapter mSongAdapter;
    private ErrorLayout mErrorLayout;
    private DeleteLayout mDeleteLayout;
    private List<Song> mSongs = new ArrayList<Song>();
    private int mPageNo = Config.DEFAULT_PAGE_INDEX;
    private boolean mFetchedAll = true;

    private SongsPlayListRequest mSongsPlayListRequest;
    private SongMainListRequest mSongMainListRequest;
    private DeleteSongRequest mDeleteSongRequest;
    private SetSongOfDayRequest mSetSongOfDayRequest;
    private List<Song> mToDelSongs;
    private Context mContext;
    private LogedInUserModel logedInUserModel;

    public static SODPlaySongsFragment newInstance(ErrorLayout errorLayout) {
        SODPlaySongsFragment mSODPSongsFragment = new SODPlaySongsFragment();
        return mSODPSongsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sod_psongs_activity, null);
        mContext = getActivity();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        logedInUserModel = new LogedInUserModel(mContext);
        mFooterView = getLayoutInflater(savedInstanceState).inflate(R.layout.footer_loader_layout, null);

        mGenericLv = (ListView) view.findViewById(R.id.generic_lv);
        mGenericLv.addFooterView(mFooterView);
        mGenericLv.setAdapter(mSongAdapter);

        mGenericLv.setOnScrollListener(new OnScrollListener() {
            private boolean bReachedListEnd;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (Config.DEBUG) {
                    Log.d(TAG, "onScrollStateChanged bReachedListEnd=" + bReachedListEnd + ", " +
                            "mFetchedAll=" + mFetchedAll + ", scrollState=" + scrollState);
                }
                if (bReachedListEnd && !mFetchedAll) {
                    mFetchedAll = true;
                    mPageNo++;
                    mSongMainListRequest.getSongListFromServer("", "", mPageNo, false);
                    // mSongSource.PlayListGUID
                }
            }

            @Override
            public void onScroll(AbsListView view, final int firstVisibleItem, final int visibleItemCount,
                                 final int totalItemCount) {
                bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);
                if (Config.DEBUG) {
                    Log.d(TAG, "onScroll bReachedListEnd=" + bReachedListEnd + ", firstVisibleItem=" + firstVisibleItem
                            + ", visibleItemCount=" + visibleItemCount + ", totalItemCount=" + totalItemCount);
                }
            }
        });

        init(view);

    }

    public static Intent getIntent(Context context, SongSource songSource) {
        Intent intent = new Intent(context, SODPlaySongsFragment.class);
        intent.putExtra("songSource", songSource);
        return intent;
    }

    private void init(View view) {

        View topView = view.findViewById(R.id.container_ll);
        topView.setVisibility(View.GONE);

        mFooterView.setVisibility(View.INVISIBLE);
        mLoaderPb = view.findViewById(R.id.loader_pb);
        mEmptyView = view.findViewById(R.id.empty_tv);

        mPullToRefreshSrl = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {
                mPageNo = Config.DEFAULT_PAGE_INDEX;
                mSongsPlayListRequest.getSongOfDayFromServer(logedInUserModel.mUserGUID);
            }
        });

        mErrorLayout = new ErrorLayout(view.findViewById(R.id.container_ll));
        mDeleteLayout = new DeleteLayout(topView);
        mDeleteLayout.setData(R.drawable.ic_delete, R.string.Remove);
        mDeleteLayout.setDeleteLayoutListener(new DeleteLayoutListener() {
            @Override
            public void onDeleteClicked() {
                String message = String.format(getString(R.string.Are_you_sure_to_remove_from_playlist),
                        mSongAdapter.getSelectionCount()
                        + " " + getString(mSongAdapter.getSelectionCount() != 1 ? R.string.songs : R.string.song));
                DialogUtil.showOkCancelDialog(mContext, R.string.Remove, R.string.ok_label,
                        R.string.cancel_caps, getString(R.string.Remove_Songs), message,
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDeleteLayout.hideDelete();
                                mToDelSongs = mSongAdapter.getSelections();
                                mDeleteSongRequest.deleteSongFromServer("", mToDelSongs, false); //mSongSource.PlayListGUID
                            }
                        }, null);
            }
        });

        mSongAdapter = new SongAdapter(mContext, ContentType.PlaylistSongs, mErrorLayout);
        mSongAdapter.setDeleteLayout(mDeleteLayout); //playlist

        mSongsPlayListRequest = new SongsPlayListRequest(mContext);
        mSongsPlayListRequest.setLoader(mLoaderPb);
        mSongsPlayListRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                System.out.println(" sond data    "+totalRecords);
                if (success) {
                    initList();
                    Song song = (Song) data;//show song of the day if from same provider
                    if (song.isValid()) {
//                        if (null != song.SongGUID && !song.SourceName.equalsIgnoreCase("")) { //mSongSource.SourceName
//                            mSongs.add(song);
//                        }
                        mSongs.add(song);
                        mSongAdapter.setList(mSongs);
                        SODLast7DayFragment.setReload(true);
                    }
                    mSongAdapter.enableHeader(mSongs.size() > 0);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                    mPullToRefreshSrl.setRefreshing(false);
                }
                mEmptyView.setVisibility(mSongAdapter.getCount() < 1 ? View.VISIBLE : View.GONE);
                if (null != mSongMainListRequest) {
                    mSongMainListRequest.getSongListFromServer("", "", mPageNo, false); //mSongSource.PlayListGUID
                }
            }
        });
        mSongsPlayListRequest.getSongOfDayFromServer(logedInUserModel.mUserGUID);

        mSongMainListRequest = new SongMainListRequest(mContext);
        mSongMainListRequest.setLoader(mPageNo == Config.DEFAULT_PAGE_INDEX ? mLoaderPb : mFooterView);
        mSongMainListRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    List<Song> list = (List<Song>) data;
                    mSongs.addAll(list);
                    mSongAdapter.setList(mSongs);
                    mEmptyView.setVisibility(mSongAdapter.getCount() < 1 ? View.VISIBLE : View.GONE);
                    mFetchedAll = (list.size() < Config.PAGE_SIZE);
                    mFooterView.setVisibility(mFetchedAll ? View.INVISIBLE : View.VISIBLE);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) :
                            (String) data), true, MsgType.Error);
                }
            }
        });

        mDeleteSongRequest = new DeleteSongRequest(mContext);
        mDeleteSongRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(getString(mSongAdapter.getSelectionCount() != 1 ? R.string.Songs : R.string.Song) + " "
                            + getString(R.string.removed_from_playlist), true, MsgType.Success);
                    mSongAdapter.selectSingle(-1);
                    mDeleteLayout.hideDelete();
                    SODPlaylistActivity.setReload(true);

                    if (null != mToDelSongs) {
                        mSongs.removeAll(mToDelSongs);
                    }
                    mSongAdapter.setList(mSongs);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                }
            }
        });

        mSetSongOfDayRequest = new SetSongOfDayRequest(mContext);
        mSetSongOfDayRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (null != mErrorLayout) {
                    if (success) {
                        //mSongOfTheDayRequest.getSongOfDayFromServer("");
                        //    mErrorLayout.showError(getString(R.string.Song_set_asSOD), true, MsgType.Success);
                    } else {
                        mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                                        : (String) data), true, MsgType.Error);
                    }
                }
            }
        });
    }

    private void initList() {

        mSongAdapter = new SongAdapter(mContext, ContentType.PlaylistSongs, mErrorLayout);
        mSongAdapter.setDeleteLayout(mDeleteLayout);
        mGenericLv.setAdapter(mSongAdapter);
        mPageNo = Config.DEFAULT_PAGE_INDEX;
        mSongs.clear();
        mSongAdapter.setList(mSongs);
    }

//    private void showSetSongDialog(final Song song, Activity activity, final ErrorLayout errorLayout) {
//        DialogUtil.showOkCancelDialog(activity, R.string.Set, R.string.ok_label, R.string.cancel_caps,
//                activity.getString(R.string.Set_Song), activity.getString(R.string.Are_you_sure_set_as_SotD),
//                new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        List<Song> songs = new ArrayList<Song>();
//                        songs.add(song);
//                        mSetSongOfDayRequest.setSongOfTheDayInServer("", //mSongSource.PlayListGUID
//                                "", songs); //mSongSource.SourceGUIID
//                    }
//                }, null);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Config.DEBUG) {
            Log.d(TAG, "onActivityResult requestCode =" + requestCode + ", resultCode=" + resultCode);
        }
        switch (requestCode) {
            case SODPAllSongsActivity.REQ_CODE_SODPALL_SONGS_ACTIVITY:
                mSongsPlayListRequest.getSongOfDayFromServer(logedInUserModel.mUserGUID);
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
