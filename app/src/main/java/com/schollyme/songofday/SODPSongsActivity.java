package com.schollyme.songofday;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.adapter.SongAdapter;
import com.schollyme.adapter.SongAdapter.ContentType;
import com.schollyme.handler.DeleteLayout;
import com.schollyme.handler.DeleteLayout.DeleteLayoutListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.Song;
import com.schollyme.model.SongSource;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.DeleteSongRequest;
import com.vinfotech.request.SetSongOfDayRequest;
import com.vinfotech.request.SongListRequest;
import com.vinfotech.request.SongOfTheDayRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;

public class SODPSongsActivity extends BaseActivity {
    private static final String TAG = SODPSongsActivity.class.getSimpleName();

    private View mLoaderPb;
    private View mEmptyView;
    private View mFooterView;
    private SwipeRefreshLayout mPullToRefreshSrl;
    private ListView mGenericLv;

    private SongSource mSongSource;
    private SongAdapter mSongAdapter;
    private ErrorLayout mErrorLayout;
    private DeleteLayout mDeleteLayout;

    private List<Song> mSongs = new ArrayList<Song>();
    private int mPageNo = Config.DEFAULT_PAGE_INDEX;
    private boolean mFetchedAll = true;

    private SongOfTheDayRequest mSongOfTheDayRequest;
    private SongListRequest mSongListRequest;
    private DeleteSongRequest mDeleteSongRequest;
    private SetSongOfDayRequest mSetSongOfDayRequest;
    private List<Song> mToDelSongs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sod_psongs_activity);

        mSongSource = getIntent().getParcelableExtra("songSource");
        if (Config.DEBUG) {
            Log.d("SODPSongsActivity", "onCreate mProvider=" + mSongSource);
        }
        if (null == mSongSource) {
            Toast.makeText(this, "Invalid provider.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        init();

        mGenericLv = (ListView) findViewById(R.id.generic_lv);
        mGenericLv.addFooterView(mFooterView);
        mGenericLv.setAdapter(mSongAdapter);
        mGenericLv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Song song = mSongAdapter.getItem(position);
                if (position > 0 || !mSongAdapter.isEnabledHeader()) {
                    showSetSongDialog(song, SODPSongsActivity.this, mErrorLayout);
                }
            }
        });

        mGenericLv.setOnScrollListener(new OnScrollListener() {
            private boolean bReachedListEnd;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (Config.DEBUG) {
                    Log.d(TAG, "onScrollStateChanged bReachedListEnd=" + bReachedListEnd + ", " +
                            "mFetchedAll=" + mFetchedAll + ", scrollState=" + scrollState);
                }
                if (bReachedListEnd && !mFetchedAll) {
                    mFetchedAll = true;
                    mPageNo++;
                    mSongListRequest.getSongListFromServer(mSongSource.PlayListGUID, "", mPageNo, false);
                }
            }

            @Override
            public void onScroll(AbsListView view, final int firstVisibleItem, final int visibleItemCount,
                                 final int totalItemCount) {
                bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);
                if (Config.DEBUG) {
                    Log.d(TAG, "onScroll bReachedListEnd=" + bReachedListEnd + ", firstVisibleItem=" + firstVisibleItem
                            + ", visibleItemCount=" + visibleItemCount + ", totalItemCount=" + totalItemCount);
                }
            }
        });

        LogedInUserModel logedInUserModel = new LogedInUserModel(this);
        final String screenName = ("SODPSongsActivity_" + mSongSource.SourceName).trim();
        if (!logedInUserModel.getIsVisitScreen(this, screenName)) {
            logedInUserModel.setIsVisitScreen(this, screenName, true);
        }
    }

    public static Intent getIntent(Context context, SongSource songSource) {
        Intent intent = new Intent(context, SODPSongsActivity.class);
        intent.putExtra("songSource", songSource);
        return intent;
    }

    private void init() {

        ImageView addSongsImg = (ImageView) findViewById(R.id.addSongsImg);
        addSongsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(SODPAllSongsActivity.getIntent(v.getContext(), mSongSource),
                        SODPAllSongsActivity.REQ_CODE_SODPALL_SONGS_ACTIVITY);
            }
        });

        View topView = findViewById(R.id.container_ll);
        HeaderLayout headerLayout = new HeaderLayout(topView);
        // only spotify allows to add and list songs
        if (getString(R.string.Spotify).equalsIgnoreCase(mSongSource.SourceName)) {
            headerLayout.setHeaderValues(R.drawable.icon_back, mSongSource.SourceName + " " + getString(R.string.Songs), 0);
            addSongsImg.setVisibility(View.VISIBLE);
        } else {
            headerLayout.setHeaderValues(R.drawable.icon_back, mSongSource.SourceName + " " + getString(R.string.Songs), 0);
            addSongsImg.setVisibility(View.GONE);
        }

        headerLayout.setListenerItI(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }, null);



        mFooterView = getLayoutInflater().inflate(R.layout.footer_loader_layout, null);
        mFooterView.setVisibility(View.INVISIBLE);
        mLoaderPb = findViewById(R.id.loader_pb);
        mEmptyView = findViewById(R.id.empty_tv);
        FontLoader.setRobotoRegularTypeface(mEmptyView);

        mPullToRefreshSrl = (SwipeRefreshLayout) findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {
                mPageNo = Config.DEFAULT_PAGE_INDEX;
                mSongOfTheDayRequest.getSongOfDayFromServer("");
            }
        });

        mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));

        mDeleteLayout = new DeleteLayout(topView);
        mDeleteLayout.setData(R.drawable.ic_delete, R.string.Remove);
        mDeleteLayout.setDeleteLayoutListener(new DeleteLayoutListener() {

            @Override
            public void onDeleteClicked() {
                String message = String.format(getString(R.string.Are_you_sure_to_remove_from_playlist),
                        mSongAdapter.getSelectionCount()
                        + " " + getString(mSongAdapter.getSelectionCount() != 1 ? R.string.songs : R.string.song));
                DialogUtil.showOkCancelDialog(SODPSongsActivity.this, R.string.Remove, R.string.ok_label,
                        R.string.cancel_caps, getString(R.string.Remove_Songs), message,
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDeleteLayout.hideDelete();
                                mToDelSongs = mSongAdapter.getSelections();
                                mDeleteSongRequest.deleteSongFromServer(mSongSource.PlayListGUID, mToDelSongs, false);
                            }
                        }, null);
            }
        });

        mSongAdapter = new SongAdapter(this, ContentType.PlaylistSongs, mErrorLayout);
        mSongAdapter.setDeleteLayout(mDeleteLayout);

        mSongOfTheDayRequest = new SongOfTheDayRequest(this);
        mSongOfTheDayRequest.setLoader(mLoaderPb);
        mSongOfTheDayRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    initList();
                    Song song = (Song) data;//show song of the day if from same provider
                    if (song.isValid()) {
                        if (null != song.SongGUID && song.SourceName.equalsIgnoreCase(mSongSource.SourceName)) {
                            mSongs.add(song);
                        }
                        mSongAdapter.setList(mSongs);
                        SODLast7DayFragment.setReload(true);
                    }
                    mSongAdapter.enableHeader(mSongs.size() > 0);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                    mPullToRefreshSrl.setRefreshing(false);
                }
                mEmptyView.setVisibility(mSongAdapter.getCount() < 1 ? View.VISIBLE : View.GONE);
                if (null != mSongListRequest) {
                    mSongListRequest.getSongListFromServer(mSongSource.PlayListGUID, "", mPageNo, false);
                }
            }
        });
        mSongOfTheDayRequest.getSongOfDayFromServer("");

        mSongListRequest = new SongListRequest(this);
        mSongListRequest.setLoader(mPageNo == Config.DEFAULT_PAGE_INDEX ? mLoaderPb : mFooterView);
        mSongListRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    List<Song> list = (List<Song>) data;
                    mSongs.addAll(list);
                    mSongAdapter.setList(mSongs);
                    mEmptyView.setVisibility(mSongAdapter.getCount() < 1 ? View.VISIBLE : View.GONE);
                    mFetchedAll = (list.size() < Config.PAGE_SIZE);
                    mFooterView.setVisibility(mFetchedAll ? View.INVISIBLE : View.VISIBLE);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) :
                            (String) data), true, MsgType.Error);
                }
            }
        });

        mDeleteSongRequest = new DeleteSongRequest(this);
        mDeleteSongRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(getString(mSongAdapter.getSelectionCount() != 1 ? R.string.Songs : R.string.Song) + " "
                            + getString(R.string.removed_from_playlist), true, MsgType.Success);
                    mSongAdapter.selectSingle(-1);
                    mDeleteLayout.hideDelete();
                    SODPlaylistActivity.setReload(true);

                    if (null != mToDelSongs) {
                        mSongs.removeAll(mToDelSongs);
                    }
                    mSongAdapter.setList(mSongs);
                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                }
            }
        });

        mSetSongOfDayRequest = new SetSongOfDayRequest(this);
        mSetSongOfDayRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (null != mErrorLayout) {
                    if (success) {
                        mSongOfTheDayRequest.getSongOfDayFromServer("");
                        //    mErrorLayout.showError(getString(R.string.Song_set_asSOD), true, MsgType.Success);
                    } else {
                        mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                                        : (String) data), true, MsgType.Error);
                    }
                }
            }
        });
    }

    private void initList() {

        mSongAdapter = new SongAdapter(this, ContentType.PlaylistSongs, mErrorLayout);
        mSongAdapter.setDeleteLayout(mDeleteLayout);
        mGenericLv.setAdapter(mSongAdapter);
        mPageNo = Config.DEFAULT_PAGE_INDEX;
        mSongs.clear();
        mSongAdapter.setList(mSongs);
    }

    private void showSetSongDialog(final Song song, Activity activity, final ErrorLayout errorLayout) {
        DialogUtil.showOkCancelDialog(activity, R.string.Set, R.string.ok_label, R.string.cancel_caps,
                activity.getString(R.string.Set_Song), activity.getString(R.string.Are_you_sure_set_as_SotD),
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<Song> songs = new ArrayList<Song>();
                        songs.add(song);
                        mSetSongOfDayRequest.setSongOfTheDayInServer(mSongSource.PlayListGUID,
                                mSongSource.SourceGUIID, songs);
                    }
                }, null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Config.DEBUG) {
            Log.d(TAG, "onActivityResult requestCode =" + requestCode + ", resultCode=" + resultCode);
        }
        switch (requestCode) {
            case SODPAllSongsActivity.REQ_CODE_SODPALL_SONGS_ACTIVITY:
                mSongOfTheDayRequest.getSongOfDayFromServer("");
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mDeleteLayout.isShowing()) {
                mDeleteLayout.hideDelete();
            } else {
                this.finish();
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
