package com.schollyme.songofday;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.schollyme.activity.BaseActivity;
import com.schollyme.R;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.header.HeaderLayout;

public class UserSongsActivity extends BaseActivity {
	private String mUserGUID = "", mUserName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_songs_activity);

		getIntentData();

		HeaderLayout headerLayout = new HeaderLayout(findViewById(R.id.header_layout));
		headerLayout.setHeaderValues(R.drawable.icon_back, mUserName + "'s " + getResources().getString(R.string.Songs), 0);
		headerLayout.setListenerItI(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setResult(RESULT_OK);
				finish();
			}
		}, null);

		getSupportFragmentManager().beginTransaction().add(R.id.container_review, UserSongsFragment.newInstance(mUserGUID)).commit();
	}

	public static boolean isMe(String UserGUID, LogedInUserModel logedInUserModel) {
		if (null != UserGUID && null != logedInUserModel) {
			return UserGUID.trim().equalsIgnoreCase(logedInUserModel.mUserGUID.trim());
		}
		return false;
	}

	private void getIntentData() {
		mUserGUID = getIntent().getStringExtra("USERID");
		mUserName = getIntent().getStringExtra("USERNAME");
	}

	public static Intent getIntent(Context context, String mUserGUID, String mUserName) {
		Intent intent = new Intent(context, UserSongsActivity.class);
		intent.putExtra("USERID", mUserGUID);
		intent.putExtra("USERNAME", mUserName);
		return intent;
	}
}
