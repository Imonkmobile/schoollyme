package com.schollyme;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.schollyme.activity.BaseActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.ErrorLayoutListener;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.vinfotech.utility.Config;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class SharePostActivity extends BaseActivity implements OnClickListener {

    private static final String TAG = SharePostActivity.class.getSimpleName();
    public static final int REQ_CODE_SHARE_POST_ACTIVITY = 1033;

    private TextView mPrivacyTv, mDescTv;
    private EditText mPostEt;
    private ImageView mMediaIv;

    private SharePostRequest mSharePostRequest;
    private HeaderLayout mHeaderLayout;
    private ErrorLayout mErrorLayout;

    private int mVisibility = WallPostCreateRequest.VISIBILITY_PUBLIC;
    private int mCommentable = WallPostCreateRequest.COMMENT_ON;
    private String mEntityGUID;
    private String mEntityType;
    private String mModuleEntityGUID;
    private String mMediaUrl;
    private String mCaption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_post_activity);

        mEntityGUID = getIntent().getStringExtra("entityGUID");
        mEntityType = getIntent().getStringExtra("entityType");
        mModuleEntityGUID = getIntent().getStringExtra("moduleEntityGUID");
        mMediaUrl = getIntent().getStringExtra("mediaUrl");
        mCaption = getIntent().getStringExtra("caption");

        if (Config.DEBUG) {
            Log.d("CreateWallPostActivity", "onCreate mEntityGUID="
                    + mEntityGUID + ", mEntityType=" + mEntityType
                    + ", mModuleEntityGUID=" + mModuleEntityGUID
                    + ", mMediaUrl=" + mMediaUrl + ", mCaption=" + mCaption);
        }
        if (null == mEntityType || null == mModuleEntityGUID || null == mMediaUrl) {
            Toast.makeText(this, "Invalid media or entity.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mHeaderLayout = new HeaderLayout(findViewById(R.id.container_ll));
        mHeaderLayout.setHeaderValues(R.drawable.ic_close, getResources()
                .getString(R.string.Post), R.drawable.icon_arrw_right_active_white_xhdpi);
        mHeaderLayout.setListenerItI(this, this);
        mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));

        mSharePostRequest = new SharePostRequest(this);
        mSharePostRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, final Object data, int totalRecords) {

                if (success) {
                    mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {
                        @Override
                        public void onErrorShown() {
                        }

                        @Override
                        public void onErrorHidden() {
                            mHeaderLayout.mRightIb1.setEnabled(true);
                            setResultAndFinish(data);
                        }
                    });
                    mErrorLayout.showError("Media shared successfully", true, MsgType.Success);
                } else {
                    mHeaderLayout.mRightIb1.setEnabled(true);
                    mErrorLayout.showError((String) data, true, MsgType.Error);
                }
            }
        });

        mPrivacyTv = (TextView) findViewById(R.id.privacy_tv);
        findViewById(R.id.visibility_ll).setOnClickListener(this);

        mPostEt = (EditText) findViewById(R.id.post_et);
        mMediaIv = (ImageView) findViewById(R.id.media_iv);
        mMediaIv.setOnClickListener(this);
        mDescTv = (TextView) findViewById(R.id.desc_tv);

        FontLoader.setRobotoRegularTypeface(mPrivacyTv, mPostEt);

        ImageLoader.getInstance().displayImage(mMediaUrl, mMediaIv);
        mDescTv.setText(mCaption);
    }

    public static Intent getIntent_(Context context, String entityGUID, String entityType, String moduleEntityGUID,
                                    String mediaUrl, String caption) {
        Intent intent = new Intent(context, SharePostActivity.class);
        intent.putExtra("entityGUID", entityGUID);
        intent.putExtra("entityType", entityType);
        intent.putExtra("moduleEntityGUID", moduleEntityGUID);
        intent.putExtra("mediaUrl", mediaUrl);
        intent.putExtra("caption", caption);
        return intent;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_button:
                Utility.hideSoftKeyboard(mPostEt);
                setResultAndFinish(null);
                break;
            case R.id.right_button:

                try {
                    InputMethodManager inputManager = (InputMethodManager) this
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(mPostEt.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                } catch (Exception e) {
                    // TODO: handle exception
                }
                mHeaderLayout.mRightIb1.setEnabled(false);
                mSharePostRequest.sharePostInServer(mEntityGUID, mEntityType,
                        mPostEt.getText().toString().trim(),
                        SharePostRequest.MODULE_ID_USERS, mModuleEntityGUID,
                        mVisibility, mCommentable);
                break;
            case R.id.visibility_ll:
                DialogUtil.showListDialog(v.getContext(), R.string.Album_Visibility,
                        new OnItemClickListener() {
                            @Override
                            public void onItemClick(int position, String item) {
                                mPrivacyTv.setText(item);
                                if (position == 0) {
                                    mVisibility = SharePostRequest.VISIBILITY_EVERYONE;
                                } else if (position == 1) {
                                    mVisibility = SharePostRequest.VISIBILITY_TEAMMATES;
                                }
                            }

                            @Override
                            public void onCancel() {

                            }
                        }, getString(R.string.Public), getString(R.string.Teammates));
                break;

            default:
                break;
        }
    }

    private void setResultAndFinish(Object object) {
        Intent data = new Intent();
        if (null != object) {
            setResult(RESULT_OK, data);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }
}
