package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class AlbumMedia implements Parcelable {
	public static final String MEDIA_TYPE_UNKNOWN = "", MEDIA_TYPE_IMAGE = "Image", MEDIA_TYPE_VIDEO = "Video",
			MEDIA_TYPE_YOUTUBE = "Youtube";

	public String MediaGUID;
	public String ImageName;
	public String Caption;
	public int NoOfComments;
	public int NoOfLikes;
	public String CreatedDate;
	public int IsCoverMedia;
	public String MediaType;
	public String ImageServerPath;
	public boolean isSelected;
	public boolean isFirst;
	public int IsLike;
	public int VideoLength;
	public int ViewCount;
	public String ConversionStatus;
	public String tmpThumb;
	public String mAlbumGUID;
	public String fullCoverPath;
	public int IsFlagged;// ":0,
	public int FlagAllowed;// ":"1",
	public int Flaggable;// ":0,

	public AlbumMedia(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			this.MediaGUID = jsonObject.optString("MediaGUID", "");
			this.ImageName = jsonObject.optString("ImageName", "");
			this.Caption = jsonObject.optString("Caption", "");
			this.NoOfComments = jsonObject.optInt("NoOfComments", 0);
			this.NoOfLikes = jsonObject.optInt("NoOfLikes", 0);
			this.CreatedDate = jsonObject.optString("CreatedDate", "");
			this.IsCoverMedia = jsonObject.optInt("IsCoverMedia", 0);
			this.MediaType = jsonObject.optString("MediaType", "");
			this.ImageServerPath = jsonObject.optString("ImageServerPath", "");
			this.MediaType = jsonObject.optString("MediaType", MEDIA_TYPE_UNKNOWN);
			this.VideoLength = jsonObject.optInt("VideoLength", 0);
			this.ViewCount = jsonObject.optInt("ViewCount", 0);
			this.ConversionStatus = jsonObject.optString("ConversionStatus", "");
			this.mAlbumGUID = jsonObject.optString("AlbumGUID", "");
			this.isSelected = false;
			this.isFirst = false;
			this.IsLike = jsonObject.optInt("IsLike", 0);
			IsFlagged = jsonObject.optInt("IsFlagged", 0);
			FlagAllowed = jsonObject.optInt("FlagAllowed", 0);
			Flaggable = jsonObject.optInt("Flaggable", 0);
		}
	}

	public AlbumMedia(String mediaGUID, String imageName, String caption) {
		super();
		MediaGUID = mediaGUID;
		ImageName = imageName;
		Caption = caption;
	}

	public AlbumMedia setDummyData() {
		ImageName = "http://freephotos.atguru.in/hdphotos/best-nature-wallpapers/nature-wallpapers-2.jpg";
		Random r = new Random();
		NoOfLikes = r.nextInt(100);
		NoOfComments = r.nextInt(100);
		return this;
	}

	public static List<AlbumMedia> getDummyMedias(int count) {
		List<AlbumMedia> list = new ArrayList<AlbumMedia>();
		for (int i = 0; i < count; i++) {
			list.add(new AlbumMedia((JSONObject) null).setDummyData());
		}
		return list;
	}

	public AlbumMedia(boolean first, String type) {
		this.isFirst = first;
		this.MediaType = type;
	}

	public AlbumMedia(String MediaGUID) {
		this.MediaGUID = MediaGUID;
	}

	public JSONObject toDelJSONObject() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("MediaGUID", MediaGUID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public static JSONArray getDelJSONArray(List<AlbumMedia> list) {
		JSONArray jsonArray = new JSONArray();
		if (null != list) {
			for (AlbumMedia albumMedia : list) {
				jsonArray.put(albumMedia.toDelJSONObject());
			}
		}
		return jsonArray;
	}

	public static List<AlbumMedia> getAlbumMedias(JSONArray jsonArray) {
		List<AlbumMedia> list = new ArrayList<AlbumMedia>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new AlbumMedia(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public AlbumMedia(Parcel in) {
		MediaGUID = in.readString();
		ImageName = in.readString();
		Caption = in.readString();
		NoOfComments = in.readInt();
		NoOfLikes = in.readInt();
		CreatedDate = in.readString();
		IsCoverMedia = in.readInt();
		MediaType = in.readString();
		ImageServerPath = in.readString();
		isSelected = in.readInt() == 1;
		
		IsLike = in.readInt();
		VideoLength = in.readInt();
		ViewCount = in.readInt();
		ConversionStatus = in.readString();
		mAlbumGUID = in.readString();
		fullCoverPath = in.readString();
		IsFlagged = in.readInt();
		FlagAllowed = in.readInt();
		Flaggable = in.readInt();
	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(MediaGUID);
		dest.writeString(ImageName);
		dest.writeString(Caption);
		dest.writeInt(NoOfComments);
		dest.writeInt(NoOfLikes);
		dest.writeString(CreatedDate);
		dest.writeInt(IsCoverMedia);
		dest.writeString(MediaType);
		dest.writeString(ImageServerPath);
		dest.writeInt(isSelected ? 1 : 0);
		dest.writeInt(IsLike);
		dest.writeInt(VideoLength);
		dest.writeInt(ViewCount);
		dest.writeString(ConversionStatus);
		dest.writeString(mAlbumGUID);
		dest.writeString(fullCoverPath);
		dest.writeInt(IsFlagged);
		dest.writeInt(FlagAllowed);
		dest.writeInt(Flaggable);
	}

	public boolean isConverted() {
		return "Finished".equalsIgnoreCase(ConversionStatus);
	}

	public static final Creator CREATOR = new Creator() {
		public AlbumMedia createFromParcel(Parcel in) {
			return new AlbumMedia(in);
		}

		public AlbumMedia[] newArray(int size) {
			return new AlbumMedia[size];
		}
	};

	public static String getVideoDuration(int ms) {
		int seconds = ms / 1000;
		int minutes = seconds / 60;
		int hours = minutes / 60;
		if (hours == 0) {
			return twoDigit(minutes) + ":" + twoDigit(seconds % 60);
		}
		return hours + ":" + twoDigit(minutes % 60) + ":" + twoDigit(seconds % 60);
	}

	private static String twoDigit(int digit) {
		if (digit < 10) {
			return "0" + digit;
		}
		return "" + digit;
	}

	@Override
	public boolean equals(Object o) {
		if (null != o && o instanceof AlbumMedia) {
			return MediaGUID.equalsIgnoreCase(((AlbumMedia) o).MediaGUID);
		}
		return super.equals(o);
	};

	@Override
	public String toString() {
		return "AlbumMedia{" +
				"MediaGUID='" + MediaGUID + '\'' +
				", ImageName='" + ImageName + '\'' +
				", Caption='" + Caption + '\'' +
				", NoOfComments=" + NoOfComments +
				", NoOfLikes=" + NoOfLikes +
				", CreatedDate='" + CreatedDate + '\'' +
				", IsCoverMedia=" + IsCoverMedia +
				", MediaType='" + MediaType + '\'' +
				", ImageServerPath='" + ImageServerPath + '\'' +
				", isSelected=" + isSelected +
				", isFirst=" + isFirst +
				", IsLike=" + IsLike +
				", VideoLength=" + VideoLength +
				", ViewCount=" + ViewCount +
				", ConversionStatus='" + ConversionStatus + '\'' +
				", tmpThumb='" + tmpThumb + '\'' +
				", mAlbumGUID='" + mAlbumGUID + '\'' +
				", fullCoverPath='" + fullCoverPath + '\'' +
				", IsFlagged=" + IsFlagged +
				", FlagAllowed=" + FlagAllowed +
				", Flaggable=" + Flaggable +
				'}';
	}
}
