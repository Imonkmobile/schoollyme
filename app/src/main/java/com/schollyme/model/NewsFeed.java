package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NewsFeed {
	public int IsEntityOwner;// ":1,
	public int IsOwner;// ":1,
	public int IsFlagged;// ":0,
	public int CanShowSettings;// ":1,
	public int CanRemove;// ":1,
	public int CanMakeSticky;// ":1,
	public int ShowPrivacy;// ":1,
	public int PostAsEntityOwner;// ":0,
	public String ActivityGUID;// ":"41f61497-1760-bc00-aab7-ac0415bbf786",
	public String ModuleID;// ":"3",
	public String UserGUID;// ":"e028d83c-352b-77e6-acaf-a44cd9fa836f",
	public String ActivityType;// ":"PostSelf",
	public int NoOfFavourites;// ":"1",
	public int NoOfComments;// ":5,
	public int NoOfLikes;// ":1,
	public String Message;// ":"{{SUBJECT}}",
	public int CommentsAllowed;// ":1,
	public int LikeAllowed;// ":"1",
	public int FlagAllowed;// ":"1",
	public int ShareAllowed;// ":0,
	public int FavouriteAllowed;// ":"1",
	public String CreatedDate;// ":"2015-05-27 05:09:02",
	public String ModifiedDate;// ":"2015-06-04 04:50:23",
	public int IsSticky;// ":"1",
	public int Visibility;// ":"1",
	public String PostContent;// ":"edit single tage {{2:Sunil}}\u00ad",
	public Album Album;// ":{},
	public int Count = 1;;// ":{},
	public int IsSubscribed;// ":1,
	public int IsFavourite;// ":"1",
	public int Viewed;// ":1,
	public int Flaggable;// ":0,
	public int FlaggedByAny;// ":0,
	public String EntityName;// ":"Suresh Patidar",
	public String EntityProfilePicture;// ":"439ecbeaeece74546297d681fef7170b.png",
	public String UserName;// ":"Suresh Patidar",
	public String UserProfilePicture;// ":"439ecbeaeece74546297d681fef7170b.png",
	public String UserProfileURL;// ":"suresh",
	public String EntityProfileURL;// ":"suresh",
	public String ActivityOwner;// ":"Suresh Patidar",
	public String ActivityOwnerLink;// ":"suresh",
	public String SharePostContent;// ":"Test share",
	public String ExtraLine;// ":"",
	public LikerModel LikeName;// ":{},
	public int IsLike;// ":1,
	public List<CommentModel> CommentModels;// ":[]
	public String EntityType;
	public String EntityGUID;
	public boolean Mybuzz;

	public NewsFeed(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			IsEntityOwner = jsonObject.optInt("IsEntityOwner", 0);
			IsOwner = jsonObject.optInt("IsOwner", 0);
			IsFlagged = jsonObject.optInt("IsFlagged", 0);
			CanShowSettings = jsonObject.optInt("CanShowSettings", 0);
			CanRemove = jsonObject.optInt("CanRemove", 0);
			CanMakeSticky = jsonObject.optInt("CanMakeSticky", 0);
			ShowPrivacy = jsonObject.optInt("ShowPrivacy", 0);
			PostAsEntityOwner = jsonObject.optInt("PostAsEntityOwner", 0);
			ActivityGUID = jsonObject.optString("ActivityGUID", "");
			ModuleID = jsonObject.optString("ModuleID", "");
			UserGUID = jsonObject.optString("UserGUID", "");
			ActivityType = jsonObject.optString("ActivityType", "");
			NoOfFavourites = jsonObject.optInt("NoOfFavourites", 0);
			NoOfComments = jsonObject.optInt("NoOfComments", 0);
			NoOfLikes = jsonObject.optInt("NoOfLikes", 0);
			Message = jsonObject.optString("Message", "");
			CommentsAllowed = jsonObject.optInt("CommentsAllowed", 0);
			LikeAllowed = jsonObject.optInt("LikeAllowed", 0);
			FlagAllowed = jsonObject.optInt("FlagAllowed", 0);
			ShareAllowed = jsonObject.optInt("ShareAllowed", 0);
			FavouriteAllowed = jsonObject.optInt("FavouriteAllowed", 0);
			CreatedDate = jsonObject.optString("CreatedDate", "");
			ModifiedDate = jsonObject.optString("ModifiedDate", "");
			IsSticky = jsonObject.optInt("IsSticky", 0);
			Visibility = jsonObject.optInt("Visibility", 0);
			PostContent = jsonObject.optString("PostContent", "");
			JSONObject albumJSONObject = jsonObject.optJSONObject("Album");
			Album = new Album(albumJSONObject);
			if (null != albumJSONObject) {
				Album.AlbumMedias = AlbumMedia.getAlbumMedias(albumJSONObject.optJSONArray("Media"));
			}
			JSONObject kk = jsonObject.optJSONObject("Params");
			if (null != kk) {

				Count = jsonObject.optJSONObject("Params").optInt("count");
			}

			IsSubscribed = jsonObject.optInt("IsSubscribed", 0);
			IsFavourite = jsonObject.optInt("IsFavourite", 0);
			Viewed = jsonObject.optInt("Viewed", 0);
			Flaggable = jsonObject.optInt("Flaggable", 0);
			FlaggedByAny = jsonObject.optInt("FlaggedByAny", 0);
			EntityName = jsonObject.optString("EntityName", "");
			EntityProfilePicture = jsonObject.optString("EntityProfilePicture", "");
			UserName = jsonObject.optString("UserName", "");
			UserProfilePicture = jsonObject.optString("UserProfilePicture", "");
			UserProfileURL = jsonObject.optString("UserProfileURL", "");
			EntityProfileURL = jsonObject.optString("EntityProfileURL", "");
			ActivityOwner = jsonObject.optString("ActivityOwner", "");
			ActivityOwnerLink = jsonObject.optString("ActivityOwnerLink", "");
			SharePostContent = jsonObject.optString("SharePostContent", "");
			ExtraLine = jsonObject.optString("ExtraLine", "");
			LikeName = new LikerModel(jsonObject.optJSONObject("LikeName"));
			IsLike = jsonObject.optInt("IsLike", 0);
			CommentModels = CommentModel.getComments(jsonObject.optJSONArray("Comments"));
			EntityType = jsonObject.optString("EntityType", "");
			EntityGUID = jsonObject.optString("EntityGUID", "");
			Mybuzz = jsonObject.optInt("MyBuzz", 0) == 1;
		}
	}

	public NewsFeed setDummyData() {
		UserProfilePicture = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnEqtar7oFabYJVa-eJ9tXi2QSz0mls79_kHG3MFMyL9DMOkyXng";
		Random r = new Random();
		CanRemove = r.nextInt(2);
		NoOfLikes = r.nextInt(100);
		NoOfComments = r.nextInt(100);
		Message = "DUMMY_MESSAGE";
		UserGUID = "DUMMY_GUID";
		Album = new Album("Dummy");
		Album.AlbumMedias = AlbumMedia.getDummyMedias(r.nextInt(5));
		return this;
	}

	public static List<NewsFeed> getDummyNewsFeeds(int count) {
		List<NewsFeed> list = new ArrayList<NewsFeed>();
		for (int i = 0; i < count; i++) {
			list.add(new NewsFeed((JSONObject) null).setDummyData());
		}
		return list;
	}

	public static List<NewsFeed> getNewsFeeds(JSONArray jsonArray) {
		List<NewsFeed> list = new ArrayList<NewsFeed>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new NewsFeed(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	@Override
	public String toString() {
		return "NewsFeed [IsEntityOwner=" + IsEntityOwner + ", IsOwner=" + IsOwner + ", IsFlagged=" + IsFlagged + ", CanShowSettings="
				+ CanShowSettings + ", CanRemove=" + CanRemove + ", CanMakeSticky=" + CanMakeSticky + ", ShowPrivacy=" + ShowPrivacy
				+ ", PostAsEntityOwner=" + PostAsEntityOwner + ", ActivityGUID=" + ActivityGUID + ", ModuleID=" + ModuleID + ", UserGUID="
				+ UserGUID + ", ActivityType=" + ActivityType + ", NoOfFavourites=" + NoOfFavourites + ", NoOfComments=" + NoOfComments
				+ ", NoOfLikes=" + NoOfLikes + ", Message=" + Message + ", CommentsAllowed=" + CommentsAllowed + ", LikeAllowed="
				+ LikeAllowed + ", FlagAllowed=" + FlagAllowed + ", ShareAllowed=" + ShareAllowed + ", FavouriteAllowed="
				+ FavouriteAllowed + ", CreatedDate=" + CreatedDate + ", ModifiedDate=" + ModifiedDate + ", IsSticky=" + IsSticky
				+ ", Visibility=" + Visibility + ", PostContent=" + PostContent + ", Album=" + Album + ", Count=" + Count
				+ ", IsSubscribed=" + IsSubscribed + ", IsFavourite=" + IsFavourite + ", Viewed=" + Viewed + ", Flaggable=" + Flaggable
				+ ", FlaggedByAny=" + FlaggedByAny + ", EntityName=" + EntityName + ", EntityProfilePicture=" + EntityProfilePicture
				+ ", UserName=" + UserName + ", UserProfilePicture=" + UserProfilePicture + ", UserProfileURL=" + UserProfileURL
				+ ", EntityProfileURL=" + EntityProfileURL + ", ActivityOwner=" + ActivityOwner + ", ActivityOwnerLink="
				+ ActivityOwnerLink + ", SharePostContent=" + SharePostContent + ", ExtraLine=" + ExtraLine + ", LikeName=" + LikeName
				+ ", IsLike=" + IsLike + ", CommentModels=" + CommentModels + ", EntityType=" + EntityType + ", EntityGUID=" + EntityGUID
				+ ", Mybuzz=" + Mybuzz + "]";
	}

}
