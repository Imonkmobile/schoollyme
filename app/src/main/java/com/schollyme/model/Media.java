package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Media implements Parcelable {
	public static final int ZENCODER_JOB_STATUS_CONVERTING = 0, ZENCODER_JOB_STATUS_CONVERTED = 1;
	public static final int MEDIA_TYPE_UNKNOWN = -1, MEDIA_TYPE_IMAGE = 0, MEDIA_TYPE_VIDEO = 1, MEDIA_TYPE_YOUTUBE = 3;

	public String mediaID;
	public String title;
	public String description;
	public String fileName;
	public String mimeType;
	public String size;
	public String zencoder_job_id;
	public int zencoder_job_status;
	public int mediaType;
	public String src;
	public String src_large;
	public String caption;

	public Media(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			this.mediaID = jsonObject.optString("mediaID", "0");
			this.title = jsonObject.optString("title", "");
			this.description = jsonObject.optString("description", "");
			this.fileName = jsonObject.optString("ImageName", "");
			this.mimeType = jsonObject.optString("mimeType", "");
			this.size = jsonObject.optString("size", "0");
			this.zencoder_job_id = jsonObject.optString("zencoder_job_id", "0");
			this.zencoder_job_status = jsonObject.optInt("zencoder_job_status", ZENCODER_JOB_STATUS_CONVERTING);
			this.mediaType = jsonObject.optInt("mediaType", MEDIA_TYPE_UNKNOWN);
			this.src = jsonObject.optString("src", "");
			this.src_large = jsonObject.optString("src_large", "");
			this.caption = jsonObject.optString("caption", "");
		}
	}

	public static List<Media> getMedias(JSONArray jsonArray) {
		List<Media> list = new ArrayList<Media>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new Media(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public Media(Parcel in) {
		mediaID = in.readString();
		title = in.readString();
		description = in.readString();
		fileName = in.readString();
		mimeType = in.readString();
		size = in.readString();
		zencoder_job_id = in.readString();
		zencoder_job_status = in.readInt();
		mediaType = in.readInt();
		src = in.readString();
		src_large = in.readString();
		caption = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mediaID);
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(fileName);
		dest.writeString(mimeType);
		dest.writeString(size);
		dest.writeString(zencoder_job_id);
		dest.writeInt(zencoder_job_status);
		dest.writeInt(mediaType);
		dest.writeString(src);
		dest.writeString(src_large);
		dest.writeString(caption);
	}

	public static final Creator CREATOR = new Creator() {
		public Media createFromParcel(Parcel in) {
			return new Media(in);
		}

		public Media[] newArray(int size) {
			return new Media[size];
		}
	};

	@Override
	public String toString() {
		return "Media [mediaID=" + mediaID + ", title=" + title + ", description=" + description + ", fileName=" + fileName + ", mimeType="
				+ mimeType + ", size=" + size + ", zencoder_job_id=" + zencoder_job_id + ", zencoder_job_status=" + zencoder_job_status
				+ ", mediaType=" + mediaType + ", src=" + src + ", src_large=" + src_large + ", caption=" + caption + "]";
	}

}
