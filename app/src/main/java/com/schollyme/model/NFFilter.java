package com.schollyme.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.annotation.SuppressLint;
import com.vinfotech.utility.Config;
import com.vinfotech.request.NewsFeedRequest;

/**
 * Class to build filter for news feed
 * 
 * @author ramanands
 *
 */
public class NFFilter {
	@SuppressLint("SimpleDateFormat")
	public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public int ModuleID;
	public String EntityGUID;
	public int FeedSortBy;
	public int PageNo;
	public int AllActivity;
	public int ActivityFIlterType;
	public int IsMediaExists;
	public String FeedUser;
	public Calendar StartDate;
	public Calendar EndDate;
	public int AsOwner;
	public String ActivityGUID;
	public boolean MyBuzz;
	public boolean MyKiltd;



	public NFFilter(int ModuleID, String EntityGUID) {
		this.ModuleID = ModuleID;
		this.EntityGUID = EntityGUID;

		init();
	}

	public void init() {
		this.FeedSortBy = NewsFeedRequest.FEED_SHORT_BY_MOST_RECENT;
		this.PageNo = Config.DEFAULT_PAGE_INDEX;
		this.AllActivity = NewsFeedRequest.ALL_ACTIVITY_NONE;
		this.ActivityFIlterType = NewsFeedRequest.ACTIVITY_FILTER_TYPE_ALL;
		this.IsMediaExists = NewsFeedRequest.IS_MEDIA_EXISTS_NONE;
		this.FeedUser = null;
		this.StartDate = null;
		this.EndDate = null;
		this.AsOwner = NewsFeedRequest.AS_OWNER_NOT_NONE;
		this.ActivityGUID = null;
		this.MyBuzz = false;
		this.MyKiltd = false;
	}

	public String getStartDate() {
		if (null != StartDate) {
			return SIMPLE_DATE_FORMAT.format(StartDate.getTime());
		}
		return null;
	}

	public String getEndDate() {
		if (null != EndDate) {
			return SIMPLE_DATE_FORMAT.format(EndDate.getTime());
		}
		return null;
	}

	@Override
	public String toString() {
		return "NFFilter [ModuleID=" + ModuleID + ", EntityGUID=" + EntityGUID + ", FeedSortBy=" + FeedSortBy + ", PageNo=" + PageNo
				+ ", AllActivity=" + AllActivity + ", ActivityFIlterType=" + ActivityFIlterType + ", IsMediaExists=" + IsMediaExists
				+ ", FeedUser=" + FeedUser + ", StartDate=" + StartDate + ", EndDate=" + EndDate + ", AsOwner=" + AsOwner
				+ ", ActivityGUID=" + ActivityGUID + ", MyBuzz=" + MyBuzz + ", MyKiltd=" + MyKiltd + "]";
	}

}
