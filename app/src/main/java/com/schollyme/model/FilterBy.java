package com.schollyme.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class FilterBy implements Parcelable {

    public static final String TYPE_ALL = "All";
    public static final String TYPE_FRIENDS = "Friends";
    public static final String TYPE_TEAMMATE = "TeamMate";
    public static final String TYPE_INCOMINGREQUEST = "IncomingRequest";
    public static final String TYPE_OUTGOINGREQUEST = "OutgoingRequest";
    public static final String TYPE_FOLLOWERS = "Followers";
    public static final String TYPE_FOLLOWING = "Following";

    public static final String GENDER_MALE = "1";
    public static final String GENDER_FEMALE = "2";
    public static final String GENDER_OTHER = "3";
    public static final String GENDER_ALL = "";
    public static final String FAV_ALL = "";

    public static final String USERTYPEID_COACH = "1";
    public static final String USERTYPEID_ATHELETE = "2";
    public static final String USERTYPEID_FAN = "3";
    public static final String USERTYPEID_ALL = "";

    private String mSearchKeyword;
    private List<SportsModel> mSportsModels;
    private String mType;
    private List<StateModel> mStateModels;
    private List<FavTeamModel> mFavTeamModals;

    private String mGender;
    private String mGPA = "";
    private String mFavoriteTeam = "";
    private String mSATScore = "";
    private String mUserTypeID = "";
    private String mRecruiting = "";
    private int mVerified = 0;
    // private List<String> mPositions;
    private FilterListener mFilterListener;

    public static final String GPA_ALL = "";
    public static final String GPA_2_5 = "2.5";
    public static final String GPA_3_0 = "3.0";
    public static final String GPA_3_5 = "3.5";
    public static final String GPA_4_0 = "4.0";

    public static final String SAT_ALL = "";
    public static final String SAT_1000 = "1000";
    public static final String SAT_1500 = "1500";
    public static final String SAT_2000 = "2000";

    public static final String ACT_ALL = "";
    public static final String ACT_15 = "15";
    public static final String ACT_20 = "20";
    public static final String ACT_25 = "25";
    public static final String ACT_30 = "30";

    public FilterBy() {
        super();
        this.mSearchKeyword = "";
        this.mSportsModels = new ArrayList<SportsModel>();
        this.mType = TYPE_ALL;
        this.mStateModels = new ArrayList<StateModel>();
        this.mFavTeamModals = new ArrayList<FavTeamModel>();
        this.mGender = GENDER_ALL;
        this.mGPA = "";
        this.mFavoriteTeam = "";
        this.mSATScore = "";
        this.mUserTypeID = USERTYPEID_ALL;
        this.mRecruiting = "";
        this.mVerified = 0;
        // this.mPositions = new ArrayList<String>();
    }

    // public void setPositionValue(String newPosition) {
    // if (null != newPosition && !newPosition.equals("")) {
    //
    // this.mPositions.add(newPosition);
    // }
    //
    // }
    //
    // public void removePositions(int position) {
    // this.mPositions.remove(position);
    // }
    //
    // public List<String> getPositions() {
    // return mPositions;
    // }
    //
    // public void setPositions(List<String> list) {
    // mPositions.clear();
    // mPositions.addAll(list);
    // }

    public boolean isDefault() {

        boolean result;
        if (mSportsModels.size() > 0) {
            result = false;
        } else if (!mType.equals(TYPE_ALL)) {
            result = false;
        } else if (mStateModels.size() > 0) {
            result = false;
        } else if(mFavTeamModals.size()>0){
            result = false;
        } else if (!mGender.equals(GENDER_ALL)) {
            result = false;
        } else if (!mFavoriteTeam.equals(FAV_ALL)) {
            result = false;
        } else if (!mGPA.equals("")) {
            result = false;
        } else if (!mSATScore.equals("")) {
            result = false;
        } else if (!mUserTypeID.equals(USERTYPEID_ALL)) {
            result = false;
        } else if (!mRecruiting.equals("")) {
            result = false;
        } else if (mVerified != 0) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }

    public void setSearchKeyword(String search) {
        mSearchKeyword = search;
    }

    public void setSports(List<SportsModel> list) {
        mSportsModels.clear();
        mSportsModels.addAll(list);
    }

    public void setType(String type) {
        mType = type;
    }

    public void setStates(List<StateModel> list) {
        mStateModels.clear();
        mStateModels.addAll(list);
    }

    public void setFavTeam(List<FavTeamModel> list) {
        if(mFavTeamModals==null){
            mFavTeamModals = new ArrayList<FavTeamModel>();
        }
        mFavTeamModals.clear();
        mFavTeamModals.addAll(list);
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public void setGPA(String gpa) {
        mGPA = gpa;
    }




    public void setSATScore(String score) {
        mSATScore = score;
    }

    public void setUserTypeId(String userTypeId) {
        mUserTypeID = userTypeId;
    }

    public void setRecruitingClass(String score) {
        mRecruiting = score;
    }

    public void setVerified(int score) {
        mVerified = score;
    }

    public String getSearchKeyword() {
        return mSearchKeyword;
    }

    public List<SportsModel> getSportsModels() {
        return mSportsModels;
    }

    public String getType() {
        return mType;
    }

    public List<StateModel> getStateModels() {
        return mStateModels;
    }


    public List<FavTeamModel> getmFavTeamModals() {
        return mFavTeamModals;
    }

    public String getGender() {
        return mGender;
    }

    public String getGPA() {
        return mGPA;
    }

    public String getFavoriteTeam() {
        return mFavoriteTeam;
    }

    public String getSATScore() {
        return mSATScore;
    }

    public String getUserTypeId() {
        return mUserTypeID;
    }

    public String getRecruitingClass() {
        return mRecruiting;
    }

    public int getVerified() {
        return mVerified;
    }


    public void clearAll() {

        this.mSportsModels.clear();
        this.mType = TYPE_ALL;
        this.mStateModels.clear();
        this.mFavTeamModals.clear();
        this.mGender = GENDER_ALL;
        this.mGPA = "";
        this.mSATScore = "";
        this.mUserTypeID = USERTYPEID_ALL;
        this.mRecruiting = "";
        this.mVerified = 0;

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void clearSports() {
        this.mSportsModels.clear();

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void clearType() {
        this.mType = TYPE_ALL;

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void clearStates() {
        this.mStateModels.clear();

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void clearFavTeam() {
        this.mFavTeamModals.clear();

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void clearGender() {
        this.mGender = GENDER_ALL;

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    // public void clearGPA() {
    // this.mGPA = "";
    //
    // if (null != mFilterListener) {
    // mFilterListener.onFilterChange(this);
    // }
    // }

    public void clearSATScore() {
        this.mSATScore = "";

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void clearUserTypeId() {
        this.mUserTypeID = USERTYPEID_ALL;

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void clearRecruiting() {
        this.mRecruiting = "";

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void clearVerified() {
        this.mVerified = 0;

        if (null != mFilterListener) {
            mFilterListener.onFilterChange(this);
        }
    }

    public void setFilterListener(FilterListener listener) {
        mFilterListener = listener;
    }

    public interface FilterListener {
        void onFilterChange(FilterBy filterBy);
    }

    public FilterBy(Parcel in) {
        mSearchKeyword = in.readString();
        mSportsModels = in.createTypedArrayList(SportsModel.CREATOR);
        mType = in.readString();
        mStateModels = in.createTypedArrayList(StateModel.CREATOR);
        mFavTeamModals = in.createTypedArrayList(FavTeamModel.CREATOR);
        mGender = in.readString();
        mGPA = in.readString();
        mSATScore = in.readString();
        mUserTypeID = in.readString();
        mRecruiting = in.readString();
        mVerified = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mSearchKeyword);
        dest.writeTypedList(mSportsModels);
        dest.writeString(mType);
        dest.writeTypedList(mStateModels);
        dest.writeTypedList(mFavTeamModals);
        dest.writeString(mGender);
        dest.writeString(mGPA);
        dest.writeString(mSATScore);
        dest.writeString(mUserTypeID);
        dest.writeString(mRecruiting);
        dest.writeInt(mVerified);
    }

    public int describeContents() {
        return 0;
    }

    public static final Creator CREATOR = new Creator() {
        public FilterBy createFromParcel(Parcel in) {
            return new FilterBy(in);
        }

        public FilterBy[] newArray(int size) {
            return new FilterBy[size];
        }
    };

    @Override
    public String toString() {
        return "FilterBy [mSearchKeyword=" + mSearchKeyword
                + ", mSportsModels=" + mSportsModels + ", mType=" + mType
                + ", mStateModels=" + mStateModels + ", mGender=" + mGender
                + ", mGPA=" + mGPA + ", =" + ", mSATScore=" + mSATScore
                + ", mUserTypeID=" + mUserTypeID + ", mRecruiting=" + mRecruiting
                + ", mVerified=" + mVerified
                + ", mFilterListener=" + mFilterListener + "]";
    }

}
