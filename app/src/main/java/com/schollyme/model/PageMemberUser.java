package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PageMemberUser {
	
	public String mUserGUID;
	public String mUserFName;
	public String mUserLName;
	public String mUserProfileLink;
	public String mUserProflePic;
	public String mUserTypeId;
	public String mModuleRoleId;
	public boolean isSelected;
	
	public PageMemberUser(JSONObject mJsonObject){
		try {
			this.mUserGUID = mJsonObject.optString("UserGUID");
			this.mUserFName = mJsonObject.optString("FirstName");
			this.mUserLName = mJsonObject.optString("LastName");
			this.mUserProfileLink = mJsonObject.optString("ProfileURL");
			this.mUserProflePic = mJsonObject.optString("ProfilePicture");
			this.mUserTypeId = mJsonObject.optString("UserTypeID");
			this.mModuleRoleId = mJsonObject.optString("ModuleRoleID");
			this.isSelected = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<PageMemberUser> getPageInviteMembers(JSONArray jsonArray) {
		List<PageMemberUser> list = new ArrayList<PageMemberUser>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new PageMemberUser(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}