package com.schollyme.model;

import java.util.ArrayList;

public class LeftMenuModel {
	
	public String mOptionMenuName;
	public int mOptionMenuCount;
	
	public LeftMenuModel(String option){
		this.mOptionMenuName = option;
		this.mOptionMenuCount = 0;
	}
	
	public static ArrayList<LeftMenuModel> getLeftMenus(ArrayList<String> options){
		ArrayList<LeftMenuModel> mMenuList = new ArrayList<LeftMenuModel>();
		for(int i=0;i<options.size();i++){
			mMenuList.add(new LeftMenuModel(options.get(i)));
		}
		return mMenuList;
	}

}
