package com.schollyme.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

public class CreatedBy {
	public String UserGUID;
	public String UserName;
	public String ProfilePicture;
	public String ProfileURL;
	public int UserTypeID;

	public CreatedBy(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			this.UserGUID = jsonObject.optString("UserGUID", "");
			this.UserName = jsonObject.optString("UserName", "");
			this.ProfilePicture = jsonObject.optString("ProfilePicture", "");
			this.ProfileURL = jsonObject.optString("ProfileURL", "");
			this.UserTypeID = jsonObject.optInt("UserTypeID", 0);
		}
	}

	public JSONObject toJSONObject() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("UserGUID", TextUtils.isEmpty(UserGUID) ? "" : UserGUID);
			jsonObject.put("UserName", TextUtils.isEmpty(UserName) ? "" : UserName);
			jsonObject.put("ProfilePicture", TextUtils.isEmpty(ProfilePicture) ? "" : ProfilePicture);
			jsonObject.put("ProfileURL", TextUtils.isEmpty(ProfileURL) ? "" : ProfileURL);
			jsonObject.put("UserTypeID", UserTypeID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
}
