package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CommentModel {
	public String PlaceholderImg;
	public String ProfilePicture;
	public String CommentGUID;
	public String UserGUID;
	public String PostComment;
	public String Name;
	public String CreatedDate;
	public int CanDelete;
	public String ProfileLink;
	public int IsLike;
	public int NoOfLikes;
	public int IsMediaExists;
	public int ModuleID;
	public List<AlbumMedia> AlbumMedias;
	

	public CommentModel(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			PlaceholderImg = jsonObject.optString("PlaceholderImg", "");
			ProfilePicture = jsonObject.optString("ProfilePicture", "");
			CommentGUID = jsonObject.optString("CommentGUID", "");
			UserGUID = jsonObject.optString("UserGUID", "");
			PostComment = jsonObject.optString("PostComment", "");
			Name = jsonObject.optString("Name", "");
			CreatedDate = jsonObject.optString("CreatedDate", "");
			CanDelete = jsonObject.optInt("CanDelete", 0);
			ProfileLink = jsonObject.optString("ProfileLink", "");
			IsLike = jsonObject.optInt("IsLike", 0);
			NoOfLikes = jsonObject.optInt("NoOfLikes", 0);
			IsMediaExists = jsonObject.optInt("IsMediaExists", 0);
			ModuleID = jsonObject.optInt("ModuleID", 0);
		}
	}

	public static List<CommentModel> getComments(JSONArray jsonArray) {
		List<CommentModel> list = new ArrayList<CommentModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new CommentModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	@Override
	public String toString() {
		return "CommentModel [PlaceholderImg=" + PlaceholderImg + ", ProfilePicture=" + ProfilePicture + ", CommentGUID=" + CommentGUID
				+ ", UserGUID=" + UserGUID + ", PostComment=" + PostComment + ", Name=" + Name + ", CreatedDate=" + CreatedDate
				+ ", CanDelete=" + CanDelete + ", ProfileLink=" + ProfileLink + ", IsLike=" + IsLike + ", NoOfLikes=" + NoOfLikes
				+ ", IsMediaExists=" + IsMediaExists + ", ModuleID=" + ModuleID + ", AlbumMedias=" + AlbumMedias + "]";
	}

}
