package com.schollyme.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InboxModel implements Serializable{
	
	public String mMessageGUID;
	public String mSubject;
	public String mCreatedDate;
	public String mModifiedDate;
	public String mBody;
	public String mUnreadCount;
	public String mStatus;
	public String mSenderUserProfileURL;
	public String mSenderUserTypeId;
	public String mReceiverUserProfileURL;
	public String mReceiverUserTypeId;
	public String mReceiverUserFName;
	public String mReceiverUserLName;
	public String mReceiverProfilePicture;
	public String mReceiverUserGUID;
	public String mSenderUserFName;
	public String mSenderUserLName;
	public String mSenderProfilePicture;
	public String mSenderUserGUID;
	
	public InboxModel(JSONObject mJsonObj){
		try {
			this.mMessageGUID =mJsonObj.optString("MessageGUID");
			this.mSubject =mJsonObj.optString("Subject");
			this.mCreatedDate =mJsonObj.optString("CreatedDate");
			this.mModifiedDate =mJsonObj.optString("ModifiedDate");
			this.mBody =mJsonObj.optString("Body");
			this.mUnreadCount =mJsonObj.optString("UnreadCount");
			this.mStatus =mJsonObj.optString("Status");
			
			JSONArray jArray = mJsonObj.optJSONArray("Users");
			if(jArray!=null){
		//		int size = jArray.length();
				
				JSONObject friendData = jArray.optJSONObject(0);
				this.mSenderUserFName =friendData.optString("FirstName");
				this.mSenderUserLName =friendData.optString("LastName");
				this.mSenderProfilePicture =friendData.optString("ProfilePicture");
				this.mSenderUserGUID =friendData.optString("UserGUID");
				this.mSenderUserProfileURL = friendData.optString("ProfileURL");
				this.mSenderUserTypeId = friendData.optString("UserTypeID");
				
				JSONObject myData = jArray.optJSONObject(1);
				this.mReceiverUserFName =myData.optString("FirstName");
				this.mReceiverUserLName =myData.optString("LastName");
				this.mReceiverProfilePicture =myData.optString("ProfilePicture");
				this.mReceiverUserGUID =myData.optString("UserGUID");
				this.mReceiverUserProfileURL = myData.optString("ProfileURL");
				this.mReceiverUserTypeId = myData.optString("UserTypeID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<InboxModel> getMessage(JSONArray jsonArray) {
		List<InboxModel> list = new ArrayList<InboxModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new InboxModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	public static List<InboxModel> getMessageDetail(JSONObject jsonObj) {
		List<InboxModel> list = new ArrayList<InboxModel>();
		if (jsonObj != null) {
			JSONArray jsonArray = jsonObj.optJSONArray("Messages");
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new InboxModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}