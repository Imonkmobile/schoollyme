package com.schollyme.model;

import android.util.Log;

import junit.framework.Test;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ravib on 11/18/2015.
 */
public class TestModel {

    public String BlogID;
    public String Title;
    public String Content;
    public String Date;
    public String Excerpt;

    public TestModel(JSONObject json){
        this.BlogID  = json.optString("ID");
        this.Title  = json.optString("title");
        this.Content  = json.optString("content");
        this.Date  = json.optString("date");
        this.Excerpt  = json.optString("excerpt");
    }

    public static ArrayList<TestModel> getModels(String result){
        ArrayList<TestModel> models = new ArrayList<TestModel>();
        try{
            JSONArray jArray = new JSONArray(result);
            Log.i("Response ", "Data : " + jArray.length());
            for(int i=0;i<jArray.length();i++){
                models.add(new TestModel(jArray.optJSONObject(i)));
                /*Log.i("Response ", "Object contains : "+jArray.optJSONObject(i));
                Log.i("Response ", ">>>>>>>>>>><<<<<<<<<<");*/
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return models;
    }

}
