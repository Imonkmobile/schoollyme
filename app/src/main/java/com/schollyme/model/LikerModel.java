package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LikerModel {
	public String FirstName;
	public String LastName;
	public String ProfilePicture;
	public String CityName;
	public String CountryName;
	public String ProfileLink;
	public String UserGUID;
	public int IsLiked;

	public LikerModel(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			FirstName = jsonObject.optString("FirstName", "");
			LastName = jsonObject.optString("LastName", "");
			ProfilePicture = jsonObject.optString("ProfilePicture", "");
			CityName = jsonObject.optString("CityName", "");
			CountryName = jsonObject.optString("CountryName", "");
			ProfileLink = jsonObject.optString("ProfileLink", "");
			UserGUID = jsonObject.optString("UserGUID", "");
			IsLiked = jsonObject.optInt("IsLiked");
		}
	}

	public static List<LikerModel> getLikers(JSONArray jsonArray) {
		List<LikerModel> list = new ArrayList<LikerModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new LikerModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	@Override
	public String toString() {
		return "LikerModel [FirstName=" + FirstName + ", LastName=" + LastName + ", ProfilePicture=" + ProfilePicture + ", CityName="
				+ CityName + ", CountryName=" + CountryName + ", ProfileLink=" + ProfileLink + ", UserGUID=" + UserGUID + ", IsLiked="
				+ IsLiked + "]";
	}

}
