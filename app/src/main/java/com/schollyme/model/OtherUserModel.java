package com.schollyme.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.text.TextUtils;

public class OtherUserModel {

	public String mUserName;
	public String mUserPic;
	public String mUserCoverPic;
	public String mUserDetail;
	public String mUserAddress;
	public String mUserFollowCount;
	public boolean mIsFollow;
	public String mUserType;
	public ArrayList<Map<String, String>> mSelectedCategoryAL;
	public ArrayList<Map<String, String>> mFollowersAL;

	public OtherUserModel(JSONObject mJson){
		if(null!=mJson){
			this.mUserName = mJson.optString("Name");
			this.mUserPic = mJson.optString("UserPic");
			this.mUserCoverPic = mJson.optString("CoverPic");
			this.mUserDetail = mJson.optString("Detail");
			this.mUserAddress = mJson.optString("Address");
			this.mUserFollowCount = mJson.optString("FollowerCount");
			this.mUserType = mJson.optString("UserType");
			String flag = mJson.optString("IsFollow");
			if(TextUtils.isEmpty(flag)){
				this.mIsFollow = false;
			}
			else if(flag.equalsIgnoreCase("True")){
				this.mIsFollow = true;
			}
			else{
				this.mIsFollow = false;
			}
			getCategoryOfIntrest(mJson.optJSONArray("Categories"));
			getFollowers(mJson.optJSONArray("Followers"));
		}
	}

	private void getCategoryOfIntrest(JSONArray jArray){
		try {
			ArrayList<Map<String,String>> categoryAL = new ArrayList<Map<String,String>>();
			if(null!=jArray && jArray.length()>0){
				for(int i=0;i<jArray.length();i++){
					Map<String, String> cMap = new HashMap<String, String>();
					cMap.put("CategoryName", jArray.getJSONObject(i).optString("CategoryName"));
					cMap.put("CategoryId", jArray.getJSONObject(i).optString("CategoryId"));
					categoryAL.add(cMap);
				}
				mSelectedCategoryAL = categoryAL;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void getFollowers(JSONArray jArray){
		try {
			ArrayList<Map<String,String>> followersAL = new ArrayList<Map<String,String>>();
			if(null!=jArray && jArray.length()>0){
				for(int i=0;i<jArray.length();i++){
					Map<String, String> cMap = new HashMap<String, String>();
					cMap.put("FollowerPic", jArray.getJSONObject(i).optString("FollowerPic"));
					cMap.put("FollowerId", jArray.getJSONObject(i).optString("FollowerId"));
					cMap.put("UserType", jArray.getJSONObject(i).optString("UserType"));
					cMap.put("Description", jArray.getJSONObject(i).optString("Description"));
					cMap.put("ContractorProfileHref", jArray.getJSONObject(i).optString("ContractorProfileHref"));
					followersAL.add(cMap);
				}
				mFollowersAL = followersAL;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}