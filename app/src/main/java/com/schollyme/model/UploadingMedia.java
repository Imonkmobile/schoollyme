package com.schollyme.model;

public class UploadingMedia {
	public String name;
	public String path;

	public UploadingMedia(String name, String path) {
		super();
		this.name = name;
		this.path = path;
	}

	@Override
	public String toString() {
		return "UploadingMedia [name=" + name + ", path=" + path + "]";
	}
}
