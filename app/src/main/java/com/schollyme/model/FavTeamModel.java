package com.schollyme.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FavTeamModel implements Parcelable {
	public String mUserID;
	public String mFavoriteTeam;
	public boolean isSelected = false;

	public FavTeamModel(JSONObject mJson) {
		if (mJson != null) {
			this.mUserID = mJson.optString("UserID");
			this.mFavoriteTeam = mJson.optString("name");
		}
	}

	public FavTeamModel(JSONObject mJson, boolean flag) {
		if (mJson != null) {
			this.mUserID = mJson.optString("UserID");
			this.mFavoriteTeam = mJson.optString("name");
		}
	}

	public JSONObject toJSONObject() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("UserID", TextUtils.isEmpty(mUserID) ? "" : mUserID);
			jsonObject.put("name", TextUtils.isEmpty(mFavoriteTeam) ? "" : mFavoriteTeam);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public static List<FavTeamModel> getSportsModels(JSONArray jsonArray) {
		List<FavTeamModel> list = new ArrayList<FavTeamModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new FavTeamModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public static JSONArray toJSONArray(List<FavTeamModel> list) {
		JSONArray jsonArray = new JSONArray();
		if (null != list) {
			for (FavTeamModel sportsModel : list) {
				jsonArray.put(sportsModel.mFavoriteTeam);
			}
		}
		return jsonArray;
	}

	public FavTeamModel(Parcel in) {
		mUserID = in.readString();
		mFavoriteTeam = in.readString();
		isSelected = (in.readInt() == 1);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mUserID);
		dest.writeString(mFavoriteTeam);
		dest.writeInt(isSelected ? 1 : 0);
	}

	public int describeContents() {
		return 0;
	}

	public static final Creator CREATOR = new Creator() {
		public FavTeamModel createFromParcel(Parcel in) {
			return new FavTeamModel(in);
		}

		public FavTeamModel[] newArray(int size) {
			return new FavTeamModel[size];
		}
	};

	@Override
	public String toString() {
		return "SportsModel [mSportsID=" + mUserID + ", mSportsName=" + mFavoriteTeam + ", isSelected=" + isSelected + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (null != o && o instanceof FavTeamModel) {
			return this.mUserID.equalsIgnoreCase(((FavTeamModel) o).mUserID);
		}
		return super.equals(o);
	}

}