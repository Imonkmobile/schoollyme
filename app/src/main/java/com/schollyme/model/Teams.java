package com.schollyme.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Teams implements Serializable{

	public String mTeamGUID;
	public String mTeamName;
	public String mTeamSportsName;
	public String mTeamSportsId;
	public String mTeamImageURL;
	public String mTeamCoverImageURL;
	public String mAdminUserGUID;
	public String mAdminUserProfilePicURL;
	public String mAdminUserFName;
	public String mAdminUserLName;
	public String mAdminUserTypeID;
	public String mAdminUserProfileLinkURL;
	public boolean isGroupAdmin;
	public boolean isGroupMember;
	public String isPublic;
	public String mStatusID;
	public String mPageCategoryName;
	public String mPageCategoryID;
	public String mPageDescription;
	public String mTeamMembersCount;
	public String mPageConferenceName;
	public String mPageConferenceId;
	public String mPageCollegeId;
	public String mPageCollegeName;

	public Teams(JSONObject mJsonObj){
		try {
			this.mTeamGUID = mJsonObj.optString("GroupGUID");
			this.mTeamName = mJsonObj.optString("GroupName");
			this.mTeamMembersCount = mJsonObj.optString("MemberCount");
			this.mPageDescription = mJsonObj.optString("GroupDescription");
			this.mTeamImageURL = mJsonObj.optString("ProfilePicture");
			this.mTeamCoverImageURL = mJsonObj.optString("CoverPicture");
			this.isPublic = mJsonObj.optString("IsPublic");
			this.mStatusID = mJsonObj.optString("StatusID");

			String mAdmin = mJsonObj.optString("ModuleRoleID");
			if(mAdmin.equals("4") || mAdmin.equals("5")){
				this.isGroupAdmin = true;
				this.isGroupMember = true;
			}
			else if(mAdmin.equals("6")){
				this.isGroupMember = true;
				this.isGroupAdmin = false;
			}
			else {
				this.isGroupAdmin = false;
				this.isGroupMember = false;
			}
			JSONArray categoryArray = mJsonObj.optJSONArray("Category");
			if(categoryArray!=null){
				JSONObject categoryJson = categoryArray.optJSONObject(0);
				this.mPageCategoryName = categoryJson.optString("Name");
				this.mPageCategoryID = categoryJson.optString("CategoryID");
			}

			JSONArray adminArray = mJsonObj.optJSONArray("CreatedBy");
			if(adminArray!=null){
				JSONObject adminJson = adminArray.optJSONObject(0);
				this.mAdminUserGUID = adminJson.optString("UserGUID");
				this.mAdminUserTypeID = adminJson.optString("UserTypeID");
				this.mAdminUserFName = adminJson.optString("FirstName");
				this.mAdminUserLName = adminJson.optString("LastName");
				this.mAdminUserProfilePicURL = adminJson.optString("ProfilePicture");
				this.mAdminUserProfileLinkURL = adminJson.optString("ProfileURL");
			}

			JSONArray sportsArray = mJsonObj.optJSONArray("Sport");
			if(null!=sportsArray){
				this.mTeamSportsName = sportsArray.optJSONObject(0).optString("Name");
				this.mTeamSportsId = sportsArray.optJSONObject(0).optString("SportsID");
			}
			else {
				this.mTeamSportsName = mJsonObj.optString("Sport");
			}

			JSONArray ConferenceArray = mJsonObj.optJSONArray("Conference");
			if(null!=ConferenceArray){
				this.mPageConferenceId = ConferenceArray.optJSONObject(0).optString("ConferenceID");
				this.mPageConferenceName = ConferenceArray.optJSONObject(0).optString("Name");
			}
			else {
				this.mPageConferenceName = mJsonObj.optString("Conference");
			}
			
			JSONArray collegeArray = mJsonObj.optJSONArray("University");
			if(null!=collegeArray){
				this.mPageCollegeId = collegeArray.optJSONObject(0).optString("UniversityID");
				this.mPageCollegeName = collegeArray.optJSONObject(0).optString("Name");
			}
			else {
				this.mPageCollegeName = mJsonObj.optString("University");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<Teams> getTeams(JSONArray jsonArray) {
		List<Teams> list = new ArrayList<Teams>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new Teams(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public static Teams getTeam(JSONObject jsonObject) {
		if (jsonObject != null) {
			try {
				return new Teams(jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "Teams [mTeamGUID=" + mTeamGUID + ", mTeamName=" + mTeamName
				+ ", mTeamSportsName=" + mTeamSportsName + ", mTeamSportsId="
				+ mTeamSportsId + ", mTeamImageURL=" + mTeamImageURL
				+ ", mTeamCoverImageURL=" + mTeamCoverImageURL
				+ ", mAdminUserGUID=" + mAdminUserGUID
				+ ", mAdminUserProfilePicURL=" + mAdminUserProfilePicURL
				+ ", mAdminUserFName=" + mAdminUserFName + ", mAdminUserLName="
				+ mAdminUserLName + ", mAdminUserTypeID=" + mAdminUserTypeID
				+ ", mAdminUserProfileLinkURL=" + mAdminUserProfileLinkURL
				+ ", isGroupAdmin=" + isGroupAdmin + ", isGroupMember="
				+ isGroupMember + ", isPublic=" + isPublic + ", mStatusID="
				+ mStatusID + ", mPageCategoryName=" + mPageCategoryName
				+ ", mPageCategoryID=" + mPageCategoryID
				+ ", mPageDescription=" + mPageDescription
				+ ", mTeamMembersCount=" + mTeamMembersCount
				+ ", mPageConferenceName=" + mPageConferenceName
				+ ", mPageConferenceId=" + mPageConferenceId
				+ ", mPageCollegeId=" + mPageCollegeId + ", mPageCollegeName="
				+ mPageCollegeName + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (null != o && o instanceof Teams) {
			return this.mTeamGUID.equalsIgnoreCase(((Teams) o).mTeamGUID);
		}
		return super.equals(o);
	}


}