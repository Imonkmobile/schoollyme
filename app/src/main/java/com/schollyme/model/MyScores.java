package com.schollyme.model;

import android.text.TextUtils;

import org.json.JSONObject;

public class MyScores {

	public String mGPAScore;
	public String mGPAVerificationDate;
	public String mGPAVerificationStatus;
	public String mGPAPaymentStatus;
	public String mGPAPaymentDate;

	public String mSATScore;
	public String mSATVerificationDate;
	public String mSATVerificationStatus;
	public String mSATPaymentStatus;
	public String mSATPaymentDate;

	public String mACTScore;
	public String mACTVerificationDate;
	public String mACTVerificationStatus;
	public String mACTPaymentStatus;
	public String mACTPaymentDate;

	public String mTranscriptsScore;
	public String mTranscriptsVerificationDate;
	public String mTranscriptsVerificationStatus;
	public String mTranscriptsPaymentStatus;
	public String mTranscriptsPaymentDate;

	public MyScores(JSONObject mJsonObj){
		try {
			JSONObject mACTJson = mJsonObj.optJSONObject("ACT");
			JSONObject mGPAJson = mJsonObj.optJSONObject("GPA");
			JSONObject mSATJson = mJsonObj.optJSONObject("SAT");
			JSONObject mTRANSCRIPTSJson = mJsonObj.optJSONObject("TRANSCRIPT_UPLOAD");

			if(null!= mACTJson){
				this.mACTScore = mACTJson.optString("Score");
				this.mACTVerificationDate = mACTJson.optString("VerificationDate");
				this.mACTVerificationStatus = mACTJson.optString("VerificationStatus");
				this.mACTPaymentStatus = mACTJson.optString("PaymentStatus");
				this.mACTPaymentDate = mACTJson.optString("PaymentDate");
			}
			if(null!= mGPAJson){
				this.mGPAScore = mGPAJson.optString("Score");
				if(!TextUtils.isEmpty(mGPAScore) && mGPAScore.startsWith("0")){
					mGPAScore = mGPAScore.substring(1);
				}
				if(mGPAScore.equals(".00")){
					mGPAScore = "";
				}
				this.mGPAVerificationDate = mGPAJson.optString("VerificationDate");
				this.mGPAVerificationStatus = mGPAJson.optString("VerificationStatus");
				this.mGPAPaymentStatus = mGPAJson.optString("PaymentStatus");
				this.mGPAPaymentDate = mGPAJson.optString("PaymentDate");
			}
			if(null!= mSATJson){
				this.mSATScore = mSATJson.optString("Score");
				this.mSATVerificationDate = mSATJson.optString("VerificationDate");
				this.mSATVerificationStatus = mSATJson.optString("VerificationStatus");
				this.mSATPaymentStatus = mSATJson.optString("PaymentStatus");
				this.mSATPaymentDate = mSATJson.optString("PaymentDate");
			}
			if(null!= mTRANSCRIPTSJson){
				this.mTranscriptsScore = mTRANSCRIPTSJson.optString("Score");
				this.mTranscriptsVerificationDate = mTRANSCRIPTSJson.optString("VerificationDate");
				this.mTranscriptsVerificationStatus = mTRANSCRIPTSJson.optString("VerificationStatus");
				this.mTranscriptsPaymentStatus = mTRANSCRIPTSJson.optString("PaymentStatus");
				this.mTranscriptsPaymentDate = mTRANSCRIPTSJson.optString("PaymentDate");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static MyScores getMyScore(JSONObject mJson){
		try {
			
			return	new MyScores(mJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toString() {
		return "MyScores [mGPAScore=" + mGPAScore + ", mGPAVerificationDate="
				+ mGPAVerificationDate + ", mGPAVerificationStatus="
				+ mGPAVerificationStatus + ", mGPAPaymentStatus="
				+ mGPAPaymentStatus + ", mGPAPaymentDate=" + mGPAPaymentDate
				+ ", mSATScore=" + mSATScore + ", mSATVerificationDate="
				+ mSATVerificationDate + ", mSATVerificationStatus="
				+ mSATVerificationStatus + ", mSATPaymentStatus="
				+ mSATPaymentStatus + ", mSATPaymentDate=" + mSATPaymentDate
				+ ", mACTScore=" + mACTScore + ", mACTVerificationDate="
				+ mACTVerificationDate + ", mACTVerificationStatus="
				+ mACTVerificationStatus + ", mACTPaymentStatus="
				+ mACTPaymentStatus + ", mACTPaymentDate=" + mACTPaymentDate
				+ ", mTranscriptsScore=" + mTranscriptsScore
				+ ", mTranscriptsVerificationDate="
				+ mTranscriptsVerificationDate
				+ ", mTranscriptsVerificationStatus="
				+ mTranscriptsVerificationStatus
				+ ", mTranscriptsPaymentStatus=" + mTranscriptsPaymentStatus
				+ ", mTranscriptsPaymentDate=" + mTranscriptsPaymentDate + "]";
	}



}
