package com.schollyme.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;


public class FriendModel implements  Serializable{

	public String mFirstName;
	public String mLastName;
	public String mProfilePicture;
	public String mUserGUID;
	public String mUserTypeID;
	public String mProfileLink;
	public String mFriendStatus = "4";
	public String mUserIsTeammate = "0";
	public String mShowFriendsBtn;
	public boolean isRequestSent = false;
	
	public FriendModel(){}

	public FriendModel(JSONObject jObj) {
		if(null!=jObj){
			this.mFirstName = jObj.optString("FirstName");
			this.mLastName = jObj.optString("LastName");
			this.mProfilePicture = jObj.optString("ProfilePicture");
			this.mUserGUID = jObj.optString("UserGUID");
			this.mUserTypeID = jObj.optString("UserTypeID");
			this.mProfileLink = jObj.optString("ProfileURL");
			if(TextUtils.isEmpty(this.mProfileLink)){
				this.mProfileLink = jObj.optString("ProfileLink");
			}
			this.mFriendStatus = jObj.optString("FriendStatus");
			this.mShowFriendsBtn = jObj.optString("ShowFriendsBtn");
			this.mUserIsTeammate = jObj.optString("TeamMate");
		}
	}
	
	
	public static List<FriendModel> getFriendModels(JSONArray jsonArray) {
		List<FriendModel> list = new ArrayList<FriendModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new FriendModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}


	@Override
	public String toString() {
		return "FriendModel{" +
				"mFirstName='" + mFirstName + '\'' +
				", mLastName='" + mLastName + '\'' +
				", mProfilePicture='" + mProfilePicture + '\'' +
				", mUserGUID='" + mUserGUID + '\'' +
				", mUserTypeID='" + mUserTypeID + '\'' +
				", mProfileLink='" + mProfileLink + '\'' +
				", mFriendStatus='" + mFriendStatus + '\'' +
				", mUserIsTeammate='" + mUserIsTeammate + '\'' +
				", mShowFriendsBtn='" + mShowFriendsBtn + '\'' +
				", isRequestSent=" + isRequestSent +
				'}';
	}
}