package com.schollyme.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class NewMedia implements Parcelable {
	public String MediaGUID;
	public String Caption;
	public String Description;
	public String Keyword;
	public String SportsID;

	public NewMedia(String mediaGUID, String caption, String description, String keyword, String sportsID) {
		super();
		MediaGUID = mediaGUID;
		Caption = caption;
		Description = description;
		Keyword = keyword;
		SportsID = sportsID;
	}

	public JSONObject toJSONObject() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("MediaGUID", TextUtils.isEmpty(MediaGUID) ? "" : MediaGUID);
			jsonObject.put("Caption", TextUtils.isEmpty(Caption) ? "" : Caption);
			jsonObject.put("Description", TextUtils.isEmpty(Description) ? "" : Description);
			jsonObject.put("Keyword", TextUtils.isEmpty(Keyword) ? "" : Keyword);
			jsonObject.put("SportsID", TextUtils.isEmpty(SportsID) ? "" : SportsID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public static JSONArray getJSONArray(List<NewMedia> list) {
		JSONArray jsonArray = new JSONArray();
		if (null != list) {
			for (NewMedia newMedia : list) {
				jsonArray.put(newMedia.toJSONObject());
			}
		}
		return jsonArray;
	}

	public NewMedia(Parcel in) {
		MediaGUID = in.readString();
		Caption = in.readString();
		Description = in.readString();
		Keyword = in.readString();
		SportsID = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(MediaGUID);
		dest.writeString(Caption);
		dest.writeString(Description);
		dest.writeString(Keyword);
		dest.writeString(SportsID);
	}

	public static final Creator CREATOR = new Creator() {
		public NewMedia createFromParcel(Parcel in) {
			return new NewMedia(in);
		}

		public NewMedia[] newArray(int size) {
			return new NewMedia[size];
		}
	};

	@Override
	public String toString() {
		return "NewMedia [MediaGUID=" + MediaGUID + ", Caption=" + Caption + ", Description=" + Description + ", Keyword=" + Keyword
				+ ", SportsID=" + SportsID + "]";
	}

}
