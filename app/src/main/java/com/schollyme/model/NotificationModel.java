package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationModel implements Parcelable{
	
	public String mNotificationGUID;
	public String mCreatedDate;
	public String mStatusID;
	public String mNotificationText;
	public String mProfilePicture;
	public String mProfileURL;
	public String mUserName;
	public String mSummary;
	public String mEntityGUID;
	public String mRefer;
	public int mNotificationTypeId;
	public String mSportsID;
	
	public NotificationModel(JSONObject jsonObject){
		super();
		if (null != jsonObject) {
			this.mNotificationGUID = jsonObject.optString("NotificationGUID","0");
			this.mCreatedDate = jsonObject.optString("CreatedDate","0");
			this.mStatusID = jsonObject.optString("StatusID","15");
			this.mNotificationText = jsonObject.optString("NotificationText","");
			this.mProfilePicture = jsonObject.optString("ProfilePicture","");
			this.mProfileURL = jsonObject.optString("ProfileUrl","");
			this.mUserName = jsonObject.optString("UserName","");
			this.mSummary = jsonObject.optString("Summary","");
			this.mEntityGUID = jsonObject.optString("EntityGUID","");
			
			this.mNotificationTypeId = jsonObject.optInt("NotificationTypeID",0);
			this.mRefer = jsonObject.optString("Refer","");
			this.mSportsID = jsonObject.optString("SportsID","");
		}
	}
	
	public static List<NotificationModel> getNotifications(JSONArray jsonArray) {
		List<NotificationModel> list = new ArrayList<NotificationModel>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new NotificationModel(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	public NotificationModel(Parcel in) {
		mNotificationGUID = in.readString();
		mCreatedDate = in.readString();
		mStatusID = in.readString();
		mNotificationText = in.readString();
		mProfilePicture = in.readString();
		mUserName = in.readString();
		mSummary = in.readString();
		mProfileURL = in.readString();
		mNotificationTypeId = in.readInt();
		mRefer = in.readString();
		mSportsID = in.readString();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mNotificationGUID);
		dest.writeString(mCreatedDate);
		dest.writeString(mStatusID);
		dest.writeString(mNotificationText);
		dest.writeString(mProfilePicture);
		dest.writeString(mUserName);
		dest.writeString(mSummary);
		dest.writeString(mProfileURL);
		dest.writeInt(mNotificationTypeId);
		dest.writeString(mRefer);
		dest.writeString(mSportsID);
	}
	
	public static final Creator CREATOR = new Creator() {
		public NotificationModel createFromParcel(Parcel in) {
			return new NotificationModel(in);
		}

		public NotificationModel[] newArray(int size) {
			return new NotificationModel[size];
		}
	};

	@Override
	public String toString() {
		return "NotificationModel [mNotificationGUID=" + mNotificationGUID + ", mCreatedDate=" + mCreatedDate + ", mStatusID=" + mStatusID
				+ ", mNotificationText=" + mNotificationText + ", mProfilePicture=" + mProfilePicture + ", mProfileURL=" + mProfileURL
				+ ", mUserName=" + mUserName + ", mSummary=" + mSummary + ", mEntityGUID=" + mEntityGUID + ", mRefer=" + mRefer
				+ ", mNotificationTypeId=" + mNotificationTypeId + ", mSportsID=" + mSportsID + "]";
	}

	
}