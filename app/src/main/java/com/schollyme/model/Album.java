package com.schollyme.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

import com.vinfotech.request.AlbumCreateRequest;

public class Album implements Parcelable {

	public String AlbumGUID;
	public String AlbumName;
	public String Description;
	public int Visibility;
	public String AlbumType;
	public int MediaCount;
	public String CoverMedia;
	public int IsEditable;
	public String CreatedDate;
	public String ModifiedDate;
	public List<AlbumMedia> AlbumMedias;
	public boolean isSelected = false;
	public int NoOfComments;
	public int NoOfLikes;
	public int IsLike;
	public String ActivityGUID;

	public Album(JSONObject jsonObject) {
		super();
		if (null != jsonObject) {
			AlbumGUID = jsonObject.optString("AlbumGUID", "");
			AlbumName = jsonObject.optString("AlbumName", "");
			Description = jsonObject.optString("Description", "");
			Visibility = jsonObject.optInt("Visibility", 0);
			AlbumType = jsonObject.optString("AlbumType", "");
			MediaCount = jsonObject.optInt("MediaCount", 0);
			CoverMedia = jsonObject.optString("CoverMedia", "");
			IsEditable = jsonObject.optInt("IsEditable", 0);
			CreatedDate = jsonObject.optString("CreatedDate", "");
			ModifiedDate = jsonObject.optString("ModifiedDate", "");
			NoOfComments = jsonObject.optInt("NoOfComments", 0);
			NoOfLikes = jsonObject.optInt("NoOfLikes", 0);
			IsLike = jsonObject.optInt("IsLike", 0);
			ActivityGUID = jsonObject.optString("ActivityGUID", "");
		}
	}

	public Album(String albumType) {
		AlbumGUID = "";
		AlbumName = "";
		Description = "";
		Visibility = AlbumCreateRequest.VISIBILITY_PUBLIC;
		AlbumType = albumType;
		MediaCount = 0;
		CoverMedia = "";
		IsEditable = 0;
		CreatedDate = "";
		ModifiedDate = "";
	}

	public static List<Album> getAlbums(JSONArray jsonArray) {
		List<Album> list = new ArrayList<Album>();
		if (jsonArray != null) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					list.add(new Album(jsonArray.getJSONObject(i)));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public Album(Parcel in) {
		AlbumGUID = in.readString();
		AlbumName = in.readString();
		Description = in.readString();
		Visibility = in.readInt();
		AlbumType = in.readString();
		MediaCount = in.readInt();
		CoverMedia = in.readString();
		IsEditable = in.readInt();
		CreatedDate = in.readString();
		ModifiedDate = in.readString();
		NoOfComments = in.readInt();
		NoOfLikes = in.readInt();
		IsLike = in.readInt();
		ActivityGUID = in.readString();

	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(AlbumGUID);
		dest.writeString(AlbumName);
		dest.writeString(Description);
		dest.writeInt(Visibility);
		dest.writeString(AlbumType);
		dest.writeInt(MediaCount);
		dest.writeString(CoverMedia);
		dest.writeInt(IsEditable);
		dest.writeString(CreatedDate);
		dest.writeString(ModifiedDate);
		dest.writeInt(NoOfComments);
		dest.writeInt(NoOfLikes);
		dest.writeInt(IsLike);
		dest.writeString(ActivityGUID);
		
	}

	public static final Creator CREATOR = new Creator() {
		public Album createFromParcel(Parcel in) {
			return new Album(in);
		}

		public Album[] newArray(int size) {
			return new Album[size];
		}
	};

	@Override
	public String toString() {
		return "Album [AlbumGUID=" + AlbumGUID + ", AlbumName=" + AlbumName + ", Description="
				+ Description + ", Visibility=" + Visibility
				+ ", AlbumType=" + AlbumType + ", MediaCount=" + MediaCount + ", CoverMedia="
				+ CoverMedia + ", IsEditable=" + IsEditable
				+ ", CreatedDate=" + CreatedDate + ", ModifiedDate=" + ModifiedDate + "]";
	}

}
