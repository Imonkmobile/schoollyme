package com.schollyme.googleapi;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.schollyme.R;
import com.vinfotech.utility.Utility;

public class AddressGoogleAdapter extends BaseAdapter {
	public List<Address> mAddresses;
	private LayoutInflater mLayoutInflater;
	private Context mContext;

	public AddressGoogleAdapter(Context context) {
		mContext = context;
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setList(List<Address> list) {
		this.mAddresses = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mAddresses ? 0 : mAddresses.size();
	}

	@Override
	public Address getItem(int position) {
		return mAddresses.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		AddressViewHolder viewHolder;

		if (convertView == null) {
			viewHolder = new AddressViewHolder();
			convertView = mLayoutInflater.inflate(R.layout.search_address_row, null, false);
			viewHolder.addressTv = (TextView) convertView.findViewById(R.id.address_tv);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (AddressViewHolder) convertView.getTag();
		}

		viewHolder.addressTv.setText(mAddresses.get(position).description);
	//	Utility.setSpan(viewHolder.addressTv, mContext);

		return convertView;
	}

	class AddressViewHolder {
		TextView addressTv;
	}

}
