package com.schollyme.googleapi;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.schollyme.R;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.server.BaseRequest.RequestListener;

public class SearchAddressActivity extends Activity implements OnClickListener, OnItemClickListener {
	
	public static final int SEARCH_REQUEST = 123;
	
	private EditText mAddressEt;
	private ListView mAddressLv;
	private TextView mCancel;
	private ImageView mCloseIv;

	private String mCityName;

	private Address mAdress;
	private AddressGoogleAdapter mAddressGoogleAdapter;

	private ArrayList<Address> mAddressArr;
	public static final String ADDRESS_API_KEY = "AIzaSyAxkOLi9kRqCIZ4jVVbz-0QSGI_Q33wsUg";
	private String mPlaceName;
	private GooglePlace mGooglePlace;
	private GooglePlaceRequest mCityGooglePlaceRequest, mInfoGooglePlaceRequest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_address_activity);

		controlInitialization();

		HeaderLayout headerLayout = new HeaderLayout(findViewById(R.id.container_ll));
		headerLayout.setHeaderValues(R.drawable.icon_back, getString(R.string.search_locaton), 0);
		headerLayout.setListenerItI(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setResultAndFinish(false);
			}
		}, null);

		initRequest();
	}

	public static Intent getIntent(Context context) {
		Intent intent = new Intent(context, SearchAddressActivity.class);
		return intent;
	}

	/**
	 * Widgets Initialization
	 */
	private void controlInitialization() {
		mAddressGoogleAdapter = new AddressGoogleAdapter(this);

		mAddressLv = (ListView) findViewById(R.id.addressLv);
		mAddressEt = (EditText) findViewById(R.id.search_et);
		mCancel = (TextView) findViewById(R.id.cancel_tv);
		mCloseIv = (ImageView) findViewById(R.id.close_iv);

		mCloseIv.setVisibility(View.GONE);

		mCancel.setOnClickListener(this);
		mCloseIv.setOnClickListener(this);
		mAddressLv.setOnItemClickListener(this);
		mAddressLv.setAdapter(mAddressGoogleAdapter);

	//	Utility.setSpan(mCancel, getApplicationContext());

		new SearchHandler(mAddressEt).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				mCityName = text;
				if (!mCityName.equals("") && mCityName.length() > 1) {
				//	mCloseIv.setVisibility(View.VISIBLE);
					mCityGooglePlaceRequest.getAddressListInServer(mCityName);
				}
			}
		}).setClearView(mCloseIv);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancel_tv:
			setResult(RESULT_CANCELED);
			finish();
			break;

		case R.id.close_iv:
			mAddressEt.setText("");
			mAddressArr = null;
			mAddressGoogleAdapter.setList(mAddressArr);
			mAddressGoogleAdapter.notifyDataSetChanged();
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
		mAdress = mAddressGoogleAdapter.getItem(position);
		mPlaceName = mAdress.description;

		mInfoGooglePlaceRequest.getAddressInfoInServer(mAdress.placeId);
	}

	private void initRequest() {
		mCityGooglePlaceRequest = new GooglePlaceRequest(this);
		mCityGooglePlaceRequest.setLoader(findViewById(R.id.loading_pb));
		mCityGooglePlaceRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					List<Address> addresses = (List<Address>) data;
					mAddressGoogleAdapter.setList(addresses);
				}
			}
		});
		mInfoGooglePlaceRequest = new GooglePlaceRequest(this);
		mInfoGooglePlaceRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mGooglePlace = (GooglePlace) data;
					setResultAndFinish(true);
				}
			}
		});
	}

	private void setResultAndFinish(boolean ok) {
		Intent data = new Intent();
		data.putExtra("LOC", mPlaceName);
		data.putExtra("googlePlace", mGooglePlace);
		setResult(ok ? RESULT_OK : RESULT_CANCELED, data);
		finish();
	}

}
