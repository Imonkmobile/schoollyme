package com.schollyme.team;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.nirhart.parallaxscroll.views.ParallaxListView;
import com.schollyme.R;
import com.schollyme.activity.CreateWallPostActivity;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.schollyme.model.Teams;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 28/6/2017.
 */

public class TeamFeedListFragment extends BaseFragment {

    private Context mContext;
    private static Teams mTeamPageDetails;
    private static String mTeamGUID, mActvityGUID;
    private ParallaxListView mGnericLv;
    private LogedInUserModel mLogedInUserModel;

    public static TeamFeedListFragment newInstance(String teamGUID, String actvityGUID, Teams teamPageDetails) {
        TeamFeedListFragment friendsTopFragment = new TeamFeedListFragment();
        mTeamGUID = teamGUID;
        mActvityGUID = actvityGUID;
        mTeamPageDetails = teamPageDetails;
        System.out.println("team page details    "+teamPageDetails);
        return friendsTopFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.team_feed_list_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();
        mLogedInUserModel = new LogedInUserModel(mContext);
        initFeedRequest(view);
    }

    private void resetList() {
        mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
        mNewsFeeds.clear();
        mNewsFeeds.add(new NewsFeed(null));
        mPostFeedAdapter.setList(mNewsFeeds, false);
    }

    private View mFooterRL;
    private TextView mNoMoreTV;
    private TextView mNoFeedTV;
    private ProgressBar mLoaderBottomPB, mLoadingCenter;
    private SwipeRefreshLayout mSwipeRefreshWidget;

    private PostFeedAdapter mPostFeedAdapter;
    private NFFilter mNFFilter;
    private List<NewsFeed> mNewsFeeds = new ArrayList<NewsFeed>();
    boolean isLoading;

    private NewsFeedRequest mNewsFeedRequest;

    private void initFeedRequest(View view) {
        mNFFilter = new NFFilter(NewsFeedRequest.MODULE_ID_GROUPS, mTeamGUID);
        mGnericLv = (ParallaxListView) view.findViewById(R.id.list_view);
        if (!TextUtils.isEmpty(mActvityGUID)) {
            mNFFilter.ActivityGUID = mActvityGUID;
            mNFFilter.AllActivity = 0;
        }
        mNFFilter.AllActivity = 0;
        mPostFeedAdapter = new PostFeedAdapter(mContext, new PostFeedAdapter.OnItemClickListenerPost() {

            @Override
            public void onClickItems(int ID, int position, NewsFeed newsFeed) {
                onItemClick(ID, position, newsFeed);
            }
        }, mTeamGUID);

        mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);

        mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
        mNoMoreTV.setVisibility(View.INVISIBLE);

        mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
        mLoaderBottomPB.setVisibility(View.INVISIBLE);
        mLoaderBottomPB.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.red_normal_color), PorterDuff.Mode.SRC_IN);

        mNoFeedTV = (TextView) view.findViewById(R.id.no_feed_tv);

        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
        mSwipeRefreshWidget.setColorScheme(R.color.red_normal_color);
        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mSwipeRefreshWidget.setRefreshing(true);
                getFeedRequest(mNFFilter);
            }
        });

        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) view.findViewById(R.id.createTeamImgMenu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                fabMenu.collapse();

                if (mTeamPageDetails.isGroupMember) {
                    startActivityForResult(CreateWallPostActivity.getIntent(mContext,
                            WallPostCreateRequest.MODULE_ID_GROUPS, true, mTeamGUID),
                            CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
                    getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
                }else{
                    DialogUtil.showOkDialog(mContext, getResources().getString(R.string.not_member),  getResources().getString(R.string.app_name));
                }
            }

            @Override
            public void onMenuCollapsed() {

            }
        });

        mGnericLv.addFooterView(mFooterRL);
        mGnericLv.setAdapter(mPostFeedAdapter);
        mGnericLv.setOnScrollListener(new ScrollEndListener());
        mGnericLv.setAdapter(mPostFeedAdapter);
        mNFFilter.AllActivity = 0;
        getFeedRequest(mNFFilter);
        mNFFilter.ActivityGUID = "";
    }


    int ClickLocation = 0;

    public void onItemClick(int ID, int position, NewsFeed newsFeed) {
        ClickLocation = position;
        switch (ID) {
            case R.id.share_tv:
                if (newsFeed.ShareAllowed == 1) {
                    sharePost(newsFeed);
                } else {
                    Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.like_tv:
                likeMediaToggleService(newsFeed, position);
                break;
            case R.id.likes_tv:
                if (newsFeed.NoOfLikes != 0) {
                    startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID,
                            "ACTIVITY", "WALL"), LikeListActivity.REQ_FROM_HOME_LIKELIST);
                    getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
                }
                break;

            case R.id.comment_tv: {
                AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
                String captionForWriteCommentScreen = "";
                if (!newsFeed.PostContent.equals("")) {
                    captionForWriteCommentScreen = newsFeed.PostContent;
                } else {

                    if (null != newsFeed.Album.AlbumMedias && !newsFeed.Album.AlbumMedias.get(0).ImageName.equals("")) {

                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).Caption;
                    } else if (null != newsFeed.Album.AlbumMedias) {
                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
                    } else {
                        captionForWriteCommentScreen = "";
                    }
                }

                startActivityForResult(WriteCommentActivity.getIntent(mContext, albumMedia, "ACTIVITY",
                        captionForWriteCommentScreen), WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY);
                getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
            }
            break;
            case R.id.comments_tv:

                AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
                String captionForWriteCommentScreen = "";
                if (!newsFeed.PostContent.equals("")) {
                    captionForWriteCommentScreen = newsFeed.PostContent;
                } else {

                    if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
                    }
                }

                startActivityForResult(CommentListActivity.getIntent(mContext, albumMedia, "WALLPOST",
                        captionForWriteCommentScreen),
                        CommentListActivity.REQ_CODE_COMMENT_LIST);
                getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

                break;

            case PostFeedAdapter.ConvertViewID:
                if (mTeamPageDetails.isGroupMember) {
                    startActivityForResult(CreateWallPostActivity.getIntent(mContext,
                            WallPostCreateRequest.MODULE_ID_GROUPS, true, mTeamGUID),
                            CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
                } else {
                    DialogUtil.showOkDialog(mContext, getResources().getString(R.string.not_member),
                            getResources().getString(R.string.app_name));
                }
                break;
            default:
                break;
        }
    }

    /**
     * API to share any media on page wall
     */
    public void sharePost(NewsFeed newsFeedModel) {
        SharePostRequest mSharePostRequest = new SharePostRequest(mContext);
        mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY",
                newsFeedModel.PostContent, NewsFeedRequest.MODULE_ID_GROUPS, mTeamGUID, 1, 1);
        mSharePostRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
//                    mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, MsgType.Success);
                } else {
//                    mErrorLayout.showError((String) data, true, MsgType.Error);
                }
            }
        });
    }


    /**
     * API to like/unlike any media on page wall
     */
    public void likeMediaToggleService(final NewsFeed newsFeed, final int position) {

        ToggleLikeRequest mToggleLikeRequest = new ToggleLikeRequest(mContext);
        mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");
        mToggleLikeRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    if (newsFeed.IsLike == 0) {
                        newsFeed.IsLike = 1;
                        newsFeed.NoOfLikes++;
                    } else {
                        newsFeed.IsLike = 0;
                        newsFeed.NoOfLikes--;
                    }
                    mPostFeedAdapter.notifyDataSetChanged();

                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), "");
                }

            }
        });
    }

    /**
     * API to get list of team page feeds to display on team wall
     */
    private void getFeedRequest(final NFFilter nffilter) {

        if (null == mNewsFeedRequest) {
            mNewsFeedRequest = new NewsFeedRequest(mContext);
        }
        mNewsFeedRequest.getNewsFeedListInServer(nffilter);
        mNoMoreTV.setVisibility(View.INVISIBLE);
        if (nffilter.PageNo == 1) {
            if (mPostFeedAdapter.getCount() <= 1 || mPostFeedAdapter == null) {
                mNewsFeedRequest.setLoader(mLoadingCenter);
            } else {
                mNewsFeedRequest.setLoader(null);
            }

            mLoaderBottomPB.setVisibility(View.INVISIBLE);
        } else {
            mNewsFeedRequest.setLoader(mLoaderBottomPB);
            mLoaderBottomPB.setVisibility(View.VISIBLE);
        }

        mNewsFeedRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mSwipeRefreshWidget.setRefreshing(false);
                if (success) {
                    if (null != data) {

                        if (mNFFilter.PageNo == 1) {
                            resetList();
                        }
                        List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
                        mNewsFeeds.addAll(newsFeeds);
                        mPostFeedAdapter.setList(mNewsFeeds, false);

                        if (totalRecords == 0) {
                            isLoading = false;
                            mNoMoreTV.setVisibility(View.INVISIBLE);
                            mNoFeedTV.setVisibility(View.VISIBLE);

                        } else if (mNewsFeeds.size() <= totalRecords) {

                            isLoading = true;
                            int newPage = mNFFilter.PageNo + 1;
                            mNFFilter.PageNo = newPage;
                            mNoFeedTV.setVisibility(View.GONE);
                            mNoMoreTV.setVisibility(View.VISIBLE);
                        } else {
                            mNoMoreTV.setVisibility(View.VISIBLE);
                            isLoading = false;
                            mNoFeedTV.setVisibility(View.GONE);
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                        if (mNFFilter.PageNo == 1) {
                            mNoMoreTV.setVisibility(View.INVISIBLE);
                        } else {
                            mNoMoreTV.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    isLoading = false;
                    DialogUtil.showOkDialog(mContext, (String) data, "");
                }
            }
        });
    }

    private class ScrollEndListener implements AbsListView.OnScrollListener {

        private int mLastFirstVisibleItem;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int k = visibleItemCount + firstVisibleItem;
            if (k >= totalItemCount && isLoading && totalItemCount != 0) {
                getFeedRequest(mNFFilter);
            }
            if (mLastFirstVisibleItem < firstVisibleItem) {
                Log.i("SCROLLING DOWN", "TRUE");
            }
            if (mLastFirstVisibleItem > firstVisibleItem) {
                Log.i("SCROLLING UP", "TRUE");
            }
            mLastFirstVisibleItem = firstVisibleItem;
        }
    }
}
