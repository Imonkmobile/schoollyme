package com.schollyme.team;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.Teams;
import com.schollyme.team.FanPageListFragment.AdapterRefreshListener;
import com.vinfotech.request.FanPageJoinRequest;
import com.vinfotech.request.TeamPageDeactivateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class SuggestedTeamAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Teams> mTeamAL, mFilteredTeamAL = new ArrayList<Teams>();
    private LayoutInflater mLayoutInflater;
    private OfficialTeamsViewHolder mOfficialTeamsViewHolder;
    private ErrorLayout mErrorLayout;
    private AdapterRefreshListener mAdapterRefreshListener;

    public SuggestedTeamAdapter(Context mContext, ErrorLayout mLayout, AdapterRefreshListener mListener) {
        this.mContext = mContext;
        this.mErrorLayout = mLayout;
        this.mAdapterRefreshListener = mListener;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setList(ArrayList<Teams> mList) {
        this.mTeamAL = mList;
        setFilter(null);
    }

    @Override
    public int getCount() {
        return null == mFilteredTeamAL ? 0 : mFilteredTeamAL.size();
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @SuppressLint("DefaultLocale")
    @SuppressWarnings("deprecation")
    @Override
    public View getView(final int position, View contentView, ViewGroup parent) {
        mOfficialTeamsViewHolder = new OfficialTeamsViewHolder();
        if (contentView == null) {
            contentView = mLayoutInflater.inflate(R.layout.team_adapter_item_view, parent, false);

            mOfficialTeamsViewHolder.mTeamNameTV = (TextView) contentView.findViewById(R.id.team_name_tv);
            mOfficialTeamsViewHolder.mTeamTypeTV = (TextView) contentView.findViewById(R.id.team_type_tv);
            mOfficialTeamsViewHolder.mJoinTeamButton = (Button) contentView.findViewById(R.id.join_remove_button);
            mOfficialTeamsViewHolder.mTeamImageIV = (ImageView) contentView.findViewById(R.id.team_image_iv);
            mOfficialTeamsViewHolder.mTeamFirstCharTV = (TextView) contentView.findViewById(R.id.team_name_fchar_tv);

            contentView.setTag(mOfficialTeamsViewHolder);
        } else {
            mOfficialTeamsViewHolder = (OfficialTeamsViewHolder) contentView.getTag();
        }
        if (mFilteredTeamAL.get(position).mPageCategoryID.equals("1")) {
            String imageUrl = Config.IMAGE_URL_PROFILE + mFilteredTeamAL.get(position).mTeamImageURL;
            ImageLoaderUniversal.ImageLoadRoundForTeamPage(mContext, imageUrl, mOfficialTeamsViewHolder.mTeamImageIV,
                    ImageLoaderUniversal.option_Round_Image_for_teampage);
        } else {

            if (position % 4 == 0) {
                mOfficialTeamsViewHolder.mTeamImageIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo1));
            } else if (position % 4 == 1) {
                mOfficialTeamsViewHolder.mTeamImageIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo2));
            } else if (position % 4 == 2) {
                mOfficialTeamsViewHolder.mTeamImageIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo3));
            } else if (position % 4 == 3) {
                mOfficialTeamsViewHolder.mTeamImageIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo4));
            }

            String mTeamName = mFilteredTeamAL.get(position).mTeamName;

            if (!TextUtils.isEmpty(mTeamName)) {
                mOfficialTeamsViewHolder.mTeamFirstCharTV.setText(String.valueOf(mTeamName.charAt(0)).toUpperCase());
            }

        }
        mOfficialTeamsViewHolder.mTeamNameTV.setText(mFilteredTeamAL.get(position).mTeamName);
        mOfficialTeamsViewHolder.mTeamTypeTV.setText(mFilteredTeamAL.get(position).mTeamSportsName);
        mOfficialTeamsViewHolder.mJoinTeamButton.setTag(position);

        if (mFilteredTeamAL.get(position).isGroupAdmin) {

            mOfficialTeamsViewHolder.mJoinTeamButton.setVisibility(View.VISIBLE);
            if (mFilteredTeamAL.get(position).mStatusID.equals("2")) {
                mOfficialTeamsViewHolder.mJoinTeamButton.setText(mContext.getResources().getString(R.string.deactivate_label));
                mOfficialTeamsViewHolder.mJoinTeamButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        final int pos = (int) v.getTag();
                        String teamName = mFilteredTeamAL.get(position).mTeamName;
                        DialogUtil.showOkCancelDialogButtonLisnter(mContext,
                                mContext.getString(R.string.deactivate_page_confirmation), teamName,
                                new OnOkButtonListner() {
                                    @Override
                                    public void onOkBUtton() {
                                        deactivatePageRequest(mFilteredTeamAL.get(pos).mTeamGUID, pos, Config.IN_ACTIVATE_PAGE);
                                    }
                                }, new OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface arg0) {

                                    }
                                });
                    }
                });
            } else {
                mOfficialTeamsViewHolder.mJoinTeamButton.setText(mContext.getResources().getString(R.string.activate_label));
                mOfficialTeamsViewHolder.mJoinTeamButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        final int pos = (int) v.getTag();

                        String teamName = mFilteredTeamAL.get(position).mTeamName;
                        DialogUtil.showOkCancelDialogButtonLisnter(mContext,
                                mContext.getString(R.string.activate_page_confirmation), teamName,
                                new OnOkButtonListner() {
                                    @Override
                                    public void onOkBUtton() {
                                        deactivatePageRequest(mFilteredTeamAL.get(pos).mTeamGUID, pos, Config.ACTIVATE_PAGE);
                                    }
                                }, new OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface arg0) {

                                    }
                                });
                    }
                });
            }

        } else if (!mFilteredTeamAL.get(position).isGroupAdmin && !mFilteredTeamAL.get(position).isGroupMember) {
            mOfficialTeamsViewHolder.mJoinTeamButton.setVisibility(View.VISIBLE);
            mOfficialTeamsViewHolder.mJoinTeamButton.setText(mContext.getResources().getString(R.string.join_label));
            mOfficialTeamsViewHolder.mJoinTeamButton.setVisibility(View.VISIBLE);
            mOfficialTeamsViewHolder.mJoinTeamButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    final int pos = (int) v.getTag();

                    String teamName = mFilteredTeamAL.get(position).mTeamName;

                    DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps, teamName,
                            mContext.getString(R.string.join_page_confirmation),
                            new OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    joinPageRequest(mFilteredTeamAL.get(pos).mTeamGUID, pos);
                                }
                            }, new OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                }
            });
        } else if (mFilteredTeamAL.get(position).isGroupMember) {
            mOfficialTeamsViewHolder.mJoinTeamButton.setVisibility(View.GONE);
        }
        return contentView;
    }

    /**
     * Call API to join a suggested fan page
     */
    private void joinPageRequest(String mTeamGUID, final int pos) {

        FanPageJoinRequest mRequest = new FanPageJoinRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mFilteredTeamAL.remove(pos);
                    notifyDataSetChanged();
                    mErrorLayout.showError(data.toString(), true, MsgType.Success);
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
        mRequest.getFanPageJoinRequest(mTeamGUID, new LogedInUserModel(mContext).mUserGUID);
    }

    /**
     * Call API to Deactivate a fan/team page
     * only page admin/owner have permision to Deactivate a page.
     */
    private void deactivatePageRequest(String mTeamGUID, final int pos, String mActionType) {

        TeamPageDeactivateRequest mRequest = new TeamPageDeactivateRequest(mContext);
        mRequest.TeamDeactivateRequest(mTeamGUID, mActionType);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), true, MsgType.Success);
                    Teams mTeamPage = mFilteredTeamAL.get(pos);
                    if (mFilteredTeamAL.get(pos).mStatusID.equals("1")) {
                        mTeamPage.mStatusID = "2";
                    } else if (mFilteredTeamAL.get(pos).mStatusID.equals("2")) {
                        mTeamPage.mStatusID = "1";
                    }
                    mFilteredTeamAL.set(pos, mTeamPage);
                    notifyDataSetChanged();
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
    }

    private class OfficialTeamsViewHolder {

        private TextView mTeamNameTV, mTeamTypeTV, mTeamFirstCharTV;
        private ImageView mTeamImageIV;
        private Button mJoinTeamButton;
    }

    public void setFilter(String text) {
        mFilteredTeamAL.clear();
        if (null != mFilteredTeamAL) {
            if (TextUtils.isEmpty(text)) {
                mFilteredTeamAL.addAll(mTeamAL);
            } else {
                for (Teams teamModel : mTeamAL) {
                    String mTeamName = teamModel.mTeamName.toLowerCase();
                    if (mTeamName.startsWith(text.toString().toLowerCase()) || mTeamName.contains(" " + text.toString().toLowerCase())) {
                        mFilteredTeamAL.add(teamModel);
                    }
                }
            }
        }
        notifyDataSetChanged();
        mAdapterRefreshListener.onRefresh(mFilteredTeamAL.size());
    }
}