package com.schollyme.team;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.media.MediaPickerActivity;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.Teams;
import com.vinfotech.request.FanPageJoinRequest;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.request.TeamPageDeactivateRequest;
import com.vinfotech.request.TeamPageDetailRequest;
import com.vinfotech.request.TeamPageLeaveRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.widget.PagerSlidingTabStrip;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Activity class. This may be useful in display team page,
 * it contains team basic info and team wall
 *
 * @author Ravi Bhandari
 */
public class TeamPageActivity extends MediaPickerActivity implements OnClickListener {

    private Context mContext;
    private ErrorLayout mErrorLayout;
    private TeamPageActivityViewHolder mTeamPageActivityViewHolder;
    private String mTeamGUID, mActvityGUID;
    private Teams mTeamPageDetails;
    private int position;
    private TextView titleTv;
    private String actionType = "";
    //private ParallaxListView mGnericLv;
    private Bitmap mPhotoBitmap;
    private boolean mFromQuery = false;
    private int mScreenHeight;

    private static final int PAGE_COUNT = 3;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private ViewPager mGenericVp;
    private GridPagerAdapter mGridPagerAdapter;
    private int mCurrentPageToShow = 0;

    private void getScreenWidthandHeight() {
        int[] dimens = new int[2];
        DisplayUtil.probeScreenSize((Activity) mContext, dimens);
        mScreenHeight = dimens[1];
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.team_page_activity);
        mContext = this;

        mTeamPageActivityViewHolder = new TeamPageActivityViewHolder(findViewById(R.id.main_rl), this);
        getScreenWidthandHeight();
        setScrollView();

        mErrorLayout = new ErrorLayout(findViewById(R.id.include_error));
        mTeamGUID = getIntent().getStringExtra("TeamGUID");
        mActvityGUID = getIntent().getStringExtra("ActvityGUID");
        position = getIntent().getIntExtra("Position", 0);
        if (getIntent().hasExtra("teamDetails")) {
            mTeamPageDetails = (Teams) getIntent().getSerializableExtra("teamDetails");
        }

        Uri uri = getIntent().getData();
        mFromQuery = !(null == uri || TextUtils.isEmpty(uri.getQueryParameter("FromQuery")));
        if (mTeamGUID == null && uri != null) {
            mTeamGUID = uri.getQueryParameter("TeamGUID");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        getTeamPageDetailsServerRequest(mTeamGUID);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public static Intent getIntent(Context mContext, String mTeamGUID, String ActvityGUID, int position, Teams teamDetails) {
        Intent intent = new Intent(mContext, TeamPageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("TeamGUID", mTeamGUID);
        intent.putExtra("ActvityGUID", ActvityGUID);
        intent.putExtra("Position", position);

        intent.putExtra("teamDetails", teamDetails);
        return intent;
    }

    public static Intent getIntent(Context mContext, String mTeamGUID, String ActvityGUID, int position) {
        Intent intent = new Intent(mContext, TeamPageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("TeamGUID", mTeamGUID);
        intent.putExtra("ActvityGUID", ActvityGUID);
        intent.putExtra("Position", position);
        return intent;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_ib:
                finish();
                break;
            case R.id.join_leave_team_button:
                if (mTeamPageDetails.isGroupMember) {
                    DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps, mTeamPageDetails.mTeamName,
                            mContext.getString(R.string.leave_page_confirmation),
                            new OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    /*joinPageRequest(mTeamPageDetails.mTeamGUID);*/
                                    leavePageRequest(mTeamPageDetails.mTeamGUID, new LogedInUserModel(mContext).mUserGUID);
                                }
                            }, new OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                } else {
                    DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps, mTeamPageDetails.mTeamName,
                            mContext.getString(R.string.join_page_confirmation),
                            new OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    joinPageRequest(mTeamPageDetails.mTeamGUID);
                                }
                            }, new OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                }
                break;
            case R.id.message_tv:
                if (mTeamPageDetails.isGroupMember) {
                    if (mTeamPageDetails != null) {
                        startActivity(NewBroadcastActivity.getIntent(mContext, 0, mTeamGUID, mTeamPageDetails.mTeamName));
                    }
                } else {
                    if (mTeamPageDetails != null) {
                        if (mTeamPageDetails.isGroupMember) {
                            DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps,
                                    mContext.getResources().getString(R.string.app_name),
                                    mContext.getString(R.string.leave_page_confirmation),
                                    new OnClickListener() {
                                        @Override
                                        public void onClick(View arg0) {
//                                            joinPageRequest(mTeamPageDetails.mTeamGUID);
                                            leavePageRequest(mTeamPageDetails.mTeamGUID, new LogedInUserModel(mContext).mUserGUID);
                                        }
                                    }, new OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    });
                        } else {
                            DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps,
                                    mContext.getResources().getString(R.string.app_name),
                                    mContext.getString(R.string.join_page_confirmation),
                                    new OnClickListener() {
                                        @Override
                                        public void onClick(View arg0) {
                                            joinPageRequest(mTeamPageDetails.mTeamGUID);
                                        }
                                    }, new OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    });
                        }
                    }
                }
                break;
            case R.id.edit_ib:
                if (mTeamPageDetails.isGroupAdmin) {
                    displayImagePickerPopup();
                }
                break;
            default:
                break;
        }
    }

    /**
     * Dislpay data in respective fields
     */
    @SuppressWarnings("deprecation")
    private void setData() {

        mTeamPageActivityViewHolder.mPageNameTV.setText(mTeamPageDetails.mTeamName);
        mTeamPageActivityViewHolder.mPageSportsTV.setText(mTeamPageDetails.mTeamSportsName);
        String mAdminName = getResources().getString(R.string.created_by) + " " + mTeamPageDetails.mAdminUserFName;
        mTeamPageActivityViewHolder.mPageAdminTV.setText(mAdminName);

        if (!mTeamPageDetails.mTeamCoverImageURL.equals("thumb.png")) {
            String imageUrl = Config.COVER_URL + mTeamPageDetails.mTeamCoverImageURL;
            ImageLoaderUniversal.ImageLoadSquare(mContext, imageUrl, mTeamPageActivityViewHolder.mCoverPicIV,
                    ImageLoaderUniversal.option_normal_Image_Thumbnail_cover);
        }

        if (mTeamPageDetails.mPageCategoryID.equals("1")) {
            String imageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mTeamImageURL;
            ImageLoaderUniversal.ImageLoadRoundForTeamPage(mContext, imageUrl, mTeamPageActivityViewHolder.mPageLogoIV,
                    ImageLoaderUniversal.option_Round_Image_for_teampage);
        } else {
            String mTeamName = mTeamPageDetails.mTeamName.trim();
            if (!TextUtils.isEmpty(mTeamName)) {
                mTeamPageActivityViewHolder.mTeamFirstCharTV.setText(String.valueOf(mTeamName.charAt(0)).toUpperCase());
            }
            if (position % 4 == 0) {
                mTeamPageActivityViewHolder.mPageLogoIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo1));
            } else if (position % 4 == 1) {
                mTeamPageActivityViewHolder.mPageLogoIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo2));
            } else if (position % 4 == 2) {
                mTeamPageActivityViewHolder.mPageLogoIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo3));
            } else if (position % 4 == 3) {
                mTeamPageActivityViewHolder.mPageLogoIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo4));
            }
        }

        if (mTeamPageDetails.isGroupAdmin) {
            mTeamPageActivityViewHolder.mEditCoverIB.setVisibility(View.VISIBLE);
            if (new LogedInUserModel(mContext).mUserType == Config.USER_TYPE_COACH) {
                mTeamPageActivityViewHolder.mDirectMessageTV.setVisibility(View.VISIBLE);
            } else {
                mTeamPageActivityViewHolder.mDirectMessageTV.setVisibility(View.INVISIBLE);
            }

            mTeamPageActivityViewHolder.mDirectMessageTV.setCompoundDrawablesWithIntrinsicBounds
                    (R.drawable.selector_team_message, 0, 0, 0);
        } else {
            mTeamPageActivityViewHolder.mDirectMessageTV.setVisibility(View.INVISIBLE);
            mTeamPageActivityViewHolder.mEditCoverIB.setVisibility(View.INVISIBLE);
            if (mTeamPageDetails.isGroupMember || mTeamPageDetails.isGroupAdmin) {
                mTeamPageActivityViewHolder.mJoinLeaveButton.setVisibility(View.VISIBLE);
                mTeamPageActivityViewHolder.mJoinLeaveButton.setText(getResources().getString(R.string.leave_header));
            } else {
                mTeamPageActivityViewHolder.mJoinLeaveButton.setVisibility(View.VISIBLE);
                mTeamPageActivityViewHolder.mDirectMessageTV.setText(getResources().getString(R.string.join_label));
            }


        }
    }

    /**
     * Call api to get team page details
     */
    private void getTeamPageDetailsServerRequest(String mTeamGUID) {

        TeamPageDetailRequest mRequest = new TeamPageDetailRequest(mContext);
        mRequest.TeamDetailsServerRequest(mTeamGUID);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mTeamPageDetails = (Teams) data;
                    setData();
                } else {
                    final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                            getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {
                                @Override
                                public void onOkBUtton() {
                                    finish();
                                }
                            });
                    builder.setCancelable(false);
                    builder.setOnKeyListener(new Dialog.OnKeyListener() {
                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode,
                                             KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                finish();
                                builder.dismiss();
                            }
                            return true;
                        }
                    });
                }

            }
        });
    }

    public class TeamPageActivityViewHolder {

        public TextView mPageNameTV;
        public TextView mPageSportsTV;
        public TextView mPageAdminTV;
        public TextView mTeamFirstCharTV;
        public Button mJoinLeaveButton;
        private ImageView mPageLogoIV, mCoverPicIV;
        public RelativeLayout mUserInfoRL;
        public LinearLayout mProfileImageLl;
        public TextView mDirectMessageTV;
        private ImageButton mBackIB, mEditCoverIB;


        public TeamPageActivityViewHolder(View view, OnClickListener listener) {

            mPageLogoIV = (ImageView) findViewById(R.id.profile_image_iv);
            mCoverPicIV = (ImageView) findViewById(R.id.profile_cover_iv);
            mJoinLeaveButton = (Button) findViewById(R.id.join_leave_team_button);
            mPageNameTV = (TextView) findViewById(R.id.user_name_tv);
            mPageSportsTV = (TextView) findViewById(R.id.user_status_tv);
            mPageAdminTV = (TextView) findViewById(R.id.friend_action_tv);
            mUserInfoRL = (RelativeLayout) findViewById(R.id.profile_image_name_rl);
            mTeamFirstCharTV = (TextView) findViewById(R.id.team_name_fchar_tv);
            mEditCoverIB = (ImageButton) view.findViewById(R.id.edit_ib);
            mProfileImageLl = (LinearLayout) view.findViewById(R.id.profile_image_ll);

            mBackIB = (ImageButton) view.findViewById(R.id.back_ib);
            mDirectMessageTV = (TextView) view.findViewById(R.id.message_tv);
            mEditCoverIB.setOnClickListener(listener);

            mBackIB.setOnClickListener(listener);
            mDirectMessageTV.setOnClickListener(listener);
            mJoinLeaveButton.setOnClickListener(listener);
            mGridPagerAdapter = new GridPagerAdapter(getSupportFragmentManager());


            mPagerSlidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs_psts);
            mGenericVp = (ViewPager) view.findViewById(R.id.generic_vp);
            mGenericVp.setOffscreenPageLimit(1);
            mGenericVp.setAdapter(mGridPagerAdapter);

            mPagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    // BaseFragment baseFragment = mGridPagerAdapter.getBaseFragment(position);
                    /*if (baseFragment != null)
                        baseFragment.onReload();*/
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int position) {

                }
            });
            mPagerSlidingTabStrip.setViewPager(mGenericVp, TeamPageActivity.this);
            mGenericVp.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mGenericVp.setCurrentItem(mCurrentPageToShow);
                }
            }, 150);

        }
    }

    private void setScrollView() {

    }

    /**
     * Call API to Deactivate a fan/team page
     * only page admin/owner have permision to Deactivate a page.
     */
    private void deactivatePageRequest(String mTeamPageGUID, String actionType) {
        TeamPageDeactivateRequest mRequest = new TeamPageDeactivateRequest(mContext);
        mRequest.TeamDeactivateRequest(mTeamPageGUID, actionType);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Success);
                    getTeamPageDetailsServerRequest(mTeamGUID);
                } else {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Error);
                }
            }
        });
    }

    /**
     * Call  api to leave current group,
     * This action can be perform by page members only
     */
    private void leavePageRequest(final String mTeamGUID, String UserGUID) {
        TeamPageLeaveRequest mRequest = new TeamPageLeaveRequest(mContext);
        mRequest.TeamPagesLeaveRequest(mTeamGUID, UserGUID);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
//                    mErrorLayout.showError(data.toString(), true, MsgType.Success);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            Intent intent;
                            if (mTeamPageDetails.mPageCategoryID.equals("1")) {
                                intent = TeamPageActivity.getIntent(mContext, mTeamGUID, mActvityGUID, position, mTeamPageDetails);
                            } else {
                                intent = TeamPageActivity.getIntent(mContext, mTeamGUID, mActvityGUID, position, mTeamPageDetails);
                            }

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }, 2000);
                } else {
//                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
    }

    /**
     * Call API to join a suggested fan page
     */
    private void joinPageRequest(String mTeamPageGUID) {
        FanPageJoinRequest mRequest = new FanPageJoinRequest(mContext);
        mRequest.getFanPageJoinRequest(mTeamPageGUID, new LogedInUserModel(mContext).mUserGUID);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
//                  mErrorLayout.showError(data.toString(), true, MsgType.Success);
                    getTeamPageDetailsServerRequest(mTeamGUID);
                } else {
//                  mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });

    }

    /**
     * Page cover image block
     */
    private void displayImagePickerPopup() {

        DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
            @Override
            public void onItemClick(int position, String item) {
                if (position == 0) {
                    openCamera();
                } else {
                    openGallery();
                }
            }

            @Override
            public void onCancel() {

            }
        }, mContext.getString(R.string.take_picture), mContext.getString(R.string.load_from_library));
    }

    @Override
    protected void onGalleryImageSelected(String fileUri, Bitmap bitmap) {

    }

    @Override
    protected void onCameraImageSelected(String fileUri, Bitmap bitmap) {
        mPhotoBitmap = bitmap;
        try {
            Uri path = Uri.parse(fileUri);
            uploadMediaServerRequest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onVideoCaptured(String videoPath) {

    }

    @Override
    protected void onMediaPickCanceled(int reqCode) {

    }

    /**
     * API to upload media to server
     */
    private void uploadMediaServerRequest() {

        MediaUploadRequest mRequest = new MediaUploadRequest(mContext, false);
        String fileName = ImageUtil.getFileNameFromURI(cropImageUri, mContext);
        File mFile = new File(cropImageUri.getPath());
        final LogedInUserModel mLoggedInUserModel = new LogedInUserModel(mContext);
        BaseRequest.setLoginSessionKey(mLoggedInUserModel.mLoginSessionKey);
        mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_GROUPS,
                mTeamGUID, MediaUploadRequest.TYPE_GROUP_COVER);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    getTeamPageDetailsServerRequest(mTeamGUID);
                } else {
                    mTeamPageActivityViewHolder.mCoverPicIV.setImageBitmap(null);
                }
            }
        });
    }


    public class GridPagerAdapter extends FragmentPagerAdapter {

        private Map<Integer, BaseFragment> mBaseFragments;

        @SuppressLint("UseSparseArrays")
        public GridPagerAdapter(FragmentManager fm) {
            super(fm);
            mBaseFragments = new HashMap<Integer, BaseFragment>();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String tabTitle = "";
            switch (position) {
                case 0:
                    tabTitle = getString(R.string.feed);
                    break;
                case 1:
                    tabTitle = getString(R.string.About_tab);
                    break;
                case 2:
                    tabTitle = getString(R.string.members_tab);
                    break;

            }
            return tabTitle;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            BaseFragment mFragment = null;
            switch (position) {
                case 0:
                    if (mTeamPageDetails != null) {
                        mFragment = TeamFeedListFragment.newInstance(mTeamGUID, mActvityGUID, mTeamPageDetails);
                    }
                    break;
                case 1:
                    mFragment = TeamAboutFragment.newInstance(mTeamGUID, "", position);
                    break;
                case 2:
                    String mUserId = "", catId = "";
                    if (mTeamPageDetails != null) {
                        mUserId = mTeamPageDetails.mAdminUserGUID;
                        catId = mTeamPageDetails.mPageCategoryID;
                    }
                    mFragment = TeamMembersFragment.newInstance(mTeamGUID, mUserId, catId);
                    break;
            }
            mBaseFragments.put(position, mFragment);
            return mFragment;
        }

        public BaseFragment getBaseFragment(int position) {
            return mBaseFragments.get(position);
        }
    }
}