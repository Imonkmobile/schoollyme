package com.schollyme.team;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.activity.BaseActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.FilterBy;
import com.schollyme.model.PageMemberUser;
import com.vinfotech.request.InviteUserToPageRequest;
import com.vinfotech.request.SearchFriendsForGroupRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

/**
 * Activity class. This may be useful in Add member to a page activity
 *
 * @author Ravi Bhandari
 */
public class AddMembersToPageActivity extends BaseActivity {

    private Context mContext;
    private ListView mUserLV;
    private TextView mNoRecordTV;
    private EditText mSearchET;
    private ImageButton mClearIB;
    private int PageNo = 1;
    private final int ADD_FORCE_FULLY = 1;
    private String mPageGUID;
    private InviteToUserAdapter mAdapter;
    private ArrayList<PageMemberUser> mInviteUserAL;
    private ErrorLayout mErrorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_message_activity);
        mContext = this;

        mUserLV = (ListView) findViewById(R.id.friends_lv);
        mNoRecordTV = (TextView) findViewById(R.id.no_record_message_tv);
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_ll));
        mSearchET = (EditText) findViewById(R.id.search_et);
        mClearIB = (ImageButton) findViewById(R.id.clear_text_ib);
        mPageGUID = getIntent().getStringExtra("PageGUID");

        initHeader();
    }

    private void initHeader() {

        setHeader(findViewById(R.id.header_layout), R.drawable.selector_cancel, R.drawable.selector_confirm,
                getResources().getString(R.string.add_members), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Utility.hideSoftKeyboard(mSearchET);
                        finish();
                        overridePendingTransition(0, R.anim.slide_down_dialog);
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        Utility.hideSoftKeyboard(mSearchET);
                        if (mAllSelections == null || mAllSelections.size() == 0) {
                            mErrorLayout.showError(getResources().getString(R.string.no_user_selected), true, MsgType.Error);
                        } else {
                            ArrayList<String> mSelectedUsers = new ArrayList<String>();

                            for (PageMemberUser pmUser : mAllSelections) {
                                mSelectedUsers.add(pmUser.mUserGUID);
                            }
                            InviteUserToPageRequest mRequest = new InviteUserToPageRequest(mContext);
                            mRequest.InviteToPageServerRequest(mPageGUID, mSelectedUsers, ADD_FORCE_FULLY);
                            mRequest.setRequestListener(new RequestListener() {

                                @Override
                                public void onComplete(boolean success, Object data, int totalRecords) {
                                    if (success) {
                                        mErrorLayout.showError(data.toString(), true, MsgType.Info);
                                        finish();
                                    } else {
                                        mErrorLayout.showError(data.toString(), true, MsgType.Info);
                                    }
                                }
                            });
                        }
                    }
                });

        PageNo = 1;
        FontLoader.setRobotoRegularTypeface(mSearchET);
        mSearchET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence searchStr, int arg1, int arg2, int arg3) {
                if (searchStr.length() == 0) {
                    mUserLV.setVisibility(View.GONE);
                    mNoRecordTV.setVisibility(View.VISIBLE);
                    mInviteUserAL = new ArrayList<PageMemberUser>();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        new SearchHandler(mSearchET).setSearchListener(new SearchListener() {

            @Override
            public void onSearch(String text) {
                getSearchUserServerRequest(mPageGUID, mSearchET.getText().toString().trim());
            }
        }).setClearView(mClearIB);
        mSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utility.hideSoftKeyboard(mSearchET);
                    return true;
                }
                return false;
            }
        });
    }

    public static Intent getIntent(Context context, String mPageGUID) {
        Intent intent = new Intent(context, AddMembersToPageActivity.class);
        intent.putExtra("PageGUID", mPageGUID);
        return intent;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
        overridePendingTransition(0, R.anim.slide_down_dialog);
    }

    private Set<PageMemberUser> mAllSelections = new HashSet<PageMemberUser>();

    /**
     * Display adapter of search result received from api
     */
    private void setSearchResultFriendsAdapter(final ArrayList<PageMemberUser> mAL) {
        mInviteUserAL = mAL;
        mAdapter = new InviteToUserAdapter(mContext);
        if (mInviteUserAL.size() == 0) {
            mUserLV.setVisibility(View.GONE);
            mNoRecordTV.setVisibility(View.VISIBLE);
        } else {
            mNoRecordTV.setVisibility(View.GONE);
            mUserLV.setVisibility(View.VISIBLE);
            mAdapter.setList(mInviteUserAL);
            mUserLV.setAdapter(mAdapter);
            mUserLV.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    PageMemberUser mPageMemberUser = (PageMemberUser) mAdapter.getItem(position);
                    mPageMemberUser.isSelected = !mPageMemberUser.isSelected;
                    if (mPageMemberUser.isSelected) {
                        mAllSelections.add(mPageMemberUser);
                    } else {
                        for (PageMemberUser sm : mAllSelections) {
                            if (sm.mUserGUID.equalsIgnoreCase(mPageMemberUser.mUserGUID)) {
                                mAllSelections.remove(sm);
                                break;
                            }
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    /**
     * Call API to get users list according to search query, display for add to team page as member
     */
    private void getSearchUserServerRequest(String mGroupGUID, String mSearchKey) {
        String searchQuery = mSearchET.getText().toString().trim();
        if (!TextUtils.isEmpty(searchQuery) && searchQuery.length() > 1) {
            SearchFriendsForGroupRequest mRequest = new SearchFriendsForGroupRequest(mContext);

            FilterBy mFilter = new FilterBy();
            mFilter.setSearchKeyword(searchQuery);
            mRequest.setLoader(findViewById(R.id.loading_center_pb));
            mRequest.getUserListInServer(mGroupGUID, mSearchKey);
            mRequest.setRequestListener(new RequestListener() {

                @Override
                public void onComplete(boolean success, Object data, int totalRecords) {
                    if (success) {
                        setSearchResultFriendsAdapter((ArrayList<PageMemberUser>) data);
                    } else {
                        setSearchResultFriendsAdapter(new ArrayList<PageMemberUser>());
                    }
                }
            });
        }
    }
}