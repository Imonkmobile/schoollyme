package com.schollyme.team;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.schollyme.R;
import com.schollyme.model.ConferenceModel;
import com.vinfotech.utility.FontLoader;

/**
 * Adapter class. This may be useful in display conference list
 * @author Ravi Bhandari
 *
 */
public class ConferencesListAdapter extends BaseAdapter {

	private ConferencesViewHolder mConferencesViewHolder;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<ConferenceModel> mConferencesAL;
	private List<ConferenceModel> mFilteredState = new ArrayList<ConferenceModel>();

	public ConferencesListAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(List<ConferenceModel> mAList) {
		this.mConferencesAL = mAList;
		setFilter(null);
	}

	public ArrayList<ConferenceModel> getSelections() {
		ArrayList<ConferenceModel> selections = new ArrayList<ConferenceModel>();
		if (null != mConferencesAL) {
			for (ConferenceModel conferenceModel : mConferencesAL) {
				if (conferenceModel.isSelected) {
					selections.add(conferenceModel);
				}
			}
		}
		return selections;
	}

	@Override
	public int getCount() {
		return null == mFilteredState ? 0 : mFilteredState.size();
	}

	@Override
	public ConferenceModel getItem(int position) {
		return mFilteredState.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		mConferencesViewHolder = new ConferencesViewHolder();
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_item_view, parent, false);
			mConferencesViewHolder.mTitleTV = (TextView) convertView.findViewById(R.id.title_tv);
			mConferencesViewHolder.mSelectIB = (ImageButton) convertView.findViewById(R.id.select_ib);
			convertView.setTag(mConferencesViewHolder);
		} else {
			mConferencesViewHolder = (ConferencesViewHolder) convertView.getTag();
		}
		mConferencesViewHolder.mTitleTV.setText(mFilteredState.get(position).mConferenceName);
		mConferencesViewHolder.mTitleTV.setTag(position);

		mConferencesViewHolder.mSelectIB.setImageResource(R.drawable.icon_blue_tick);
		if (mFilteredState.get(position).isSelected) {
			mConferencesViewHolder.mSelectIB.setVisibility(View.VISIBLE);
		} else {
			mConferencesViewHolder.mSelectIB.setVisibility(View.GONE);
		}
		FontLoader.setRobotoRegularTypeface(mConferencesViewHolder.mTitleTV);
		convertView.setClickable(false);

		return convertView;
	}

	private class ConferencesViewHolder {
		public TextView mTitleTV;
		public ImageButton mSelectIB;
	}
	
	public void setFilter(String text) {
		mFilteredState.clear();

		if (null != mConferencesAL) {
			if (TextUtils.isEmpty(text)) {
				mFilteredState.addAll(mConferencesAL);
			} else {
				for (ConferenceModel conferenceModel : mConferencesAL) {
					if (conferenceModel.mConferenceName.toLowerCase().startsWith(text.toLowerCase())
							|| conferenceModel.mConferenceName.toLowerCase().contains((" "+text.toLowerCase()))) {
						mFilteredState.add(conferenceModel);
					}
				}
			}
		}
		notifyDataSetChanged();
	}
}