package com.schollyme.team;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.schollyme.R;
import com.schollyme.activity.BaseActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.friends.FriendProfileActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.PageMemberUser;
import com.vinfotech.request.TeamMembersListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;

import java.util.ArrayList;

/**
 * Activity class. This may be useful in display list of team members using adapter
 *
 * @author Ravi Bhandari
 */
public class TeamMembersActivity extends BaseActivity implements OnClickListener, OnRefreshListener {

    private Context mContext;
    private String mPageGUID, mAdminGUID, mPageCategoryID;
    private ArrayList<PageMemberUser> mMembers;
    private int mPageIndex = 1;
    private TeamMembersAdapter mMembersAdapter;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private boolean loadingFlag = false;
    private ListView mMembersLV;
    private EditText mSearchET;
    private ImageButton mClearIB;
    private ErrorLayout mErrorLayout;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.team_members_activity);
        mContext = this;

        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        mPageGUID = getIntent().getStringExtra("TeamPageGUID");
        mAdminGUID = getIntent().getStringExtra("AdminGUID");
        mPageCategoryID = getIntent().getStringExtra("PageCategoryID");

        initHeader();

        mMembersLV = (ListView) findViewById(R.id.team_members_lv);
        mClearIB = (ImageButton) findViewById(R.id.clear_text_ib);
        mSearchET = (EditText) findViewById(R.id.search_et);
        mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red,
                R.color.app_text_color, R.color.text_hint_color);
        mSwipeRefreshWidget.setOnRefreshListener(this);

        new SearchHandler(mSearchET).setSearchListener(new SearchListener() {

            @Override
            public void onSearch(String text) {

                if (!TextUtils.isEmpty(text) && text.trim().length() > 1) {
                    mPageIndex = 1;
                    mMembers = new ArrayList<PageMemberUser>();
                    getTeamMembersServerRequest(mPageGUID, text);
                } else if (text.trim().length() == 0) {
                    mPageIndex = 1;
                    mMembers = new ArrayList<PageMemberUser>();
                    getTeamMembersServerRequest(mPageGUID, "");
                }
            }
        }).setClearView(mClearIB);

    }

    private void initHeader() {

        try {
            if (mAdminGUID.equals(new LogedInUserModel(mContext).mUserGUID)) {
                setHeader(findViewById(R.id.header_layout), R.drawable.icon_back,
                        R.drawable.selector_add_page_header, getResources().getString(R.string.members_tab),
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        }, new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                startActivity(AddMembersToPageActivity.getIntent(mContext, mPageGUID));
                                //((Activity) mContext).overridePendingTransition(R.anim.slide_up_dialog, 0);
                            }
                        });
            } else {
                setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, 0,
                        getResources().getString(R.string.members_tab),
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        }, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        mPageIndex = 1;
        mMembers = new ArrayList<PageMemberUser>();
        getTeamMembersServerRequest(mPageGUID, "");
        super.onStart();
    }

    private void setTeamMembersAdapter() {
        mMembersAdapter = new TeamMembersAdapter(mContext, mAdminGUID, mErrorLayout, mPageGUID, mPageCategoryID);
        mMembersAdapter.setList(mMembers);
        mMembersLV.setAdapter(mMembersAdapter);

        mMembersLV.setOnScrollListener(new OnScrollListener() {
            boolean bReachedListEnd = false;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
                    Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
                    if (null != mMembersAdapter && !loadingFlag) {
                        mPageIndex++;
                        loadingFlag = true;
                        getTeamMembersServerRequest(mPageGUID, "");
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount / 2));
            }
        });
        mMembersLV.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {
                PageMemberUser mModel = mMembers.get(pos);

                if (new LogedInUserModel(mContext).mUserGUID.equals(mModel.mUserGUID)) {
                    mContext.startActivity(DashboardActivity.getIntent(mContext, 7, ""));
                } else {
                    mContext.startActivity(FriendProfileActivity.getIntent(mContext,
                            mModel.mUserGUID, mModel.mUserProfileLink, "TeamPage"));
                }
            }
        });
    }

    public static Intent getIntent(Context mContext, String mTeamPageGUID, String mAdminGUID, String mPageCategoryID) {
        Intent intent = new Intent(mContext, TeamMembersActivity.class);
        intent.putExtra("TeamPageGUID", mTeamPageGUID);
        intent.putExtra("AdminGUID", mAdminGUID);
        intent.putExtra("PageCategoryID", mPageCategoryID);
        return intent;
    }

    @Override
    public void onClick(View v) {

    }

    private void getTeamMembersServerRequest(String mTeamGUID, String mSearchText) {
        TeamMembersListRequest mRequest = new TeamMembersListRequest(mContext);
        mRequest.TeamMembersListServerRequest(mSearchText, mTeamGUID, "All", mPageIndex);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                loadingFlag = false;
                mSwipeRefreshWidget.setRefreshing(false);
                if (success) {
                    ArrayList<PageMemberUser> mList = (ArrayList<PageMemberUser>) data;
                    mMembers.addAll(mList);
                    if (null == mMembersAdapter) {
                        setTeamMembersAdapter();
                    } else {
                        mMembersAdapter.setList(mMembers);
                    }
                } else {
                    final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext,
                            data.toString(), getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {
                                @Override
                                public void onOkBUtton() {
                                    finish();
                                }
                            });
                    builder.setCancelable(false);
                    builder.setOnKeyListener(new Dialog.OnKeyListener() {
                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode,
                                             KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                finish();
                                builder.dismiss();
                            }
                            return true;
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        mPageIndex = 1;
        mMembers = new ArrayList<PageMemberUser>();
        getTeamMembersServerRequest(mPageGUID, "");
    }
}