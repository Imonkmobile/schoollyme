package com.schollyme.team;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.schollyme.R;
import com.schollyme.activity.BaseActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.team.BroadcastFragment.OnTextChangeListener;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.MessageMemberRequest;
import com.vinfotech.widget.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

public class NewBroadcastActivity extends BaseActivity {

    private static final int PAGE_COUNT = 3;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private ViewPager mGenericVp;
    private GridPagerAdapter mGridPagerAdapter;
    private ErrorLayout mErrorLayout;
    private String mGroupGUID, mGroupName;
    private String mBroadcastText;
    private View mHeaderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_broadcast_activity);

        mGroupGUID = getIntent().getStringExtra("groupGUID");
        mGroupName = getIntent().getStringExtra("groupName");
        if (TextUtils.isEmpty(mGroupGUID)) {
            Toast.makeText(this, "Invalid GroupGUID", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mHeaderLayout = findViewById(R.id.header_layout);
        mGridPagerAdapter = new GridPagerAdapter(getSupportFragmentManager());
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_container_ll));

        mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.sliding_tabs_psts);
        mPagerSlidingTabStrip.setTabPaddingLeftRight((int) getResources().getDimension(R.dimen.space_tinny4));
        mGenericVp = (ViewPager) findViewById(R.id.generic_vp);
        mGenericVp.setOffscreenPageLimit(2);
        mGenericVp.setAdapter(mGridPagerAdapter);

        mPagerSlidingTabStrip.setViewPager(mGenericVp, this);
        mPagerSlidingTabStrip.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                mGridPagerAdapter.clearSelection();
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

        int currentPageToShow = getIntent().getIntExtra("currentPageToShow", 0);
        mGenericVp.setCurrentItem(currentPageToShow);
    }

    public static Intent getIntent(Context context, int currentPageToShow, String groupGUID, String groupName) {
        Intent intent = new Intent(context, NewBroadcastActivity.class);
        intent.putExtra("currentPageToShow", currentPageToShow);
        intent.putExtra("groupGUID", groupGUID);
        intent.putExtra("groupName", groupName);
        return intent;
    }

    public class GridPagerAdapter extends FragmentStatePagerAdapter {
        private List<BroadcastFragment> mBroadcastFragments;

        public GridPagerAdapter(FragmentManager fm) {
            super(fm);
            mBroadcastFragments = new ArrayList<BroadcastFragment>();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String tabTitle = "";
            switch (position) {
                case 0:
                    tabTitle = getString(R.string.all_tab);
                    break;
                case 1:
                    tabTitle = getString(R.string.parent_tab);
                    break;
                case 2:
                    tabTitle = getString(R.string.athlete_tab);
                    break;

            }
            return tabTitle;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            BroadcastFragment fragment = null;

            switch (position) {
                case 0:
                    fragment = BroadcastFragment.newInstance(mErrorLayout, mGroupGUID, new HeaderLayout(mHeaderLayout),
                            mGroupName, MessageMemberRequest.FILTER_ALL, mOnTextChangeListener);
                    break;
                case 1:
                    fragment = BroadcastFragment.newInstance(mErrorLayout, mGroupGUID, new HeaderLayout(mHeaderLayout),
                            mGroupName, MessageMemberRequest.FILTER_PARENT, mOnTextChangeListener);
                    break;
                case 2:
                    fragment = BroadcastFragment.newInstance(mErrorLayout, mGroupGUID, new HeaderLayout(mHeaderLayout),
                            mGroupName, MessageMemberRequest.FILTER_ATHLETE, mOnTextChangeListener);
                    break;

            }
            mBroadcastFragments.add(fragment);
            return fragment;
        }

        public BroadcastFragment getBroadcastFragment(int position) {
            return mBroadcastFragments.get(position);
        }

        private OnTextChangeListener mOnTextChangeListener = new OnTextChangeListener() {

            @Override
            public void onTextChange(String text) {
                mBroadcastText = text;
                int position = mGenericVp.getCurrentItem();
                for (int i = 0; i < getCount(); i++) {
                    if (i != position) {
                        mGridPagerAdapter.mBroadcastFragments.get(i).setSearchText(mBroadcastText);
                    }
                }
            }
        };

        private void clearSelection() {
            for (int i = 0; i < getCount(); i++) {
                mGridPagerAdapter.mBroadcastFragments.get(i).clearSelection();
            }
        }
    }

}
