package com.schollyme.team;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.activity.BaseActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.Teams;
import com.vinfotech.request.FanPageJoinRequest;
import com.vinfotech.request.TeamPageDeactivateRequest;
import com.vinfotech.request.TeamPageDetailRequest;
import com.vinfotech.request.TeamPageLeaveRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

/**
 * Activity class. This may be useful in display team about details
 *
 * @author Ravi Bhandari
 */
public class TeamAboutActivity extends BaseActivity implements OnClickListener {

    private TeamAboutActivityViewHolder mTeamAboutActivityViewHolder;
    private Context mContext;
    private Teams mTeamPageDetails;
    private static String mTeamGUID, mAdminGUID;
    private ErrorLayout mErrorLayout;
    private int position;
    private String actionType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.team_about_activity);
        mContext = this;
        mAdminGUID = getIntent().getStringExtra("AdminGUID");
        mTeamGUID = getIntent().getStringExtra("TeamPageGUID");
        position = getIntent().getIntExtra("Position", 0);
        initHeader();

        mErrorLayout = new ErrorLayout(findViewById(R.id.main_rl));
        mTeamAboutActivityViewHolder = new TeamAboutActivityViewHolder(findViewById(R.id.main_rl), this);
        FontLoader.setRobotoMediumTypeface(mTeamAboutActivityViewHolder.mTeamNameTV,
                mTeamAboutActivityViewHolder.mSportsHeaderTV, mTeamAboutActivityViewHolder.mGroupInfoHeaderTV,
                mTeamAboutActivityViewHolder.mConferenceHeaderTV, mTeamAboutActivityViewHolder.mCollegeHeaderTV);

        FontLoader.setRobotoRegularTypeface(mTeamAboutActivityViewHolder.mTeamNameTV,
                mTeamAboutActivityViewHolder.mTeamTypeTV,
                mTeamAboutActivityViewHolder.mTeamDescriptionTV, mTeamAboutActivityViewHolder.mTeamSportsTV,
                mTeamAboutActivityViewHolder.mPageAdminNameTV, mTeamAboutActivityViewHolder.mPageAdminStatusTV,
                mTeamAboutActivityViewHolder.mTeamConferenceTV, mTeamAboutActivityViewHolder.mTeamCollegeTV,
                mTeamAboutActivityViewHolder.mGroupInfoTV, mTeamAboutActivityViewHolder.mDeactivatePageTV);

    }

    private void initHeader() {

        try {
            if (mAdminGUID.equals(new LogedInUserModel(mContext).mUserGUID)) {
                setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_edit,
                        getResources().getString(R.string.About_tab),
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        }, new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(TeamPageEditActivity.getIntent(mContext, mTeamGUID, mAdminGUID));
                            }
                        });
            } else {
                setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, 0,
                        getResources().getString(R.string.About_tab),
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        }, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        getTeamPageDetailsServerRequest(mTeamGUID);
    }

    public static Intent getIntent(Context mContext, String mTeamPageGUID, String mAdminGUID, int position) {
        Intent intent = new Intent(mContext, TeamAboutActivity.class);
        intent.putExtra("TeamPageGUID", mTeamPageGUID);
        intent.putExtra("AdminGUID", mAdminGUID);
        intent.putExtra("Position", position);
        return intent;
    }

    @SuppressLint("DefaultLocale")
    @SuppressWarnings("deprecation")
    private void setData() {
        mTeamAboutActivityViewHolder.mTeamNameTV.setText(mTeamPageDetails.mTeamName);
        mTeamAboutActivityViewHolder.mTeamTypeTV.setText(mTeamPageDetails.mTeamSportsName);
        mTeamAboutActivityViewHolder.mPageAdminNameTV.setText(mTeamPageDetails.mAdminUserFName);

		/*String imageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mTeamImageURL;
        ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mTeamAboutActivityViewHolder.mTeamLogoIV ,
				ImageLoaderUniversal.option_Round_Image);*/

        String adminImageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mAdminUserProfilePicURL;
        ImageLoaderUniversal.ImageLoadRound(mContext, adminImageUrl, mTeamAboutActivityViewHolder.mPageAdminLogoIV,
                ImageLoaderUniversal.option_Round_Image);

        if (mTeamPageDetails.isPublic.equals("1")) {
            mTeamAboutActivityViewHolder.mTeamTypeTV.setText(getResources().getString(R.string.public_group));
        } else {
            mTeamAboutActivityViewHolder.mTeamTypeTV.setText(getResources().getString(R.string.private_group));
        }

        mTeamAboutActivityViewHolder.mTeamDescriptionTV.setText(mTeamPageDetails.mPageDescription);
        mTeamAboutActivityViewHolder.mTeamSportsTV.setText(mTeamPageDetails.mTeamSportsName);
        mTeamAboutActivityViewHolder.mTeamConferenceTV.setText(mTeamPageDetails.mPageConferenceName);
        mTeamAboutActivityViewHolder.mTeamCollegeTV.setText(mTeamPageDetails.mPageCollegeName.trim());
        if (Integer.parseInt(mTeamPageDetails.mTeamMembersCount) < 2) {
            mTeamAboutActivityViewHolder.mGroupInfoTV.setText(mTeamPageDetails.mTeamMembersCount + " " + getResources().getString(R.string.member_tab));
        } else {
            mTeamAboutActivityViewHolder.mGroupInfoTV.setText(mTeamPageDetails.mTeamMembersCount + " " + getResources().getString(R.string.members_tab));
        }

        mTeamAboutActivityViewHolder.mPageAdminStatusTV.setText(getResources().getString(R.string.page_admin_label));


        if (mTeamPageDetails.mPageCategoryID.equals("1")) {
            String imageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mTeamImageURL;
            ImageLoaderUniversal.ImageLoadRoundForTeamPage(mContext, imageUrl, mTeamAboutActivityViewHolder.mTeamLogoIV,
                    ImageLoaderUniversal.option_Round_Image_for_teampage);
        } else {
            String mTeamName = mTeamPageDetails.mTeamName.trim();
            if (!TextUtils.isEmpty(mTeamName)) {
                mTeamAboutActivityViewHolder.mTeamFirstCharTV.setText(String.valueOf(mTeamName.charAt(0)).toUpperCase());
            }
            if (position % 4 == 0) {
                mTeamAboutActivityViewHolder.mTeamLogoIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo1));
            } else if (position % 4 == 1) {
                mTeamAboutActivityViewHolder.mTeamLogoIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo2));
            } else if (position % 4 == 2) {
                mTeamAboutActivityViewHolder.mTeamLogoIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo3));
            } else if (position % 4 == 3) {
                mTeamAboutActivityViewHolder.mTeamLogoIV.setBackgroundDrawable(mContext.getResources()
                        .getDrawable(R.drawable.circle_blank_logo4));
            }
        }
    }

    private void setViewAccordingTouserType() {
        if (mTeamPageDetails.mPageCategoryID.equals("1")) {
            mTeamAboutActivityViewHolder.mConferenceHeaderTV.setVisibility(View.VISIBLE);
            mTeamAboutActivityViewHolder.mCollegeHeaderTV.setVisibility(View.VISIBLE);
            mTeamAboutActivityViewHolder.mTeamConferenceTV.setVisibility(View.VISIBLE);
            mTeamAboutActivityViewHolder.mTeamCollegeTV.setVisibility(View.VISIBLE);
            mTeamAboutActivityViewHolder.mDevider1.setVisibility(View.VISIBLE);
            mTeamAboutActivityViewHolder.mDevider2.setVisibility(View.VISIBLE);

        } else {
            mTeamAboutActivityViewHolder.mConferenceHeaderTV.setVisibility(View.GONE);
            mTeamAboutActivityViewHolder.mCollegeHeaderTV.setVisibility(View.GONE);
            mTeamAboutActivityViewHolder.mTeamConferenceTV.setVisibility(View.GONE);
            mTeamAboutActivityViewHolder.mTeamCollegeTV.setVisibility(View.GONE);
            mTeamAboutActivityViewHolder.mDevider1.setVisibility(View.GONE);
            mTeamAboutActivityViewHolder.mDevider2.setVisibility(View.GONE);

        }
        if (mTeamPageDetails.isGroupAdmin) {

            if (mTeamPageDetails != null) {
                int optionText;
                if (mTeamPageDetails.mStatusID.equals("2")) {
                    optionText = R.string.deactivate_label;
                } else {
                    optionText = R.string.activate_label;
                }
                mTeamAboutActivityViewHolder.mDeactivatePageTV.setText(getResources().getString(optionText));
            }
        } else if (mTeamPageDetails.isGroupMember) {
            mTeamAboutActivityViewHolder.mDeactivatePageTV.setText(getResources().getString(R.string.leave_header));
        } else {
            mTeamAboutActivityViewHolder.mDeactivatePageTV.setText(getResources().getString(R.string.join_label));
        }
        setData();
    }

    private class TeamAboutActivityViewHolder {

        private ImageView mTeamLogoIV, mPageAdminLogoIV;
        private TextView mTeamNameTV, mTeamTypeTV, mTeamDescriptionTV, mTeamSportsTV, mPageAdminNameTV;
        private TextView mPageAdminStatusTV, mTeamConferenceTV, mTeamCollegeTV, mGroupInfoTV, mDeactivatePageTV;
        private TextView mSportsHeaderTV, mGroupInfoHeaderTV, mConferenceHeaderTV, mCollegeHeaderTV;
        private View mDevider1, mDevider2;
        public TextView mTeamFirstCharTV;

        private TeamAboutActivityViewHolder(View view, OnClickListener listener) {

            mTeamLogoIV = (ImageView) view.findViewById(R.id.profile_image_iv);
            mPageAdminLogoIV = (ImageView) view.findViewById(R.id.admin_image_iv);

            mTeamNameTV = (TextView) view.findViewById(R.id.team_name_tv);
            mTeamTypeTV = (TextView) view.findViewById(R.id.team_type_tv);
            mTeamDescriptionTV = (TextView) view.findViewById(R.id.description_tv);
            mTeamSportsTV = (TextView) view.findViewById(R.id.sports_value_tv);
            mPageAdminNameTV = (TextView) view.findViewById(R.id.admin_name_tv);
            mPageAdminStatusTV = (TextView) view.findViewById(R.id.admin_status_tv);
            mTeamConferenceTV = (TextView) view.findViewById(R.id.conference_tv);
            mTeamCollegeTV = (TextView) view.findViewById(R.id.college_tv);
            mGroupInfoTV = (TextView) view.findViewById(R.id.group_info_tv);
            mDeactivatePageTV = (TextView) view.findViewById(R.id.deactivate_tv);
            mTeamFirstCharTV = (TextView) view.findViewById(R.id.team_name_fchar_tv);

            mSportsHeaderTV = (TextView) view.findViewById(R.id.sports_header_tv);
            mGroupInfoHeaderTV = (TextView) view.findViewById(R.id.group_info_header_tv);
            mConferenceHeaderTV = (TextView) view.findViewById(R.id.conference_header_tv);
            mCollegeHeaderTV = (TextView) view.findViewById(R.id.college_header_tv);

            mDevider1 = view.findViewById(R.id.divider_view_conference);
            mDevider2 = view.findViewById(R.id.divider_view_college);

            mGroupInfoTV.setOnClickListener(listener);
            mDeactivatePageTV.setOnClickListener(listener);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.group_info_tv:
                if (null != mTeamPageDetails)
                    startActivity(TeamMembersActivity.getIntent(mContext, mTeamGUID, mTeamPageDetails.mAdminUserGUID, mTeamPageDetails.mPageCategoryID));
                break;
            case R.id.deactivate_tv:
                String optionText;
                if (null != mTeamPageDetails && mTeamPageDetails.isGroupAdmin) {

                    if (mTeamPageDetails.mStatusID.equals("2")) {
                        optionText = mContext.getResources().getString(R.string.deactivate_page_confirmation);
                        actionType = Config.IN_ACTIVATE_PAGE;
                    } else {
                        optionText = mContext.getResources().getString(R.string.activate_page_confirmation);
                        actionType = Config.ACTIVATE_PAGE;
                    }

                } else if (mTeamPageDetails != null && mTeamPageDetails.isGroupMember) {

                    optionText = mContext.getResources().getString(R.string.leave_page_confirmation);
                } else {
                    optionText = mContext.getResources().getString(R.string.join_page_confirmation);
                }

                DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps,
                        mContext.getResources().getString(R.string.app_name), optionText, new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (null != mTeamPageDetails && mTeamPageDetails.isGroupAdmin) {
                                    deactivatePageRequest(mTeamPageDetails.mTeamGUID, actionType);
                                } else if (null != mTeamPageDetails && mTeamPageDetails.isGroupMember) {
                                    leavePageRequest(mTeamPageDetails.mTeamGUID, new LogedInUserModel(mContext).mUserGUID);
                                } else if (null != mTeamPageDetails) {
                                    joinPageRequest(mTeamPageDetails.mTeamGUID);
                                }
                            }
                        }, new OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });

                break;
            default:
                break;
        }
    }

    private void getTeamPageDetailsServerRequest(String mTeamGUID) {

        TeamPageDetailRequest mRequest = new TeamPageDetailRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mTeamPageDetails = (Teams) data;
                    setViewAccordingTouserType();
                } else {
                    final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {
                                    finish();
                                }
                            });
                    builder.setCancelable(false);
                    builder.setOnKeyListener(new Dialog.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode,
                                             KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                finish();
                                builder.dismiss();
                            }
                            return true;
                        }
                    });
                }
            }
        });
        mRequest.TeamDetailsServerRequest(mTeamGUID);
    }

    /**
     * Call API to Deactivate a fan/team page
     * only page admin/owner have permision to Deactivate a page.
     */
    private void deactivatePageRequest(String mGUID, String actionType) {
        TeamPageDeactivateRequest mRequest = new TeamPageDeactivateRequest(mContext);
        mRequest.TeamDeactivateRequest(mGUID, actionType);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    getTeamPageDetailsServerRequest(mTeamGUID);
                    mErrorLayout.showError(data.toString(), true, MsgType.Success);
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });

    }

    /**
     * Call  api to leave current group,
     * This action can be perform by page members only
     */
    private void leavePageRequest(String mTeamGUID, String UserGUID) {
        TeamPageLeaveRequest mRequest = new TeamPageLeaveRequest(mContext);
        mRequest.TeamPagesLeaveRequest(mTeamGUID, UserGUID);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), true, MsgType.Success);

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            Intent intent;
                            if (mTeamPageDetails.mPageCategoryID.equals("1")) {
                                intent = TeamActivity.getIntent(mContext, 0);
                            } else {
                                intent = TeamActivity.getIntent(mContext, 1);
                            }
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }, 2000);

                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
    }


    /**
     * Call API to join a suggested fan page
     */
    private void joinPageRequest(String mTeamPageGUID) {
        FanPageJoinRequest mRequest = new FanPageJoinRequest(mContext);
        mRequest.getFanPageJoinRequest(mTeamPageGUID, new LogedInUserModel(mContext).mUserGUID);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), true, MsgType.Success);
                    getTeamPageDetailsServerRequest(mTeamGUID);
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });

    }
}