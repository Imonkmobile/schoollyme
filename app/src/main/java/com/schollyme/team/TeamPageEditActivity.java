package com.schollyme.team;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.media.MediaPickerActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.Teams;
import com.vinfotech.request.CreatePageRequest;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.request.TeamPageDetailRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.utility.Utility;

public class TeamPageEditActivity extends MediaPickerActivity implements OnClickListener {

    private Context mContext;
    private EditPageViewHolder mEditPageViewHolder;
    private ErrorLayout mErrorLayout;
    private Teams mTeamPageDetails;
    private String mTeamGUID;
    private int mIsPublic = 2;
    private Bitmap mPhotoBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.team_page_create);
        mContext = this;
        initHeader();

        mErrorLayout = new ErrorLayout(findViewById(R.id.main_ll));
        mEditPageViewHolder = new EditPageViewHolder(findViewById(R.id.main_ll), this);
        mTeamGUID = getIntent().getStringExtra("TeamPageGUID");
        getTeamPageDetailsServerRequest(mTeamGUID);
    }

    private void initHeader() {

        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm,
                getResources().getString(R.string.edit_page), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Utility.hideSoftKeyboard(mEditPageViewHolder.mPageDescripionET);
                        finish();
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        if (HttpConnector.getConnectivityStatus(mContext) == 0) {
                            DialogUtil.showOkDialog(mContext, getResources().getString(R.string.No_internet_connection), getResources().getString(R.string.app_name));
                        } else {

                            String mDescription = mEditPageViewHolder.mPageDescripionET.getText().toString();
                            if (TextUtils.isEmpty(mDescription)) {
                                mErrorLayout.showError(getResources().getString(R.string.page_description_require), true, MsgType.Error);
                            } else {
                                if (null != cropImageUri/* && !TextUtils.isEmpty(cropImageUri)*/) {
                                    uploadMediaServerRequest();
                                } else {
                                    editPageServerRequest(mTeamPageDetails);
                                }
                            }
                        }
                    }
                });
    }

    private void setData() {
        mEditPageViewHolder.mPageNameET.setText(mTeamPageDetails.mTeamName);
        //	mEditPageViewHolder.mTeamTypeTV.setText(mTeamPageDetails.mTeamSportsName);

        String imageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mTeamImageURL;
        ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mEditPageViewHolder.mPageLogoIV,
                ImageLoaderUniversal.option_Round_Image);


        mEditPageViewHolder.mPrivacySwitch.setClickable(true);
        if (mTeamPageDetails.isPublic.equals("1")) {
            mEditPageViewHolder.mPagePrivacyTV.setText(getResources().getString(R.string.public_group));
            mEditPageViewHolder.mPrivacySwitch.setChecked(false);
        } else {
            mEditPageViewHolder.mPagePrivacyTV.setText(getResources().getString(R.string.private_group));
            mEditPageViewHolder.mPrivacySwitch.setChecked(true);
        }

        mEditPageViewHolder.mPageDescripionET.setText(mTeamPageDetails.mPageDescription);
        mEditPageViewHolder.mPageSportsTV.setText(mTeamPageDetails.mTeamSportsName);
        mEditPageViewHolder.mPageConferenceTV.setText(mTeamPageDetails.mPageConferenceName);
        mEditPageViewHolder.mPageCollegeTV.setText(mTeamPageDetails.mPageCollegeName.trim());
        mEditPageViewHolder.mPageDescripionET.requestFocus();
        mEditPageViewHolder.mPageDescripionET.setSelection(mEditPageViewHolder.mPageDescripionET.getText().toString().length());
        mIsPublic = Integer.parseInt(mTeamPageDetails.isPublic);
    }

    private void setViewAccordingTouserType() {
        if (mTeamPageDetails.mPageCategoryID.equals("1")) {
            mEditPageViewHolder.mPageConferenceTV.setVisibility(View.VISIBLE);
            mEditPageViewHolder.mPageConferenceLabelTV.setVisibility(View.VISIBLE);
            mEditPageViewHolder.mPageCollegeTV.setVisibility(View.VISIBLE);
            mEditPageViewHolder.mPageCollegeLabelTV.setVisibility(View.VISIBLE);
            mEditPageViewHolder.mPageLogoIV.setVisibility(View.VISIBLE);
            mEditPageViewHolder.mPrivacySwitch.setVisibility(View.GONE);
        } else {
            mEditPageViewHolder.mPageConferenceTV.setVisibility(View.GONE);
            mEditPageViewHolder.mPageConferenceLabelTV.setVisibility(View.GONE);
            mEditPageViewHolder.mPageCollegeTV.setVisibility(View.GONE);
            mEditPageViewHolder.mPageCollegeLabelTV.setVisibility(View.GONE);
            mEditPageViewHolder.mPageLogoIV.setVisibility(View.GONE);
            mEditPageViewHolder.mPrivacySwitch.setVisibility(View.VISIBLE);
        }
        setData();
    }


    private void getTeamPageDetailsServerRequest(String mTeamGUID) {

        TeamPageDetailRequest mRequest = new TeamPageDetailRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mTeamPageDetails = (Teams) data;
                    setViewAccordingTouserType();
                } else {
                    DialogUtil.showOkDialog(mContext, data.toString(), getResources().getString(R.string.app_name));
                }
            }
        });
        mRequest.TeamDetailsServerRequest(mTeamGUID);
    }

    private void editPageServerRequest(Teams mTeamDetail) {
        CreatePageRequest mRequest = new CreatePageRequest(mContext);

        String mTeamDexcription = mEditPageViewHolder.mPageDescripionET.getText().toString();
        int publicFlag = mIsPublic;//Integer.parseInt(mTeamDetail.isPublic);
        mRequest.CreatePageServerRequest(mTeamDetail.mTeamName, mTeamDexcription,
                mTeamDetail.mTeamImageURL, mTeamDetail.mTeamCoverImageURL,
                mTeamDetail.mPageCategoryID, publicFlag, mTeamDetail.mTeamSportsId,
                mTeamDetail.mPageCollegeId, mTeamDetail.mPageConferenceId, mTeamDetail.mTeamGUID);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {


                if (success) {
                    mErrorLayout.showError(data.toString(), false, MsgType.Success);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            finish();
                        }
                    }, 2000);
                } else {
                    mErrorLayout.showError(data.toString(), false, MsgType.Error);
                }
            }
        });
    }

    public static Intent getIntent(Context mContext, String mTeamPageGUID, String mAdminGUID) {
        Intent intent = new Intent(mContext, TeamPageEditActivity.class);
        intent.putExtra("TeamPageGUID", mTeamPageGUID);
        intent.putExtra("AdminGUID", mAdminGUID);
        return intent;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.page_logo_iv:
                displayImagePickerPopup();
                break;

            default:
                break;
        }
    }

    private void displayImagePickerPopup() {

        DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
            @Override
            public void onItemClick(int position, String item) {
                if (position == 0) {
                    openCamera();
                } else {
                    openGallery();
                }
            }

            @Override
            public void onCancel() {

            }
        }, mContext.getString(R.string.take_picture), mContext.getString(R.string.load_from_library));
    }

    @Override
    protected void onGalleryImageSelected(String fileUri, Bitmap bitmap) {

    }

    @Override
    protected void onCameraImageSelected(String fileUri, Bitmap bitmap) {
        mPhotoBitmap = bitmap;
        mEditPageViewHolder.mPageLogoIV.setImageBitmap(ImageUtil.getCircledBitmap(mPhotoBitmap));
    }

    @Override
    protected void onVideoCaptured(String videoPath) {

    }

    @Override
    protected void onMediaPickCanceled(int reqCode) {

    }

    private void uploadMediaServerRequest() {
        MediaUploadRequest mRequest = new MediaUploadRequest(mContext, false);
        String fileName = ImageUtil.getFileNameFromURI(cropImageUri, mContext);
        File mFile = new File(cropImageUri.getPath());
        final LogedInUserModel mLoggedInUserModel = new LogedInUserModel(mContext);
        BaseRequest.setLoginSessionKey(mLoggedInUserModel.mLoginSessionKey);
        mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_GROUPS, "",
                MediaUploadRequest.TYPE_PROFILE);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    AlbumMedia mAlbumMedia = (AlbumMedia) data;
                    mTeamPageDetails.mTeamImageURL = mAlbumMedia.ImageName;
                    editPageServerRequest(mTeamPageDetails);
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
    }

    public class EditPageViewHolder {

        private EditText mPageNameET, mPageDescripionET;
        private TextView mPageSportsTV, mPageConferenceTV, mPageConferenceLabelTV, mPageCollegeTV, mPageCollegeLabelTV;
        private TextView mPagePrivacyTV;
        private ImageView mPageLogoIV;
        private Switch mPrivacySwitch;

        public EditPageViewHolder(View view, OnClickListener listener) {
            mPageLogoIV = (ImageView) view.findViewById(R.id.page_logo_iv);
            mPageNameET = (EditText) view.findViewById(R.id.team_name_et);
            mPageSportsTV = (TextView) view.findViewById(R.id.team_sport_tv);
            mPageConferenceTV = (TextView) view.findViewById(R.id.team_conference_tv);
            mPageConferenceLabelTV = (TextView) view.findViewById(R.id.team_conference_tv_label);
            mPageCollegeTV = (TextView) view.findViewById(R.id.team_college_tv);
            mPageCollegeLabelTV = (TextView) view.findViewById(R.id.team_college_tv_label);
            mPageDescripionET = (EditText) view.findViewById(R.id.team_description_et);
            mPagePrivacyTV = (TextView) view.findViewById(R.id.team_privacy_tv);
            mPrivacySwitch = (Switch) view.findViewById(R.id.team_privacy_switch);
            mPageNameET.setEnabled(false);
            mPageLogoIV.setOnClickListener(listener);
            mPrivacySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                    if (isChecked) {
                        mIsPublic = 2;
                        mEditPageViewHolder.mPagePrivacyTV.setText(getResources().getString(R.string.private_group));
                    } else {
                        mIsPublic = 1;
                        mEditPageViewHolder.mPagePrivacyTV.setText(getResources().getString(R.string.public_group));
                    }

                }
            });

            FontLoader.setRobotoRegularTypeface(mPageSportsTV, mPageNameET, mPageConferenceTV,
                    mPageCollegeTV, mPageDescripionET, mPagePrivacyTV);
        }
    }
}