package com.schollyme.team;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import com.schollyme.activity.BaseActivity;
import com.schollyme.R;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.ConferenceModel;
import com.vinfotech.request.GetConferencesRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

/**
 * Activity class. This may be useful in display list of conference
 * @author Ravi Bhandari
 *
 */
public class ConferencesListActivity extends BaseActivity implements OnClickListener,OnRefreshListener {

	private ListView mOptionsLV;
	private AutoCompleteTextView mSearchACTV;
	private LinearLayout mContainerLl;
	private LinearLayout mContainerRl;
	private Context mContext;
	private ConferencesListAdapter mConferencesAdapter;
	private ConferenceModel mSelections = null;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private int mPageIndex = 1;
	private boolean loadingFlag = false;
	private ArrayList<ConferenceModel> mConferencesAL;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sports_list_activity);
		mContext = this;

		mOptionsLV = (ListView) findViewById(R.id.items_lv);
		mSearchACTV = (AutoCompleteTextView) findViewById(R.id.search_actv);
		mSearchACTV.setHint(getResources().getString(R.string.search_state));
		mContainerRl = (LinearLayout) findViewById(R.id.confirmation_parent_rl);
		mContainerLl = (LinearLayout) findViewById(R.id.view_contaniner_layout_confirm_rl);
		mSearchACTV.setText("");

		initHeader();

		mSelections = getIntent().getParcelableExtra("selections");
		mSearchACTV.setHint(getResources().getString(R.string.search_conferences));
		mPageIndex = 1;
		mConferencesAL = new ArrayList<ConferenceModel>();
		getConferencesRequest();
		startAnimation();
		FontLoader.setRobotoRegularTypeface(mSearchACTV);
		mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);

	}

	private void initHeader() {

		setHeader(findViewById(R.id.header_layout), R.drawable.ic_close, R.drawable.selector_confirm,
				getResources().getString(R.string.select_conference_label), new OnClickListener() {

					@Override
					public void onClick(View v) {
						Utility.hideSoftKeyboard(mSearchACTV);
						reverseAnimation(null);
					}
				}, new OnClickListener() {

					@Override
					public void onClick(View v) {
						Utility.hideSoftKeyboard(mSearchACTV);
						if (mConferencesAdapter != null) {

							if (mSelections!=null) {
								Intent intent = new Intent();
								intent.putExtra("data", mSelections);
								setResult(RESULT_OK, intent);
								reverseAnimation(intent);
							}
							else {
								Toast.makeText(ConferencesListActivity.this, getResources()
										.getString(R.string.select_one_conference), Toast.LENGTH_SHORT).show();
							}
						} else {
							reverseAnimation(null);
						}
					}
				});
	}

	private void setAdapter(final List<ConferenceModel> conferencesModels) {
		mConferencesAdapter = new ConferencesListAdapter(ConferencesListActivity.this);
		mConferencesAdapter.setList(conferencesModels);
		mOptionsLV.setAdapter(mConferencesAdapter);
		mConferencesAdapter.notifyDataSetChanged();
		new SearchHandler(mSearchACTV).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				mConferencesAdapter.setFilter(text);
			}
		});
		mOptionsLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mSelections = mConferencesAdapter.getItem(position);

				for(int i=0;i<conferencesModels.size();i++){
					ConferenceModel mCModel  = conferencesModels.get(i);
					if(mCModel.mConferenceId.equals(mSelections.mConferenceId)){
						mSelections.isSelected = !mSelections.isSelected;
						conferencesModels.set(i, mSelections);
					}
					else{
						mCModel.isSelected = false;
						conferencesModels.set(i, mCModel);
					}
				}
				mConferencesAdapter.notifyDataSetChanged();
			}
		});

		/*mOptionsLV.setOnScrollListener(new OnScrollListener() {
			boolean bReachedListEnd = false;
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
					Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
					if (null != mConferencesAdapter&& !loadingFlag) {
						mPageIndex++;
						loadingFlag = true;
						getConferencesRequest();
					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
				bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount/2));
			}
		});*/
	}

	public static Intent getIntent(Context context, ConferenceModel selections) {
		Intent intent = new Intent(context, ConferencesListActivity.class);
		intent.putExtra("selections", selections);
		return intent;
	}

	/**
	 * Animation from bottom to top
	 * */
	private void startAnimation() {
		// Cancels any animations for this container.
		mContainerLl.clearAnimation();
		mContainerRl.clearAnimation();

		Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up_dialog);
		mContainerLl.startAnimation(animation);

		AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
	}

	/**
	 * Animation from top to bottom
	 * */
	private void reverseAnimation(final Intent intent) {
		mContainerLl.clearAnimation();
		Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_down_dialog);
		mContainerLl.startAnimation(animation);
		mContainerLl.setVisibility(View.GONE);

		AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
		alphaAnimation.setDuration(500);
		mContainerRl.startAnimation(alphaAnimation);
		mContainerRl.setVisibility(View.GONE);

		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation animation) {
				finish();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}
	}

	private void getConferencesRequest() {
		if(HttpConnector.getConnectivityStatus(mContext)==0){
			DialogUtil.showOkDialog(mContext, Utility.FromResouarceToString(mContext, R.string.No_internet_connection),
					Utility.FromResouarceToString(mContext, R.string.Network_erro));
			if(null!=mSwipeRefreshWidget)
				mSwipeRefreshWidget.setRefreshing(false);
		}
		else{
			GetConferencesRequest mRequest = new GetConferencesRequest(mContext);
			mRequest.GetConferencesServerRequest(mPageIndex);
			mRequest.setRequestListener(new RequestListener() {

				@SuppressWarnings("unchecked")
				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					loadingFlag = false;
					mSwipeRefreshWidget.setRefreshing(false);

					if (success) {
						@SuppressWarnings("unchecked")
						List<ConferenceModel> mConferencesModels = (List<ConferenceModel>) data;
						mConferencesAL.addAll(mConferencesModels);
						if (null != mConferencesAL && null != mSelections) {
							for (ConferenceModel conferenceModel : mConferencesAL) {
								if (mSelections.equals(conferenceModel)) {
									conferenceModel.isSelected = true;
								}
							}
						}
						if(mConferencesAdapter==null){
							setAdapter(mConferencesAL);
						}
						else{
							mConferencesAdapter.setList(mConferencesAL);
							
						}
					}
				}
			});
		}
	}

	@Override
	public void onRefresh() {
		mPageIndex = 1;
		mConferencesAL = new ArrayList<ConferenceModel>();
		getConferencesRequest();
	}
}