package com.schollyme.team;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.PageMemberUser;
import com.vinfotech.request.TeamPageLeaveRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

import java.util.ArrayList;
import java.util.List;

public class TeamMembersAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private MembersViewHolder mMembersViewHolder;
    private List<PageMemberUser> mPageMemberUserAL;
    private String mAdminUserGUID;
    private ErrorLayout mErrorLayout;
    private List<PageMemberUser> mFilteredPageMembers = new ArrayList<PageMemberUser>();
    private String mTeamGUID;
    private String mPageCategoryID;

    public TeamMembersAdapter(Context context, String mAdminGUID, ErrorLayout mError, String mTeamID, String categoryId) {
        this.mContext = context;
        this.mAdminUserGUID = mAdminGUID;
        this.mLayoutInflater = LayoutInflater.from(mContext);
        this.mErrorLayout = mError;
        this.mPageCategoryID = categoryId;
        this.mTeamGUID = mTeamID;
    }

    public void setList(List<PageMemberUser> mAList) {
        this.mPageMemberUserAL = mAList;
        setFilter(null);
    }

    @Override
    public int getCount() {
        return null == mFilteredPageMembers ? 0 : mFilteredPageMembers.size();
    }

    @Override
    public PageMemberUser getItem(int position) {
        return mFilteredPageMembers.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mMembersViewHolder = new MembersViewHolder();

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.team_members_item, parent, false);
            mMembersViewHolder.mNameTV = (TextView) convertView.findViewById(R.id.user_name_tv);
            mMembersViewHolder.mTypeTV = (TextView) convertView.findViewById(R.id.user_type_tv);
            mMembersViewHolder.mOptionTV = (TextView) convertView.findViewById(R.id.remove_tv);
            mMembersViewHolder.mProfilePicIV = (ImageView) convertView.findViewById(R.id.profile_image_iv);
            convertView.setTag(mMembersViewHolder);
        } else {
            mMembersViewHolder = (MembersViewHolder) convertView.getTag();
        }
        mMembersViewHolder.mNameTV.setText(mFilteredPageMembers.get(position).mUserFName);

        if (mFilteredPageMembers.get(position).mUserTypeId.equals("1")) {
            mMembersViewHolder.mTypeTV.setText(String.valueOf(Config.UserType.Coach));
        } else if (mFilteredPageMembers.get(position).mUserTypeId.equals("2")) {
            mMembersViewHolder.mTypeTV.setText(String.valueOf(Config.UserType.Athlete));
        } else if (mFilteredPageMembers.get(position).mUserTypeId.equals("3")) {
            mMembersViewHolder.mTypeTV.setText(String.valueOf(Config.UserType.Fan));
        }

        mMembersViewHolder.mOptionTV.setId(position);
        /** if loggedin user is admin then admin can delete any user*/
        if (new LogedInUserModel(mContext).mUserGUID.equals(mAdminUserGUID)) {
            if (mFilteredPageMembers.get(position).mModuleRoleId.equals("4")
                    || mFilteredPageMembers.get(position).mModuleRoleId.equals("5")) {
                mMembersViewHolder.mOptionTV.setText(mContext.getResources().getString(R.string.page_admin_label));
                mMembersViewHolder.mOptionTV.setBackgroundResource(R.drawable.chat_right_bg);
                mMembersViewHolder.mOptionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                convertView.setBackgroundColor(mContext.getResources().getColor(R.color.edit_text_bg_color));
                mMembersViewHolder.mOptionTV.setTag(5);

            } else if (mFilteredPageMembers.get(position).mModuleRoleId.equals("6")) {
                mMembersViewHolder.mOptionTV.setText("");
                mMembersViewHolder.mOptionTV.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                mMembersViewHolder.mOptionTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_delete_member, 0, 0, 0);
                mMembersViewHolder.mOptionTV.setTag(4);
                convertView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            } else {
                mMembersViewHolder.mOptionTV.setText("");
                mMembersViewHolder.mOptionTV.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                mMembersViewHolder.mOptionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                convertView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                mMembersViewHolder.mOptionTV.setTag(0);
            }


        } else {
            if (mFilteredPageMembers.get(position).mModuleRoleId.equals("4")
                    || mFilteredPageMembers.get(position).mModuleRoleId.equals("5")) {
                mMembersViewHolder.mOptionTV.setText(mContext.getResources().getString(R.string.page_admin_label));
                mMembersViewHolder.mOptionTV.setBackgroundResource(R.drawable.chat_right_bg);
                mMembersViewHolder.mOptionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                convertView.setBackgroundColor(mContext.getResources().getColor(R.color.edit_text_bg_color));
                mMembersViewHolder.mOptionTV.setTag(5);
            } else if (mFilteredPageMembers.get(position).mModuleRoleId.equals("6")
                    && mFilteredPageMembers.get(position).mUserGUID.equals(new LogedInUserModel(mContext).mUserGUID)) {
                mMembersViewHolder.mOptionTV.setText("");
                mMembersViewHolder.mOptionTV.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                mMembersViewHolder.mOptionTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_delete_member, 0, 0, 0);
                mMembersViewHolder.mOptionTV.setTag(6);
                convertView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            } else {
                mMembersViewHolder.mOptionTV.setText("");
                mMembersViewHolder.mOptionTV.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                mMembersViewHolder.mOptionTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mMembersViewHolder.mOptionTV.setTag(0);
                convertView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            }


        }

        mMembersViewHolder.mOptionTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                int userType = (int) view.getTag();
                final int pos = view.getId();
                final PageMemberUser mUser = mFilteredPageMembers.get(pos);

                if (userType == 4) {
                    String message = mContext.getResources().getString(R.string.remove_from_page_confirmation);

                    DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps,
                            "", message, new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //TODO when admin will remove other members then user remove and same list will be display
                                    leavePageRequest(mTeamGUID, mUser.mUserGUID, false, pos);
                                }
                            }, new OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                    /*DialogUtil.showOkCancelDialogButtonLisnter(mContext, message, "", new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {

							//TODO when admin will remove other members then user remove and same list will be display
							leavePageRequest(mTeamGUID, mUser.mUserGUID,false,pos);
						}
					}, new OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {

						}
					});*/
                } else if (userType == 6) {
                    String message = mContext.getResources().getString(R.string.leave_page_confirmation);
                    DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps,
                            "", message, new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //TODO when user will lefts then user remove and naviagate to list screen
                                    leavePageRequest(mTeamGUID, mUser.mUserGUID, true, pos);
                                }
                            }, new OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });

					/*DialogUtil.showOkCancelDialogButtonLisnter(mContext, message, "", new OnOkButtonListner() {

						@Override
						public void onOkBUtton() {
							leavePageRequest(mTeamGUID, mUser.mUserGUID,true,pos);
						}
					}, new OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {

						}
					});*/
                }
            }
        });

        FontLoader.setRobotoRegularTypeface(mMembersViewHolder.mNameTV, mMembersViewHolder.mTypeTV, mMembersViewHolder.mOptionTV);
        convertView.setClickable(false);
        String imageUrl = Config.IMAGE_URL_PROFILE + mFilteredPageMembers.get(position).mUserProflePic;
        ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mMembersViewHolder.mProfilePicIV,
                ImageLoaderUniversal.option_Round_Image);

        return convertView;
    }

    private class MembersViewHolder {
        public TextView mNameTV, mTypeTV, mOptionTV;
        public ImageView mProfilePicIV;
    }

    public void setFilter(String text) {
        mFilteredPageMembers.clear();

        if (null != mPageMemberUserAL) {
            if (TextUtils.isEmpty(text)) {
                mFilteredPageMembers.addAll(mPageMemberUserAL);
            } else {
                for (PageMemberUser memberModel : mPageMemberUserAL) {
                    if (memberModel.mUserFName.toLowerCase().startsWith(text.toLowerCase()) || memberModel.mUserFName.toLowerCase().contains((" " + text.toLowerCase()))) {
                        mFilteredPageMembers.add(memberModel);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }


    /**
     * Call  api to leave current group,
     * This action can be perform by page members only
     */
    private void leavePageRequest(String mTeamGUID, String UserGUID, final boolean isSelfRemove, final int pos) {
        TeamPageLeaveRequest mRequest = new TeamPageLeaveRequest(mContext);
        mRequest.TeamPagesLeaveRequest(mTeamGUID, UserGUID);
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), true, MsgType.Success);
                    if (isSelfRemove) {
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                Intent intent;
                                if (mPageCategoryID.equals("1")) {
                                    intent = TeamActivity.getIntent(mContext, 0);
                                } else {
                                    intent = TeamActivity.getIntent(mContext, 1);
                                }
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                mContext.startActivity(intent);
                                ((Activity) mContext).finish();
                            }
                        }, 2000);
                    } else {
                        mFilteredPageMembers.remove(pos);
                        notifyDataSetChanged();
                    }
                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
    }
}