package com.schollyme.team;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.friends.BaseFriendFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.SearchHandler;
import com.schollyme.handler.SearchHandler.SearchListener;
import com.schollyme.model.Teams;
import com.schollyme.team.FanPageListFragment.AdapterRefreshListener;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.TeamSuggestionsRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
/**
 * Adapter class. This may be useful in display pages suggestion to a loggedin user
 * @author Ravi Bhandari
 *
 */
public class SuggestedPageListFragment extends BaseFriendFragment implements OnClickListener, OnRefreshListener{

	private static Context mContext;
	private ListView mTeamLV;
	private SwipeRefreshLayout mSwipeRefreshWidget;
	private LinearLayout mLinearLayout;
	private EditText mTeamSearchET;
	private ImageButton mClearIB;
	private TextView mNoRecordTV;
	private SuggestedTeamAdapter mSuggestionsPageAdapter;
	private ArrayList<Teams> mTeamsSuggestionsAL;
	private int pageIndex = 1;
	private boolean loadingFlag = false;
	private static ErrorLayout mErrorLayout;
	private View view;


	public static SuggestedPageListFragment newInstance(ErrorLayout mLayout) {
		mErrorLayout = mLayout;
		SuggestedPageListFragment suggestedPageListFragment = new SuggestedPageListFragment();
		suggestedPageListFragment.mErrorLayout = mLayout;
		return suggestedPageListFragment;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		pageIndex = 1;
		mTeamsSuggestionsAL = new ArrayList<Teams>();
		getSuggestedPageRequest(mTeamSearchET.getText().toString().trim());
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.team_fragment, null);
		mContext = getActivity();
		mTeamLV = (ListView) view.findViewById(R.id.team_lv);
		mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
		mSwipeRefreshWidget.setColorScheme(R.color.blue,R.color.red, R.color.app_text_color,
				R.color.text_hint_color);
		mSwipeRefreshWidget.setOnRefreshListener(this);
		mTeamSearchET = (EditText) view.findViewById(R.id.search_et);
		mClearIB = (ImageButton) view.findViewById(R.id.clear_text_ib);
		mLinearLayout = (LinearLayout) view.findViewById(R.id.search_view_ll);
		mNoRecordTV = (TextView) view.findViewById(R.id.no_record_message_tv);
		mLinearLayout.setVisibility(View.GONE);

		mClearIB.setOnClickListener(this);


		mTeamSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					return true;
				}
				return false;
			}
		});

		new SearchHandler(mTeamSearchET).setSearchListener(new SearchListener() {

			@Override
			public void onSearch(String text) {
				if(null!=mSuggestionsPageAdapter){
					mSuggestionsPageAdapter.setFilter(text);
				}
			}
		}).setClearView(mClearIB);

		return view;
	}



	private void setTeamSuggestionsAdapter(){
		mSuggestionsPageAdapter = new SuggestedTeamAdapter(mContext,mErrorLayout,new AdapterRefreshListener() {

			@Override
			public void onRefresh(int listSize) {
				if(listSize==0){
					mNoRecordTV.setVisibility(View.VISIBLE);
				}
				else{
					mNoRecordTV.setVisibility(View.GONE);
				}
			}
		});
		if(mTeamsSuggestionsAL.size()>0){
			
			mSuggestionsPageAdapter.setList(mTeamsSuggestionsAL);
			mTeamLV.setAdapter(mSuggestionsPageAdapter);
			mTeamLV.setVisibility(View.VISIBLE);
			mNoRecordTV.setVisibility(View.GONE);
			mTeamLV.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					if(mTeamsSuggestionsAL.size()>0){
						String mTeamGUID = mTeamsSuggestionsAL.get(position).mTeamGUID;
						startActivity(TeamPageActivity.getIntent(mContext,mTeamGUID,"",position, mTeamsSuggestionsAL.get(position)));
					}
				}
			});
		}
		else{
			mTeamLV.setAdapter(mSuggestionsPageAdapter);
			mSuggestionsPageAdapter.setList(mTeamsSuggestionsAL);
			mNoRecordTV.setVisibility(View.VISIBLE);
		}

	}

	@Override
	public void onRefresh() {
		pageIndex = 1;
		mTeamsSuggestionsAL = new ArrayList<Teams>();
		getSuggestedPageRequest(mTeamSearchET.getText().toString().trim());
	}

	@Override
	public void onReload() {
		if(null != mTeamLV){
			mTeamLV.post(new Runnable() {

				@Override
				public void run() {
					onRefresh();
				}
			});
		}
	}

	@Override
	public void onSearchBtnClick() {
		if(mLinearLayout.getVisibility() == View.VISIBLE){
			mLinearLayout.setVisibility(View.GONE);
		}else{
			mLinearLayout.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.clear_text_ib:
			mTeamSearchET.setText("");
			break;

		default:
			break;
		}
	}

	/**
	 * Make server request to call list of page suggestion
	 * */
	private void getSuggestedPageRequest(String mSearchText){
		TeamSuggestionsRequest mRequest = new TeamSuggestionsRequest(mContext);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				mSwipeRefreshWidget.setRefreshing(false);
				if(success){
					mTeamsSuggestionsAL = (ArrayList<Teams>) data;

				}
				else{
					mTeamsSuggestionsAL = new ArrayList<Teams>();
					mErrorLayout.showError(data.toString(), true,MsgType.Error);
				}
				setTeamSuggestionsAdapter();

			}
		});
		mRequest.TeamSuggestionsListServerRequest(pageIndex);
	}
}