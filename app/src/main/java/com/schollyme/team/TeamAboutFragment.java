package com.schollyme.team;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.PageMemberUser;
import com.schollyme.model.Teams;
import com.vinfotech.request.FanPageJoinRequest;
import com.vinfotech.request.TeamMembersListRequest;
import com.vinfotech.request.TeamPageDeactivateRequest;
import com.vinfotech.request.TeamPageDetailRequest;
import com.vinfotech.request.TeamPageLeaveRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 28/6/2017.
 */

public class TeamAboutFragment extends BaseFragment implements View.OnClickListener {

    private TeamAboutActivityViewHolder mTeamAboutActivityViewHolder;
    private Context mContext;
    private Teams mTeamPageDetails;
    private static String mTeamGUID, mAdminGUID;
    private ErrorLayout mErrorLayout;
    private static int position;
    private String actionType;
    private View convertView;


    public static TeamAboutFragment newInstance(String teamGUID, String adminGUID, int pos) {
        TeamAboutFragment friendsTopFragment = new TeamAboutFragment();
        mTeamGUID = teamGUID;
        mAdminGUID = adminGUID;
        position = pos;
        return friendsTopFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        convertView = inflater.inflate(R.layout.team_about_activity, null);

        return convertView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();

        mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_rl));

       /* mTeamGUID = getIntent().getStringExtra("TeamPageGUID");
        position = getIntent().getIntExtra("Position", 0);*/

    }

    @Override
    public void onResume() {
        super.onResume();
        getTeamPageDetailsServerRequest(mTeamGUID);
    }


    @SuppressLint("DefaultLocale")
    @SuppressWarnings("deprecation")
    private void setData() {
        try {
            mTeamAboutActivityViewHolder.mTeamNameTV.setText(mTeamPageDetails.mTeamName);
            mTeamAboutActivityViewHolder.mTeamTypeTV.setText(mTeamPageDetails.mTeamSportsName);
            mTeamAboutActivityViewHolder.mPageAdminNameTV.setText(mTeamPageDetails.mAdminUserFName);

		/*String imageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mTeamImageURL;
              ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mTeamAboutActivityViewHolder.mTeamLogoIV ,
				ImageLoaderUniversal.option_Round_Image);*/

            String adminImageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mAdminUserProfilePicURL;
            ImageLoaderUniversal.ImageLoadRound(mContext, adminImageUrl, mTeamAboutActivityViewHolder.mPageAdminLogoIV,
                    ImageLoaderUniversal.option_Round_Image);

            if (mTeamPageDetails.isPublic.equals("1")) {
                mTeamAboutActivityViewHolder.mTeamTypeTV.setText(getResources().getString(R.string.public_group));
            } else {

                mTeamAboutActivityViewHolder.mTeamTypeTV.setText(getResources().getString(R.string.private_group));

            }

            mTeamAboutActivityViewHolder.mTeamDescriptionTV.setText(mTeamPageDetails.mPageDescription);
            mTeamAboutActivityViewHolder.mTeamSportsTV.setText(mTeamPageDetails.mTeamSportsName);
            mTeamAboutActivityViewHolder.mTeamConferenceTV.setText(mTeamPageDetails.mPageConferenceName);
            mTeamAboutActivityViewHolder.mTeamCollegeTV.setText(mTeamPageDetails.mPageCollegeName.trim());
            if (Integer.parseInt(mTeamPageDetails.mTeamMembersCount) < 2) {
                mTeamAboutActivityViewHolder.mGroupInfoTV.setText(mTeamPageDetails.mTeamMembersCount + " " + getResources().getString(R.string.member_tab));
            } else {
                mTeamAboutActivityViewHolder.mGroupInfoTV.setText(mTeamPageDetails.mTeamMembersCount + " " + getResources().getString(R.string.members_tab));
            }

            mTeamAboutActivityViewHolder.mPageAdminStatusTV.setText(getResources().getString(R.string.page_admin_label));


        /*if(mTeamPageDetails.mPageCategoryID.equals("1")){
            *//*String imageUrl = Config.IMAGE_URL_PROFILE + mTeamPageDetails.mTeamImageURL;
            ImageLoaderUniversal.ImageLoadRoundForTeamPage(mContext, imageUrl, mTeamAboutActivityViewHolder.mTeamLogoIV ,
                    ImageLoaderUniversal.option_Round_Image_for_teampage);*//*
        }
        else{
            String mTeamName = mTeamPageDetails.mTeamName.trim();
            if(!TextUtils.isEmpty(mTeamName)){
                mTeamAboutActivityViewHolder.mTeamFirstCharTV.setText(String.valueOf(mTeamName.charAt(0)).toUpperCase());
            }
            if(position%4==0){
                mTeamAboutActivityViewHolder.mTeamLogoIV.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.circle_blank_logo1));
            }
            else if(position%4==1){
                mTeamAboutActivityViewHolder.mTeamLogoIV.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.circle_blank_logo2));
            }
            else if(position%4==2){
                mTeamAboutActivityViewHolder.mTeamLogoIV.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.circle_blank_logo3));
            }
            else if(position%4==3){
                mTeamAboutActivityViewHolder.mTeamLogoIV.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.circle_blank_logo4));
            }
        }*/
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private void setViewAccordingTouserType() {
        try {
            mTeamAboutActivityViewHolder = new TeamAboutActivityViewHolder(convertView.findViewById(R.id.main_rl), this);
            if (mTeamPageDetails.mPageCategoryID.equals("1")) {
                mTeamAboutActivityViewHolder.mConferenceHeaderTV.setVisibility(View.VISIBLE);
                mTeamAboutActivityViewHolder.mCollegeHeaderTV.setVisibility(View.VISIBLE);
                mTeamAboutActivityViewHolder.mTeamConferenceTV.setVisibility(View.VISIBLE);
                mTeamAboutActivityViewHolder.mTeamCollegeTV.setVisibility(View.VISIBLE);
                mTeamAboutActivityViewHolder.mDevider1.setVisibility(View.VISIBLE);
                mTeamAboutActivityViewHolder.mDevider2.setVisibility(View.VISIBLE);

            } else {
                mTeamAboutActivityViewHolder.mConferenceHeaderTV.setVisibility(View.GONE);
                mTeamAboutActivityViewHolder.mCollegeHeaderTV.setVisibility(View.GONE);
                mTeamAboutActivityViewHolder.mTeamConferenceTV.setVisibility(View.GONE);
                mTeamAboutActivityViewHolder.mTeamCollegeTV.setVisibility(View.GONE);
                mTeamAboutActivityViewHolder.mDevider1.setVisibility(View.GONE);
                mTeamAboutActivityViewHolder.mDevider2.setVisibility(View.GONE);

            }
            if (mTeamPageDetails.isGroupAdmin) {

                if (mTeamPageDetails != null) {
                    int optionText;
                    if (mTeamPageDetails.mStatusID.equals("2")) {
                        optionText = R.string.deactivate_label;
                    } else {
                        optionText = R.string.activate_label;
                    }
                    mTeamAboutActivityViewHolder.mDeactivatePageTV.setText(mContext.getResources().getString(optionText));
                }
            } else if (mTeamPageDetails.isGroupMember) {
                mTeamAboutActivityViewHolder.mDeactivatePageTV.setText(mContext.getResources().getString(R.string.leave_header));
            } else {
                mTeamAboutActivityViewHolder.mDeactivatePageTV.setText(getResources().getString(R.string.join_label));
            }
            setData();
            getTeamMembersServerRequest(mTeamGUID, "");
        } catch (IllegalStateException e) {

        }
    }

    private class TeamAboutActivityViewHolder {

        private ImageView mTeamLogoIV, mPageAdminLogoIV;
        private TextView mTeamNameTV, mTeamTypeTV, mTeamDescriptionTV, mTeamSportsTV, mPageAdminNameTV;
        private TextView mPageAdminStatusTV, mTeamConferenceTV, mTeamCollegeTV, mGroupInfoTV, mDeactivatePageTV;
        private TextView mSportsHeaderTV, mGroupInfoHeaderTV, mConferenceHeaderTV, mCollegeHeaderTV;
        private View mDevider1, mDevider2;
        public TextView mTeamFirstCharTV;

        private TeamAboutActivityViewHolder(View view, View.OnClickListener listener) {

            mTeamLogoIV = (ImageView) view.findViewById(R.id.profile_image_iv);
            mPageAdminLogoIV = (ImageView) view.findViewById(R.id.admin_image_iv);

            mTeamNameTV = (TextView) view.findViewById(R.id.team_name_tv);
            mTeamTypeTV = (TextView) view.findViewById(R.id.team_type_tv);
            mTeamDescriptionTV = (TextView) view.findViewById(R.id.description_tv);
            mTeamSportsTV = (TextView) view.findViewById(R.id.sports_value_tv);
            mPageAdminNameTV = (TextView) view.findViewById(R.id.admin_name_tv);
            mPageAdminStatusTV = (TextView) view.findViewById(R.id.admin_status_tv);
            mTeamConferenceTV = (TextView) view.findViewById(R.id.conference_tv);
            mTeamCollegeTV = (TextView) view.findViewById(R.id.college_tv);
            mGroupInfoTV = (TextView) view.findViewById(R.id.group_info_tv);
            mDeactivatePageTV = (TextView) view.findViewById(R.id.deactivate_tv);
            mTeamFirstCharTV = (TextView) view.findViewById(R.id.team_name_fchar_tv);

            mSportsHeaderTV = (TextView) view.findViewById(R.id.sports_header_tv);
            mGroupInfoHeaderTV = (TextView) view.findViewById(R.id.group_info_header_tv);
            mConferenceHeaderTV = (TextView) view.findViewById(R.id.conference_header_tv);
            mCollegeHeaderTV = (TextView) view.findViewById(R.id.college_header_tv);
            mThumbContainerLl = (LinearLayout) view.findViewById(R.id.thumb_container_ll);


            mDevider1 = view.findViewById(R.id.divider_view_conference);
            mDevider2 = view.findViewById(R.id.divider_view_college);

            mGroupInfoHeaderTV.setOnClickListener(listener);
            mGroupInfoTV.setOnClickListener(listener);
            mDeactivatePageTV.setOnClickListener(listener);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.group_info_tv:
                if (null != mTeamPageDetails)
                    startActivity(TeamMembersActivity.getIntent(mContext, mTeamGUID, mTeamPageDetails.mAdminUserGUID,
                            mTeamPageDetails.mPageCategoryID));
                break;
            case R.id.group_info_header_tv:
                if (null != mTeamPageDetails)
                    startActivity(TeamMembersActivity.getIntent(mContext, mTeamGUID, mTeamPageDetails.mAdminUserGUID,
                            mTeamPageDetails.mPageCategoryID));
                break;
            case R.id.deactivate_tv:
                String optionText;
                if (null != mTeamPageDetails && mTeamPageDetails.isGroupAdmin) {

                    if (mTeamPageDetails.mStatusID.equals("2")) {
                        optionText = mContext.getResources().getString(R.string.deactivate_page_confirmation);
                        actionType = Config.IN_ACTIVATE_PAGE;
                    } else {
                        optionText = mContext.getResources().getString(R.string.activate_page_confirmation);
                        actionType = Config.ACTIVATE_PAGE;
                    }

                } else if (mTeamPageDetails != null && mTeamPageDetails.isGroupMember) {
                    optionText = mContext.getResources().getString(R.string.leave_page_confirmation);
                } else {
                    optionText = mContext.getResources().getString(R.string.join_page_confirmation);
                }
                DialogUtil.showOkCancelDialog(mContext, R.string.ok_label, R.string.cancel_caps,
                        mContext.getResources().getString(R.string.app_name), optionText, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (null != mTeamPageDetails && mTeamPageDetails.isGroupAdmin) {
                                    deactivatePageRequest(mTeamPageDetails.mTeamGUID, actionType);
                                } else if (null != mTeamPageDetails && mTeamPageDetails.isGroupMember) {
                                    leavePageRequest(mTeamPageDetails.mTeamGUID, new LogedInUserModel(mContext).mUserGUID);
                                } else if (null != mTeamPageDetails) {
                                    joinPageRequest(mTeamPageDetails.mTeamGUID);
                                }
                            }
                        }, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
        /*	DialogUtil.showOkCancelDialogButtonLisnter(mContext,optionText, "", new OnOkButtonListner() {

				@Override
				public void onOkBUtton() {
					if(null!=mTeamPageDetails && mTeamPageDetails.isGroupAdmin){
						deactivatePageRequest(mTeamPageDetails.mTeamGUID,actionType);
					}
					else if(null!=mTeamPageDetails && mTeamPageDetails.isGroupMember){
						leavePageRequest(mTeamPageDetails.mTeamGUID,new LogedInUserModel(mContext).mUserGUID);
					}
					else if(null!=mTeamPageDetails){
						joinPageRequest(mTeamPageDetails.mTeamGUID);
					}
				}
			}, new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {

				}
			});*/
                break;

            default:
                break;
        }
    }

    private void getTeamPageDetailsServerRequest(String mTeamGUID) {

        TeamPageDetailRequest mRequest = new TeamPageDetailRequest(mContext);
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mTeamPageDetails = (Teams) data;
                    setViewAccordingTouserType();
                } else {
                    final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
                            new DialogUtil.OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {
                                }
                            });
                    builder.setCancelable(false);
                    builder.setOnKeyListener(new Dialog.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode,
                                             KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                builder.dismiss();
                            }
                            return true;
                        }
                    });
                }
            }
        });
        mRequest.TeamDetailsServerRequest(mTeamGUID);
    }

    /**
     * Call API to Deactivate a fan/team page
     * only page admin/owner have permision to Deactivate a page.
     */
    private void deactivatePageRequest(String mGUID, String actionType) {
        TeamPageDeactivateRequest mRequest = new TeamPageDeactivateRequest(mContext);
        mRequest.TeamDeactivateRequest(mGUID, actionType);
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    getTeamPageDetailsServerRequest(mTeamGUID);
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Success);
                } else {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Error);
                }
            }
        });

    }

    /**
     * Call  api to leave current group,
     * This action can be perform by page members only
     */
    private void leavePageRequest(String mTeamGUID, String UserGUID) {
        TeamPageLeaveRequest mRequest = new TeamPageLeaveRequest(mContext);
        mRequest.TeamPagesLeaveRequest(mTeamGUID, UserGUID);
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Success);

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                           /* Intent intent;
                            if(mTeamPageDetails.mPageCategoryID.equals("1")){
                                intent = TeamActivity.getIntent(mContext, 0);
                            }
                            else{
                                intent = TeamActivity.getIntent(mContext, 1);
                            }
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);*/
                        }
                    }, 2000);

                } else {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Error);
                }
            }
        });
    }


    /**
     * Call API to join a suggested fan page
     */
    private void joinPageRequest(String mTeamPageGUID) {
        FanPageJoinRequest mRequest = new FanPageJoinRequest(mContext);
        mRequest.getFanPageJoinRequest(mTeamPageGUID, new LogedInUserModel(mContext).mUserGUID);
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Success);
                    getTeamPageDetailsServerRequest(mTeamGUID);
                } else {
                    mErrorLayout.showError(data.toString(), true, ErrorLayout.MsgType.Error);
                }
            }
        });

    }


    private LinearLayout mThumbContainerLl;
    private Map<Integer, View> mViews = new HashMap<Integer, View>();
    private ArrayList<PageMemberUser> mMembers = new ArrayList<>();

    private void addMediaInLayout() {
        mThumbContainerLl.removeAllViews();
        mViews.clear();
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        if (null != mTeamPageDetails && mMembers.size() > 0) {

            int loopCout = 0;
            if (mMembers.size() > 5) {
                loopCout = 5;
            } else {
                loopCout = mMembers.size();
            }

            for (int i = 0; i < loopCout; i++) {
                final View view = layoutInflater.inflate(R.layout.about_member_list, null);
                final View containerRl = view.findViewById(R.id.container_rl);
                containerRl.setTag(i);
                ImageView mediaIv = (ImageView) view.findViewById(R.id.team_members_dp);
                String imageUrl = Config.IMAGE_URL_PROFILE + mMembers.get(i).mUserProflePic;
                ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mediaIv,
                        ImageLoaderUniversal.option_Round_Image);
                mThumbContainerLl.addView(view);
                mViews.put(i, containerRl);
            }
        }
    }

    private void getTeamMembersServerRequest(String mTeamGUID, String mSearchText) {
        TeamMembersListRequest mRequest = new TeamMembersListRequest(mContext);
        mRequest.TeamMembersListServerRequest(mSearchText, mTeamGUID, "All", 1);
        mRequest.setRequestListener(new BaseRequest.RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    ArrayList<PageMemberUser> mList = (ArrayList<PageMemberUser>) data;
                    mMembers.addAll(mList);
                    addMediaInLayout();
                } else {
                    final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(), getResources().getString(R.string.app_name),
                            new DialogUtil.OnOkButtonListner() {

                                @Override
                                public void onOkBUtton() {

                                }
                            });
                    builder.setCancelable(false);
                    builder.setOnKeyListener(new Dialog.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode,
                                             KeyEvent event) {
                            return true;
                        }
                    });
                }
            }
        });
    }

}
