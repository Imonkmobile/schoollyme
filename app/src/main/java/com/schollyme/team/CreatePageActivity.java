package com.schollyme.team;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.activity.SportsListActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.media.MediaPickerActivity;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.ConferenceModel;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.SportsModel;
import com.schollyme.model.UniversityModel;
import com.vinfotech.request.CreatePageRequest;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.utility.MarshMallowPermission;
import com.vinfotech.utility.Utility;

import java.io.File;
import java.util.ArrayList;

/**
 * Activity class. This may be useful in create new TeamPage
 * that may be either official page or fan page
 *
 * @author Ravi Bhandari
 */
public class CreatePageActivity extends MediaPickerActivity implements OnClickListener {

    private Context mContext;
    private CreatePageViewHolder mCreatePageViewHolder;
    private static final int REQ_CODE_SELECT_SPORT = 100;
    private static final int REQ_CODE_SELECT_CONFERENCE = 200;
    private static final int REQ_CODE_SELECT_UNIVERSITY = 300;
    private ArrayList<SportsModel> mSelectedSportModel;
    private ConferenceModel mConferenceModel;
    private UniversityModel mUniversityModel;
    private Bitmap mPhotoBitmap;
    private ErrorLayout mErrorLayout;
    private String mGroupGUID, mGroupName, mGroupDescription, mLogoName,
            mCategoryIds, mGroupCoverImage = "";
    private int mIsPublic = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.team_page_create);
        mContext = this;

        mCategoryIds = getIntent().getStringExtra("PageTypeSelected");
        mErrorLayout = new ErrorLayout(findViewById(R.id.main_ll));
        mCreatePageViewHolder = new CreatePageViewHolder(findViewById(R.id.main_ll), this);

        initHeader();
        viewListnres();
    }

    @Override
    protected void onResume() {
        super.onResume();

        MarshMallowPermission marshMallowPermission = new MarshMallowPermission(this);
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        }

        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        }
    }

    private void initHeader() {

        String headerTittle = mCategoryIds.equals(Config.PAGE_TYPE_OFFICIAL) ?
                getResources().getString(R.string.create_official_team) : getResources().getString(R.string.create_fan_team);
        setHeader(findViewById(R.id.header_layout), R.drawable.icon_back, R.drawable.selector_confirm, headerTittle,
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utility.hideSoftKeyboard(mCreatePageViewHolder.mPageNameET);
                        finish();
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        if (isValidated()) {
                            if (null != cropImageUri) {
                                uploadMediaServerRequest();
                            } else {
                                if (!mCategoryIds.equals(Config.PAGE_TYPE_OFFICIAL)) {
                                    createPageServerRequest("");
                                }else{
                                    mErrorLayout.showError(getResources().getString(R.string.page_photo_require), false, MsgType.Error);
                                }
                            }
                        }
                    }
                });
    }

    @SuppressWarnings("ResourceAsColor")
    private void viewListnres() {

        mCreatePageViewHolder.mPageNameET.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mCreatePageViewHolder.mPageNameTV.setTextColor(Color.parseColor("#01579B"));
                mCreatePageViewHolder.mPageNameET.setTextColor(Color.parseColor("#000000"));
                mCreatePageViewHolder.mPageNameET.setBackgroundResource(R.drawable.ic_blue_line);
                changeColorOnView();
            }
        });

        mCreatePageViewHolder.mPageNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mCreatePageViewHolder.mPageNameTV.setTextColor(Color.parseColor("#01579B"));
                    mCreatePageViewHolder.mPageNameET.setTextColor(Color.parseColor("#000000"));
                    view.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mCreatePageViewHolder.mPageNameTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mCreatePageViewHolder.mPageNameET.setTextColor(Color.parseColor("#666666"));
                    view.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mCreatePageViewHolder.mPageDescripionET.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mCreatePageViewHolder.mPageDescripionTV.setTextColor(Color.parseColor("#01579B"));
                mCreatePageViewHolder.mPageDescripionET.setTextColor(Color.parseColor("#000000"));
                mCreatePageViewHolder.mViewDiscriptionLine.setBackgroundResource(R.drawable.ic_blue_line);
                changeColorOnView();
            }
        });

        mCreatePageViewHolder.mPageDescripionET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mCreatePageViewHolder.mPageDescripionTV.setTextColor(Color.parseColor("#01579B"));
                    mCreatePageViewHolder.mPageDescripionET.setTextColor(Color.parseColor("#000000"));
                    mCreatePageViewHolder.mViewDiscriptionLine.setBackgroundResource(R.drawable.ic_blue_line);
                } else {
                    mCreatePageViewHolder.mPageDescripionTV.setTextColor(Color.parseColor("#cfcfcf"));
                    mCreatePageViewHolder.mPageDescripionET.setTextColor(Color.parseColor("#666666"));
                    mCreatePageViewHolder.mViewDiscriptionLine.setBackgroundResource(R.drawable.ic_gray_line);
                }
                changeColorOnView();
            }
        });

        mCreatePageViewHolder.layoutPrivacy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsPublic == 2) {
                    mIsPublic = 1;
                    mCreatePageViewHolder.mPrivacySwitch.setChecked(false);
                } else {
                    mIsPublic = 2;
                    mCreatePageViewHolder.mPrivacySwitch.setChecked(true);
                }
                changeColorOnPrivacyView();
            }
        });
    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnView() {

        mCreatePageViewHolder.mPageSportsLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageSportsTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageSportsTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageConferenceLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageConferenceTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageConferenceTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageCollegeLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageCollegeTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageCollegeTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPagePrivacyTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.layoutPrivacy.setBackgroundResource(R.drawable.ic_gray_line);

    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnSportsView() {

        mCreatePageViewHolder.mPageNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageNameET.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageDescripionTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageDescripionET.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mViewDiscriptionLine.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageSportsLabelTV.setTextColor(Color.parseColor("#01579B"));
        mCreatePageViewHolder.mPageSportsTV.setTextColor(Color.parseColor("#000000"));
        mCreatePageViewHolder.mPageSportsTV.setBackgroundResource(R.drawable.ic_blue_line);

        mCreatePageViewHolder.mPageConferenceLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageConferenceTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageConferenceTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageCollegeLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageCollegeTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageCollegeTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPagePrivacyTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.layoutPrivacy.setBackgroundResource(R.drawable.ic_gray_line);

    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnConferenceView() {

        mCreatePageViewHolder.mPageNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageNameET.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageDescripionTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageDescripionET.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mViewDiscriptionLine.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageSportsLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageSportsTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageSportsTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageConferenceLabelTV.setTextColor(Color.parseColor("#01579B"));
        mCreatePageViewHolder.mPageConferenceTV.setTextColor(Color.parseColor("#000000"));
        mCreatePageViewHolder.mPageConferenceTV.setBackgroundResource(R.drawable.ic_blue_line);

        mCreatePageViewHolder.mPageCollegeLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageCollegeTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageCollegeTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPagePrivacyTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.layoutPrivacy.setBackgroundResource(R.drawable.ic_gray_line);

    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnCollegeView() {

        mCreatePageViewHolder.mPageNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageNameET.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageDescripionTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageDescripionET.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mViewDiscriptionLine.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageSportsLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageSportsTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageSportsTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageConferenceLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageConferenceTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageConferenceTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageCollegeLabelTV.setTextColor(Color.parseColor("#01579B"));
        mCreatePageViewHolder.mPageCollegeTV.setTextColor(Color.parseColor("#000000"));
        mCreatePageViewHolder.mPageCollegeTV.setBackgroundResource(R.drawable.ic_blue_line);

        mCreatePageViewHolder.mPagePrivacyTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.layoutPrivacy.setBackgroundResource(R.drawable.ic_gray_line);

    }

    @SuppressWarnings("ResourceAsColor")
    private void changeColorOnPrivacyView() {

        mCreatePageViewHolder.mPageNameTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageNameET.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageNameET.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageDescripionTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageDescripionET.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mViewDiscriptionLine.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageSportsLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageSportsTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageSportsTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageConferenceLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageConferenceTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageConferenceTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPageCollegeLabelTV.setTextColor(Color.parseColor("#cfcfcf"));
        mCreatePageViewHolder.mPageCollegeTV.setTextColor(Color.parseColor("#666666"));
        mCreatePageViewHolder.mPageCollegeTV.setBackgroundResource(R.drawable.ic_gray_line);

        mCreatePageViewHolder.mPagePrivacyTV.setTextColor(Color.parseColor("#01579B"));
        mCreatePageViewHolder.layoutPrivacy.setBackgroundResource(R.drawable.ic_blue_line);

    }

    public static Intent getIntent(Context context, String mPageType) {
        Intent intent = new Intent(context, CreatePageActivity.class);
        intent.putExtra("PageTypeSelected", mPageType);
        return intent;
    }

    public class CreatePageViewHolder {

        private EditText mPageNameET, mPageDescripionET;
        private TextView mPageNameTV, mPageSportsLabelTV, mPageDescripionTV, mPageSportsTV,
                mPageConferenceTV, mPageCollegeTV, mPageConferenceLabelTV, mPageCollegeLabelTV;
        private TextView mPagePrivacyTV;
        private ImageView mPageLogoIV;
        private Switch mPrivacySwitch;
        private RelativeLayout layoutPrivacy;
        private View mViewDiscriptionLine;

        public CreatePageViewHolder(View view, OnClickListener listener) {
            mPageLogoIV = (ImageView) view.findViewById(R.id.page_logo_iv);
            mPageNameTV = (TextView) view.findViewById(R.id.team_name_tv);
            mPageNameET = (EditText) view.findViewById(R.id.team_name_et);
            mPageSportsLabelTV = (TextView) view.findViewById(R.id.team_sport_tv_label);
            mPageSportsTV = (TextView) view.findViewById(R.id.team_sport_tv);
            mPageConferenceLabelTV = (TextView) view.findViewById(R.id.team_conference_tv_label);
            mPageConferenceTV = (TextView) view.findViewById(R.id.team_conference_tv);
            mPageCollegeLabelTV = (TextView) view.findViewById(R.id.team_college_tv_label);
            mPageCollegeTV = (TextView) view.findViewById(R.id.team_college_tv);

            mPageDescripionTV = (TextView) view.findViewById(R.id.team_description_tv);
            mPageDescripionET = (EditText) view.findViewById(R.id.team_description_et);
            mViewDiscriptionLine = (View) view.findViewById(R.id.devider6);

            mPagePrivacyTV = (TextView) view.findViewById(R.id.team_privacy_tv);
            layoutPrivacy = (RelativeLayout) view.findViewById(R.id.layout_privacy);
            mPrivacySwitch = (Switch) view.findViewById(R.id.team_privacy_switch);
            mPrivacySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                    if (isChecked) {
                        mIsPublic = 2;
                    } else {
                        mIsPublic = 1;
                    }
                }
            });

            mPageLogoIV.setOnClickListener(listener);
            mPageSportsTV.setOnClickListener(listener);
            mPageConferenceTV.setOnClickListener(listener);
            mPageCollegeTV.setOnClickListener(listener);
            FontLoader.setRobotoRegularTypeface(mPageSportsTV, mPageNameET, mPageConferenceTV,
                    mPageCollegeTV, mPageDescripionET, mPagePrivacyTV);

            mPrivacySwitch.setChecked(true);
            if (mCategoryIds.equals("2")) {
                mPageConferenceTV.setVisibility(View.GONE);
                mPageConferenceLabelTV.setVisibility(View.GONE);
                mPageCollegeTV.setVisibility(View.GONE);
                mPageCollegeLabelTV.setVisibility(View.GONE);
                mPageLogoIV.setVisibility(View.GONE);
                mPrivacySwitch.setVisibility(View.VISIBLE);
                mPagePrivacyTV.setText(getResources().getString(R.string.team_privacy));
                //mPagePrivacyTV.setTextColor(getResources().getColor(R.color.black));
            } else {
                mPageConferenceTV.setVisibility(View.VISIBLE);
                mPageConferenceLabelTV.setVisibility(View.VISIBLE);
                mPageCollegeTV.setVisibility(View.VISIBLE);
                mPageCollegeLabelTV.setVisibility(View.VISIBLE);
                mPageLogoIV.setVisibility(View.VISIBLE);
                mPrivacySwitch.setVisibility(View.GONE);
                mPagePrivacyTV.setText(getResources().getString(R.string.Private));
                // mPagePrivacyTV.setTextColor(getResources().getColor(R.color.text_hint_color));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.page_logo_iv:
                displayImagePickerPopup();

                break;
            case R.id.team_sport_tv:
                changeColorOnSportsView();
                startActivityForResult(SportsListActivity.getIntent(mContext, false, mSelectedSportModel),
                        REQ_CODE_SELECT_SPORT);
                break;
            case R.id.team_conference_tv:
                changeColorOnConferenceView();
                startActivityForResult(ConferencesListActivity.getIntent(mContext, mConferenceModel),
                        REQ_CODE_SELECT_CONFERENCE);
                break;
            case R.id.team_college_tv:
                changeColorOnCollegeView();
                if (null != mConferenceModel) {
                    startActivityForResult(UniversityListActivity.getIntent(mContext, mUniversityModel,
                            mConferenceModel.mConferenceId), REQ_CODE_SELECT_UNIVERSITY);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQ_CODE_SELECT_SPORT:
                    if (data != null) {
                        mSelectedSportModel = data.getParcelableArrayListExtra("data");
                        if (mSelectedSportModel != null && mSelectedSportModel.size() > 0)
                            mCreatePageViewHolder.mPageSportsTV.setText(mSelectedSportModel.get(0).mSportsName);
                    }
                    break;

                case REQ_CODE_SELECT_CONFERENCE:
                    if (data != null) {
                        mConferenceModel = data.getParcelableExtra("data");
                        if (mConferenceModel != null)
                            mCreatePageViewHolder.mPageConferenceTV.setText(mConferenceModel.mConferenceName);
                    }
                    break;

                case REQ_CODE_SELECT_UNIVERSITY:
                    if (data != null) {
                        mUniversityModel = data.getParcelableExtra("data");
                        if (mUniversityModel != null)
                            mCreatePageViewHolder.mPageCollegeTV.setText(mUniversityModel.mUniversityName);
                    }
                    break;
            }
        }
    }

    /**
     * Display dialog to pick image for page logo
     */
    private void displayImagePickerPopup() {

        DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
            @Override
            public void onItemClick(int position, String item) {
                if (position == 0) {
                    openCamera();
                } else {
                    openGallery();
                }
            }

            @Override
            public void onCancel() {

            }
        }, mContext.getString(R.string.take_picture), mContext.getString(R.string.load_from_library));
    }

    @Override
    protected void onGalleryImageSelected(String fileUri, Bitmap bitmap) {

    }

    @Override
    protected void onCameraImageSelected(String fileUri, Bitmap bitmap) {
        mPhotoBitmap = bitmap;
        mCreatePageViewHolder.mPageLogoIV.setImageBitmap(ImageUtil.getCircledBitmap(mPhotoBitmap));
    }

    @Override
    protected void onVideoCaptured(String videoPath) {

    }

    @Override
    protected void onMediaPickCanceled(int reqCode) {

    }

    /**
     * Validate Team page require params
     */
    private boolean isValidated() {

        mGroupName = mCreatePageViewHolder.mPageNameET.getText().toString();

        mGroupDescription = mCreatePageViewHolder.mPageDescripionET.getText().toString();
        if (TextUtils.isEmpty(mGroupName.trim())) {
            mErrorLayout.showError(getResources().getString(R.string.page_name_require), true, MsgType.Error);
            return false;
        } else if (mSelectedSportModel == null || TextUtils.isEmpty(mSelectedSportModel.get(0).mSportsID)) {
            mErrorLayout.showError(getResources().getString(R.string.page_sports_require), true, MsgType.Error);
            return false;
        } else if (mCategoryIds.equals("1")) {
            if (mConferenceModel == null || TextUtils.isEmpty(mConferenceModel.mConferenceId)) {
                mErrorLayout.showError(getResources().getString(R.string.page_conference_require), true, MsgType.Error);
                return false;
            } else if (mUniversityModel == null || TextUtils.isEmpty(mUniversityModel.mUniversityId)) {
                mErrorLayout.showError(getResources().getString(R.string.page_university_require), true, MsgType.Error);
                return false;
            } else if (TextUtils.isEmpty(mGroupDescription.trim())) {
                mErrorLayout.showError(getResources().getString(R.string.page_description_require), true, MsgType.Error);
                return false;
            } else {
                return true;
            }
        } else if (TextUtils.isEmpty(mGroupDescription.trim())) {
            mErrorLayout.showError(getResources().getString(R.string.page_description_require), true, MsgType.Error);
            return false;
        } else {
            return true;
        }
    }

    /**
     * API to upload team page logo to server
     */
    private void uploadMediaServerRequest() {

        MediaUploadRequest mRequest = new MediaUploadRequest(mContext, false);
        String fileName = ImageUtil.getFileNameFromURI(cropImageUri, mContext);
        File mFile = new File(cropImageUri.getPath());
        final LogedInUserModel mLoggedInUserModel = new LogedInUserModel(mContext);
        BaseRequest.setLoginSessionKey(mLoggedInUserModel.mLoginSessionKey);
        mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_GROUPS,
                "", MediaUploadRequest.TYPE_PROFILE);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    AlbumMedia mAlbumMedia = (AlbumMedia) data;
                    createPageServerRequest(mAlbumMedia.ImageName);
                }
            }
        });
    }

    /**
     * Create server request to create a new page
     */
    private void createPageServerRequest(String mLogoName) {

        CreatePageRequest mRequest = new CreatePageRequest(mContext);
        String mConference = "", mCollege = "";
        if (null != mConferenceModel) {
            mConference = mConferenceModel.mConferenceId;
        }
        if (null != mUniversityModel) {
            mCollege = mUniversityModel.mUniversityId;
        }
        mRequest.CreatePageServerRequest(mGroupName, mGroupDescription, mLogoName, mGroupCoverImage, mCategoryIds,
                mIsPublic, mSelectedSportModel.get(0).mSportsID, mCollege, mConference, mGroupGUID);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(data.toString(), false, MsgType.Success);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 2000);
                } else {
                    mErrorLayout.showError(data.toString(), false, MsgType.Error);
                }
            }
        });
    }
}