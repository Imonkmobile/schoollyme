package com.schollyme.team;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.friends.BaseFriendFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.widget.PagerSlidingTabStrip;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 27/6/2017.
 */

public class TeamsFragment extends BaseFragment {

    private static final int PAGE_COUNT = 3;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private ViewPager mGenericVp;
    private ImageView mCreateTeamBtn;
    private GridPagerAdapter mGridPagerAdapter;
    private ErrorLayout mErrorLayout;
    private int IsCreateOfficialPage = 1;
    private int mCurrentPageToShow;
    private boolean mShowBackBtn = true;

    private HeaderLayout mHeaderLayout;
    private Context mContext;

    public static TeamsFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        TeamsFragment teamsFragment = new TeamsFragment();
        teamsFragment.mCurrentPageToShow = currentPageToShow;
        teamsFragment.mHeaderLayout = headerLayout;
        teamsFragment.mShowBackBtn = showBack;
        return teamsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.team_activity, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();

        initHeader();

        mGridPagerAdapter = new GridPagerAdapter(getChildFragmentManager());
        mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_container_ll));
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs_psts);
        mGenericVp = (ViewPager) view.findViewById(R.id.generic_vp);

        mGenericVp.setOffscreenPageLimit(1);
        mGenericVp.setAdapter(mGridPagerAdapter);

        mPagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                BaseFriendFragment baseFriendFragment = mGridPagerAdapter.getBaseFriendFragment(position);
                if (baseFriendFragment != null)
                    baseFriendFragment.onReload();
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });
        mPagerSlidingTabStrip.setViewPager(mGenericVp, getActivity());
        mGenericVp.postDelayed(new Runnable() {
            @Override
            public void run() {
                mGenericVp.setCurrentItem(mCurrentPageToShow);
            }
        }, 150);

        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) view.findViewById(R.id.createTeamImgMenu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                fabMenu.collapse();
                callCreateTeam();
            }

            @Override
            public void onMenuCollapsed() {

            }
        });

    }

    private void callCreateTeam() {

        if (new LogedInUserModel(mContext).mUserType == 1) {
            final String[] options = (IsCreateOfficialPage == 1 ? new String[]
                    {getString(R.string.official_team_page), getString(R.string.fan_page)}
                    : new String[]{getString(R.string.fan_page)});
            if (options.length == 2) {
                DialogUtil.showListDialog(mContext, 0, new DialogUtil.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position, String item) {
                        String mPageTypeSelected = (options.length == 2 ? Integer.toString(position + 1) : "2");
                        startActivity(CreatePageActivity.getIntent(mContext, mPageTypeSelected));
                    }

                    @Override
                    public void onCancel() {

                    }

                }, options);
            } else {
                String mPageTypeSelected = "2";
                startActivity(CreatePageActivity.getIntent(mContext, mPageTypeSelected));
            }

        } else {
            String mPageTypeSelected = "2";
            startActivity(CreatePageActivity.getIntent(mContext, mPageTypeSelected));
        }
    }

    private void initHeader() {

        try {
            mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                    getResources().getString(R.string.team_page_header), R.drawable.ic_search_header);
            mHeaderLayout.setListenerItI(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mShowBackBtn) {
                        getActivity().finish();
                    } else {
                        ((DashboardActivity) mContext).sliderListener();
                    }
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mGridPagerAdapter.getBaseFriendFragment(mGenericVp.getCurrentItem()).onSearchBtnClick();
                }
            });
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public class GridPagerAdapter extends FragmentPagerAdapter {

        private Map<Integer, BaseFriendFragment> mBaseFriendFragments;

        @SuppressLint("UseSparseArrays")
        public GridPagerAdapter(FragmentManager fm) {
            super(fm);
            mBaseFriendFragments = new HashMap<Integer, BaseFriendFragment>();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String tabTitle = "";
            switch (position) {
                case 0:
                    tabTitle = getString(R.string.official_tab);
                    break;
                case 1:
                    tabTitle = getString(R.string.fan_tab);
                    break;
                case 2:
                    tabTitle = getString(R.string.suggested_tab);
                    break;

            }
            return tabTitle;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            BaseFriendFragment mFragment = null;
            switch (position) {
                case 0:
                    mFragment = OfficialPageListFragment.newInstance(mErrorLayout);
                    break;
                case 1:
                    mFragment = FanPageListFragment.newInstance(mErrorLayout);
                    break;
                case 2:
                    mFragment = SuggestedPageListFragment.newInstance(mErrorLayout);
                    break;
            }
            mBaseFriendFragments.put(position, mFragment);
            return mFragment;
        }

        public BaseFriendFragment getBaseFriendFragment(int position) {
            return mBaseFriendFragments.get(position);
        }
    }

}
