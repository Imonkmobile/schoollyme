package com.schollyme.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.model.SportsModel;
import com.vinfotech.utility.FontLoader;

public class SportsDisplayAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private ArrayList<SportsModel> mSportsAL;
	private SportsDisplyHolder mSportsDisplyHolder;

	public SportsDisplayAdapter(Context context) {
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(ArrayList<SportsModel> mAL) {
		this.mSportsAL = mAL;
	}

	@Override
	public int getCount() {
		return null == mSportsAL ? 0 : mSportsAL.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		mSportsDisplyHolder = new SportsDisplyHolder();
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.sports_display_adapter_item_view, parent, false);
			mSportsDisplyHolder.mSportNameTV = (TextView) convertView.findViewById(R.id.sports_name_tv);
			mSportsDisplyHolder.mDeleteIB = (ImageButton) convertView.findViewById(R.id.delete_ib);
			convertView.setTag(mSportsDisplyHolder);
		} else {
			mSportsDisplyHolder = (SportsDisplyHolder) convertView.getTag();
		}
		FontLoader.setRobotoRegularTypeface(mSportsDisplyHolder.mSportNameTV);
		mSportsDisplyHolder.mSportNameTV.setText(mSportsAL.get(position).mSportsName);
		mSportsDisplyHolder.mDeleteIB.setTag(position);
		mSportsDisplyHolder.mDeleteIB.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSportsAL.remove(position);
				notifyDataSetChanged();
			}
		});
		
		mSportsDisplyHolder.mSportNameTV.setTag(position);
		mSportsDisplyHolder.mSportNameTV.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mSportsAL.remove(position);
				notifyDataSetChanged();
			}
		});
		return convertView;
	}

	public class SportsDisplyHolder {
		public TextView mSportNameTV;
		public ImageButton mDeleteIB;
	}

}
