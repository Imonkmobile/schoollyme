package com.schollyme.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.R;
import com.schollyme.friends.FriendProfileActivity;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.FriendAddRequest;
import com.vinfotech.request.FriendCancelRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class FriendSuggestionsAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private ArrayList<FriendModel> mSuggestedFriendsAL;
	private FriendsSuggestionViewHolder mFriendsSuggestionViewHolder;

	public FriendSuggestionsAdapter(Context context) {
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(ArrayList<FriendModel> mList) {
		this.mSuggestedFriendsAL = mList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mSuggestedFriendsAL ? 0 : mSuggestedFriendsAL.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup parent) {
		mFriendsSuggestionViewHolder = new FriendsSuggestionViewHolder();
		if (contentView == null) {
			contentView = mLayoutInflater.inflate(R.layout.friend_suggestions_item, parent, false);
			mFriendsSuggestionViewHolder.mFriendNameTV = (TextView) contentView.findViewById(R.id.user_name_tv);
			mFriendsSuggestionViewHolder.mFriendTypeTV = (TextView) contentView.findViewById(R.id.user_status_tv);
			mFriendsSuggestionViewHolder.mFriendAddRemoveButton = (Button) contentView.findViewById(R.id.add_remove_button);
			mFriendsSuggestionViewHolder.mFriendIV = (ImageView) contentView.findViewById(R.id.profile_image_iv);
			FontLoader.setRobotoMediumTypeface(mFriendsSuggestionViewHolder.mFriendNameTV,
					mFriendsSuggestionViewHolder.mFriendAddRemoveButton);
			FontLoader.setRobotoRegularTypeface(mFriendsSuggestionViewHolder.mFriendTypeTV);
			contentView.setTag(mFriendsSuggestionViewHolder);
		} else {
			mFriendsSuggestionViewHolder = (FriendsSuggestionViewHolder) contentView.getTag();
		}
		String mUserType = mSuggestedFriendsAL.get(position).mUserTypeID;
		if (mUserType.equals("1")) {
			mFriendsSuggestionViewHolder.mFriendTypeTV.setText(String.valueOf(Config.UserType.Coach));
		} else if (mUserType.equals("2")) {
			mFriendsSuggestionViewHolder.mFriendTypeTV.setText(String.valueOf(Config.UserType.Athlete));
		} else if (mUserType.equals("3")) {
			mFriendsSuggestionViewHolder.mFriendTypeTV.setText(String.valueOf(Config.UserType.Fan));
		}

		mFriendsSuggestionViewHolder.mFriendNameTV.setText(mSuggestedFriendsAL.get(position).mFirstName + " "
				+ mSuggestedFriendsAL.get(position).mLastName);
		mFriendsSuggestionViewHolder.mFriendNameTV.setTag(position);
		mFriendsSuggestionViewHolder.mFriendTypeTV.setTag(position);
		mFriendsSuggestionViewHolder.mFriendIV.setTag(position);
		mFriendsSuggestionViewHolder.mFriendNameTV.setOnClickListener(UserProfileLineListener);
		mFriendsSuggestionViewHolder.mFriendTypeTV.setOnClickListener(UserProfileLineListener);
		mFriendsSuggestionViewHolder.mFriendIV.setOnClickListener(UserProfileLineListener);
		
		String imageUrl = Config.IMAGE_URL_PROFILE + mSuggestedFriendsAL.get(position).mProfilePicture;
		ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mFriendsSuggestionViewHolder.mFriendIV,
				ImageLoaderUniversal.option_Round_Image);
		if (mSuggestedFriendsAL.get(position).mFriendStatus.equals("2")) {
			mFriendsSuggestionViewHolder.mFriendAddRemoveButton.setText(mContext.getResources().getString(R.string.Cancel));
		} else if (!mSuggestedFriendsAL.get(position).isRequestSent) {
			mFriendsSuggestionViewHolder.mFriendAddRemoveButton.setText(mContext.getResources().getString(R.string.add_label));
		} else {
			mFriendsSuggestionViewHolder.mFriendAddRemoveButton.setText(mContext.getResources().getString(R.string.undo_label));
		}
		mFriendsSuggestionViewHolder.mFriendAddRemoveButton.setTag(position);
		mFriendsSuggestionViewHolder.mFriendAddRemoveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				final int pos = (Integer) view.getTag();
				final FriendModel mModel = mSuggestedFriendsAL.get(pos);

				if (mSuggestedFriendsAL.get(pos).mFriendStatus.equals("2")) {
					mSuggestedFriendsAL.remove(pos);
					cancelFriendRequest(mModel.mUserGUID);
					notifyDataSetChanged();
				} else if (mModel.isRequestSent) {
					mModel.isRequestSent = false;
					mSuggestedFriendsAL.set(pos, mModel);
					cancelFriendRequest(mModel.mUserGUID);
					notifyDataSetChanged();
				} else {
					DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
						@Override
						public void onItemClick(int position, String item) {

							if (!mModel.isRequestSent) {
								mModel.isRequestSent = true;
								mSuggestedFriendsAL.set(pos, mModel);
								addFriendRequest(mModel.mUserGUID, "" + position);
								notifyDataSetChanged();
							}
						}

						@Override
						public void onCancel() {

						}
					}, mContext.getString(R.string.add_as_friend), mContext.getString(R.string.add_as_teammate));
				}
			}
		});
		return contentView;
	}

	OnClickListener UserProfileLineListener = new OnClickListener() {

		@Override
		public void onClick(View view) {
			int pos = (Integer) view.getTag();
			FriendModel mModel = mSuggestedFriendsAL.get(pos);

			if (new LogedInUserModel(mContext).mUserGUID.equals(mModel.mUserGUID)) {
				mContext.startActivity(DashboardActivity.getIntent(mContext,7, ""));
			} else {
				mContext.startActivity(FriendProfileActivity.getIntent(mContext,mModel.mUserGUID,
						mModel.mProfileLink, "FriendSuggestionsAdapter"));
			}
		}
	};

	private class FriendsSuggestionViewHolder {
		private TextView mFriendNameTV, mFriendTypeTV;
		private ImageView mFriendIV;
		private Button mFriendAddRemoveButton;
	}

	private void addFriendRequest(String friendGUID, String teammate) {
		FriendAddRequest mRequest = new FriendAddRequest(mContext);
		mRequest.AddFriendServerRequest(friendGUID, teammate);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {

				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
				}
			}
		});
	}

	private void cancelFriendRequest(String friendGUID) {
		FriendCancelRequest mRequest = new FriendCancelRequest(mContext);
		mRequest.CancelFriendServerRequest(friendGUID);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {

				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
				}
			}
		});
	}
}