package com.schollyme.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.model.Song;
import com.schollyme.model.SongSource;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class UserSongAdapter extends BaseAdapter {
	private static final String TAG = UserSongAdapter.class.getSimpleName();

	private LayoutInflater mLayoutInflater;
	private List<Song> mSongs;
	private boolean mEnableHeader = true;

	public UserSongAdapter(Context context) {
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setList(List<Song> list, boolean enableHeader) {
		this.mSongs = list;
		mEnableHeader = enableHeader;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mSongs ? 0 : mSongs.size();
	}

	@Override
	public Song getItem(int position) {
		return mSongs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.song_row, null);

			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final Song song = getItem(position);

		ImageLoaderUniversal.ImageLoadRound(viewHolder.songIv.getContext(), song.ImageURL, viewHolder.songIv,
				ImageLoaderUniversal.option_Round_Image);
		viewHolder.typeIv.setImageResource(SongSource.getSongSourceImgResId(song.SourceName));
		viewHolder.titleTv.setText(song.Title);
		viewHolder.subTitleTv.setText(song.ArtistName);
		viewHolder.descTv.setText(song.AlbumName);

		viewHolder.rightIv.setImageResource(song.selected ? R.drawable.ic_purchased : R.drawable.ic_buy);
		viewHolder.rightIv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!song.selected) {
					if (v.getContext().getString(R.string.Spotify).equalsIgnoreCase(song.SourceName)) {
						launchBrowser("https://www.spotify.com/");
					} else if (v.getContext().getString(R.string.iTunes).equalsIgnoreCase(song.SourceName)) {
						launchBrowser("http://www.apple.com/in/itunes/");
					} else {
						launchBrowser("http://www.schollyme.com/");
					}
				}
			}
		});

		if (mEnableHeader) {
			if (position == 0) {
				viewHolder.headerTv.setText(R.string.CURRENT_SONG_OF_THE_DAY);
				viewHolder.headerTv.setVisibility(View.VISIBLE);
				viewHolder.separator1.setVisibility(View.VISIBLE);
			} else if (position == 1) {
				viewHolder.headerTv.setText(R.string.MORE_SONGS);
				viewHolder.headerTv.setVisibility(View.VISIBLE);
				viewHolder.separator1.setVisibility(View.VISIBLE);
			} else {
				viewHolder.headerTv.setVisibility(View.GONE);
				viewHolder.separator1.setVisibility(View.GONE);
			}
		}

		return convertView;
	}

	private class ViewHolder {
		private ImageView songIv, typeIv, rightIv, overlayIv;
		private TextView titleTv, subTitleTv, descTv, headerTv;
		private View separator1;
		private ProgressBar bufferingPb;

		private ViewHolder(View view) {
			overlayIv = (ImageView) view.findViewById(R.id.overlay_iv);
			overlayIv.setVisibility(View.GONE);
			songIv = (ImageView) view.findViewById(R.id.song_iv);
			typeIv = (ImageView) view.findViewById(R.id.type_iv);
			rightIv = (ImageView) view.findViewById(R.id.right_iv);
			titleTv = (TextView) view.findViewById(R.id.title_tv);
			subTitleTv = (TextView) view.findViewById(R.id.sub_title_tv);
			descTv = (TextView) view.findViewById(R.id.desc_tv);
			headerTv = (TextView) view.findViewById(R.id.header_tv);
			separator1 = view.findViewById(R.id.separator1);
			bufferingPb = (ProgressBar) view.findViewById(R.id.buffering_pb);
			bufferingPb.setVisibility(View.GONE);

			FontLoader.setRobotoMediumTypeface(titleTv);
			FontLoader.setRobotoRegularTypeface(subTitleTv, descTv, headerTv);

			rightIv.setVisibility(View.GONE);
		}
	}

	private void launchBrowser(String url) {
		if (null != url && url.trim().length() < 1) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(url));
			mLayoutInflater.getContext().startActivity(intent);
		}
	}
}
