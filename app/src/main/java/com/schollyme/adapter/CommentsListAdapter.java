package com.schollyme.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.friends.FriendProfileActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.CommentModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.DeleteCommentRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.CalStartEndUtil;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class CommentsListAdapter extends BaseAdapter {
	private static final String TAG = AlbumAdapter.class.getSimpleName();
	private LayoutInflater mLayoutInflater;
	private List<CommentModel> mList;
	private Context mContext;
	private ErrorLayout mErrorLayout;

	public CommentsListAdapter(Context context, ErrorLayout errorlayout) {
		mContext = context;
		this.mErrorLayout = errorlayout;
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setList(List<CommentModel> list) {
		this.mList = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null == mList ? 0 : mList.size();
	}

	@Override
	public CommentModel getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.comment_row, null);

			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final CommentModel mCommentModel = getItem(position);
		if (null != mCommentModel) {

			String imageUrl = Config.IMAGE_URL_PROFILE + mCommentModel.ProfilePicture;
			ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, viewHolder.mUserImg, ImageLoaderUniversal.option_Round_Image);
			viewHolder.mUserName.setText(mCommentModel.Name);

			viewHolder.mUserImg.setTag(mCommentModel);
			viewHolder.mUserName.setTag(mCommentModel);
			/*
			 * viewHolder.mUserImg.setOnClickListener(mOnClickListener);
			 * viewHolder.mUserName.setOnClickListener(mOnClickListener);
			 */
			viewHolder.mDescTv.setText(Html.fromHtml(mCommentModel.PostComment));
			Calendar cal = getCalTime(mCommentModel.CreatedDate);

			if (null != cal) {
				viewHolder.mTime.setText(CalStartEndUtil.getDifference(mContext, cal));

			} else {
				viewHolder.mTime.setText((mCommentModel.CreatedDate));
			}

			if (mCommentModel.CanDelete == 1) {
				viewHolder.mDeteleIV.setVisibility(View.VISIBLE);
			} else {
				viewHolder.mDeteleIV.setVisibility(View.GONE);
			}
			viewHolder.mDeteleIV.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					DeleteComment(mCommentModel.CommentGUID);
				}
			});

			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (new LogedInUserModel(mContext).mUserGUID.equals(mCommentModel.UserGUID)) {
						mContext.startActivity(DashboardActivity.getIntent(mContext, 1, ""));
					} else {
						mContext.startActivity(FriendProfileActivity.getIntent(mContext, mCommentModel.UserGUID,
								mCommentModel.ProfileLink,
								"BroadcastMemberAdapter"));
					}
				}
			});
		}

		return convertView;
	}

	// private OnClickListener mOnClickListener = new OnClickListener() {
	// @Override
	// public void onClick(View view) {
	// CommentModel commentModel = (CommentModel) view.getTag();
	// if (new
	// LogedInUserModel(mContext).mUserGUID.equals(commentModel.UserGUID)) {
	// mContext.startActivity(DashboardActivity.getIntent(mContext, 6, ""));
	// } else {
	// mContext.startActivity(FriendProfileActivity.getIntent(mContext,
	// commentModel.UserGUID, commentModel.ProfileLink,
	// "BroadcastMemberAdapter"));
	// }
	// }
	// };

	private DeleteCommentRequest mDeleteCommentRequest;

	public void DeleteComment(final String commentGuid) {
		DialogUtil.showOkCancelDialog(mContext, R.string.Delete, R.string.ok_label, R.string.cancel_caps,
				mContext.getResources().getString(R.string.Delete_Comment), mContext
				.getResources().getString(R.string.Confirm_delete_Comment), new OnClickListener() {

			@Override
			public void onClick(View v) {

				mDeleteCommentRequest = new DeleteCommentRequest(mContext);
				mDeleteCommentRequest.setRequestListener(new RequestListener() {

					@Override
					public void onComplete(boolean success, Object data, int totalRecords) {
						if (success) {
							((CommentListActivity) mContext).deleteAndRefresh();
						} else {
							if (null != mErrorLayout) {
								mErrorLayout.showError((null == data ? mContext.getString(R.string.Something_went_wrong) : (String) data),
										true, MsgType.Error);
							}
						}
					}
				});
				mDeleteCommentRequest.deleteCommentFromServer(commentGuid);
			}
		}, null);

	}

	Calendar cal = Calendar.getInstance();

	public Calendar getCalTime(String dateString) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {

			cal.setTime(sdf.parse(dateString));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TimeZone tz = cal.getTimeZone();
		int Offset = tz.getOffset(cal.getTimeInMillis());
		cal.setTimeInMillis(cal.getTimeInMillis() + Offset);

		return cal;
	}

	private class ViewHolder {
		private ImageView mUserImg;
		private ImageButton mDeteleIV;
		private TextView mUserName, mDescTv, mTime;

		private ViewHolder(View view) {
			mUserImg = (ImageView) view.findViewById(R.id.user_iv);
			mUserName = (TextView) view.findViewById(R.id.title_tv);
			mDescTv = (TextView) view.findViewById(R.id.desc_tv);
			mTime = (TextView) view.findViewById(R.id.time_tv);
			mDeteleIV = (ImageButton) view.findViewById(R.id.clear_iv);

			FontLoader.setRobotoRegularTypeface(mUserName, mDescTv, mTime);
		}

	}
}
