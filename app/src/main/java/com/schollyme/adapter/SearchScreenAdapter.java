package com.schollyme.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.FriendModel;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class SearchScreenAdapter extends BaseAdapter {
    private static final String TAG = AlbumAdapter.class.getSimpleName();
    private LayoutInflater mLayoutInflater;
    private List<FriendModel> mList;
    private Context mContext;

    public SearchScreenAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setList(List<FriendModel> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == mList ? 0 : mList.size();
    }

    @Override
    public FriendModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.search_screen_row, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final FriendModel mFriendModel = getItem(position);
        if (null != mFriendModel) {

            String imageUrl = Config.IMAGE_URL_PROFILE + mFriendModel.mProfilePicture;
            ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, viewHolder.mUserImg,
                    ImageLoaderUniversal.option_Round_Image);
            viewHolder.mUserName.setText(mFriendModel.mFirstName + " " + mFriendModel.mLastName);

            String userType = "";
            if (mFriendModel.mUserTypeID.equals("1")) {
                userType = "Coach";
            } else if (mFriendModel.mUserTypeID.equals("2")) {
                userType = "Athlete";
            } else if (mFriendModel.mUserTypeID.equals("3")) {
                userType = "Fan";
            }
            viewHolder.sport_tv.setText(userType);
        }

        return convertView;
    }

    private class ViewHolder {

        private ImageView mUserImg;
        private TextView mUserName, sport_tv;

        private ViewHolder(View view) {
            mUserImg = (ImageView) view.findViewById(R.id.user_iv);
            mUserName = (TextView) view.findViewById(R.id.user_name);
            sport_tv = (TextView) view.findViewById(R.id.sport_tv);

            FontLoader.setRobotoMediumTypeface(mUserName);
            FontLoader.setRobotoRegularTypeface(sport_tv);
        }

    }
}
