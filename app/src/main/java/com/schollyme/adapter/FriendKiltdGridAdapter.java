package com.schollyme.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.schollyme.R;
import com.schollyme.media.MediaViewerActivity;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewsFeed;
import com.schollyme.team.TeamPageActivity;
import com.schollyme.wall.MediaViewerWallActivity;
import com.schollyme.wall.SingleMediaViewWallActivity;
import com.vinfotech.request.DeleteFeedRequest;
import com.vinfotech.request.FlagMediaRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.ImageUtil;
import com.vinfotech.utility.Utility;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import chintan.khetiya.android.Twitter_code.Twitt_Sharing;

/**
 * Created by user on 10/8/2017.
 */

public class FriendKiltdGridAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private float mExtraSpace, mImgWidth;
    private List<NewsFeed> mNewsFeeds;
    private KiltdMediaAdapter.OnItemClickListenerPost onItemClickListenerPost;

    public final String consumer_key = "DAIEsfXAb0IgmpUcunHBORtVD";
    public final String secret_key = "NmuIFsjTHiwrQkVpBJHNiZOtotXsoXklnpqeMcc4h657eplb4a";
    private String mUserGUID;
    private boolean mShareEnabled = true, mWriteEnabled = true, mShowBuzzMarker = true;

    public FriendKiltdGridAdapter(Context context, KiltdMediaAdapter.OnItemClickListenerPost onItemClickListenerPost, String mUserGUID_) {
        this.mLayoutInflater = LayoutInflater.from(context);
        this.onItemClickListenerPost = onItemClickListenerPost;
        this.mContext = context;
        this.mNewsFeeds = new ArrayList<NewsFeed>();
        this.mUserGUID = mUserGUID_;
        int[] screenDimen = new int[2];
        DisplayUtil.probeScreenSize(context, screenDimen);
        this.mExtraSpace = context.getResources().getDimension(R.dimen.space_large16);
        this.mImgWidth = screenDimen[0];
    }

    public void setList(List<NewsFeed> list) {
        mNewsFeeds = list;
        notifyDataSetChanged();
    }

    public void setList(List<NewsFeed> list, boolean shareEnabled) {
        mNewsFeeds = list;
        mShareEnabled = shareEnabled;
        notifyDataSetChanged();
    }

    public void setWriteEnabled(boolean enabled, boolean showBuzzMarker) {
        mWriteEnabled = enabled;
        mShowBuzzMarker = showBuzzMarker;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == mNewsFeeds ? 0 : mNewsFeeds.size();
    }

    public void onClickTwitt(final NewsFeed newsfeed, final String string_img_url, final String string_msg) {

        if (isNetworkAvailable()) {
            final Twitt_Sharing twitt = new Twitt_Sharing((Activity) mContext, consumer_key, secret_key);
            String imgPath = "";
            if (!TextUtils.isEmpty(string_img_url)) {
                if (MediaViewerActivity.isYouTubeVideo(string_img_url)) {
                    imgPath = MediaViewerActivity.getYoutubeVideoThumb(string_img_url);
                } else {
                    if (null != newsfeed.Album.AlbumMedias && newsfeed.Album.AlbumMedias.size() > 0
                            && AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(newsfeed.Album.AlbumMedias.get(0).MediaType)) {
                        imgPath = Config.getCoverPath(newsfeed.Album, Config.getVideoToJPG(newsfeed
                                .Album.AlbumMedias.get(0).ImageName), true);
                    } else {
                        imgPath = Config.getCoverPath(newsfeed.Album, newsfeed.Album.AlbumMedias.get(0).ImageName, false);
                    }
                }

                ImageLoaderUniversal.getImageLoader().loadImage(imgPath, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        File file = ImageUtil.saveBitmap(loadedImage, Environment.getExternalStorageDirectory(), "twitt.jpg");
                        if (MediaViewerActivity.isYouTubeVideo(string_img_url)) {
                            twitt.shareToTwitter(string_img_url, file);
                        } else {
                            String mediaUrl = "";
                            if (null != newsfeed.Album.AlbumMedias && newsfeed.Album.AlbumMedias.size() > 0
                                    && AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(newsfeed.Album.AlbumMedias.get(0).MediaType)) {
                                mediaUrl = Config.getCoverPath(newsfeed.Album, newsfeed.Album.AlbumMedias.get(0).ImageName, true, false);
                            }
                            twitt.shareToTwitter(mediaUrl, file);
                        }
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        Toast.makeText(mContext, "Twitter: unsupported media.", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (!TextUtils.isEmpty(string_msg)) {
                twitt.shareToTwitter(string_msg, null);
            }

        } else {
            // showToast("No Network Connection Available !!!");
        }
    }

    // when user will click on twitte then first that will check that is
    // internet exist or not
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public NewsFeed getItem(int position) {
        return mNewsFeeds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 100+position;
    }

    public static final int ConvertViewID = 56565;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.friend_kiltd_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (position == 0 && mWriteEnabled) {
            viewHolder.mContainerMediaRl.setVisibility(View.GONE);
        } else {
            viewHolder.mContainerMediaRl.setVisibility(View.VISIBLE);

            final NewsFeed newsFeedModel = getItem(position);
            final List<AlbumMedia> albumMedias = newsFeedModel.Album.AlbumMedias;
            final Activity activity = ((Activity) mContext);

            String mediaUrl = "";
            if (albumMedias != null && albumMedias.size()>0) {
                Utility.LogP("getView ","position: "+position);
               if (albumMedias.size() == 1) {
                    viewHolder.mContainerMediaRl.setVisibility(View.VISIBLE);
                    viewHolder.mWallPostIV.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams layoutParams = viewHolder.mWallPostIV.getLayoutParams();
                    layoutParams.width = (int) mImgWidth;
                    layoutParams.height = (int) (mImgWidth * 0.66f);
                    viewHolder.mWallPostIV.setLayoutParams(layoutParams);
                    if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
                        viewHolder.mPlayIV.setVisibility(View.GONE);
                        viewHolder.mLoaderUIL.setVisibility(View.GONE);
                        albumMedias.get(0).fullCoverPath = Config.getCoverPath(newsFeedModel.Album,
                                albumMedias.get(0).ImageName, false, true);
                        ImageLoaderUniversal.ImageLoadSquare(mContext, albumMedias.get(0).fullCoverPath,
                                viewHolder.mWallPostIV, ImageLoaderUniversal.option_normal_Image);
                    } else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
                        viewHolder.mPlayIV.setVisibility(View.VISIBLE);
                        albumMedias.get(0).fullCoverPath = Config.getCoverPath(newsFeedModel.Album,
                                Config.getVideoToJPG(albumMedias.get(0).ImageName), true, true);
                        Utility.LogP("PostFeedValue ", "albumMedias: " + albumMedias.get(0).fullCoverPath);
                        ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, albumMedias.get(0).fullCoverPath,
                                viewHolder.mWallPostIV, ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoaderUIL);

                    } else if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
                        viewHolder.mPlayIV.setVisibility(View.VISIBLE);
                        albumMedias.get(0).fullCoverPath = MediaViewerActivity.getYoutubeVideoThumb
                                (albumMedias.get(0).ImageName);
                        ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, albumMedias.get(0)
                                        .fullCoverPath, viewHolder.mWallPostIV,
                                ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoaderUIL);
                    }

                } else if (albumMedias.size() > 1) {
                    viewHolder.mContainerMediaRl.setVisibility(View.VISIBLE);
                    viewHolder.mLoaderUIL.setVisibility(View.GONE);
                    viewHolder.mPlayIV.setVisibility(View.GONE);

                    viewHolder.mWallPostIV.setVisibility(View.GONE);
                }
            } else {
                viewHolder.mContainerMediaRl.setVisibility(View.GONE);
            }



            viewHolder.mWallPostIV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
                        playYouTubeURl(albumMedias.get(0).ImageName, activity);
                    } else if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
                        if (newsFeedModel.Album.AlbumName.startsWith("Wall Photos")) {
                            activity.startActivity(SingleMediaViewWallActivity.getIntent(activity,
                                    albumMedias.get(0).ImageName, "IMAGE", true));
                            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        } else {

                            Activity activity = ((Activity) mContext);
                            ArrayList<AlbumMedia> albumMedias_ = new ArrayList<AlbumMedia>(albumMedias);
                            System.out.println("ACTIVITY TYPE-- " + newsFeedModel.ActivityType);
                            if ("ShareMediaSelf".equals(newsFeedModel.ActivityType)) {
                                activity.startActivityForResult(MediaViewerWallActivity.getIntent(activity, 0,
                                        newsFeedModel.Album, true, albumMedias_.get(0).MediaGUID, newsFeedModel.IsOwner),
                                        MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
                                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            } else {
                                activity.startActivityForResult(MediaViewerWallActivity.getIntent(activity, 0,
                                        newsFeedModel.Album, false, "", newsFeedModel.IsOwner),
                                        MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
                                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        }

                    } else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(albumMedias.get(0).MediaType)) {
                        if (newsFeedModel.Album.AlbumName.startsWith("Wall Video")) {
                            activity.startActivity(SingleMediaViewWallActivity.getIntent(activity,
                                    albumMedias.get(0).ImageName, "VIDEO", true));
                            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        } else {

                            Activity activity = ((Activity) mContext);
                            ArrayList<AlbumMedia> albumMedias_ = new ArrayList<AlbumMedia>(albumMedias);
                            activity.startActivityForResult(MediaViewerActivity.getIntent(activity,
                                    albumMedias_, 0, newsFeedModel.Album, newsFeedModel.IsOwner),
                                    MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
                            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    }
                }
            });

        }

        return convertView;
    }

    public void playYouTubeURl(String url, Activity activity) {

        String id = MediaViewerActivity.getYouTubeVideoId(url);
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
            activity.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(MediaViewerActivity.youtubePrefix2 + id));
            activity.startActivity(intent);
        }

    }

    private String[] options;

    protected void showMenuOptionDialog(final NewsFeed newsFeedModel, final int position) {
        int pos = -1;
        boolean allowTwitterShare = false;
        boolean isOwner = newsFeedModel.IsOwner == 1 ? true : false;
        boolean flag = (newsFeedModel.IsFlagged == 0 && newsFeedModel.FlagAllowed == 1 && newsFeedModel.Flaggable == 1);
        /*if(newsFeedModel.IsOwner == 1){
            flag = false;
		}*/
        if ((null != newsFeedModel.Album && null != newsFeedModel.Album.AlbumMedias
                && newsFeedModel.Album.AlbumMedias.size() > 0)
                || !TextUtils.isEmpty(newsFeedModel.PostContent)) {
            if (newsFeedModel.IsOwner == 1 || newsFeedModel.IsEntityOwner == 1) {
                allowTwitterShare = true;
            }
        }

        if (allowTwitterShare && flag) {
            if (isOwner) {
                options = new String[]{mContext.getString(R.string.post_twitter), mContext.getString(R.string.delete_label)};
            } else {
                options = new String[]{mContext.getString(R.string.post_twitter), mContext.getString(R.string.delete_label), mContext.getString(R.string.flag)};
            }

        } else if (allowTwitterShare && !flag && newsFeedModel.IsFlagged == 0) {
            options = new String[]{mContext.getString(R.string.post_twitter), mContext.getString(R.string.delete_label)};
        } else if (allowTwitterShare && !flag && newsFeedModel.IsFlagged == 1) {
            pos = 2;
            options = new String[]{mContext.getString(R.string.post_twitter), mContext.getString(R.string.delete_label), mContext.getString(R.string.flaged)};
        } else if (!allowTwitterShare && flag) {
            options = new String[]{mContext.getString(R.string.flag)};
        } else if (!allowTwitterShare && !flag && newsFeedModel.IsFlagged == 1) {
            pos = 0;
            options = new String[]{mContext.getString(R.string.flaged)};
        }


        //	options = flag?new String[] { mContext.getString(R.string.post_twitter),
        // mContext.getString(R.string.Delete), mContext.getString(R.string.flag) }:
        // allowTwitterShare ? new String[] { mContext.getString(R.string.post_twitter),
        // mContext.getString(R.string.Delete)} : new String[] { mContext.getString(R.string.Delete) };

        if (null != options && options.length > 0)
            DialogUtil.showListCustomDialog(mContext, 0, new DialogUtil.OnItemClickListener() {
                @Override
                public void onItemClick(int position, String item) {
                    switch (item) {
                        case "Publicar en twitter":
                            break;
                        case "Post on twitter":
                            if (options.length == 1) {
                                showDeleteDialog(newsFeedModel, position);
                            } else {
                                Album malbum = newsFeedModel.Album;
                                if (null != malbum.AlbumMedias && malbum.AlbumMedias.size() > 0) {
                                    onClickTwitt(newsFeedModel, newsFeedModel.Album.AlbumMedias.get(0).ImageName,
                                            newsFeedModel.PostContent);
                                } else {
                                    onClickTwitt(newsFeedModel, "", newsFeedModel.PostContent);
                                }
                            }

                            break;
                        case "borrar":
                            break;
                        case "Delete":
                            showDeleteDialog(newsFeedModel, position);
                            break;
                        case "Flag":
                            showFlagInputDialog(newsFeedModel, position);
                            break;
                        case "Bandera":
                            showFlagInputDialog(newsFeedModel, position);
                            break;
                        case "Flaged":
                            break;
                        case "marcado":
                            //	showFlagInputDialog(newsFeedModel,position);
                            break;
                    }
                }

                @Override
                public void onCancel() {

                }
            }, pos, options);

    }

    private void showFlagInputDialog(final NewsFeed newsFeed, final int pos) {

        DialogUtil.showSingleEditDialog(mContext, R.string.flag_reason, "",
                new DialogUtil.SingleEditListener() {
                    @Override
                    public void onEdit(boolean canceled, EditText editText, String text) {
                        String reason = editText.getText().toString();
                        if (TextUtils.isEmpty(reason)) {
                            showFlagInputDialog(newsFeed, pos);
                        } else {
                            FlagMediaRequest request = new FlagMediaRequest(mContext);
                            request.setRequestListener(new BaseRequest.RequestListener() {
                                @Override
                                public void onComplete(boolean success, Object data, int totalRecords) {
                                    NewsFeed feed = newsFeed;
                                    feed.IsFlagged = 1;
                                    mNewsFeeds.set(pos, feed);
                                    setList(mNewsFeeds);
                                }
                            });
                            String EntityGUID = newsFeed.ActivityGUID;
                            String EntityType = "ACTIVITY";
                            String FlagReason = reason;
                            request.FlagMediaServerRequest(EntityGUID, EntityType, FlagReason);
                        }
                    }
                });
    }

    public interface OnItemClickListenerPost {
        void onClickItems(int ID, int position, NewsFeed newsFeed);

    }

    protected void showDeleteDialog(final NewsFeed newsFeedModel, final int Position) {

        DialogUtil.showOkCancelDialog(mContext, R.string.Delete, R.string.cancel_caps,
                mContext.getString(R.string.Delete_post),
                mContext.getString(R.string.Confirm_delete_Post), new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mDeleteFeedRequest = new DeleteFeedRequest(mContext);
                        mDeleteFeedRequest.deleteNewsFeedFromServer(newsFeedModel.ActivityGUID);
                        mDeleteFeedRequest.setRequestListener(new BaseRequest.RequestListener() {
                            @Override
                            public void onComplete(boolean success, Object data, int totalRecords) {
                                if (success) {
                                    mNewsFeeds.remove(newsFeedModel);
                                    notifyDataSetChanged();
                                    // DialogUtil.showOkDialog(mContext,
                                    // (String) data, "");
                                } else {
                                    DialogUtil.showOkDialog(mContext, (String) data, "");
                                }
                            }
                        });
                    }
                }, null);
    }

    private DeleteFeedRequest mDeleteFeedRequest;

    private void updateCommentCount(TextView textView, NewsFeed newsFeed) {
        textView.setText("" + newsFeed.NoOfComments);// + " "
        //  + (textView.getResources().getString(1 == newsFeed.NoOfComments ? R.string.Comment : R.string.Comments)));
        //textView.setVisibility(newsFeed.NoOfComments > 0 ? View.VISIBLE : View.GONE);

    }

    private void updateLikeCount(ImageView image, TextView textView, NewsFeed newsFeed) {

        image.setImageResource(1 == newsFeed.NoOfLikes ? R.drawable.ic_like_media_active : R.drawable.ic_like_media);
        textView.setText("" + newsFeed.NoOfLikes);// + " "
        // + (textView.getResources().getString(1 == newsFeed.NoOfLikes ? R.string.Like : R.string.Likes)));
        //textView.setVisibility(newsFeed.NoOfLikes > 0 ? View.VISIBLE : View.GONE);
    }


    /*
     * For Tittle in message and pass newsfeed object
     */
    private static final String USER_MARKER = "{{User}}";
    private static final String ENTITY_MARKER = "{{Entity}}";
    private static final String COUNT_MARKER = "{{count}}";
    private static final String ALBUM_TYPE_MARKER = "{{AlbumType}}";
    private static final String SUBJECT_MARKER = "{{SUBJECT}}";
    private static final String OBJECT_MARKER = "{{OBJECT}}";
    private static final String ENTITYTYPE_MARKER = "{{ENTITYTYPE}}";

    public static String ACTIVITYTYPE_AlbumAdded = "AlbumAdded";
    public static String ACTIVITYTYPE_AlbumUpdate = "AlbumUpdated";
    public static String ACTIVITYTYPE_GroupJoined = "GroupJoined";
    public static String ACTIVITYTYPE_GroupPostAdded = "GroupPostAdded";
    public static String ACTIVITYTYPE_PagePost = "PagePost";
    public static String ACTIVITYTYPE_FriendAdded = "FriendAdded";
    public static String ACTIVITYTYPE_Share = "Share";
    public static String ACTIVITYTYPE_ShareMedia = "ShareMedia";
    public static String ACTIVITYTYPE_ShareSelf = "ShareSelf";
    public static String ACTIVITYTYPE_ShareMediaSelf = "ShareMediaSelf";
    public static String ACTIVITYTYPE_Post = "Post";
    public static String ACTIVITYTYPE_PostSelf = "PostSelf";
    public static String ACTIVITYTYPE_DefaultAlbumAdded = "DefaultAlbumAdded";

    public String getMessage(NewsFeed newsFeed) {

        String message = newsFeed.Message;

        if (mContext instanceof TeamPageActivity) {
            if (message.startsWith(USER_MARKER) && !newsFeed.ActivityType.equals(ACTIVITYTYPE_GroupJoined)) {
                message = message.substring(0, USER_MARKER.length());
            }
        }

        if (message.contains(USER_MARKER)) {

            if (null != newsFeed.ActivityType && newsFeed.ActivityType.equals("GroupPostAdded")) {

                if (mContext instanceof TeamPageActivity) {
                    message = message.replace(USER_MARKER, getLink(newsFeed.UserName, "UserGUID="
                            + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL, "Profile"));
                } else {
                    message = message.replace(USER_MARKER, getLink(newsFeed.UserName, "UserGUID="
                            + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL, "Profile"))
                            + " " + mContext.getResources().getString(R.string.posted_in) + " " + getLink(newsFeed.EntityName, "TeamGUID=" + newsFeed.EntityGUID, "Group");
                }

            } else {
                message = message.replace(USER_MARKER, getLink(newsFeed.UserName, "UserGUID="
                        + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL, "Profile"));
            }

        }

        if (newsFeed.ActivityType.equals(ACTIVITYTYPE_AlbumAdded)) {

            if (message.contains(ENTITY_MARKER)) {
                // ALBUMNAMECASE
                // message = message.replace(ENTITY_MARKER,
                // newsFeed.EntityName);
                message = message.replace(
                        ENTITY_MARKER,
                        getLink(newsFeed.EntityName, "UserGUID=" + newsFeed.UserGUID + "&FromQuery=FromQuery&AlbumGUID="
                                + newsFeed.Album.AlbumGUID, "Added"));

            }
            if (message.contains(COUNT_MARKER)) {
                message = message.replace(COUNT_MARKER, "" + newsFeed.Count);
            }
            if (message.contains(ALBUM_TYPE_MARKER)) {
                if (null != newsFeed.Album && null != newsFeed.Album.AlbumName) {
                    message = message.replace(ALBUM_TYPE_MARKER, newsFeed.Album.AlbumType);
                } else {
                    message = message.replace(ALBUM_TYPE_MARKER, "");
                }
            }

        } else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_AlbumUpdate)) {

            if (message.contains(COUNT_MARKER)) {
                message = message.replace(COUNT_MARKER, "" + newsFeed.Count);
            }

            if (message.contains(ALBUM_TYPE_MARKER)) {
                if (null != newsFeed.Album && null != newsFeed.Album.AlbumName) {
                    message = message.replace(ALBUM_TYPE_MARKER, newsFeed.Album.AlbumType);
                } else {
                    message = message.replace(ALBUM_TYPE_MARKER, "");
                }
            }
            if (message.contains(ENTITY_MARKER)) {
                message = message.replace(ENTITY_MARKER, newsFeed.EntityName);
            }

        } else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_GroupJoined)) {

            if (message.contains(ENTITY_MARKER)) {

                if (mContext instanceof TeamPageActivity) {
                    message = message.replace(ENTITY_MARKER, "");
                    // message = message.replace(ENTITY_MARKER,
                    // newsFeed.EntityName);
                } else {
                    message = message.replace(ENTITY_MARKER,
                            getLink(newsFeed.EntityName, "TeamGUID=" + newsFeed.EntityGUID + "&FromQuery=FromQuery", "Group"));
                }

            }

        } else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_GroupPostAdded)) {

            if (message.contains(ENTITY_MARKER)) {
                message = message.replace(ENTITY_MARKER, newsFeed.EntityName);
            }
        } else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_PagePost)) {
            if (message.contains(ENTITY_MARKER)) {
                message = message.replace(ENTITY_MARKER, newsFeed.EntityName);
            }

        } else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_FriendAdded)) {

            if (message.contains(ENTITY_MARKER)) {

                // message = message.replace(ENTITY_MARKER,
                // newsFeed.EntityName);

                message = message.replace(
                        ENTITY_MARKER,
                        getLink(newsFeed.EntityName, "UserGUID=" + newsFeed.UserGUID
                                + "&UserProfileLink=" + newsFeed.EntityProfileURL, "Profile"));
            }
        } else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_Share) || newsFeed.ActivityType.equals(ACTIVITYTYPE_ShareMedia)) {

            if (newsFeed.UserGUID.equals(mUserGUID)) {

                message = message.replace(SUBJECT_MARKER,
                        getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + "NOTREDIRECTION", "Profile"));

            } else {
                message = message.replace(
                        SUBJECT_MARKER,
                        getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink=" + newsFeed.UserProfileURL,
                                "Profile"));
            }

            if (message.contains(ENTITYTYPE_MARKER)) {
                // if (newsFeed.Album.l) {
                // message = message.replace(ENTITYTYPE_MARKER,"Post" )
                // }else {
                // message = message.replace(ENTITYTYPE_MARKER,"Post" )
                //
                // // AlbumType first Index
                // }

				/* SHARE TYPE MISSING */
                message = message.replace(ENTITYTYPE_MARKER, newsFeed.EntityType);
            }

            if (message.contains(OBJECT_MARKER)) {
                message = message.replace(OBJECT_MARKER, getLink(newsFeed.ActivityOwner, "UserGUID="
                        + newsFeed.EntityGUID + "&UserProfileLink=" + newsFeed.ActivityOwnerLink, "Profile"));

                // message = message.replace(OBJECT_MARKER,
                // newsFeed.EntityName);
            }
        } else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_ShareSelf)
                || newsFeed.ActivityType.equals(ACTIVITYTYPE_ShareMediaSelf)) {

            // if (newsFeed.UserGUID.equals(mUserGUID)) {
            //
            // message = message.replace(SUBJECT_MARKER, newsFeed.UserName);
            // } else {
            message = message.replace(SUBJECT_MARKER,
                    getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID
                            + "&UserProfileLink=" + newsFeed.UserProfileURL, "Profile"));
            // }

            if (message.contains(ENTITYTYPE_MARKER)) {
                message = message.replace(ENTITYTYPE_MARKER, newsFeed.EntityType);
            }

            if (message.contains(OBJECT_MARKER)) {
                LogedInUserModel mLogedInUserModel = new LogedInUserModel(mContext);
                if (newsFeed.ActivityOwnerLink.equals(newsFeed.UserProfileURL)) {

                    if (mLogedInUserModel.mUserGender.equals("1")) {
                        message = message.replace(OBJECT_MARKER, mContext.getResources().getString(R.string.his));
                    } else {
                        message = message.replace(OBJECT_MARKER, mContext.getResources().getString(R.string.her));
                    }
                    if (message.contains("his's")) {
                        message = message.replace("his's", "his");
                    } else if (message.contains("her's")) {
                        message = message.replace("her's", "her");
                    }

					/*
                     * message = message.replace( OBJECT_MARKER,
					 * getLink(newsFeed.ActivityOwner, "UserGUID=" +
					 * newsFeed.EntityGUID + "&UserProfileLink=" +
					 * newsFeed.ActivityOwnerLink, "Profile"));
					 */
                } else {
                    message = message.replace(
                            OBJECT_MARKER, getLink(newsFeed.ActivityOwner, "UserGUID="
                                    + newsFeed.EntityGUID + "&UserProfileLink="
                                    + newsFeed.ActivityOwnerLink, "Profile"));
                }

            }

        } else if (newsFeed.ActivityType.equals(ACTIVITYTYPE_Post)) {

            if (message.contains(OBJECT_MARKER)) {

                message = message.replace(
                        OBJECT_MARKER,
                        getLink(newsFeed.EntityName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink="
                                + newsFeed.EntityProfileURL, "Profile"));
                // message = message.replace(OBJECT_MARKER,
                // newsFeed.EntityName);
            }
            if (message.contains(SUBJECT_MARKER)) {
                // if (newsFeed.UserGUID.equals(mUserGUID)) {
                // message = message.replace(SUBJECT_MARKER, newsFeed.UserName);
                // } else {
                message = message.replace(
                        SUBJECT_MARKER,
                        getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink="
                                + newsFeed.UserProfileURL, "Profile"));
                // }
            }

        } else if ((newsFeed.ActivityType.equals(ACTIVITYTYPE_PostSelf))) {
            if (message.contains(SUBJECT_MARKER)) {
                // if (newsFeed.UserGUID.equals(mUserGUID)) {
                // message = message.replace(SUBJECT_MARKER, newsFeed.UserName);
                // } else {
                message = message.replace(
                        SUBJECT_MARKER,
                        getLink(newsFeed.UserName, "UserGUID=" + newsFeed.UserGUID + "&UserProfileLink="
                                + newsFeed.UserProfileURL, "Profile"));
                // }
            }
        } else if ((newsFeed.ActivityType.equals(ACTIVITYTYPE_DefaultAlbumAdded))) {

        }

        return message;
    }

    private String getLink(String disp, String data, String forClass) {
        LogedInUserModel mLogedInUserModel = new LogedInUserModel(mContext);

        String Link = "";
        if (forClass.equals("Profile")) {
            if (data.contains("UserProfileLink=" + mLogedInUserModel.mUserProfileURL)) {
                Link = "<a href='schollyme-dashboard://app.postfeed.wall/params?" + data + "'>" + disp + "</a>";
            } else {
                Link = "<a href='schollyme-userprofile://app.postfeed.wall/params?" + data + "'>" + disp + "</a>";
            }
        } else if (forClass.equals("Added")) {

            Link = "<a href='mediagrid://app.postfeed.wall/params?" + data + "'>" + disp + "</a>";
            // schollyme-mediagrid
        } else if (forClass.equals("Group")) {

            Link = "<a href='group://app.team.wall/params?" + data + "'>" + disp + "</a>";
            // schollyme-mediagrid
        }

        return Link;
    }

    private class ViewHolder {
        private ImageView mWallPostIV, mPlayIV;
        private RelativeLayout mContainerMediaRl;
        private ProgressBar mLoaderUIL;

        //private RelativeLayout like_bottom_rl, comment_bottom_rl, share_bottom_rl;

        private ViewHolder(View view) {

            mContainerMediaRl = (RelativeLayout) view.findViewById(R.id.container_media);
            mWallPostIV = (ImageView) view.findViewById(R.id.wall_post_iv);
            mLoaderUIL = (ProgressBar) view.findViewById(R.id.loading_pb);
            mPlayIV = (ImageView) view.findViewById(R.id.play_iv);

        }
    }

    Calendar cal = Calendar.getInstance();

    public Calendar getCalTime(String dateString) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            cal.setTime(sdf.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TimeZone tz = cal.getTimeZone();
        int Offset = tz.getOffset(cal.getTimeInMillis());
        cal.setTimeInMillis(cal.getTimeInMillis() + Offset);

        return cal;
    }

    public void setLinkText(TextView textView, String text) {
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        Spannable spannable = (Spannable) Html.fromHtml(text);
        for (URLSpan span : spannable.getSpans(0, spannable.length(), URLSpan.class)) {
            spannable.setSpan(new UnderlineSpan() {
                public void updateDrawState(TextPaint tp) {
                    tp.setUnderlineText(false);
                }
            }, spannable.getSpanStart(span), spannable.getSpanEnd(span), 0);
        }
        textView.setText(spannable);
    }
}
