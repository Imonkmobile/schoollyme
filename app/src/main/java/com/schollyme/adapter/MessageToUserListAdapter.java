package com.schollyme.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.FriendModel;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class MessageToUserListAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<FriendModel> mSuggestedFriendsAL;
    private FriendsSuggestionViewHolder mFriendsSuggestionViewHolder;
    String teammateOptionString;

    public MessageToUserListAdapter(Context context) {
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setList(ArrayList<FriendModel> mList) {
        this.mSuggestedFriendsAL = mList;
    }

    @Override
    public int getCount() {
        return null == mSuggestedFriendsAL ? 0 : mSuggestedFriendsAL.size();
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        mFriendsSuggestionViewHolder = new FriendsSuggestionViewHolder();

        if (contentView == null) {
            contentView = mLayoutInflater.inflate(R.layout.message_touserlist_adapter, parent, false);
            mFriendsSuggestionViewHolder.mFriendNameTV = (TextView) contentView.findViewById(R.id.user_name_tv);
            mFriendsSuggestionViewHolder.mFriendIV = (ImageView) contentView.findViewById(R.id.profile_image_iv);
            FontLoader.setRobotoRegularTypeface(mFriendsSuggestionViewHolder.mFriendNameTV);
            contentView.setTag(mFriendsSuggestionViewHolder);
        } else {
            mFriendsSuggestionViewHolder = (FriendsSuggestionViewHolder) contentView.getTag();
        }

        mFriendsSuggestionViewHolder.mFriendNameTV.setTag(position);
        mFriendsSuggestionViewHolder.mFriendNameTV.setText(mSuggestedFriendsAL.get(position).mFirstName
                + " " + mSuggestedFriendsAL.get(position).mLastName);

//		mFriendsSuggestionViewHolder.mFriendNameTV.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				int pos = (Integer) view.getTag();
//				FriendModel mModel = mSuggestedFriendsAL.get(pos);
//				
//				if(new LogedInUserModel(mContext).mUserGUID.equals(mModel.mUserGUID)){
//					mContext.startActivity(UserProfileActivity.getIntent(mContext));
//				}
//				else{
//					mContext.startActivity(FriendProfileActivity.getIntent(mContext,mModel));
//				}
//			}
//		});

        String imageUrl = Config.IMAGE_URL_PROFILE + mSuggestedFriendsAL.get(position).mProfilePicture;
        ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mFriendsSuggestionViewHolder.mFriendIV,
                ImageLoaderUniversal.option_Round_Image);
        return contentView;
    }

    private class FriendsSuggestionViewHolder {
        private TextView mFriendNameTV;
        private ImageView mFriendIV;
    }
}