package com.schollyme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.UserTypeModel;
import com.vinfotech.utility.FontLoader;

import java.util.List;

public class UserTypeAdapter extends BaseAdapter {

    private List<UserTypeModel> mUserTypes;
    private ViewHolder mViewHolder;
    private Context mContext;
    private LogedInUserModel mLogedInUserModel;
    private LayoutInflater mLayoutInflater;
    private int mUserType;

     public UserTypeAdapter(Context context,int UserType) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(mContext);
        this.mLogedInUserModel = new LogedInUserModel(mContext);
        this.mUserType = UserType;
    }

    public void changeUserType(int type){
        this.mUserType = type;
        notifyDataSetChanged();
    }

    public void setList(List<UserTypeModel> list) {
        this.mUserTypes = list;
    }

    @Override
    public int getCount() {
        return mUserTypes.size();
    }

    @Override
    public UserTypeModel getItem(int position) {
        return mUserTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mViewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.user_type_row, parent, false);
            mViewHolder.mTypeTV = (TextView) convertView.findViewById(R.id.user_type_tv);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.mTypeTV.setText(mUserTypes.get(position).mTypeName);
        mViewHolder.mTypeTV.setTag(position);
        if (mUserTypes.get(position).mTypeId.equals(String.valueOf(mUserType))) {
             mViewHolder.mTypeTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_blue_tick, 0);
        } else {
            mViewHolder.mTypeTV.setCompoundDrawables(null,null,null,null);
        }
        FontLoader.setRobotoRegularTypeface(mViewHolder.mTypeTV);
        convertView.setClickable(false);

        mViewHolder.mTypeTV.setTag(position);
  //      mViewHolder.mTypeTV.setOnClickListener(ChangeUserTypeListener);
        return convertView;
    }


    View.OnClickListener ChangeUserTypeListener = new View.OnClickListener() {
        @Override
            public void onClick(View v) {
            int pos = (int)v.getTag();
            mUserType = Integer.parseInt(mUserTypes.get(pos).mTypeId);
            notifyDataSetChanged();
        }
    };

   /* private void updateUserType(){
        if(mLogedInUserModel.mUserType==mUserType){

        }
        else{
            DialogUtil.showOkCancelDialog(mContext, mContext.getResources().getString(R.string.app_name),
                    mContext.getResources().getString(R.string.change_user_type_confirm), new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            LogoutRequest mRequest = new LogoutRequest(mContext);
                            mRequest.LogoutServerRequest();
                            mRequest.setRequestListener(new BaseRequest.RequestListener() {

                                @Override
                                public void onComplete(boolean success, Object data, int totalRecords) {
                                    if (success) {
                                        LogedInUserModel mModel = new LogedInUserModel(mContext);
                                        mModel.removeUserData(mContext);
                                        Intent intent = LoginActivity.getIntent(mContext);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        mContext.startActivity(intent);
                                        ((Activity) mContext).finish();
                                    } else {
                                        DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                                                mContext.getResources().getString(R.string.app_name), null);
                                    }
                                }
                            });
                        }
                    }, new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                        }
                    });

        }

    }*/

    private class ViewHolder{
        private TextView mTypeTV;
    }
}
