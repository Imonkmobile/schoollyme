package com.schollyme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.model.BlogModel;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.UnBlockUserRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.CalStartEndUtil;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

import org.jsoup.Jsoup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class BlogListAdapter extends BaseAdapter {

    private static final String TAG = BlogListAdapter.class.getSimpleName();
    private LayoutInflater mLayoutInflater;
    private List<BlogModel> mBlogList;
    private Context mContext;
    private OnItemClickBlogList mOnItemClickBlogList;


    public BlogListAdapter(Context context, OnItemClickBlogList onItemClickBlogList) {
        this.mContext = context;
        this.mOnItemClickBlogList = onItemClickBlogList;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setList(List<BlogModel> list) {
        this.mBlogList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == mBlogList ? 0 : mBlogList.size();

        // return 20;
    }

    @Override
    public BlogModel getItem(int position) {
        return mBlogList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.blog_list_row, null);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final BlogModel blogModel = getItem(position);
        String mCoverImagePath;
        if (null != blogModel) {

            Calendar calendar = getCalTime(blogModel.CreatedDate);
            if (null != calendar) {
                viewHolder.mTimeTV.setText(CalStartEndUtil.getTimeInformatedform(mContext, calendar));
            } else {
                viewHolder.mTimeTV.setText(blogModel.CreatedDate);
            }

            if (null != blogModel.mConCoverMediaModel && null != blogModel.mConCoverMediaModel.ImageName) {

                mCoverImagePath = Config.getCoverMediaPathBLOG(blogModel.mConCoverMediaModel.ImageName);
                Utility.LogP("PepTalk BlogList","mCoverImagePath: "+mCoverImagePath);
                ImageLoaderUniversal.ImageLoadSquareWithProgressBar(mContext, mCoverImagePath, viewHolder.mCoverIV,
                        ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoaderCoverPB);
                viewHolder.mCoverivRL.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mCoverivRL.setVisibility(View.GONE);
            }

            viewHolder.mTitleTV.setText(blogModel.Title);
            viewHolder.mAutorTV.setText(mContext.getResources().getString(R.string.By) + " " + blogModel.Author);
            viewHolder.mLikeIV.setSelected(blogModel.IsLike == 0 ? false : true);
            viewHolder.mLikesCountTV.setText("" + blogModel.NoOfLikes /*+ " "
                    + (mContext.getResources().getString(1 == blogModel.NoOfLikes ? R.string.Like : R.string.Likes))*/);
            viewHolder.mDescriptionTV.setText(Jsoup.parse(blogModel.Description).text());
            viewHolder.mCommentsCountTV.setText("" + blogModel.NoOfComments /*+ " "
					+ (mContext.getResources().getString(1 == blogModel.NoOfComments ? R.string.Comment : R.string.Comments))*/);

            viewHolder.mCommentsCountTV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickBlogList.onClickItems(R.id.comments_tv, position, blogModel);
                }
            });

            viewHolder.mLikesCountTV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickBlogList.onClickItems(R.id.likes_tv, position, blogModel);
                }
            });

            viewHolder.mLikeIV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayUtil.bounceView(mContext, v);
                    LikeMediaToggleService(position);
                }
            });

            viewHolder.mCommentIV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickBlogList.onClickItems(R.id.comments_iv, position, blogModel);
                }
            });

            viewHolder.mShareIV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
//                    DialogUtil.showYesNoDialogWithTittle(v.getContext(), "",
//                            v.getContext().getString(R.string.Are_share_this_post),
//                            new OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    mOnItemClickBlogList.onClickItems(R.id.shares_iv, position, blogModel);
//                                }
//                            });
                }
            });

            convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickBlogList.onClickItems(5555, position, blogModel);
                }
            });

        }
        return convertView;
    }

    Calendar cal = Calendar.getInstance();

    public Calendar getCalTime(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            cal.setTime(sdf.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TimeZone tz = cal.getTimeZone();
        int Offset = tz.getOffset(cal.getTimeInMillis());
        cal.setTimeInMillis(cal.getTimeInMillis() + Offset);
        return cal;
    }

    public interface OnItemClickBlogList {
        void onClickItems(int ID, int position, BlogModel newsFeed);

    }

    private class ViewHolder {

        private TextView mTimeTV, mTitleTV, mAutorTV, mDescriptionTV, mLikesCountTV, mCommentsCountTV, mSharesCountTV;
        private ImageView mLikeIV, mCommentIV, mShareIV;

        private ImageView mCoverIV;
        public ProgressBar mLoaderCoverPB;
        public RelativeLayout mCoverivRL;

        private ViewHolder(View view) {

            mTimeTV = (TextView) view.findViewById(R.id.time_tv);
            mTitleTV = (TextView) view.findViewById(R.id.title_tv);
            mAutorTV = (TextView) view.findViewById(R.id.author_tv);
            mDescriptionTV = (TextView) view.findViewById(R.id.description_tv);
            mLikesCountTV = (TextView) view.findViewById(R.id.likes_tv);
            mCommentsCountTV = (TextView) view.findViewById(R.id.comments_tv);
            mSharesCountTV = (TextView) view.findViewById(R.id.shares_tv);
            mLikeIV = (ImageView) view.findViewById(R.id.likes_iv);
            mCommentIV = (ImageView) view.findViewById(R.id.comments_iv);
            mShareIV = (ImageView) view.findViewById(R.id.shares_iv);
            mSharesCountTV = (TextView) view.findViewById(R.id.shares_tv);
            mShareIV.setVisibility(View.GONE);
            mSharesCountTV.setVisibility(View.GONE);
            mCoverIV = (ImageView) view.findViewById(R.id.cover_iv);
            mLoaderCoverPB = (ProgressBar) view.findViewById(R.id.loading_cover_pb);
            mCoverivRL = (RelativeLayout) view.findViewById(R.id.cover_image_rl);
            FontLoader.setRobotoMediumTypeface(mTitleTV);
            FontLoader.setRobotoRegularTypeface(mTimeTV, mAutorTV, mDescriptionTV, mLikesCountTV, mCommentsCountTV);
        }
    }

    private ToggleLikeRequest mToggleLikeRequest;

    public void LikeMediaToggleService(final int position) {
        mToggleLikeRequest = new ToggleLikeRequest(mContext);
        mToggleLikeRequest.toggleLikeInServer(mBlogList.get(position).BlogGUID, "BLOG");
        mToggleLikeRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    if (mBlogList.get(position).IsLike == 0) {
                        mBlogList.get(position).NoOfLikes++;
                        mBlogList.get(position).IsLike = 1;
                    } else {
                        mBlogList.get(position).NoOfLikes--;
                        mBlogList.get(position).IsLike = 0;
                    }
                    /*setValues(blogModal);*/
                    notifyDataSetChanged();
                }
            }
        });

    }

    /*public void setValues(BlogModel blogModel){

    }*/
}
