package com.schollyme.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.model.StateModel;
import com.vinfotech.utility.FontLoader;

public class StateListAdapter extends BaseAdapter {

    private StateViewHolder mStateViewHolder;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<StateModel> mStatesAL;
    private List<StateModel> mFilteredState = new ArrayList<StateModel>();

    public StateListAdapter(Context context) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setList(List<StateModel> mAList) {
        this.mStatesAL = mAList;
        setFilter(null);

    }

    public ArrayList<StateModel> getSelections() {
        ArrayList<StateModel> selections = new ArrayList<StateModel>();
        if (null != mStatesAL) {
            for (StateModel stateModel : mStatesAL) {
                if (stateModel.isSelected) {
                    selections.add(stateModel);
                }
            }
        }
        return selections;
    }

    @Override
    public int getCount() {
        return null == mFilteredState ? 0 : mFilteredState.size();
    }

    @Override
    public StateModel getItem(int position) {
        return mFilteredState.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mStateViewHolder = new StateViewHolder();
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item_view, parent, false);
            mStateViewHolder.mTitleTV = (TextView) convertView.findViewById(R.id.title_tv);
            mStateViewHolder.mSelectIB = (ImageButton) convertView.findViewById(R.id.select_ib);
            convertView.setTag(mStateViewHolder);
        } else {
            mStateViewHolder = (StateViewHolder) convertView.getTag();
        }
        mStateViewHolder.mTitleTV.setText(mFilteredState.get(position).mStateName);
        mStateViewHolder.mTitleTV.setTag(position);
        mStateViewHolder.mSelectIB.setImageResource(R.drawable.icon_blue_tick);
        if (mFilteredState.get(position).isSelected) {
            mStateViewHolder.mSelectIB.setVisibility(View.VISIBLE);
        } else {
            mStateViewHolder.mSelectIB.setVisibility(View.GONE);
        }
        FontLoader.setRobotoRegularTypeface(mStateViewHolder.mTitleTV);
        convertView.setClickable(false);

        return convertView;
    }

    private class StateViewHolder {
        public TextView mTitleTV;
        public ImageButton mSelectIB;
    }

    public void setFilter(String text) {
        mFilteredState.clear();

        if (null != mStatesAL) {
            if (TextUtils.isEmpty(text)) {
                mFilteredState.addAll(mStatesAL);
            } else {
                for (StateModel sportsModel : mStatesAL) {
                    if (sportsModel.mStateName.toLowerCase().startsWith(text.toLowerCase())
                            || sportsModel.mStateName.toLowerCase().contains((" " + text.toLowerCase()))) {
                        mFilteredState.add(sportsModel);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
}