package com.schollyme.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.media.MediaViewerActivity;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.vinfotech.request.AlbumCreateRequest;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class MediaAdapter extends BaseAdapter {
	private static final String TAG = MediaAdapter.class.getSimpleName();
	private LayoutInflater mLayoutInflater;
	private List<AlbumMedia> mMedias;
	private int mImgWidth = 0;
	private Context mContext;
	private boolean mMultiSelMode = false;
	private ModeListener mModeListener;
	private Album mAlbum;
	private boolean mCreatorAdded = true;

	public MediaAdapter(Context context, Album album, boolean creatorAdded) {
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mContext = context;
		mAlbum = album;
		this.mCreatorAdded = creatorAdded;

		int[] screenDimen = new int[2];
		DisplayUtil.probeScreenSize(context, screenDimen);
		float spaceBwItems = context.getResources().getDimension(R.dimen.space_tinny2) * 2;
		mImgWidth = (int) ((screenDimen[0] - spaceBwItems) / 3f);
		if (Config.DEBUG) {
			Log.d(TAG, "screenDimen[0]=" + screenDimen[0] + ", screenDimen[1]=" + screenDimen[1] + ", spaceBwItems=" + spaceBwItems
					+ ", mImgWidth=" + mImgWidth);
		}
	}

	public void setList(List<AlbumMedia> list) {
		this.mMedias = list;
		setMode(false);
	}

	public boolean hasCreator() {
		return mCreatorAdded;
	}

	public void setAlbum(Album album) {
		mAlbum = album;
		notifyDataSetChanged();
	}

	public List<AlbumMedia> getList() {
		return this.mMedias;
	}

	public void clearList() {
		if (null != mMedias) {
			mMedias.clear();
		}
		notifyDataSetChanged();
	}

	public void setModeListener(ModeListener listener) {
		this.mModeListener = listener;
	}

	public void setMode(boolean multiSel) {
		this.mMultiSelMode = multiSel;
		if (!mMultiSelMode && null != mMedias) {
			for (AlbumMedia albumMedia : mMedias) {
				albumMedia.isSelected = false;
			}
		}
		if (null != mModeListener) {
			mModeListener.onModeChange(mMultiSelMode);
		}
		notifyDataSetChanged();
	}

	public boolean isMultiSelectionMode() {
		return mMultiSelMode;
	}

	public List<AlbumMedia> getSelections() {
		List<AlbumMedia> list = new ArrayList<AlbumMedia>();
		for (AlbumMedia albumMedia : mMedias) {
			if (albumMedia.isSelected) {
				list.add(albumMedia);
			}
		}
		return list;
	}

	@Override
	public int getCount() {
		return null == mMedias ? 0 : mMedias.size();
	}

	@Override
	public AlbumMedia getItem(int position) {
		return mMedias.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.media_row, null);
			convertView.setLayoutParams(new AbsListView.LayoutParams(mImgWidth, mImgWidth));
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final AlbumMedia media = getItem(position);
		viewHolder.multiSelectIv.setVisibility(mMultiSelMode && media.isSelected ? View.VISIBLE : View.GONE);
		viewHolder.privacyIv.setVisibility(/*
											 * mAlbum.Visibility !=
											 * AlbumCreateRequest
											 * .VISIBILITY_PUBLIC && position >
											 * 0 ? View.VISIBLE :
											 */View.GONE);

		if (position == 0 && mCreatorAdded) {
			viewHolder.mediaIv.setImageResource(AlbumCreateRequest.ALBUMTYPE_PHOTO.equalsIgnoreCase(media.MediaType)
					|| AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(media.MediaType) ? R.drawable.ic_default_add_album
					: R.drawable.ic_default_add_video);
			viewHolder.vidInfoRl.setVisibility(View.INVISIBLE);
		} else {
			if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(media.MediaType)) {

				ImageLoaderUniversal.ImageLoadSquare(mContext, Config.getCoverPath(mAlbum, media.ImageName, false), viewHolder.mediaIv,
						ImageLoaderUniversal.option_normal_Image_Thumbnail);
				viewHolder.vidInfoRl.setVisibility(View.INVISIBLE);
			} else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(media.MediaType)) {
				ImageLoaderUniversal.ImageLoadSquare(mContext, Config.getCoverPath(mAlbum, Config.getVideoToJPG(media.ImageName), true),
						viewHolder.mediaIv, ImageLoaderUniversal.option_normal_Image_Thumbnail_Video);
				viewHolder.vidInfoRl.setVisibility(View.VISIBLE);
				if (media.isConverted()) {
					viewHolder.durationTv.setText(AlbumMedia.getVideoDuration(media.VideoLength));
				} else {
					viewHolder.durationTv.setText("--:--");
				}
			} else if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(media.MediaType)) {
				ImageLoaderUniversal.ImageLoadSquare(mContext, MediaViewerActivity.getYoutubeVideoThumb(media.ImageName),
						viewHolder.mediaIv, ImageLoaderUniversal.option_normal_Image_Thumbnail_Video);
				viewHolder.vidInfoRl.setVisibility(View.VISIBLE);
				viewHolder.durationTv.setText(AlbumMedia.getVideoDuration(media.VideoLength));
			} else {
				viewHolder.vidInfoRl.setVisibility(View.INVISIBLE);
			}
			// viewHolder.vidInfoRl.setVisibility(View.INVISIBLE);
		}

		return convertView;
	}

	public class ViewHolder {
		public ImageView mediaIv, privacyIv;
		public ImageView multiSelectIv;
		public RelativeLayout vidInfoRl;
		private TextView durationTv;

		public ViewHolder(View view) {
			mediaIv = (ImageView) view.findViewById(R.id.media_iv);
			privacyIv = (ImageView) view.findViewById(R.id.privacy_iv);
			multiSelectIv = (ImageView) view.findViewById(R.id.multi_select_iv);
			vidInfoRl = (RelativeLayout) view.findViewById(R.id.vid_info_rl);
			durationTv = (TextView) view.findViewById(R.id.duration_tv);
			FontLoader.setRobotoRegularTypeface(durationTv);
		}
	}

	public interface ModeListener {
		void onModeChange(boolean isMultiSelect);
	}
}
