package com.schollyme.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.friends.FriendProfileActivity;
import com.schollyme.friends.FriendRequestFragment.FriendCountListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.inbox.ChatHistoryActivity;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.FriendAddRequest;
import com.vinfotech.request.FriendCancelRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class FriendRequestAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private ArrayList<FriendModel> mSuggestedFriendsAL,mFilteredFriendsAL = new ArrayList<FriendModel>();
	private FriendsSuggestionViewHolder mFriendsSuggestionViewHolder;
	private String teammateOptionString;
	private ErrorLayout mErrorLayout;
	private FriendCountListener mFriendCountListener;

	public FriendRequestAdapter(Context context, ErrorLayout errorLayout, FriendCountListener listener) {
		this.mContext = context;
		mLayoutInflater = LayoutInflater.from(mContext);
		this.mErrorLayout = errorLayout;
		mFriendCountListener = listener;
	}

	public void setList(ArrayList<FriendModel> mList) {
		this.mSuggestedFriendsAL = mList;
	//	notifyDataSetChanged();
		setFilter(null);
	}

	@Override
	public int getCount() {
		return null == mFilteredFriendsAL ? 0 : mFilteredFriendsAL.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(final int position, View contentView, ViewGroup parent) {

		mFriendsSuggestionViewHolder = new FriendsSuggestionViewHolder();
		if (contentView == null) {
			contentView = mLayoutInflater.inflate(R.layout.friend_request_item_fragment, parent, false);
			mFriendsSuggestionViewHolder.mFriendNameTV = (TextView) contentView.findViewById(R.id.user_name_tv);
			mFriendsSuggestionViewHolder.mFriendTypeTV = (TextView) contentView.findViewById(R.id.user_status_tv);
			mFriendsSuggestionViewHolder.mFriendAcceptBtn = (Button) contentView.findViewById(R.id.accept_button);
			mFriendsSuggestionViewHolder.mFriendDenyBtn = (Button) contentView.findViewById(R.id.deny_button);
			mFriendsSuggestionViewHolder.mFriendIV = (ImageView) contentView.findViewById(R.id.profile_image_iv);
			mFriendsSuggestionViewHolder.mOptionsIB = (ImageButton) contentView.findViewById(R.id.options_ib);
			FontLoader.setRobotoMediumTypeface(mFriendsSuggestionViewHolder.mFriendNameTV,
					mFriendsSuggestionViewHolder.mFriendAcceptBtn,
					mFriendsSuggestionViewHolder.mFriendDenyBtn);
			FontLoader.setRobotoRegularTypeface(mFriendsSuggestionViewHolder.mFriendTypeTV);
			contentView.setTag(mFriendsSuggestionViewHolder);
		} else {
			mFriendsSuggestionViewHolder = (FriendsSuggestionViewHolder) contentView.getTag();
		}
		String mUserType = mFilteredFriendsAL.get(position).mUserTypeID;
		if (mUserType.equals("1")) {
			mFriendsSuggestionViewHolder.mFriendTypeTV.setText(String.valueOf(Config.UserType.Coach));
		} else if (mUserType.equals("2")) {
			mFriendsSuggestionViewHolder.mFriendTypeTV.setText(String.valueOf(Config.UserType.Athlete));
		} else if (mUserType.equals("3")) {
			mFriendsSuggestionViewHolder.mFriendTypeTV.setText(String.valueOf(Config.UserType.Fan));
		}
		
		mFriendsSuggestionViewHolder.mFriendNameTV.setText(mFilteredFriendsAL.get(position).mFirstName + " "
				+ mFilteredFriendsAL.get(position).mLastName);
		mFriendsSuggestionViewHolder.mFriendNameTV.setTag(position);
		mFriendsSuggestionViewHolder.mFriendTypeTV.setTag(position);
		mFriendsSuggestionViewHolder.mFriendIV.setTag(position);
		mFriendsSuggestionViewHolder.mFriendNameTV.setOnClickListener(UserProfileLineListener);
		mFriendsSuggestionViewHolder.mFriendTypeTV.setOnClickListener(UserProfileLineListener);
		mFriendsSuggestionViewHolder.mFriendIV.setOnClickListener(UserProfileLineListener);
		String imageUrl = Config.IMAGE_URL_PROFILE + mFilteredFriendsAL.get(position).mProfilePicture;
		ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mFriendsSuggestionViewHolder.mFriendIV,
				ImageLoaderUniversal.option_Round_Image);
		
		if (mFilteredFriendsAL.get(position).mFriendStatus.equals("4")) {
			mFriendsSuggestionViewHolder.mFriendAcceptBtn.setVisibility(View.VISIBLE);
			mFriendsSuggestionViewHolder.mFriendDenyBtn.setVisibility(View.VISIBLE);
			mFriendsSuggestionViewHolder.mOptionsIB.setVisibility(View.GONE);
		} else if (mFilteredFriendsAL.get(position).mFriendStatus.equals("3")) {
			mFriendsSuggestionViewHolder.mFriendAcceptBtn.setVisibility(View.VISIBLE);
			mFriendsSuggestionViewHolder.mFriendDenyBtn.setVisibility(View.VISIBLE);
			mFriendsSuggestionViewHolder.mOptionsIB.setVisibility(View.GONE);
		} else if (mFilteredFriendsAL.get(position).mFriendStatus.equals("1")) {
			mFriendsSuggestionViewHolder.mOptionsIB.setVisibility(View.VISIBLE);
			mFriendsSuggestionViewHolder.mFriendAcceptBtn.setVisibility(View.GONE);
			mFriendsSuggestionViewHolder.mFriendDenyBtn.setVisibility(View.GONE);
			mFriendsSuggestionViewHolder.mOptionsIB.setTag(position);

			mFriendsSuggestionViewHolder.mOptionsIB.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					final int pos = (Integer) v.getTag();
					if (mFilteredFriendsAL.get(pos).mUserIsTeammate.equals("0")) {
						teammateOptionString = mContext.getString(R.string.mark_friend_as_teammate);
					} else {
						teammateOptionString = mContext.getString(R.string.remove_teammate);
					}
					DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
						@Override
						public void onItemClick(int position, String item) {
							if (position == 0) {
								String mGUID = mFilteredFriendsAL.get(pos).mUserGUID;
								mFilteredFriendsAL.remove(pos);
								for(int i=0;i<mSuggestedFriendsAL.size();i++){
									if(mSuggestedFriendsAL.get(i).mUserGUID.equals(mGUID)){
										mSuggestedFriendsAL.remove(i);
										break;
									}
								}
								
								notifyDataSetChanged();
								denyFriendRequest(mGUID);
							} else if (position == 1) {
								String mFriendName = mFilteredFriendsAL.get(pos).mFirstName+" "+mFilteredFriendsAL.get(pos).mLastName;
								if (item.equals(mContext.getString(R.string.mark_friend_as_teammate))) {
									mFilteredFriendsAL.get(pos).mUserIsTeammate = "1";
									String message = mFriendName+" "+mContext.getResources().getString(R.string.marked_friend_as_teammate);
									acceptFriendRequest(mFilteredFriendsAL.get(pos).mUserGUID,message, String.valueOf("1"));
								} else if (item.equals(mContext.getString(R.string.remove_teammate))) {
									mFilteredFriendsAL.get(pos).mUserIsTeammate = "0";
									String message = mFriendName+" "+mContext.getResources().getString(R.string.remove_from_teammate);
									acceptFriendRequest(mFilteredFriendsAL.get(pos).mUserGUID,message, String.valueOf("0"));
									// mSuggestedFriendsAL.remove(pos);
									notifyDataSetChanged();
								}
							} else if (position == 2) {
								String userName = mFilteredFriendsAL.get(pos).mFirstName + " " + mFilteredFriendsAL.get(pos).mLastName;
								mContext.startActivity(ChatHistoryActivity.getIntent(mContext,userName, mFilteredFriendsAL.get(pos),"0"));
							}
						}

						@Override
						public void onCancel() {

						}
					}, mContext.getString(R.string.unfriend), teammateOptionString, mContext.getString(R.string.message));
				}
			});
		} else {
			mFriendsSuggestionViewHolder.mFriendAcceptBtn.setVisibility(View.GONE);
			mFriendsSuggestionViewHolder.mFriendDenyBtn.setVisibility(View.GONE);
			mFriendsSuggestionViewHolder.mOptionsIB.setVisibility(View.GONE);
		}
		mFriendsSuggestionViewHolder.mFriendAcceptBtn.setTag(position);
		mFriendsSuggestionViewHolder.mFriendAcceptBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				final int pos = (Integer) view.getTag();
				final FriendModel mModel = mFilteredFriendsAL.get(pos);
				if(mErrorLayout!=null)
				
				DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {
					@Override
					public void onItemClick(int position, String item) {
						mFilteredFriendsAL.remove(pos);

						if(null != mFriendCountListener){
							mFriendCountListener.onCountChange(mFilteredFriendsAL.size());
						}
						notifyDataSetChanged();
						String mFriendName = mModel.mFirstName+" "+mModel.mLastName;
						
						String customeMessage = position==0?mContext.getResources().getString(R.string.marked_as_friend):mContext.getResources().getString(R.string.marked_as_teammate);
						String message = mFriendName+" "+customeMessage;
						acceptFriendRequest(mModel.mUserGUID,message, String.valueOf(position));
					}

					@Override
					public void onCancel() {

					}
				}, mContext.getString(R.string.add_as_friend), mContext.getString(R.string.add_as_teammate));
			}
		});

		mFriendsSuggestionViewHolder.mFriendDenyBtn.setTag(position);
		mFriendsSuggestionViewHolder.mFriendDenyBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				int pos = (Integer) view.getTag();
				FriendModel mModel = mFilteredFriendsAL.get(pos);
				mFilteredFriendsAL.remove(pos);
				notifyDataSetChanged();
				denyFriendRequest(mModel.mUserGUID);
				
				if(null != mFriendCountListener){
					mFriendCountListener.onCountChange(mFilteredFriendsAL.size());
				}
			}
		});
		return contentView;
	}
	
	OnClickListener UserProfileLineListener = new OnClickListener() {

		
		@Override
		public void onClick(View view) {
			int pos = (Integer) view.getTag();
			FriendModel mModel = mFilteredFriendsAL.get(pos);

			if (new LogedInUserModel(mContext).mUserGUID.equals(mModel.mUserGUID)) {
				//mContext.startActivity(DashboardActivity.getIntent(mContext,6));
			} else {
				mContext.startActivity(FriendProfileActivity.getIntent(mContext,mModel.mUserGUID,
						mModel.mProfileLink, "FriendRequestAdapter"));
			}
		}
	};

	private class FriendsSuggestionViewHolder {
		private TextView mFriendNameTV, mFriendTypeTV;
		private ImageView mFriendIV;
		private ImageButton mOptionsIB;
		private Button mFriendAcceptBtn, mFriendDenyBtn;
	}

	private void acceptFriendRequest(String friendGUID,final String message, String teammate) {
		
		FriendAddRequest mRequest = new FriendAddRequest(mContext);
		mRequest.AddFriendServerRequest(friendGUID, teammate);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					mErrorLayout.showError(message, true, MsgType.Success);
				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
				}
			}
		});
	}

	private void denyFriendRequest(String friendGUID) {
		FriendCancelRequest mRequest = new FriendCancelRequest(mContext);
		mRequest.CancelFriendServerRequest(friendGUID);
		mRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					notifyDataSetChanged();
				} else {
					DialogUtil.showOkDialog(mContext, data.toString(), mContext.getResources().getString(R.string.app_name));
				}
			}
		});
	}
	
	public void setFilter(String text) {
		mFilteredFriendsAL.clear();

		if (null != mSuggestedFriendsAL) {
			if (TextUtils.isEmpty(text)) {
				mFilteredFriendsAL.addAll(mSuggestedFriendsAL);
			} else {
				for (FriendModel friendModel : mSuggestedFriendsAL) {
					String mFirstName = friendModel.mFirstName.toLowerCase();//+" "+mFriendsDataAL.get(k).mLastName.toLowerCase();
					String mLastName = friendModel.mLastName.toLowerCase();
					String mFullName = mFirstName+" "+mLastName;
					if(mFirstName.startsWith(text.toString().toLowerCase()) || mLastName.startsWith(text.toString().toLowerCase()) || mFullName.startsWith(text.toString().toLowerCase())){
						mFilteredFriendsAL.add(friendModel);
					}
				}
			}
		}
		notifyDataSetChanged();
	}
}