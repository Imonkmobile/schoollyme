package com.schollyme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.model.FilterBy;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;

public class FilterByCategoryAdapter extends BaseAdapter {

    private String[] list_string;
    private Context mContext;
    private LayoutInflater mInflater;
    private int unreadCount = 0;
    private FilterBy mFilterBy;
    private ArrayList<Boolean> isDefault = new ArrayList<Boolean>();
    private int paidType = 0;

    public FilterByCategoryAdapter(Context c, String[] list_string_, FilterBy filterBy, int paidtype) {
        this.mContext = c;
        this.mFilterBy = filterBy;
        this.paidType = paidtype;
        this.list_string = list_string_;

    }

    public void NotifyDataChange() {

        notifyDataSetChanged();

    }

    public void setFilteragain(FilterBy filterBy) {
        this.mFilterBy = filterBy;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list_string.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.filter_by_category_row, null);
        View mDotView = (View) convertView.findViewById(R.id.filter_view);
        ImageView rightIcon = (ImageView) convertView.findViewById(R.id.right_icon);
        rightIcon.setImageResource(R.drawable.ic_next);
        switch (position) {
            case 0:
                if (null != mFilterBy.getSportsModels() && mFilterBy.getSportsModels().size() > 0) {
                    mDotView.setVisibility(View.VISIBLE);
                } else {
                    mDotView.setVisibility(View.INVISIBLE);
                }
                break;
            case 1:
                if (null != mFilterBy.getStateModels() && mFilterBy.getStateModels().size() > 0) {
                    mDotView.setVisibility(View.VISIBLE);
                } else {
                    mDotView.setVisibility(View.INVISIBLE);
                }
                break;
            case 2:
                if (mFilterBy.getGender().equals("")) {
                    mDotView.setVisibility(View.INVISIBLE);
                } else {
                    mDotView.setVisibility(View.VISIBLE);
                }

                break;
            case 3:
                if (mFilterBy.getUserTypeId().equals("")) {
                    mDotView.setVisibility(View.INVISIBLE);
                } else {
                    mDotView.setVisibility(View.VISIBLE);
                }
                break;
            case 4:
                if (paidType == 0) {

                    if (null != mFilterBy.getmFavTeamModals() && mFilterBy.getmFavTeamModals().size() > 0) {
                        mDotView.setVisibility(View.VISIBLE);
                    } else {
                        mDotView.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if (mFilterBy.getGPA().equals("")) {
                        mDotView.setVisibility(View.INVISIBLE);
                    } else {
                        mDotView.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case 5:
                if (mFilterBy.getSATScore().equals("")) {
                    mDotView.setVisibility(View.INVISIBLE);
                } else {
                    mDotView.setVisibility(View.VISIBLE);
                }
                break;
            case 6:
                if (mFilterBy.getRecruitingClass().equals("")) {
                    mDotView.setVisibility(View.INVISIBLE);
                } else {
                    mDotView.setVisibility(View.VISIBLE);
                }

                break;
            case 7:
                if (mFilterBy.getVerified() == 0) {
                    rightIcon.setImageResource(0);
                    mDotView.setVisibility(View.INVISIBLE);
                } else {
                    rightIcon.setImageResource(R.drawable.ic_check);
                    mDotView.setVisibility(View.VISIBLE);
                }

                break;
            default:
                break;
        }

        TextView categoty_tv = (TextView) convertView.findViewById(R.id.categoty_tv);
        categoty_tv.setText(list_string[position]);

        /*if (((FilterListActivity) mContext).isListSelected(position)) {

            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.verify_text_color));
            categoty_tv.setTextColor(mContext.getResources().getColor(R.color.white));
        } else {

            convertView.setBackgroundColor(0);
            categoty_tv.setTextColor(mContext.getResources().getColor(R.color.app_text_color));
        }*/

        FontLoader.setRobotoMediumTypeface(categoty_tv);

        return convertView;
    }
}
