package com.schollyme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.model.LeftMenuModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;

public class LeftNavigatorAdapter extends BaseAdapter {

    private ArrayList<LeftMenuModel> mSliderOptionsAL;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private NavigaotViewHolder mHolder;
    private int[] SLIDER_NORMAL_RESOURCES;
    private LogedInUserModel mLogedInUserModel;

    public LeftNavigatorAdapter(int mNormalRes[], Context context) {

        this.mContext = context;
        this.SLIDER_NORMAL_RESOURCES = mNormalRes;
        this.mLayoutInflater = LayoutInflater.from(mContext);
        this.mLogedInUserModel = new LogedInUserModel();

    }

    public void setList(ArrayList<LeftMenuModel> list) {
        this.mSliderOptionsAL = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mSliderOptionsAL.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mHolder = new NavigaotViewHolder();

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.navigation_drawer_item, parent, false);
            mHolder.mTitleTV = (TextView) convertView.findViewById(R.id.title_tv);
            mHolder.mDeviderView = convertView.findViewById(R.id.devider_view);
            mHolder.notificationCountTV = (TextView) convertView.findViewById(R.id.count_tv);
            mHolder.mInvisibleView = convertView.findViewById(R.id.devider_invisble_view);
            FontLoader.setRobotoMediumTypeface(mHolder.mTitleTV, mHolder.notificationCountTV);

            convertView.setTag(mHolder);
        } else {
            mHolder = (NavigaotViewHolder) convertView.getTag();
        }

        if ((position == 3 && mSliderOptionsAL.get(position).mOptionMenuCount > 0)
                || ((mLogedInUserModel.mUserType == 2 && position == 8)
                || (mLogedInUserModel.mUserType == 1 && position == 5)
                || (mLogedInUserModel.mUserType == 3 && position == 5)
                && mSliderOptionsAL.get(position).mOptionMenuCount > 0)) {

            mHolder.notificationCountTV.setVisibility(View.VISIBLE);
            mHolder.notificationCountTV.setText(String.valueOf(mSliderOptionsAL.get(position).mOptionMenuCount));
        } else {
            mHolder.notificationCountTV.setVisibility(View.GONE);
        }

        if (position == 0) { //|| position == 7 || position == 11 || position == 13
            mHolder.mInvisibleView.setVisibility(View.VISIBLE);
        } else {
            mHolder.mInvisibleView.setVisibility(View.GONE);
        }

        if (position == 6 || position == 10 || position == 13) {
            mHolder.mDeviderView.setVisibility(View.VISIBLE);
        } else {
            mHolder.mDeviderView.setVisibility(View.GONE);
        }

        mHolder.mTitleTV.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources()
                .getDrawable(SLIDER_NORMAL_RESOURCES[position]), null, null, null);
        mHolder.mTitleTV.setText(mSliderOptionsAL.get(position).mOptionMenuName);
        convertView.setClickable(false);
        return convertView;
    }

    private class NavigaotViewHolder {
        public TextView mTitleTV, notificationCountTV;
        public View mDeviderView, mInvisibleView;
    }
}
