package com.schollyme.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.model.SportsModel;
import com.vinfotech.utility.FontLoader;

public class SportsListAdapter extends BaseAdapter {
	private StateViewHolder mStateViewHolder;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<SportsModel> mFilteredSports = new ArrayList<SportsModel>(), mSports;

	public SportsListAdapter(Context context) {
		this.mContext = context;
		this.mLayoutInflater = LayoutInflater.from(mContext);
	}

	public void setList(List<SportsModel> list) {
		this.mSports = list;
		setFilter(null);
	}

	public ArrayList<SportsModel> getSelections() {
		ArrayList<SportsModel> selections = new ArrayList<SportsModel>();
		if (null != mFilteredSports) {
			for (SportsModel sportsModel : mFilteredSports) {
				if (sportsModel.isSelected) {
					selections.add(sportsModel);
				}
			}
		}
		return selections;
	}

	public void select(boolean all) {
		for (SportsModel sportsModel : mFilteredSports) {
			sportsModel.isSelected = all;
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mFilteredSports.size();
	}

	@Override
	public SportsModel getItem(int position) {
		return mFilteredSports.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		mStateViewHolder = new StateViewHolder();
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.list_item_view, parent, false);
			mStateViewHolder.mTitleTV = (TextView) convertView.findViewById(R.id.title_tv);
			mStateViewHolder.mSelectIB = (ImageButton) convertView.findViewById(R.id.select_ib);
			convertView.setTag(mStateViewHolder);
		} else {
			mStateViewHolder = (StateViewHolder) convertView.getTag();
		}
		mStateViewHolder.mTitleTV.setText(mFilteredSports.get(position).mSportsName);
		mStateViewHolder.mTitleTV.setTag(position);
		mStateViewHolder.mSelectIB.setImageResource(R.drawable.icon_blue_tick);
		if (mFilteredSports.get(position).isSelected) {
			mStateViewHolder.mSelectIB.setVisibility(View.VISIBLE);
		} else {
			mStateViewHolder.mSelectIB.setVisibility(View.GONE);
		}
		FontLoader.setRobotoRegularTypeface(mStateViewHolder.mTitleTV);
		convertView.setClickable(false);

		return convertView;
	}

	private class StateViewHolder {
		public TextView mTitleTV;
		public ImageButton mSelectIB;
	}

	public void setFilter(String text) {
		mFilteredSports.clear();

		if (null != mSports) {
			if (TextUtils.isEmpty(text)) {
				mFilteredSports.addAll(mSports);
			} else {
				for (SportsModel sportsModel : mSports) {
					if (sportsModel.mSportsName.toLowerCase().startsWith(text.toLowerCase())
							|| sportsModel.mSportsName.toLowerCase().contains((" "+text.toLowerCase()))) {
						mFilteredSports.add(sportsModel);
					}
				}
			}
		}
		notifyDataSetChanged();
	}
}