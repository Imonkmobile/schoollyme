package com.schollyme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.activity.BlockedUserActivity;
import com.schollyme.model.FriendModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.UnBlockUserRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

import java.util.ArrayList;

/**
 * Created by ravib on 1/19/2016.
 */
public class BlockedUserAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<FriendModel> mUsersAL;
    private ViewHolder mViewHolder;

    public BlockedUserAdapter(Context context) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setList(ArrayList<FriendModel> list) {
        mUsersAL = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == mUsersAL ? 0 : mUsersAL.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        mViewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.blocked_user_item, parent, false);
            mViewHolder.mUnBlockButton = (ImageView) convertView.findViewById(R.id.unblock_button);
            mViewHolder.mUserIV = (ImageView) convertView.findViewById(R.id.profile_image_iv);
            mViewHolder.mUserNameTV = (TextView) convertView.findViewById(R.id.user_name_tv);

            FontLoader.setRobotoRegularTypeface(mViewHolder.mUnBlockButton, mViewHolder.mUserNameTV);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.mUserNameTV.setText(mUsersAL.get(position).mFirstName
                + " " + mUsersAL.get(position).mFirstName);

        String imageUrl = Config.IMAGE_URL_PROFILE + mUsersAL.get(position).mProfilePicture;
        ImageLoaderUniversal.ImageLoadRound(mContext, imageUrl, mViewHolder.mUserIV,
                ImageLoaderUniversal.option_Round_Image);

        mViewHolder.mUnBlockButton.setTag(position);
        mViewHolder.mUnBlockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int pos = (int) v.getTag();

                DialogUtil.showOkCancelDialog(mContext, R.string.unblock_caps, R.string.cancel_caps, "",
                        mContext.getString(R.string.unblock_confirm_msg), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                UnBlockUserRequest request = new UnBlockUserRequest(mContext);
                                request.setRequestListener(new BaseRequest.RequestListener() {
                                    @Override
                                    public void onComplete(boolean success, Object data, int totalRecords) {
                                        mUsersAL.remove(pos);
                                        ((BlockedUserActivity) mContext).getBlockedUsers();
                                    }
                                });
                                String friendId = mUsersAL.get(pos).mUserGUID;
                                String userId = new LogedInUserModel(mContext).mUserGUID;
                                String moduleId = "3";
                                request.UnBlockUserServerRequest(friendId, moduleId, userId);
                            }
                        }, null);
            }
        });


        return convertView;
    }

    private class ViewHolder {
        private TextView mUserNameTV;
        private ImageView mUserIV;
        private ImageView mUnBlockButton;
    }
}
