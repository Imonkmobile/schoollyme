package com.schollyme.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.media.MediaViewerActivity;
import com.schollyme.model.Album;
import com.vinfotech.request.AlbumCreateRequest;
import com.vinfotech.request.AlbumListRequest;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;

public class AlbumAdapter extends BaseAdapter {
	private static final String TAG = AlbumAdapter.class.getSimpleName();
	private LayoutInflater mLayoutInflater;
	private List<Album> mAlbums;
	private int mImgWidth = 0, mImgHeight = 0;
	private Context mContext;
	private boolean mPhotos;
	private boolean mCreatorAdded = true;

	public AlbumAdapter(Context context, boolean photos, boolean creatorAdded) {
		mContext = context;
		mPhotos = photos;
		this.mCreatorAdded = creatorAdded;

		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		int[] screenDimen = new int[2];
		DisplayUtil.probeScreenSize(context, screenDimen);
		float spaceBwItems = context.getResources().getDimension(R.dimen.space_large15) * 2
				+ context.getResources().getDimension(R.dimen.space_mid10) * 2;
		mImgWidth = (int) ((screenDimen[0] - spaceBwItems) / 3f);
		mImgHeight = mImgWidth + (int) context.getResources().getDimension(R.dimen.space_tinny4);
		if (Config.DEBUG) {
			Log.d(TAG, "screenDimen[0]=" + screenDimen[0] + ", screenDimen[1]=" + screenDimen[1] + ", spaceBwItems=" + spaceBwItems
					+ ", mImgWidth=" + mImgWidth + ", mImgHeight=" + mImgHeight);
		}
	}

	public void setList(List<Album> list) {
		this.mAlbums = list;
		notifyDataSetChanged();
	}

	public boolean hasCreator() {
		return mCreatorAdded;
	}

	@Override
	public int getCount() {
		return null == mAlbums ? 0 : mAlbums.size();
	}

	@Override
	public Album getItem(int position) {
		return mAlbums.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.album_item, null);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final Album album = getItem(position);

		viewHolder.setAlbum(position == 0 && mCreatorAdded, album);

		return convertView;
	}

	private class ViewHolder {
		private RelativeLayout coverRl;
		private ImageView coverIv, privacyIv, videoIv;
		private TextView titleTv, subTitleTv;

		private ViewHolder(View view) {
			coverRl = (RelativeLayout) view.findViewById(R.id.cover_rl);
			coverIv = (ImageView) view.findViewById(R.id.cover_iv);
			privacyIv = (ImageView) view.findViewById(R.id.privacy_iv);
			videoIv = (ImageView) view.findViewById(R.id.video_iv);
			titleTv = (TextView) view.findViewById(R.id.title_tv);
			subTitleTv = (TextView) view.findViewById(R.id.sub_title_tv);
			FontLoader.setRobotoRegularTypeface(titleTv, subTitleTv);

			ViewGroup.LayoutParams params = coverRl.getLayoutParams();
			params.width = mImgWidth;
			params.height = mImgHeight;
		}

		private void setAlbum(boolean creator, Album album) {
			if (creator) {
				coverIv.setImageResource(mPhotos ? R.drawable.ic_deault_media : R.drawable.ic_default_video);
				titleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
				titleTv.setText(R.string.create_album_label);
				subTitleTv.setVisibility(View.INVISIBLE);
			} else {
				if (AlbumListRequest.ALBUM_TYPE_PHOTO.equalsIgnoreCase(album.AlbumType)) {

					ImageLoaderUniversal.ImageLoadSquare(mContext, Config.getCoverPath(album, album.CoverMedia, false), coverIv,
							ImageLoaderUniversal.ROUNDED_THUMB_OPTIONS.build());
				} else {
					if (MediaViewerActivity.isYouTubeVideo(album.CoverMedia)) {
						ImageLoaderUniversal.ImageLoadSquare(mContext, MediaViewerActivity.getYoutubeVideoThumb(album.CoverMedia), coverIv,
								ImageLoaderUniversal.ROUNDED_THUMB_OPTIONS.build());
					} else {

						ImageLoaderUniversal.ImageLoadSquare(mContext,
								Config.getCoverPath(album, Config.getVideoToJPG(album.CoverMedia), true), coverIv,
								ImageLoaderUniversal.ROUNDED_THUMB_OPTIONS.build());
					}
				}
				titleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
				titleTv.setText(album.AlbumName);
				subTitleTv.setVisibility(View.VISIBLE);
				subTitleTv.setText(album.MediaCount
						+ " "
						+ subTitleTv.getResources().getString(
								album.MediaCount == 1 ? (mPhotos ? R.string.photo_label : R.string.video_label) : (mPhotos ? R.string.photos_label
										: R.string.videos_label)));
				privacyIv.setVisibility(album.Visibility != AlbumCreateRequest.VISIBILITY_PUBLIC ? View.VISIBLE : View.GONE);
				videoIv.setVisibility(mPhotos ? View.GONE : View.VISIBLE);
			}
		}
	}
}
