package com.schollyme.notifications;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.R;
import com.schollyme.blog.BlogDetailActivity;
import com.schollyme.friends.FriendProfileActivity;
import com.schollyme.activity.TranscriptsRequestFromCoachActivity;
import com.schollyme.adapter.NotificationsAdapter;
import com.schollyme.evaluation.EvaluationDetailActivity;
import com.schollyme.evaluation.WriteEvaluationActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.friends.FriendsActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NotificationModel;
import com.schollyme.scores.MyScoresActivity;
import com.schollyme.team.TeamPageActivity;
import com.schollyme.wall.MediaViewerWallActivity;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.NotificationChangeReadStatusRequest;
import com.vinfotech.request.NotificationChangeSeenStatusRequest;
import com.vinfotech.request.NotificationListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.Utility;

public class NotificationFragment extends BaseFragment implements OnClickListener, OnRefreshListener {

    private TextView mNoRecordMessageTV;
    private ListView mNotificationLV;
    private Context mContext;
    private int pageIndex = 1;
    private List<NotificationModel> mNotifications;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private NotificationsAdapter mAdapter;
    private boolean loadingFlag = false;
    private ErrorLayout mErrorLayout;

    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;

    public static BaseFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        NotificationFragment mFragment = new NotificationFragment();
        mFragment.mCurrentPageToShow = currentPageToShow;
        mFragment.mHeaderLayout = headerLayout;
        mFragment.mShowBackBtn = showBack;

        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_fragment, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initHeader();

        mNotificationLV = (ListView) view.findViewById(R.id.notification_lv);
        mNoRecordMessageTV = (TextView) view.findViewById(R.id.no_record_message_tv);
        mContext = getActivity();
        mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_rl));
        mNotifications = new ArrayList<>();

        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color,
                R.color.text_hint_color);
        mSwipeRefreshWidget.setOnRefreshListener(this);

        getNotificationsRequest();
    }

    private void initHeader() {

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                getResources().getString(R.string.notifications), 0);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashboardActivity) getActivity()).sliderListener();
            }
        }, null);

    }

    @Override
    public void onClick(View arg0) {

    }

    private void displayNotificationsAdapter() {

        if (mNotifications.size() > 0) {
            mNotificationLV.setVisibility(View.VISIBLE);
            mNoRecordMessageTV.setVisibility(View.GONE);
            mAdapter = new NotificationsAdapter(mContext);
            mAdapter.setList(mNotifications);
            mNotificationLV.setAdapter(mAdapter);
            markNotificationSeenRequest();
            mNotificationLV.setOnScrollListener(new OnScrollListener() {
                boolean bReachedListEnd = false;

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (SCROLL_STATE_IDLE == scrollState && bReachedListEnd) {
                        Log.d(this.getClass().getName(), "Reached to list bottom. loading next page...");
                        if (null != mAdapter && !loadingFlag) {
                            pageIndex++;
                            loadingFlag = true;
                            getNotificationsRequest();
                        }
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    bReachedListEnd = ((firstVisibleItem + visibleItemCount) >= (totalItemCount / 2));
                }
            });

            mNotificationLV.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    markNotificationReadRequest(mNotifications.get(position).mNotificationGUID);
                    int typeId = mNotifications.get(position).mNotificationTypeId;
                    Utility.LogP("NotificationLV ","typeId: "+typeId);
                    switch (typeId) {

                        case 1:
                            //added new post in team page
                            String mActvityGUID = mNotifications.get(position).mProfileURL;
                            String mTeamGUID = mNotifications.get(position).mEntityGUID;
                            ((DashboardActivity) mContext).switchFragment(DashboardActivity.HOME_FRAGMENTINDEX,
                                    mActvityGUID, mTeamGUID, typeId, mNotifications.get(position).mEntityGUID);

                            break;
                        case 2:
                            //Commented on wall post
                            String commmentGUID = mNotifications.get(position).mEntityGUID;
                            ((DashboardActivity) mContext).switchFragment(DashboardActivity.HOME_FRAGMENTINDEX,
                                    mNotifications.get(position).mProfileURL,
                                    commmentGUID, typeId, mNotifications.get(position).mEntityGUID);
                            break;
                        case 3:
                            //Like your wall Post
                            String nTeamGUID = mNotifications.get(position).mEntityGUID;
                            if ("PHOTO_ALBUM".equals(mNotifications.get(position).mRefer)
                                    || "VIDEO_ALBUM".equals(mNotifications.get(position).mRefer)) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("mediagrid://app.postfeed.wall/params?UserGUID="
                                                + new LogedInUserModel(mContext).mUserGUID + "&FromQuery=FromQuery&AlbumGUID="
                                        + mNotifications.get(position).mProfileURL));
                                startActivity(browserIntent);
                            } else if ("WALL".equals(mNotifications.get(position).mRefer)) {
                                ((DashboardActivity) mContext).switchFragment(DashboardActivity.HOME_FRAGMENTINDEX,
                                        mNotifications.get(position).mProfileURL,
                                        nTeamGUID, typeId, mNotifications.get(position).mEntityGUID);
                            } else if ("WALL".equals(mNotifications.get(position).mRefer)) {
                                ((DashboardActivity) mContext).switchFragment(DashboardActivity.HOME_FRAGMENTINDEX,
                                        mNotifications.get(position).mProfileURL,
                                        nTeamGUID, typeId, mNotifications.get(position).mEntityGUID);
                            } else if ("USER".equals(mNotifications.get(position).mRefer)) {
                                String nActvityGUID = mNotifications.get(position).mProfileURL;
                                ((DashboardActivity) mContext).switchFragment(DashboardActivity.HOME_FRAGMENTINDEX,
                                        nActvityGUID, nTeamGUID, 1, "");
                                //	startActivity(TeamPageActivity.getIntent(mContext,nTeamGUID,nActvityGUID, 0));
                            }

                            break;
                        case 4:

                            //Member added to group
                            break;
                        case 16:

                            //Friend Request Received
                            //		markNotificationReadRequest(mNotifications.get(position).mNotificationGUID);
                            startActivity(FriendProfileActivity.getIntent(mContext, "0",
                                    mNotifications.get(position).mProfileURL, "NotificationFragment"));
                            break;
                        case 17:
                            //Friend Request Accepted
                            //	markNotificationReadRequest(mNotifications.get(position).mNotificationGUID);
                            startActivity(FriendProfileActivity.getIntent(mContext, "0",
                                    mNotifications.get(position).mProfileURL, "NotificationFragment"));
                            break;

                        case 18:
                            //Tag in Post

                            break;
                        case 19:
                            //Post on Wall
                            String sTeamGUID = mNotifications.get(position).mEntityGUID;
                            ((DashboardActivity) mContext).switchFragment(DashboardActivity.HOME_FRAGMENTINDEX,
                                    mNotifications.get(position).mProfileURL,
                                    sTeamGUID, typeId, mNotifications.get(position).mEntityGUID);
                            break;
                        case 20: //Like On Comment
                            String sPostGUID = mNotifications.get(position).mEntityGUID;
                            ((DashboardActivity) mContext).switchFragment(DashboardActivity.HOME_FRAGMENTINDEX,
                                    mNotifications.get(position).mProfileURL,
                                    sPostGUID, typeId, mNotifications.get(position).mEntityGUID);

                            break;
                        case 21:
                            //Tag On Comment

                            break;
                        case 22:
                            //Group Request Received

                            break;
                        case 23:
                            //Group Request Accepted

                            break;
                        case 24:
                            //Group Joined

                            break;
                        case 25:
                            //Group Blocked

                            break;
                        case 26:
                            //Event Edit notification to host

                            break;
                        case 27:
                            //Event Edit notification to admin

                            break;
                        case 28:
                            //Event Edit notification to attending user
                            break;
                        case 29:
                            //Event Edit notification to user, may attend
                            break;

                        case 43://Left Team Page
                            startActivity(TeamPageActivity.getIntent(mContext, mNotifications.get(position).mEntityGUID, "", 0));
                            break;
                        case 48:
                            //Likes your Photo
                            startActivity(MediaViewerWallActivity.getIntent(mContext, mNotifications.get(position).mProfileURL,
                                    mNotifications.get(position).mEntityGUID));

                            break;
                        case 50:
                            //commented on your Photo
                            startActivity(MediaViewerWallActivity.getIntent(mContext, mNotifications.get(position).mProfileURL,
                                    mNotifications.get(position).mEntityGUID));

                            break;
                        case 56:
                            startActivity(FriendProfileActivity.getIntent(mContext, "0",
                                    mNotifications.get(position).mProfileURL, "NotificationFragment"));
                            break;
                        case 51:
                            break;
                        case 57:
                            break;
                        case 58:
                            startActivity(MyScoresActivity.getIntent(mContext, new LogedInUserModel(mContext).mUserGUID,new LogedInUserModel(mContext).mFirstName));
                            break;
                        case 59:
                            break;
                        case 60:
                            //User Marked as Evaluator from admin
//                            ((DashboardActivity) mContext).switchFragment(6, mNotifications.get(position).mProfileURL,
//                                    "", typeId, mNotifications.get(position).mEntityGUID);
                            //		LogedInUserModel.updatePreferenceString(mContext, "isPaidCoach", "1");
                            //		LogedInUserModel.updatePreferenceString(mContext, "isEvaluator", "0");

                            break;
                        case 54:
                            startActivity(TranscriptsRequestFromCoachActivity.getIntent(mContext, ""));
                            break;
                        case 64:
                            //Evaluation request to coach
                            LogedInUserModel model = new LogedInUserModel(mContext);
                            if (model.mUserSportsId.equals(mNotifications.get(position).mSportsID)) {
                                String mTitle = mNotifications.get(position).mUserName + "'s " + getString(R.string.Evaluation);
                                startActivity(WriteEvaluationActivity.getIntent(mContext,
                                        mNotifications.get(position).mProfileURL, model.mUserSportsId,
                                        model.mUserSportsName, mNotifications.get(position).mEntityGUID, "", mTitle));
                            } else {
                                DialogUtil.showOkDialog(mContext, getResources().getString(R.string.changed_sports), "");
                            }
                            break;

                        case 65:
                            //Coach Write Evaluation for athlete request
                            startActivity(EvaluationDetailActivity.getIntent(mContext, mNotifications.get(position).mProfileURL,
                                    getString(R.string.My_Evaluation), null, mNotifications.get(position).mEntityGUID, ""));
                            break;

                        case 66:
                            //User Marked as PaidCoach from admin
//                            ((DashboardActivity) mContext).switchFragment(7, mNotifications.get(position).mProfileURL,
//                                    "", typeId, mNotifications.get(position).mEntityGUID);
                            //	LogedInUserModel.updatePreferenceString(mContext, "isEvaluator", "1");
                            //	LogedInUserModel.updatePreferenceString(mContext, "isPaidCoach", "0");

                            break;

                        case 67:
                            if ("PHOTO_ALBUM".equals(mNotifications.get(position).mRefer) || "VIDEO_ALBUM"
                                    .equals(mNotifications.get(position).mRefer)) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("mediagrid://app.postfeed.wall/params?UserGUID="
                                                + new LogedInUserModel(mContext).mUserGUID + "&FromQuery=FromQuery&AlbumGUID="
                                        + mNotifications.get(position).mProfileURL));
                                startActivity(browserIntent);
                            } else if ("WALL".equals(mNotifications.get(position).mRefer)) {
                                ((DashboardActivity) mContext).switchFragment(DashboardActivity.HOME_FRAGMENTINDEX, mNotifications.get(position).mProfileURL,
                                        mNotifications.get(position).mEntityGUID, typeId, mNotifications.get(position).mEntityGUID);
                            } else if ("VIDEO".equals(mNotifications.get(position).mRefer) ||
                                    "PHOTO".equals(mNotifications.get(position).mRefer)) {
                                startActivity(MediaViewerWallActivity.getIntent(mContext, mNotifications.get(position).mProfileURL,
                                        mNotifications.get(position).mEntityGUID));
                            }
                            break;

                        case 70:
                            ((DashboardActivity) mContext).switchFragment(16, mNotifications.get(position).mProfileURL, "",
                                    typeId, mNotifications.get(position).mEntityGUID);

//                            String mBlogGUID = mNotifications.get(position).mEntityGUID;
//                            startActivityForResult(BlogDetailActivity.getIntent(mContext, mBlogGUID),
//                                    BlogDetailActivity.REQ_CODE_BLOGDETAIL_SCREEN);
                            break;

                        default:
                            break;
                    }
                }
            });
        } else {
            mAdapter = new NotificationsAdapter(mContext);
            mAdapter.setList(mNotifications);
            mNotificationLV.setAdapter(mAdapter);
            mNoRecordMessageTV.setVisibility(View.VISIBLE);
        }
    }

    /**
     * This api will retrive all inapp notification list
     */
    private void getNotificationsRequest() {
        NotificationListRequest mRequest = new NotificationListRequest(mContext);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mSwipeRefreshWidget.setRefreshing(false);
                if (success) {
                    mNotifications.addAll((Collection<? extends NotificationModel>) data);
                    if (pageIndex == 1) {
                        displayNotificationsAdapter();
                    } else {
                        mAdapter.notifyDataSetChanged();
                    }

                } else {
                    mErrorLayout.showError(data.toString(), true, MsgType.Error);
                }
            }
        });
        mRequest.NotificationListServerRequest(pageIndex);
    }

    /**
     * This api will mark notification as seen
     */
    private void markNotificationSeenRequest() {
        NotificationChangeSeenStatusRequest mRequest = new NotificationChangeSeenStatusRequest(mContext);
        mRequest.getNotificationSeenStatusServerRequest();
        mRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                Log.i("Notification Mark Seen Status", "Users All Notification are marked as seen ::" + data.toString());
                //Do nothing
            }
        });
    }

    /**
     * This api will mark notification as read
     */
    private void markNotificationReadRequest(String mNotificationGUID) {

        NotificationChangeReadStatusRequest mRequest = new NotificationChangeReadStatusRequest(mContext);
        mRequest.getNotificationReadStatusServerRequest(mNotificationGUID);
        mRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                Log.i("Notification Mark read Status", "Users clicked Notification marked as read ::" + data.toString());
                //Do nothing
            }
        });
    }

    @Override
    public void onRefresh() {
        pageIndex = 1;
        mNotifications = new ArrayList<NotificationModel>();
        getNotificationsRequest();
    }
}