package com.schollyme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.R;
import com.vinfotech.utility.FontLoader;

public class SMTop10Fragment extends BaseFragment{
	
	private Context mContext;
	private TextView mCommingSoonTV;

	public static BaseFragment getInstance() {
		SMTop10Fragment mSettingsFragment = new SMTop10Fragment();
		return mSettingsFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.sm_top10_fragment, container, false);
		BaseFragmentActivity.setHeader(R.drawable.icon_menu, 0, getResources().getString(R.string.sm_10), new OnClickListener() {

			@Override
			public void onClick(View v) {
					((DashboardActivity) mContext).sliderListener();
			}
		}, null);
		DashboardActivity.hideShowActionBar(true);
		mCommingSoonTV = (TextView)view.findViewById(R.id.no_record_message_tv);
		mContext = getActivity();
		FontLoader.setRobotoRegularTypeface(mCommingSoonTV);
		return view;
	}
}