package com.schollyme.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schollyme.R;
import com.schollyme.activity.AboutActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.friends.FriendsActivity;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.UserModel;
import com.vinfotech.header.HeaderLayout;

/**
 * Activity class. This may be useful in display user about/profile details
 *
 * @author Ravi Bhandari
 */
public class AboutmeFragment extends BaseFragment implements OnClickListener {

    private static String mUserGUID = "";
    private String fromActivity = "";
    private UserModel mUserModel;
    private View view;

    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;
    private int mCurrentPageToShow;
    private Context mContext;
    private Activity mActivity;

    public static AboutmeFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        AboutmeFragment aboutmeFragment = new AboutmeFragment();
        aboutmeFragment.mCurrentPageToShow = currentPageToShow;
        aboutmeFragment.mHeaderLayout = headerLayout;
        aboutmeFragment.mShowBackBtn = showBack;

        return aboutmeFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.about_me_fragment, null);
        mContext = mActivity = getActivity();
        mLogedInUserModel = new LogedInUserModel(mContext);

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                getResources().getString(R.string.about_header), 0);
        mHeaderLayout.setListenerItI(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShowBackBtn) {
                    if (((FriendsActivity) getActivity()).mFromActivity.equalsIgnoreCase("GCMIntentService")) {
                        Intent intent = DashboardActivity.getIntent(getActivity(),
                                DashboardActivity.HOME_FRAGMENTINDEX, "FriendsTopFragment");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getActivity().finish();
                    } else
                        getActivity().finish();
                } else {
                    ((DashboardActivity) mContext).sliderListener();
                }
            }
        }, null);

        mUserGUID = getArguments().getString("mUserGUID");
        fromActivity = getArguments().getString("fromActivity");

        return view;
    }
    LogedInUserModel mLogedInUserModel;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {



        TextView txtAboutme = (TextView) view.findViewById(R.id.aboutme_tv);
        TextView txtMoreAboutme = (TextView) view.findViewById(R.id.more_aboutme_tv);
        TextView txtMyFavorite = (TextView) view.findViewById(R.id.my_favorite_tv);
        TextView txtMySports = (TextView) view.findViewById(R.id.my_sports_tv);

        RelativeLayout rlAboutme = (RelativeLayout) view.findViewById(R.id.aboutme_rl);
        RelativeLayout rlMoreAboutme = (RelativeLayout) view.findViewById(R.id.more_aboutme_rl);
        RelativeLayout rlMyFavorite = (RelativeLayout) view.findViewById(R.id.my_favorite_rl);
        RelativeLayout rlMySports = (RelativeLayout) view.findViewById(R.id.my_sports_rl);

        if (mLogedInUserModel.mUserType == 1) {

            rlAboutme.setVisibility(View.VISIBLE);
            rlMyFavorite.setVisibility(View.VISIBLE);
            rlMoreAboutme.setVisibility(View.VISIBLE);
            rlMySports.setVisibility(View.GONE);

        } else if (mLogedInUserModel.mUserType == 2) {

            rlAboutme.setVisibility(View.VISIBLE);
            rlMyFavorite.setVisibility(View.VISIBLE);
            rlMoreAboutme.setVisibility(View.VISIBLE);
            rlMySports.setVisibility(View.VISIBLE);

        } else if (mLogedInUserModel.mUserType == 3) {

            rlAboutme.setVisibility(View.VISIBLE);
            rlMyFavorite.setVisibility(View.VISIBLE);
            rlMoreAboutme.setVisibility(View.VISIBLE);
            rlMySports.setVisibility(View.GONE);

        }


        txtAboutme.setOnClickListener(this);
        txtMoreAboutme.setOnClickListener(this);
        txtMyFavorite.setOnClickListener(this);
        txtMySports.setOnClickListener(this);
    }

    public static Intent getIntent(Context context, String mUserGUID, String fromAct) {
        Intent intent = new Intent(context, AboutmeFragment.class);
        intent.putExtra("mUserGUID", mUserGUID);
        intent.putExtra("fromActivity", fromAct);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.aboutme_tv:
                intent = new Intent(mActivity, AboutActivity.class);
                intent.putExtra("mUserGUID", mUserGUID);
                intent.putExtra("fromActivity", "");
                intent.putExtra("aboutType", "About Me");
                intent.putExtra("userType",""+mLogedInUserModel.mUserType);
                startActivity(intent);
                break;
            case R.id.more_aboutme_tv:
                intent = new Intent(mActivity, AboutActivity.class);
                intent.putExtra("mUserGUID", mUserGUID);
                intent.putExtra("fromActivity", "");
                intent.putExtra("aboutType", "More About Me");
                intent.putExtra("userType", ""+mLogedInUserModel.mUserType);
                startActivity(intent);
                break;
            case R.id.my_favorite_tv:
                intent = new Intent(mActivity, AboutActivity.class);
                intent.putExtra("mUserGUID", mUserGUID);
                intent.putExtra("fromActivity", "");
                intent.putExtra("aboutType", "My Favorite");
                intent.putExtra("userType",""+mLogedInUserModel.mUserType);
                startActivity(intent);
                break;
            case R.id.my_sports_tv:
                intent = new Intent(mActivity, AboutActivity.class);
                intent.putExtra("mUserGUID", mUserGUID);
                intent.putExtra("fromActivity", "");
                intent.putExtra("aboutType", "My Sports");
                intent.putExtra("userType",""+mLogedInUserModel.mUserType);
                startActivity(intent);
                break;
        }
    }
}