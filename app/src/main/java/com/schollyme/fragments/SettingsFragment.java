package com.schollyme.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.activity.BlockedUserActivity;
import com.schollyme.activity.ChangePasswordActivity;
import com.schollyme.activity.ChangeUserTypeActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.activity.LanguageChangeActivity;
import com.schollyme.activity.SetupProfileMandatoryActivity;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.MarshMallowPermission;

/**
 * Fragment class. This will display list of user specific settings
 *
 * @author Ravi Bhandari
 */
public class SettingsFragment extends BaseFragment implements OnClickListener {

    private Context mContext;
    private TextView mNotificationTV, mChangePasswordTV, mChangeLanguageTV,
            mChangeUserTypeTV, mBlockedUsersTV, mChangeProfilePicTV, mChangeBackgroundPicTV;
    private SwitchCompat mSwitch;
    private SharedPreferences mNotificationPreferance;

    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;

    public static BaseFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        SettingsFragment mSettingsFragment = new SettingsFragment();
        mSettingsFragment.mCurrentPageToShow = currentPageToShow;
        mSettingsFragment.mHeaderLayout = headerLayout;
        mSettingsFragment.mShowBackBtn = showBack;
        return mSettingsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment_layout, container, false);
        this.mContext = getActivity();

        initHeader();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        MarshMallowPermission marshMallowPermission = new MarshMallowPermission(getActivity());
        if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        }

        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mChangeLanguageTV = (TextView) view.findViewById(R.id.language_change_tv);
        mChangePasswordTV = (TextView) view.findViewById(R.id.change_password_tv);
        mNotificationTV = (TextView) view.findViewById(R.id.notification_tv);
        mChangeUserTypeTV = (TextView) view.findViewById(R.id.change_usertype_tv);
        mBlockedUsersTV = (TextView) view.findViewById(R.id.blocked_user_tv);
        mChangeProfilePicTV = (TextView) view.findViewById(R.id.change_profile_photo_tv);
        mChangeBackgroundPicTV = (TextView) view.findViewById(R.id.change_background_photo_tv);

        mChangeLanguageTV.setOnClickListener(this);
        mChangePasswordTV.setOnClickListener(this);
        mNotificationTV.setOnClickListener(this);
        mChangeUserTypeTV.setOnClickListener(this);
        mBlockedUsersTV.setOnClickListener(this);
        mChangeProfilePicTV.setOnClickListener((View.OnClickListener) getActivity());
        mChangeBackgroundPicTV.setOnClickListener((View.OnClickListener) getActivity());
        mSwitch = (SwitchCompat) view.findViewById(R.id.notification_switch);

        mNotificationPreferance = mContext.getSharedPreferences(Config.NOTIFICATION_SETTINGS, 0);
        if (mNotificationPreferance.getBoolean("isOn", true)) {
            mSwitch.setChecked(true);
        } else {
            mSwitch.setChecked(false);
        }

        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                if (isChecked) {
                    updateNotificationSettings(true);
                } else {
                    updateNotificationSettings(false);
                }
            }
        });
    }

    private void initHeader() {

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                getResources().getString(R.string.settings), 0);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                ((DashboardActivity) getActivity()).sliderListener();
                }catch (NullPointerException e){

                }
            }
        }, null);
    }

    private void updateNotificationSettings(boolean setting) {
        SharedPreferences.Editor editor = mNotificationPreferance.edit();
        editor.putBoolean("isOn", setting);
        editor.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_password_tv:
                startActivity(ChangePasswordActivity.getIntent(mContext));
                break;
            case R.id.language_change_tv:
                startActivity(LanguageChangeActivity.getIntent(mContext));
                break;
            case R.id.notification_tv:
                break;
            case R.id.change_usertype_tv:
                startActivity(ChangeUserTypeActivity.getIntent(mContext));
                break;
            case R.id.blocked_user_tv:
                startActivity(BlockedUserActivity.getIntent(mContext));
                break;
        }
    }

    @Override
    public void onLocaleUpdate() {
        super.onLocaleUpdate();
        setLocalChanges();
    }

    /**
     * This will refresh view after user change language
     */
    private void setLocalChanges() {
        BaseFragmentActivity.setHeader(R.drawable.icon_menu, 0, getResources().getString(R.string.settings), new OnClickListener() {

            @Override
            public void onClick(View v) {
                ((DashboardActivity) mContext).sliderListener();
            }
        }, null);
        mNotificationTV.setText(getResources().getString(R.string.notifications));
        mChangePasswordTV.setText(getResources().getString(R.string.change_password_setting));
        mChangeLanguageTV.setText(getResources().getString(R.string.language));
        mChangeUserTypeTV.setText(getResources().getString(R.string.change_user_type));
        mBlockedUsersTV.setText(getResources().getString(R.string.blocked_user));
        mChangeProfilePicTV.setText(getResources().getString(R.string.change_profile_photo));
        mChangeBackgroundPicTV.setText(getResources().getString(R.string.change_background_photo));

        if (mSwitch.isChecked()) {
            mSwitch.setTextOn(getResources().getString(R.string.on));
        } else {
            mSwitch.setTextOff(getResources().getString(R.string.off));
        }
    }
}