package com.schollyme.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkStateChangedReceiver extends BroadcastReceiver{

	protected boolean connected;

	@Override
	public void onReceive(Context context, Intent intent) {

		if(intent == null || intent.getExtras() == null)
			return;

		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = manager.getActiveNetworkInfo();

		if(ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {
			connected = true;
			context.sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
			context.sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));
		} else if(intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
			connected = false;
		}

	}

	public void notifyNetworkState(NetworkStateChangeListener listener) {
		if(listener == null)
			return;
		if(connected == true)
			listener.onConnected();
		else
			listener.onDisConnected();
	}

	public interface NetworkStateChangeListener{
		public void onConnected();
		public void onDisConnected();
	}
}