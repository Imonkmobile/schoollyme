package com.schollyme.signup;

import java.io.Serializable;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class FacebookProfile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 29681746120246595L;
	private static final String PREF_NAME = "FacebookProfilePref";
	public String id;
	public String name;
	public String firstName;
	public String lastName;
	public String link;
	public String birthday;
	public String location;
	public String bio;
	public String quotes;
	public String work;
	public String education;
	public String gender;
	public String email;
	public String timezone;
	public String locale;
	public boolean verified;
	public String updatedTime;
	public String username;
	public String accessToken;
	public long accessExpire;
	public String zipCode;

	public FacebookProfile(){}

	public FacebookProfile(String id, String name, String firstName, String lastName, String link, String birthday, String location,
			String bio, String quotes, String work, String education, String gender, String email, String timezone, String locale,
			boolean verified, String updatedTime, String username) {
		super();
		this.id = id;
		this.name = name;
		this.firstName = firstName;
		this.lastName = lastName;
		this.link = link;
		this.birthday = birthday;
		this.location = location;
		this.bio = bio;
		this.quotes = quotes;
		this.work = work;
		this.education = education;
		this.gender = gender;
		this.email = email;
		this.timezone = timezone;
		this.locale = locale;
		this.verified = verified;
		this.updatedTime = updatedTime;
		this.username = username;
	}

	public FacebookProfile(JSONObject jsonObject) {
		if (null != jsonObject) {
			this.id = jsonObject.optString("id");
			this.name = jsonObject.optString("name");
			this.firstName = jsonObject.optString("first_name");
			this.lastName = jsonObject.optString("last_name");
			this.link = jsonObject.optString("link", "");
			this.birthday = jsonObject.optString("birthday", "");
			JSONObject locJSONObject = jsonObject.optJSONObject("location");
			if (null != locJSONObject) {
				this.location = jsonObject.optJSONObject("location").optString("name", "");
			} else {
				this.location = "Not Shared";
			}
			this.bio = jsonObject.optString("bio", "");
			this.quotes = jsonObject.optString("quotes", "");
			this.work = jsonObject.optString("work", "");
			this.education = jsonObject.optString("education", "");
			this.gender = jsonObject.optString("gender", "");
			if (jsonObject.has("email")) {
				this.email = jsonObject.optString("email", "");
			} else {
				if (jsonObject.has("username")) {
					this.email = jsonObject.optString("username", "") + "@facebook.com";
				} else {
					this.email = jsonObject.optString("id", "") + "@facebook.com";
				}
			}
			this.timezone = jsonObject.optString("timezone", "");
			this.locale = jsonObject.optString("locale", "");
			this.verified = jsonObject.optBoolean("verified", false);
			this.updatedTime = jsonObject.optString("updated_time", "");
			this.username = jsonObject.optString("username", "");
		}
	}

	public FacebookProfile(String accessToken,long accessExpire,Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor mEditor = sharedPreferences.edit();
		mEditor.putString("accessToken", accessToken);
		mEditor.putLong("accessExpire", accessExpire);
		mEditor.commit();
	}

	public FacebookProfile(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

		this.id = sharedPreferences.getString("id", "");
		this.name = sharedPreferences.getString("name", "");
		this.firstName = sharedPreferences.getString("firstName", "");
		this.lastName = sharedPreferences.getString("lastName", "");
		this.link = sharedPreferences.getString("link", "");
		this.birthday = sharedPreferences.getString("birthday", "");
		this.location = sharedPreferences.getString("location", "");
		this.bio = sharedPreferences.getString("bio", "");
		this.quotes = sharedPreferences.getString("quotes", "");
		this.work = sharedPreferences.getString("work", "");
		this.education = sharedPreferences.getString("education", "");
		this.gender = sharedPreferences.getString("gender", "");
		this.email = sharedPreferences.getString("email", "");
		this.timezone = sharedPreferences.getString("timezone", "");
		this.locale = sharedPreferences.getString("locale", "");
		this.verified = sharedPreferences.getBoolean("verified", false);
		this.updatedTime = sharedPreferences.getString("updatedTime", "");
		this.username = sharedPreferences.getString("username", "");
		this.accessToken = sharedPreferences.getString("accessToken", "");
	}

	public void clearFacebookProfileFromPreferences(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor prefsEditor = sharedPreferences.edit();

		prefsEditor.remove("id");
		prefsEditor.remove("name");
		prefsEditor.remove("firstName");
		prefsEditor.remove("lastName");
		prefsEditor.remove("link");
		prefsEditor.remove("birthday");
		prefsEditor.remove("location");
		prefsEditor.remove("bio");
		prefsEditor.remove("quotes");
		prefsEditor.remove("work");
		prefsEditor.remove("education");
		prefsEditor.remove("gender");
		prefsEditor.remove("email");
		prefsEditor.remove("timezone");
		prefsEditor.remove("locale");
		prefsEditor.remove("verified");
		prefsEditor.remove("updatedTime");
		prefsEditor.remove("apiToken");
		prefsEditor.remove("accessToken");
		prefsEditor.commit();

		this.id = "";
		this.name = "";
		this.firstName = "";
		this.lastName = "";
		this.link = "";
		this.birthday = "";
		this.location = "";
		this.bio = "";
		this.quotes = "";
		this.work = "";
		this.education = "";
		this.gender = "";
		this.email = "";
		this.timezone = "";
		this.locale = "";
		this.verified = false;
		this.updatedTime = "";
		this.username = "";
	}

	public void saveFacebookProfileInPreferences(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor prefsEditor = sharedPreferences.edit();

		prefsEditor.putString("id", id);
		prefsEditor.putString("name", name);
		prefsEditor.putString("firstName", firstName);
		prefsEditor.putString("lastName", lastName);
		prefsEditor.putString("link", link);
		prefsEditor.putString("birthday", birthday);
		prefsEditor.putString("location", location);
		prefsEditor.putString("bio", bio);
		prefsEditor.putString("quotes", quotes);
		prefsEditor.putString("work", work);
		prefsEditor.putString("education", education);
		prefsEditor.putString("gender", gender);
		prefsEditor.putString("email", email);
		prefsEditor.putString("timezone", timezone);
		prefsEditor.putString("locale", locale);
		prefsEditor.putBoolean("verified", verified);
		prefsEditor.putString("updatedTime", updatedTime);
		prefsEditor.putString("username", username);

		prefsEditor.commit();
	}

	public boolean isEmpty() {
		if (isEmpty(id) || isEmpty(username)) {
			return true;
		}
		return false;
	}

	private boolean isEmpty(String name) {
		if (null == name || name.length() < 1) {
			return true;
		}
		return false;
	}

	public String getImage() {
		return "http://graph.facebook.com/" + id + "/picture?type=large";
	}

	public String getAccessToken(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getString("accessToken", "");
	}

	public long getAccessExpire(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getLong("accessExpire", 0);
	}
	
	public String getFBID(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getString("id", "");
	}

	@Override
	public String toString() {
		return "FacebookProfile [id=" + id + ", name=" + name + ", firstName=" + firstName + ", lastName=" + lastName + ", link=" + link
				+ ", birthday=" + birthday + ", location=" + location + ", bio=" + bio + ", quotes=" + quotes + ", work=" + work
				+ ", education=" + education + ", gender=" + gender + ", email=" + email + ", timezone=" + timezone + ", locale=" + locale
				+ ", verified=" + verified + ", updatedTime=" + updatedTime + ", username=" + username + "]";
	}
}