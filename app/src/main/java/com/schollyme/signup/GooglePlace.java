package com.schollyme.signup;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Entity class to store google place api decoded address. Sample address is set
 * on every variable.
 * 
 * @author ramanands
 * 
 */
public class GooglePlace implements Parcelable {
	/**
	 * ChIJ2w1BG638YjkR9EBiNdrEbgk
	 */
	public String place_id = "";
	public String id = "";
	/**
	 * 17/2, Chhoti Gwaltoli, Indore, Madhya Pradesh 452001, India
	 */
	public String formatted_address = "";
	/**
	 * 22.714731
	 */
	public double lat = 0d;
	/**
	 * 75.87035299999999
	 */
	public double lng = 0d;
	/**
	 * 17/2
	 */
	public String premise = "";

	/**
	 * 17/2
	 */
	public String premise_sn = "";
	/**
	 * Chhoti Gwaltoli
	 */
	public String sublocality_level_1 = "";
	/**
	 * Chhoti Gwaltoli
	 */
	public String sublocality_level_1_sn = "";
	/**
	 * Indore
	 */
	public String locality = "";
	/**
	 * Indore
	 */
	public String locality_sn = "";
	/**
	 * Indore
	 */
	public String administrative_area_level_2 = "";
	/**
	 * Indore
	 */
	public String administrative_area_level_2_sn = "";
	/**
	 * Madhya Pradesh
	 */
	public String administrative_area_level_1 = "";
	/**
	 * MP
	 */
	public String administrative_area_level_1_sn = "";
	/**
	 * India
	 */
	public String country = "";
	/**
	 * IN
	 */
	public String country_sn = "";
	/**
	 * 452001
	 */
	public String postal_code = "";
	public String postal_codesn = "";
	/**
	 * 576
	 */
	public String street_number = "";
	public String street_numbersn = "";
	/**
	 * Mahatma Gandhi Road
	 */
	public String route = "";
	public String routesn = "";

	public GooglePlace(JSONObject jsonObject) {
		if (null != jsonObject) {
			JSONArray acJsonArray = jsonObject.optJSONArray("address_components");
			if (null != acJsonArray) {
				for (int i = 0; i < acJsonArray.length(); i++) {
					try {
						JSONObject acJsonObject = acJsonArray.getJSONObject(i);
						if (getTyepes(acJsonObject).contains("premise")) {
							premise = acJsonObject.optString("long_name", "");
							premise = acJsonObject.optString("short_name", "");
						} else if (getTyepes(acJsonObject).contains("sublocality_level_1")) {
							sublocality_level_1 = acJsonObject.optString("long_name", "");
							sublocality_level_1 = acJsonObject.optString("short_name", "");
						} else if (getTyepes(acJsonObject).contains("locality")) {
							locality = acJsonObject.optString("long_name", "");
							locality_sn = acJsonObject.optString("short_name", "");
						} else if (getTyepes(acJsonObject).contains("administrative_area_level_2")) {
							administrative_area_level_2 = acJsonObject.optString("long_name", "");
							administrative_area_level_2_sn = acJsonObject.optString("short_name", "");
						} else if (getTyepes(acJsonObject).contains("administrative_area_level_1")) {
							administrative_area_level_1 = acJsonObject.optString("long_name", "");
							administrative_area_level_1_sn = acJsonObject.optString("short_name", "");
						} else if (getTyepes(acJsonObject).contains("country")) {
							country = acJsonObject.optString("long_name", "");
							country_sn = acJsonObject.optString("short_name", "");
						} else if (getTyepes(acJsonObject).contains("street_number")) {
							street_number = acJsonObject.optString("long_name", "");
							street_numbersn = acJsonObject.optString("short_name", "");
						} else if (getTyepes(acJsonObject).contains("postal_code")) {
							postal_code = acJsonObject.optString("long_name", "");
							postal_codesn = acJsonObject.optString("short_name", "");
						} else if (getTyepes(acJsonObject).contains("route")) {
							route = acJsonObject.optString("long_name", "");
							routesn = acJsonObject.optString("short_name", "");
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
			place_id = jsonObject.optString("place_id", sublocality_level_1);
			id = jsonObject.optString("id", sublocality_level_1);
			formatted_address = jsonObject.optString("formatted_address", "");
			JSONObject gJsonObject = jsonObject.optJSONObject("geometry");
			if (null != gJsonObject) {
				JSONObject lJsonObject = gJsonObject.optJSONObject("location");
				if (null != lJsonObject) {
					lat = lJsonObject.optDouble("lat", 0d);
					lng = lJsonObject.optDouble("lng", 0d);
				}
			}
		}
	}

	public GooglePlace(String locality, String administrative_area_level_1, String country) {
		this.locality = TextUtils.isEmpty(locality) ? "" : locality;
		this.administrative_area_level_1 = TextUtils.isEmpty(administrative_area_level_1) ? "" : administrative_area_level_1;
		this.country = TextUtils.isEmpty(country) ? "" : country;
	}

	private List<String> getTyepes(JSONObject jsonObject) {
		List<String> types = new ArrayList<String>();
		JSONArray jsonArray = jsonObject.optJSONArray("types");
		if (null != jsonArray) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					types.add(jsonArray.getString(i));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return types;
	}

	public boolean hasAddress() {
		return formatted_address.trim().length() > 0;
	}

	public boolean hasCityStateCountry() {
		return !TextUtils.isEmpty(locality) && !TextUtils.isEmpty(administrative_area_level_1) && !TextUtils.isEmpty(country);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(place_id);
		dest.writeString(formatted_address);
		dest.writeDouble(lat);
		dest.writeDouble(lng);
		dest.writeString(premise);
		dest.writeString(sublocality_level_1);
		dest.writeString(locality);
		dest.writeString(administrative_area_level_2);
		dest.writeString(administrative_area_level_2_sn);
		dest.writeString(administrative_area_level_1);
		dest.writeString(administrative_area_level_1_sn);
		dest.writeString(country);
		dest.writeString(country_sn);
		dest.writeString(postal_code);
	}

	private GooglePlace(Parcel in) {
		this.place_id = in.readString();
		this.formatted_address = in.readString();
		this.lat = in.readDouble();
		this.lng = in.readDouble();
		this.premise = in.readString();
		this.sublocality_level_1 = in.readString();
		this.locality = in.readString();
		this.administrative_area_level_2 = in.readString();
		this.administrative_area_level_2_sn = in.readString();
		this.administrative_area_level_1 = in.readString();
		this.administrative_area_level_1_sn = in.readString();
		this.country = in.readString();
		this.country_sn = in.readString();
		this.postal_code = in.readString();
	}

	public static final Creator<GooglePlace> CREATOR = new Creator<GooglePlace>() {

		@Override
		public GooglePlace createFromParcel(Parcel source) {
			return new GooglePlace(source);
		}

		@Override
		public GooglePlace[] newArray(int size) {
			return new GooglePlace[size];
		}
	};

	@Override
	public String toString() {
		return "GooglePlace [place_id=" + place_id + ", formatted_address=" + formatted_address + ", lat=" + lat + ", lng=" + lng
				+ ", premise=" + premise + ", sublocality_level_1=" + sublocality_level_1 + ", locality=" + locality
				+ ", administrative_area_level_2=" + administrative_area_level_2 + ", administrative_area_level_2_sn="
				+ administrative_area_level_2_sn + ", administrative_area_level_1=" + administrative_area_level_1
				+ ", administrative_area_level_1_sn=" + administrative_area_level_1_sn + ", country=" + country + ", country_sn="
				+ country_sn + ", postal_code=" + postal_code + "]";
	}
}
