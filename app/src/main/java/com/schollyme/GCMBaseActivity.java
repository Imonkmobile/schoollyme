package com.schollyme;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
import com.schollyme.GCMIntentService.GCMRegisterListener;
import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;

/**
 * Base activity class. This may be useful in<br/>
 * Implementing google analytics or <br/>
 * Any app wise implementation
 * 
 * @author ramanands
 * 
 */
public abstract class GCMBaseActivity extends BaseActivity {
	private static final String TAG = GCMBaseActivity.class.getSimpleName();
	
	protected String mDeviceToken = "deviceToken";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			GCMIntentService.addGCMRegisterListener(mGCMRegisterListener);
			GCMRegistrar.checkDevice(this);
			GCMRegistrar.checkManifest(this);
			final String regId = GCMRegistrar.getRegistrationId(this);
			if(Config.DEBUG){
				Log.d(TAG, "onCreate regId=" + regId);
			}
			if (regId.equals("")) {
				GCMRegistrar.register(this, GCMIntentService.PROJECT_ID);
			} else {
				mGCMRegisterListener.onRegistered(regId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	GCMRegisterListener mGCMRegisterListener = new GCMRegisterListener() {
		
		@Override
		public void onRegistered(String deviceToken) {
			GCMBaseActivity.this.mDeviceToken = deviceToken;
			if(Config.DEBUG){
				Log.i(TAG, "onRegistered deviceToken=" + deviceToken);
			}
		}
		
		@Override
		public void onErrored() {
			if(Config.DEBUG){
				Log.e(TAG, "onErrored deviceToken=" + mDeviceToken);
			}
		}
	};
}
