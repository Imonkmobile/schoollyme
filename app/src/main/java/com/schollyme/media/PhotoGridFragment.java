package com.schollyme.media;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.R;
import com.schollyme.adapter.AlbumAdapter;
import com.schollyme.adapter.PhotoVideoAdapter;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Album;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.AlbumListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
import java.util.List;

public class PhotoGridFragment extends BaseFragment {

    private static final String TAG = PhotoGridFragment.class.getSimpleName();
    private TextView mEmptyTv;
    private SwipeRefreshLayout mPullToRefreshSrl;

    private AlbumListRequest mAlbumListRequest;
    private PhotoVideoAdapter mPhotoVideoAdapter;
    private ErrorLayout mErrorLayout;

    private List<Album> mAlbums = new ArrayList<Album>();
    private int mPageNo = Config.DEFAULT_PAGE_INDEX;
    private boolean mFetchedAll = true;
    private String mUserGUID;
    private String mAlbumType;
    private Album mCreatorAlbum;
    //private LogedInUserModel mLogedInUserModel;

    public static PhotoGridFragment newInstance(ErrorLayout errorLayout, String UserGUID, String AlbumType) {
        //LogedInUserModel logedInUserModel,
        PhotoGridFragment albumGridFragment = new PhotoGridFragment();
        albumGridFragment.mErrorLayout = errorLayout;
        albumGridFragment.mUserGUID = UserGUID;
        //albumGridFragment.mLogedInUserModel = logedInUserModel;
        albumGridFragment.mAlbumType = AlbumType;
        return albumGridFragment;
    }

    @Override
    public void onStop() {
        super.onStop();
        mAlbumListRequest.setActivityStatus(false);

    }

    @Override
    public void onStart() {
        mAlbumListRequest.setActivityStatus(true);
        super.onStart();
    }

    @Override
    public void onResume() {
        mAlbumListRequest.getAlbumListInServer(mUserGUID, mAlbumType, mPageNo,
                AlbumListRequest.SORT_BY_ALBUMNAME, AlbumListRequest.ORDER_BY_DESC);
        super.onResume();
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.photo_grid_fragment, null);

        return view;
    }

    private View mLoadMoreRl, loaderPb;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mPullToRefreshSrl = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                //resetList();
                mPullToRefreshSrl.setRefreshing(true);
                mAlbumListRequest.setLoader(null);
                mAlbumListRequest.getAlbumListInServer(mUserGUID, mAlbumType, mPageNo,
                        AlbumListRequest.SORT_BY_ALBUMNAME,
                        AlbumListRequest.ORDER_BY_DESC);
            }
        });

        mEmptyTv = (TextView) view.findViewById(R.id.empty_tv);
        FontLoader.setRobotoRegularTypeface(mEmptyTv);
        loaderPb = view.findViewById(R.id.loader_pb);

        mLoadMoreRl = view.findViewById(R.id.load_more_rl);
        View bgView = mLoadMoreRl.findViewById(R.id.dummy_bg_v);
        bgView.setBackgroundColor(bgView.getResources().getColor(R.color.verify_text_color));
        bgView.setAlpha(0.4f);

        mAlbumListRequest = new AlbumListRequest(getActivity());
        mAlbumListRequest.setLoader(loaderPb);
        mAlbumListRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    if (null != data) {
                        mAlbums.clear();
                        @SuppressWarnings("unchecked")
                        List<Album> albums = (List<Album>) data;
                        for (Album album : albums) {
                            album.AlbumType = mAlbumType;
                        }
                        mAlbums.addAll(albums);

                        mFetchedAll = (mAlbums.size() >= totalRecords);
                        mPhotoVideoAdapter.setList(mAlbums);
                        mEmptyTv.setVisibility(mAlbums.size() < 1 ? View.VISIBLE : View.INVISIBLE);
                    }
                } else if (null != data) {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, MsgType.Error);
                }
            }
        });
        mPhotoVideoAdapter = new PhotoVideoAdapter(getActivity(),
                AlbumListRequest.ALBUM_TYPE_PHOTO.equalsIgnoreCase(mAlbumType));

        GridView albumGv = (GridView) view.findViewById(R.id.generic_gv);
        albumGv.setAdapter(mPhotoVideoAdapter);
        albumGv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                if (Config.DEBUG) {
                    Log.d(TAG, "onItemClick position=" + position + ", url=" + mPhotoVideoAdapter.getItem(position));
                }

                /*if (position == 0) { //&& mAlbumAdapter.hasCreator()
                    //if (AlbumGridActivity.isMe(mUserGUID, mLogedInUserModel)) {

                    if (mAlbumType.equals(AlbumListRequest.ALBUM_TYPE_PHOTO)) {
                        mCreatorAlbum = new Album(AlbumListRequest.ALBUM_TYPE_PHOTO);
                    } else if (mAlbumType.equals(AlbumListRequest.ALBUM_TYPE_VIDEO)) {
                        mCreatorAlbum = new Album(AlbumListRequest.ALBUM_TYPE_VIDEO);
                    }

                    startActivityForResult(CreateAlbumActivity.getIntent(getActivity(), mCreatorAlbum),
                            CreateAlbumActivity.REQ_CODE_CREATE_ALBUM_ACTIVITY);
                }*//* else {
                    Toast.makeText(getActivity(), R.string.Create_album_not_allowed, Toast.LENGTH_SHORT).show();
                }*//*else{*/

                startActivityForResult(MediaGridActivity.getIntent(getActivity(),
                        mUserGUID, mPhotoVideoAdapter.getItem(position)),
                        MediaGridActivity.REQ_CODE_MEDIA_GRID_ACTIVITY);

                /*}*/
            }
        });

        albumGv.setOnScrollListener(new OnScrollListener() {
            private boolean bReachedListEnd;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (Config.DEBUG) {
                    Log.d(TAG, "onScrollStateChanged bReachedListEnd=" + bReachedListEnd + ", " +
                            "mFetchedAll=" + mFetchedAll + ", scrollState=" + scrollState);
                }
                if (bReachedListEnd && !mFetchedAll) {
                    mFetchedAll = true;
                    mPageNo++;

                    if (mPageNo == 1) {
                        mAlbumListRequest.setLoader(loaderPb);
                    } else {
                        mAlbumListRequest.setLoader(mLoadMoreRl);
                    }
                    mAlbumListRequest.getAlbumListInServer(mUserGUID, mAlbumType, mPageNo,
                            AlbumListRequest.SORT_BY_ALBUMNAME, AlbumListRequest.ORDER_BY_DESC);
                }
            }

            @Override
            public void onScroll(AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
                bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);
                if (Config.DEBUG) {
                    Log.d(TAG, "onScroll bReachedListEnd=" + bReachedListEnd + ", firstVisibleItem=" + firstVisibleItem
                            + ", visibleItemCount=" + visibleItemCount + ", totalItemCount=" + totalItemCount);
                }
            }
        });

        //resetList();
//        mAlbumListRequest.getAlbumListInServer(mUserGUID, mAlbumType, mPageNo, AlbumListRequest.SORT_BY_ALBUMNAME,
//                AlbumListRequest.ORDER_BY_DESC);


        super.onViewCreated(view, savedInstanceState);
    }

//	private void resetList() {
//		mAlbums.clear();
//		if (mPhotoVideoAdapter.hasCreator()) {
//			mAlbums.add(mCreatorAlbum);
//		}
//		mPageNo = Config.DEFAULT_PAGE_INDEX;
//        mPhotoVideoAdapter.setList(mAlbums);
//	}
}
