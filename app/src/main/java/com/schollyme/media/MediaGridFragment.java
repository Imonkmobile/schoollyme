package com.schollyme.media;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.luminous.pick.LuminousGallery.LuminousAction;
import com.luminous.pick.LuminousGalleryActivity;
import com.schollyme.R;
import com.schollyme.adapter.MediaAdapter;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.handler.DeleteLayout;
import com.schollyme.handler.DeleteLayout.DeleteLayoutListener;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.ErrorLayoutListener;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.handler.RefreshLayout;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewMedia;
import com.schollyme.model.NewYoutubeUrl;
import com.schollyme.model.Youtube;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AlbumCreateRequest;
import com.vinfotech.request.AlbumDeleteRequest;
import com.vinfotech.request.AlbumDetailRequest;
import com.vinfotech.request.AlbumListMediaRequest;
import com.vinfotech.request.AlbumListRequest;
import com.vinfotech.request.AlbumMediaDeleteRequest;
import com.vinfotech.request.LikerListRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.widget.QuickReturnGridView;

import java.util.ArrayList;
import java.util.List;

public class MediaGridFragment extends BaseFragment {

    private static final String TAG = MediaGridFragment.class.getSimpleName();
    private RelativeLayout mEmptyRl;
    private View loaderPb, loaderLl;
    private TextView mTitleTv, mPhotosCountTv, mAlbumDescTv;
    private SwipeRefreshLayout mPullToRefreshSrl;

    private AlbumCreateRequest mAlbumCreateRequest;
    private AlbumListMediaRequest mAlbumListMediaRequest;
    private AlbumMediaDeleteRequest mAlbumMediaDeleteRequest;
    private AlbumDeleteRequest mAlbumDeleteRequest;
    private MediaAdapter mMediaAdapter;
    private AlbumDetailRequest mAlbumDetailRequest;
    private ArrayList<AlbumMedia> mAlbumMedias = new ArrayList<AlbumMedia>();
    private int mPageNo = Config.DEFAULT_PAGE_INDEX;
    private boolean mFetchedAll = true;

    private RefreshLayout mRefreshLayout;
    private ErrorLayout mErrorLayout;
    private DeleteLayout mDeleteLayout;
    private Album mAlbum;
    private AlbumMedia creatorAlbumMedia = null;
    private LogedInUserModel mLogedInUserModel;
    private RelativeLayout mQuickReturnFooterView;
    private RelativeLayout mLoaderLayonBootom;

    private int mQuickReturnHeight;
    private static final int STATE_ONSCREEN = 0;
    private static final int STATE_OFFSCREEN = 1;
    private static final int STATE_RETURNING = 2;
    private int mState = STATE_ONSCREEN;
    private int mScrollY;
    private int mMinRawY = 0;

    private TranslateAnimation anim;
    private View mView = null;
    private Album mAlbumDetail;
    private Context mContext;
    private HeaderLayout mHeaderLayout;
    private boolean mIsMe = false;
    private String mUserGUID;

    // CalStartEndUtil.bounceView(mContext, mLikeTv);
    public static MediaGridFragment newInstance(String UserGUID, LogedInUserModel logedInUserModel,
                                                Album album, HeaderLayout headerLayout) {
        MediaGridFragment mediaGridFragment = new MediaGridFragment();
        mediaGridFragment.mAlbum = album;
        mediaGridFragment.mHeaderLayout = headerLayout;
        mediaGridFragment.mUserGUID = UserGUID;
        mediaGridFragment.mLogedInUserModel = logedInUserModel;
        mediaGridFragment.mIsMe = AlbumGridActivity.isMe(UserGUID, logedInUserModel);
        mediaGridFragment.creatorAlbumMedia = new AlbumMedia(true, album.AlbumType);
        return mediaGridFragment;
    }

    @Override
    public void onStart() {
        mAlbumListMediaRequest.setActivityStatus(true);
        mAlbumDetailRequest.setActivityStatus(true);
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mAlbumListMediaRequest.setActivityStatus(false);
        mAlbumDetailRequest.setActivityStatus(false);
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.media_grid_fragment, null);
        mContext = getActivity();
        return mView;
    }

    public void groupDetailService() {

        mAlbumDetailRequest = new AlbumDetailRequest(getActivity());
        mAlbumDetailRequest.setLoader(mLoaderLayonBootom);
        mAlbumDetailRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mAlbumDetail = (Album) data;
                    updateAlbum(mAlbumDetail);
                    setFooterValues(mAlbumDetail);
                    mPrivacyIV.setImageResource(mAlbumDetail.Visibility == AlbumCreateRequest
                            .VISIBILITY_PUBLIC ? R.drawable.ic_globe
                            : R.drawable.ic_lock_grey);
                    if (!mLogedInUserModel.getIsVisitScreen(getActivity(), LogedInUserModel.IsVisitAlbumDetail)
                            && mAlbumDetail.IsEditable == 1) {

                        DialogUtil.HintViewer(getActivity(),
                                mAlbum.AlbumType.equals(AlbumCreateRequest.ALBUMTYPE_PHOTO) ?
                                        getString(R.string.Long_press_on_picture)
                                        : getString(R.string.Long_press_on_video), R.drawable.icon_tap_hand);
                        mLogedInUserModel.setIsVisitScreen(getActivity(), LogedInUserModel.IsVisitAlbumDetail, true);
                    }
                } else {
                    // DialogUtil.showOkDialog(getActivity(), (String) data, "");
                    final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext, data.toString(),
                            getResources().getString(R.string.app_name),
                            new OnOkButtonListner() {
                                @Override
                                public void onOkBUtton() {
                                    ((Activity) mContext).finish();
                                }
                            });

                    builder.setCancelable(false);
                    builder.setOnKeyListener(new Dialog.OnKeyListener() {
                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                ((Activity) mContext).finish();
                                builder.dismiss();
                            }
                            return true;
                        }
                    });
                }
            }
        });
        if (mAlbum != null && !TextUtils.isEmpty(mAlbum.AlbumGUID)) {

            mAlbumDetailRequest.getAlbumDetailFromServer(mAlbum.AlbumGUID);
            mQuickReturnFooterView.setVisibility(!Config.isWallFolder(mAlbum.AlbumName) ? View.VISIBLE : View.GONE);
            mShareTv.setVisibility(mAlbum.Visibility == AlbumCreateRequest.VISIBILITY_TEAMMATES ? View.GONE : View.VISIBLE);
        } else {
            mQuickReturnFooterView.setVisibility(View.GONE);
        }

    }

    ;

    private void setFooterValues(Album nAlbumDetail1) {
        if (null != nAlbumDetail1) {
            mCommentsTv.setText(nAlbumDetail1.NoOfComments + " "
                    + (getResources().getString(1 == nAlbumDetail1.NoOfComments ? R.string.Comment : R.string.Comments)));
            mLikesTv.setText(nAlbumDetail1.NoOfLikes + " "
                    + (getResources().getString(1 == nAlbumDetail1.NoOfLikes ? R.string.Like : R.string.Likes)));
            mLikeTv.setSelected(nAlbumDetail1.IsLike == 0 ? false : true);
        }
    }

    private void loadAlbum() {

        mAlbumListMediaRequest = new AlbumListMediaRequest(getActivity());
        mAlbumListMediaRequest.setLoader(loaderPb);
        mAlbumListMediaRequest.setRequestListener(new RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);
                if (success) {
                    if (null != data) {
                        if (mPageNo == 1) {
                            //mAlbumMedias = new ArrayList<AlbumMedia>();
                        }
                        mAlbumMedias.addAll((List<AlbumMedia>) data);
                        mEmptyRl.setVisibility(mAlbumMedias.size() == 0 ? View.VISIBLE : View.INVISIBLE);
                        mFetchedAll = (mAlbumMedias.size() >= totalRecords);
                        mMediaAdapter.setList(mAlbumMedias);
                    }

                    mediaGv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {

                            mQuickReturnHeight = mQuickReturnFooterView.getHeight();
                            mediaGv.computeScrollY();
                        }
                    });

                } else if (null != data) {
                    if (getString(R.string.No_internet_connection).equalsIgnoreCase((String) data)) {
                        mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                                : (String) data), true, MsgType.Error);
                    } else {
                        final AlertDialog builder = DialogUtil.showOkDialogButtonLisnter(mContext,
                                data.toString(), getResources().getString(R.string.app_name),
                                new OnOkButtonListner() {
                                    @Override
                                    public void onOkBUtton() {
                                        ((Activity) mContext).finish();
                                    }
                                });
                        builder.setCancelable(false);
                        builder.setOnKeyListener(new Dialog.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                                if (keyCode == KeyEvent.KEYCODE_BACK) {
                                    ((Activity) mContext).finish();
                                    builder.dismiss();
                                }
                                return true;
                            }
                        });
                    }
                }
            }
        });
    }

    QuickReturnGridView mediaGv;
    ImageView mPrivacyIV;
    TextView mPrivacyTV;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mPullToRefreshSrl = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorSchemeColors(R.color.blue, R.color.red,
                R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetList();
                mPullToRefreshSrl.setRefreshing(true);
                mAlbumListMediaRequest.setLoader(null);
                if (null != mAlbum && !TextUtils.isEmpty(mAlbum.AlbumGUID)) {
                    mAlbumListMediaRequest.getAlbumMediaListFromServer(mUserGUID, mAlbum.AlbumGUID, mPageNo,
                            AlbumDetailRequest.SORT_BY_CREATEDDATE, AlbumDetailRequest.ORDER_BY_CREATEDDATE);
                }
            }
        });

        loaderLl = view.findViewById(R.id.loader_ll);
        // View bgView = loaderLl.findViewById(R.id.dummy_bg_v);

        loaderPb = view.findViewById(R.id.middle_pb);
        loaderPb.setVisibility(View.GONE);
        // bgView.setBackgroundColor(Color.RED);
        // bgView.setAlpha(0.4f);
        mLoaderLayonBootom = (RelativeLayout) view.findViewById(R.id.bottom_footer_loader_rl);

        mQuickReturnFooterView = (RelativeLayout) view.findViewById(R.id.bottom_footer_rl);

        mRefreshLayout = new RefreshLayout(getActivity());
        mErrorLayout = new ErrorLayout(view.findViewById(R.id.container_ll));
        mDeleteLayout = new DeleteLayout(view.findViewById(R.id.container_ll));
        mDeleteLayout.setDeleteLayoutListener(new DeleteLayoutListener() {

            @Override
            public void onDeleteClicked() {
                deleteSelectedMedia();
            }
        });
        loadAlbum();


        mAlbumCreateRequest = new AlbumCreateRequest(getActivity());
        mAlbumCreateRequest.setLoader(loaderLl);
        mAlbumCreateRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, final Object data, int totalRecords) {
                if (success) {
                    getActivity().setResult(Activity.RESULT_OK);
                    AlbumGridActivity.setReloadData(true);
                    if (TextUtils.isEmpty(mAlbum.AlbumGUID)) {
                        mErrorLayout.showError(getString(R.string.Album_created_successfully), true, MsgType.Success);
                        mAlbum = (Album) data;
                    } else {
                        mErrorLayout.showError(getString(R.string.Album_updated_successfully), true, MsgType.Success);
                    }
                    groupDetailService();
                    mAlbumListMediaRequest.getAlbumMediaListFromServer(mUserGUID, mAlbum.AlbumGUID, mPageNo,
                            AlbumDetailRequest.SORT_BY_CREATEDDATE, AlbumDetailRequest.ORDER_BY_CREATEDDATE);
                } else if (null != data) {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
                }
            }
        });
        mAlbumDeleteRequest = new AlbumDeleteRequest(getActivity());
        mAlbumDeleteRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    getActivity().setResult(Activity.RESULT_OK);
                    AlbumGridActivity.setReloadData(true);
                    mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {

                        @Override
                        public void onErrorShown() {
                        }

                        @Override
                        public void onErrorHidden() {
                            if (null == getActivity()) {
                                return;
                            }
                            getActivity().setResult(Activity.RESULT_OK);
                            AlbumGridActivity.setReloadData(true);
                            getActivity().finish();
                        }
                    });
                    mErrorLayout.showError(getString(R.string.Album_deleted_successfully), true, MsgType.Success);
                } else if (null != data) {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true, MsgType.Error);
                }
            }
        });

        mMediaAdapter = new MediaAdapter(getActivity(), mAlbum, mIsMe && !Config.isWallFolder(mAlbum.AlbumName));

        mTitleTv = (TextView) view.findViewById(R.id.album_title_tv);
        mPhotosCountTv = (TextView) view.findViewById(R.id.photos_count_tv);
        mAlbumDescTv = (TextView) view.findViewById(R.id.album_desc_tv);
        mPrivacyIV = (ImageView) view.findViewById(R.id.globe_iv);
        mPrivacyTV = (TextView) view.findViewById(R.id.globe_tv);
        mPrivacyIV.setClickable(false);

        try {
            mPrivacyIV.setImageResource(mAlbum.Visibility == AlbumCreateRequest.VISIBILITY_PUBLIC ? R.drawable.ic_globe
                    : R.drawable.ic_lock_grey);
            mPrivacyTV.setVisibility(View.GONE);
            /*mPrivacyTV.setText(mAlbum.Visibility == AlbumCreateRequest.VISIBILITY_PUBLIC ? R.string.Public
                    : R.string.Teammates);*/
        } catch (Exception e) {
            // TODO: handle exception
        }

        // privacyView.setOnClickListener(mPrivacyClickListener);
        mPrivacyIV.setVisibility(mIsMe && !Config.isWallFolder(mAlbum.AlbumName) ? View.VISIBLE : View.INVISIBLE);

        mediaGv = (QuickReturnGridView) view.findViewById(R.id.media_gv);
        mediaGv.setAdapter(mMediaAdapter);
        InitFooterValues();
        mediaGv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (Config.DEBUG) {
                    Log.d(TAG, "onItemClick position=" + position + ", url=" + mMediaAdapter.getItem(position));
                }

                AlbumMedia media = mMediaAdapter.getItem(position);
                if (mMediaAdapter.isMultiSelectionMode()) {
                    if (position > 0 && !Config.isWallFolder(mAlbum.AlbumName)) {
                        media.isSelected = !media.isSelected;
                        mMediaAdapter.notifyDataSetChanged();
                        fetchSelections();
                    }
                    return;
                }

                if (position == 0 && mMediaAdapter.hasCreator()) {
                    if (!Config.isWallFolder(mAlbum.AlbumName)) {
                        onPlusClicked();
                    }
                } else {
                    ArrayList<AlbumMedia> albumMedias = new ArrayList<AlbumMedia>(mAlbumMedias);
                    if (mMediaAdapter.hasCreator()) {
                        albumMedias.remove(0);
                    }
                    startActivityForResult(MediaViewerActivity.getIntent(getActivity(), albumMedias/*mAlbumMedias*/, albumMedias.indexOf(media), mAlbum, (mIsMe ? 1 : 0)),
                            MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        });
        mediaGv.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                if (position != 0 && mMediaAdapter.hasCreator() && !Config.isWallFolder(mAlbum.AlbumName)) {
                    mMediaAdapter.setMode(true);
                    AlbumMedia media = mMediaAdapter.getItem(position);
                    media.isSelected = true;
                    mMediaAdapter.notifyDataSetChanged();
                    fetchSelections();

                    Vibrator v = (Vibrator) getActivity().getSystemService(getActivity().VIBRATOR_SERVICE);
                    v.vibrate(1000);
                    return true;
                }
                return false;
            }
        });

        mediaGv.setOnScrollListener(new OnScrollListener() {

            private boolean bReachedListEnd;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (Config.DEBUG) {
                    Log.d(TAG, "onScrollStateChanged bReachedListEnd=" + bReachedListEnd + ", mFetchedAll=" + mFetchedAll
                            + ", scrollState=" + scrollState);
                }
                if (bReachedListEnd && !mFetchedAll) {
                    mFetchedAll = true;
                    mPageNo++;

                    if (mPageNo == 1) {
                        mAlbumListMediaRequest.setLoader(loaderLl);
                    } else {
                        mAlbumListMediaRequest.setLoader(loaderLl);
                    }
                    mAlbumListMediaRequest.getAlbumMediaListFromServer(mUserGUID, mAlbum.AlbumGUID, mPageNo,
                            AlbumDetailRequest.SORT_BY_CREATEDDATE, AlbumDetailRequest.ORDER_BY_CREATEDDATE);
                }
            }

            @Override
            public void onScroll(AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
                bReachedListEnd = ((firstVisibleItem + visibleItemCount) == totalItemCount);

                mScrollY = 0;
                int translationY = 0;

                if (mediaGv.scrollYIsComputed()) {
                    mScrollY = mediaGv.getComputedScrollY();
                }

                int rawY = mScrollY;

                switch (mState) {
                    case STATE_OFFSCREEN:
                        if (rawY >= mMinRawY) {
                            mMinRawY = rawY;
                        } else {
                            mState = STATE_RETURNING;
                        }
                        translationY = rawY;

                        break;

                    case STATE_ONSCREEN:
                        if (rawY > mQuickReturnHeight) {
                            mState = STATE_OFFSCREEN;
                            mMinRawY = rawY;
                        }
                        translationY = rawY;

                        break;

                    case STATE_RETURNING:

                        translationY = (rawY - mMinRawY) + mQuickReturnHeight;
                        // translationYH= (rawY - mMinRawY) +
                        // mQuickReturnHeightHeader;
                        System.out.println(translationY);
                        if (translationY < 0) {
                            translationY = 0;
                            mMinRawY = rawY + mQuickReturnHeight;

                        }

                        if (rawY == 0) {
                            mState = STATE_ONSCREEN;
                            translationY = 0;
                        }

                        if (translationY > mQuickReturnHeight) {
                            mState = STATE_OFFSCREEN;
                            mMinRawY = rawY;
                        }
                        break;
                }

                /** this can be used if the build is below honeycomb **/
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
                    anim = new TranslateAnimation(0, 0, translationY, translationY);
                    anim.setFillAfter(true);
                    anim.setDuration(0);
                    mQuickReturnFooterView.startAnimation(anim);
                    // mQuickReturnHeaderView.startAnimation(anim);
                } else {
                    mQuickReturnFooterView.setTranslationY(translationY);
                    // mQuickReturnHeaderView.setTranslationY(translationY);

                }

            }
        });
        mEmptyRl = (RelativeLayout) view.findViewById(R.id.empty_rl);
        resetList();
        updateAlbum(mAlbum);
        if (null != mAlbum)
            if (!TextUtils.isEmpty(mAlbum.AlbumGUID)) {
                mAlbumListMediaRequest.getAlbumMediaListFromServer(mUserGUID, mAlbum.AlbumGUID, mPageNo,
                        AlbumDetailRequest.SORT_BY_CREATEDDATE, AlbumDetailRequest.ORDER_BY_CREATEDDATE);
            }

        if (mAlbum != null) {
            if (TextUtils.isEmpty(mAlbum.AlbumGUID)) {
                mPullToRefreshSrl.setEnabled(false);
            }
        }

        groupDetailService();
        FontLoader.setRobotoRegularTypeface(mTitleTv, mPhotosCountTv, mAlbumDescTv);
        super.onViewCreated(view, savedInstanceState);
    }

    final static int REQ_CODE_FOR_COMMENT_LIST = 9638;
    private TextView mDescriptionTv, mLikesTv, mCommentsTv, mLikeTv, mCommentTv, mShareTv;

    private void InitFooterValues() {

        mLikesTv = (TextView) mQuickReturnFooterView.findViewById(R.id.likes_tv);
        mCommentsTv = (TextView) mQuickReturnFooterView.findViewById(R.id.comments_tv);
        mLikeTv = (TextView) mQuickReturnFooterView.findViewById(R.id.like_tv);
        mCommentTv = (TextView) mQuickReturnFooterView.findViewById(R.id.comment_tv);
        mShareTv = (TextView) mQuickReturnFooterView.findViewById(R.id.share_tv);
        FontLoader.setRobotoRegularTypeface(mLikesTv, mCommentsTv, mLikeTv, mCommentTv, mShareTv);

        mCommentsTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null == mAlbumDetail) {
                    return;
                }
                startActivityForResult(CommentListActivity.getIntent(getActivity(), mAlbumDetail, ""), REQ_CODE_FOR_COMMENT_LIST);
                getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

            }
        });

        mCommentTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null == mAlbumDetail) {
                    return;
                }
                startActivityForResult(WriteCommentActivity.getIntent(getActivity(), mAlbumDetail, mAlbum.AlbumName),
                        WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY);
                getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
            }
        });

        mLikesTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null == mAlbumDetail) {
                    return;
                }
                if (mAlbumDetail.NoOfLikes != 0) {
                    startActivityForResult(LikeListActivity.getIntent(mContext, mAlbumDetail.ActivityGUID,
                            LikerListRequest.ENTITYTYPE_ACTIVITY, "MEDIAGRID"), LikeListActivity.REQ_CODE_LIKE_ACTIVITY_WALL);
                    ((Activity) mContext).overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
                }

            }
        });
        mLikeTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null == mAlbumDetail) {
                    return;
                }
                DisplayUtil.bounceView(mContext, mLikeTv);
                LikeMediaToggleService(mAlbumDetail);
            }
        });

        mShareTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null == mAlbumDetail) {
                    return;
                }
                String mediaUrl = "";
                if (AlbumListRequest.ALBUM_TYPE_PHOTO.equalsIgnoreCase(mAlbum.AlbumType)) {
                    mediaUrl = Config.IMAGE_URL_UPLOAD + Config.ALBUM_FOLDER + Config.THUMB_FOLDER + mAlbum.CoverMedia;
                } else if (AlbumListRequest.ALBUM_TYPE_VIDEO.equalsIgnoreCase(mAlbum.AlbumType)) {

                    if (mAlbum.CoverMedia.startsWith("http")) {
                        mediaUrl = MediaViewerActivity.getYoutubeVideoThumb(mAlbum.CoverMedia);
                    } else {
                        mediaUrl = Config.getCoverPath(mAlbum, Config.getVideoToJPG(mAlbum.CoverMedia), true);
                    }
                }

                DialogUtil.showOkCancelDialog(v.getContext(), R.string.ok_label, R.string.cancel_caps,
                        "", v.getContext().getString(R.string.Are_share_this_album),
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sharePost(mAlbumDetail.ActivityGUID, SharePostRequest.ENTITYTYPE_ACTIVITY,
                                        mLogedInUserModel.mUserGUID);
                            }
                        }, null);

                // startActivity(SharePostActivity.getIntent(v.getContext(),
                // mAlbumDetail.ActivityGUID,
                // SharePostRequest.ENTITYTYPE_ACTIVITY,
                // mLogedInUserModel.mUserGUID, mediaUrl, mAlbum.Description));
            }
        });
    }

    private int mVisibility = WallPostCreateRequest.VISIBILITY_PUBLIC;
    private int mCommentable = WallPostCreateRequest.COMMENT_ON;
    private SharePostRequest mSharePostRequest;

    public void sharePost(String mEntityGUID, String mEntityType, String mModuleEntityGUID) {
        mSharePostRequest = new SharePostRequest(getActivity());
        mSharePostRequest.sharePostInServer(mEntityGUID, mEntityType, "", SharePostRequest.MODULE_ID_USERS, mModuleEntityGUID, mVisibility,
                mCommentable);

        mSharePostRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, final Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {

                        @Override
                        public void onErrorShown() {
                        }

                        @Override
                        public void onErrorHidden() {

                        }
                    });
                    mErrorLayout.showError(getActivity().getResources().getString(R.string.Album_shared_successfully), true,
                            MsgType.Success);
                } else {
                    mHeaderLayout.mRightIb1.setEnabled(true);
                    mErrorLayout.showError((String) data, true, MsgType.Error);
                }
            }
        });
    }

    public void LikeMediaToggleService(final Album mMedia) {

        ToggleLikeRequest mToggleLikeRequest = new ToggleLikeRequest(getActivity());
        mToggleLikeRequest.toggleLikeInServer(mMedia.ActivityGUID, ToggleLikeRequest.ENTITY_TYPE_ACTIVITY);

        mToggleLikeRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {

                    if (mMedia.IsLike == 0) {

                        mMedia.IsLike = 1;
                        mMedia.NoOfLikes++;
                        // Broadcast Here

                    } else {
                        mMedia.IsLike = 0;
                        mMedia.NoOfLikes--;
                    }

                    setFooterValues(mAlbumDetail);

                } else {

                    DialogUtil.showOkDialog(getActivity(), data.toString(), "");
                }

            }
        });
    }

    private OnClickListener mPrivacyClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            onBackPressedOn();
            DialogUtil.showListDialog(v.getContext(), R.string.Album_Visibility, new OnItemClickListener() {

                @Override
                public void onItemClick(int position, String item) {
                    if (position == 0) {
                        mAlbum.Visibility = AlbumCreateRequest.VISIBILITY_PUBLIC;
                    } else if (position == 1) {
                        mAlbum.Visibility = AlbumCreateRequest.VISIBILITY_TEAMMATES;
                    }
                    mMediaAdapter.setAlbum(mAlbum);
                    if (!TextUtils.isEmpty(mAlbum.AlbumGUID)) {// new
                        // album
                        resetList();
                        mAlbumCreateRequest.createAlbumInServer(mAlbum.AlbumGUID, mAlbum.AlbumName, mAlbum.Description, mAlbum.Visibility,
                                mAlbum.AlbumType, null, null);
                    }
                }

                @Override
                public void onCancel() {

                }
            }, getString(R.string.Public), getString(R.string.Teammates));
        }
    };

    public void onPlusClicked() {
        if (AlbumCreateRequest.ALBUMTYPE_PHOTO.equalsIgnoreCase(mAlbum.AlbumType)) {
            startActivityForResult(LuminousGalleryActivity.getIntent(LuminousAction.ACTION_MULTIPLE_PICK),
                    LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY);
        } else {
            DialogUtil.showListDialog(getActivity(), R.string.Add_Video, new OnItemClickListener() {
                @Override
                public void onItemClick(int position, String item) {
                    switch (position) {
                        case 0:
                            startActivityForResult(LuminousGalleryActivity.getIntentVideo(LuminousAction.ACTION_PICK, "Wall", false),
                                    LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY);
                            break;
                        case 1:
                            startActivityForResult(UploadYoutubeActivity.getIntent(getActivity()),
                                    UploadYoutubeActivity.REQ_CODE_UPLOAD_YOUTUBE_ACTIVITY);
                            break;

                        default:
                            break;
                    }
                }

                @Override
                public void onCancel() {
                }
            }, getString(R.string.Upload_from_Device), getString(R.string.Add_Youtube_Video));
        }
    }

    public void onActionClicked(final View v) {
        String[] options = null;
        if (TextUtils.isEmpty(mAlbum.AlbumGUID)) {
            options = new String[]{getString(AlbumCreateRequest.ALBUMTYPE_PHOTO.equalsIgnoreCase(mAlbum.AlbumType) ? R.string.Add_Photos
                    : R.string.Add_Video)};
        } else {
            options = new String[]{
                    getString(AlbumCreateRequest.ALBUMTYPE_PHOTO.equalsIgnoreCase(mAlbum.AlbumType) ? R.string.Add_Photos
                            : R.string.Add_Video), getString(R.string.Edit_Album), getString(R.string.Delete_Album)};
        }
        DialogUtil.showListDialog(v.getContext(), 0, new OnItemClickListener() {

            @Override
            public void onItemClick(int position, String item) {
                switch (position) {
                    case 0:
                        onPlusClicked();
                        break;
                    case 1:
                        startActivityForResult(CreateAlbumActivity.getIntent(v.getContext(), mAlbumDetail),
                                CreateAlbumActivity.REQ_CODE_CREATE_ALBUM_ACTIVITY);
                        break;
                    case 2:
                        DialogUtil.showOkCancelDialog(v.getContext(), R.string.Delete, R.string.ok_label,
                                R.string.cancel_caps, getString(R.string.Delete_Album),
                                getString(R.string.Confirm_delete_Album), new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mAlbumDeleteRequest.deleteAlbumFromServer(mAlbum.AlbumGUID);
                                    }
                                }, null);
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onCancel() {
            }
        }, options);
    }

    private void resetList() {
        mAlbumMedias.clear();
        if (mMediaAdapter.hasCreator()) {
            mAlbumMedias.add(creatorAlbumMedia);
        }
        mPageNo = Config.DEFAULT_PAGE_INDEX;
        mMediaAdapter.setList(mAlbumMedias);
    }

    private void updateAlbum(Album album) {
        if (null != album) {
            mTitleTv.setText(album.AlbumName);
            mAlbumDescTv.setText(album.Description);
            mShareTv.setVisibility(album.Visibility == AlbumCreateRequest.VISIBILITY_TEAMMATES ? View.GONE : View.VISIBLE);
            if (null != mHeaderLayout && null != mHeaderLayout.mTitleTv) {
                mHeaderLayout.mTitleTv.setText(album.AlbumName);
            }

            boolean isPhotoAlbum = AlbumCreateRequest.ALBUMTYPE_PHOTO.equalsIgnoreCase(album.AlbumType)
                    || AlbumCreateRequest.ALBUMTYPE_IMAGE.equalsIgnoreCase(album.AlbumType);
            if (album.MediaCount == 1) {
                mPhotosCountTv.setText(album.MediaCount + " " + getString(isPhotoAlbum ? R.string.photo_label : R.string.video_label));
            } else {
                mPhotosCountTv.setText(album.MediaCount + " " + getString(isPhotoAlbum ? R.string.photos_label : R.string.videos_label));
            }
            if (album.MediaCount == 0) {
                mMediaAdapter.clearList();
                mHeaderLayout.setHeaderValues(R.drawable.icon_back, mAlbum.AlbumName, 0);

                mRefreshLayout.show(mEmptyRl, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        onPlusClicked();
                    }
                }, isPhotoAlbum ? R.drawable.ic_no_photos : R.drawable.ic_no_video, isPhotoAlbum ? R.string.There_are_no_photos
                        : R.string.There_are_no_videos, Config.isWallFolder(mAlbum.AlbumName) ? 0 : (isPhotoAlbum ? R.string.Add_Photos
                        : R.string.Add_Video), R.color.red_normal_color);
            } else {
                mRefreshLayout.hide(mEmptyRl);
            }
        }
    }

    private List<AlbumMedia> mSelAlbumMedias = null;

    public void fetchSelections() {
        mSelAlbumMedias = mMediaAdapter.getSelections();
        if (mSelAlbumMedias.size() > 0) {
            mDeleteLayout.showDelete();
        } else {
            mDeleteLayout.hideDelete();
            mMediaAdapter.setMode(false);
        }
    }

    private boolean mDelAllMedia = false;

    private void deleteSelectedMedia() {
        if (null != mSelAlbumMedias) {
            mDelAllMedia = (mSelAlbumMedias.size() == mMediaAdapter.getCount() - 1);
            boolean isMultiSel = mSelAlbumMedias.size() > 1;
            boolean isPhotoAlbum = AlbumCreateRequest.ALBUMTYPE_PHOTO.equalsIgnoreCase(mAlbum.AlbumType);
            String delTtl = null, delMsg = null;
            if (isMultiSel) {
                delTtl = getString(isPhotoAlbum ? R.string.Delete_Photos : R.string.Delete_Videos);
                delMsg = String.format(getString(isPhotoAlbum ? R.string.Confirm_delete_Photos : R.string.Confirm_delete_Videos),
                        mSelAlbumMedias.size());
            } else {
                delTtl = getString(isPhotoAlbum ? R.string.Delete_Photo : R.string.Delete_Video);
                delMsg = getString(isPhotoAlbum ? R.string.Confirm_delete_Photo : R.string.Confirm_delete_Video);
            }
            DialogUtil.showOkCancelDialog(getActivity(), R.string.Delete, R.string.ok_label,
                    R.string.cancel_caps, delTtl, delMsg, new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            resetList();
                            if (null == mAlbumMediaDeleteRequest) {
                                mAlbumMediaDeleteRequest = new AlbumMediaDeleteRequest(getActivity());
                                mAlbumMediaDeleteRequest.setRequestListener(new RequestListener() {

                                    @Override
                                    public void onComplete(boolean success, final Object data, int totalRecords) {
                                        if (success) {
                                            AlbumGridActivity.setReloadData(true);
                                            getActivity().setResult(Activity.RESULT_OK);
                                            AlbumGridActivity.setReloadData(true);
                                            mErrorLayout.showError(getString(R.string.Media_deleted_successfully), true, MsgType.Success);

                                            if (mDelAllMedia) {
                                                DialogUtil.showOkDialogNonCancelable(getActivity(), getString(R.string.Album_is_deleted), "",
                                                        new OnOkButtonListner() {

                                                            @Override
                                                            public void onOkBUtton() {
                                                                getActivity().finish();
                                                            }
                                                        });
                                            } else {
                                                groupDetailService();
                                                mAlbumListMediaRequest.getAlbumMediaListFromServer(mUserGUID, mAlbum.AlbumGUID, mPageNo,
                                                        AlbumDetailRequest.SORT_BY_CREATEDDATE, AlbumDetailRequest.ORDER_BY_CREATEDDATE);
                                            }
                                        } else if (null != data) {
                                            mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong) : (String) data), true,
                                                    MsgType.Error);
                                        }
                                    }
                                });
                            }
                            mAlbumMediaDeleteRequest.deleteMediaFromServer(mAlbum.AlbumGUID, mSelAlbumMedias);
                            mDeleteLayout.hideDelete();
                        }
                    }, null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Config.DEBUG) {
            Log.d(TAG, "onActivityResult requestCode =" + requestCode + ", resultCode=" + resultCode);
        }
        mAlbumListMediaRequest.setActivityStatus(true);
        mAlbumDetailRequest.setActivityStatus(true);
        switch (requestCode) {
            case LuminousGalleryActivity.REQ_CODE_LUMINOUS_GALLERY_ACTIVITY:
                if (Activity.RESULT_OK == resultCode) {
                    boolean selTypeImg = data.getBooleanExtra("sel_type_img", true);
                    if (selTypeImg) {
                        final String[] imagePaths = data.getStringArrayExtra("data");
                        startActivityForResult(UploadMediaActivity.getIntent(getActivity(), mAlbum.AlbumName, imagePaths),
                                UploadMediaActivity.REQ_CODE_UPLOAD_MEDIA_ACTIVITY);
                    } else {
                        final String videoPath = data.getStringExtra("data");
                        startActivityForResult(UploadVideoActivity.getIntent(getActivity(), mAlbum.AlbumName, videoPath),
                                UploadVideoActivity.REQ_CODE_UPLOAD_VIDEO_ACTIVITY);
                    }
                }
                break;
            case UploadYoutubeActivity.REQ_CODE_UPLOAD_YOUTUBE_ACTIVITY:
                if (Activity.RESULT_OK == resultCode) {
                    AlbumGridActivity.setReloadData(true);
                    resetList();
                    ArrayList<NewYoutubeUrl> newYoutubeUrls = data.getParcelableArrayListExtra("newYoutubeUrls");
                    Youtube youtube = data.getParcelableExtra("youtube");
                    if (TextUtils.isEmpty(mAlbum.AlbumGUID)) {
                        mAlbumCreateRequest.createAlbumInServer(null, mAlbum.AlbumName, mAlbum.Description, mAlbum.Visibility,
                                mAlbum.AlbumType, null, newYoutubeUrls);
                    } else {
                        mAlbumCreateRequest.addMediaInServer(mAlbum.AlbumGUID, mAlbum.AlbumName, mAlbum.Description, mAlbum.Visibility,
                                mAlbum.AlbumType, null, newYoutubeUrls);
                    }
                    if (Config.DEBUG) {
                        Log.d(TAG, "onActivityResult newYoutubeUrls =" + newYoutubeUrls + ", size=" + (null == newYoutubeUrls ? 0 : 1));
                    }
                    if (mPageNo == 1) {
                        mAlbumListMediaRequest.setLoader(loaderLl);
                    } else {
                        mAlbumListMediaRequest.setLoader(mLoaderLayonBootom);
                    }
                }
                break;
            case UploadVideoActivity.REQ_CODE_UPLOAD_VIDEO_ACTIVITY:
            case UploadMediaActivity.REQ_CODE_UPLOAD_MEDIA_ACTIVITY:
                if (Activity.RESULT_OK == resultCode) {
                    AlbumGridActivity.setReloadData(true);
                    resetList();
                    ArrayList<NewMedia> newMedias = data.getParcelableArrayListExtra("newMeidas");
                    if (TextUtils.isEmpty(mAlbum.AlbumGUID)) {
                        mAlbumCreateRequest.createAlbumInServer(null, mAlbum.AlbumName, mAlbum.Description, mAlbum.Visibility,
                                mAlbum.AlbumType, newMedias, null);
                    } else {
                        mAlbumCreateRequest.addMediaInServer(mAlbum.AlbumGUID, mAlbum.AlbumName, mAlbum.Description, mAlbum.Visibility,
                                mAlbum.AlbumType, newMedias, null);
                    }
                    if (Config.DEBUG) {
                        Log.d(TAG, "onActivityResult newMeidas =" + newMedias + ", size=" + (null == newMedias ? 0 : 1));
                    }
                    if (mPageNo == 1) {
                        mAlbumListMediaRequest.setLoader(loaderLl);
                    } else {
                        mAlbumListMediaRequest.setLoader(mLoaderLayonBootom);
                    }
                }
                break;
            case MediaViewerActivity.REQ_CODE_MEDIA_VIEWER_ACTIVITY:

                if (Activity.RESULT_OK == resultCode) {

                    if (data.getExtras().containsKey("AlbumMedia")) {
                        AlbumMedia media = data.getParcelableExtra("AlbumMedia");
                        if (null != media) {
                            for (int i = 1; i < mAlbumMedias.size(); i++) {
                                if (mAlbumMedias.get(i).MediaGUID.equals(media.MediaGUID)) {
                                    mAlbumMedias.set(i, media);
                                }
                            }
                            mMediaAdapter.setList(mAlbumMedias);
                        }
                    }

                    if (data.getExtras().containsKey("DELETEDMEDIA")) {
                        String mRemoveIds = data.getStringExtra("DELETEDMEDIA");
                        if (!mRemoveIds.equals("")) {

                            for (int i = 1; i < mAlbumMedias.size(); i++) {
                                if (mAlbumMedias.get(i).MediaGUID.equals(mRemoveIds)) {
                                    mAlbumMedias.remove(i);
                                }
                            }
                            mMediaAdapter.setList(mAlbumMedias);
                        } else {
                            getActivity().finish();
                            onBackPressedOn();
                        }
                    }

                    getActivity().setResult(Activity.RESULT_OK);
                }
            case CreateAlbumActivity.REQ_CODE_CREATE_ALBUM_ACTIVITY:
                if (Activity.RESULT_OK == resultCode) {
                    getActivity().setResult(Activity.RESULT_OK);
                    AlbumGridActivity.setReloadData(true);
                    mAlbumDetailRequest.getAlbumDetailFromServer(mAlbum.AlbumGUID);
                    groupDetailService();

                }
                break;
            case REQ_CODE_FOR_COMMENT_LIST:
                if (Activity.RESULT_OK == resultCode) {
                    Album media = data.getParcelableExtra("Album");
                    if (null != media) {
                        mAlbumDetail.NoOfComments = media.NoOfComments;
                        setFooterValues(mAlbumDetail);
                    }
                    groupDetailService();
                }
                break;
            case LikeListActivity.REQ_CODE_LIKE_ACTIVITY_WALL:
                if (Activity.RESULT_OK == resultCode) {
                    groupDetailService();
                }
                break;
            case WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY:
                if (Activity.RESULT_OK == resultCode) {
                    mAlbumDetail.NoOfComments = mAlbumDetail.NoOfComments + 1;
                    setFooterValues(mAlbumDetail);
                }
                break;

            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onBackPressedOn() {
        if (TextUtils.isEmpty(mAlbum.AlbumGUID)) {
            startActivity(CreateAlbumActivity.getIntent(getActivity(), mAlbum));
            getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_left_exit);
            getActivity().finish();
            return false;
        }
        boolean multiSelMode = mMediaAdapter.isMultiSelectionMode();
        if (multiSelMode) {
            mMediaAdapter.setMode(false);
            mDeleteLayout.hideDelete();

        }
        return multiSelMode;
    }
}
