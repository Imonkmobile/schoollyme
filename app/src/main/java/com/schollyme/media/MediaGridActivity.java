package com.schollyme.media;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.Album;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AlbumDetailRequest;
import com.vinfotech.server.BaseRequest.RequestListener;

public class MediaGridActivity extends BaseActivity implements OnClickListener {
	public static final int REQ_CODE_MEDIA_GRID_ACTIVITY = 1023;
	private MediaMainHolder mMediaMainHolder;

	private MediaGridFragment mMediaGridFragment;
	private String mUserGUID = "";
	private Album mAlbum;
	private LogedInUserModel mLogedInUserModel;
	private boolean mFromQuery = false;
	private String mAlbumGUID;
	private AlbumDetailRequest mAlbumDetailRequest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.media_grid_activity);

		mLogedInUserModel = new LogedInUserModel(this);
		mUserGUID = getIntent().getStringExtra("UserGUID");
		mAlbum = getIntent().getParcelableExtra("album");
		Uri uri = getIntent().getData();

		mFromQuery = !(null == uri || TextUtils.isEmpty(uri.getQueryParameter("FromQuery")));
		if (Config.DEBUG) {
			Log.d("CreateAlbumActivity", "onCreate mAlbum=" + mAlbum + ", mFromQuery=" + mFromQuery);
		}
		if (null == mAlbum && !mFromQuery) {
			Toast.makeText(this, "Invalid album type.", Toast.LENGTH_SHORT).show();
			finish();
			return;
		} else if (mFromQuery) {
			mUserGUID = uri.getQueryParameter("UserGUID");
			mAlbumGUID = uri.getQueryParameter("AlbumGUID");

			mAlbumDetailRequest = new AlbumDetailRequest(this);
			mAlbumDetailRequest.setRequestListener(new RequestListener() {

				@Override
				public void onComplete(boolean success, Object data, int totalRecords) {
					if (success) {
						mAlbum = (Album) data;
						mMediaMainHolder = new MediaMainHolder(findViewById(R.id.main_container_rl), MediaGridActivity.this);
					}
				}
			});
			mAlbumDetailRequest.getAlbumDetailFromServer(mAlbumGUID);
		} else {
			mMediaMainHolder = new MediaMainHolder(findViewById(R.id.main_container_rl), MediaGridActivity.this);
		}

		
	}

	public static final String getQueryParams(String UserGUID, String AlbumGUID) {
		return "FromQuery=FromQuery&UserGUID=" + UserGUID + "&AlbumGUID=" + AlbumGUID;
	}

	public static Intent getIntent(Context context, String UserGUID, Album album) {
		Intent intent = new Intent(context, MediaGridActivity.class);
		intent.putExtra("UserGUID", UserGUID);
		intent.putExtra("album", album);
		return intent;
	}

	class MediaMainHolder {
		private HeaderLayout mHeaderLayout;

		public MediaMainHolder(View view, OnClickListener listener) {
			mHeaderLayout = new HeaderLayout(view.findViewById(R.id.header_layout));

			if (AlbumGridActivity.isMe(mUserGUID, mLogedInUserModel)) {
				mHeaderLayout.setHeaderValues(R.drawable.icon_back, mAlbum.AlbumName,
						Config.isWallFolder(mAlbum.AlbumName) ? 0 : R.drawable.icon_dot_menu_header);
				mHeaderLayout.setListenerItI(MediaGridActivity.this, MediaGridActivity.this);
			} else {
				mHeaderLayout.setHeaderValues(R.drawable.icon_back, mAlbum.AlbumName, 0);
				mHeaderLayout.setListenerItI(MediaGridActivity.this, null);
			}

			mMediaGridFragment = MediaGridFragment.newInstance(mUserGUID, mLogedInUserModel, mAlbum, mHeaderLayout);
			getSupportFragmentManager().beginTransaction().add(R.id.container_review, mMediaGridFragment).commit();

		}
	}

	public HeaderLayout getHeaderLayout() {
		return mMediaMainHolder.mHeaderLayout;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_button:
			if (!mMediaGridFragment.onBackPressedOn()) {
				this.finish();
			}
			break;

		case R.id.right_button:
			mMediaGridFragment.onActionClicked(v);
			break;

		default:
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (null != mMediaGridFragment) {
				if (!mMediaGridFragment.onBackPressedOn()) {
					this.finish();
					return true;
				}
				return false;
			}
		}

		return super.onKeyDown(keyCode, event);
	}
}
