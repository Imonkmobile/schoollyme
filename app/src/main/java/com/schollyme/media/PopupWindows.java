package com.schollyme.media;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.schollyme.R;
import com.vinfotech.request.LikerListRequest;
import com.vinfotech.server.BaseRequest.RequestListener;

public class PopupWindows {
	public static PopupWindow getLikePopupWindow(Context context, String EntityGUID, String EntityType) {
		LikerListRequest likerListRequest = new LikerListRequest(context);

		View view = LayoutInflater.from(context).inflate(R.layout.likes_activity, null);
		View loaderView = view.findViewById(R.id.loading_pb);

		ListView genericLv = (ListView) view.findViewById(R.id.generic_lv);

		PopupWindow popupWindow = new PopupWindow(view, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		popupWindow.setContentView(view);

		likerListRequest.setLoader(loaderView);
		likerListRequest.setRequestListener(new RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {

			}
		});
		likerListRequest.getLikerListInServer(EntityGUID, EntityType);

		return popupWindow;
	}
}
