package com.schollyme.media;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.adapter.KiltdMediaAdapter;
import com.schollyme.adapter.PostFeedAdapter;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.fragments.HomeFragment;
import com.schollyme.friends.FriendProfileActivity;
import com.schollyme.googleapi.GooglePlaceRequest;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.KiltdFeedRequest;
import com.vinfotech.request.NewsFeedRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.FontLoader;

import java.util.ArrayList;
import java.util.List;

public class KiltdMediaFragment extends BaseFragment {

    private View mView;
    private Context mContext;
    private KiltdMediaAdapter mKiltdAdapter;
    private SwipeRefreshLayout mPullToRefreshSrl;
    private List<NewsFeed> mNewsFeeds = new ArrayList<>();
    public LogedInUserModel mLogedInUserModel;
    private String mActivityGUID = "", mTeamGUID;
    private ErrorLayout mErrorLayout;
    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn;

    final private int REQ_FROM_HOME_LIKELIST = 5555;
    final private int REQ_FROM_HOME_COMMENTLIST = 5556;
    final private int REQ_FROM_HOME_WRITE_COMMENT = 58554;
    private KiltdFeedRequest mNewsFeedRequest;
    private NFFilter mNFFilter;
    private ListView videosListView;
    private int mNotificationType = 0;

    public static KiltdMediaFragment getInstance(int ModuleID, String EntityGUID, HeaderLayout headerLayout, boolean showBackBtn) {
        KiltdMediaFragment kiltdMediaFragment = new KiltdMediaFragment();
        kiltdMediaFragment.mHeaderLayout = headerLayout;
        kiltdMediaFragment.mShowBackBtn = showBackBtn;
        kiltdMediaFragment.mNFFilter = new NFFilter(ModuleID, EntityGUID);
        kiltdMediaFragment.mNFFilter.AllActivity = NewsFeedRequest.ALL_ACTIVITY_NEWS_FEED;
        return kiltdMediaFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mLogedInUserModel = new LogedInUserModel(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.kiltd_media_fragment, null);
        mContext = getActivity();

        mActivityGUID = getArguments().getString("mActivityGUID");
        mTeamGUID = getArguments().getString("mTeamGUID");
        mNotificationType = getArguments().getInt("mNotificationType");

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mNewsFeeds.add(new NewsFeed(null));

        initHeader();
        initialization(view);

        getFeedRequest(mNFFilter);
    }

    private void initHeader() {

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu,
                getResources().getString(R.string.kilt_label), 0);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashboardActivity) getActivity()).sliderListener();
            }
        }, null);
    }

    private ProgressBar mLoadingCenter;
    private TextView mNoMoreTV;
    private ProgressBar mLoaderBottomPB;

    // inir_ll
    public void initialization(View view) {

        mLoadingCenter = (ProgressBar) view.findViewById(R.id.loading_pb);

        mKiltdAdapter = new KiltdMediaAdapter(view.getContext(), new KiltdMediaAdapter.OnItemClickListenerPost() {
            @Override
            public void onClickItems(int ID, int position, NewsFeed newsFeed) {
                ItemClickListner(ID, position, newsFeed);
            }
        }, mLogedInUserModel.mUserGUID);

        videosListView = (ListView) view.findViewById(R.id.videos_list_lv);
        mErrorLayout = new ErrorLayout(view.findViewById(R.id.bg_container_rl));
        mNewsFeedRequest = new KiltdFeedRequest(view.getContext());
        mFooterRL = LayoutInflater.from(mContext).inflate(R.layout.footer_list_scroll, null);
        mNoMoreTV = (TextView) mFooterRL.findViewById(R.id.no_more_data_tv);
        mNoMoreTV.setVisibility(View.INVISIBLE);
        mLoaderBottomPB = (ProgressBar) mFooterRL.findViewById(R.id.loading_bottom_pb);
        mLoaderBottomPB.setVisibility(View.INVISIBLE);
        mLoaderBottomPB.getIndeterminateDrawable().setColorFilter(Color.parseColor("#01579B"), PorterDuff.Mode.SRC_IN);

        videosListView.addFooterView(mFooterRL);
        videosListView.setAdapter(mKiltdAdapter);
        videosListView.setOnScrollListener(new MyCustomonScrol());

        mPullToRefreshSrl = (SwipeRefreshLayout) view.findViewById(R.id.pull_to_refresh_srl);
        mPullToRefreshSrl.setColorScheme(R.color.blue, R.color.red, R.color.app_text_color, R.color.text_hint_color);
        mPullToRefreshSrl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
                getFeedRequest(mNFFilter);
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();

//        mLoadingCenter.post(new Runnable() {
//            @Override
//            public void run() {
//                if (null == mActivityGUID || TextUtils.isEmpty(mActivityGUID)) {
//                    if (null != mNFFilter)
//                        getFeedRequest(mNFFilter);
//                } else {
//                    mNFFilter.ActivityGUID = mActivityGUID;
//                    mNFFilter.EntityGUID = mTeamGUID;
//                    if (mNotificationType == 1) {
//                        mNFFilter.ModuleID = 1;
//                    } else {
//                        mNFFilter.ModuleID = 3;
//                    }
//
//                    mNFFilter.AllActivity = 0;
//                    getFeedRequest(mNFFilter);
//
//                    mNFFilter.AllActivity = 1;
//                    mNFFilter.ModuleID = 3;
//                }
//                //	getFeedRequest(mNFFilter);
//            }
//        });
    }


    private void getFeedRequest(final NFFilter nffilter) {

        if (getConnectivityStatus(mContext) == 0) {
            mPullToRefreshSrl.setRefreshing(false);
        }
        isLoading = false;
        mNoMoreTV.setVisibility(View.INVISIBLE);

        if (mPullToRefreshSrl.isRefreshing()) {
            mNewsFeedRequest.setLoader(null);
        } else {
            mNewsFeedRequest.setLoader(nffilter.PageNo <= 1 ? mLoadingCenter : mLoaderBottomPB);
        }

        mNewsFeedRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                mPullToRefreshSrl.setRefreshing(false);

                if (success) {

                    if (null != data) {
                        if (mNFFilter.PageNo == 1) {
                            resetList();
                        }
                        List<NewsFeed> newsFeeds = (List<NewsFeed>) data;
                        mNewsFeeds.addAll(newsFeeds);
                        mKiltdAdapter.setList(mNewsFeeds);

                        if (totalRecords == 0) {
                            isLoading = false;
                        } else if (mNewsFeeds.size() < totalRecords) {
                            isLoading = true;
                            int newPage = mNFFilter.PageNo + 1;
                            mNFFilter.PageNo = newPage;
                        } else {
                            isLoading = false;
                            mNoMoreTV.setVisibility(View.VISIBLE);
                            mPullToRefreshSrl.setRefreshing(false);
                        }

                        if (mNFFilter.PageNo == 1) {
                            mNoMoreTV.setVisibility(View.INVISIBLE);
                        } else {
                            mNoMoreTV.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    isLoading = false;
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, ErrorLayout.MsgType.Error);
                }

                mPullToRefreshSrl.setRefreshing(false);
            }
        });
        mNewsFeedRequest.getNewsFeedListInServer(nffilter);
    }

    public int getConnectivityStatus(Context context) {
        if (null == context) {
            return 0;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (null != activeNetwork && activeNetwork.isConnected()) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return 1;
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return 2;
            }
        }
        return 0;
    }

    private void resetList() {

        mNFFilter.PageNo = Config.DEFAULT_PAGE_INDEX;
        mNewsFeeds.clear();
        mNewsFeeds.add(new NewsFeed(null));
        mKiltdAdapter.setList(mNewsFeeds);
    }
    @Override
    public void onStart() {
        mNewsFeedRequest.setActivityStatus(true);
        super.onStart();
    }

    @Override
    public void onStop() {
        mNewsFeedRequest.setActivityStatus(false);
        super.onStop();
    }


    private View mFooterRL;
    int ClickLocation = 0;

    public void ItemClickListner(int ID, int position, NewsFeed newsFeed) {
        ClickLocation = position;

        switch (ID) {
            case R.id.share_tv:

                if (newsFeed.ShareAllowed == 1) {
                    sharePost(newsFeed);
                } else {
                    Toast.makeText(mContext, "You cant share own post", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.like_IV:
                LikeMediaToggleService(newsFeed, position);
                break;
            case R.id.likes_tv:
                if (newsFeed.NoOfLikes != 0) {
                    startActivityForResult(LikeListActivity.getIntent(mContext, newsFeed.ActivityGUID,
                            "ACTIVITY", "WALL"), REQ_FROM_HOME_LIKELIST);
                    getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
                }
                break;
            case R.id.comments_tv:
                AlbumMedia albumMedia = new AlbumMedia(newsFeed.ActivityGUID, newsFeed.UserProfilePicture, "");
                WriteCommentActivity.addFullPathIfNeeded(newsFeed, albumMedia);
                String captionForWriteCommentScreen = "";
                if (!newsFeed.PostContent.equals("")) {
                    captionForWriteCommentScreen = newsFeed.PostContent;
                } else {
                    if (null != newsFeed.Album.AlbumMedias && newsFeed.Album.AlbumMedias.size() != 0) {
                        captionForWriteCommentScreen = newsFeed.Album.AlbumMedias.get(0).ImageName;
                    }
                }

                startActivityForResult(CommentListActivity.getIntent(mContext, albumMedia, "WALLPOST",
                        captionForWriteCommentScreen), REQ_FROM_HOME_COMMENTLIST);
                getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

                break;

//            case PostFeedAdapter.ConvertViewID:
//                startActivityForResult(CreateWallPostActivity.getIntent(mContext,
//                        WallPostCreateRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID, true),
//                        CreateWallPostActivity.REQ_CODE_CREATE_WALL_POST_ACTIVITY);
//                getActivity().overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);
//
//                break;

            default:
                break;
        }
    }

    boolean isLoading;

    private class MyCustomonScrol implements AbsListView.OnScrollListener {

        private int mLastFirstVisibleItem;

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int k = visibleItemCount + firstVisibleItem;

            if (k >= totalItemCount && isLoading && totalItemCount != 0) {
                getFeedRequest(mNFFilter);
            }
            if (mLastFirstVisibleItem < firstVisibleItem) {
                Log.i("SCROLLING DOWN", "TRUE");
            }
            if (mLastFirstVisibleItem > firstVisibleItem) {
                Log.i("SCROLLING UP", "TRUE");
            }
            mLastFirstVisibleItem = firstVisibleItem;
        }
    }

    public void sharePost(NewsFeed newsFeedModel) {

        SharePostRequest mSharePostRequest = new SharePostRequest(mContext);
        mSharePostRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mErrorLayout.showError(getResources().getString(R.string.wall_post_shared_Succes), true, ErrorLayout.MsgType.Success);
                } else {
                    mErrorLayout.showError((String) data, true, ErrorLayout.MsgType.Error);
                }
            }
        });
        mSharePostRequest.sharePostInServer(newsFeedModel.ActivityGUID, "ACTIVITY", newsFeedModel.PostContent,
                NewsFeedRequest.MODULE_ID_USERS, mLogedInUserModel.mUserGUID, 1, 1);
    }

    private ToggleLikeRequest mToggleLikeRequest;

    public void LikeMediaToggleService(final NewsFeed newsFeed, final int position) {

        mToggleLikeRequest = new ToggleLikeRequest(mContext);
        mToggleLikeRequest.setRequestListener(new BaseRequest.RequestListener() {
            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    if (newsFeed.IsLike == 0) {
                        newsFeed.IsLike = 1;
                        newsFeed.NoOfLikes++;
                    } else {
                        newsFeed.IsLike = 0;
                        newsFeed.NoOfLikes--;
                    }
                    mKiltdAdapter.notifyDataSetChanged();

                } else {
                    mErrorLayout.showError((null == data ? getString(R.string.Something_went_wrong)
                            : (String) data), true, ErrorLayout.MsgType.Error);
                }

            }
        });
        mToggleLikeRequest.toggleLikeInServer(newsFeed.ActivityGUID, "ACTIVITY");
    }
}
