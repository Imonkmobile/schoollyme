package com.schollyme.media;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.SchollyMeApplication;
import com.schollyme.commentslike.CommentListActivity;
import com.schollyme.commentslike.LikeListActivity;
import com.schollyme.commentslike.WriteCommentActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.ErrorLayoutListener;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.Album;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.AlbumCreateRequest;
import com.vinfotech.request.AlbumMediaDeleteRequest;
import com.vinfotech.request.AlbumSetCoverRequest;
import com.vinfotech.request.EntityViewRequest;
import com.vinfotech.request.LikerListRequest;
import com.vinfotech.request.MarkSpamRequest;
import com.vinfotech.request.MediaDetailRequest;
import com.vinfotech.request.MediaUpdateCaptionRequest;
import com.vinfotech.request.ReportMediaRequest;
import com.vinfotech.request.SharePostRequest;
import com.vinfotech.request.ToggleLikeRequest;
import com.vinfotech.request.WallPostCreateRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.DialogUtil.SingleEditListener;
import com.vinfotech.utility.DisplayUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;
import com.vinfotech.widget.TouchImageView;

public class MediaViewerActivity extends BaseActivity {

    private static final String TAG = MediaViewerActivity.class.getSimpleName();
    public static final int REQ_CODE_MEDIA_VIEWER_ACTIVITY = 1013;

    final int REQ_CODE_COMMENT_LIST = 656;
    private ErrorLayout mErrorLayout;
    private MediaDetailRequest mMediaDetailRequest;
    private AlbumMediaDeleteRequest mMediaDeleteRequest;
    private MediaUpdateCaptionRequest mMediaEditRequest;
    private AlbumSetCoverRequest mAlbumSetCoverRequest;
    private MarkSpamRequest mMarkSpamRequest;
    private ArrayList<AlbumMedia> mAlbumMedias;
    private LogedInUserModel mLogedInUserModel;

    private VHolder mVHolder;
    private int IsOwner = 0;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.media_viewer_activity);
        mContext = this;
        mLogedInUserModel = new LogedInUserModel(this);
        SchollyMeApplication sma = (SchollyMeApplication) getApplication();
        sma.pauseSong();

        getIntentData();
        initlization();
        setupRequestListners();
    }

    private void initlization() {
        mErrorLayout = new ErrorLayout(findViewById(R.id.error_layout));
        mMediaDeleteRequest = new AlbumMediaDeleteRequest(this);
        mMediaDetailRequest = new MediaDetailRequest(this);
        mMediaDetailRequest.setLoader(findViewById(R.id.loading_pb));
        mMarkSpamRequest = new MarkSpamRequest(this);
        mToggleLikeRequest = new ToggleLikeRequest(this);
        mMediaEditRequest = new MediaUpdateCaptionRequest(this);
        mAlbumSetCoverRequest = new AlbumSetCoverRequest(this);
        mViewPagerAdapter = new ViewPagerAdapter();
        mVHolder = new VHolder(findViewById(R.id.main_container_rl));
    }

    ViewPagerAdapter mViewPagerAdapter;

    class VHolder {
        private ViewPager mViewPager;
        TextView mDoneTv;

        public VHolder(View view) {
            mViewPager = (ViewPager) view.findViewById(R.id.myfivepanelpager);
            mViewPager.setAdapter(mViewPagerAdapter);
            mDoneTv = (TextView) view.findViewById(R.id.done_tv);
            FontLoader.setRobotoRegularTypeface(mDoneTv);
            mViewPager.setCurrentItem(SelectedPosition);
            mDoneTv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    public void setupRequestListners() {
        mMarkSpamRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.showError(getString(R.string.Media_flagged_in_appropriate), true, MsgType.Success);
                } else if (null != data) {
                    DialogUtil.showOkDialog(MediaViewerActivity.this, (String) data, "");
                }
            }
        });

        mMediaEditRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    final String newCaption = (String) data;
                    mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()).Caption = newCaption;
                    mErrorLayout.showError(getString(R.string.Media_caption_updated_successfully), true, MsgType.Success);
                    if (null != mOnActionDescTv) {
                        mOnActionDescTv.setText(newCaption);
                    }
                    Intent in = new Intent();
                    in.putExtra("AlbumMedia", mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()));
                    MediaViewerActivity.this.setResult(RESULT_OK, in);
                } else if (null != data) {
                    DialogUtil.showOkDialog(MediaViewerActivity.this, (String) data, "");
                }
            }
        });

        mMediaDetailRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    AlbumMedia mMedia = (AlbumMedia) data;

                    mAlbumMedias.set(mVHolder.mViewPager.getCurrentItem(), mMedia);
                    mViewPagerAdapter.notifyDataSetChanged();

                } else if (null != data) {
                    DialogUtil.showOkDialog(MediaViewerActivity.this, (String) data, "");
                }
            }
        });

        mAlbumSetCoverRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    AlbumGridActivity.setReloadData(true);
                    mErrorLayout.setErrorLayoutListener(null);
                    mErrorLayout.showError(getString(R.string.Album_cover_updated_successfully), true, MsgType.Success);
                } else if (null != data) {
                    DialogUtil.showOkDialog(MediaViewerActivity.this, (String) data, "");
                }
            }
        });
    }

    private int SelectedPosition = 0;
    private Album mAlbum = null;

    private void getIntentData() {
        mAlbumMedias = getIntent().getParcelableArrayListExtra("albumMedias");
        SelectedPosition = getIntent().getIntExtra("SelectedPosition", 0);
        mAlbum = getIntent().getParcelableExtra("album");
        IsOwner = getIntent().getIntExtra("IsOwner", 0);
    }

    public static Intent getIntent(Context context, ArrayList<AlbumMedia> albumMedias, int SelectedPosition, Album album, int IsOwner) {
        Intent intent = new Intent(context, MediaViewerActivity.class);
        intent.putParcelableArrayListExtra("albumMedias", albumMedias);
        intent.putExtra("SelectedPosition", SelectedPosition);
        intent.putExtra("album", album);
        intent.putExtra("IsOwner", IsOwner);
        return intent;
    }

    private TextView mOnActionDescTv = null;

    public void deleteMedia(String albumGUID, final List<AlbumMedia> mAlbumMediaList) {

        mMediaDeleteRequest.deleteMediaFromServer(albumGUID, mAlbumMediaList);
        mMediaDeleteRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {

                if (success) {
                    mErrorLayout.showError(getString(R.string.Media_deleted_successfully), true, MsgType.Success);

                    if (mAlbumMedias.size() == 1) {

                        DialogUtil.showOkDialogNonCancelable(MediaViewerActivity.this, getString(R.string.Album_is_deleted), "",
                                new OnOkButtonListner() {

                                    @Override
                                    public void onOkBUtton() {
                                        Intent in = new Intent();
                                        in.putExtra("DELETEDMEDIA", "");
                                        setResult(RESULT_OK, in);
                                        finish();
                                    }
                                });
                    } else {
                        Intent in = new Intent();
                        in.putExtra("DELETEDMEDIA", mAlbumMediaList.get(0).MediaGUID);
                        setResult(RESULT_OK, in);
                        finish();
                    }

                } else if (null != data) {
                    DialogUtil.showOkDialog(MediaViewerActivity.this, (String) data, "");
                }
            }
        });

    }

    public void onActionClick(final AlbumMedia mMedia, TextView descTv) {
        this.mOnActionDescTv = descTv;
        final boolean isOwnder = (mAlbum.IsEditable == 1);
        final boolean isVideo = AlbumCreateRequest.ALBUMTYPE_VIDEO.equalsIgnoreCase(mMedia.MediaType)
                || AlbumCreateRequest.ALBUMTYPE_YOUTUBE.equalsIgnoreCase(mMedia.MediaType);
        String[] items;
        if (isOwnder && mMedia.IsCoverMedia == 0) {
            items = new String[3];
            items[0] = getString(R.string.Set_as_Album_Cover);
            items[1] = getString(R.string.Edit_Caption);
            items[2] = getString(isVideo ? R.string.Delete_Video : R.string.Delete_Photo);
        } else if (isOwnder && mMedia.IsCoverMedia == 1) {
            items = new String[2];
            // items[0] = getString(R.string.Set_as_Album_Cover);
            items[0] = getString(R.string.Edit_Caption);
            items[1] = getString(isVideo ? R.string.Delete_Video : R.string.Delete_Photo);
        } else {
            items = new String[1];
            items[0] = getString(R.string.Report);
        }

        DialogUtil.showListDialog(this, 0, new OnItemClickListener() {

            @Override
            public void onItemClick(int position, String item) {

                if (isOwnder && mMedia.IsCoverMedia == 0) {

                    switch (position) {
                        case 0:
                            mAlbumSetCoverRequest.setCoverMediaInServer(mAlbum.AlbumGUID, mMedia.MediaGUID);
                            break;
                        case 1:
                            DialogUtil.showSingleEditDialog(MediaViewerActivity.this, R.string.Write_caption, mMedia.Caption,
                                    new SingleEditListener() {

                                        @Override
                                        public void onEdit(boolean canceled, EditText editText, String text) {
                                            Utility.hideSoftKeyboard(editText);
                                            if (canceled) {
                                            } else {
                                                mMediaEditRequest.updateMediaFromServer(mAlbum.AlbumGUID, mMedia.MediaGUID, text);
                                            }
                                        }
                                    });
                            break;
                        case 2:
                            DialogUtil.showOkCancelDialog(MediaViewerActivity.this, R.string.Delete, R.string.ok_label,
                                    R.string.cancel_caps, getString(isVideo ? R.string.Delete_Album : R.string.Delete_Photo),
                                    getString(isVideo ? R.string.Confirm_delete_Video : R.string.Confirm_delete_Photo), new OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            List<AlbumMedia> mAlbumMedias = new ArrayList<AlbumMedia>();
                                            mAlbumMedias.add(mMedia);
                                            deleteMedia(mAlbum.AlbumGUID, mAlbumMedias);

                                        }
                                    }, null);

                            break;
                        default:
                            break;
                    }

                } else if (isOwnder && mMedia.IsCoverMedia == 1) {
                    switch (position) {
                        case 0:
                            DialogUtil.showSingleEditDialog(MediaViewerActivity.this, R.string.Write_caption, mMedia.Caption,
                                    new SingleEditListener() {

                                        @Override
                                        public void onEdit(boolean canceled, EditText editText, String text) {
                                            Utility.hideSoftKeyboard(editText);
                                            if (canceled) {
                                            } else {
                                                mMediaEditRequest.updateMediaFromServer(mAlbum.AlbumGUID, mMedia.MediaGUID, text);
                                            }
                                        }
                                    });

                            break;
                        case 1:
                            DialogUtil.showOkCancelDialog(MediaViewerActivity.this, R.string.Delete, R.string.ok_label,
                                    R.string.cancel_caps, getString(isVideo ? R.string.Delete_Album : R.string.Delete_Photo),
                                    getString(isVideo ? R.string.Confirm_delete_Video : R.string.Confirm_delete_Photo),
                                    new OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            List<AlbumMedia> mAlbumMedias = new ArrayList<AlbumMedia>();
                                            mAlbumMedias.add(mMedia);
                                            deleteMedia(mAlbum.AlbumGUID, mAlbumMedias);

                                        }
                                    }, null);

                            break;
                        default:
                            break;
                    }

                } else {
                    mMarkSpamRequest.showFlagDialog(Config.IMAGE_URL_UPLOAD + Config.ALBUM_FOLDER + mMedia.ImageName);
                }

            }

            @Override
            public void onCancel() {
            }
        }, items);
    }

    private class ViewPagerAdapter extends PagerAdapter {
        public ViewPagerAdapter() {

        }

        public void setCurrentItem() {
            mVHolder.mViewPager.setCurrentItem(SelectedPosition);
            notifyDataSetChanged();
        }

        // @Override
        // public float getPageWidth(int position) {
        // return 0.8f;
        // }
        public int getCount() {
            return mAlbumMedias.size();
        }

        public Object instantiateItem(View collection, int position) {
            LayoutInflater layoutInflater = MediaViewerActivity.this.getLayoutInflater();
            final View view = layoutInflater.inflate(R.layout.media_view_items, null);
            final ViewHolder viewHolder = new ViewHolder(view);

            final AlbumMedia mAlbumMedia = mAlbumMedias.get(position);
            if (null != mAlbumMedia) {
                viewHolder.mDescriptionTv.setText(mAlbumMedia.Caption);

                updateCommentCount(viewHolder.mCommentsTv, mAlbumMedia);
                updateLike(viewHolder.mLikeTv, mAlbumMedia, viewHolder.mLikesTv);
                viewHolder.mPlayIv.setVisibility(View.GONE);
                viewHolder.mViewCountTV.setText("" + mAlbumMedia.ViewCount);

                viewHolder.mShareTv.setVisibility(mAlbum.Visibility == AlbumCreateRequest.VISIBILITY_TEAMMATES ? View.GONE : View.VISIBLE);
                if (AlbumMedia.MEDIA_TYPE_IMAGE.equalsIgnoreCase(mAlbumMedia.MediaType)) {
                    viewHolder.mViewCountTV.setVisibility(View.GONE);
                    mAlbumMedia.tmpThumb = Config.getCoverPath(mAlbum, mAlbumMedia.ImageName, false, true);
                    ImageLoaderUniversal.ImageLoadSquareWithProgressBar(MediaViewerActivity.this, mAlbumMedia.tmpThumb,
                            viewHolder.mMediaIv, ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoadingPb);
                } else if (AlbumMedia.MEDIA_TYPE_VIDEO.equalsIgnoreCase(mAlbumMedia.MediaType)) {
                    viewHolder.mViewCountTV.setVisibility(View.VISIBLE);

                    mAlbumMedia.tmpThumb = Config.getCoverPath(mAlbum, Config.getVideoToJPG(mAlbumMedia.ImageName), true);
                    viewHolder.mLoadingPb.setVisibility(View.GONE);

                    // ImageLoaderUniversal.ImageLoadSquareWithProgressBar(MediaViewerActivity.this,
                    // media.tmpThumb, viewHolder.mMediaIv,
                    // ImageLoaderUniversal.DiaplayOptionForProgresser,
                    // viewHolder.mLoadingPb);

                    ImageLoaderUniversal.ImageLoadSquare(MediaViewerActivity.this, mAlbumMedia.tmpThumb, viewHolder.mMediaIv,
                            ImageLoaderUniversal.option_normal_Image_Thumbnail);

                    viewHolder.mMediaIv.setEnabled(false);
                    viewHolder.mPlayIv.setVisibility(View.VISIBLE);
                    final VideoView videoView = viewHolder.mediaVv;
                    videoView.setTag(viewHolder);
                    viewHolder.mPlayIv.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String mediaUrl = Config.VIDEO_URL_UPLOAD.replace("https:", "http:") + Config.ALBUM_FOLDER
                                    + Config.VIDEO_FOLDER + mAlbumMedia.ImageName;
                            setViewCount(mAlbumMedia, EntityViewRequest.ENTITY_TYPE_MEDIA);
                            prepareVideoDisplay(mediaUrl, videoView);
                        }
                    });
                } else if (AlbumMedia.MEDIA_TYPE_YOUTUBE.equalsIgnoreCase(mAlbumMedia.MediaType)) {
                    viewHolder.mViewCountTV.setVisibility(View.VISIBLE);
                    mAlbumMedia.tmpThumb = getYoutubeVideoThumb(mAlbumMedia.ImageName);
                    ImageLoaderUniversal.ImageLoadSquareWithProgressBar(MediaViewerActivity.this, mAlbumMedia.tmpThumb,
                            viewHolder.mMediaIv, ImageLoaderUniversal.DiaplayOptionForProgresser, viewHolder.mLoadingPb);
                    viewHolder.mMediaIv.setEnabled(false);
                    viewHolder.mPlayIv.setVisibility(View.VISIBLE);
                    viewHolder.mPlayIv.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setViewCount(mAlbumMedia, EntityViewRequest.ENTITY_TYPE_MEDIA);
                            watchVideoAlternate(mAlbumMedia.ImageName);
                        }
                    });
                }
            }
            viewHolder.mLikeTv.setTag(viewHolder);
            viewHolder.mLikesTv.setTag(viewHolder);

            viewHolder.mLikeTv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    ViewHolder vh = (ViewHolder) v.getTag();
                    mOnActionLikeTV = (TextView) v;
                    mOnActionLikesCount = vh.mLikesTv;
                    DisplayUtil.bounceView(view.getContext(), viewHolder.mLikeTv);
                    LikeMediaToggleService(mAlbumMedia, mOnActionLikeTV);
                }
            });

            updateMenuAfterFlag(viewHolder.mActionIB, mAlbumMedia, position);
            /*if(IsOwner == 1){
                viewHolder.mActionIB.setTag(viewHolder.mDescriptionTv);
				viewHolder.mActionIB.setVisibility(mAlbum.IsEditable == 1 ? View.VISIBLE : View.GONE);
				viewHolder.mActionIB.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						onActionClick(mAlbumMedia, (TextView) v.getTag());
					}
				});
			}
			else{
				boolean  flag = (mAlbumMedia.IsFlagged == 0 && mAlbumMedia.FlagAllowed == 1 && mAlbumMedia.Flaggable == 1);
				viewHolder.mActionIB.setVisibility(View.VISIBLE);
				viewHolder.mActionIB.setTag(mAlbumMedia);
				viewHolder.mActionIB.setId(position);
				if(flag && IsOwner==0){

					viewHolder.mActionIB.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							int pos = v.getId();
							AlbumMedia media = (AlbumMedia) v.getTag();
							showFlagDialog(media,pos,true);
						}
					});
				}
				else{
					viewHolder.mActionIB.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							int pos = v.getId();
							AlbumMedia media = (AlbumMedia) v.getTag();
							showFlagDialog(media, pos, false);
						}
					});
				}
			}*/

            viewHolder.mCommentTv.setTag(viewHolder);
            viewHolder.mCommentTv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    ViewHolder vh = (ViewHolder) v.getTag();
                    mOnActionCommentTv = vh.mCommentsTv;
                    // startActivityForResult(
                    // CommentListActivity.getIntent(MediaViewerActivity.this,
                    // mAlbumMedia, "MEDIA", mAlbumMedia.Caption),
                    // REQ_CODE_COMMENT_LIST);
                    startActivityForResult(
                            WriteCommentActivity.getIntent(MediaViewerActivity.this, mAlbumMedia, "MEDIA", mAlbumMedia.Caption),
                            WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY);
                    MediaViewerActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

                }
            });

            viewHolder.mCommentsTv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    mOnActionCommentTv = (TextView) v;
                    startActivityForResult(
                            CommentListActivity.getIntent(MediaViewerActivity.this, mAlbumMedia, "MEDIA", mAlbumMedia.Caption),
                            REQ_CODE_COMMENT_LIST);
                    MediaViewerActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

                }
            });
            viewHolder.mLikesTv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!viewHolder.mLikesTv.getText().toString().startsWith("0")) {
                        ViewHolder vh = (ViewHolder) v.getTag();

                        mOnActionLikesCount = (TextView) v;
                        mOnActionLikeTV = vh.mLikeTv;

                        startActivityForResult(LikeListActivity.getIntent(MediaViewerActivity.this, mAlbumMedia.MediaGUID,
                                LikerListRequest.ENTITYTYPE_MEDIA, "MEDIAVIEWER", mAlbumMedia),
                                LikeListActivity.REQ_CODE_LIKE_ACTIVITY_WALL);
                        MediaViewerActivity.this.overridePendingTransition(R.anim.slide_up_in, R.anim.slide_down_in);

                    }

                }
            });

            viewHolder.mShareTv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String mediaUrl = (TextUtils.isEmpty(mAlbumMedia.tmpThumb) ? "" : mAlbumMedia.tmpThumb);

                    DialogUtil.showOkCancelDialog(MediaViewerActivity.this, R.string.ok_label, R.string.cancel_caps,
                            "", v.getContext().getString(R.string.Are_share_this_media),
                            new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    sharePost(mAlbumMedia.MediaGUID, SharePostRequest.ENTITYTYPE_MEDIA, mLogedInUserModel.mUserGUID);
                                }
                            }, null);

                    // startActivity(SharePostActivity.getIntent(v.getContext(),
                    // mAlbumMedia.MediaGUID, SharePostRequest.ENTITYTYPE_MEDIA,
                    // mLogedInUserModel.mUserGUID, mediaUrl,
                    // mAlbumMedia.Caption));

                }
            });

            ((ViewPager) collection).addView(view, 0);
            return view;
        }

        public void updateMenuAfterFlag(final ImageButton mActionIB, AlbumMedia media, int position) {
            boolean flag = (media.IsFlagged == 0 && media.FlagAllowed == 1 && media.Flaggable == 1);
            mActionIB.setVisibility(View.VISIBLE);
            mActionIB.setTag(media);
            mActionIB.setId(position);
            if (flag && IsOwner == 0) {

                mActionIB.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int pos = v.getId();
                        AlbumMedia media = (AlbumMedia) v.getTag();
                        showFlagDialog(media, pos, true, mActionIB);
                    }
                });
            } else {
                mActionIB.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int pos = v.getId();
                        AlbumMedia media = (AlbumMedia) v.getTag();
                        showFlagDialog(media, pos, false, mActionIB);
                        //mViewPagerAdapter.setCurrentItem();
                    }
                });
            }
        }

        private void showFlagDialog(final AlbumMedia newsFeed, final int pos, final boolean isClickable, final ImageButton mActionIB) {
            if (isClickable) {
                String[] options = new String[]{mContext.getString(R.string.flag)};
                DialogUtil.showListDialog(mContext, 0, new OnItemClickListener() {

                    @Override
                    public void onItemClick(int position, String item) {
                        switch (item) {
                            case "Flag":
                                showFlagInputDialog(newsFeed, pos, mActionIB);
                                break;
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                }, options);
            } else {
                String[] options = new String[]{mContext.getString(R.string.flaged)};
                DialogUtil.showListDialogCustom(mContext, 0, null, options);
            }
        }


        private void showFlagInputDialog(final AlbumMedia newsFeed, final int pos, final ImageButton mActionIB) {
            DialogUtil.showSingleEditDialog(mContext, R.string.flag_reason, "", new DialogUtil.SingleEditListener() {
                @Override
                public void onEdit(boolean canceled, EditText editText, String text) {
                    String reason = editText.getText().toString();
                    if (TextUtils.isEmpty(reason)) {
                        showFlagInputDialog(newsFeed, pos, mActionIB);
                    } else {
                        ReportMediaRequest request = new ReportMediaRequest(mContext);
                        request.setRequestListener(new RequestListener() {
                            @Override
                            public void onComplete(boolean success, Object data, int totalRecords) {

                                mErrorLayout.showError(getString(R.string.flaged), false, MsgType.Success);
                                AlbumMedia feed = newsFeed;
                                feed.IsFlagged = 1;

                                mAlbumMedias.set(pos, feed);

                                mViewPagerAdapter.updateMenuAfterFlag(mActionIB, newsFeed, pos);
                                //		mViewPagerAdapter.notifyDataSetChanged();
                                //	mViewPagerAdapter.setCurrentItem();
                                //	button.setVisibility(View.GONE);
                            }
                        });
                        String EntityGUID = newsFeed.MediaGUID;
                        String EntityType = "ACTIVITY";
                        String FlagReason = reason;
                        Utility.hideSoftKeyboard(editText);
                        request.ReportMediaServerRequest(EntityGUID, FlagReason);
                    }
                }
            });
        }

        protected void setViewCount(AlbumMedia albumMedia, String entityTypeUser) {
            albumMedia.ViewCount = albumMedia.ViewCount + 1;
            EntityViewRequest mEntityViewRequest = new EntityViewRequest(MediaViewerActivity.this);
            mEntityViewRequest.setViewAtServer(albumMedia.MediaGUID, entityTypeUser);

        }

        @Override
        public void destroyItem(View arg0, int arg1, Object arg2) {

        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {

        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        private void updateCommentCount(TextView textView, AlbumMedia media) {
            textView.setText(media.NoOfComments + " "
                    + (textView.getResources().getString(1 == media.NoOfComments ? R.string.Comment : R.string.Comments)));
        }

        private void updateLike(TextView textView, AlbumMedia media, TextView mLikeCount) {
            textView.setSelected(media.IsLike == 0 ? false : true);
            mLikeCount.setText(media.NoOfLikes + " " + getString(media.NoOfLikes == 1 ? R.string.Like : R.string.Likes));
        }

        private class ViewHolder {
            private TextView mDescriptionTv, mLikesTv, mCommentsTv, mLikeTv, mCommentTv, mShareTv, mViewCountTV;
            ;
            private ImageView mPlayIv;
            private ImageButton mActionIB;
            private TouchImageView mMediaIv;
            private VideoView mediaVv;
            private ProgressBar mLoadingPb;

            public ViewHolder(View view) {
                mDescriptionTv = (TextView) view.findViewById(R.id.description_tv);
                mLikesTv = (TextView) view.findViewById(R.id.likes_tv);
                mCommentsTv = (TextView) view.findViewById(R.id.comments_tv);
                mLikeTv = (TextView) view.findViewById(R.id.like_tv);
                mCommentTv = (TextView) view.findViewById(R.id.comment_tv);
                mShareTv = (TextView) view.findViewById(R.id.share_tv);
                mMediaIv = (TouchImageView) view.findViewById(R.id.media_iv);
                mViewCountTV = (TextView) view.findViewById(R.id.view_count_tv);
                mPlayIv = (ImageView) view.findViewById(R.id.play_iv);
                mActionIB = (ImageButton) view.findViewById(R.id.action_ib);
                mLoadingPb = (ProgressBar) view.findViewById(R.id.loading_pb);
                mediaVv = (VideoView) view.findViewById(R.id.media_vv);
                view.findViewById(R.id.bottom_rl).setVisibility(Config.isWallFolder(mAlbum.AlbumName) ? View.GONE : View.VISIBLE);
                FontLoader.setRobotoRegularTypeface(mDescriptionTv, mLikesTv, mCommentsTv, mLikeTv, mCommentTv, mShareTv);
            }
        }
    }

    private int mVisibility = WallPostCreateRequest.VISIBILITY_PUBLIC;
    private int mCommentable = WallPostCreateRequest.COMMENT_ON;
    private SharePostRequest mSharePostRequest;

    public void sharePost(String mEntityGUID, String mEntityType, String mModuleEntityGUID) {

        mSharePostRequest = new SharePostRequest(this);

        mSharePostRequest.sharePostInServer(mEntityGUID, mEntityType, "", SharePostRequest.MODULE_ID_USERS, mModuleEntityGUID, mVisibility,
                mCommentable);

        mSharePostRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, final Object data, int totalRecords) {
                if (success) {
                    mErrorLayout.setErrorLayoutListener(new ErrorLayoutListener() {

                        @Override
                        public void onErrorShown() {
                        }

                        @Override
                        public void onErrorHidden() {

                        }
                    });
                    mErrorLayout.showError(MediaViewerActivity.this.getResources().getString(R.string.Media_shared_successfully), true,
                            MsgType.Success);
                } else {

                    mErrorLayout.showError((String) data, true, MsgType.Error);
                }
            }
        });
    }

    final String MARKER_mov = "mov";
    final String MARKER_MOV = "MOV";
    final String NEW_MARKER = "mp4";

    private void prepareVideoDisplay(String urlVideo_, final VideoView mediaVv) {
        String urlVideo = urlVideo_;
        if (Config.DEBUG) {
            Log.d(TAG, "prepareVideoDisplay url=" + urlVideo + ", mediaVv=" + mediaVv);
        }
        urlVideo.trim();

        if (urlVideo.contains(MARKER_MOV)) {
            urlVideo = urlVideo.replaceAll(MARKER_MOV, NEW_MARKER);
        }
        if (urlVideo.contains(MARKER_mov)) {
            urlVideo = urlVideo.replaceAll(MARKER_mov, NEW_MARKER);
        }

        if (TextUtils.isEmpty(urlVideo) || !android.util.Patterns.WEB_URL.matcher(urlVideo).matches()) {
            Toast.makeText(this, "Invalid media URL: " + urlVideo, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        final String url = urlVideo;
        final ViewPagerAdapter.ViewHolder viewHolder = (ViewPagerAdapter.ViewHolder) mediaVv
                .getTag();
        mediaVv.setVisibility(View.VISIBLE);
        viewHolder.mPlayIv.setVisibility(View.GONE);
        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        final ProgressDialog progressDialog = DialogUtil.createDiloag(MediaViewerActivity.this, getString(R.string.Buffering));
        progressDialog.show();
        try {
            MediaController mediacontroller = new MediaController(MediaViewerActivity.this);
            mediacontroller.setAnchorView(mediaVv);
            mediacontroller.setMediaPlayer(mediaVv);
            mediaVv.setMediaController(mediacontroller);
            mediaVv.setVideoURI(Uri.parse(url));
        } catch (Exception e) {
            e.printStackTrace();
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        progressDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                viewHolder.mPlayIv.setVisibility(View.VISIBLE);
            }
        });
        mediaVv.requestFocus();
        mediaVv.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                if (null != progressDialog && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                viewHolder.mPlayIv.setVisibility(View.GONE);
                viewHolder.mMediaIv.setVisibility(View.GONE);
                mediaVv.start();
            }
        });

        mediaVv.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                viewHolder.mMediaIv.setVisibility(View.VISIBLE);
                viewHolder.mPlayIv.setVisibility(View.VISIBLE);
                viewHolder.mPlayIv.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!mediaVv.isPlaying()) {
                            viewHolder.mPlayIv.setVisibility(View.GONE);
                            viewHolder.mMediaIv.setVisibility(View.GONE);
                            mediaVv.start();
                        }
                    }
                });
            }
        });
        mediaVv.setOnErrorListener(new OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                if (null != progressDialog && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                viewHolder.mPlayIv.setVisibility(View.VISIBLE);
                DialogUtil.showOkCancelDialog(MediaViewerActivity.this, R.string.ok_label, R.string.cancel_caps,
                        null, getString(R.string.Sorry_this_video_can_not),
                        new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                watchVideoAlternate(url);
                            }
                        }, null);
                return true;
            }
        });
    }

    private ToggleLikeRequest mToggleLikeRequest;

    public void LikeMediaToggleService(AlbumMedia mMedia, final TextView mLike) {
        mToggleLikeRequest.toggleLikeInServer(mMedia.MediaGUID, ToggleLikeRequest.ENTITY_TYPE_MEDIA);
        mToggleLikeRequest.setRequestListener(new RequestListener() {

            @Override
            public void onComplete(boolean success, Object data, int totalRecords) {
                if (success) {
                    if (mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()).IsLike == 0) {
                        mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()).IsLike = 1;
                        mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()).NoOfLikes++;
                    } else {
                        mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()).IsLike = 0;
                        mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()).NoOfLikes--;
                    }
                    mViewPagerAdapter.updateLike(mOnActionLikeTV, mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()),
                            mOnActionLikesCount);

                    Intent in = new Intent();
                    in.putExtra("AlbumMedia", mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem()));
                    MediaViewerActivity.this.setResult(RESULT_OK, in);

                } else {
                    DialogUtil.showOkDialog(MediaViewerActivity.this, data.toString(), "");
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case WriteCommentActivity.REQ_CODE_WRITE_COMMENT_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    AlbumMedia media = data.getParcelableExtra("albumMedia");
                    if (null != mOnActionCommentTv) {
                        AlbumMedia mAlbumMedia = mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem());
                        mAlbumMedia.NoOfComments = media.NoOfComments;
                        ;
                        Intent in = new Intent();
                        in.putExtra("AlbumMedia", mAlbumMedia);
                        MediaViewerActivity.this.setResult(RESULT_OK, in);
                        mViewPagerAdapter.updateCommentCount(mOnActionCommentTv, media);
                    }
                }
                break;
            case REQ_CODE_COMMENT_LIST:
                if (resultCode == RESULT_OK) {
                    AlbumMedia media = data.getParcelableExtra("AlbumMedia");
                    Intent in = new Intent();
                    in.putExtra("AlbumMedia", media);
                    MediaViewerActivity.this.setResult(RESULT_OK, in);

                    if (null != media) {
                        for (AlbumMedia albumMedia : mAlbumMedias) {
                            if (media.MediaGUID.equalsIgnoreCase(albumMedia.MediaGUID)) {
                                albumMedia.NoOfComments = media.NoOfComments;
                                if (null != mOnActionCommentTv) {
                                    mViewPagerAdapter.updateCommentCount(mOnActionCommentTv, media);
                                }
                                break;
                            }
                        }
                    }
                }
                break;
            case LikeListActivity.REQ_CODE_LIKE_ACTIVITY_WALL:
                if (resultCode == RESULT_OK) {
                    AlbumMedia media = data.getParcelableExtra("AlbumMedia");
                    if (null != media) {
                        Intent in = new Intent();
                        in.putExtra("AlbumMedia", media);
                        MediaViewerActivity.this.setResult(RESULT_OK, in);
                        // mAlbumMedias.get(mVHolder.mViewPager.getCurrentItem())
                        for (int i = 0; i < mAlbumMedias.size(); i++) {
                            if (mAlbumMedias.get(i).MediaGUID.equals(media.MediaGUID)) {
                                mAlbumMedias.set(i, media);
                                break;
                            }
                        }
                        mViewPagerAdapter.updateLike(mOnActionLikeTV, media, mOnActionLikesCount);
                    }
                }
                break;

            default:
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onBackPressed() {

        // Intent in = new Intent();
        // MediaViewerActivity.this.setResult(RESULT_OK, in);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void watchVideoAlternate(String url) {
        if (Config.DEBUG) {
            Log.d(TAG, "watchVideoAlternate url=" + url);
        }
        if (isYouTubeVideo(url)) {
            String id = getYouTubeVideoId(url);
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubePrefix2 + id));
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(url), "video/mp4");
            Intent chooser = Intent.createChooser(intent, getString(R.string.Complete_action_using));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(chooser);
            }
        }
    }

    public static final String youtubePrefix1 = "http://youtu.be/";

    public static final String youtubePrefix2 = "https://www.youtube.com/watch?v=";
    public static final String youtubePrefix3 = "https://youtu.be/";

    public static boolean isYouTubeVideo(String url) {
        return (url.startsWith(youtubePrefix1) || url.startsWith(youtubePrefix2) || url.startsWith(youtubePrefix3));
    }

    public static String getYouTubeVideoId(String url) {
        String id = url.replaceAll(youtubePrefix1, "").replaceAll(youtubePrefix2, "").replaceAll(youtubePrefix3, "");
        int eIndex = id.indexOf("=");
        if (eIndex != -1) {
            id = id.substring(eIndex + 1);
        }
        return id;
    }

    public static String getYoutubeVideoThumb(String url) {
        String vidId = getYouTubeVideoId(url);
        String thumb = "http://img.youtube.com/vi/" + vidId + "/hq3.jpg";
        if (Config.DEBUG) {
            Log.d(TAG, "getYoutubeVideoThumb thumb=" + thumb);
        }
        return thumb;
    }

    // @Override
    // protected void onStart() {
    // mMediaDetailRequest.setActivityStatus(true);
    // this.registerReceiver(mCommentBroadcastReceiver, getIntenFilter());
    // super.onStart();
    // }
    //
    // @Override
    // protected void onStop() {
    // this.unregisterReceiver(mCommentBroadcastReceiver);
    // this.mMediaDetailRequest.setActivityStatus(false);
    // super.onStop();
    // }

    private TextView mOnActionCommentTv = null;
    private TextView mOnActionLikeTV = null;
    private TextView mOnActionLikesCount = null;
    private CommentBroadcastReceiver mCommentBroadcastReceiver = new CommentBroadcastReceiver();

    private class CommentBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            AlbumMedia media = intent.getParcelableExtra("AlbumMedia");
            Intent in = new Intent();
            MediaViewerActivity.this.setResult(RESULT_OK, in);

            if (null != media) {
                for (AlbumMedia albumMedia : mAlbumMedias) {
                    if (media.MediaGUID.equalsIgnoreCase(albumMedia.MediaGUID)) {
                        albumMedia.NoOfComments = media.NoOfComments;
                        if (null != mOnActionCommentTv) {
                            mViewPagerAdapter.updateCommentCount(mOnActionCommentTv, media);

                        }
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
