package com.schollyme.media;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff.Mode;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewMedia;
import com.schollyme.model.UploadingMedia;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnOkButtonListner;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.ImageLoaderUniversal;
import com.vinfotech.utility.Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UploadMediaActivity extends BaseActivity implements OnClickListener {

    private static final String TAG = UploadMediaActivity.class.getSimpleName();
    public static final int REQ_CODE_UPLOAD_MEDIA_ACTIVITY = 1012;

    private EditText mAboutEt;
    private TextView mUploadCountTv;
    private TextView mUploadingInTxtTv;
    private TextView mUploadingInTv;
    private TextView mMediaCountTv;
    private LinearLayout mThumbContainerLl;

    private HeaderLayout mHeaderLayout;
    private ErrorLayout mErrorLayout;
    private MediaUploadRequest mMediaUploadRequest;
    private AQuery mAQuery;
    private boolean mImageUpload;
    private String mAlbumName, mVideoPath;
    private LogedInUserModel mLogedInUserModel;
    private String mAllText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_media_activity);

        mHeaderLayout = new HeaderLayout(findViewById(R.id.container_ll));
        mHeaderLayout.setHeaderValues(R.drawable.icon_back, getResources().getString(R.string.Upload_Media), R.drawable.ic_white_tick);
        mHeaderLayout.setListenerItI(this, this);
        mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));
        mAQuery = new AQuery(this);
        mLogedInUserModel = new LogedInUserModel(this);

        mAlbumName = getIntent().getStringExtra("albumName");
        mImageUpload = getIntent().getBooleanExtra("imageUpload", true);
        if (mImageUpload) {
            String[] mImagePaths = getIntent().getStringArrayExtra("imagePaths");
            if (Config.DEBUG) {
                Log.d(TAG, "onCreate mImageUpload=" + mImageUpload + ", imagePaths=" + mImagePaths);
            }
            if ((null == mImagePaths || mImagePaths.length < 1)) {
                Toast.makeText(this, "Invalid image media!", Toast.LENGTH_SHORT).show();
                setResultAndFinish(RESULT_CANCELED);
                return;
            }

            for (int i = 0; i < mImagePaths.length; i++) {
                mUploadingMedias.add(new UploadingMedia("", mImagePaths[i]));
            }
        } else {
            mVideoPath = getIntent().getStringExtra("videoPath");
            if (Config.DEBUG) {
                Log.d(TAG, "onCreate mImageUpload=" + mImageUpload + ", mVideoPath=" + mVideoPath);
            }
            if (TextUtils.isEmpty(mVideoPath)) {
                Toast.makeText(this, "Invalid video media!", Toast.LENGTH_SHORT).show();
                setResultAndFinish(RESULT_CANCELED);
                return;
            }

            mUploadingMedias.add(new UploadingMedia("", mVideoPath));
        }
        mMediaUploadRequest = new MediaUploadRequest(this, true);
        mMediaUploadRequest.setRequestListener(mRequestListener);

        mAboutEt = (EditText) findViewById(R.id.about_et);
        mAboutEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = mAboutEt.getText().toString().trim();
                if (mSelMediaPos < 0 || mSelMediaPos >= mUploadingMedias.size()) {
                    for (UploadingMedia uploadingMedia : mUploadingMedias) {
                        uploadingMedia.name = text;
                    }
                    mAllText = text;
                } else {
                    mUploadingMedias.get(mSelMediaPos).name = text;
                }
            }
        });

        mUploadCountTv = (TextView) findViewById(R.id.upload_count_tv);
        mUploadingInTxtTv = (TextView) findViewById(R.id.uploading_in_txt_tv);
        mUploadingInTv = (TextView) findViewById(R.id.uploading_in_tv);
        mMediaCountTv = (TextView) findViewById(R.id.media_count_tv);
        mThumbContainerLl = (LinearLayout) findViewById(R.id.thumb_container_ll);
        mUploadingInTv.setText(mAlbumName);

        if (mUploadingMedias.size() == 1) {
            mMediaCountTv.setVisibility(View.GONE);
        }

        addMediaInLayout();
    }

    public static Intent getIntent(Context context, String albumName, String[] imagePaths) {
        Intent intent = new Intent(context, UploadMediaActivity.class);
        intent.putExtra("imageUpload", true);
        intent.putExtra("albumName", albumName);
        intent.putExtra("imagePaths", imagePaths);
        return intent;

    }

    public static Intent getIntent(Context context, String albumName, String videoPath) {
        Intent intent = new Intent(context, UploadMediaActivity.class);
        intent.putExtra("imageUpload", false);
        intent.putExtra("albumName", albumName);
        intent.putExtra("videoPath", videoPath);
        return intent;

    }

    @Override
    public void onBackPressed() {
        if (mUploadingMedias.size() > 0) {
            DialogUtil.showOkCancelDialog(this, R.string.ok_label, R.string.cancel_caps,
                    getResources().getString(R.string.Discard_Media),
                    getResources().getString(R.string.Media_files_are_not_uploaded), new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            setResultAndFinish(RESULT_CANCELED);
                        }
                    }, null);
        } else {
            setResultAndFinish(RESULT_CANCELED);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_button:
                if (mUploadingMedias.size() > 0) {
                    DialogUtil.showOkCancelDialog(this, R.string.ok_label, R.string.cancel_caps,
                            getResources().getString(R.string.Discard_Media),
                            getResources().getString(R.string.Media_files_are_not_uploaded),
                            new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    setResultAndFinish(RESULT_CANCELED);
                                }
                            }, null);
                } else {
                    setResultAndFinish(RESULT_CANCELED);
                }
                break;
            case R.id.right_button:
                uploadMedias();
                break;

            default:
                break;
        }
    }

    private int mSelMediaPos = -1;
    private Map<Integer, View> mViews = new HashMap<Integer, View>();

    private void addMediaInLayout() {

        mUploadCountTv.setText(getString(R.string.Upload) + " " + mUploadingMedias.size()
                + " Photo" + (mUploadingMedias.size() == 1 ? "" : "s"));

        nutralizeAllTextColor();
        mMediaCountTv.setText(getString(R.string.all) + " " + mUploadingMedias.size());
        mMediaCountTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mSelMediaPos = -1;
                mAboutEt.setText(mAllText);

                nutralizeAllTextColor();
                for (int j = 0; j < mViews.size(); j++) {
                    mViews.get(j).findViewById(R.id.inner_container_rl)
                            .setBackgroundColor(j == mSelMediaPos ? Color.RED : Color.TRANSPARENT);
                }
            }
        });

        mThumbContainerLl.removeAllViews();
        mViews.clear();
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final int imgWidth = (int) getResources().getDimension(R.dimen.space_mid10) * 17;
        for (int i = 0; i < mUploadingMedias.size(); i++) {
            final View view = layoutInflater.inflate(R.layout.upload_media_item, null);
            final View containerRl = view.findViewById(R.id.container_rl);
            containerRl.setTag(i);

            final UploadingMedia uploadingMedia = mUploadingMedias.get(i);

            ImageView mediaIv = (ImageView) view.findViewById(R.id.media_iv);
            mediaIv.getLayoutParams().width = imgWidth;
            mediaIv.getLayoutParams().height = imgWidth;
            ImageView typeIv = (ImageView) view.findViewById(R.id.type_iv);
            if (mImageUpload) {
                ImageLoaderUniversal.ImageLoadSquare(this, "file://" + uploadingMedia.path, mediaIv,
                        ImageLoaderUniversal.option_normal_Image);

                typeIv.setVisibility(View.INVISIBLE);
            } else {

                loadVideoThumb(uploadingMedia.path, mediaIv);
                typeIv.setVisibility(View.VISIBLE);
            }

            mThumbContainerLl.addView(view);
            mViews.put(i, containerRl);

            containerRl.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    mMediaCountTv.setBackgroundResource(R.drawable.grey_rect);
                    mSelMediaPos = mThumbContainerLl.indexOfChild(view);
                    final int selection = (Integer) v.getTag();
                    for (int j = 0; j < mViews.size(); j++) {
                        mViews.get(j).findViewById(R.id.inner_container_rl).setBackgroundColor(
                                (j == selection ? getResources().getColor(R.color.red_normal_color) : Color.TRANSPARENT));
                    }
                    if (TextUtils.isEmpty(uploadingMedia.name)) {
                        mAboutEt.setText(new File(uploadingMedia.path).getName());
                    } else {
                        mAboutEt.setText(uploadingMedia.name);
                    }
                }
            });

            view.findViewById(R.id.close_iv).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (mUploadingMedias.size() == 1) {
                        DialogUtil.showOkDialogNonCancelableTwoButtons(v.getContext(),
                                getResources().getString(R.string.Album_can_not_be_created_without_photos___), "",
                                new OnOkButtonListner() {
                                    @Override
                                    public void onOkBUtton() {
                                        setResultAndFinish(RESULT_CANCELED);
                                    }
                                });

                    } else {

                        mThumbContainerLl.removeView(view);
                        mUploadingMedias.remove(uploadingMedia);
                        mMediaCountTv.setText(getString(R.string.all) + " " + mUploadingMedias.size());
                        mUploadCountTv.setText(getString(R.string.Upload) + " " + mUploadingMedias.size()
                                + " Photo" + (mUploadingMedias.size() == 1 ? "" : "s"));
                        mSelMediaPos--;

                        if (mUploadingMedias.size() == 1) {
                            mMediaCountTv.setVisibility(View.GONE);
                        }

                    }
                }
            });
        }
    }

    private void nutralizeAllTextColor() {
        mMediaCountTv.setBackgroundResource(R.drawable.white_rect);
        mMediaCountTv.getBackground().setColorFilter(getResources()
                .getColor(R.color.red_normal_color), Mode.MULTIPLY);
    }

    @Override
    protected void onStart() {
        mMediaUploadRequest.setActivityStatus(true);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mMediaUploadRequest.setActivityStatus(false);
        super.onStop();
    }

    private int mCurrIndex = 0;
    private List<UploadingMedia> mUploadingMedias = new ArrayList<UploadingMedia>();
    private ArrayList<NewMedia> mNewMeidas = new ArrayList<NewMedia>();
    private RequestListener mRequestListener = new RequestListener() {
        @Override
        public void onComplete(boolean success, Object data, int totalRecords) {
            if (success) {
                AlbumMedia mAlbumMedia = (AlbumMedia) data;
                mNewMedia.MediaGUID = mAlbumMedia.MediaGUID;
                mNewMeidas.add(mNewMedia);
                mCurrIndex++;
                uploadMedias();
            } else if (null != data) {
                dismissProgressDialog();
                if (null != data && (((String) data).contains("space") || ((String) data).contains("storage"))) {
                    DialogUtil.showOkCancelDialog(UploadMediaActivity.this, R.string.ok_label, R.string.cancel_caps,
                            getString(R.string.Upgrade_Your_Storage),
                            getString(R.string.You_dont_have_sufficientt_space), new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // Toast.makeText(UploadMediaActivity.this,
                                    // "Redirect to buy space page",
                                    // Toast.LENGTH_SHORT).show();
                                    mErrorLayout.showError("Redirect to buy space page", true);
                                }
                            }, null);
                } else {
                    DialogUtil.showOkCancelDialog(UploadMediaActivity.this, R.string.ok_label, R.string.cancel_caps,
                            null, "Error occured while uploading media! Retry uploading?",
                            new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    uploadMedias();
                                }
                            }, null);
                }
            }
        }
    };

    private ProgressDialog mProgressDialog = null;
    private NewMedia mNewMedia = null;

    private void uploadMedias() {

        if (mCurrIndex >= mUploadingMedias.size()) {
            dismissProgressDialog();
            mUploadingMedias.clear();
            setResultAndFinish(RESULT_OK);
            return;
        }

        if (Config.DEBUG) {
            Log.d(TAG, "uploadMedias mUploadingMedias=" + mUploadingMedias.size() + ", mCurrIndex=" + mCurrIndex);
        }

        if (0 == mCurrIndex) {
            mProgressDialog = DialogUtil.createDiloag(UploadMediaActivity.this, composeProgressMessage());
        }

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }

        mProgressDialog.setMessage(composeProgressMessage());

        final UploadingMedia uploadingMedia = mUploadingMedias.get(mCurrIndex);
        //Bitmap bitmap = ImageUtil.getBitmap(uploadingMedia.path, 1500, 1500);
        File fileOld = new File(uploadingMedia.path);
        File newFile = get_CompressedFile(fileOld);

        mNewMedia = new NewMedia("", (TextUtils.isEmpty(uploadingMedia.name)
                ? newFile.getName() : uploadingMedia.name), "description",
                "keyword", "sportsID");
        mMediaUploadRequest.uploadMediaInServer(true, newFile, mNewMedia.Caption,
                MediaUploadRequest.MODULE_ID_ALBUM,
                mLogedInUserModel.mUserGUID, MediaUploadRequest.TYPE_ALBUM);
    }

    public File get_CompressedFile(File nFile) {

        long size_file = getFileSize(nFile);
        size_file = (size_file) / 1000;// in Kb now
        int ample_size = 1;
        if (size_file > 251 && size_file < 1500) {
            ample_size = 2;
        } else if (size_file >= 1500 && size_file < 4500) {
            ample_size = 4;
        } else if (size_file >= 4500 && size_file <= 8000) {
            ample_size = 8;
        } else if (size_file > 8000) {
            ample_size = 16;
        }
        Bitmap bitmap = null;
        BitmapFactory.Options bitoption = new BitmapFactory.Options();
        bitoption.inSampleSize = ample_size;
        Bitmap bitmapPhoto = BitmapFactory.decodeFile(nFile.getAbsolutePath(), bitoption);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(nFile.getAbsolutePath());
        } catch (IOException e) {
            // Auto-generated catch block
            e.printStackTrace();
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        Matrix matrix = new Matrix();

        if ((orientation == 3)) {
            matrix.postRotate(180);

        } else if (orientation == 6) {
            matrix.postRotate(90);

        } else if (orientation == 8) {
            matrix.postRotate(270);

        } else {
            matrix.postRotate(0);

        }

        bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0, bitmapPhoto.getWidth(),
                bitmapPhoto.getHeight(), matrix, true);

        File newFile = SaveBitmap(bitmap);
        return newFile;
    }

    public long getFileSize(final File file) {
        if (file == null || !file.exists())
            return 0;
        if (!file.isDirectory())
            return file.length();
        final List<File> dirs = new LinkedList<File>();
        dirs.add(file);
        long result = 0;
        while (!dirs.isEmpty()) {
            final File dir = dirs.remove(0);
            if (!dir.exists())
                continue;
            final File[] listFiles = dir.listFiles();
            if (listFiles == null || listFiles.length == 0)
                continue;
            for (final File child : listFiles) {
                result += child.length();
                if (child.isDirectory())
                    dirs.add(child);
            }
        }
        return result;
    }

    private File SaveBitmap(Bitmap photo_) {

        File baseDIR = new File(
                Environment.getExternalStorageDirectory() + File.separator
                        + this.getResources().getString(
                        R.string.app_name));
        if (!baseDIR.exists()) {
            baseDIR.mkdir();
            baseDIR.mkdirs();
        }

        File file = new File(baseDIR, String.valueOf(System.currentTimeMillis()) + ".jpg");

        try {
            FileOutputStream out = new FileOutputStream(file);
            photo_.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }

    private String composeProgressMessage() {
        return String.format(getString(R.string.Uploading_d_of_d), (mCurrIndex + 1), mUploadingMedias.size());
    }

    private void dismissProgressDialog() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void setResultAndFinish(int resultCode) {
        Utility.hideSoftKeyboard(mAboutEt);
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("newMeidas", mNewMeidas);
        setResult(resultCode, intent);
        finish();
    }

    public void loadVideoThumb(final String path, final ImageView imageView) {
        imageView.setImageResource(R.drawable.ic_launcher);
        new Thread(new Runnable() {

            @Override
            public void run() {
                File file = new File(path);
                final Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(file.getPath(),
                        MediaStore.Images.Thumbnails.MINI_KIND);

                imageView.post(new Runnable() {

                    @Override
                    public void run() {
                        imageView.setImageBitmap(thumbnail);
                    }
                });
            }
        }).start();
    }
}
