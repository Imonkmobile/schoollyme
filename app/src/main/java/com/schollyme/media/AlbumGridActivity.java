package com.schollyme.media;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;

import com.schollyme.activity.BaseActivity;
import com.schollyme.R;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AlbumListRequest;
import com.vinfotech.widget.PagerSlidingTabStrip;

public class AlbumGridActivity extends BaseActivity implements OnClickListener {

    private static final int PAGE_COUNT = 2;
    private String mUserGUID = "";
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private ViewPager mGenericVp;
    private GridPagerAdapter mGridPagerAdapter;
    private static boolean sReloadData = false;
    private LogedInUserModel mLogedInUserModel;
    private String mHeaderTittle;
    private ErrorLayout mErrorLayout;
    private int FragmentToOpen = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_grid_activity);

        getIntentData();
        HeaderLayout headerLayout = new HeaderLayout(findViewById(R.id.header_layout));
        headerLayout.setHeaderValues(R.drawable.icon_back, mHeaderTittle, 0);
        headerLayout.setListenerItI(this, this);

        mGridPagerAdapter = new GridPagerAdapter(getSupportFragmentManager());
        mLogedInUserModel = new LogedInUserModel(this);

        mErrorLayout = new ErrorLayout(findViewById(R.id.main_container_ll));
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.sliding_tabs_psts);
        mGenericVp = (ViewPager) findViewById(R.id.generic_vp);
        mGenericVp.setOffscreenPageLimit(PAGE_COUNT);
        mGenericVp.setAdapter(mGridPagerAdapter);
        mPagerSlidingTabStrip.setViewPager(mGenericVp, this);
        FragmentToOpen = getIntent().getIntExtra("FragmentToOpen", 0);
        mGenericVp.setCurrentItem(FragmentToOpen, true);

    }

    public static boolean isMe(String UserGUID, LogedInUserModel logedInUserModel) {
        if (null != UserGUID && null != logedInUserModel) {
            return UserGUID.trim().equalsIgnoreCase(logedInUserModel.mUserGUID.trim());
        }
        return false;
    }

    private void getIntentData() {
        mUserGUID = getIntent().getStringExtra("USERID");

        mHeaderTittle = getIntent().getStringExtra("USERNAME");
    }

    public static Intent getIntent(Context context, String mUserGUID, String userName) {
        Intent intent = new Intent(context, AlbumGridActivity.class);
        intent.putExtra("USERNAME", userName);
        intent.putExtra("USERID", mUserGUID);
        return intent;
    }

    public static Intent getIntent(Context context, String mUserGUID, String userName, int FragmentToOpen) {
        Intent intent = new Intent(context, AlbumGridActivity.class);
        intent.putExtra("USERNAME", userName);
        intent.putExtra("USERID", mUserGUID);
        intent.putExtra("FragmentToOpen", FragmentToOpen);
        return intent;
    }

    public static void setReloadData(boolean reload) {
        sReloadData = reload;
    }

    public class GridPagerAdapter extends FragmentPagerAdapter {

        private AlbumGridFragment mPhotoAlbumGridFragment, mVideoAlbumGridFragment;

        public GridPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return (getString(position == 0 ? R.string.photos_label : R.string.videos_label));
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                mPhotoAlbumGridFragment = AlbumGridFragment.newInstance(mErrorLayout, mUserGUID,
                        mLogedInUserModel, AlbumListRequest.ALBUM_TYPE_PHOTO);
                return mPhotoAlbumGridFragment;
            } else {
                mVideoAlbumGridFragment = AlbumGridFragment.newInstance(mErrorLayout, mUserGUID,
                        mLogedInUserModel, AlbumListRequest.ALBUM_TYPE_VIDEO);
                return mVideoAlbumGridFragment;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_button:
                this.finish();
                break;

            default:
                break;
        }
    }

    @Override
    protected void onRestart() {
        if (sReloadData) {
            sReloadData = false;
            int currentItem = mGenericVp.getCurrentItem();
            mGridPagerAdapter = new GridPagerAdapter(getSupportFragmentManager());
            mGenericVp.setAdapter(mGridPagerAdapter);
            mGenericVp.setCurrentItem(currentItem, false);
        }
        super.onRestart();
    }

}
