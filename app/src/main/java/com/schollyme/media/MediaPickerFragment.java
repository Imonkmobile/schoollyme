package com.schollyme.media;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schollyme.R;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.ImageUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


/**
 * Activity class. This may be useful in
 * Media Pick from gallery or camera implementation
 * 
 * @author Ravi Bhandari
 * 
 */
public abstract class MediaPickerFragment extends BaseFragment {

	protected static final int IMAGE_DIMENSION = 1080;
	protected static final int REQ_CODE_TAKE_FROM_CAMERA = 500;
	protected static final int REQ_CODE_PICK_FROM_GALLERY = 501;
	protected static final int REQ_CODE_CROP_PHOTO = 502;
	protected static final int REQ_CODE_RECORD_VIDEO = 503;

	private static final String IMAGE_UNSPECIFIED = "image/*";
	protected Uri capturedImageUri, cropImageUri;
	protected Bitmap capturedImageBitmap = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	protected void initTmpUris() {
		File proejctDirectory = new File(Environment.getExternalStorageDirectory() + File.separator + "SchollyMeCache");
		if (!proejctDirectory.exists()) {
			proejctDirectory.mkdir();
		} else {
			// delete all old files
			for (File file : proejctDirectory.listFiles()) {
				if (file.getName().startsWith("tmp_")) {
					file.delete();
				}
			}
		}
		// Construct temporary image path and name to save the taken
		// photo
		capturedImageUri = Uri.fromFile(new File(proejctDirectory, "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
		File extraOutputFile = new File(proejctDirectory, "croped_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
		extraOutputFile.setWritable(true);
		cropImageUri = Uri.fromFile(extraOutputFile);
	}

	private final int mOutputX =  960, mAspectX = 1;
	private int mOutputY =  960, mAspectY = 1;

	protected void openCamera(float aspect) {
		mOutputY = (int)(mOutputX / aspect);
		mAspectY = (int) aspect;
		openCamera();
	}

	/**
	 * This method use for take picture from camera and crop function
	 */

	protected void openCamera() {
		initTmpUris();

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
		intent.putExtra("return-data", true);
		try {
			// Start a camera capturing activity
			// REQUEST_CODE_TAKE_FROM_CAMERA is an integer tag you
			// defined to identify the activity in onActivityResult()
			// when it returns
			startActivityForResult(intent, REQ_CODE_TAKE_FROM_CAMERA);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
		return;
	}

	protected void openGallery(float aspect) {
		mOutputY = (int)(mOutputX / aspect);
		mAspectY = (int) aspect;
		openGallery();
	}

	/**
	 * This method use for open gallery and crop image using default crop of
	 * android
	 */
	protected void openGallery() {
		System.out.println("MediaPickerActivity - openGallery SDK_INT=" + Build.VERSION.SDK_INT);
		if (Build.VERSION.SDK_INT < 19) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent, "Complete action using"), REQ_CODE_PICK_FROM_GALLERY);
		} else {
			Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(intent, REQ_CODE_PICK_FROM_GALLERY);
		}
	}

	protected void openVideoCamera() {
		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 15);
		intent.putExtra("EXTRA_VIDEO_QUALITY", 0);
		startActivityForResult(intent, MediaPickerFragment.REQ_CODE_RECORD_VIDEO);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case REQ_CODE_TAKE_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				System.out.println("REQ_CODE_TAKE_FROM_CAMERA imageUri: " + capturedImageUri);

				cropImage();
			} else {
				onMediaPickCanceled(REQ_CODE_TAKE_FROM_CAMERA);
			}
			break;
		case REQ_CODE_PICK_FROM_GALLERY:
			if (resultCode == Activity.RESULT_OK && data.getData() != null) {
				initTmpUris();
				capturedImageUri = data.getData();
				cropImage();
			} else {
				onMediaPickCanceled(REQ_CODE_PICK_FROM_GALLERY);
			}
			break;
		case REQ_CODE_CROP_PHOTO:
			if (resultCode == Activity.RESULT_OK) {
				try {
					capturedImageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropImageUri);
					onCameraImageSelected(cropImageUri.toString(), capturedImageBitmap);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				onMediaPickCanceled(REQ_CODE_CROP_PHOTO);
			}
			break;
		case REQ_CODE_RECORD_VIDEO:
			if (resultCode == Activity.RESULT_OK) {
				Uri vid = data.getData();
				onVideoCaptured(getVideoPathFromURI(vid));
			} else {
				onMediaPickCanceled(REQ_CODE_RECORD_VIDEO);
			}
			break;

		default:
			break;
		}
	}

	/*
	 * This method use for Crop image taken from camera
	 */
	private void cropImage() {
		// Use existing crop activity.
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");
		intent.setDataAndType(capturedImageUri, IMAGE_UNSPECIFIED); // Uri to the image you want to crop
		/*intent.putExtra("outputX", 296);
		intent.putExtra("outputY", 296);*/
		intent.putExtra("outputX", mOutputX);
		intent.putExtra("outputY", mOutputY);
		intent.putExtra("aspectX", mAspectX);
		intent.putExtra("aspectY", mAspectY);
		intent.putExtra("scale", true);
		intent.putExtra("circleCrop", true);
		intent.putExtra("return-data", false);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, cropImageUri);
		startActivityForResult(intent, REQ_CODE_CROP_PHOTO);
		
		
		/*Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(capturedImageUri, IMAGE_UNSPECIFIED);
		intent.putExtra("outputX", IMAGE_DIMENSION);
		intent.putExtra("outputY", IMAGE_DIMENSION);
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("scale", true);
		// intent.putExtra("return-data", true);
		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, cropImageUri);
		startActivityForResult(intent, REQ_CODE_CROP_PHOTO);*/
	}

	private String getVideoPathFromURI(Uri contentUri) {
		String videoPath = null;
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
		if (cursor.moveToFirst()) {
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			videoPath = cursor.getString(column_index);
		}
		cursor.close();
		return videoPath;
	}

	protected void displayImagePickerPopup() {
		DialogUtil.showListDialog(getActivity(), 0, new DialogUtil.OnItemClickListener() {
			@Override
			public void onItemClick(int position, String item) {
				if (position == 0) {
					openCamera(1.5f);
				} else {
					openGallery(1.5f);
				}
			}

			@Override
			public void onCancel() {

			}
		}, getActivity().getString(R.string.take_picture), getActivity().getString(R.string.load_from_library));
	}

	protected void uploadMediaServerRequest() {
		MediaUploadRequest mRequest = new MediaUploadRequest(getActivity(), false);
		String fileName = ImageUtil.getFileNameFromURI(cropImageUri, getActivity());
		final File mFile = new File(cropImageUri.getPath());
		final LogedInUserModel mLoggedInUserModel = new LogedInUserModel(getActivity());
		BaseRequest.setLoginSessionKey(mLoggedInUserModel.mLoginSessionKey);
		mRequest.uploadMediaInServer(true, mFile, fileName, MediaUploadRequest.MODULE_ID_USERS, mLoggedInUserModel.mUserGUID,
				MediaUploadRequest.TYPE_GROUP_COVER);
		mRequest.setRequestListener(new BaseRequest.RequestListener() {

			@Override
			public void onComplete(boolean success, Object data, int totalRecords) {
				if (success) {
					AlbumMedia albumMedia = (AlbumMedia) data;
					onBannerUpdate(true, albumMedia.ImageServerPath + "/", albumMedia.ImageName);
				} else {
					onBannerUpdate(false, mFile.getPath(), mFile.getName());
				}
			}
		});
	}

	protected abstract void onBannerUpdate(boolean success, String imageServerPath, String imageName);

	protected abstract void onGalleryImageSelected(String fileUri, Bitmap bitmap);

	protected abstract void onCameraImageSelected(String fileUri, Bitmap bitmap);

	protected abstract void onVideoCaptured(String videoPath);

	protected abstract void onMediaPickCanceled(int reqCode);

}
