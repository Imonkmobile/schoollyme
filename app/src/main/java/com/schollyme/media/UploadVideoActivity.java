package com.schollyme.media;

import java.io.File;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.activity.BaseActivity;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.activity.SportsListActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.handler.ErrorLayout.MsgType;
import com.schollyme.model.AlbumMedia;
import com.schollyme.model.LogedInUserModel;
import com.schollyme.model.NewMedia;
import com.schollyme.model.SportsModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.MediaUploadRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.FontLoader;
import com.vinfotech.utility.Utility;

public class UploadVideoActivity extends BaseActivity implements OnClickListener {

    private static final String TAG = UploadVideoActivity.class.getSimpleName();
    public static final int REQ_CODE_UPLOAD_VIDEO_ACTIVITY = 1024;

    private TextView mUploadingInTxtTv;
    private TextView mUploadingInTv, mDurationTv;
    private ImageView mMediaIv, mCloseIv;
    private EditText mCaptionEt, mSportEt, mKeywordEt;

    private HeaderLayout mHeaderLayout;
    private ErrorLayout mErrorLayout;
    private MediaUploadRequest mMediaUploadRequest;
    private String mAlbumName, mVideoPath;
    private LogedInUserModel mLogedInUserModel;
    private ArrayList<SportsModel> mSelections;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_video_activity);

        mHeaderLayout = new HeaderLayout(findViewById(R.id.container_ll));
        mHeaderLayout.setHeaderValues(R.drawable.icon_back, getResources().getString(R.string.Add_Video), R.drawable.ic_white_tick);
        mHeaderLayout.setListenerItI(this, this);
        mErrorLayout = new ErrorLayout(findViewById(R.id.container_ll));
        mLogedInUserModel = new LogedInUserModel(this);

        mAlbumName = getIntent().getStringExtra("albumName");
        mVideoPath = getIntent().getStringExtra("videoPath");

        if (Config.DEBUG) {
            Log.d(TAG, "onCreate mAlbumName=" + mAlbumName + ", mVideoPath=" + mVideoPath);
        }
        if (TextUtils.isEmpty(mVideoPath)) {
            Toast.makeText(this, "Invalid video media!", Toast.LENGTH_SHORT).show();
            setResultAndFinish(RESULT_CANCELED);
            return;
        }

        mUploadingMedia = new UploadingMedia("", mVideoPath);
        mMediaUploadRequest = new MediaUploadRequest(this, true);
        mMediaUploadRequest.setRequestListener(mRequestListener);

        mCaptionEt = (EditText) findViewById(R.id.caption_et);
        mSportEt = (EditText) findViewById(R.id.sport_et);
        mKeywordEt = (EditText) findViewById(R.id.keyword_et);

        mUploadingInTxtTv = (TextView) findViewById(R.id.uploading_in_txt_tv);
        mUploadingInTv = (TextView) findViewById(R.id.uploading_in_tv);
        mMediaIv = (ImageView) findViewById(R.id.media_iv);
        mCloseIv = (ImageView) findViewById(R.id.close_iv);
        mDurationTv = (TextView) findViewById(R.id.duration_tv);

        mSportEt.setFocusable(false);
        mSportEt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(SportsListActivity.getIntent(v.getContext(), false, mSelections),
                        SportsListActivity.REQ_CODE_SPORT_LIST_ACTIVITY);
            }
        });
        FontLoader.setRobotoRegularTypeface(mUploadingInTxtTv, mUploadingInTv, mCaptionEt, mSportEt, mKeywordEt);
        showVideoInfo();
        validateVidSizeWithDialogs(this, mVideoPath, null);
    }

    public static Intent getIntent(Context context, String albumName, String videoPath) {
        Intent intent = new Intent(context, UploadVideoActivity.class);
        intent.putExtra("albumName", albumName);
        intent.putExtra("videoPath", videoPath);
        return intent;

    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(mUploadingMedia.path)) {
            DialogUtil.showOkCancelDialog(this, R.string.ok_label, R.string.cancel_caps,
                    getResources().getString(R.string.Discard_Video),
                    getResources().getString(R.string.Video_files_are_not_uploaded), new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            setResultAndFinish(RESULT_CANCELED);
                        }
                    }, null);
        } else {
            setResultAndFinish(RESULT_CANCELED);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_button:
                if (!TextUtils.isEmpty(mUploadingMedia.path)) {
                    DialogUtil.showOkCancelDialog(this, R.string.ok_label, R.string.cancel_caps,
                            getResources().getString(R.string.Discard_Video),
                            getResources().getString(R.string.Video_files_are_not_uploaded), new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    setResultAndFinish(RESULT_CANCELED);
                                }
                            }, null);
                } else {
                    setResultAndFinish(RESULT_CANCELED);
                }
                break;
            case R.id.right_button:
                if (isValid() && validateVidSizeWithDialogs(v.getContext(), mVideoPath, mErrorLayout)) {
                    uploadMedias();
                }
                break;

            default:
                break;
        }
    }

    private String mCaption, mSportIds = "", mKeywords;

    private boolean isValid() {
        // if (TextUtils.isEmpty(mSportIds)) {
        // mErrorLayout.showError("Please select Sport", true, MsgType.Error);
        // return false;
        // }
        mCaption = mCaptionEt.getText().toString().trim();
        mKeywords = mKeywordEt.getText().toString().trim();

        return true;
    }

    private void showVideoInfo() {
        mUploadingInTv.setText(mAlbumName);
        loadVideoThumb(mUploadingMedia.path, mMediaIv);
        mDurationTv.setText(AlbumMedia.getVideoDuration(MediaPlayer.create(this,
                Uri.parse(mUploadingMedia.path)).getDuration()));
    }

    @Override
    protected void onStart() {
        mMediaUploadRequest.setActivityStatus(true);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mMediaUploadRequest.setActivityStatus(false);
        super.onStop();
    }

    private class UploadingMedia {
        public String name;
        public String path;

        public UploadingMedia(String name, String path) {
            super();
            this.name = name;
            this.path = path;
        }

        @Override
        public String toString() {
            return "UploadingMedia [name=" + name + ", path=" + path + "]";
        }
    }

    private UploadingMedia mUploadingMedia;
    private ArrayList<NewMedia> mNewMeidas = new ArrayList<NewMedia>();
    private RequestListener mRequestListener = new RequestListener() {
        @Override
        public void onComplete(boolean success, Object data, int totalRecords) {
            if (success) {
                AlbumMedia mAlbumMedia = (AlbumMedia) data;
                mNewMedia.MediaGUID = mAlbumMedia.MediaGUID;
                mNewMeidas.add(mNewMedia);
                setResultAndFinish(RESULT_OK);
            } else if (null != data) {
                dismissProgressDialog();
                if (null != data && (((String) data).contains("space") || ((String) data).contains("storage"))) {
                    DialogUtil.showOkCancelDialog(UploadVideoActivity.this, R.string.ok_label, R.string.cancel_caps,
                            getString(R.string.Upgrade_Your_Storage),
                            getString(R.string.You_dont_have_sufficientt_space), new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    mErrorLayout.showError(getString(R.string.No_space_available), true);
                                }
                            }, null);
                } else {
                    DialogUtil.showOkCancelDialog(UploadVideoActivity.this, R.string.ok_label, R.string.cancel_caps,
                            null, getString(R.string.Error_occurred_while_uploading_media),
                            new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    uploadMedias();
                                }
                            }, null);
                }
            }
        }
    };

    private ProgressDialog mProgressDialog = null;
    private NewMedia mNewMedia = null;

    private void uploadMedias() {
        if (Config.DEBUG) {
            Log.d(TAG, "uploadMedias mUploadingMedia=" + mUploadingMedia);
        }
        mProgressDialog = DialogUtil.createDiloag(UploadVideoActivity.this, composeProgressMessage());
        mProgressDialog.show();
        mProgressDialog.setMessage(composeProgressMessage());

        File file = new File(mUploadingMedia.path);
        mNewMedia = new NewMedia("", (TextUtils.isEmpty(mCaption) ? file.getName() : mCaption),
                mCaption, mKeywords, mSportIds);
        mMediaUploadRequest.uploadMediaInServer(false, file, mNewMedia.Caption, MediaUploadRequest.MODULE_ID_ALBUM,
                mLogedInUserModel.mUserGUID, MediaUploadRequest.TYPE_ALBUM);
    }

    private String composeProgressMessage() {
        return "Uploading video... ";
    }

    private void dismissProgressDialog() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void setResultAndFinish(int resultCode) {
        Utility.hideSoftKeyboard(mCaptionEt);
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("newMeidas", mNewMeidas);
        setResult(resultCode, intent);
        finish();
    }

    public void loadVideoThumb(final String path, final ImageView imageView) {
        imageView.setImageResource(R.drawable.ic_deault_media);
        new Thread(new Runnable() {

            @Override
            public void run() {
                File file = new File(path);
                final Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(file.getPath(),
                        MediaStore.Images.Thumbnails.MINI_KIND);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (null != thumbnail && null != imageView) {
                            imageView.setImageBitmap(thumbnail);
                        }
                    }
                });
            }
        }).start();
    }

    /**
     * Show ok dialog if not valid video
     *
     * @param videoPath
     * @return
     */
    public boolean validateVidSizeWithDialogs(Context context, String videoPath, ErrorLayout errorLayout) {
        File file = new File(videoPath);
        if (!file.exists()) {
            if (null != errorLayout) {
                errorLayout.showError(context.getString(R.string.Invalid_file), true, MsgType.Error);
            } else {
                DialogUtil.showOkDialog(context, context.getString(R.string.Invalid_file), "");
            }
            return false;
        } else if (file.length() > Config.MAX_VIDEO_LEN_BYTES) {
            if (null != errorLayout) {
                errorLayout.showError(context.getString(R.string.Video_is_larger_than_40MB), true, MsgType.Error);
            } else {
                DialogUtil.showOkDialog(context, context.getString(R.string.Video_is_larger_than_40MB),
                        context.getString(R.string.Video_Limt_Crossed));
            }
            return false;
        } else if (getVideoDuration(videoPath) > Config.MAX_VIDEO_LEN_MILLIS) {
            if (null != errorLayout) {
                errorLayout.showError(context.getString(R.string.Video_is_longer_than_6), true, MsgType.Error);
            } else {
                DialogUtil.showOkDialog(context, context.getString(R.string.Video_is_longer_than_6),
                        context.getString(R.string.Video_Limt_Crossed));
            }
            return false;
        }

        return true;
    }

    public static long getVideoDuration(String videoPath) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(videoPath);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        try {
            return Long.parseLong(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    private String getCsv(boolean id) {
        StringBuilder sb = new StringBuilder();
        if (null != mSelections) {
            for (int i = 0; i < mSelections.size(); i++) {
                SportsModel sportsModel = mSelections.get(i);
                sb.append(id ? sportsModel.mSportsID : sportsModel.mSportsName);
                if (i < mSelections.size() - 1) {
                    sb.append(", ");
                }
            }
        }
        return sb.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SportsListActivity.REQ_CODE_SPORT_LIST_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    mSelections = data.getParcelableArrayListExtra("data");
                    mSportIds = getCsv(true);
                    mSportEt.setText(getCsv(false));
                }
                break;
            case 2020:
                if (resultCode == RESULT_OK) {

                    // ArrayList<NewYoutubeUrl> newYoutubeUrls =
                    // data.getParcelableArrayListExtra("newYoutubeUrls");
                    // if (TextUtils.isEmpty(mAlbum.AlbumGUID)) {
                    // mAlbumCreateRequest.createAlbumInServer(null,
                    // mAlbum.AlbumName, mAlbum.Description, mAlbum.Visibility,
                    // mAlbum.AlbumType, null, newYoutubeUrls);
                    // } else {
                    // mAlbumCreateRequest.addMediaInServer(mAlbum.AlbumGUID,
                    // mAlbum.AlbumName, mAlbum.Description, mAlbum.Visibility,
                    // mAlbum.AlbumType, null, newYoutubeUrls);
                    // }
                    // if (Config.DEBUG) {
                    // Log.d(TAG, "onActivityResult newYoutubeUrls =" +
                    // newYoutubeUrls + ", size=" + (null == newYoutubeUrls ? 0 :
                    // 1));
                    // }
                }
                break;

            default:
                break;
        }
    }
}
