package com.schollyme.media;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.schollyme.R;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.friends.FriendsActivity;
import com.schollyme.handler.ErrorLayout;
import com.schollyme.model.Album;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AlbumListRequest;
import com.vinfotech.widget.PagerSlidingTabStrip;
import com.vinfotech.widget.PagerSlidingTabStrip.NotificationTabProvider;

import java.util.HashMap;
import java.util.Map;

public class MediaMainFragment extends BaseFragment {

    private static final int PAGE_COUNT = 2;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private ViewPager mGenericVp;
    private GridPagerAdapter mGridPagerAdapter;
    private ErrorLayout mErrorLayout;
    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;
    private String mUserGUID = "";
    private Album mCreatorAlbum;
    private String mAlbumType;

    private Context mContext;

    public static MediaMainFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {
        MediaMainFragment friendsTopFragment = new MediaMainFragment();
        friendsTopFragment.mCurrentPageToShow = currentPageToShow;
        friendsTopFragment.mHeaderLayout = headerLayout;
        friendsTopFragment.mShowBackBtn = showBack;

        return friendsTopFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.media_main_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();
        mUserGUID = getArguments().getString("mUserGUID");

        initHeader(getResources().getString(R.string.photos_label));
        mAlbumType = AlbumListRequest.ALBUM_TYPE_PHOTO;
        mGridPagerAdapter = new GridPagerAdapter(getChildFragmentManager());
        mErrorLayout = new ErrorLayout(view.findViewById(R.id.main_container_ll));

        mPagerSlidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs_psts);
        mGenericVp = (ViewPager) view.findViewById(R.id.generic_vp);
        mGenericVp.setOffscreenPageLimit(1);
        mGenericVp.setAdapter(mGridPagerAdapter);
        mPagerSlidingTabStrip.setAllCaps(true);
        mPagerSlidingTabStrip.setOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                BaseFragment baseFragment = mGridPagerAdapter.getBaseFragment(position);
                baseFragment.onLocaleUpdate();
                if(position == 0) {
                    mAlbumType = AlbumListRequest.ALBUM_TYPE_PHOTO;
                    initHeader(getResources().getString(R.string.photos_label));
                } else {
                    mAlbumType = AlbumListRequest.ALBUM_TYPE_VIDEO;
                    initHeader(getResources().getString(R.string.videos_label));
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });
        mPagerSlidingTabStrip.setViewPager(mGenericVp, getActivity());
        mGenericVp.postDelayed(new Runnable() {
            @Override
            public void run() {
                mGenericVp.setCurrentItem(mCurrentPageToShow);
            }
        }, 150);

        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) view.findViewById(R.id.createAlbumImg);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                fabMenu.collapse();
                System.out.println("album type  1"+mAlbumType);
                if (mAlbumType.equalsIgnoreCase(AlbumListRequest.ALBUM_TYPE_PHOTO)) {
                    mCreatorAlbum = new Album(AlbumListRequest.ALBUM_TYPE_PHOTO);
                } else if (mAlbumType.equalsIgnoreCase(AlbumListRequest.ALBUM_TYPE_VIDEO)) {
                    mCreatorAlbum = new Album(AlbumListRequest.ALBUM_TYPE_VIDEO);
                }else{
                    System.out.println("album type  "+mAlbumType);
                }

                startActivityForResult(CreateAlbumActivity.getIntent(getActivity(), mCreatorAlbum),
                        CreateAlbumActivity.REQ_CODE_CREATE_ALBUM_ACTIVITY);
            }

            @Override
            public void onMenuCollapsed() {

            }
        });


    }

    public void initHeader(String title) {

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu, title, 0);
        mHeaderLayout.setListenerItI(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShowBackBtn) {
                    if (((FriendsActivity) getActivity()).mFromActivity.equalsIgnoreCase("GCMIntentService")) {
                        Intent intent = DashboardActivity.getIntent(getActivity(), 12, "FriendsTopFragment");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getActivity().finish();
                    } else
                        getActivity().finish();
                } else {
                    ((DashboardActivity) mContext).sliderListener();
                }
            }
        }, null);
    }

    public class GridPagerAdapter extends FragmentStatePagerAdapter implements NotificationTabProvider {

        private Map<Integer, BaseFragment> mBaseFragments;
        private String friendCount = "";

        public GridPagerAdapter(FragmentManager fm) {
            super(fm);
            mBaseFragments = new HashMap<Integer, BaseFragment>();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String tabTitle = "";
            switch (position) {
                case 0:
                    tabTitle = getString(R.string.photos_label);
                    break;
                case 1:
                    tabTitle = getString(R.string.videos_label);
                    break;
            }
            return tabTitle;
        }

        @Override
        public int getCount() {

            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            BaseFragment mFragment = null;
            switch (position) {
                case 0:
                    mFragment = PhotoGridFragment.newInstance(mErrorLayout, mUserGUID, AlbumListRequest.ALBUM_TYPE_PHOTO);
                    break;
                case 1:
                    mFragment = PhotoGridFragment.newInstance(mErrorLayout, mUserGUID, AlbumListRequest.ALBUM_TYPE_VIDEO);
                    break;
            }
            mBaseFragments.put(position, mFragment);
            return mFragment;
        }

        public BaseFragment getBaseFragment(int position) {
            return mBaseFragments.get(position);
        }

        @Override
        public String getNotificationText(int position) {

            if (position == 0) {
                return friendCount;
            } else {
                return "";
            }
        }
    }
}