package com.schollyme.media;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.BaseFragmentActivity;
import com.schollyme.R;
import com.schollyme.activity.BaseActivity;
import com.schollyme.activity.DashboardActivity;
import com.schollyme.fragments.BaseFragment;
import com.schollyme.inbox.InboxFragment;
import com.schollyme.model.Album;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.header.HeaderLayout;
import com.vinfotech.request.AlbumDetailRequest;
import com.vinfotech.server.BaseRequest.RequestListener;
import com.vinfotech.utility.Config;

public class MediaTypeFragment extends BaseFragment {

    //private String mUserGUID = "";
    private View mView;
    private Context mContext;
    private TextView mKiltTxt, mPepTalkTxt, mPlayListTxt;

    private int mCurrentPageToShow;
    private HeaderLayout mHeaderLayout;
    private boolean mShowBackBtn = true;

    public static BaseFragment newInstance(int currentPageToShow, HeaderLayout headerLayout, boolean showBack) {

        MediaTypeFragment mFragment = new MediaTypeFragment();
        mFragment.mCurrentPageToShow = currentPageToShow;
        mFragment.mHeaderLayout = headerLayout;
        mFragment.mShowBackBtn = showBack;

        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.media_type_fragment, null);
        mContext = getActivity();

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initHeader();
        mKiltTxt = (TextView) view.findViewById(R.id.kiltd_media_Txt);
        mPepTalkTxt = (TextView) view.findViewById(R.id.peptalk_media_Txt);
        mPlayListTxt = (TextView) view.findViewById(R.id.playlist_media_Txt);

        mKiltTxt.setOnClickListener((View.OnClickListener) getActivity());
        mPepTalkTxt.setOnClickListener((View.OnClickListener) getActivity());
        mPlayListTxt.setOnClickListener((View.OnClickListener) getActivity());
    }

    private void initHeader() {

        String title = getArguments().getString("HeaderText");

        mHeaderLayout.setHeaderValues(mShowBackBtn ? R.drawable.icon_back : R.drawable.icon_menu, title, 0);
        mHeaderLayout.setListenerItI(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DashboardActivity) getActivity()).sliderListener();
            }
        }, null);
    }
}
