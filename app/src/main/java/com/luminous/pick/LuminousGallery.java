package com.luminous.pick;

import java.util.ArrayList;

public class LuminousGallery {
	public static final int REQ_CODE_PICK_SINGLE = 8888;
	public static final int REQ_CODE_PICK_MULTIPLE = 8889;

	public String sdcardPath;
	public boolean isSeleted = false;

	public LuminousGallery(String sdcardPath) {
		super();
		this.sdcardPath = sdcardPath;
		this.isSeleted = false;
	}

	public enum LuminousAction {
		ACTION_PICK, ACTION_MULTIPLE_PICK;

		public static LuminousAction fromString(String action) {
			if ("SchollyMe.ACTION_LUM_MULTIPLE_PICK".equals(action)) {
				return ACTION_MULTIPLE_PICK;
			}
			return LuminousAction.ACTION_PICK;
		}

		public static String getString(LuminousAction action) {
			if (ACTION_MULTIPLE_PICK == action) {
				return "SchollyMe.ACTION_LUM_MULTIPLE_PICK";
			}
			return "SchollyMe.ACTION_LUM_PICK";
		}
	}

	public static ArrayList<String> toArrayList(String[] dataArray) {
		ArrayList<String> selections = new ArrayList<String>(null == dataArray ? 0 : dataArray.length);
		if (null != dataArray) {
			for (int i = 0; i < 10; i++) {
				selections.add(dataArray[i]);
			}
		}
		return selections;
	}

	@Override
	public String toString() {
		return "LuminousGallery [sdcardPath=" + sdcardPath + ", isSeleted=" + isSeleted + "]";
	}

}
