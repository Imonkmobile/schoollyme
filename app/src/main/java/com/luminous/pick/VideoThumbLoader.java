package com.luminous.pick;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.widget.ImageView;

public class VideoThumbLoader {
	private ExecutorService executorService;
	private Map<String, Bitmap> videoThumbs;

	public VideoThumbLoader() {
		executorService = Executors.newFixedThreadPool(5);
		videoThumbs = new HashMap<String, Bitmap>();
	}

	public void loadVideoThumb(ImageView imageView, String path) {
		if (null != videoThumbs.get(path) || videoThumbs.containsKey(path)) {
			imageView.setImageBitmap(videoThumbs.get(path));
		} else {
			imageView.setImageBitmap(null);
			executorService.submit(new ThumbLoader(imageView, path));
		}
	}

	private class ThumbLoader implements Runnable {
		private ImageView imageView;
		private String path;

		public ThumbLoader(ImageView imageView, String path) {
			super();
			this.imageView = imageView;
			this.path = path;
		}

		@Override
		public void run() {
			if (null == this.imageView || null != videoThumbs.get(path) || videoThumbs.containsKey(path)) {
				return;
			}
			File file = new File(this.path);
			final Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(file.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
			videoThumbs.put(this.path, thumbnail);

			if (null == this.imageView) {
				return;
			}
			this.imageView.post(new Runnable() {

				@Override
				public void run() {
					ThumbLoader.this.imageView.setImageBitmap(thumbnail);
				}
			});
		}

		@Override
		public String toString() {
			return "ThumbLoader [imageView=" + imageView + ", path=" + path + "]";
		}

	}
}
