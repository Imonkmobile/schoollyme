package com.vinfotech.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.text.TextUtils;

public class DateFormatter {
	public static final String FORMAT_MM_SLASH_DD_SlASH_YYYY = "MM/dd/yyyy";
	public static final String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	public static final String FORMAT_MMM_DD_YYYY = "MMM dd yyyy";

	private SimpleDateFormat inSdf, outSdf;
	private Calendar calendar;
	private int formatType;

	public DateFormatter(String inFormat, String outFormat) {
		calendar = Calendar.getInstance();

		if (FORMAT_YYYY_MM_DD.equalsIgnoreCase(inFormat) && FORMAT_MMM_DD_YYYY.equalsIgnoreCase(outFormat)) {
			inSdf = new SimpleDateFormat(FORMAT_YYYY_MM_DD, Locale.getDefault());
			outSdf = new SimpleDateFormat(FORMAT_MMM_DD_YYYY, Locale.getDefault());
			formatType = 0;
		} else {
			inSdf = new SimpleDateFormat();
			outSdf = new SimpleDateFormat();
			formatType = -1;
		}
	}

	public String convertFormat(String date) {
		if (!TextUtils.isEmpty(date) && !date.equalsIgnoreCase("0000-00-00")) {
			try {
				switch (formatType) {
				case 0:
					calendar.setTime(inSdf.parse(date));
					return outSdf.format(calendar.getTime());

				default:
					break;
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			// calendar=Calendar.getInstance();
			// calendar.add(Calendar.DATE, -1);
			// return outSdf.format(calendar.getTime());
			return "";
		}

		return date;
	}
}
