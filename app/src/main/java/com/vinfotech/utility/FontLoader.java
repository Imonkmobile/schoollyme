package com.vinfotech.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class FontLoader {
	private static Typeface robotoBold, robotoLight, robotoRegular, robotoMedium, handleeRegular;

	public static void loadFonts(Context context) {
		robotoBold = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
		robotoLight = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
		robotoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
		robotoMedium = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
		handleeRegular = Typeface.createFromAsset(context.getAssets(), "Handlee-Regular.ttf");

	}

	public static void unloadFonts() {
		robotoBold = null;
		robotoLight = null;
		robotoMedium = null;
		robotoRegular = null;
		handleeRegular = null;
	}

	public static void setRobotoBoldTypeface(View... view) {
		if (null != view) {
			if (null == robotoBold) {
				robotoBold = Typeface.createFromAsset(view[0].getContext().getAssets(), "fonts/Roboto-Bold.ttf");
			}
			for (int i = 0; i < view.length; i++) {
				if (view[i] instanceof TextView) {
					((TextView) view[i]).setTypeface(robotoBold);
				} else if (view[i] instanceof EditText) {
					((EditText) view[i]).setTypeface(robotoBold);
				} else if (view[i] instanceof RadioButton) {
					((RadioButton) view[i]).setTypeface(robotoBold);
				} else if (view[i] instanceof CheckBox) {
					((CheckBox) view[i]).setTypeface(robotoBold);
				} else if (view[i] instanceof AutoCompleteTextView) {
					((AutoCompleteTextView) view[i]).setTypeface(robotoBold);
				} else if (view[i] instanceof Button) {
					((Button) view[i]).setTypeface(robotoBold);
				}
			}
		}
	}

	public static void setRobotoLightTypeface(View... view) {
		if (null != view) {
			if (null == robotoLight) {
				robotoLight = Typeface.createFromAsset(view[0].getContext().getAssets(), "fonts/Roboto-Light.ttf");
			}
			for (int i = 0; i < view.length; i++) {
				if (view[i] instanceof TextView) {
					((TextView) view[i]).setTypeface(robotoLight);
				} else if (view[i] instanceof EditText) {
					((EditText) view[i]).setTypeface(robotoLight);
				} else if (view[i] instanceof RadioButton) {
					((RadioButton) view[i]).setTypeface(robotoLight);
				} else if (view[i] instanceof CheckBox) {
					((CheckBox) view[i]).setTypeface(robotoLight);
				} else if (view[i] instanceof AutoCompleteTextView) {
					((AutoCompleteTextView) view[i]).setTypeface(robotoLight);
				} else if (view[i] instanceof Button) {
					((Button) view[i]).setTypeface(robotoLight);
				}
			}
		}
	}

	public static void setRobotoRegularTypeface(View... view) {
		if (null != view) {
			if (null == robotoRegular) {
				robotoRegular = Typeface.createFromAsset(view[0].getContext().getAssets(), "fonts/Roboto-Regular.ttf");
			}
			for (int i = 0; i < view.length; i++) {
				if (view[i] instanceof TextView) {
					((TextView) view[i]).setTypeface(robotoRegular);
				} else if (view[i] instanceof EditText) {
					((EditText) view[i]).setTypeface(robotoRegular);
				} else if (view[i] instanceof RadioButton) {
					((RadioButton) view[i]).setTypeface(robotoRegular);
				} else if (view[i] instanceof CheckBox) {
					((CheckBox) view[i]).setTypeface(robotoRegular);
				} else if (view[i] instanceof AutoCompleteTextView) {
					((AutoCompleteTextView) view[i]).setTypeface(robotoRegular);
				} else if (view[i] instanceof Button) {
					((Button) view[i]).setTypeface(robotoRegular);
				}
			}
		}
	}

	public static void setRobotoMediumTypeface(View... view) {
		if (null != view) {
			if (null == robotoMedium) {
				robotoMedium = Typeface.createFromAsset(view[0].getContext().getAssets(), "fonts/Roboto-Regular.ttf");
			}
			for (int i = 0; i < view.length; i++) {
				if (view[i] instanceof TextView) {
					((TextView) view[i]).setTypeface(robotoMedium);
				} else if (view[i] instanceof EditText) {
					((EditText) view[i]).setTypeface(robotoMedium);
				} else if (view[i] instanceof RadioButton) {
					((RadioButton) view[i]).setTypeface(robotoMedium);
				} else if (view[i] instanceof CheckBox) {
					((CheckBox) view[i]).setTypeface(robotoMedium);
				} else if (view[i] instanceof AutoCompleteTextView) {
					((AutoCompleteTextView) view[i]).setTypeface(robotoMedium);
				} else if (view[i] instanceof Button) {
					((Button) view[i]).setTypeface(robotoMedium);
				}
			}
		}
	}

	public static void SetFontToWholeView(final Context context, final View v, Typeface typeface) {

		try {

			if (v instanceof ViewGroup) {

				ViewGroup vg = (ViewGroup) v;

				for (int i = 0; i < vg.getChildCount(); i++) {

					View child = vg.getChildAt(i);

					SetFontToWholeView(context, child, typeface);

				}
			} else if (v instanceof TextView) {

				((TextView) v).setTypeface(typeface);

			} else if (v instanceof EditText) {

				((EditText) v).setTypeface(typeface);

			}
		} catch (Exception e) {
		}
	}

	public static Typeface getRobotoRegular(Context context) {
		if (null == robotoRegular) {
			robotoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
		}
		return robotoRegular;
	}

	public static void setHandleeRegularTypeface(View... view) {
		if (null != view) {
			if (null == handleeRegular) {
				handleeRegular = Typeface.createFromAsset(view[0].getContext().getAssets(), "fonts/Handlee-Regular.ttf");
			}
			for (int i = 0; i < view.length; i++) {
				if (view[i] instanceof TextView) {
					((TextView) view[i]).setTypeface(handleeRegular);
				} else if (view[i] instanceof EditText) {
					((EditText) view[i]).setTypeface(handleeRegular);
				} else if (view[i] instanceof RadioButton) {
					((RadioButton) view[i]).setTypeface(handleeRegular);
				} else if (view[i] instanceof CheckBox) {
					((CheckBox) view[i]).setTypeface(handleeRegular);
				} else if (view[i] instanceof AutoCompleteTextView) {
					((AutoCompleteTextView) view[i]).setTypeface(handleeRegular);
				} else if (view[i] instanceof Button) {
					((Button) view[i]).setTypeface(handleeRegular);
				}
			}
		}
	}

}
