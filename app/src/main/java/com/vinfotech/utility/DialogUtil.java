package com.vinfotech.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.schollyme.R;

public class DialogUtil {
    private static final String TAG = DialogUtil.class.getSimpleName();

    public static void showOkDialog(Context context, String msg, String titleRes) {
        if (null == context || null == context || null == msg) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (!titleRes.equals("")) {
            builder.setTitle(titleRes);
        }

        builder.setMessage(msg);
        builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        builder.show();
    }

    public static AlertDialog showOkDialogButtonLisnter(final Context context, String msg, String titleResId,
                                                        final OnOkButtonListner mListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (!titleResId.equals("")) {
            builder.setTitle(titleResId);
        }

        builder.setMessage(msg);

        builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkBUtton();

            }
        });
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        return builder.show();
    }

    public static void showOkDialogNonCancelable(final Context context, String msg, String titleResId, final OnOkButtonListner mListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (!titleResId.equals("")) {
            builder.setTitle(titleResId);
        }

        builder.setMessage(msg);

        builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkBUtton();

            }
        });
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        builder.show();
    }

    public static void showOkDialogNonCancelableTwoButtons(final Context context, String msg, String titleResId,
                                                           final OnOkButtonListner mListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (!titleResId.equals("")) {
            builder.setTitle(titleResId);
        }

        builder.setMessage(msg);

        builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkBUtton();

            }
        });

        builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        builder.show();
    }

    public static Dialog showOkDialogUncancelableButtonLisnter(final Context context, String msg, String title,
                                                               final OnOkButtonListner mListener) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (!title.equals("")) {
            builder.setTitle(title);
        }

        builder.setMessage(msg);

        builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkBUtton();

            }
        });
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
//        final Dialog mDialog = builder.create();

        alertDialog.show();
        return alertDialog;
    }

    public interface OnOkCancelButtonListner {

        public void onCancelBUtton();

        public void onOkBUtton();

    }

    public static Dialog showOkConcelDialogButtonLisnter(final Context context, String msg, String titleResId,
                                                         final OnOkCancelButtonListner mListener) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (!titleResId.equals("")) {
            builder.setTitle(titleResId);
        }

        builder.setMessage(msg);

        builder.setPositiveButton(R.string.Cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkBUtton();

            }
        });

        builder.setNegativeButton(R.string.Continue, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onCancelBUtton();

            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
//        final Dialog mDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    public static void showOkCancelDialogButtonLisnter(final Context context, String msg, String titleResId,
                                                       final OnOkButtonListner mListener,
                                                       DialogInterface.OnCancelListener mCancelListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!titleResId.equals("")) {
            builder.setTitle(titleResId);
        }
        builder.setMessage(msg);

        builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkBUtton();
            }
        });
        builder.setCancelable(true);
        builder.setOnCancelListener(mCancelListener);

        if (!((Activity) context).isFinishing()) { //here activity means your activity class
            final AlertDialog alertDialog = builder.create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                    alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
                }
            });
            builder.show();
        }
    }


    public static void showTwoButtonDialogLisnter(final Context context, String btnTxtYes, String btnTxtNo, String msg,
                                                  String titleResId, final OnOkButtonListner mListener,
                                                  final DialogInterface.OnCancelListener mCancelListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!titleResId.equals("")) {
            builder.setTitle(titleResId);
        }
        builder.setMessage(msg);

        builder.setPositiveButton(btnTxtYes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkBUtton();
            }
        });

        builder.setNegativeButton(btnTxtNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCancelListener.onCancel(dialog);
            }
        });
        builder.setCancelable(true);
        //builder.setOnCancelListener(mCancelListener);
        if (!((Activity) context).isFinishing()) { //here activity means your activity class
            final AlertDialog alertDialog = builder.create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                    alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
                }
            });
            builder.show();
        }
    }

    public static void showSingleButtonDialogLisnter(final Context context, String btnTxtYes, String msg,
                                                     String titleResId, final OnOkButtonListner mListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!titleResId.equals("")) {
            builder.setTitle(titleResId);
        }
        builder.setMessage(msg);

        builder.setPositiveButton(btnTxtYes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onOkBUtton();
            }
        });
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        builder.show();
    }

    public static Dialog showYesNoDialogWithTittle(final Context context, String titleResId,
                                                   String msg, final OnClickListener mListener) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(titleResId)) {
            adb.setTitle(titleResId);
        }
        adb.setMessage(msg);
        adb.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (null != mListener) {
                    mListener.onClick(new View(context));
                }
            }
        });

        adb.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();
        final AlertDialog alertDialog = adb.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        return adb.create();
    }

    //public static int mPosition = -1;

    @SuppressLint("NewApi")
    public static void showListDialog(final Context context, int titleResId, final OnItemClickListener listener, String... items) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (0 != titleResId) {
            builder.setTitle(titleResId);
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
        arrayAdapter.addAll(items);
        builder.setTitle(R.string.dialog_option_title);
        builder.setCancelable(true);
        builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if (null != listener) {
                    listener.onCancel();
                }
                if (Config.DEBUG) {
                    Log.d(TAG, "showListDialog.onClick position=" + position + ", canceled=true");
                }
                dialog.dismiss();
            }
        });

        builder.setPositiveButton(R.string.Done, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {

                if (arrayAdapter.getCount()>0 && position>=0) {
                    String item = arrayAdapter.getItem(position);
                    if (null != listener) {
                        listener.onItemClick(position, item);
                    }
                    if (Config.DEBUG) {
                        Log.d(TAG, "showListDialog.onClick position=" + position + ", canceled=true");
                    }
                    dialog.dismiss();
                }
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                // mPosition = position;
                String item = arrayAdapter.getItem(position);
                if (null != listener) {
                    listener.onItemClick(position, item);
                }
                if (Config.DEBUG) {
                    Log.d(TAG, "showListDialog.onClick position=" + position + ", item=" + item);
                }
            }
        });
        // create alert dialog
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        // show it
        alertDialog.show();

    }

    @SuppressLint("NewApi")
    public static void showMediaDialog(final Context context, int titleResId, final OnItemClickListener listener, String[] items) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (0 != titleResId) {
            builder.setTitle(titleResId);
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
        arrayAdapter.addAll(items);

        builder.setCancelable(false);
        builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != listener) {
                    listener.onCancel();
                }
                if (Config.DEBUG) {
                    Log.d(TAG, "showListDialog.onClick which=" + which + ", canceled=true");
                }
                dialog.dismiss();
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String item = arrayAdapter.getItem(which);
                if (null != listener) {
                    listener.onItemClick(which, item);
                }
                if (Config.DEBUG) {
                    Log.d(TAG, "showListDialog.onClick which=" + which + ", item=" + item);
                }
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        builder.show();
    }

    @SuppressLint("NewApi")
    public static void showListDialogNonClickableForSomePosition(final Context context, int titleResId,
                                                                 final OnItemClickListener listener,
                                                                 final int position, String... items) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (0 != titleResId) {
            builder.setTitle(titleResId);
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
        arrayAdapter.addAll(items);

        builder.setCancelable(false);

        builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != listener) {
                    listener.onCancel();
                }
                if (Config.DEBUG) {
                    Log.d(TAG, "showListDialog.onClick which=" + which + ", canceled=true");
                }
                dialog.dismiss();
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String item = arrayAdapter.getItem(which);
                if (position == which) {
                    builder.show();
                } else {
                    if (null != listener) {
                        listener.onItemClick(which, item);
                    }
                    if (Config.DEBUG) {
                        Log.d(TAG, "showListDialog.onClick which=" + which + ", item=" + item);
                    }
                    dialog.dismiss();
                }
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        builder.show();
    }

    @SuppressLint("NewApi")
    public static void showListCustomDialog(final Context context, int titleResId, final OnItemClickListener listener,
                                            final int excludedItemPos, final String... items) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflator = ((Activity) context).getLayoutInflater();
        View view = inflator.inflate(R.layout.dialog_list, null);
        ListView lv = (ListView) view.findViewById(R.id.dialog_list);
        TextView cancelBtn = (TextView) view.findViewById(R.id.cancel_tv);
        DialogAdapter adapter = new DialogAdapter(context, items);
        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        dialog.setContentView(view);
        cancelBtn.setTextColor(Color.parseColor("#0084FF"));
        cancelBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (position == excludedItemPos) {
//
//                } else {
                listener.onItemClick(position, items[position]);
                dialog.dismiss();
                // }
            }
        });
        dialog.show();
    }

    @SuppressLint("NewApi")
    public static void showFeedListCustomDialog(final Context context, final OnItemClickListener listener,
                                                final String... items) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflator = ((Activity) context).getLayoutInflater();
        View view = inflator.inflate(R.layout.dialog_list, null);
        ListView lv = (ListView) view.findViewById(R.id.dialog_list);
        TextView cancelBtn = (TextView) view.findViewById(R.id.cancel_tv);
        DialogAdapter adapter = new DialogAdapter(context, items);
        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        dialog.setContentView(view);
        cancelBtn.setTextColor(Color.parseColor("#0084FF"));
        cancelBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listener.onItemClick(position, items[position]);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static class DialogAdapter extends BaseAdapter {

        public Context context;
        public ViewHolder mViewHolder;
        public LayoutInflater inflator;
        public String mData[];

        public DialogAdapter(Context ctx, String data[]) {
            mData = data;
            context = ctx;
            inflator = LayoutInflater.from(ctx);
        }

        @Override
        public int getCount() {
            return mData.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            mViewHolder = new ViewHolder();
            if (convertView == null) {
                convertView = inflator.inflate(R.layout.dialog_list_item, parent, false);
                mViewHolder.mTitleTV = (TextView) convertView.findViewById(R.id.list_item);
                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (ViewHolder) convertView.getTag();
            }
            mViewHolder.mTitleTV.setText(mData[position]);
            return convertView;
        }

        public class ViewHolder {
            public TextView mTitleTV;
        }
    }

    @SuppressLint("NewApi")
    public static void showListDialogCustom(final Context context, int titleResId, final OnItemClickListener listener, String... items) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (0 != titleResId) {
            builder.setTitle(titleResId);
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
        arrayAdapter.addAll(items);
        builder.setCancelable(true);
        builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != listener) {
                    listener.onCancel();
                }
                if (Config.DEBUG) {
                    Log.d(TAG, "showListDialog.onClick which=" + which + ", canceled=true");
                }
                dialog.dismiss();
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                // mPosition = position;
                String item = arrayAdapter.getItem(position);
                if (null != listener) {
                    listener.onItemClick(position, item);
                }
                if (Config.DEBUG) {
                    Log.d(TAG, "showListDialog.onClick position=" + position + ", item=" + item);
                }
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });
        builder.show();
    }

    public static ProgressDialog createDiloag(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        if (!TextUtils.isEmpty(message)) {
            progressDialog.setMessage(message);
        }
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static void showOkCancelDialog(final Context context, int btnYes, int btnNo, String title,
                                          String message, final OnClickListener okListener,
                                          final OnClickListener cancelListener) {
        showOkCancelDialog(context, 0, btnYes, btnNo, title, message, okListener, cancelListener);

    }

    public static void showOkCancelDialog(final Context context, int okResId, int btnYes, int btnNo,
                                          String title, String message,
                                          final OnClickListener okListener, final OnClickListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (null != title && title.length() > 0) {
            builder.setTitle(title);
        }

        builder.setMessage(message);
        builder.setPositiveButton(okResId > 0 ? okResId : btnYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (null != okListener) {
                    okListener.onClick(new View(context));
                }
            }
        });

        builder.setNegativeButton(btnNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (null != cancelListener) {
                    cancelListener.onClick(new View(context));
                }
            }
        });

        // create alert dialog
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });

        builder.show();
    }

    public static void showSingleEditDialog(final Context context, int hintResId, String existText, final SingleEditListener listener) {
        final int margin = (int) context.getResources().getDimension(R.dimen.space_xlarge30);

        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(context.getString(R.string.flag));
        final EditText editText = new EditText(context);
        //editText.setHeight(margin * 10);
        editText.setPadding(margin, margin, margin, margin);
        editText.setBackgroundColor(Color.TRANSPARENT);
        editText.setGravity(Gravity.TOP);
        editText.setHint(hintResId);
        editText.requestFocus();
        editText.setText(existText);

        final boolean forWriteCaption = (hintResId == R.string.Write_caption);
        if (forWriteCaption) {
            editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            editText.setHeight((int) context.getResources().getDimension(R.dimen.space_mid10) * 5);
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
        }
        alert.setView(editText);
        alert.setCancelable(false);
        alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String text = editText.getText().toString().trim();
                if (TextUtils.isEmpty(text)) {
                    Toast.makeText(context, "Please enter text", Toast.LENGTH_SHORT).show();
                    if (null != listener) {
                        listener.onEdit(true, editText, "");
                    }
                    //return;
                } else if (null != listener) {
                    listener.onEdit(false, editText, text);
                    dialog.dismiss();
                }

            }
        });

        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        // create alert dialog
        final AlertDialog alertDialog = alert.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#01579B"));
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#01579B"));
            }
        });

        alert.show();
    }

    public interface SingleEditListener {
        void onEdit(boolean canceled, EditText editText, String text);
    }

    public interface ShareItemClickListener {
        public void shareListener(String socialSiteName);

        public void onCancel();

    }

    public interface OnOkButtonListner {

        public void onOkBUtton();

    }

    public interface OnItemClickListener {
        void onItemClick(int position, String item);

        void onCancel();
    }

    public static Dialog createProgressDialog(Context context, int msgResId) {
        if (msgResId == 0) {
            WhiteProgressDialog dialog = new WhiteProgressDialog(context, msgResId);
            dialog.setCanceledOnTouchOutside(false);
            return dialog;
        } else {
            ProgressDialog mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(context.getResources().getString(msgResId));
            return mProgressDialog;
        }
    }

    public static Dialog createProgressDialogDefaultWithMessage(Context context, int msgResId) {

        ProgressDialog dialog = new ProgressDialog(context);
        if (msgResId != 0) {

            dialog.setMessage(context.getResources().getString(msgResId));
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return dialog;
    }

    public static void HintViewer(final Context mContext, String msg, int imgId) {
        final Dialog mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        LayoutInflater mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = mLayoutInflater.inflate(R.layout.hint_viewer, null);
        mDialog.setContentView(layout);
        ImageView hint_img = (ImageView) mDialog.findViewById(R.id.hint_img);
        TextView msg_tv = (TextView) mDialog.findViewById(R.id.msg_tv);

        hint_img.setImageResource(imgId);

        msg_tv.setText(msg);
        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        FontLoader.setHandleeRegularTypeface(msg_tv);

        mDialog.show();
    }

    public static void showHintDialog(final Context context, int layoutId) {
        final Dialog mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = mLayoutInflater.inflate(layoutId, null);
        mDialog.setContentView(layout);
        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    public static void showSODHintDialog(final Context context, boolean hint1Visible, boolean hint2Visible, boolean hint3Visible) {
        final Dialog mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = mLayoutInflater.inflate(R.layout.sod_coach_mark_dialog, null);

        layout.findViewById(R.id.hint_1_iv).setVisibility(hint1Visible ? View.VISIBLE : View.INVISIBLE);
        layout.findViewById(R.id.hint_2_iv).setVisibility(hint2Visible ? View.VISIBLE : View.INVISIBLE);
        layout.findViewById(R.id.hint_3_iv).setVisibility(hint3Visible ? View.VISIBLE : View.INVISIBLE);

        mDialog.setContentView(layout);
        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }
}
