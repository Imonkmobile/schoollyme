package com.vinfotech.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;
import com.schollyme.R;
import com.vinfotech.widget.TouchImageView;

@SuppressLint("NewApi")
public class ImageLoaderUniversal {

	static ImageLoader imageLoader;

	static ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

	public static Builder option_Round_Image = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.ic_launcher)
			.displayer(new RoundedBitmapDisplayer(1000)).cacheInMemory(true).cacheOnDisc().considerExifParams(true);
	
	public static Builder option_Round_Image_for_teampage = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.icon_members)
			.displayer(new RoundedBitmapDisplayer(1000)).cacheInMemory(true).cacheOnDisc().considerExifParams(true);

	public static final Builder ROUNDED_THUMB_OPTIONS = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.media_loader)
			.displayer(new RoundedBitmapDisplayer(6)).cacheInMemory(true).cacheOnDisc().considerExifParams(true);

	public static DisplayImageOptions option_normal_Image = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.media_loader)
			.showImageForEmptyUri(R.drawable.media_loader).showImageOnFail(R.drawable.media_loader).cacheInMemory(true).cacheOnDisc()
			.considerExifParams(true).build();

	public static DisplayImageOptions option_normal_Image_WTL = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc()
			.considerExifParams(true).build();

	public static DisplayImageOptions option_thumb_Gallery = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.media_loader)

			.postProcessor(new BitmapProcessor() {
				@Override
				public Bitmap process(Bitmap bmp) {

					return Bitmap.createScaledBitmap(bmp, bmp.getWidth() / 3,
							bmp.getHeight() / 3, true);
				}
			}).cacheInMemory(true)
			.showImageForEmptyUri(R.drawable.media_loader)
			.showImageOnFail(R.drawable.media_loader).cacheInMemory(true)
			.cacheOnDisc(true).considerExifParams(true).build();

	public static DisplayImageOptions option_normal_Image_Thumbnail = new DisplayImageOptions.Builder()
	.showImageOnLoading(R.drawable.media_loader)
	// .postProcessor for decreasing size of downloaded bitmap afer
	// lazzy loading
	.postProcessor(new BitmapProcessor() {
		@Override
		public Bitmap process(Bitmap bmp) {
			int sizeOfBitmap = sizeOf(bmp);
			System.out.println("Size of bitmap-" + sizeOfBitmap);

			int divider = 1;
			if (sizeOfBitmap <= 400) {
				divider = 1;
			} else if (sizeOfBitmap > 400 && sizeOfBitmap <= 800) {
				divider = 2;
			} else if (sizeOfBitmap > 800 && sizeOfBitmap <= 1200) {
				divider = 3;
			} else if (sizeOfBitmap > 1200 && sizeOfBitmap <= 1700) {
				divider = 4;
			} else if (sizeOfBitmap > 1700 && sizeOfBitmap <= 3000) {
				divider = 5;
			} else if (sizeOfBitmap > 3000 && sizeOfBitmap <= 4500) {
				divider = 6;
			} else {
				divider = 8;
			}
			if(divider == 1){
				return bmp;
			}
			return Bitmap.createScaledBitmap(bmp, bmp.getWidth() / divider, bmp.getHeight() / divider, true);
		}
	}).showImageForEmptyUri(R.drawable.media_loader).showImageOnFail(R.drawable.media_loader).cacheInMemory(true).cacheOnDisc(true)
	.considerExifParams(true).build();

	public static DisplayImageOptions option_normal_Image_Thumbnail_cover = new DisplayImageOptions.Builder()
			.showImageOnLoading(null)
			// .postProcessor for decreasing size of downloaded bitmap afer
			// lazzy loading
			.postProcessor(new BitmapProcessor() {
				@Override
				public Bitmap process(Bitmap bmp) {
					int sizeOfBitmap = sizeOf(bmp);
					System.out.println("Size of bitmap-" + sizeOfBitmap);

					int divider = 1;
					if (sizeOfBitmap <= 200) {
						divider = 1;
					} else if (sizeOfBitmap > 200 && sizeOfBitmap <= 400) {
						divider = 2;
					} else if (sizeOfBitmap > 400 && sizeOfBitmap <= 800) {
						divider = 3;
					} else if (sizeOfBitmap > 8000 && sizeOfBitmap <= 1200) {
						divider = 4;
					} else if (sizeOfBitmap > 1200 && sizeOfBitmap <= 1700) {
						divider = 5;
					} else if (sizeOfBitmap > 1700 && sizeOfBitmap <= 3000) {
						divider = 6;
					} else if (sizeOfBitmap > 3000 && sizeOfBitmap <= 4500) {
						divider = 7;
					} else {
						divider = 8;
					}

					return Bitmap.createScaledBitmap(bmp, bmp.getWidth() / divider, bmp.getHeight() / divider, true);
				}
			}).showImageForEmptyUri(null).showImageOnFail(null).cacheInMemory(true).cacheOnDisc()
			.considerExifParams(true).build();

	public static DisplayImageOptions option_normal_Image_Thumbnail_Video = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.ic_default_video)
			// .postProcessor for decreasing size of downloaded bitmap afer
			// lazzy loading
			.postProcessor(new BitmapProcessor() {
				@Override
				public Bitmap process(Bitmap bmp) {
					int sizeOfBitmap = sizeOf(bmp);
					System.out.println("Size of bitmap-" + sizeOfBitmap);

					int divider = 1;
					if (sizeOfBitmap <= 200) {
						divider = 1;
					} else if (sizeOfBitmap > 200 && sizeOfBitmap <= 400) {
						divider = 2;
					} else if (sizeOfBitmap > 400 && sizeOfBitmap <= 800) {
						divider = 3;
					} else if (sizeOfBitmap > 8000 && sizeOfBitmap <= 1200) {
						divider = 4;
					} else if (sizeOfBitmap > 1200 && sizeOfBitmap <= 1700) {
						divider = 5;
					} else if (sizeOfBitmap > 1700 && sizeOfBitmap <= 3000) {
						divider = 6;
					} else if (sizeOfBitmap > 3000 && sizeOfBitmap <= 4500) {
						divider = 7;
					} else {
						divider = 8;
					}

					return Bitmap.createScaledBitmap(bmp, bmp.getWidth() / divider, bmp.getHeight() / divider, true);
				}
			}).showImageForEmptyUri(R.drawable.ic_default_video).showImageOnFail(R.drawable.ic_default_video).cacheInMemory(true).cacheOnDisc()
			.considerExifParams(true).build();

	public static DisplayImageOptions DiaplayOptionForProgresser = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.ic_empty).showImageOnFail(R.drawable.ic_empty).cacheInMemory(true)
			.resetViewBeforeLoading(true).cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true).build();

	public static DisplayImageOptions DiaplayProfileOptionForProgresser = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.ic_default_profile).showImageOnFail(R.drawable.ic_default_profile).cacheInMemory(true)
			.resetViewBeforeLoading(true).cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true).build();

	@SuppressLint("NewApi")
	public static int sizeOf(Bitmap data) {
		final int kb = 1024;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
			return (data.getRowBytes() * data.getHeight()) / kb;
		} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			return data.getByteCount() / kb;
		} else {
			return data.getAllocationByteCount() / kb;
		}
	}

	public static void ImageLoadRoundSlider(Context mContext, String URl, ImageView imgView, Builder Display_options) {
		if (!TextUtils.isEmpty(URl)) {
			Display_options.showImageOnLoading(R.drawable.ic_default_profile1).showImageForEmptyUri(R.drawable.ic_default_profile1)
					.showImageOnFail(R.drawable.ic_default_profile1);

			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(mContext)
					.defaultDisplayImageOptions(Display_options.build());

			ImageLoaderConfiguration config = builder.build();
			imageLoader = ImageLoader.getInstance();
			if (!imageLoader.isInited()) {
				imageLoader.init(config.createDefault(mContext));
			}
			imageLoader.displayImage(URl, imgView, Display_options.build(), animateFirstListener);
		}
	}

	public static void ImageLoadRound(Context mContext, String URl, ImageView imgView, Builder Display_options) {
		if (!TextUtils.isEmpty(URl)) {
			Display_options.showImageOnLoading(R.drawable.ic_default_profile1)
					.showImageForEmptyUri(R.drawable.ic_default_profile1)
					.showImageOnFail(R.drawable.ic_default_profile1);

			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(mContext)
					.defaultDisplayImageOptions(Display_options.build());

			ImageLoaderConfiguration config = builder.build();
			imageLoader = ImageLoader.getInstance();
			if (!imageLoader.isInited()) {
				imageLoader.init(config.createDefault(mContext));
			}
			imageLoader.displayImage(URl, imgView, Display_options.build(), animateFirstListener);
		}
	}

	public static void ImageLoadRoundNotification(Context mContext, String URl, ImageView imgView, Builder Display_options) {
		if (!TextUtils.isEmpty(URl)) {
			Display_options.showImageOnLoading(R.drawable.ic_launcher)
					.showImageForEmptyUri(R.drawable.ic_launcher)
					.showImageOnFail(R.drawable.ic_launcher);

			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(mContext)
					.defaultDisplayImageOptions(Display_options.build());

			ImageLoaderConfiguration config = builder.build();
			imageLoader = ImageLoader.getInstance();
			if (!imageLoader.isInited()) {
				imageLoader.init(config.createDefault(mContext));
			}
			imageLoader.displayImage(URl, imgView, Display_options.build(), animateFirstListener);
		}
	}
	
	public static void ImageLoadRoundForTeamPage(Context mContext, String URl, ImageView imgView, Builder Display_options) {

		if (!TextUtils.isEmpty(URl)) {
			Display_options.showImageOnLoading(R.drawable.icon_members).showImageForEmptyUri(R.drawable.icon_members)
					.showImageOnFail(R.drawable.icon_members);

			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(mContext)
					.defaultDisplayImageOptions(Display_options.build());

			ImageLoaderConfiguration config = builder.build();
			imageLoader = ImageLoader.getInstance();
			if (!imageLoader.isInited()) {
				imageLoader.init(config.createDefault(mContext));
			}

			imageLoader.displayImage(URl, imgView, Display_options.build(), animateFirstListener);
		}
	}

	public static void ImageLoadSquare(Context mContext, String URl, ImageView imgView, DisplayImageOptions Display_options) {
		if (!TextUtils.isEmpty(URl)) {
			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(mContext).defaultDisplayImageOptions(
					Display_options).memoryCache(new WeakMemoryCache());

			ImageLoaderConfiguration config = builder.build();
			imageLoader = ImageLoader.getInstance();
			if (!imageLoader.isInited()) {
				imageLoader.init(config.createDefault(mContext));
			}

			imageLoader.displayImage(URl, imgView, Display_options, animateFirstListener);
		}
	}

	public static void ImageLoadSquareWithProgressBar(final Context context, final String URl, final ImageView imgView,
			final DisplayImageOptions displayImageOptions, final ProgressBar mLoader) {

		if (!TextUtils.isEmpty(URl)) {
			imageLoader.displayImage(URl, imgView, displayImageOptions, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					if (null != mLoader) {
						mLoader.setVisibility(View.VISIBLE);
					}

				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					@SuppressWarnings("unused")
					String message = null;
					switch (failReason.getType()) {
					case IO_ERROR:
						if(!TextUtils.isEmpty(URl) && URl.endsWith("hq3.jpg")){
							ImageLoadSquareWithProgressBar(context, URl.replace("hq3.jpg", "default.jpg"), imgView, displayImageOptions, mLoader);
						}
//						else{
//							ImageLoadSquareWithProgressBar(context,URl,imgView,displayImageOptions, mLoader);
//						}
						message = "Input/Output error";
						break;
					case DECODING_ERROR:
						message = "Image can't be decoded";
						break;
					case NETWORK_DENIED:
						message = "Downloads are denied";
						break;
					case OUT_OF_MEMORY:
						message = "Out Of Memory error";
						break;
					case UNKNOWN:
						message = "Unknown error";
						break;
					}
					if (null != mLoader) {
						mLoader.setVisibility(View.GONE);
					}
					Log.d("Downloading imageUri = ",imageUri);
					Log.d("Error Downloading Image",message);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					mLoader.setVisibility(View.GONE);
					if ( imgView instanceof TouchImageView) {
						TouchImageView iv=(TouchImageView)imgView;
						iv.setZoom(1);
//						iv.setImageBitmap(loadedImage);
						System.out.println("IMAGE");
					}else {
						System.out.println("IMAGE1111");
					}

				}
			}

			);

			// ImageLoaderConfiguration.Builder builder = new
			// ImageLoaderConfiguration.Builder(
			// mContext).defaultDisplayImageOptions(Display_options)
			// .memoryCache(new WeakMemoryCache());
			//
			// ImageLoaderConfiguration config = builder.build();
			// imageLoader = ImageLoader.getInstance();
			// imageLoader.init(config.createDefault(mContext));
			//
			// imageLoader.displayImage(URl, imgView, Display_options,
			// animateFirstListener);
		}
	}

	public static ImageLoader getImageLoader(){
		return imageLoader;
	}

}
