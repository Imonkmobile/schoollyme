package com.vinfotech.utility;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

public class LocaleUtil {
	private static final String TAG = LocaleUtil.class.getSimpleName();

	public String mLocaleCode;
	private Activity mActivity;
	public LocaleUtil(Activity activity){
		mActivity = activity;
		SharedPreferences sharedPreferences = mActivity.getSharedPreferences(TAG, Context.MODE_PRIVATE);
		mLocaleCode = sharedPreferences.getString("mLocaleCode", "");
	}
	
	public void setLocale (String localeCode , Bundle bundle ){
	    Log.d(TAG, "setLocale localeCode="+localeCode + ", bundle=" + bundle);
	    mLocaleCode = localeCode;
	    
	    Locale locale = new Locale(localeCode);
	    Locale.setDefault(locale);
	    Configuration config = new Configuration();
	    config.locale = locale;
	    
	    mActivity.getBaseContext().getResources().updateConfiguration(config, mActivity.getResources().getDisplayMetrics());
	    mActivity.getApplicationContext().getResources().updateConfiguration(config, mActivity.getResources().getDisplayMetrics());
	    mActivity.getResources().updateConfiguration(config, mActivity.getResources().getDisplayMetrics());
	    
	   	    
	    SharedPreferences sharedPreferences = mActivity.getSharedPreferences(TAG, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.putString("mLocaleCode", mLocaleCode);
		prefsEditor.commit();
	}
}
