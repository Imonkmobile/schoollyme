package com.vinfotech.utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.schollyme.R;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

public class CalStartEndUtil {
	private Calendar nowCalendar;
	private Calendar startCalendar;
	private Calendar endCalendar;
	private Calendar dummyCalendar;

	public enum Type {
		Now, Start, End, Dummy;
	}

	public CalStartEndUtil(long startTs, long endTs) {
		nowCalendar = Calendar.getInstance();

		startCalendar = (Calendar) nowCalendar.clone();
		if (startTs > 0) {
			startCalendar.setTimeInMillis(startTs);
		}

		endCalendar = null;
		if (endTs > 0) {
			endCalendar = (Calendar) nowCalendar.clone();
			endCalendar.setTimeInMillis(endTs);
		}

		dummyCalendar = (Calendar) nowCalendar.clone();
	}

	/**
	 * object.getCalendar(Type.Start).set(year, month, day);
	 * 
	 * @param type
	 * @return
	 */
	public Calendar getCalendar(Type type) {
		if (Type.Now == type) {
			return nowCalendar;
		} else if (Type.Start == type) {
			return startCalendar;
		} else if (Type.End == type) {
			return endCalendar;
		} else if (Type.Dummy == type) {
			return dummyCalendar;
		}
		return null;
	}

	public boolean setStartToDummy() {
		if (dummyCalendar.before(nowCalendar)) {
			return false;
		} else if (null != endCalendar && dummyCalendar.after(endCalendar)) {
			return false;
		}

		startCalendar.setTime(dummyCalendar.getTime());
		return true;
	}

	public boolean setEndToDummy() {
		if (dummyCalendar.before(nowCalendar)) {
			return false;
		} else if (dummyCalendar.before(startCalendar)) {
			return false;
		}

		if (null == endCalendar) {
			endCalendar = (Calendar) nowCalendar.clone();
		}
		endCalendar.setTime(dummyCalendar.getTime());
		return true;
	}

	public enum DiffType {
		Year, Month, Day, Hours, Minutes, Seconds, Millis;
	}

	public static long getDifference(DiffType diffType, Calendar start, Calendar end) {

		long millis = end.getTimeInMillis() - start.getTimeInMillis();
		if (millis > 0) {
			switch (diffType) {
			case Millis:
				return millis;
			case Seconds:
				return millis / 1000;
			case Minutes:
				return millis / (1000 * 60L);
			case Hours:
				return millis / (1000 * 60 * 60L);
			case Day:
				return millis / (1000 * 60 * 60 * 24L);
			case Month:
				return millis / (31556952000l / 12);
			case Year:
				return millis / 31556952000l;

			default:
				break;
			}
		}
		return 0;
	}

	public static String getDifferenceInDays(Calendar start) {
		Calendar end = Calendar.getInstance();

		long mins = getDifference(DiffType.Minutes, start, end);

		int hrs = (int) mins / 60;

		if (DateUtils.isToday(start.getTimeInMillis())) {

			return "Today";

		} else if (hrs < 24) {

			return "Yesterday";

		} else {
			int weeks = hrs / 24;

			return weeks + " week ago";
		}

	}

	// public static String getDifference(Calendar start) {
	// Calendar end = Calendar.getInstance();
	//
	// long mins = getDifference(DiffType.Minutes, start, end);
	//
	// if (mins < 1) {
	// return "Now";
	// } else if (mins >= 1) {
	// if (mins < 60) {
	// return mins + " minute ago";
	// } else {
	// int hrs = (int) mins / 60;
	// if (hrs < 24) {
	//
	// return hrs + " hour ago";
	// } else {
	//
	// return new SimpleDateFormat("dd MMM,yyyy").format(start.getTime());
	// }
	//
	// }
	// }
	// return "";
	// }

	public static String getTimeInformatedform(Context c, Calendar start) {
		if (null != start) {
			return new SimpleDateFormat("MMMM dd,yyyy").format(start.getTime()) + " at "
					+ new SimpleDateFormat("hh:mm aa").format(start.getTime());
		} else {
			return "";
		}
	}

	public static String getDifference(Context c, Calendar start) {
		Calendar end = Calendar.getInstance();
		long mins = getDifference(DiffType.Minutes, start, end);
		if (mins < 1) {

			return c.getResources().getString(R.string.Just_now);
		} else if (mins >= 1) {
			if (mins < 60) {
				return mins == 1 ? mins + " " + c.getResources().getString(R.string.minute_ago) : mins + " "
						+ c.getResources().getString(R.string.minutes_ago);
			} else {
				int hrs = (int) mins / 60;
				if (hrs < 24) {

					return hrs == 1 ? hrs + " " + c.getResources().getString(R.string.hour_ago) : hrs + " "
							+ c.getResources().getString(R.string.hours_ago);
				} else {
				/*	String time = new SimpleDateFormat("dd MMM,yyyy").format(start.getTime()) + " at "
							+ new SimpleDateFormat("hh:mm aa").format(start.getTime());*/
					String time = new SimpleDateFormat("MMMM dd,yyyy").format(start.getTime()) + " at "
							+ new SimpleDateFormat("hh:mm aa").format(start.getTime());
					return time;
				}
			}
		}
		return "";
	}

	public static String getDate(Context c, Calendar start) {
		String time = new SimpleDateFormat("dd MMM,yyyy").format(start.getTime()) + " at "
				+ new SimpleDateFormat("hh:mm aa").format(start.getTime());
		return time;
	}

	public static String formateDate(String date){
		String mDate = date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, YYYY hh:mm");
			mDate = sdf.format(date);
			return mDate;
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return mDate;
	}

	// new SimpleDateFormat("")

	// public static String getDifferenceForChatRoom(Calendar start) {
	// Calendar end = Calendar.getInstance();
	// long mins = getDifference(DiffType.Minutes, start, end);
	//
	// int hrs = (int) mins / 60;
	// if (hrs < 24) {
	//
	// return " Today";
	// } else if (48 > 24) {
	//
	// return " Yesterday";
	// } else {
	//
	// return new SimpleDateFormat().format(start.getTime());
	// }
	//
	// }
}
