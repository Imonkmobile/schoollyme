package com.vinfotech.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import com.schollyme.R;

public class DisplayUtil {
	/**
	 * Find device real display size in pixel.
	 * 
	 * @param activity
	 * @param dimens
	 *            int array of more than or equal to two size<br/>
	 *            0-width<br/>
	 *            1-height
	 */
	@SuppressLint("NewApi")
	public static void probeScreenSize(Activity activity, int[] dimens) {
		if (null != activity) {
			if (null == dimens) {
				dimens = new int[2];
			}
			Display display = activity.getWindowManager().getDefaultDisplay();
			if (android.os.Build.VERSION.SDK_INT >= 13) {
				Point size = new Point();
				display.getSize(size);
				dimens[0] = size.x;
				dimens[1] = size.y;
			} else {
				dimens[0] = display.getWidth();
				dimens[1] = display.getHeight();
			}
		}
	}

	/**
	 * Find device real display size in pixel.
	 * 
	 * @param context
	 * @param dimens
	 *            int array of more than or equal to two size<br/>
	 *            0-width<br/>
	 *            1-height
	 */
	public static void probeScreenSize(Context context, int[] dimens) {
		if (null != context) {
			if (context instanceof FragmentActivity) {
				FragmentActivity fragmentActivity = (FragmentActivity) context;
				probeScreenSize(fragmentActivity, dimens);
			} else if (context instanceof Activity) {
				Activity activity = (Activity) context;
				probeScreenSize(activity, dimens);
			}
		}
	}

	public static void bounceView(Context context, final View v) {

		Animation zoomIn = AnimationUtils.loadAnimation(context, R.anim.bounce_zoom_in);
		final Animation zoomout = AnimationUtils.loadAnimation(context, R.anim.bounce_zoom_out);
		v.startAnimation(zoomIn);
		zoomIn.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				v.startAnimation(zoomout);
			}
		});

	}

	public static  void hideKeyboard(View v, Context context) {

		try {
			InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
