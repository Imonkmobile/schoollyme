package com.vinfotech.font;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by user on 1/4/2016.
 */
@SuppressLint("AppCompatCustomView")
public class CustomEditTextRobotoLight extends EditText {

    public CustomEditTextRobotoLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEditTextRobotoLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditTextRobotoLight(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface face = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Light.ttf");
            setTypeface(face);
        }
    }

}
