package com.vinfotech.font;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by user on 1/4/2016.
 */
@SuppressLint("AppCompatCustomView")
public class CustomEditTextRobotoMediam extends EditText {

    public CustomEditTextRobotoMediam(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEditTextRobotoMediam(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditTextRobotoMediam(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface face = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Medium.ttf");
            setTypeface(face);
        }
    }

}
