package com.vinfotech.font;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user on 1/4/2016.
 */
public class CustomTextViewRobotoMediam extends TextView {

    Context context;

    public CustomTextViewRobotoMediam(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);
    }

    public CustomTextViewRobotoMediam(Context context) {
        super(context);
        this.context = context;
        init(null);
    }

    public CustomTextViewRobotoMediam(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);

    }

    public void init(AttributeSet attr) {

        if (attr != null) {

            Typeface face = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Medium.ttf");
            this.setTypeface(face);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}

