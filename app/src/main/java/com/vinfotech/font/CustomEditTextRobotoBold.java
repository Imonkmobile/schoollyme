package com.vinfotech.font;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by user on 1/4/2016.
 */
@SuppressLint("AppCompatCustomView")
public class CustomEditTextRobotoBold extends EditText {

    public CustomEditTextRobotoBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEditTextRobotoBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditTextRobotoBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface face = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Bold.ttf");
            setTypeface(face);
        }
    }

}
