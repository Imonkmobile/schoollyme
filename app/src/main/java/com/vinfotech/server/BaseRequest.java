package com.vinfotech.server;

import org.json.JSONArray;
import org.json.JSONObject;

import android.text.TextUtils;

import com.vinfotech.utility.Config;

public abstract class BaseRequest {
    public String mMessage = Config.DEFAULT_SERVICE_ERROR;
    public String mResponseCode = "0";
    private JSONObject mRespJSONObject = null;
    private JSONObject mObject;

    // private String mRequest="";

    public boolean parse(String request, String json) {
        if (!TextUtils.isEmpty(json)) {
            // mRequest = request;
            try {
                mRespJSONObject = new JSONObject(json);
                mObject = mRespJSONObject;
                //	mRespJSONObject = mObject.optJSONObject(request);
                if (null != mRespJSONObject) {
                    mResponseCode = mRespJSONObject.optString("ResponseCode", "Response code not found");
                    mMessage = mRespJSONObject.optString("Message", "Message not found");
                    return true;
                } else {
                    mResponseCode = mObject.optString("ResponseCode", "Response code not found");
                    mMessage = mObject.optString("Message", "Message not found");
                    if (mResponseCode.equals("200") || mResponseCode.equals("201")) {
                        return true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * @return Received data object
     */
    protected JSONObject getDataObject() {
        if (null == mRespJSONObject && null == mObject) {
            return null;
        }
        if (null != mRespJSONObject) {
            return mRespJSONObject.optJSONObject("Data");
        } else {
            return mObject.optJSONObject("Data");
        }
    }

    /**
     * @param name
     * @return
     */
    protected JSONObject getObject(String name) {
        if (null == mRespJSONObject || TextUtils.isEmpty(name)) {
            return null;
        }

        return mRespJSONObject.optJSONObject(name);
    }

    protected int getTotalRecord() {
        if (null != mRespJSONObject) {
            String totalRecordsStr = mRespJSONObject.optString("totalRecords", mRespJSONObject.optString("TotalRecords"));
            try {
                if (totalRecordsStr.contains("+")) {
                    totalRecordsStr = totalRecordsStr.substring(0, totalRecordsStr.lastIndexOf("+"));
                    return Integer.parseInt(totalRecordsStr);
                }
            }catch (Exception e){

            }
            return mRespJSONObject.optInt("totalRecords", mRespJSONObject.optInt("TotalRecords", 0));
        }
        return 0;
    }

    /**
     * @return Received data array
     */
    protected JSONArray getDataArray() {
        if (null == mRespJSONObject) {
            return null;
        }
        return mRespJSONObject.optJSONArray("Data");
    }

    protected JSONArray getDataArray(String parameter) {
        if (null != mRespJSONObject) {
            return mRespJSONObject.optJSONArray(parameter);
        } else if (null != mObject) {
            return mObject.optJSONArray(parameter);
        }
        return null;
    }

    protected JSONArray getJsonArray(String parameter) {
        if (null != mRespJSONObject) {
            return mRespJSONObject.optJSONArray(parameter);
        } else if (null != mObject) {
            return mObject.optJSONArray(parameter);
        } else {
            return null;
        }
    }

    /**
     * @return Received data array
     */
    protected JSONArray getArray(String name) {
        if (null == mRespJSONObject || TextUtils.isEmpty(name)) {
            return null;
        }

        return mRespJSONObject.optJSONArray(name);
    }

    /**
     * @return Status code
     */
    public boolean isSuccess() {
        return ("200".equalsIgnoreCase(mResponseCode) || "201".equalsIgnoreCase(mResponseCode));
    }

    /**
     * @return Response code
     */
    public String getResponseCode() {
        return this.mResponseCode;
    }

    /**
     * @return Parse message. It may be error or success message
     */
    public String getMessage() {
        return this.mMessage;
    }

    protected int mMsgResId = 0;
    protected boolean mActivityLive = true;
    protected RequestListener mRequestListener;

    public interface RequestListener {
        void onComplete(boolean success, Object data, int totalRecords);
    }

    public void setRequestListener(RequestListener listener) {
        this.mRequestListener = listener;
    }

    /**
     * Method for parsing inner data. Outer data will be parsed by local
     * BaseRequest.parse(String request, String json) method
     *
     * @param json
     * @return
     */
    protected abstract boolean parse(String json);

    /**
     * Sets custom loader message
     *
     * @param msgResId
     */
    public void setLoadingMsg(int msgResId) {
        this.mMsgResId = msgResId;
    }

    /**
     * Set the current activity status. onStart() set true and onStop() set
     * false;
     *
     * @param live
     */
    public abstract void setActivityStatus(boolean live);

    /**
     * Login Session key required by all web services
     */
    private static String sLoginSessionKey;

    /**
     * Set the Key
     *
     * @param key
     */
    public static void setLoginSessionKey(String key) {
        sLoginSessionKey = key;
    }

    /**
     * Gets the login session key
     *
     * @return
     */
    public static String getLoginSessionKey() {
        return sLoginSessionKey;
    }
}
