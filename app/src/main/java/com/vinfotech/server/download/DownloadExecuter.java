package com.vinfotech.server.download;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.vinfotech.utility.Config;

public class DownloadExecuter {
	private static final String TAG = DownloadExecuter.class.getSimpleName();

	private DownloadListener mDownloadListener;
	private CancelListener mCancelListener;
	
	private Activity mActivity;
	private boolean mCanceled = false;
	private String mDownloadingMsg = "Downloading file. Please wait...";

	public DownloadExecuter(Activity activity) {
		mActivity = activity;
	}

	public void setDownloadListener(DownloadListener listener) {
		mDownloadListener = listener;
	}
	
	public void setCancelListener(CancelListener listener){
		mCancelListener = listener;
	}

	public void setMessage(String message) {
		mDownloadingMsg = message;
	}

	public void cancel() {
		mCanceled = true;
	}

	public void start(String url, File toDownloadFile, boolean runInBg) {
		if (TextUtils.isEmpty(url) || !android.util.Patterns.WEB_URL.matcher(url).matches()) {
			if (Config.DEBUG) {
				Log.w(TAG, "start Invalid url=" + url);
			}
			return;
		}
		new DownloadFileFromURL(url, toDownloadFile, runInBg ? null : getDialog()).execute(url);
	}

	private class DownloadFileFromURL extends AsyncTask<String, Integer, Long> {
		private String mURL;
		private File mDownloadedFile;
		private ProgressDialog mProgressDialog;

		public DownloadFileFromURL(String donwloadUrl, File file, ProgressDialog progressDialog) {
			mURL = donwloadUrl;
			mDownloadedFile = file;
			mProgressDialog = progressDialog;
		}

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (null != mProgressDialog) {
				mProgressDialog.show();
			}

			if (null != mDownloadListener) {
				mDownloadListener.onDownloadStart(mURL);
			}
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected Long doInBackground(String... urls) {
			long total = 0;
			try {
				URL url = new URL(mURL);
				URLConnection conection = url.openConnection();
				conection.connect();
				int lenghtOfFile = conection.getContentLength();
				InputStream input = new BufferedInputStream(url.openStream(), 8192);
				OutputStream output = new FileOutputStream(mDownloadedFile);

				byte data[] = new byte[1024];
				int count;
				while ((count = input.read(data)) != -1 && !mCanceled) {
					total += count;
					publishProgress((int) ((total * 100) / lenghtOfFile));
					output.write(data, 0, count);
				}

				output.flush();
				output.close();
				input.close();

				if(null != mCancelListener && mCanceled){
					mCancelListener.onCancel(mURL, mDownloadedFile);
				}
			} catch (Exception e) {
				Log.e(TAG, "doInBackground, Error: " + e.getMessage());
			}

			return total;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(Integer... progress) {
			int percent = 0;
			if (null != progress && progress.length > 0) {
				try {
					percent = progress[0];
				} catch (Exception e) {
					percent = 0;
				}
				if (null != mProgressDialog && mProgressDialog.isShowing()) {
					mProgressDialog.setProgress(percent);
				}
				if (null != mDownloadListener) {
					mDownloadListener.onProgressChange(mURL, percent);
				}
			}
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(Long result) {
			if (null != mProgressDialog && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}

			if (null != mDownloadListener) {
				mDownloadListener.onDownloadEnd(mURL, mDownloadedFile, result);
			}
		}

	}

	private ProgressDialog getDialog() {
		ProgressDialog pDialog = new ProgressDialog(mActivity);
		pDialog.setMessage(mDownloadingMsg);
		pDialog.setIndeterminate(false);
		pDialog.setMax(100);
		pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pDialog.setCancelable(true);
		pDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				cancel();
			}
		});
		return pDialog;
	}

	public interface DownloadListener {
		void onDownloadStart(String url);

		void onDownloadEnd(String url, File file, long size);

		void onProgressChange(String url, int progress);
	}
	
	public interface CancelListener{
		void onCancel(String url, File file);
	}
}
