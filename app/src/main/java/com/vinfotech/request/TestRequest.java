package com.vinfotech.request;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.model.UserModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class TestRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = TestRequest.class.getSimpleName();

	private static final int REQ_CODE_ABOUT = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	private UserModel mUserModel;

	public TestRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void call() {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already requsting....");
			}
			return;
		}

		mRequesting = true;
	//	String jsonData = getAboutJson(LoginSessionKey, UserGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
	//	mHttpConnector.requestPost("http://www.mychildapp.in/blog/wp-json/posts/");
		mHttpConnector.executeAsync("http://www.mychildapp.in/blog/wp-json/posts/", REQ_CODE_ABOUT, true, false, 4);
	}



	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getAboutJson(String LoginSessionKey, String UserGUID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", LoginSessionKey,"ProfileURL", UserGUID);
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ABOUT:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + mUserModel);
				}
				if (null != mRequestListener) {
					try {
						mRequestListener.onComplete(true, mUserModel, 0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} 
			else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to SignIn. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		try {
			JSONArray jArray = new JSONArray(json);
			Log.i("Response ", "Data : "+jArray.length());
			for(int i=0;i<jArray.length();i++){
				Log.i("Response ", "Object contains : "+jArray.optJSONObject(i));
				Log.i("Response ", ">>>>>>>>>>><<<<<<<<<<");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*if (super.parse("get_user_profile", json) && isSuccess()) {
			mUserModel = new UserModel(getDataObject());
			LogedInUserModel mLogedInUserModel = new LogedInUserModel(mContext);
			if(mLogedInUserModel.mUserProfileURL.equals(mUserModel.mUserProfileURL)){
				mLogedInUserModel.isPaidCoach = mUserModel.mUserIsPaidCoach;
				mLogedInUserModel.isEvaluator = mUserModel.mUserIsEvaluator;
			}
			mLogedInUserModel.persist(mContext);
			return true;
		}*/
		return false;
	}
}