package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.model.Album;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class AlbumDetailRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = AlbumDetailRequest.class.getSimpleName();

	public static final String SORT_BY_ALBUMNAME = "AlbumName";
	public static final String SORT_BY_CREATEDDATE = "CreatedDate";
	public static final String ORDER_BY_ALBUMNAME = "ASC";
	public static final String ORDER_BY_CREATEDDATE = "DESC";
	public static final String ALBUM_TYPE_PHOTO = "PHOTO";
	public static final String ALBUM_TYPE_VIDEO = "VIDEO";

	private static final int REQ_CODE_ALBUM_DETAIL = 2;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private Album mAlbum;

	public AlbumDetailRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getAlbumDetailFromServer(String AlbumGUID) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting album detail...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAlbumDetailJSON(AlbumGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/details", REQ_CODE_ALBUM_DETAIL, "post", false, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	// {
	// "LoginSessionKey": "341e30da-560e-0f86-5c3e-36cc8ebd1d4a",
	// "AlbumGUID": "341e30da-560e-0f86-5c3e-36cc8ebd1d4a"
	// }

	private String getAlbumDetailJSON(String AlbumGUID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumGUID", AlbumGUID);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ALBUM_DETAIL:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "Album detail successful totalRecords=" + totalRecords + ", mAlbum=" + mAlbum);
				}
				if (null != mRequestListener) {

					mRequestListener.onComplete(true, mAlbum, totalRecords);

				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get album detail. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	// "ResponseCode": 200,
	// "Message": "Success",
	// "Data": {
	// "AlbumGUID": "341e30da-560e-0f86-5c3e-36cc8ebd1d4a",
	// "AlbumName": "My Photo",
	// "Description": "Testing",
	// "Visibility": "1",
	// "MediaCount": 0,
	// "CoverMedia": "abc.jpg",
	// "IsEditable": "1",
	// "NoOfComments": "10",
	// "NoOfLikes": "2",
	// "IsLike": 0,
	// "ActivityGUID": "341e30da-560e-0f86-5c3e-36cc8ebd1ddfs",
	// "Comments": [
	// {
	// "ProfilePicture": "439ecbeaeece74546297d681fef7170b.png",
	// "CommentGUID": "50537975-b0e1-258a-c11d-2adea054f106",
	// "PostComment": "adasd\u00ad",
	// "Name": "Suresh Patidar",
	// "CreatedDate": "2015-06-03 09:13:13",
	// "CanDelete": 1,
	// "ProfileLink": "suresh",
	// "IsLike": 0,
	// "NoOfLikes": 0,
	// "IsMediaExists": "1",
	// "ModuleID": 3,
	// "Media": {
	// "ImageName": "481ed3afcd80df26d14fade0187efd0b.jpg",
	// "Caption": "481ed3afcd80df26d14fade0187efd0b.jpg"
	// }
	// }
	// ]
	// },
	@Override
	protected boolean parse(String json) {
		if (super.parse("album", json) && isSuccess()) {

			mAlbum = new Album(getDataObject());
			JSONObject jsonObject = getDataObject();

			if (null != jsonObject) {

				return true;
			}
		}
		return false;
	}
}
