package com.vinfotech.request;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.schollyme.R;
import com.schollyme.model.NFFilter;
import com.schollyme.model.NewsFeed;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class KiltdFeedRequest extends BaseRequest implements HttpResponseListener {

	public static final String TAG = KiltdFeedRequest.class.getSimpleName();

	public static final int MODULE_ID_GROUPS = 1;
	public static final int MODULE_ID_USERS = 3;
	public static final int MODULE_ID_EVENT = 14;

	public static final int FEED_SHORT_BY_NONE = -1;
	// public static final int FEED_SHORT_BY_TOP_STATUS = 1;
	public static final int FEED_SHORT_BY_MOST_RECENT = 1;

	public static final int ALL_ACTIVITY_NONE = -1;
	public static final int ALL_ACTIVITY_WALL_POST = 0;
	public static final int ALL_ACTIVITY_NEWS_FEED = 1;

	public static final int ACTIVITY_FILTER_TYPE_ALL = 0;
	public static final int ACTIVITY_FILTER_TYPE_FAVOURITE = 1;
	public static final int ACTIVITY_FILTER_TYPE_FLAGGED = 2;

	public static final int IS_MEDIA_EXISTS_NONE = -1;
	public static final int IS_MEDIA_EXISTS_TEXT = 0;
	public static final int IS_MEDIA_EXISTS_IMAGE = 1;
	public static final int IS_MEDIA_EXISTS_BOTH = 2;

	public static final int AS_OWNER_NOT_NONE = -1;
	public static final int AS_OWNER_NOT_APPLY = 0;
	public static final int AS_OWNER_PAGE_GROUP = 1;

	private static final int REQ_CODE_NEWS_FEED_LIST = 100;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private boolean mRunInBg = false;

	private List<NewsFeed> mNewsFeeds;

	public KiltdFeedRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_NEWS_FEED_LIST, 0, mMessage);
			}
		});
	}

	public 	void getNewsFeedListInServer(NFFilter nfFilter) {

		if (mRequesting || !mActivityLive || null == nfFilter) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting news feed list..." + nfFilter);
			}
			return;
		}

		mRequesting = true;
		String jsonData = getNewsFeedListJSON(nfFilter);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("activity", REQ_CODE_NEWS_FEED_LIST,
				"post", mRunInBg, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
		mRunInBg = (null == view);
	}

	public class Params {
		public int ModuleID = MODULE_ID_USERS;
	}

	private String getNewsFeedListJSON(NFFilter nfFilter) {
		JSONObject jsonObject = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(),
                "ModuleID", Integer.toString(nfFilter.ModuleID),
                "EntityGUID", nfFilter.EntityGUID,
                "ActivityFIlterType", Integer.toString(nfFilter.ActivityFIlterType),
                "PageNo", Integer.toString(nfFilter.PageNo),
                "PageSize", Integer.toString(Config.PAGE_SIZE_10),
                "MyBuzz", "0", "MyKiltd", "1");





		if (FEED_SHORT_BY_NONE != nfFilter.FeedSortBy) {
			try {
				jsonObject.put("FeedSortBy", nfFilter.FeedSortBy);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (ALL_ACTIVITY_NONE != nfFilter.AllActivity) {
			try {
				jsonObject.put("AllActivity", "0");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (IS_MEDIA_EXISTS_NONE != nfFilter.IsMediaExists) {
			try {
				jsonObject.put("IsMediaExists", nfFilter.IsMediaExists);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (!TextUtils.isEmpty(nfFilter.FeedUser)) {
			try {
				jsonObject.put("FeedUser", nfFilter.FeedUser);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		String startDate = nfFilter.getStartDate();
		if (!TextUtils.isEmpty(startDate)) {
			try {
				jsonObject.put("StartDate", startDate);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		String endDate = nfFilter.getEndDate();
		if (!TextUtils.isEmpty(endDate)) {
			try {
				jsonObject.put("EndDate", endDate);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (AS_OWNER_NOT_NONE != nfFilter.AsOwner) {
			try {
				jsonObject.put("AsOwner", nfFilter.AsOwner);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (!TextUtils.isEmpty(nfFilter.ActivityGUID)) {
			try {
				jsonObject.put("ActivityGUID", nfFilter.ActivityGUID);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		System.out.println("Kiltd request    "+jsonObject);

		return null == jsonObject ? "" : jsonObject.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_NEWS_FEED_LIST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "NewsFeed list successful totalRecords="
							+ totalRecords + ", mNewsFeeds=" + mNewsFeeds);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mNewsFeeds, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get NewsFeed list. Error: "
							+ getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {

		if (super.parse("activity", json) && isSuccess()) {
			System.out.println("JSON DATA- " + getDataArray());
			mNewsFeeds = NewsFeed.getNewsFeeds(getDataArray());
			return true;
		}
		return false;
	}
}