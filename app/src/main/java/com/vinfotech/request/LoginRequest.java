package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class LoginRequest extends BaseRequest implements HttpResponseListener {
    public static final String TAG = LoginRequest.class.getSimpleName();

    private static final int REQ_CODE_LOGIN = 1;

    private Context mContext;
    private boolean mRequesting;
    private HttpConnector mHttpConnector;

    private LogedInUserModel mLogedInUserModel;

    public LoginRequest(Context context) {
        this.mContext = context;
        mRequesting = false;
        mHttpConnector = new HttpConnector(context);
        mHttpConnector.setHttpResponseListener(this);
    }

    public void LoginRequestServer(String Username, String Password, String DeviceType, String DeviceID,
                                   String Latitude, String Longitude, String IPAddress, String Token) {

        if (mRequesting || !mActivityLive) {
            if (Config.DEBUG) {
                Log.v(TAG, "You Already Login....");
            }
            return;
        }

        mRequesting = true;
        String jsonData = getSignInJson(Username, Password, DeviceType, DeviceID, Latitude, Longitude, IPAddress, Token);
        mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
        mHttpConnector.executeAsync("login/", REQ_CODE_LOGIN, "post", false, jsonData, null, UrlType.SERVICE);

    }

    public void setLoader(View view) {
        mHttpConnector.setLoader(view);
    }

    private String getSignInJson(String Username, String Password, String DeviceType, String DeviceID,
                                 String Latitude, String Longitude, String IPAddress, String Token) {

        JSONObject jsonObj = JSONUtil.getJSONObject("Username", Username, "Password", Password,
                "DeviceType", DeviceType, "DeviceID", DeviceID, "Latitude", Latitude, "Longitude",
                Longitude, "IPAddress", IPAddress, "Token", Token);

        return null == jsonObj ? "" : jsonObj.toString();

    }

    @Override
    public void onResponse(int reqCode, int statusCode, String json) {

        switch (reqCode) {
            case REQ_CODE_LOGIN:
                mRequesting = false;
                if (parse(json)) {
                    if (Config.DEBUG) {
                        Log.v(TAG, "onResponse " + mLogedInUserModel);
                    }
                    if (null != mRequestListener) {
                        try {
                            mLogedInUserModel = new LogedInUserModel(getDataObject());
                            mLogedInUserModel.persist(mContext);
                            mRequestListener.onComplete(true, mLogedInUserModel, 0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (getResponseCode().equals("501")) {
                    try {
                        mLogedInUserModel = new LogedInUserModel(getDataObject());
                        mLogedInUserModel.persist(mContext);
                        mRequestListener.onComplete(true, mLogedInUserModel, 0);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // Handling Error Messages
                    if (Config.DEBUG) {
                        Log.e(TAG, "Failed to SignIn. Error: " + getMessage());
                    }
                    if (null != mRequestListener) {
                        mRequestListener.onComplete(false, getMessage(), 0);
                    }
                }
                break;

            default:
                break;
        }
    }


    @Override
    public void onCancel(boolean canceled) {

    }

    @Override
    public void onProgressChange(int progress) {

    }

    @Override
    public void setActivityStatus(boolean live) {
        super.mActivityLive = live;
        mHttpConnector.setActivityStatus(mActivityLive);
    }

    @Override
    protected boolean parse(String json) {
        if (super.parse("login", json) && isSuccess()) {

            return true;
        }
        return false;
    }
}