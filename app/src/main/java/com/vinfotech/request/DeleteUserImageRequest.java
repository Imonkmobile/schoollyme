package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class DeleteUserImageRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = DeleteUserImageRequest.class.getSimpleName();

	private static final int REQ_CODE_CHANGE_PASSWORD = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public DeleteUserImageRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void deleteImageServerRequest(String type,String AccessToken) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already Login....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getDeleteImageJson(type,AccessToken);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("UsersApi/DeleteImage/", REQ_CODE_CHANGE_PASSWORD, "post", true, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getDeleteImageJson(String type,String AccessToken) {
		JSONObject jsonObj = JSONUtil.getJSONObject("type", type,"AccessToken",AccessToken);
		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_CHANGE_PASSWORD:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to SignIn. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("DeleteImage", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}