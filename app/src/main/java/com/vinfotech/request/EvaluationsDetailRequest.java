package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.Evaluation;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class EvaluationsDetailRequest extends BaseRequest implements HttpResponseListener {

	private static final String TAG = EvaluationsDetailRequest.class.getSimpleName();
	private static final int REQ_CODE_EVALUATION_DETAILS = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Evaluation mEvaluations;

	public EvaluationsDetailRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_EVALUATION_DETAILS, 0, mMessage);
			}
		});
	}

	public void EvaluationsDetailServerRequest(String mEvaluationGUID) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}
		mRequesting = true;
		String jsonData = getEvaluationsDetailJson(mEvaluationGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("evaluation/details/", REQ_CODE_EVALUATION_DETAILS, "post", true, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getEvaluationsDetailJson(String mEvaluationGUID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(),"EvaluationGUID",mEvaluationGUID);
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_EVALUATION_DETAILS:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mEvaluations, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to response. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("evaluation/details/", json) && isSuccess()) {
		
			try {
				mEvaluations = new Evaluation(getDataArray().getJSONObject(0));
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
}