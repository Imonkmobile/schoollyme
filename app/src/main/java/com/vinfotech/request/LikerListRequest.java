package com.vinfotech.request;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.LikerModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class LikerListRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = LikerListRequest.class.getSimpleName();

	public static final String ENTITYTYPE_ACTIVITY = "ACTIVITY";
	public static final String ENTITYTYPE_COMMENT = "COMMENT";
	public static final String ENTITYTYPE_MEDIA = "MEDIA";
	private static final int REQ_CODE_LIKER_LIST = 11;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private boolean mRunInBg = false;
	private List<LikerModel> mLikers;

	public LikerListRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_LIKER_LIST, 0, mMessage);
			}
		});
	}

	public void getLikerListInServer(String EntityGUID, String EntityType) {
		Log.v(TAG, "getLikerListInServer EntityGUID=" + EntityGUID + ", EntityType=" + EntityType);
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting liker list...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getLikerListJSON(EntityGUID, EntityType);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("activity/getLikeDetails", REQ_CODE_LIKER_LIST, "post", mRunInBg, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
		mRunInBg = (null == view);
	}

	private String getLikerListJSON(String EntityGUID, String EntityType) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "EntityGUID", EntityGUID, "EntityType",
				EntityType, "PageNo", Integer.toString(Config.DEFAULT_PAGE_INDEX), "PageSize", "1000");

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_LIKER_LIST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "Liker list successful totalRecords=" + totalRecords + ", mLikers=" + mLikers);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mLikers, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get liker list. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("getLikeDetails", json) && isSuccess()) {
			mLikers = LikerModel.getLikers(getDataArray());
			return true;
		}
		return false;
	}
}
