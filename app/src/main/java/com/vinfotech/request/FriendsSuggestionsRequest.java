package com.vinfotech.request;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.FriendModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class FriendsSuggestionsRequest extends BaseRequest implements HttpResponseListener {

	private static final String TAG = FriendsSuggestionsRequest.class.getSimpleName();
	private static final int REQ_CODE_FRIEND_SUGGESTIONS = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public FriendsSuggestionsRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_FRIEND_SUGGESTIONS, 0, mMessage);
			}
		});
	}

	public void FriendsSuggestionsServerRequest(String SortBy,String OrderBy,String PageNo,String PageSize,boolean flag) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getFriendsSuggestionsJson(SortBy,OrderBy,PageNo,PageSize);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("friends/suggested_friend/", REQ_CODE_FRIEND_SUGGESTIONS, "post", flag, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getFriendsSuggestionsJson(String SortBy,String OrderBy,String PageNo,String PageSize) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(),"SortBy",SortBy,"OrderBy",OrderBy,"PageNo",PageNo,"PageSize",PageSize);
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_FRIEND_SUGGESTIONS:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					ArrayList<FriendModel> mSuggestedFriendAL = new ArrayList<FriendModel>();
					try {
						JSONArray jArray = getDataArray("Data");
						if(jArray!=null)
							for(int i=0;i<jArray.length();i++){
								mSuggestedFriendAL.add(new FriendModel(jArray.getJSONObject(i)));
							}
					} catch (Exception e) {
						e.printStackTrace();
					}

					mRequestListener.onComplete(true, mSuggestedFriendAL, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to response. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("friends/suggested_friend/", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}