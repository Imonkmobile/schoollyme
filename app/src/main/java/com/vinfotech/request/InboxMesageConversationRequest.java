package com.vinfotech.request;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.model.ChatModel;
import com.schollyme.model.LogedInUserModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONObject;

import java.util.List;

public class InboxMesageConversationRequest extends BaseRequest implements
HttpResponseListener {
	public static final String TAG = InboxMesageConversationRequest.class.getSimpleName();

	private static final int REQ_CODE_INBOX_REQUEST = 1;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private List<ChatModel> mInboxList;

	public InboxMesageConversationRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getInboxMesageDetailServerRequest(String UserGUID, String PageNo,String PageSize ) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting Inbox details...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getInboxMesageConversationJSON(UserGUID, PageNo, PageSize);
		if(null!=mHttpConnector.getDialog()){
			mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext,mMsgResId));
		}

		mHttpConnector.executeAsync("messages/get_conversation/",REQ_CODE_INBOX_REQUEST, "post", false, jsonData, null,
				UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getInboxMesageConversationJSON(String UserGUID, String PageNo,String PageSize) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey",getLoginSessionKey(), "UserGUID", UserGUID,"PageNo", PageNo, "PageSize", PageSize);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_INBOX_REQUEST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "Comment list successful totalRecords="
							+ totalRecords + ", mInboxList=" + mInboxList);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mInboxList,
							totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get comment list. Error: "
							+ getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("messages/get_conversation/", json) && isSuccess()) {
			mInboxList = ChatModel.getMessageDetail(getDataObject(),new LogedInUserModel(mContext).mUserGUID,mContext);
			return true;
		}
		return false;
	}
}