package com.vinfotech.request;

import java.util.List;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.PageCategory;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class GetPageCategoryRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = GetPageCategoryRequest.class.getSimpleName();

	private static final int REQ_CODE_GET_PAGE_CATEGORY = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private List<PageCategory> mPageCategoies;

	public GetPageCategoryRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_GET_PAGE_CATEGORY, 0, mMessage);
			}
		});
	}

	public void GetPageCategoryServerRequest(String ModuleID) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getPageCategoryJSON(ModuleID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("category/getCategories/", REQ_CODE_GET_PAGE_CATEGORY, "post", true, jsonData, null, UrlType.SERVICE);

	}

	private String getPageCategoryJSON(String ModuleID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "ModuleID",ModuleID);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_GET_PAGE_CATEGORY:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mPageCategoies, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed  Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("category/getCategories/", json) && isSuccess()) {
			mPageCategoies = PageCategory.getPageCategorys(getJsonArray("Data"));
			return true;
		}
		return false;
	}
}