package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.DialogUtil.OnItemClickListener;
import com.vinfotech.utility.DialogUtil.SingleEditListener;
import com.vinfotech.utility.JSONUtil;

public class MarkSpamRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = MarkSpamRequest.class.getSimpleName();
	public static final String SPAM_TYPE_MARK_AS_SPAM = "Mark as Spam";
	public static final String SPAM_TYPE_INAPPROPRIATE_CONTENT = "Inappropriate Content";
	public static final String SPAM_TYPE_OTHER = "Other";

	private static final int REQ_CODE_MARK_SPAM = 5;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private String mURL;

	public MarkSpamRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void showFlagDialog(final String URL) {
		this.mURL = URL;
		final Resources resources = mContext.getResources();
		DialogUtil.showListDialog(mContext, R.string.Report, new OnItemClickListener() {

			@Override
			public void onItemClick(int position, String item) {
				if (0 == position) {
					flagMediaInServer(URL, SPAM_TYPE_MARK_AS_SPAM, "");
				} else if (1 == position) {
					flagMediaInServer(URL, SPAM_TYPE_INAPPROPRIATE_CONTENT, "");
				} else {
					DialogUtil.showSingleEditDialog(mContext, R.string.Why_should_this, "", new SingleEditListener() {
						
						@Override
						public void onEdit(boolean canceled, EditText editText, String text) {
							if(!canceled){
								flagMediaInServer(URL, SPAM_TYPE_OTHER, text);
							}
						}
					});
				}
			}

			@Override
			public void onCancel() {
				// Toast.makeText(mCategoryColors.getContext(),
				// "Action canceled", Toast.LENGTH_SHORT).show();
			}
		}, resources.getString(R.string.Mark_as_Spam), resources.getString(R.string.Inappropriate_Content),
				resources.getString(R.string.Other));
	}

	public void flagMediaInServer(String URL, String spamType, String reason) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already deleting media...");
			}
			return;
		}
		if (SPAM_TYPE_OTHER == spamType && TextUtils.isEmpty(reason)) {
			if (Config.DEBUG) {
				Log.e(TAG, "reason is mandatory if SPAM_TYPE_OTHER");
			}
			return;
		}
		this.mURL = URL;

		mRequesting = true;
		String jsonData = getMarkSpamJSON(URL, spamType, reason);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("MarkSpam/", REQ_CODE_MARK_SPAM, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getMarkSpamJSON(String URL, String spamType, String reason) {
		JSONObject jsonObj = JSONUtil.getJSONObject("loginSessionKey", getLoginSessionKey(), "URL", URL, "spamType", spamType, "reason",
				reason);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_MARK_SPAM:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Media delete successful: " + mURL);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mURL, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to delete media. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("MarkSpam", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}
