package com.vinfotech.request;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONObject;

public class AlbumExistsRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = AlbumExistsRequest.class.getSimpleName();
	public static final String REQ_CODE_ALBUM_TYPE_PHOTO = "PHOTO";
	public static final String REQ_CODE_ALBUM_TYPE_VIDEO = "VIDEO";

	private static final int REQ_CODE_ALBUM_NAME_EXISTS = 17;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	public AlbumExistsRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void checkAlbumExistsInServer(String AlbumName, String AlbumType) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already checking album name...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAlbumExistsJSON(AlbumName, AlbumType);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/check_name", REQ_CODE_ALBUM_NAME_EXISTS, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getAlbumExistsJSON(String AlbumName, String AlbumType) {
		JSONObject jsonObj = JSONUtil
				.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumName", AlbumName, "AlbumType", AlbumType);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ALBUM_NAME_EXISTS:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Check album name exists: " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, json, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to check album name. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("check_name", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}