package com.vinfotech.request;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.CommentModel;
import com.schollyme.model.NewMedia;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class AddCommentRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = AddCommentRequest.class.getSimpleName();
	public static final String ENTITYTYPE_ACTIVITY = "Activity";
	public static final String ENTITYTYPE_MEDIA = "Media";

	private static final int REQ_CODE_ADD_COMMENT = 11;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private final Context mContext;

	private CommentModel mComment;

	public AddCommentRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_ADD_COMMENT, 0, mMessage);
			}
		});
	}

	public void addCommentInServer(String Comment, String EntityType, String EntityGUID, List<NewMedia> newMedias) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already adding in album...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAddCommentJSON(Comment, EntityType, EntityGUID, newMedias);
		mHttpConnector.setDialog(DialogUtil.createProgressDialogDefaultWithMessage(mContext, R.string.Please_wait));
		mHttpConnector.executeAsync("activity/addComment", REQ_CODE_ADD_COMMENT, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getAddCommentJSON(String Comment, String EntityType, String EntityGUID, List<NewMedia> newMedias) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "Comment", Comment, "EntityType", EntityType,
				"EntityGUID", EntityGUID);

		if (null != newMedias) {
			try {
				jsonObj.put("Media", NewMedia.getJSONArray(newMedias));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ADD_COMMENT:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Comment add successful: " + mComment);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mComment, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to add comment. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("addComment", json) && isSuccess()) {
			List<CommentModel> comments = CommentModel.getComments(getDataArray());
			mComment = comments.size() > 0 ? comments.get(0) : null;
			return true;
		}
		return false;
	}
}
