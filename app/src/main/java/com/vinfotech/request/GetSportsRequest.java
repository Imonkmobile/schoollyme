package com.vinfotech.request;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.model.SportsModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;

public class GetSportsRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = GetSportsRequest.class.getSimpleName();

	private static final int REQ_CODE_GET_STATES = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private List<SportsModel> mSportsModels;

	public GetSportsRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void GetSportsServerRequest() {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}

		mRequesting = true;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("sports/get_sports_list/", REQ_CODE_GET_STATES, "post", false, null, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_GET_STATES:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mSportsModels, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed  Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("get_sports_list", json) && isSuccess()) {
			mSportsModels = SportsModel.getSportsModels(getJsonArray("Data"));
			return true;
		}
		return false;
	}
}