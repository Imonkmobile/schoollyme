package com.vinfotech.request;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.model.ChatModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class InboxSendMessageRequest extends BaseRequest implements HttpResponseListener {
    public static final String TAG = InboxSendMessageRequest.class.getSimpleName();

    private static final int REQ_CODE_SEND_MESSAGE = 1;

    private ChatModel mModel;
    private Context mContext;
    private boolean mRequesting;
    private HttpConnector mHttpConnector;

    public InboxSendMessageRequest(Context context) {
        this.mContext = context;
        mRequesting = false;
        mHttpConnector = new HttpConnector(context);
        mHttpConnector.setHttpResponseListener(this);
    }

    public void InboxSendMessageServerRequest(String Subject, String Body, String ReceiversId) {

        if (mRequesting || !mActivityLive) {
            if (Config.DEBUG) {
                Log.v(TAG, "You Already request....");
            }
            return;
        }
        mRequesting = true;
        String jsonData = getInboxSendMessageJson(Subject, Body, ReceiversId);
        mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
        mHttpConnector.executeAsync("messages/sendMessage/", REQ_CODE_SEND_MESSAGE, "post", true, jsonData, null, UrlType.SERVICE);
    }

    public void setLoader(View view) {
        mHttpConnector.setLoader(view);
    }

    private String getInboxSendMessageJson(String Subject, String Body, String ReceiversId) {
        JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "Subject", Subject, "Body", Body);
        try {
            JSONArray jArray = new JSONArray();
            jArray.put(ReceiversId);
            jsonObj.put("Receivers", jArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null == jsonObj ? "" : jsonObj.toString();

    }

    @Override
    public void onResponse(int reqCode, int statusCode, String json) {

        switch (reqCode) {
            case REQ_CODE_SEND_MESSAGE:
                mRequesting = false;
                if (parse(json)) {
                    if (Config.DEBUG) {
                        Log.v(TAG, "onResponse " + mModel);
                    }
                    if (null != mRequestListener) {
                        mRequestListener.onComplete(true, mModel, 0);
                    }
                } else {
                    // Handling Error Messages
                    if (Config.DEBUG) {
                        Log.e(TAG, "Failed to response. Error: " + mModel);
                    }
                    if (null != mRequestListener) {
                        mRequestListener.onComplete(false, getMessage(), 0);
                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onCancel(boolean canceled) {

    }

    @Override
    public void onProgressChange(int progress) {

    }

    @Override
    public void setActivityStatus(boolean live) {
        super.mActivityLive = live;
        mHttpConnector.setActivityStatus(mActivityLive);
    }

    @Override
    protected boolean parse(String json) {
        if (super.parse("messages/sendMessage/", json) && isSuccess()) {

            mModel = new ChatModel(getDataObject(), mContext);
            return true;
        }
        return false;
    }
}