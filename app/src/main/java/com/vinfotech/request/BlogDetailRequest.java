package com.vinfotech.request;

import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.BlogModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class BlogDetailRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = BlogDetailRequest.class.getSimpleName();

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private boolean mRunInBg = false;
	private List<BlogModel> mBlogModelList;
	private static final int REQ_CODE_NEWS_FEED_LIST = 100;

	public BlogDetailRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {

			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_NEWS_FEED_LIST, 0, mMessage);
			}
		});
	}

	public void getBlogDetailInServer(String blogGUID) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting blog list...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = BlogDetail(blogGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("blog/details", REQ_CODE_NEWS_FEED_LIST, "post", mRunInBg, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
		mRunInBg = (null == view);
	}

	/*
	 * { "LoginSessionKey": "4698184e-76a5-e0c5-e4d3-19a1d68dcdb7", "BlogGUID":
	 * "4698184e-76a5-e0c5-e4d3-19a1d68dcDFG" }
	 */
	private String BlogDetail(String blogGUID) {
		JSONObject jsonObject = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "BlogGUID", blogGUID);
		return null == jsonObject ? "" : jsonObject.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_NEWS_FEED_LIST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "NewsFeed list successful totalRecords=" + totalRecords + ", mNewsFeeds=" + mBlogModelList);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mBlogModelList, totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get NewsFeed list. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {

		if (super.parse("blog/list", json) && isSuccess()) {
			mBlogModelList = BlogModel.getBlogList(getDataArray());
			return true;
		}
		return false;
	}
}
