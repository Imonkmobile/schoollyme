package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class CoachTranscriptsRequest extends BaseRequest implements HttpResponseListener {

    public static final String TAG = CoachTranscriptsRequest.class.getSimpleName();
    private static final int REQ_CODE_VERIFY_SCORES_REQUEST = 1;
    private boolean mRequesting;
    private HttpConnector mHttpConnector;
    private Context mContext;

    public CoachTranscriptsRequest(Context context) {
        this.mContext = context;
        mRequesting = false;
        mHttpConnector = new HttpConnector(mContext);
        mHttpConnector.setHttpResponseListener(this);
        mHttpConnector.setINetConnListener(new INetConnListener() {
            @Override
            public void onNotConnected() {
                mMessage = mContext.getString(R.string.No_internet_connection);
                onResponse(REQ_CODE_VERIFY_SCORES_REQUEST, 0, mMessage);
            }
        });
    }

    public void getCoachTranscriptRequest(String mRequestType) {
        if (mRequesting || !mActivityLive) {
            if (Config.DEBUG) {
                Log.v(TAG, "Already requesting ");
            }
            return;
        }

        mRequesting = true;
        String jsonData = getCoachTranscriptJSON(mRequestType);
        mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
        mHttpConnector.executeAsync("users/transcript_request/", REQ_CODE_VERIFY_SCORES_REQUEST, "post",
                false, jsonData, null, UrlType.SERVICE);

    }

    public void setLoader(View view) {
        mHttpConnector.setLoader(view);
    }

    private String getCoachTranscriptJSON(String mAthleteUserGUID) {
        JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "UserGUID", mAthleteUserGUID);

        return null == jsonObj ? "" : jsonObj.toString();

    }

    @Override
    public void onResponse(int reqCode, int statusCode, String json) {

        switch (reqCode) {
            case REQ_CODE_VERIFY_SCORES_REQUEST:
                mRequesting = false;
                if (parse(json)) {
                    final int totalRecords = getTotalRecord();
                    if (Config.DEBUG) {
                        Log.v(TAG, "Transcripts request sent");
                    }
                    if (null != mRequestListener) {
                        mRequestListener.onComplete(true, getMessage(),
                                totalRecords);
                    }
                } else {
                    // Handling Error Messages
                    if (Config.DEBUG) {
                        Log.e(TAG, "Failed to send Transcripts request "
                                + getMessage());
                    }
                    if (null != mRequestListener) {
                        mRequestListener.onComplete(false, getMessage(), 0);
                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onCancel(boolean canceled) {

    }

    @Override
    public void onProgressChange(int progress) {

    }

    @Override
    public void setActivityStatus(boolean live) {
        super.mActivityLive = live;
        mRequesting = false;
        mHttpConnector.setActivityStatus(mActivityLive);
    }

    @Override
    protected boolean parse(String json) {
        if (super.parse("users/transcript_request/", json) && isSuccess()) {
            return true;
        }
        return false;
    }
}