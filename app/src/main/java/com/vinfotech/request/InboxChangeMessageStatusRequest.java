package com.vinfotech.request;

import java.util.List;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.vinfotech.utility.Config;
import com.schollyme.model.InboxModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class InboxChangeMessageStatusRequest extends BaseRequest implements
		HttpResponseListener {
	public static final String TAG = InboxChangeMessageStatusRequest.class.getSimpleName();

	private static final int REQ_CODE_INBOX_CHANGE_MESSGAE_STATUS_REQUEST = 1;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	private List<InboxModel> mInboxList;

	public InboxChangeMessageStatusRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getInboxChangeStatusServerRequest(String MessageGUID, String MessageReceiverGUID,String Status,boolean DeleteThread) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting Inbox list...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getInboxChangeStatusJSON(MessageGUID, MessageReceiverGUID,Status,DeleteThread);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext,
				mMsgResId));
		mHttpConnector.executeAsync("messages/ChangeMessageStatus/",REQ_CODE_INBOX_CHANGE_MESSGAE_STATUS_REQUEST, "post", true, jsonData, null,
				UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getInboxChangeStatusJSON(String MessageGUID, String MessageReceiverGUID,String Status,boolean DeleteThread) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey",getLoginSessionKey(), "MessageGUID", MessageGUID,"MessageReceiverGUID", MessageReceiverGUID, "Status", Status);
		try {
			jsonObj.put("DeleteThread",DeleteThread);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_INBOX_CHANGE_MESSGAE_STATUS_REQUEST:
			mRequesting = false;
			if (parse(json)) {
				final int totalRecords = getTotalRecord();
				if (Config.DEBUG) {
					Log.v(TAG, "Comment list successful totalRecords="
							+ totalRecords + ", mInboxList=" + mInboxList);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(),
							totalRecords);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get comment list. Error: "
							+ getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("messages/ChangeMessageStatus/", json) && isSuccess()) {
			//mInboxList = InboxModel.getMessage(getDataArray());
			return true;
		}
		return false;
	}
}