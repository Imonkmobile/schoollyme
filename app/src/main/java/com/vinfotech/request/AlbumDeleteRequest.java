package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class AlbumDeleteRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = AlbumDeleteRequest.class.getSimpleName();

	private static final int REQ_CODE_ALBUM_DELETE = 7;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	public AlbumDeleteRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_ALBUM_DELETE, 0, mMessage);
			}
		});
	}

	public void deleteAlbumFromServer(String AlbumGUID) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already deleting album...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAlbumDeleteJSON(AlbumGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/delete", REQ_CODE_ALBUM_DELETE, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getAlbumDeleteJSON(String AlbumGUID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumGUID", AlbumGUID);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ALBUM_DELETE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Album delete successful: " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, json, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to delete album. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("delete", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}
