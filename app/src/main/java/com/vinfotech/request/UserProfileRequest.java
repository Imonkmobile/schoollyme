package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.model.RenovateUser;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class UserProfileRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = UserProfileRequest.class.getSimpleName();

	private static final int REQ_CODE_GET_USER_PROFILE = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private RenovateUser mRenovateUser;

	public UserProfileRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getProfileServerRequest(String ImageDeviceModel,String EventTags,String AccessToken) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already requesting....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getProfileJson(ImageDeviceModel,EventTags,AccessToken);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("UsersApi/GetUserDetail/", REQ_CODE_GET_USER_PROFILE, "post", false, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getProfileJson(String ImageDeviceModel,String EventTags,String AccessToken) {
		JSONObject jsonObj = JSONUtil.getJSONObject("EventTags", EventTags,"ImageDeviceModel",ImageDeviceModel,"AccessToken",AccessToken);
		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_GET_USER_PROFILE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mRenovateUser, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to SignIn. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("GetUserDetail", json) && isSuccess()) {
			mRenovateUser = new RenovateUser(getDataObject());
			return true;
		}
		return false;
	}
}