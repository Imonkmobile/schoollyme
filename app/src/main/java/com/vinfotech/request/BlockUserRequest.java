package com.vinfotech.request;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONObject;

public class BlockUserRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = BlockUserRequest.class.getSimpleName();

	private static final int REQ_CODE_BLOCK_USER = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public BlockUserRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void BlockUserServerRequest(String EntityGUID,String ModuleID,String ModuleEntityGUID) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}
		mRequesting = true;
		String jsonData = getFriendAcceptJson(EntityGUID,ModuleID,ModuleEntityGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("activity/blockUser/", REQ_CODE_BLOCK_USER, "post", true, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getFriendAcceptJson(String EntityGUID,String ModuleID,String ModuleEntityGUID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey",getLoginSessionKey(),"EntityGUID",EntityGUID,"ModuleID",ModuleID,"ModuleEntityGUID",ModuleEntityGUID);
		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_BLOCK_USER:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} 
			else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to response. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("activity/blockUser/",json) && isSuccess()) {
			
			return true;
		}
		return false;
	}
}