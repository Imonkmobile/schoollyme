package com.vinfotech.request;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.R;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SetupProfileRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = SetupProfileRequest.class.getSimpleName();
	private static final int REQ_CODE_SETUP_PROFILE = 1;
	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public SetupProfileRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}


	public void SetupProfileSeverRequest(String name,String userName,String email,String dob,String Gender,String StateID,ArrayList<String> SportsID,String School,String CoachingTitle,String CoachingPhilosophy,String Mentor,String AlmaMater,String FavoriteAthlete,String FavoriteTeam,String FavoriteMovie,String FavoriteMusic,String FavoriteFood,String FavoriteTeacher,String ProfilePicture) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already requested....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSetupProfileCoachJson(name,userName,email,dob,Gender,StateID,SportsID,School,CoachingTitle,CoachingPhilosophy,Mentor,AlmaMater,FavoriteAthlete,FavoriteTeam,FavoriteMovie,FavoriteMusic,FavoriteFood,FavoriteTeacher,ProfilePicture);
		mMsgResId = R.string.skip_loading_message;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("users/updateUserProfile/", REQ_CODE_SETUP_PROFILE, "post", false, jsonData, null, UrlType.SERVICE);

	}
	
	public void SetupProfileSeverRequest(String name,String userName,String email,String dob, String Gender,String StateID,ArrayList<String> SportsID,String POSITION ,String HT,
			String JerseyNumber ,String NameOfCurrentSchool,String ClubTeam,String FavoriteAthlete,String FavoriteTeam,String FavoriteMovie,String FavoriteMusic,String FavoriteFood,String FavoriteTeacher,
			String ProfilePicture,String FavoriteQuote,String HomeAddress,String PhoneNumber,String AlternateEmail,String GPA,String SATScore,String ACTScore,String Transcript ,String NCAAEligibility,
			String ParentID,String ParentsName,String ParentsEmail,String ParentsPhoneNumber,String RelationWithUser,String mHeight) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already requested....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSetupProfileAthleteJson(name,userName,email,dob, Gender,StateID, SportsID,POSITION ,HT,
				JerseyNumber ,NameOfCurrentSchool,ClubTeam,FavoriteAthlete,FavoriteTeam,FavoriteMovie, FavoriteMusic, FavoriteFood, FavoriteTeacher,
				 ProfilePicture, FavoriteQuote, HomeAddress, PhoneNumber, AlternateEmail, GPA, SATScore, ACTScore, Transcript , NCAAEligibility,
				 ParentID, ParentsName, ParentsEmail, ParentsPhoneNumber, RelationWithUser,mHeight);
		mMsgResId = R.string.skip_loading_message;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("users/updateUserProfile/", REQ_CODE_SETUP_PROFILE, "post", false, jsonData, null, UrlType.SERVICE);

	}
	
	public void SetupProfileSeverRequest(String name,String userName,String email,String dob, String Gender,String StateID,String FavoriteAthlete,String FavoriteTeam,String FavoriteMovie,String FavoriteMusic,String FavoriteFood,String FavoriteTeacher,String ProfilePicture,String FavoriteQuote) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already requested....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSetupProfileFanJson(name,userName,email,dob, Gender,StateID,FavoriteAthlete,FavoriteTeam, FavoriteMovie, FavoriteMusic, FavoriteFood, FavoriteTeacher, ProfilePicture, FavoriteQuote);
		mMsgResId = R.string.skip_loading_message;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("users/updateUserProfile/", REQ_CODE_SETUP_PROFILE, "post", false, jsonData, null, UrlType.SERVICE);

	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	public static String getSetupProfileCoachJson(String name,String userName,String email,String dob, String Gender,String StateID,ArrayList<String> SportsID,String School,String CoachingTitle,String CoachingPhilosophy,String Mentor,String AlmaMater,String FavoriteAthlete,String FavoriteTeam,String FavoriteMovie,String FavoriteMusic,String FavoriteFood,String FavoriteTeacher,String ProfilePicture) {

		JSONObject requestObject = null;
		requestObject = JSONUtil.getJSONObject("Username",userName,"LoginSessionKey",getLoginSessionKey(),"Name",name,"Email",email,"DOB",dob, "Gender", Gender, "StateID", StateID,"School",School,"CoachingTitle",CoachingTitle,"CoachingPhilosophy",CoachingPhilosophy,"Mentor",Mentor,"AlmaMater",AlmaMater,"FavoriteAthlete",FavoriteAthlete,"FavoriteTeam",FavoriteTeam,"FavoriteMovie",FavoriteMovie,"FavoriteMusic",FavoriteMusic,"FavoriteFood",FavoriteFood,"FavoriteTeacher",FavoriteTeacher,"ProfilePicture",ProfilePicture);
		JSONArray jArray = new JSONArray();
		try{
			for(int i=0;i<SportsID.size();i++){
				jArray.put(SportsID.get(i));
			}
			requestObject.put("SportsID",jArray);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null == requestObject ? "" : requestObject.toString();
	}

	public static String getSetupProfileAthleteJson(String name,String userName,String email,String dob, String Gender,String StateID,ArrayList<String> SportsID,String POSITION ,String HT,
					String JerseyNumber ,String NameOfCurrentSchool,String ClubTeam,String FavoriteAthlete,String FavoriteTeam,String FavoriteMovie,String FavoriteMusic,String FavoriteFood,String FavoriteTeacher,
					String ProfilePicture,String FavoriteQuote,String HomeAddress,String PhoneNumber,String AlternateEmail,String GPA,String SATScore,String ACTScore,String Transcript ,String NCAAEligibility,
					String ParentID,String ParentsName,String ParentsEmail,String ParentsPhoneNumber,String RelationWithUser,String mHeight) {

		JSONObject requestObject = null;
		requestObject = JSONUtil.getJSONObject("Username",userName,"LoginSessionKey",getLoginSessionKey(),"Name",name,"Email",email,"DOB",dob, "Gender", Gender, "StateID", StateID,"Position",POSITION ,"HT",HT,"JerseyNumber",JerseyNumber ,"School",NameOfCurrentSchool,"ClubTeam",ClubTeam,"FavoriteAthlete",FavoriteAthlete,"FavoriteTeam",FavoriteTeam,"FavoriteMovie",FavoriteMovie,"FavoriteMusic",FavoriteMusic,"FavoriteFood",FavoriteFood,"FavoriteTeacher",FavoriteTeacher,"ProfilePicture",ProfilePicture,"FavoriteQuote",FavoriteQuote,
												"HomeAddress",HomeAddress,"PhoneNumber",PhoneNumber,"AlternateEmail",AlternateEmail,"GPA",GPA,"SATScore",SATScore,"ACTScore",ACTScore,"Transcript ",Transcript,"NCAAEligibility",NCAAEligibility,"Height",mHeight,"NCAAEligibility",NCAAEligibility );
		JSONArray jArray = new JSONArray();
		try{
			for(int i=0;i<SportsID.size();i++){
				jArray.put(SportsID.get(i));
			}
			requestObject.put("SportsID",jArray);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		try{
			JSONObject parentJson = new JSONObject();
			parentJson.put("ParentID",ParentID);
			parentJson.put("ParentsName",ParentsName);
			parentJson.put("ParentsEmail",ParentsEmail);
			parentJson.put("ParentsPhoneNumber",ParentsPhoneNumber);
			parentJson.put("RelationWithUser",RelationWithUser);
			JSONArray parentArray = new JSONArray();
			parentArray.put(parentJson);
			requestObject.put("Parents",parentArray);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null == requestObject ? "" : requestObject.toString();
	}
	
	public static String getSetupProfileFanJson(String name,String userName,String email,String dob, String Gender,String StateID,String FavoriteAthlete,String FavoriteTeam,String FavoriteMovie,String FavoriteMusic,String FavoriteFood,String FavoriteTeacher,String ProfilePicture,String FavoriteQuote) {

		JSONObject requestObject = null;
		requestObject = JSONUtil.getJSONObject("Username",userName,"LoginSessionKey",getLoginSessionKey(),"Name",name,"Email",email,"DOB",dob, "Gender", Gender, "StateID", StateID,"FavoriteQuote",FavoriteQuote,"FavoriteAthlete",FavoriteAthlete,"FavoriteTeam",FavoriteTeam,"FavoriteMovie",FavoriteMovie,"FavoriteMusic",FavoriteMusic,"FavoriteFood",FavoriteFood,"FavoriteTeacher",FavoriteTeacher,"ProfilePicture",ProfilePicture);
		return null == requestObject ? "" : requestObject.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_SETUP_PROFILE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "SignUp Details=" + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				if (Config.DEBUG) {
					Log.e("", "Failed to SignUp. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {
	}

	@Override
	public void onProgressChange(int progress) {
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("setupprofile", json) && isSuccess()) {

			return true;
		}
		return false;
	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
	}
}