package com.vinfotech.request;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.schollyme.R;
import com.schollyme.model.Song;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class SongMainListRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = SongMainListRequest.class.getSimpleName();

	private static final int REQ_CODE_SONG_LIST = 115;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private List<Song> mSongs;

	public SongMainListRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_SONG_LIST, 0, mMessage);
			}
		});
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	public void getSongListFromServer(String PlayListGUID, String UserGUID, int PageNo, boolean last7Days) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting song list...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSongJSON(PlayListGUID, UserGUID, PageNo);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("song/playlist", REQ_CODE_SONG_LIST, "post", false, jsonData, null, UrlType.SERVICE);
	}

	private String getSongJSON(String PlayListGUID, String UserGUID, int PageNo) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey()/*, "PlayListGUID", PlayListGUID*/);
		try {
			if(PageNo >= 0){
				jsonObj.put("PageNo", PageNo);
				jsonObj.put("PageSize", Config.PAGE_SIZE);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		/*if (!TextUtils.isEmpty(UserGUID)) {
			try {
				jsonObj.put("UserGUID", UserGUID);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}*/

		System.out.println("requestjson    "+jsonObj);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_SONG_LIST:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Song list successful: " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mSongs, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to list song. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("list", json) && isSuccess()) {
			mSongs = Song.getSongs(getDataArray());
			return true;
		}
		return false;
	}
}
