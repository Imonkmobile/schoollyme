package com.vinfotech.request;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.schollyme.model.FavTeamModel;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.Config;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

import org.json.JSONObject;

import java.util.List;

public class GetFavTeamRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = GetFavTeamRequest.class.getSimpleName();

	private static final int REQ_CODE_GET_STATES = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private List<FavTeamModel> mSportsModels;

	public GetFavTeamRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getFavTeamServerRequest() {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getLoginsessionParams();
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("users/favourite_team_list", REQ_CODE_GET_STATES, "post",
				false, jsonData, null, UrlType.SERVICE);

	}


	private String getLoginsessionParams() {
		JSONObject jsonObject = JSONUtil.getJSONObject("LoginSessionKey",getLoginSessionKey());
		return null == jsonObject ? "" : jsonObject.toString();

	}



	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_GET_STATES:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mSportsModels, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed  Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("get_fav_team_list", json) && isSuccess()) {
			mSportsModels = FavTeamModel.getSportsModels(getJsonArray("Data"));
			return true;
		}
		return false;
	}
}