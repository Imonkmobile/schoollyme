package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class AlbumSetCoverRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = AlbumSetCoverRequest.class.getSimpleName();

	private static final int REQ_CODE_SET_COVER_MEDIA = 8;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	public AlbumSetCoverRequest(Context mContext) {
		this.mContext = mContext;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void setCoverMediaInServer(String AlbumGUID, String MediaGUID) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already updating cover media...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSetMediaCoverJSON(AlbumGUID, MediaGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("album/set_cover_media", REQ_CODE_SET_COVER_MEDIA, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getSetMediaCoverJSON(String AlbumGUID, String MediaGUID) {
		JSONObject jsonObj = JSONUtil
				.getJSONObject("LoginSessionKey", getLoginSessionKey(), "AlbumGUID", AlbumGUID, "MediaGUID", MediaGUID);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_SET_COVER_MEDIA:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Media cover update successful");
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, json, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to update media cover. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("set_cover_media", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}
