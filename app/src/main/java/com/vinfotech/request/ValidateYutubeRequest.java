package com.vinfotech.request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.model.Youtube;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;

public class ValidateYutubeRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = ValidateYutubeRequest.class.getSimpleName();

	private static final int REQ_CODE_VALIDATE_URL = 117;
	private static final int REQ_CODE_GET_VIDEO_INFO = 118;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private Youtube mYoutube;

	public ValidateYutubeRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void getVideoDetailsInServer(String VideoID) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already validating...");
			}
			return;
		}

		mRequesting = true;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=" + VideoID + "&format=json",
				REQ_CODE_VALIDATE_URL, "get", false, null, null, UrlType.EXTERNAL);
	}

	public void getVideoInfoInServer(String VideoID) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already validating...");
			}
			return;
		}

		mRequesting = true;
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&id=" + VideoID
				+ "&key=AIzaSyDwirsoehgfeVExRyPU9rOADjeOOupRHCk", REQ_CODE_GET_VIDEO_INFO, "get", false, null, null, UrlType.EXTERNAL);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_VALIDATE_URL:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mYoutube, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to validate. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, false, 0);
				}
			}
			break;
		case REQ_CODE_GET_VIDEO_INFO:
			mRequesting = false;
			if (parseDetails(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mYoutube, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get info. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, false, 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (null != json && json.length() > 100) {
			try {
				JSONObject jsonObject = new JSONObject(json);
				mYoutube = new Youtube(jsonObject);
				return true;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	private boolean parseDetails(String json) {
		if (null != json && json.length() > 100) {
			try {
				JSONObject jsonObject = new JSONObject(json);
				JSONArray itemsJsonArray = jsonObject.optJSONArray("items");
				if (null != itemsJsonArray && itemsJsonArray.length() > 0) {
					JSONObject itemsJsonObject = itemsJsonArray.getJSONObject(0);
					JSONObject snippetJsonObject = itemsJsonObject.optJSONObject("snippet");
					JSONObject contentDetailsJsonObject = itemsJsonObject.optJSONObject("contentDetails");
					if (null != snippetJsonObject && null != contentDetailsJsonObject) {
						mYoutube = new Youtube(itemsJsonObject, snippetJsonObject, contentDetailsJsonObject);
						return true;
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
