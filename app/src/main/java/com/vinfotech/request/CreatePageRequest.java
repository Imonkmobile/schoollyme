package com.vinfotech.request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.vinfotech.utility.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class CreatePageRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = CreatePageRequest.class.getSimpleName();

	private static final int REQ_CODE_CREATE_PAGE_REQUEST = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public CreatePageRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void CreatePageServerRequest(String GroupName,String GroupDescription,String GroupLogoImage,String GroupCoverImage,String CategoryIds,int IsPublic,String SportsID,String UniversityID,String ConferenceID,String GroupGUID) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already Requested....");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getCreatePageJson( GroupName, GroupDescription,GroupLogoImage,GroupCoverImage,CategoryIds,IsPublic,SportsID,UniversityID,ConferenceID,GroupGUID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("group/create_update/", REQ_CODE_CREATE_PAGE_REQUEST, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getCreatePageJson(String GroupName,String GroupDescription,String GroupLogoImage,String GroupCoverImage,String CategoryIds,int IsPublic,String SportsID,String UniversityID,String ConferenceID,String GroupGUID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey",getLoginSessionKey(),"GroupName",GroupName,"GroupDescription",GroupDescription,
				"GroupLogoImage",GroupLogoImage,"GroupCoverImage",GroupCoverImage
				,"IsPublic",String.valueOf(IsPublic),"SportsID",SportsID,"UniversityID",UniversityID,"ConferenceID",ConferenceID,"GroupGUID", GroupGUID);
		
		JSONArray jArray  = new JSONArray(); 
		jArray.put(CategoryIds);
		try {
			jsonObj.put("CategoryIds",jArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_CREATE_PAGE_REQUEST:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to SignIn. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("group/createUpdateGroup/", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}