package com.vinfotech.request;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.schollyme.R;
import com.schollyme.model.Song;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.INetConnListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class SetSongOfDayRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = SetSongOfDayRequest.class.getSimpleName();

	private static final int REQ_CODE_SET_SONG_OF_DAY = 113;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;

	public SetSongOfDayRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
		mHttpConnector.setINetConnListener(new INetConnListener() {
			
			@Override
			public void onNotConnected() {
				mMessage = mContext.getString(R.string.No_internet_connection);
				onResponse(REQ_CODE_SET_SONG_OF_DAY, 0, mMessage);
			}
		});
	}

	public void setSongOfTheDayInServer(String PlayListGUID, String SourceGUID, List<Song> songs) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already setting song of the day...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getSongOfDayJSON(PlayListGUID, SourceGUID, songs);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("song/set_song_of_day", REQ_CODE_SET_SONG_OF_DAY, "post", false, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getSongOfDayJSON(String PlayListGUID, String SourceGUID, List<Song> songs) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "PlayListGUID", PlayListGUID, "SourceGUID", SourceGUID);
		if (null != songs) {
			try {
				jsonObj.put("Song", Song.getJSONArray(songs, false));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_SET_SONG_OF_DAY:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Song of the day set successful: " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to set song of the day. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("set_song_of_day", json) && isSuccess()) {
			return true;
		}
		return false;
	}
}
