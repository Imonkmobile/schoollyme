package com.vinfotech.request;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.vinfotech.utility.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class InviteUserToPageRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = InviteUserToPageRequest.class.getSimpleName();

	private static final int REQ_CODE_INVITE_USER_TO_PAGE = 1;

	private Context mContext;
	private boolean mRequesting;
	private HttpConnector mHttpConnector;

	public InviteUserToPageRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(context);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void InviteToPageServerRequest(String GroupGUID,ArrayList<String> UserGUIDs,int AddForceFully) {

		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "You Already request....");
			}
			return;
		}
		mRequesting = true;
		String jsonData = getInviteToPageFriendJson(GroupGUID,UserGUIDs,AddForceFully);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("group/invite_users/", REQ_CODE_INVITE_USER_TO_PAGE, "post", true, jsonData, null, UrlType.SERVICE);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	private String getInviteToPageFriendJson(String GroupGUID,ArrayList<String> UserGUIDs,int AddForceFully) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey",getLoginSessionKey(),"GroupGUID",GroupGUID,"AddForceFully",String.valueOf(AddForceFully));
		try{
			JSONArray usersArray = new JSONArray();
			for(int i=0;i<UserGUIDs.size();i++){
				usersArray.put(UserGUIDs.get(i));
			}
			jsonObj.put("UsersGUID", usersArray);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null == jsonObj ? "" : jsonObj.toString();
	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_INVITE_USER_TO_PAGE:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "onResponse " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, getMessage(), 0);
				}
			} 
			else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to response. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("group/inviteGroupUsers/",json) && isSuccess()) {
			
			return true;
		}
		return false;
	}
}