package com.vinfotech.request;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.vinfotech.utility.Config;
import com.vinfotech.server.BaseRequest;
import com.vinfotech.server.HttpConnector;
import com.vinfotech.server.HttpConnector.HttpResponseListener;
import com.vinfotech.server.HttpConnector.UrlType;
import com.vinfotech.utility.DialogUtil;
import com.vinfotech.utility.JSONUtil;

public class GetAccessTokenRequest extends BaseRequest implements HttpResponseListener {
	public static final String TAG = GetAccessTokenRequest.class.getSimpleName();

	private static final int REQ_CODE_ACCESS_TOKEN = 111;

	private boolean mRequesting;
	private HttpConnector mHttpConnector;
	private Context mContext;
	private String mAccessToken;
	private String mRefreshToken;

	public GetAccessTokenRequest(Context context) {
		this.mContext = context;
		mRequesting = false;
		mHttpConnector = new HttpConnector(mContext);
		mHttpConnector.setHttpResponseListener(this);
	}

	public void setLoader(View view) {
		mHttpConnector.setLoader(view);
	}

	public void getAccessTokenFromServer(String SourceID) {
		if (mRequesting || !mActivityLive) {
			if (Config.DEBUG) {
				Log.v(TAG, "Already getting access token...");
			}
			return;
		}

		mRequesting = true;
		String jsonData = getAccessTokenJSON(SourceID);
		mHttpConnector.setDialog(DialogUtil.createProgressDialog(mContext, mMsgResId));
		mHttpConnector.executeAsync("song/get_access_token", REQ_CODE_ACCESS_TOKEN, "post", false, jsonData, null, UrlType.SERVICE);
	}

	private String getAccessTokenJSON(String SourceID) {
		JSONObject jsonObj = JSONUtil.getJSONObject("LoginSessionKey", getLoginSessionKey(), "SourceID", SourceID);

		return null == jsonObj ? "" : jsonObj.toString();

	}

	@Override
	public void onResponse(int reqCode, int statusCode, String json) {

		switch (reqCode) {
		case REQ_CODE_ACCESS_TOKEN:
			mRequesting = false;
			if (parse(json)) {
				if (Config.DEBUG) {
					Log.v(TAG, "Access token get successful: " + json);
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(true, mAccessToken, 0);
				}
			} else {
				// Handling Error Messages
				if (Config.DEBUG) {
					Log.e(TAG, "Failed to get access token. Error: " + getMessage());
				}
				if (null != mRequestListener) {
					mRequestListener.onComplete(false, getMessage(), 0);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onCancel(boolean canceled) {

	}

	@Override
	public void onProgressChange(int progress) {

	}

	@Override
	public void setActivityStatus(boolean live) {
		super.mActivityLive = live;
		mRequesting = false;
		mHttpConnector.setActivityStatus(mActivityLive);
	}

	@Override
	protected boolean parse(String json) {
		if (super.parse("get_access_token", json) && isSuccess()) {
			JSONObject jsonObject = getDataObject();
			if (null != jsonObject) {
				mAccessToken = jsonObject.optString("AccessToken");
				mRefreshToken = jsonObject.optString("RefreshToken");
				return true;
			}
		}
		return false;
	}

	public String getRefreshToken() {
		return mRefreshToken;
	}
}
